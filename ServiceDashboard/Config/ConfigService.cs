﻿// <copyright file="ConfigService.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>ServiceDashboard</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2018-01-09</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2018-01-09</lastchangeddate>
namespace ServiceDashboard.Config
{
    using System.Collections.Concurrent;
    using System.Configuration;
    using System.Linq;

    public class ConfigService
    {
        private static readonly ConcurrentDictionary<string, string> AppSettings = new ConcurrentDictionary<string, string>();
        private static readonly ConcurrentDictionary<string, string> ConnectionStrings = new ConcurrentDictionary<string, string>();

        public string GetAppSetting(string settingKey)
        {
            if (!AppSettings.ContainsKey(settingKey))
            {
                if (ConfigurationManager.AppSettings.Count > 0 && ConfigurationManager.AppSettings[settingKey] != null)
                {
                    AppSettings.TryAdd(settingKey, ConfigurationManager.AppSettings[settingKey]);
                }
                else
                {
                    AppSettings.TryAdd(settingKey, string.Empty);
                }
            }

            return AppSettings.FirstOrDefault(s => s.Key.Equals(settingKey)).Value;
        }

        public string GetConnectionString(string connectionKey, bool isAppSettingKey)
        {
            var connectionName = isAppSettingKey ? GetAppSetting(connectionKey) : connectionKey;
            if (!ConnectionStrings.ContainsKey(connectionName))
            {
                if (ConfigurationManager.ConnectionStrings.Count > 0 &&
                    ConfigurationManager.ConnectionStrings[connectionName] != null)
                {
                    ConnectionStrings.TryAdd(connectionName,
                        ConfigurationManager.ConnectionStrings[connectionName]
                            .ConnectionString);
                }
                else
                {
                    ConnectionStrings.TryAdd(connectionName, string.Empty);
                }
            }
            return ConnectionStrings.FirstOrDefault(s => s.Key.Equals(connectionName)).Value;
        }
    }
}