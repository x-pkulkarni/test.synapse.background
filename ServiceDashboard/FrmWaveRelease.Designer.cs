﻿namespace ServiceDashboard
{
    partial class FrmWaveRelease
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRequestType = new System.Windows.Forms.Label();
            this.txtRequestType = new System.Windows.Forms.TextBox();
            this.gbWaveRelease = new System.Windows.Forms.GroupBox();
            this.btnPublish = new System.Windows.Forms.Button();
            this.lblTrace = new System.Windows.Forms.Label();
            this.txtTrace = new System.Windows.Forms.TextBox();
            this.lblPickType = new System.Windows.Forms.Label();
            this.txtPickType = new System.Windows.Forms.TextBox();
            this.lblTaskPriority = new System.Windows.Forms.Label();
            this.txtTaskPriority = new System.Windows.Forms.TextBox();
            this.lblLotNumber = new System.Windows.Forms.Label();
            this.txtLotNumber = new System.Windows.Forms.TextBox();
            this.lblShipId = new System.Windows.Forms.Label();
            this.txtShipId = new System.Windows.Forms.TextBox();
            this.lblWave = new System.Windows.Forms.Label();
            this.txtWave = new System.Windows.Forms.TextBox();
            this.lblFacility = new System.Windows.Forms.Label();
            this.txtFacility = new System.Windows.Forms.TextBox();
            this.lblQuantity = new System.Windows.Forms.Label();
            this.txtQuantity = new System.Windows.Forms.TextBox();
            this.lblItem = new System.Windows.Forms.Label();
            this.txtItem = new System.Windows.Forms.TextBox();
            this.lblOrderId = new System.Windows.Forms.Label();
            this.txtOrderId = new System.Windows.Forms.TextBox();
            this.lblUserId = new System.Windows.Forms.Label();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.gbWaveRelease.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblRequestType
            // 
            this.lblRequestType.AutoSize = true;
            this.lblRequestType.Location = new System.Drawing.Point(24, 41);
            this.lblRequestType.Name = "lblRequestType";
            this.lblRequestType.Size = new System.Drawing.Size(71, 13);
            this.lblRequestType.TabIndex = 0;
            this.lblRequestType.Text = "RequestType";
            // 
            // txtRequestType
            // 
            this.txtRequestType.Location = new System.Drawing.Point(131, 41);
            this.txtRequestType.Name = "txtRequestType";
            this.txtRequestType.Size = new System.Drawing.Size(161, 20);
            this.txtRequestType.TabIndex = 1;
            // 
            // gbWaveRelease
            // 
            this.gbWaveRelease.BackColor = System.Drawing.Color.LightSteelBlue;
            this.gbWaveRelease.Controls.Add(this.btnPublish);
            this.gbWaveRelease.Controls.Add(this.lblTrace);
            this.gbWaveRelease.Controls.Add(this.txtTrace);
            this.gbWaveRelease.Controls.Add(this.lblPickType);
            this.gbWaveRelease.Controls.Add(this.txtPickType);
            this.gbWaveRelease.Controls.Add(this.lblTaskPriority);
            this.gbWaveRelease.Controls.Add(this.txtTaskPriority);
            this.gbWaveRelease.Controls.Add(this.lblLotNumber);
            this.gbWaveRelease.Controls.Add(this.txtLotNumber);
            this.gbWaveRelease.Controls.Add(this.lblShipId);
            this.gbWaveRelease.Controls.Add(this.txtShipId);
            this.gbWaveRelease.Controls.Add(this.lblWave);
            this.gbWaveRelease.Controls.Add(this.txtWave);
            this.gbWaveRelease.Controls.Add(this.lblFacility);
            this.gbWaveRelease.Controls.Add(this.txtFacility);
            this.gbWaveRelease.Controls.Add(this.lblQuantity);
            this.gbWaveRelease.Controls.Add(this.txtQuantity);
            this.gbWaveRelease.Controls.Add(this.lblItem);
            this.gbWaveRelease.Controls.Add(this.txtItem);
            this.gbWaveRelease.Controls.Add(this.lblOrderId);
            this.gbWaveRelease.Controls.Add(this.txtOrderId);
            this.gbWaveRelease.Controls.Add(this.lblUserId);
            this.gbWaveRelease.Controls.Add(this.txtUserId);
            this.gbWaveRelease.Controls.Add(this.lblRequestType);
            this.gbWaveRelease.Controls.Add(this.txtRequestType);
            this.gbWaveRelease.Location = new System.Drawing.Point(29, 12);
            this.gbWaveRelease.Name = "gbWaveRelease";
            this.gbWaveRelease.Size = new System.Drawing.Size(678, 381);
            this.gbWaveRelease.TabIndex = 2;
            this.gbWaveRelease.TabStop = false;
            this.gbWaveRelease.Text = "Wave Release Request";
            // 
            // btnPublish
            // 
            this.btnPublish.Location = new System.Drawing.Point(27, 332);
            this.btnPublish.Name = "btnPublish";
            this.btnPublish.Size = new System.Drawing.Size(75, 23);
            this.btnPublish.TabIndex = 13;
            this.btnPublish.Text = "Publish";
            this.btnPublish.UseVisualStyleBackColor = true;
            this.btnPublish.Click += new System.EventHandler(this.btnPublish_Click);
            // 
            // lblTrace
            // 
            this.lblTrace.AutoSize = true;
            this.lblTrace.Location = new System.Drawing.Point(378, 255);
            this.lblTrace.Name = "lblTrace";
            this.lblTrace.Size = new System.Drawing.Size(35, 13);
            this.lblTrace.TabIndex = 22;
            this.lblTrace.Text = "Trace";
            // 
            // txtTrace
            // 
            this.txtTrace.Location = new System.Drawing.Point(485, 255);
            this.txtTrace.Name = "txtTrace";
            this.txtTrace.Size = new System.Drawing.Size(161, 20);
            this.txtTrace.TabIndex = 12;
            // 
            // lblPickType
            // 
            this.lblPickType.AutoSize = true;
            this.lblPickType.Location = new System.Drawing.Point(24, 255);
            this.lblPickType.Name = "lblPickType";
            this.lblPickType.Size = new System.Drawing.Size(55, 13);
            this.lblPickType.TabIndex = 20;
            this.lblPickType.Text = "Pick Type";
            // 
            // txtPickType
            // 
            this.txtPickType.Location = new System.Drawing.Point(131, 255);
            this.txtPickType.Name = "txtPickType";
            this.txtPickType.Size = new System.Drawing.Size(161, 20);
            this.txtPickType.TabIndex = 6;
            // 
            // lblTaskPriority
            // 
            this.lblTaskPriority.AutoSize = true;
            this.lblTaskPriority.Location = new System.Drawing.Point(378, 211);
            this.lblTaskPriority.Name = "lblTaskPriority";
            this.lblTaskPriority.Size = new System.Drawing.Size(65, 13);
            this.lblTaskPriority.TabIndex = 18;
            this.lblTaskPriority.Text = "Task Priority";
            // 
            // txtTaskPriority
            // 
            this.txtTaskPriority.Location = new System.Drawing.Point(485, 211);
            this.txtTaskPriority.Name = "txtTaskPriority";
            this.txtTaskPriority.Size = new System.Drawing.Size(161, 20);
            this.txtTaskPriority.TabIndex = 11;
            // 
            // lblLotNumber
            // 
            this.lblLotNumber.AutoSize = true;
            this.lblLotNumber.Location = new System.Drawing.Point(378, 167);
            this.lblLotNumber.Name = "lblLotNumber";
            this.lblLotNumber.Size = new System.Drawing.Size(62, 13);
            this.lblLotNumber.TabIndex = 16;
            this.lblLotNumber.Text = "Lot Number";
            // 
            // txtLotNumber
            // 
            this.txtLotNumber.Location = new System.Drawing.Point(485, 167);
            this.txtLotNumber.Name = "txtLotNumber";
            this.txtLotNumber.Size = new System.Drawing.Size(161, 20);
            this.txtLotNumber.TabIndex = 10;
            // 
            // lblShipId
            // 
            this.lblShipId.AutoSize = true;
            this.lblShipId.Location = new System.Drawing.Point(378, 121);
            this.lblShipId.Name = "lblShipId";
            this.lblShipId.Size = new System.Drawing.Size(40, 13);
            this.lblShipId.TabIndex = 14;
            this.lblShipId.Text = "Ship Id";
            // 
            // txtShipId
            // 
            this.txtShipId.Location = new System.Drawing.Point(485, 121);
            this.txtShipId.Name = "txtShipId";
            this.txtShipId.Size = new System.Drawing.Size(161, 20);
            this.txtShipId.TabIndex = 9;
            // 
            // lblWave
            // 
            this.lblWave.AutoSize = true;
            this.lblWave.Location = new System.Drawing.Point(378, 81);
            this.lblWave.Name = "lblWave";
            this.lblWave.Size = new System.Drawing.Size(36, 13);
            this.lblWave.TabIndex = 12;
            this.lblWave.Text = "Wave";
            // 
            // txtWave
            // 
            this.txtWave.Location = new System.Drawing.Point(485, 81);
            this.txtWave.Name = "txtWave";
            this.txtWave.Size = new System.Drawing.Size(161, 20);
            this.txtWave.TabIndex = 8;
            // 
            // lblFacility
            // 
            this.lblFacility.AutoSize = true;
            this.lblFacility.Location = new System.Drawing.Point(378, 41);
            this.lblFacility.Name = "lblFacility";
            this.lblFacility.Size = new System.Drawing.Size(39, 13);
            this.lblFacility.TabIndex = 10;
            this.lblFacility.Text = "Facility";
            // 
            // txtFacility
            // 
            this.txtFacility.Location = new System.Drawing.Point(485, 41);
            this.txtFacility.Name = "txtFacility";
            this.txtFacility.Size = new System.Drawing.Size(161, 20);
            this.txtFacility.TabIndex = 7;
            // 
            // lblQuantity
            // 
            this.lblQuantity.AutoSize = true;
            this.lblQuantity.Location = new System.Drawing.Point(24, 211);
            this.lblQuantity.Name = "lblQuantity";
            this.lblQuantity.Size = new System.Drawing.Size(46, 13);
            this.lblQuantity.TabIndex = 8;
            this.lblQuantity.Text = "Quantity";
            // 
            // txtQuantity
            // 
            this.txtQuantity.Location = new System.Drawing.Point(131, 211);
            this.txtQuantity.Name = "txtQuantity";
            this.txtQuantity.Size = new System.Drawing.Size(161, 20);
            this.txtQuantity.TabIndex = 5;
            // 
            // lblItem
            // 
            this.lblItem.AutoSize = true;
            this.lblItem.Location = new System.Drawing.Point(24, 167);
            this.lblItem.Name = "lblItem";
            this.lblItem.Size = new System.Drawing.Size(27, 13);
            this.lblItem.TabIndex = 6;
            this.lblItem.Text = "Item";
            // 
            // txtItem
            // 
            this.txtItem.Location = new System.Drawing.Point(131, 167);
            this.txtItem.Name = "txtItem";
            this.txtItem.Size = new System.Drawing.Size(161, 20);
            this.txtItem.TabIndex = 4;
            // 
            // lblOrderId
            // 
            this.lblOrderId.AutoSize = true;
            this.lblOrderId.Location = new System.Drawing.Point(24, 121);
            this.lblOrderId.Name = "lblOrderId";
            this.lblOrderId.Size = new System.Drawing.Size(45, 13);
            this.lblOrderId.TabIndex = 4;
            this.lblOrderId.Text = "Order Id";
            // 
            // txtOrderId
            // 
            this.txtOrderId.Location = new System.Drawing.Point(131, 121);
            this.txtOrderId.Name = "txtOrderId";
            this.txtOrderId.Size = new System.Drawing.Size(161, 20);
            this.txtOrderId.TabIndex = 3;
            // 
            // lblUserId
            // 
            this.lblUserId.AutoSize = true;
            this.lblUserId.Location = new System.Drawing.Point(24, 81);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(41, 13);
            this.lblUserId.TabIndex = 2;
            this.lblUserId.Text = "User Id";
            // 
            // txtUserId
            // 
            this.txtUserId.Location = new System.Drawing.Point(131, 81);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(161, 20);
            this.txtUserId.TabIndex = 2;
            // 
            // FrmWaveRelease
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 410);
            this.Controls.Add(this.gbWaveRelease);
            this.Name = "FrmWaveRelease";
            this.Text = "FrmWaveRelease";
            this.Load += new System.EventHandler(this.FrmWaveRelease_Load);
            this.gbWaveRelease.ResumeLayout(false);
            this.gbWaveRelease.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblRequestType;
        private System.Windows.Forms.TextBox txtRequestType;
        private System.Windows.Forms.GroupBox gbWaveRelease;
        private System.Windows.Forms.Label lblTaskPriority;
        private System.Windows.Forms.TextBox txtTaskPriority;
        private System.Windows.Forms.Label lblLotNumber;
        private System.Windows.Forms.TextBox txtLotNumber;
        private System.Windows.Forms.Label lblShipId;
        private System.Windows.Forms.TextBox txtShipId;
        private System.Windows.Forms.Label lblWave;
        private System.Windows.Forms.TextBox txtWave;
        private System.Windows.Forms.Label lblFacility;
        private System.Windows.Forms.TextBox txtFacility;
        private System.Windows.Forms.Label lblQuantity;
        private System.Windows.Forms.TextBox txtQuantity;
        private System.Windows.Forms.Label lblItem;
        private System.Windows.Forms.TextBox txtItem;
        private System.Windows.Forms.Label lblOrderId;
        private System.Windows.Forms.TextBox txtOrderId;
        private System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.Label lblTrace;
        private System.Windows.Forms.TextBox txtTrace;
        private System.Windows.Forms.Label lblPickType;
        private System.Windows.Forms.TextBox txtPickType;
        private System.Windows.Forms.Button btnPublish;
    }
}