﻿// <copyright file="ServiceResponse.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>ServiceDashboard</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2017-07-25</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2017-07-25</lastchangeddate>
namespace ServiceDashboard
{
    public class ServiceResponse
    {
        public string Server { get; set; }
        public string Environment { get; set; }
        public string ConnectionKey { get; set; }
        public int NoOfThreads { get; set; }
        public string Status { get; set; }
    }
}