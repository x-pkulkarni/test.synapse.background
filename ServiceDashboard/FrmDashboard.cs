﻿// <copyright file="FrmDashboard.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>ServiceDashboard</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2017-07-25</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2017-07-25</lastchangeddate>

namespace ServiceDashboard
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Linq;
    using System.Threading;
    using System.Windows.Forms;
    using Autofac;
    using AutoMapper;
    using Dapper;
    using Oracle.ManagedDataAccess.Client;
    using Synapse.Backgrounds.Configuration;
    using Synapse.Backgrounds.Core;
    using Synapse.Backgrounds.Grains.Contract;
    using Synapse.Backgrounds.Grains.Contract.Command;
    using Synapse.Backgrounds.Infrastructure.Logging.Interface;
    using Synapse.Backgrounds.Infrastructure.MessageQueue;
    using Synapse.Backgrounds.Infrastructure.MessageQueue.Interface;
    using Synapse.Backgrounds.Infrastructure.Profiler.Interface;
    using Synapse.Backgrounds.Publisher;
    using Synapse.Backgrounds.Publisher.Data;
    using Synapse.Backgrounds.Publisher.Data.Interface;
    using Synapse.Backgrounds.Publisher.Data.Mapper;
    using Synapse.Backgrounds.Publisher.DependencyModule.cs;
    using Synapse.Backgrounds.Publisher.DependencyModule.cs.Interface;
    using Synapse.Backgrounds.Publisher.Processor;
    using Synapse.Backgrounds.Publisher.Processor.Interface;

    public partial class FrmDashboard : Form
    {
        private readonly IMessageQueueFactory _messageQueueFactory;
        private readonly IComponentContext _autofacContext;
        private ControlAction CurrentAction { get; set; }
        private readonly BrokerService _brokerService;
        private readonly ServiceControlCore _serviceControlCore;
        private readonly IRequestProcessor _requestProcessor;
        private readonly IRequestProvider _requestProvider;
        private readonly ILoggerService _loggerService;
        private readonly IConfigService _configService;
        private readonly IProfileService _profileService;
        private readonly IOrderDbContext _orderDbContext;
        private readonly IKeyedTypeFactory _keyedTypeFactory;
        private readonly Dictionary<int, string> _propertyStack;
        private readonly IAppLockService _appLockService;
        private CommandMapper _commandMapper;
        private readonly IOracleConnectionProvider _oracleConnection;
   

        public FrmDashboard()
        {
            InitializeComponent();
            _configService = new ConfigService();
            _oracleConnection = new OracleConnectionProvider(_configService);
            _orderDbContext = new OrderDbContext();
            _messageQueueFactory = new MessageQueueFactory(_configService);
           _requestProvider = new RequestProvider(new CommandMapper(_keyedTypeFactory));
            _brokerService = new BrokerService(_configService, _messageQueueFactory, _appLockService, _loggerService, _oracleConnection);
            _requestProcessor = new RequestProcessor(_oracleConnection, _requestProvider, _profileService, _loggerService, _orderDbContext, _brokerService);
            _serviceControlCore = new ServiceControlCore(_configService, _requestProcessor,  _loggerService, _appLockService, _brokerService);
            CurrentAction = ControlAction.Get;
            IntializeResponseGrid();
            IntializeWavesGrid();
            //SubscribeServiceControlRequest();
            LoadFacilities();
        }

        //private void btnGet_Click(object sender, EventArgs e)
        //{
        //    CurrentAction = ControlAction.Get;
        //    _messageQueueFactory.CreateBus("LOG_MESSAGE_QUEUE", true)
        //        .PublishAsync(new ServiceContext
        //        {
        //            Status = ServiceStatus.Running
        //        });
        //}

        //private void btnStart_Click(object sender, EventArgs e)
        //{
        //    CurrentAction = ControlAction.Start;
        //    _messageQueueFactory.CreateBus("LOG_MESSAGE_QUEUE", true)
        //        .PublishAsync(new ServiceContext
        //        {
        //            Status = ServiceStatus.Running
        //        });
        //}

        //private void btnStop_Click(object sender, EventArgs e)
        //{
        //    CurrentAction = ControlAction.Stop;
        //    _messageQueueFactory.CreateBus("LOG_MESSAGE_QUEUE", true)
        //        .PublishAsync(new ServiceContext
        //        {
        //            Status = ServiceStatus.Running
        //        });
        //}

        //private void btnAdd_Click(object sender, EventArgs e)
        //{
        //    CurrentAction = ControlAction.Add;
        //    _messageQueueFactory.CreateBus("LOG_MESSAGE_QUEUE", true)
        //        .PublishAsync(new ServiceContext
        //        {
        //            Status = ServiceStatus.Running
                    
        //        });
        //}

        //private void btnRemove_Click(object sender, EventArgs e)
        //{
        //    CurrentAction = ControlAction.Remove;
        //    _messageQueueFactory.CreateBus("LOG_MESSAGE_QUEUE", true)
        //        .PublishAsync(new ServiceContext
        //        {
        //            Status = ServiceStatus.Running
        //        });
        //}

        //private void SubscribeServiceControlRequest()
        //{
        //    _messageQueueFactory.CreateBus("LOG_MESSAGE_QUEUE", true).SubscribeAsync<ServiceContext>(
        //        async (message, context) =>
        //        {
        //            await Task
        //                .Factory.StartNew(() =>
        //                {
        //                    ProcessAndBindServiceResponse(message);
        //                }, TaskCreationOptions.AttachedToParent).ContinueWith(
        //                    task =>
        //                    {
        //                        if (task.IsCompleted && !task.IsFaulted)
        //                        {
        //                            if (InvokeRequired)
        //                            {
        //                                Invoke(new MethodInvoker(delegate
        //                                {
        //                                    lblMessage.Text =
        //                                        $@"Service status request completed sucessfully at : {DateTime.Now}";
        //                                }));
        //                            }
        //                        }
        //                        else
        //                        {
        //                            if (InvokeRequired)
        //                            {
        //                                Invoke(new MethodInvoker(delegate
        //                                {
        //                                    lblMessage.Text =
        //                                        @"Service status request returned an error while processing.";
        //                                }));
        //                            }
        //                        }
        //                    });

        //        }, cfg => cfg.WithQueue(q => q.WithName("LOG_MESSAGE_QUEUE")));
        //}

        //private void ProcessAndBindServiceResponse(ServiceContextResponse response)
        //{
        //    var responses = response.ActiveServiceContexts.Select(responseActiveServiceContext => new ServiceResponse
        //    {
        //        Server = response.Server,
        //        Environment = responseActiveServiceContext.Environment,
        //        ConnectionKey = responseActiveServiceContext.ConnectionKey,
        //        NoOfThreads = responseActiveServiceContext.NoOfThreads,
        //        Status = responseActiveServiceContext.Status.ToString()
        //    })
        //        .ToList();

        //    switch (CurrentAction)
        //    {
        //        case ControlAction.Start:
        //            SendServiceControlRequest(response, ServiceCommand.Start);
        //            break;
        //        case ControlAction.Stop:
        //            SendServiceControlRequest(response, ServiceCommand.Stop);
        //            break;
        //        case ControlAction.Add:
        //            foreach (var responseActiveServiceContext in response.ActiveServiceContexts)
        //            {
        //                responseActiveServiceContext.NoOfThreads++;
        //            }
        //            SendServiceControlRequest(response, ServiceCommand.Start);
        //            break;
        //        case ControlAction.Remove:
        //            foreach (var responseActiveServiceContext in response.ActiveServiceContexts)
        //            {
        //                responseActiveServiceContext.NoOfThreads--;
        //            }
        //            SendServiceControlRequest(response, ServiceCommand.Start);
        //            break;
        //        default:
        //            BindServiceResponse(new BindingList<ServiceResponse>(responses));
        //            break;
        //    }
        //}

        private void IntializeResponseGrid()
        {
            grdResponse.AutoGenerateColumns = false;

            var col = new DataGridViewTextBoxColumn
            {
                Name = "Server",
                HeaderText = @"Server",
                DataPropertyName = "Server"
            };

            grdResponse.Columns.Add(col);

            col = new DataGridViewTextBoxColumn
            {
                Name = "Environment",
                HeaderText = @"Environment",
                DataPropertyName = "Environment"
            };

            grdResponse.Columns.Add(col);

            col = new DataGridViewTextBoxColumn
            {
                Name = "NoOfThreads",
                HeaderText = @"No. Of Threads",
                DataPropertyName = "NoOfThreads"
            };

            grdResponse.Columns.Add(col);

            col = new DataGridViewTextBoxColumn
            {
                Name = "Status",
                HeaderText = @"Status",
                DataPropertyName = "Status"
            };

            grdResponse.Columns.Add(col);
        }

        private void BindServiceResponse(BindingList<ServiceResponse> responses)
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(delegate
                {
                    grdResponse.DataSource = responses;
                }));
            }
        }

        //private void SendServiceControlRequest(ServiceContextResponse response, ServiceCommand command)
        //{
        //    foreach (var responseActiveServiceContext in response.ActiveServiceContexts)
        //    {
        //        _messageQueueFactory.CreateBus("LOG_MESSAGE_QUEUE", true).PublishAsync(new ServiceControlRequest
        //        {
        //            Command = command,
        //            Context = responseActiveServiceContext,
        //            RequestedUser = "Dashboard",
        //            RequestedAt = DateTime.Now
        //        }, Guid.Empty,
        //            cfg => cfg.WithExchange(exchange => exchange.WithType(ExchangeType.Topic))
        //                .WithRoutingKey(response.Server));
        //    }
        //}

        private void LoadFacilities()
        {
            using (var connection = new OracleConnection(_configService.GetConnectionString("Environment1", true)))
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }
                var facilities =
                    connection.Query<FacilityModel>("SELECT FACILITY, NAME FROM ALPS.FACILITY ORDER BY FACILITY", null,
                        null, true, null, CommandType.Text);

                if (facilities == null) return;
                cbFacility.DataSource = facilities;
                cbFacility.DisplayMember = "Facility";
                cbFacility.ValueMember = "Facility";
            }
        }

        private void SearchCommittedWaves()
        {
            long.TryParse(txtWave.Text, out long wave);
            var sql =
                "SELECT WAVE AS WAVEID, DESCR, WAVESTATUS, FACILITY, LASTUSER AS USERID, TASKPRIORITY, PICKTYPE, CNTORDER, QTYORDER, QTYCOMMIT FROM ALPS.WAVES WHERE WAVESTATUS IN ('1','3')  " +
                $"AND WAVE = CASE WHEN NVL({wave}, 0) = 0 THEN WAVE ELSE NVL({wave}, 0) END " +
                $"AND FACILITY = CASE WHEN NVL('{cbFacility.Text}', '(none)') = '(none)' THEN FACILITY ELSE NVL('{cbFacility.Text}', '(none)') END " +
                $"AND PICKTYPE = CASE WHEN NVL('{cbPickType.Text}', '(none)') = '(none)' THEN PICKTYPE ELSE NVL('{cbPickType.Text}', '(none)') END " +
                $"AND LASTUPDATE BETWEEN TO_DATE('{dtpFrom.Value:MM/dd/yyyy}', 'MM/dd/yyyy') AND TO_DATE('{dtpTo.Value:MM/dd/yyyy}', 'MM/dd/yyyy')";

            using (var connection = new OracleConnection(_configService.GetConnectionString("Environment1", true)))
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                }

                var waves = connection.Query<Wave>(sql, null, null, true, null, CommandType.Text).ToList();
                grdWaves.DataSource = new BindingList<Wave>(waves);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchCommittedWaves();
        }


        private void IntializeWavesGrid()
        {
            grdWaves.AutoGenerateColumns = false;

            var col = new DataGridViewTextBoxColumn
            {
                Name = "Facility",
                HeaderText = @"Facility",
                DataPropertyName = "Facility"
            };

            grdWaves.Columns.Add(col);

            col = new DataGridViewTextBoxColumn
            {
                Name = "WaveId",
                HeaderText = @"Wave Id",
                DataPropertyName = "WaveId"
            };

            grdWaves.Columns.Add(col);

            col = new DataGridViewTextBoxColumn
            {
                Name = "Descr",
                HeaderText = @"Description",
                DataPropertyName = "Descr"
            };

            grdWaves.Columns.Add(col);

            col = new DataGridViewTextBoxColumn
            {
                Name = "WaveStatus",
                HeaderText = @"Wave Status",
                DataPropertyName = "WaveStatus"
            };

            grdWaves.Columns.Add(col);

            col = new DataGridViewTextBoxColumn
            {
                Name = "UserId",
                HeaderText = @"User",
                DataPropertyName = "UserId"
            };

            grdWaves.Columns.Add(col);

            col = new DataGridViewTextBoxColumn
            {
                Name = "TaskPriority",
                HeaderText = @"Task Priority",
                DataPropertyName = "TaskPriority"
            };

            grdWaves.Columns.Add(col);

            col = new DataGridViewTextBoxColumn
            {
                Name = "PickType",
                HeaderText = @"Pick Type",
                DataPropertyName = "PickType"
            };

            grdWaves.Columns.Add(col);

            col = new DataGridViewTextBoxColumn
            {
                Name = "CntOrder",
                HeaderText = @"Order Count",
                DataPropertyName = "CntOrder"
            };

            grdWaves.Columns.Add(col);

            col = new DataGridViewTextBoxColumn
            {
                Name = "QtyOrder",
                HeaderText = @"Qty. Order",
                DataPropertyName = "QtyOrder"
            };

            grdWaves.Columns.Add(col);

            col = new DataGridViewTextBoxColumn
            {
                Name = "QtyCommit",
                HeaderText = @"Qty. Commit",
                DataPropertyName = "QtyCommit"
            };

            grdWaves.Columns.Add(col);
        }

        private void btnReleaseWave_Click(object sender, EventArgs e)
        {
            if (grdWaves.CurrentRow != null)
            {
                var wave = grdWaves.CurrentRow.DataBoundItem as Wave;
                

                if (wave != null)
                {
                    var serviceContexts = _configService.GetServiceContexts();
                  
                    var waveReleaseRequest = new ReleaseWaveCommand()
                    {
                        CustomerId = wave.Facility,
                        Facility = wave.Facility,
                        UserId = wave.UserId,
                        Wave = wave.WaveId,
                        OrderId = 0,
                        ShipId = 0,
                        Item = "(none)",
                        LotNumber = "(none)",
                        Quantity = 0.00,
                        TaskPriority = wave.TaskPriority,
                        PickType = wave.PickType,
                        Trace = "N",
                        SessionId = new Random().Next(),
                        Environment = "SYNTSTBConn"
                    };

                    _brokerService.PushMessage(serviceContexts.FirstOrDefault(), waveReleaseRequest);
                    MessageBox.Show("Release message pushed to queue.");

                }
            }
        }

        private void btnUnrelease_Click(object sender, EventArgs e)
        {
            if (grdWaves.CurrentRow != null)
            {
                var wave = grdWaves.CurrentRow.DataBoundItem as Wave;


                if (wave != null)
                {
                    var serviceContexts = _configService.GetServiceContexts();

                    var waveUnReleaseRequest = new UnreleaseWaveCommand()
                    {
                        CustomerId = wave.Facility,
                        Facility = wave.Facility,
                        UserId = wave.UserId,
                        Wave = wave.WaveId,
                        OrderId = 0,
                        ShipId = 0,
                        Item = "(none)",
                        LotNumber = "(none)",
                        Quantity = 0.00,
                        TaskPriority = wave.TaskPriority,
                        PickType = wave.PickType,
                        Trace = "N",
                        SessionId = new Random().Next(),
                        Environment = "SYNTSTBConn"
                    };

                    _brokerService.PushMessage(serviceContexts.FirstOrDefault(), waveUnReleaseRequest);
                    MessageBox.Show("UnRelease message pushed to queue.");

                }
            }
        }

        private void btnRegenerate_Click(object sender, EventArgs e)
        {
            if (grdWaves.CurrentRow != null)
            {
                var wave = grdWaves.CurrentRow.DataBoundItem as Wave;
               
                if (string.IsNullOrEmpty(taskID.Text))
                {
                    MessageBox.Show("Not allowed.Enter taskid to regenerate");
                    return;
                }

                if (wave != null)
                {
                    var serviceContexts = _configService.GetServiceContexts();

                    var waveRegenerateRequest = new RegenerateBatchCommand()
                    {
                        CustomerId = wave.Facility,
                        Facility = wave.Facility,
                        UserId = wave.UserId,
                        Wave = wave.WaveId,
                        OrderId = 0,
                        ShipId = 0,
                        Item = "(none)",
                        LotNumber = "(none)",
                        Quantity = 0.00,
                        TaskPriority = wave.TaskPriority,
                        PickType = wave.PickType,
                        Trace = "N",
                        SessionId = new Random().Next(),
                        Environment = "SYNTSTBConn",
                        TaskId = Convert.ToInt64(taskID.Text)
                    };

                    _brokerService.PushMessage(serviceContexts.FirstOrDefault(), waveRegenerateRequest);
                    MessageBox.Show("UnRelease message pushed to queue.");

                }
            }
        }
    }
}
