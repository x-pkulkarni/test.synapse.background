﻿// <copyright file="Facility.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>ServiceDashboard</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2018-01-09</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2018-01-09</lastchangeddate>
namespace ServiceDashboard
{
    public class FacilityModel
    {
        public string Facility { get; set; }
        public string Name { get; set; }
    }
}