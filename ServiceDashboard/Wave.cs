﻿// <copyright file="Wave.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>ServiceDashboard</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2018-01-09</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2018-01-09</lastchangeddate>
namespace ServiceDashboard
{
    public class Wave
    {
        public long WaveId { get; set; }
        public string Descr { get; set; }
        public int WaveStatus { get; set; }
        public string Facility { get; set; }
        public string UserId { get; set; }
        public string TaskPriority { get; set; }
        public string PickType { get; set; }
        public int? CntOrder { get; set; }
        public int? QtyOrder { get; set; }
        public int? QtyCommit { get; set; }
    }
}