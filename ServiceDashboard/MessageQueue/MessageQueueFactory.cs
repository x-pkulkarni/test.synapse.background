﻿// <copyright file="MessageQueueFactory.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Infrastructure</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2017-07-06</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2017-07-06</lastchangeddate>
namespace ServiceDashboard.MessageQueue
{
    using System.Collections.Concurrent;
    using RawRabbit;
    using RawRabbit.Common;
    using RawRabbit.vNext;
    using Synapse.Backgrounds.Configuration;

    public class MessageQueueFactory : IMessageQueueFactory
    {
        private readonly IConfigService _configService;
        private static readonly ConcurrentDictionary<string, IBusClient> MessageBusDictionary = new ConcurrentDictionary<string, IBusClient>();

        public MessageQueueFactory()
        {
            _configService = new ConfigService();
        }

        public IBusClient CreateBus(string connectionKey, bool isAppSettingKey)
        {
            return CreateMessageBusInstance(_configService.GetConnectionString(connectionKey, isAppSettingKey));
        }

        private static IBusClient CreateMessageBusInstance(string key)
        {
            var config = ConnectionStringParser.Parse(key);

            IBusClient messageBusInstance;
            // Check if the instance of a bus is already there with the same key. If yes return that.
            MessageBusDictionary.TryGetValue(key, out messageBusInstance);

            // if found return the instance.
            if (messageBusInstance != null) return messageBusInstance;

            // Else create a new instance , add to the dict and then return the same instance.
            messageBusInstance = BusClientFactory.CreateDefault(config);
            MessageBusDictionary.TryAdd(key, messageBusInstance);
            return messageBusInstance;
        }
    }
}