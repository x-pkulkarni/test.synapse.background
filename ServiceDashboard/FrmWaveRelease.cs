﻿using ServiceDashboard.MessageQueue;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Synapse.Backgrounds.Integration;
using RawRabbit.Configuration.Exchange;

namespace ServiceDashboard
{
    public partial class FrmWaveRelease : Form
    {
        private readonly IMessageQueueFactory _messageQueueFactory;
        public FrmWaveRelease()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            _messageQueueFactory = new MessageQueueFactory();
        }

        private void btnPublish_Click(object sender, EventArgs e)
        {

            #region Input validation
            int wave, orderId, shipId, quantity;



            if (string.IsNullOrWhiteSpace(txtRequestType.Text) ||
                string.IsNullOrWhiteSpace(txtFacility.Text) ||
                string.IsNullOrWhiteSpace(txtUserId.Text) ||
                string.IsNullOrWhiteSpace(txtWave.Text) || !int.TryParse(txtWave.Text, out wave) ||
                string.IsNullOrWhiteSpace(txtOrderId.Text) || !int.TryParse(txtOrderId.Text, out orderId) ||
                string.IsNullOrWhiteSpace(txtShipId.Text) || !int.TryParse(txtShipId.Text, out shipId) ||
                string.IsNullOrWhiteSpace(txtItem.Text) ||
                string.IsNullOrWhiteSpace(txtLotNumber.Text) ||
                string.IsNullOrWhiteSpace(txtQuantity.Text) || !int.TryParse(txtQuantity.Text, out quantity) ||
                string.IsNullOrWhiteSpace(txtTaskPriority.Text) || string.IsNullOrWhiteSpace(txtTaskPriority.Text) ||
                string.IsNullOrWhiteSpace(txtTrace.Text)
                )
            {
                MessageBox.Show("Invalid data!");
                return;
            }
            #endregion

            //_messageQueueFactory.CreateBus("PROCESS_QUEUE_CONNECTION_DEV", false)
            //   .PublishAsync(new WaveReleaseRequest
            //   {
            //       RequestType = (Enums.RequestType) Enum.Parse(typeof(Enums.RequestType), txtRequestType.Text),
            //       Facility = txtFacility.Text,
            //       UserId = txtUserId.Text,
            //       Wave = wave,
            //       OrderId = orderId,
            //       ShipId = shipId,
            //       Item = txtItem.Text,
            //       LotNumber = txtLotNumber.Text,
            //       Quantity = quantity,
            //       TaskPriority = txtTaskPriority.Text,
            //       PickType = txtPickType.Text,
            //       Trace = txtTrace.Text,
            //       SessionId = new Random().Next(),
            //       Environment= "SYNTSTBConn"
            //   }, Guid.Empty,
            //        cfg => cfg.WithExchange(exchange => exchange.WithType(ExchangeType.Topic))
            //            .WithRoutingKey(string.Empty));
            MessageBox.Show("Wave Release request  has been published");
        }

        private void FrmWaveRelease_Load(object sender, EventArgs e)
        {
            txtPickType.Text = "BAT";
            txtRequestType.Text = "AggInven";
            txtFacility.Text = "RHR";
            txtUserId.Text = "RRAJASEK";
            txtWave.Text = "410662";
            txtOrderId.Text = "19071622";
            txtShipId.Text = "1";
            txtItem.Text = "100601";
            txtLotNumber.Text = "(none)";
            txtQuantity.Text = "3";
            txtTaskPriority.Text = "3";
        }
    }
}
