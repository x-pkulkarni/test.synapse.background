echo off
setlocal enableDelayedExpansion
REM ****************************************************************************************
REM ***   ReportManager-Sonar-Runner_v3.bat  ****
REM This script executes the Sonar Runner analysis. 
REM On the first execution, it will prompt for the project name and key.  
REM It will store those values and the analysis version in sonarParameter.txt
REM Thereafter it will reads the parameters from a supporting text file and 
REM update the version with each execution.
REM ****************************************************************************************
REM
echo.
echo Starting SonarRunner
REM ****************************************************************************************
REM Set Environment.  To switch environments, change which value is assigned to SonarEnvironment.
set SonarProd=http://SonarQube.OHLogistics.com
set SonarDev=http://SonarQube-Dev.OHLogistics.com
REM set SonarEnvironment=%SonarProd%
set SonarEnvironment=%SonarDev%
set MsBuildPath="C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild\15.0\Bin\"

echo Checking for parameter file.
if exist sonarParameters.txt (
    echo SonarQube parameters file found.
    for /f "delims== tokens=1,2" %%G in (sonarParameters.txt) do set %%G=%%H
) else (
    echo SonarQube parameters file not found. Please enter parameters.
    set /p sonarName="Enter a unique name for the project: "
    set /p sonarKey="Enter a unique key for the project: "
    set /a sonarVersion=1
)
echo.
:ConfirmParameters
REM Remove trailing spaces from Name & Key
for /f "tokens=* delims= " %%A in ('echo %sonarName% ') do set sonarName=%%A
set sonarName=%sonarName:~0,-1%
for /f "tokens=* delims= " %%A in ('echo %sonarKey% ') do set sonarKey=%%A
set sonarKey=%sonarKey:~0,-1%

REM Display the parameter values, prompt to continue.
echo Run analysis with the following values:
if [%SonarEnvironment%]==[%SonarProd%] (
    echo Analyzing in SonarQube PRODUCTION - %SonarEnvironment%
	set sonarToken=1b0a9b3de341be9542f968f1be2455a5ecb46763
) else (
    echo Analyzing in SonarQube Development - %SonarEnvironment%
	set sonarToken=a4ea5a03e1ddea92d4ec1755fc10113b5e450f8c
)
echo Project: %sonarName%
echo     Key: %sonarKey%
echo Version: %sonarVersion%
set userresponse=Y
set /p userresponse="Hit <Enter> to continue, X to change or N to cancel. " 
if /I %userresponse%==N goto Canceled
if /I %userresponse%==X goto ChangeParameters
goto Analyze
REM *********** Change Parameters *************
:ChangeParameters
set userresponse=
set /p userresponse="Enter a unique name for the project or hit <Enter> to accept %sonarName%: "
if [%userresponse%] NEQ [] (
    set sonarName=%userresponse%
    set userresponse=
)
set /p userresponse="Enter a unique key for the project or hit <Enter> to accept %sonarKey%: "
if [%userresponse%] NEQ [] (
    set sonarKey=%userresponse%
    set userresponse=
)
set /p userresponse="Enter a version number for the analysis or hit <Enter> to accept %sonarVersion%: "
if [%userresponse%] NEQ [] (
    set sonarVersion=%userresponse%
)
goto ConfirmParameters
REM *********** Execute analysis **************
:Analyze
echo.
echo Step 1: Building rule sets
MSBuild.sonarQube.runner.exe begin /k:%sonarKey% /n:%sonarName% /v:%sonarVersion% /d:sonar.host.url=%SonarEnvironment% /d:sonar.login=%sonarToken%  >Step_1.txt
if errorlevel 1 goto ErrorHandler
echo Step 2: Cleaning
%MsBuildPath%MSBuild.exe /T:Clean /m >Step_2.txt
if errorlevel 1 goto ErrorHandler
echo Step 3: Building the solution
%MsBuildPath%MSBuild.exe /T:Build >Step_3.txt
if errorlevel 1 goto ErrorHandler
echo Step 4: Analyzing
MSBuild.sonarQube.runner.exe end /d:sonar.login=%sonarToken% >Step_4.txt
if errorlevel 1 goto ErrorHandler


REM *********** Save settings *****************
REM Increment the version and overwrite the old parameter file.
:UpdateParameters
echo Updating the parameters file.
set /a nextVersion2=sonarVersion+1
echo Next Version: %nextVersion2%	
REM Overwrite
echo sonarName=%sonarName%> sonarParameters.txt
echo sonarKey=%sonarKey%>> sonarParameters.txt       
REM NOTE: The space between "%nextVersion%" and ">>" is required.
echo sonarVersion=%nextVersion2% >> sonarParameters.txt
goto finished
:ErrorHandler
echo Failure Reason Given is %errorlevel%
goto Exit
:Canceled
echo Analysis canceled.	
:finished
echo Sonar-Runner Finished
echo.
:Exit
set /p userresponse2="Hit <Enter> to exit. " 


