@echo off
set /p environment= Please enter the environment: (dev, qa, prod, uat):
set /p migrationVersion= Please enter the version number (don't include 'V'):
set /p schemaUserName= Please enter the schema user name:
set /p schemaPassword= Please enter the schema password:
set /p schemaService= Please enter the schema service name:
set locationPath=%~dp0sql\alps\Rel-Mgmt\%environment% 
cd tools/flyway-4.2.0
echo %locationPath%
ECHO %~dp0sql\alps\Rel-Mgmt\%environment%
flyway -user=%schemaUserName% -password=%schemaPassword% -locations=filesystem:%locationPath% -target=%migrationVersion% migrate -url=jdbc:oracle:thin:@ldap://ldap.usup.ohlogistics.com/%schemaService%,cn=OracleContext,dc=ohlogistics,dc=com