CREATE OR REPLACE PROCEDURE WVR_COMTSPLTLINERESET_U_P
(
i_orderid IN NUMBER,
i_shipid IN NUMBER,
o_rowsaffected OUT NUMBER
) AS 
BEGIN  

update commitments
   set ptlline = null
 where ORDERID = i_orderid
    AND SHIPID = i_shipid;

o_rowsaffected := SQL%ROWCOUNT;

END WVR_COMTSPLTLINERESET_U_P;
  