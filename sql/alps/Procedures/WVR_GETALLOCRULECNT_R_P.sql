CREATE OR REPLACE PROCEDURE WVR_GETALLOCRULECNT_R_P( 
	 i_facility IN varchar2
	,i_custid IN varchar2
	,i_item IN varchar2
	,i_allocneed IN varchar2
	,o_bcount OUT NUMBER
 ) AS
    lv_allocrule varchar2(100) := ''; 
	l_baseuom custitem.baseuom%type;	
BEGIN   
	
	select nvl(baseuom,'?')
  into l_baseuom
  from custitem
 where custid = i_custid
   and item = i_item ;
   
   select 
      decode(altallocrule,null,allocrule,altallocrule) allocrule into lv_allocrule
    from ALPS.custitemfacilityview
    where custid = i_custid
    and   item = i_item
    and   facility = i_facility;
	
	select count(*) INTO o_bcount
    from allocrulesdtl
    where allocrule = lv_allocrule
    and   facility = i_facility
    and   uom = l_baseuom
    and allocneed = i_allocneed;
    
              
EXCEPTION WHEN no_data_found THEN    
        o_bcount := 0;


END WVR_GETALLOCRULECNT_R_P;