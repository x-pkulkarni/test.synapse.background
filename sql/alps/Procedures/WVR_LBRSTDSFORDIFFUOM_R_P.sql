create or replace PROCEDURE WVR_LBRSTDSFORDIFFUOM_R_P ( 
  i_facility IN VARCHAR2,
  i_custid IN VARCHAR2,
  i_category IN VARCHAR2,
  i_zoneid IN VARCHAR2,
  c_laborstd OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_laborstd FOR
      select uom,
         qtyperhour
    	from laborstandards
   	where facility = i_facility
     		and custid = i_custid
     		and category = i_category
     		and ( (nvl(zoneid,'x') = nvl(i_zoneid,'x')) or
           		( zoneid is null) )
   	order by zoneid,uom;

END WVR_LBRSTDSFORDIFFUOM_R_P;