create or replace PROCEDURE WVR_SETUPCONVEYOR_R_P (
 i_orderid IN NUMBER,
  i_shipid IN NUMBER) AS
  out_msg varchar2(255);
BEGIN
  zcnv.setup_conveyor(i_orderid, i_shipid, out_msg);
END WVR_SETUPCONVEYOR_R_P;