create or replace PROCEDURE WVR_PENDINGBATCHITEM_R_P ( 
  i_orderid IN NUMBER, 
  i_orderitem IN VARCHAR2,
  i_shipid IN NUMBER,
  i_orderlot IN VARCHAR2,
  i_item IN VARCHAR2,
  i_invstatus IN VARCHAR2,
  i_inventoryclass IN VARCHAR2,
  o_quantity OUT NUMBER
) AS
BEGIN

SELECT NVL(SUM(qty),0) INTO o_quantity
    FROM batchtasks
	WHERE orderid = i_orderid
     AND shipid = i_shipid
     AND orderitem = i_orderitem
     AND NVL(orderlot,'(none)') = NVL(i_orderlot,'(none)')
     AND item = i_item
     AND invstatus = i_invstatus
     AND inventoryclass = i_inventoryclass;
EXCEPTION WHEN OTHERS THEN
  	o_quantity := 0;


END WVR_PENDINGBATCHITEM_R_P;