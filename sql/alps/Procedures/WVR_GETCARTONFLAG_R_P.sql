CREATE OR REPLACE PROCEDURE WVR_GETCARTONFLAG_R_P( 
  i_custid IN VARCHAR2,
  o_cartonenabled OUT VARCHAR2
 ) AS
 
BEGIN
    
      select nvl(carton_standard_yn ,'N')
        into o_cartonenabled
        from customer_aux 
      where custid = i_custid;
      

EXCEPTION WHEN OTHERS THEN
  		o_cartonenabled := 'N';

END WVR_GETCARTONFLAG_R_P;
