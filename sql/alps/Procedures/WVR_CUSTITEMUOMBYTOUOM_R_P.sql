CREATE OR REPLACE PROCEDURE WVR_CUSTITEMUOMBYTOUOM_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_touom IN VARCHAR2,
  o_sequence OUT NUMBER
) AS
BEGIN

    select sequence into o_sequence 
         from custitemuom
         where custid = i_custid
           and item = i_item
           and touom = i_touom;
	EXCEPTION WHEN OTHERS THEN
  		o_sequence := 0;


END WVR_CUSTITEMUOMBYTOUOM_R_P;