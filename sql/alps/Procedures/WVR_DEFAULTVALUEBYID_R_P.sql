CREATE OR REPLACE PROCEDURE WVR_DEFAULTVALUEBYID_R_P( 
  i_defaultid IN VARCHAR2,
  o_defaultvalue OUT VARCHAR2
) AS
BEGIN
       select defaultvalue into o_defaultvalue
     	   from systemdefaults
   	   where defaultid = i_defaultid;
	EXCEPTION WHEN OTHERS THEN
  		o_defaultvalue:='';


END WVR_DEFAULTVALUEBYID_R_P;