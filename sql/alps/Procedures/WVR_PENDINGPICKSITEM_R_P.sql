create or replace PROCEDURE WVR_PENDINGPICKSITEM_R_P ( 
  i_orderid IN NUMBER, 
  i_orderitem IN VARCHAR2,
  i_shipid IN NUMBER,
  i_orderlot IN VARCHAR2,
  i_item IN VARCHAR2,
  i_invstatus IN VARCHAR2,
  i_inventoryclass IN VARCHAR2,
  o_quantity OUT NUMBER
) AS
BEGIN

 SELECT NVL(SUM(quantity),0) INTO o_quantity
    FROM shippingplate
    WHERE orderid = i_orderid
     AND shipid = i_shipid
     AND orderitem = i_orderitem
     AND nvl(orderlot,'(none)') = NVL(i_orderlot,'(none)')
     AND item = i_item
     AND invstatus = i_invstatus
     AND inventoryclass = i_inventoryclass
     AND type in ('F','P')
     AND status = 'U';

END WVR_PENDINGPICKSITEM_R_P;