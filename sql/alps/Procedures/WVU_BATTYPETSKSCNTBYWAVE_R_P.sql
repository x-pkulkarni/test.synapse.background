CREATE OR REPLACE PROCEDURE WVU_BATTYPETSKSCNTBYWAVE_R_P ( 
  i_wave IN NUMBER, 
  o_rowscount OUT NUMBER
) AS
BEGIN

    SELECT count(1)
      INTO o_rowscount
      FROM tasks
    WHERE wave = i_wave
       AND tasktype in ('BP','SO');
EXCEPTION WHEN OTHERS THEN
    o_rowscount := 0;
END WVU_BATTYPETSKSCNTBYWAVE_R_P;