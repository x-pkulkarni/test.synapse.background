CREATE OR REPLACE PROCEDURE WVR_ODRHAZMATFLAG_R_P ( 
  i_orderid IN NUMBER, 
  i_shipid IN NUMBER,
  o_flag OUT VARCHAR2
) AS
BEGIN

  SELECT hazmatind into o_flag 
  	FROM orderhdr
  	WHERE orderid = i_orderid
  	  AND shipid = i_shipid;

END WVR_ODRHAZMATFLAG_R_P;