CREATE OR REPLACE PROCEDURE WVU_ORDRCNTNOTRLSDORCNCLD_R_P ( 
  i_wave IN NUMBER, 
  o_rowscount OUT NUMBER
) AS
BEGIN

     SELECT COUNT(1)
      INTO o_rowscount
      FROM orderhdr
    WHERE wave = i_wave
        AND orderstatus NOT IN ('9', 'X');
EXCEPTION WHEN OTHERS THEN
    o_rowscount := 1;
END WVU_ORDRCNTNOTRLSDORCNCLD_R_P;