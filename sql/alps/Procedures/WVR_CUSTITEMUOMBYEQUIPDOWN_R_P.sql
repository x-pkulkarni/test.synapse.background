CREATE OR REPLACE PROCEDURE WVR_CUSTITEMUOMBYEQUIPDOWN_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_fromseq IN NUMBER,
  i_toseq IN NUMBER,
  c_qty OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_qty FOR
      select qty 
    	from custitemuom
   	where custid = i_custid
     		and item = i_item
     		and sequence <= i_fromseq
     		and sequence >= i_toseq
   	order by sequence;

END WVR_CUSTITEMUOMBYEQUIPDOWN_R_P;