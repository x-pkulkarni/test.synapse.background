CREATE OR REPLACE PROCEDURE WVR_GETPUTWALLCOUNT_R_P( 
  i_custid IN VARCHAR2,
  i_facility IN VARCHAR2,
  o_pwcnt OUT VARCHAR2
 ) AS
 
BEGIN
    
 select count(1)
           into o_pwcnt
           from putwall pw join putwallcubby pc
             on pw.facility = pc.facility
            and pw.putwall_id = pc.putwall_id
            join store_location_xref xref
              on pc.locid = xref.location
             and pc.facility = xref.facility
          where pw.facility = i_facility
            and xref.custid = i_custid;
          
EXCEPTION WHEN OTHERS THEN
  		o_pwcnt := 0;

END WVR_GETPUTWALLCOUNT_R_P;
