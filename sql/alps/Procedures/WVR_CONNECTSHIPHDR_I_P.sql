create or replace PROCEDURE WVR_CONNECTSHIPHDR_I_P
(
    i_custid CONNECTSHIPHDR.CUSTID%TYPE,
    i_opcode CONNECTSHIPHDR.OPCODE%TYPE,
    i_facility CONNECTSHIPHDR.FACILITY%TYPE,
    i_lpid CONNECTSHIPHDR.LPID%TYPE,
    i_orderid CONNECTSHIPHDR.ORDERID%TYPE,
    i_shipid CONNECTSHIPHDR.SHIPID%TYPE,
    i_wave CONNECTSHIPHDR.WAVE%TYPE,
    i_taskid CONNECTSHIPHDR.TASKID%TYPE,
    i_carrier CONNECTSHIPHDR.CARRIER%TYPE,
    i_deliveryservice CONNECTSHIPHDR.DELIVERYSERVICE%TYPE,
    i_sscc CONNECTSHIPHDR.SSCC%TYPE,
    i_reference CONNECTSHIPHDR.REFERENCE%TYPE,
    i_po CONNECTSHIPHDR.PO%TYPE,
    i_connectshipservice CONNECTSHIPHDR.CONNECTSHIPSERVICE%TYPE,
    i_shiptoname CONNECTSHIPHDR.SHIPTONAME%TYPE,
    i_shiptocontact CONNECTSHIPHDR.SHIPTOCONTACT%TYPE,
    i_shiptoaddr1 CONNECTSHIPHDR.SHIPTOADDR1%TYPE,
    i_shiptoaddr2 CONNECTSHIPHDR.SHIPTOADDR2%TYPE,
    i_shiptoaddr3 CONNECTSHIPHDR.SHIPTOADDR3%TYPE,
    i_shiptocity CONNECTSHIPHDR.SHIPTOCITY%TYPE,
    i_shiptostate CONNECTSHIPHDR.SHIPTOSTATE%TYPE,
    i_shiptopostalcode CONNECTSHIPHDR.SHIPTOPOSTALCODE%TYPE,
    i_shiptocountry CONNECTSHIPHDR.SHIPTOCOUNTRY%TYPE,
    i_shiptophone CONNECTSHIPHDR.SHIPTOPHONE%TYPE,
    i_shiptoemail CONNECTSHIPHDR.SHIPTOEMAIL%TYPE,
    i_residentialflag CONNECTSHIPHDR.RESIDENTIALFLAG%TYPE,
    i_saturdaydeliveryflag CONNECTSHIPHDR.SATURDAYDELIVERYFLAG%TYPE,
    i_signaturerequiredflag CONNECTSHIPHDR.SIGNATUREREQUIREDFLAG%TYPE,
    i_terms CONNECTSHIPHDR.TERMS%TYPE,
    i_seq CONNECTSHIPHDR.SEQ%TYPE,
    i_seqof CONNECTSHIPHDR.SEQOF%TYPE,
    i_billtoname CONNECTSHIPHDR.BILLTONAME%TYPE,
    i_billtocontact CONNECTSHIPHDR.BILLTOCONTACT%TYPE,
    i_billtoaddr1 CONNECTSHIPHDR.BILLTOADDR1%TYPE,
    i_billtoaddr2 CONNECTSHIPHDR.BILLTOADDR2%TYPE,
    i_billtoaddr3 CONNECTSHIPHDR.BILLTOADDR3%TYPE,
    i_billtocity CONNECTSHIPHDR.BILLTOCITY%TYPE,
    i_billtostate CONNECTSHIPHDR.BILLTOSTATE%TYPE,
    i_billtopostalcode CONNECTSHIPHDR.BILLTOPOSTALCODE%TYPE,
    i_billtocountry CONNECTSHIPHDR.BILLTOCOUNTRY%TYPE,
    i_billtophone CONNECTSHIPHDR.BILLTOPHONE%TYPE,
    i_billtoemail CONNECTSHIPHDR.BILLTOEMAIL%TYPE,
    i_thirdpartynumber CONNECTSHIPHDR.THIRDPARTYNUMBER%TYPE,
    i_shippernumber CONNECTSHIPHDR.SHIPPERNUMBER%TYPE,
    i_accountnumber CONNECTSHIPHDR.ACCOUNTNUMBER%TYPE,
    i_containertype CONNECTSHIPHDR.CONTAINERTYPE%TYPE,
    i_length CONNECTSHIPHDR.LENGTH%TYPE,
    i_width CONNECTSHIPHDR.WIDTH%TYPE,
    i_height CONNECTSHIPHDR.HEIGHT%TYPE,
    i_weight CONNECTSHIPHDR.WEIGHT%TYPE,
    i_shipdate CONNECTSHIPHDR.SHIPDATE%TYPE,
    i_packaging CONNECTSHIPHDR.PACKAGING%TYPE,
    i_codamount CONNECTSHIPHDR.CODAMOUNT%TYPE,
    i_codmethod CONNECTSHIPHDR.CODMETHOD%TYPE,
    i_declaredvalue CONNECTSHIPHDR.DECLAREDVALUE%TYPE,
    i_description CONNECTSHIPHDR.DESCRIPTION%TYPE,
    i_sedmethod CONNECTSHIPHDR.SEDMETHOD%TYPE,
    i_ultimatecountry CONNECTSHIPHDR.ULTIMATECOUNTRY%TYPE,
    i_carrierinstructions CONNECTSHIPHDR.CARRIERINSTRUCTIONS%TYPE,
    i_ci_method CONNECTSHIPHDR.CI_METHOD%TYPE,
    i_printer CONNECTSHIPHDR.PRINTER%TYPE,
    i_trackingnumber CONNECTSHIPHDR.TRACKINGNUMBER%TYPE,
    i_cost CONNECTSHIPHDR.COST%TYPE,
    i_labelimagename CONNECTSHIPHDR.LABELIMAGENAME%TYPE,
    i_errormessage CONNECTSHIPHDR.ERRORMESSAGE%TYPE,
    i_msn_id CONNECTSHIPHDR.MSN_ID%TYPE,
    i_creationdate CONNECTSHIPHDR.CREATIONDATE%TYPE,
    i_canceldate CONNECTSHIPHDR.CANCELDATE%TYPE,
    i_cancelledby CONNECTSHIPHDR.CANCELLEDBY%TYPE,
    i_bypassdate CONNECTSHIPHDR.BYPASSDATE%TYPE,
    i_bypassedby CONNECTSHIPHDR.BYPASSEDBY%TYPE,
    i_receivedate CONNECTSHIPHDR.RECEIVEDATE%TYPE,
    i_status CONNECTSHIPHDR.STATUS%TYPE,
    i_item CONNECTSHIPHDR.ITEM%TYPE,
    i_used CONNECTSHIPHDR.USED%TYPE,
    i_hdrpassthruchar01 CONNECTSHIPHDR.HDRPASSTHRUCHAR01%TYPE,
    i_hdrpassthruchar02 CONNECTSHIPHDR.HDRPASSTHRUCHAR02%TYPE,
    i_hdrpassthruchar03 CONNECTSHIPHDR.HDRPASSTHRUCHAR03%TYPE,
    i_hdrpassthruchar04 CONNECTSHIPHDR.HDRPASSTHRUCHAR04%TYPE,
    i_hdrpassthruchar05 CONNECTSHIPHDR.HDRPASSTHRUCHAR05%TYPE,
    i_hdrpassthruchar06 CONNECTSHIPHDR.HDRPASSTHRUCHAR06%TYPE,
    i_hdrpassthruchar07 CONNECTSHIPHDR.HDRPASSTHRUCHAR07%TYPE,
    i_hdrpassthruchar08 CONNECTSHIPHDR.HDRPASSTHRUCHAR08%TYPE,
    i_hdrpassthruchar09 CONNECTSHIPHDR.HDRPASSTHRUCHAR09%TYPE,
    i_hdrpassthruchar10 CONNECTSHIPHDR.HDRPASSTHRUCHAR10%TYPE,
    i_hdrpassthruchar11 CONNECTSHIPHDR.HDRPASSTHRUCHAR11%TYPE,
    i_hdrpassthruchar12 CONNECTSHIPHDR.HDRPASSTHRUCHAR12%TYPE,
    i_hdrpassthruchar13 CONNECTSHIPHDR.HDRPASSTHRUCHAR13%TYPE,
    i_hdrpassthruchar14 CONNECTSHIPHDR.HDRPASSTHRUCHAR14%TYPE,
    i_hdrpassthruchar15 CONNECTSHIPHDR.HDRPASSTHRUCHAR15%TYPE,
    i_hdrpassthruchar16 CONNECTSHIPHDR.HDRPASSTHRUCHAR16%TYPE,
    i_hdrpassthruchar17 CONNECTSHIPHDR.HDRPASSTHRUCHAR17%TYPE,
    i_hdrpassthruchar18 CONNECTSHIPHDR.HDRPASSTHRUCHAR18%TYPE,
    i_hdrpassthruchar19 CONNECTSHIPHDR.HDRPASSTHRUCHAR19%TYPE,
    i_hdrpassthruchar20 CONNECTSHIPHDR.HDRPASSTHRUCHAR20%TYPE,
    i_hdrpassthrunum01 CONNECTSHIPHDR.HDRPASSTHRUNUM01%TYPE,
    i_hdrpassthrunum02 CONNECTSHIPHDR.HDRPASSTHRUNUM02%TYPE,
    i_hdrpassthrunum03 CONNECTSHIPHDR.HDRPASSTHRUNUM03%TYPE,
    i_hdrpassthrunum04 CONNECTSHIPHDR.HDRPASSTHRUNUM04%TYPE,
    i_hdrpassthrunum05 CONNECTSHIPHDR.HDRPASSTHRUNUM05%TYPE,
    i_hdrpassthrunum06 CONNECTSHIPHDR.HDRPASSTHRUNUM06%TYPE,
    i_hdrpassthrunum07 CONNECTSHIPHDR.HDRPASSTHRUNUM07%TYPE,
    i_hdrpassthrunum08 CONNECTSHIPHDR.HDRPASSTHRUNUM08%TYPE,
    i_hdrpassthrunum09 CONNECTSHIPHDR.HDRPASSTHRUNUM09%TYPE,
    i_hdrpassthrunum10 CONNECTSHIPHDR.HDRPASSTHRUNUM10%TYPE,
    i_hdrpassthrudate01 CONNECTSHIPHDR.HDRPASSTHRUDATE01%TYPE,
    i_hdrpassthrudate02 CONNECTSHIPHDR.HDRPASSTHRUDATE02%TYPE,
    i_hdrpassthrudate03 CONNECTSHIPHDR.HDRPASSTHRUDATE03%TYPE,
    i_hdrpassthrudate04 CONNECTSHIPHDR.HDRPASSTHRUDATE04%TYPE,
    i_hdrpassthrudoll01 CONNECTSHIPHDR.HDRPASSTHRUDOLL01%TYPE,
    i_hdrpassthrudoll02 CONNECTSHIPHDR.HDRPASSTHRUDOLL02%TYPE,
    i_gc3hdrpassthruchar01 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR01%TYPE,
    i_gc3hdrpassthruchar02 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR02%TYPE,
    i_gc3hdrpassthruchar03 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR03%TYPE,
    i_gc3hdrpassthruchar04 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR04%TYPE,
    i_gc3hdrpassthruchar05 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR05%TYPE,
    i_gc3hdrpassthruchar06 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR06%TYPE,
    i_gc3hdrpassthruchar07 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR07%TYPE,
    i_gc3hdrpassthruchar08 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR08%TYPE,
    i_gc3hdrpassthruchar09 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR09%TYPE,
    i_gc3hdrpassthruchar10 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR10%TYPE,
    i_gc3hdrpassthruchar11 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR11%TYPE,
    i_gc3hdrpassthruchar12 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR12%TYPE,
    i_gc3hdrpassthruchar13 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR13%TYPE,
    i_gc3hdrpassthruchar14 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR14%TYPE,
    i_gc3hdrpassthruchar15 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR15%TYPE,
    i_gc3hdrpassthruchar16 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR16%TYPE,
    i_gc3hdrpassthruchar17 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR17%TYPE,
    i_gc3hdrpassthruchar18 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR18%TYPE,
    i_gc3hdrpassthruchar19 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR19%TYPE,
    i_gc3hdrpassthruchar20 CONNECTSHIPHDR.GC3HDRPASSTHRUCHAR20%TYPE,
    i_gc3hdrpassthrunum01 CONNECTSHIPHDR.GC3HDRPASSTHRUNUM01%TYPE,
    i_gc3hdrpassthrunum02 CONNECTSHIPHDR.GC3HDRPASSTHRUNUM02%TYPE,
    i_gc3hdrpassthrunum03 CONNECTSHIPHDR.GC3HDRPASSTHRUNUM03%TYPE,
    i_gc3hdrpassthrunum04 CONNECTSHIPHDR.GC3HDRPASSTHRUNUM04%TYPE,
    i_gc3hdrpassthrunum05 CONNECTSHIPHDR.GC3HDRPASSTHRUNUM05%TYPE,
    i_gc3hdrpassthrunum06 CONNECTSHIPHDR.GC3HDRPASSTHRUNUM06%TYPE,
    i_gc3hdrpassthrunum07 CONNECTSHIPHDR.GC3HDRPASSTHRUNUM07%TYPE,
    i_gc3hdrpassthrunum08 CONNECTSHIPHDR.GC3HDRPASSTHRUNUM08%TYPE,
    i_gc3hdrpassthrunum09 CONNECTSHIPHDR.GC3HDRPASSTHRUNUM09%TYPE,
    i_gc3hdrpassthrunum10 CONNECTSHIPHDR.GC3HDRPASSTHRUNUM10%TYPE,
    i_gc3hdrpassthrudate01 CONNECTSHIPHDR.GC3HDRPASSTHRUDATE01%TYPE,
    i_gc3hdrpassthrudate02 CONNECTSHIPHDR.GC3HDRPASSTHRUDATE02%TYPE,
    i_gc3hdrpassthrudate03 CONNECTSHIPHDR.GC3HDRPASSTHRUDATE03%TYPE,
    i_gc3hdrpassthrudate04 CONNECTSHIPHDR.GC3HDRPASSTHRUDATE04%TYPE,
    i_gc3hdrpassthrudoll01 CONNECTSHIPHDR.GC3HDRPASSTHRUDOLL01%TYPE,
    i_gc3hdrpassthrudoll02 CONNECTSHIPHDR.GC3HDRPASSTHRUDOLL02%TYPE,
    i_asnhdrpassthruchar01 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR01%TYPE,
    i_asnhdrpassthruchar02 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR02%TYPE,
    i_asnhdrpassthruchar03 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR03%TYPE,
    i_asnhdrpassthruchar04 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR04%TYPE,
    i_asnhdrpassthruchar05 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR05%TYPE,
    i_asnhdrpassthruchar06 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR06%TYPE,
    i_asnhdrpassthruchar07 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR07%TYPE,
    i_asnhdrpassthruchar08 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR08%TYPE,
    i_asnhdrpassthruchar09 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR09%TYPE,
    i_asnhdrpassthruchar10 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR10%TYPE,
    i_asnhdrpassthruchar11 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR11%TYPE,
    i_asnhdrpassthruchar12 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR12%TYPE,
    i_asnhdrpassthruchar13 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR13%TYPE,
    i_asnhdrpassthruchar14 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR14%TYPE,
    i_asnhdrpassthruchar15 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR15%TYPE,
    i_asnhdrpassthruchar16 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR16%TYPE,
    i_asnhdrpassthruchar17 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR17%TYPE,
    i_asnhdrpassthruchar18 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR18%TYPE,
    i_asnhdrpassthruchar19 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR19%TYPE,
    i_asnhdrpassthruchar20 CONNECTSHIPHDR.ASNHDRPASSTHRUCHAR20%TYPE,
    i_asnhdrpassthrunum01 CONNECTSHIPHDR.ASNHDRPASSTHRUNUM01%TYPE,
    i_asnhdrpassthrunum02 CONNECTSHIPHDR.ASNHDRPASSTHRUNUM02%TYPE,
    i_asnhdrpassthrunum03 CONNECTSHIPHDR.ASNHDRPASSTHRUNUM03%TYPE,
    i_asnhdrpassthrunum04 CONNECTSHIPHDR.ASNHDRPASSTHRUNUM04%TYPE,
    i_asnhdrpassthrunum05 CONNECTSHIPHDR.ASNHDRPASSTHRUNUM05%TYPE,
    i_asnhdrpassthrunum06 CONNECTSHIPHDR.ASNHDRPASSTHRUNUM06%TYPE,
    i_asnhdrpassthrunum07 CONNECTSHIPHDR.ASNHDRPASSTHRUNUM07%TYPE,
    i_asnhdrpassthrunum08 CONNECTSHIPHDR.ASNHDRPASSTHRUNUM08%TYPE,
    i_asnhdrpassthrunum09 CONNECTSHIPHDR.ASNHDRPASSTHRUNUM09%TYPE,
    i_asnhdrpassthrunum10 CONNECTSHIPHDR.ASNHDRPASSTHRUNUM10%TYPE,
    i_asnhdrpassthrudate01 CONNECTSHIPHDR.ASNHDRPASSTHRUDATE01%TYPE,
    i_asnhdrpassthrudate02 CONNECTSHIPHDR.ASNHDRPASSTHRUDATE02%TYPE,
    i_asnhdrpassthrudate03 CONNECTSHIPHDR.ASNHDRPASSTHRUDATE03%TYPE,
    i_asnhdrpassthrudate04 CONNECTSHIPHDR.ASNHDRPASSTHRUDATE04%TYPE,
    i_asnhdrpassthrudoll01 CONNECTSHIPHDR.ASNHDRPASSTHRUDOLL01%TYPE,
    i_asnhdrpassthrudoll02 CONNECTSHIPHDR.ASNHDRPASSTHRUDOLL02%TYPE,
    i_subtask_seq CONNECTSHIPHDR.SUBTASK_SEQ%TYPE,
    i_batch CONNECTSHIPHDR.BATCH%TYPE,
    i_location CONNECTSHIPHDR.LOCATION%TYPE,
    i_route CONNECTSHIPHDR.ROUTE%TYPE,
    i_replacement_order_yn CONNECTSHIPHDR.REPLACEMENT_ORDER_YN%TYPE,
    i_returntoname CONNECTSHIPHDR.RETURNTONAME%TYPE,
    i_returntocontact CONNECTSHIPHDR.RETURNTOCONTACT%TYPE,
    i_returntoaddr1 CONNECTSHIPHDR.RETURNTOADDR1%TYPE,
    i_returntoaddr2 CONNECTSHIPHDR.RETURNTOADDR2%TYPE,
    i_returntoaddr3 CONNECTSHIPHDR.RETURNTOADDR3%TYPE,
    i_returntocity CONNECTSHIPHDR.RETURNTOCITY%TYPE,
    i_returntostate CONNECTSHIPHDR.RETURNTOSTATE%TYPE,
    i_returntopostalcode CONNECTSHIPHDR.RETURNTOPOSTALCODE%TYPE,
    i_returntocountrycode CONNECTSHIPHDR.RETURNTOCOUNTRYCODE%TYPE,
    i_returntophone CONNECTSHIPHDR.RETURNTOPHONE%TYPE,
    i_returntofax CONNECTSHIPHDR.RETURNTOFAX%TYPE,
    i_returntoemail CONNECTSHIPHDR.RETURNTOEMAIL%TYPE,
    i_returndcname CONNECTSHIPHDR.RETURNDCNAME%TYPE,
    i_returndccontact CONNECTSHIPHDR.RETURNDCCONTACT%TYPE,
    i_returndcaddr1 CONNECTSHIPHDR.RETURNDCADDR1%TYPE,
    i_returndcaddr2 CONNECTSHIPHDR.RETURNDCADDR2%TYPE,
    i_returndcaddr3 CONNECTSHIPHDR.RETURNDCADDR3%TYPE,
    i_returndccity CONNECTSHIPHDR.RETURNDCCITY%TYPE,
    i_returndcstate CONNECTSHIPHDR.RETURNDCSTATE%TYPE,
    i_returndcpostalcode CONNECTSHIPHDR.RETURNDCPOSTALCODE%TYPE,
    i_returndccountrycode CONNECTSHIPHDR.RETURNDCCOUNTRYCODE%TYPE,
    i_returndcphone CONNECTSHIPHDR.RETURNDCPHONE%TYPE,
    i_returndcfax CONNECTSHIPHDR.RETURNDCFAX%TYPE,
    i_returndcemail CONNECTSHIPHDR.RETURNDCEMAIL%TYPE,
    i_labelimagename2 CONNECTSHIPHDR.LABELIMAGENAME2%TYPE,
    i_return_trackingnumber CONNECTSHIPHDR.RETURN_TRACKINGNUMBER%TYPE,
    i_dtlpassthruchar02 CONNECTSHIPHDR.DTLPASSTHRUCHAR02%TYPE,
    i_dtlpassthruchar03 CONNECTSHIPHDR.DTLPASSTHRUCHAR03%TYPE,
    i_dtlpassthruchar05 CONNECTSHIPHDR.DTLPASSTHRUCHAR05%TYPE,
    i_dtlpassthruchar11 CONNECTSHIPHDR.DTLPASSTHRUCHAR11%TYPE,
    i_dtlpassthruchar13 CONNECTSHIPHDR.DTLPASSTHRUCHAR13%TYPE,
    i_dtlpassthruchar16 CONNECTSHIPHDR.DTLPASSTHRUCHAR16%TYPE,
    i_dtlpassthruchar17 CONNECTSHIPHDR.DTLPASSTHRUCHAR17%TYPE,
    i_dtlpassthruchar18 CONNECTSHIPHDR.DTLPASSTHRUCHAR18%TYPE,
    i_dtlpassthruchar19 CONNECTSHIPHDR.DTLPASSTHRUCHAR19%TYPE,
    i_dtlpassthrunum04 CONNECTSHIPHDR.DTLPASSTHRUNUM04%TYPE,
    i_dtlpassthrudate01 CONNECTSHIPHDR.DTLPASSTHRUDATE01%TYPE,
    i_ediauditchar16 CONNECTSHIPHDR.EDIAUDITCHAR16%TYPE,
    i_ediauditchar17 CONNECTSHIPHDR.EDIAUDITCHAR17%TYPE,
    i_itemdescr CONNECTSHIPHDR.ITEMDESCR%TYPE,
    i_bolcomment CONNECTSHIPHDR.BOLCOMMENT%TYPE,
    i_msn_id_rtn CONNECTSHIPHDR.MSN_ID_RTN%TYPE,
    i_dtlpassthrunum01 CONNECTSHIPHDR.DTLPASSTHRUNUM01%TYPE,
    i_dtlpassthrunum02 CONNECTSHIPHDR.DTLPASSTHRUNUM02%TYPE,
    i_dtlpassthrunum03 CONNECTSHIPHDR.DTLPASSTHRUNUM03%TYPE,
    i_dtlpassthrunum05 CONNECTSHIPHDR.DTLPASSTHRUNUM05%TYPE,
    i_dtlpassthrunum06 CONNECTSHIPHDR.DTLPASSTHRUNUM06%TYPE,
    i_dtlpassthrunum07 CONNECTSHIPHDR.DTLPASSTHRUNUM07%TYPE,
    i_dtlpassthrunum08 CONNECTSHIPHDR.DTLPASSTHRUNUM08%TYPE,
    i_dtlpassthrunum09 CONNECTSHIPHDR.DTLPASSTHRUNUM09%TYPE,
    i_dtlpassthrunum10 CONNECTSHIPHDR.DTLPASSTHRUNUM10%TYPE,
    i_dtlpassthruchar01 CONNECTSHIPHDR.DTLPASSTHRUCHAR01%TYPE,
    i_dtlpassthruchar04 CONNECTSHIPHDR.DTLPASSTHRUCHAR04%TYPE,
    i_dtlpassthruchar06 CONNECTSHIPHDR.DTLPASSTHRUCHAR06%TYPE,
    i_dtlpassthruchar07 CONNECTSHIPHDR.DTLPASSTHRUCHAR07%TYPE,
    i_dtlpassthruchar08 CONNECTSHIPHDR.DTLPASSTHRUCHAR08%TYPE,
    i_dtlpassthruchar09 CONNECTSHIPHDR.DTLPASSTHRUCHAR09%TYPE,
    i_dtlpassthruchar10 CONNECTSHIPHDR.DTLPASSTHRUCHAR10%TYPE,
    i_dtlpassthruchar12 CONNECTSHIPHDR.DTLPASSTHRUCHAR12%TYPE,
    i_dtlpassthruchar14 CONNECTSHIPHDR.DTLPASSTHRUCHAR14%TYPE,
    i_dtlpassthruchar15 CONNECTSHIPHDR.DTLPASSTHRUCHAR15%TYPE,
    i_dtlpassthruchar20 CONNECTSHIPHDR.DTLPASSTHRUCHAR20%TYPE,
    i_dtlpassthrudate02 CONNECTSHIPHDR.DTLPASSTHRUDATE02%TYPE,
    i_dtlpassthrudate03 CONNECTSHIPHDR.DTLPASSTHRUDATE03%TYPE,
    i_dtlpassthrudate04 CONNECTSHIPHDR.DTLPASSTHRUDATE04%TYPE,
    i_dtlpassthrudoll01 CONNECTSHIPHDR.DTLPASSTHRUDOLL01%TYPE,
    i_dtlpassthrudoll02 CONNECTSHIPHDR.DTLPASSTHRUDOLL02%TYPE,
    i_baseuomunitweight CONNECTSHIPHDR.BASEUOMUNITWEIGHT%TYPE,
    i_trackingnumber2 CONNECTSHIPHDR.TRACKINGNUMBER2%TYPE,
    i_trackingnumber3 CONNECTSHIPHDR.TRACKINGNUMBER3%TYPE,
    i_return_trackingnumber2 CONNECTSHIPHDR.RETURN_TRACKINGNUMBER2%TYPE,
    i_return_trackingnumber3 CONNECTSHIPHDR.RETURN_TRACKINGNUMBER3%TYPE,
    i_zone CONNECTSHIPHDR.ZONE%TYPE,
    i_aisle CONNECTSHIPHDR.AISLE%TYPE,
    i_printgroupid CONNECTSHIPHDR.PRINTGROUPID%TYPE,
    i_battery_count CONNECTSHIPHDR.BATTERY_COUNT%TYPE,
    i_cell_count CONNECTSHIPHDR.CELL_COUNT%TYPE,
    i_lithium_weight CONNECTSHIPHDR.LITHIUM_WEIGHT%TYPE,
    i_battery_watthrs CONNECTSHIPHDR.BATTERY_WATTHRS%TYPE,
    i_iataprimarychemcode CONNECTSHIPHDR.IATAPRIMARYCHEMCODE%TYPE,
    i_primaryhazardclass CONNECTSHIPHDR.PRIMARYHAZARDCLASS%TYPE,
    i_hazmat CONNECTSHIPHDR.HAZMAT%TYPE,
    i_ediauditchar01 CONNECTSHIPHDR.EDIAUDITCHAR01%TYPE,
    i_ediauditchar02 CONNECTSHIPHDR.EDIAUDITCHAR02%TYPE,
    i_ediauditchar03 CONNECTSHIPHDR.EDIAUDITCHAR03%TYPE,
    i_ediauditchar04 CONNECTSHIPHDR.EDIAUDITCHAR04%TYPE,
    i_ediauditchar05 CONNECTSHIPHDR.EDIAUDITCHAR05%TYPE,
    i_ediauditchar06 CONNECTSHIPHDR.EDIAUDITCHAR06%TYPE,
    i_ediauditchar07 CONNECTSHIPHDR.EDIAUDITCHAR07%TYPE,
    i_ediauditchar08 CONNECTSHIPHDR.EDIAUDITCHAR08%TYPE,
    i_ediauditchar09 CONNECTSHIPHDR.EDIAUDITCHAR09%TYPE,
    i_ediauditchar10 CONNECTSHIPHDR.EDIAUDITCHAR10%TYPE,
    i_ediauditchar11 CONNECTSHIPHDR.EDIAUDITCHAR11%TYPE,
    i_ediauditchar12 CONNECTSHIPHDR.EDIAUDITCHAR12%TYPE,
    i_ediauditchar13 CONNECTSHIPHDR.EDIAUDITCHAR13%TYPE,
    i_ediauditchar14 CONNECTSHIPHDR.EDIAUDITCHAR14%TYPE,
    i_ediauditchar15 CONNECTSHIPHDR.EDIAUDITCHAR15%TYPE,
    i_ediauditchar18 CONNECTSHIPHDR.EDIAUDITCHAR18%TYPE,
    i_ediauditchar19 CONNECTSHIPHDR.EDIAUDITCHAR19%TYPE,
    i_ediauditchar20 CONNECTSHIPHDR.EDIAUDITCHAR20%TYPE,
    i_labelimagename3 CONNECTSHIPHDR.LABELIMAGENAME3%TYPE,
    i_labelimagename4 CONNECTSHIPHDR.LABELIMAGENAME4%TYPE,
    i_labelimagename5 CONNECTSHIPHDR.LABELIMAGENAME5%TYPE,
    i_labelimagename6 CONNECTSHIPHDR.LABELIMAGENAME6%TYPE,
    i_transmit_to_packsize_yn CONNECTSHIPHDR.TRANSMIT_TO_PACKSIZE_YN%TYPE,
    i_wcsstatus CONNECTSHIPHDR.WCSSTATUS%TYPE,
    i_wcsprocessdatetime CONNECTSHIPHDR.WCSPROCESSDATETIME%TYPE,
    i_premnfst_attempts CONNECTSHIPHDR.PREMNFST_ATTEMPTS%TYPE,
    i_hazmatind CONNECTSHIPHDR.HAZMATIND%TYPE,
    i_toteid CONNECTSHIPHDR.TOTEID%TYPE,
    i_return_only_yn CONNECTSHIPHDR.RETURN_ONLY_YN%TYPE,
    i_return_only_lbl_printed CONNECTSHIPHDR.RETURN_ONLY_LBL_PRINTED%TYPE,
    i_return_only_cartonid CONNECTSHIPHDR.RETURN_ONLY_CARTONID%TYPE,
    out_success out number
) AS
BEGIN
    INSERT INTO connectshiphdr (
        custid,
        opcode,
        facility,
        lpid,
        orderid,
        shipid,
        wave,
        taskid,
        carrier,
        deliveryservice,
        sscc,
        reference,
        po,
        connectshipservice,
        shiptoname,
        shiptocontact,
        shiptoaddr1,
        shiptoaddr2,
        shiptoaddr3,
        shiptocity,
        shiptostate,
        shiptopostalcode,
        shiptocountry,
        shiptophone,
        shiptoemail,
        residentialflag,
        saturdaydeliveryflag,
        signaturerequiredflag,
        terms,
        seq,
        seqof,
        billtoname,
        billtocontact,
        billtoaddr1,
        billtoaddr2,
        billtoaddr3,
        billtocity,
        billtostate,
        billtopostalcode,
        billtocountry,
        billtophone,
        billtoemail,
        thirdpartynumber,
        shippernumber,
        accountnumber,
        containertype,
        length,
        width,
        height,
        weight,
        shipdate,
        packaging,
        codamount,
        codmethod,
        declaredvalue,
        description,
        sedmethod,
        ultimatecountry,
        carrierinstructions,
        ci_method,
        printer,
        trackingnumber,
        cost,
        labelimagename,
        errormessage,
        msn_id,
        creationdate,
        canceldate,
        cancelledby,
        bypassdate,
        bypassedby,
        receivedate,
        status,
        item,
        used,
        hdrpassthruchar01,
        hdrpassthruchar02,
        hdrpassthruchar03,
        hdrpassthruchar04,
        hdrpassthruchar05,
        hdrpassthruchar06,
        hdrpassthruchar07,
        hdrpassthruchar08,
        hdrpassthruchar09,
        hdrpassthruchar10,
        hdrpassthruchar11,
        hdrpassthruchar12,
        hdrpassthruchar13,
        hdrpassthruchar14,
        hdrpassthruchar15,
        hdrpassthruchar16,
        hdrpassthruchar17,
        hdrpassthruchar18,
        hdrpassthruchar19,
        hdrpassthruchar20,
        hdrpassthrunum01,
        hdrpassthrunum02,
        hdrpassthrunum03,
        hdrpassthrunum04,
        hdrpassthrunum05,
        hdrpassthrunum06,
        hdrpassthrunum07,
        hdrpassthrunum08,
        hdrpassthrunum09,
        hdrpassthrunum10,
        hdrpassthrudate01,
        hdrpassthrudate02,
        hdrpassthrudate03,
        hdrpassthrudate04,
        hdrpassthrudoll01,
        hdrpassthrudoll02,
        gc3hdrpassthruchar01,
        gc3hdrpassthruchar02,
        gc3hdrpassthruchar03,
        gc3hdrpassthruchar04,
        gc3hdrpassthruchar05,
        gc3hdrpassthruchar06,
        gc3hdrpassthruchar07,
        gc3hdrpassthruchar08,
        gc3hdrpassthruchar09,
        gc3hdrpassthruchar10,
        gc3hdrpassthruchar11,
        gc3hdrpassthruchar12,
        gc3hdrpassthruchar13,
        gc3hdrpassthruchar14,
        gc3hdrpassthruchar15,
        gc3hdrpassthruchar16,
        gc3hdrpassthruchar17,
        gc3hdrpassthruchar18,
        gc3hdrpassthruchar19,
        gc3hdrpassthruchar20,
        gc3hdrpassthrunum01,
        gc3hdrpassthrunum02,
        gc3hdrpassthrunum03,
        gc3hdrpassthrunum04,
        gc3hdrpassthrunum05,
        gc3hdrpassthrunum06,
        gc3hdrpassthrunum07,
        gc3hdrpassthrunum08,
        gc3hdrpassthrunum09,
        gc3hdrpassthrunum10,
        gc3hdrpassthrudate01,
        gc3hdrpassthrudate02,
        gc3hdrpassthrudate03,
        gc3hdrpassthrudate04,
        gc3hdrpassthrudoll01,
        gc3hdrpassthrudoll02,
        asnhdrpassthruchar01,
        asnhdrpassthruchar02,
        asnhdrpassthruchar03,
        asnhdrpassthruchar04,
        asnhdrpassthruchar05,
        asnhdrpassthruchar06,
        asnhdrpassthruchar07,
        asnhdrpassthruchar08,
        asnhdrpassthruchar09,
        asnhdrpassthruchar10,
        asnhdrpassthruchar11,
        asnhdrpassthruchar12,
        asnhdrpassthruchar13,
        asnhdrpassthruchar14,
        asnhdrpassthruchar15,
        asnhdrpassthruchar16,
        asnhdrpassthruchar17,
        asnhdrpassthruchar18,
        asnhdrpassthruchar19,
        asnhdrpassthruchar20,
        asnhdrpassthrunum01,
        asnhdrpassthrunum02,
        asnhdrpassthrunum03,
        asnhdrpassthrunum04,
        asnhdrpassthrunum05,
        asnhdrpassthrunum06,
        asnhdrpassthrunum07,
        asnhdrpassthrunum08,
        asnhdrpassthrunum09,
        asnhdrpassthrunum10,
        asnhdrpassthrudate01,
        asnhdrpassthrudate02,
        asnhdrpassthrudate03,
        asnhdrpassthrudate04,
        asnhdrpassthrudoll01,
        asnhdrpassthrudoll02,
        subtask_seq,
        batch,
        location,
        route,
        replacement_order_yn,
        returntoname,
        returntocontact,
        returntoaddr1,
        returntoaddr2,
        returntoaddr3,
        returntocity,
        returntostate,
        returntopostalcode,
        returntocountrycode,
        returntophone,
        returntofax,
        returntoemail,
        returndcname,
        returndccontact,
        returndcaddr1,
        returndcaddr2,
        returndcaddr3,
        returndccity,
        returndcstate,
        returndcpostalcode,
        returndccountrycode,
        returndcphone,
        returndcfax,
        returndcemail,
        labelimagename2,
        return_trackingnumber,
        dtlpassthruchar02,
        dtlpassthruchar03,
        dtlpassthruchar05,
        dtlpassthruchar11,
        dtlpassthruchar13,
        dtlpassthruchar16,
        dtlpassthruchar17,
        dtlpassthruchar18,
        dtlpassthruchar19,
        dtlpassthrunum04,
        dtlpassthrudate01,
        ediauditchar16,
        ediauditchar17,
        itemdescr,
        bolcomment,
        msn_id_rtn,
        dtlpassthrunum01,
        dtlpassthrunum02,
        dtlpassthrunum03,
        dtlpassthrunum05,
        dtlpassthrunum06,
        dtlpassthrunum07,
        dtlpassthrunum08,
        dtlpassthrunum09,
        dtlpassthrunum10,
        dtlpassthruchar01,
        dtlpassthruchar04,
        dtlpassthruchar06,
        dtlpassthruchar07,
        dtlpassthruchar08,
        dtlpassthruchar09,
        dtlpassthruchar10,
        dtlpassthruchar12,
        dtlpassthruchar14,
        dtlpassthruchar15,
        dtlpassthruchar20,
        dtlpassthrudate02,
        dtlpassthrudate03,
        dtlpassthrudate04,
        dtlpassthrudoll01,
        dtlpassthrudoll02,
        baseuomunitweight,
        trackingnumber2,
        trackingnumber3,
        return_trackingnumber2,
        return_trackingnumber3,
        zone,
        aisle,
        printgroupid,
        battery_count,
        cell_count,
        lithium_weight,
        battery_watthrs,
        iataprimarychemcode,
        primaryhazardclass,
        hazmat,
        ediauditchar01,
        ediauditchar02,
        ediauditchar03,
        ediauditchar04,
        ediauditchar05,
        ediauditchar06,
        ediauditchar07,
        ediauditchar08,
        ediauditchar09,
        ediauditchar10,
        ediauditchar11,
        ediauditchar12,
        ediauditchar13,
        ediauditchar14,
        ediauditchar15,
        ediauditchar18,
        ediauditchar19,
        ediauditchar20,
        labelimagename3,
        labelimagename4,
        labelimagename5,
        labelimagename6,
        transmit_to_packsize_yn,
        wcsstatus,
        wcsprocessdatetime,
        premnfst_attempts,
        hazmatind,
        toteid,
        return_only_yn,
        return_only_lbl_printed,
        return_only_cartonid

   )
   values (
        i_custid,
        i_opcode,
        i_facility,
        i_lpid,
        i_orderid,
        i_shipid,
        i_wave,
        i_taskid,
        i_carrier,
        i_deliveryservice,
        i_sscc,
        i_reference,
        i_po,
        i_connectshipservice,
        i_shiptoname,
        i_shiptocontact,
        i_shiptoaddr1,
        i_shiptoaddr2,
        i_shiptoaddr3,
        i_shiptocity,
        i_shiptostate,
        i_shiptopostalcode,
        i_shiptocountry,
        i_shiptophone,
        i_shiptoemail,
        i_residentialflag,
        i_saturdaydeliveryflag,
        i_signaturerequiredflag,
        i_terms,
        i_seq,
        i_seqof,
        i_billtoname,
        i_billtocontact,
        i_billtoaddr1,
        i_billtoaddr2,
        i_billtoaddr3,
        i_billtocity,
        i_billtostate,
        i_billtopostalcode,
        i_billtocountry,
        i_billtophone,
        i_billtoemail,
        i_thirdpartynumber,
        i_shippernumber,
        i_accountnumber,
        i_containertype,
        i_length,
        i_width,
        i_height,
        i_weight,
        i_shipdate,
        i_packaging,
        i_codamount,
        i_codmethod,
        i_declaredvalue,
        i_description,
        i_sedmethod,
        i_ultimatecountry,
        i_carrierinstructions,
        i_ci_method,
        i_printer,
        i_trackingnumber,
        i_cost,
        i_labelimagename,
        i_errormessage,
        i_msn_id,
        i_creationdate,
        i_canceldate,
        i_cancelledby,
        i_bypassdate,
        i_bypassedby,
        i_receivedate,
        i_status,
        i_item,
        i_used,
        i_hdrpassthruchar01,
        i_hdrpassthruchar02,
        i_hdrpassthruchar03,
        i_hdrpassthruchar04,
        i_hdrpassthruchar05,
        i_hdrpassthruchar06,
        i_hdrpassthruchar07,
        i_hdrpassthruchar08,
        i_hdrpassthruchar09,
        i_hdrpassthruchar10,
        i_hdrpassthruchar11,
        i_hdrpassthruchar12,
        i_hdrpassthruchar13,
        i_hdrpassthruchar14,
        i_hdrpassthruchar15,
        i_hdrpassthruchar16,
        i_hdrpassthruchar17,
        i_hdrpassthruchar18,
        i_hdrpassthruchar19,
        i_hdrpassthruchar20,
        i_hdrpassthrunum01,
        i_hdrpassthrunum02,
        i_hdrpassthrunum03,
        i_hdrpassthrunum04,
        i_hdrpassthrunum05,
        i_hdrpassthrunum06,
        i_hdrpassthrunum07,
        i_hdrpassthrunum08,
        i_hdrpassthrunum09,
        i_hdrpassthrunum10,
        i_hdrpassthrudate01,
        i_hdrpassthrudate02,
        i_hdrpassthrudate03,
        i_hdrpassthrudate04,
        i_hdrpassthrudoll01,
        i_hdrpassthrudoll02,
        i_gc3hdrpassthruchar01,
        i_gc3hdrpassthruchar02,
        i_gc3hdrpassthruchar03,
        i_gc3hdrpassthruchar04,
        i_gc3hdrpassthruchar05,
        i_gc3hdrpassthruchar06,
        i_gc3hdrpassthruchar07,
        i_gc3hdrpassthruchar08,
        i_gc3hdrpassthruchar09,
        i_gc3hdrpassthruchar10,
        i_gc3hdrpassthruchar11,
        i_gc3hdrpassthruchar12,
        i_gc3hdrpassthruchar13,
        i_gc3hdrpassthruchar14,
        i_gc3hdrpassthruchar15,
        i_gc3hdrpassthruchar16,
        i_gc3hdrpassthruchar17,
        i_gc3hdrpassthruchar18,
        i_gc3hdrpassthruchar19,
        i_gc3hdrpassthruchar20,
        i_gc3hdrpassthrunum01,
        i_gc3hdrpassthrunum02,
        i_gc3hdrpassthrunum03,
        i_gc3hdrpassthrunum04,
        i_gc3hdrpassthrunum05,
        i_gc3hdrpassthrunum06,
        i_gc3hdrpassthrunum07,
        i_gc3hdrpassthrunum08,
        i_gc3hdrpassthrunum09,
        i_gc3hdrpassthrunum10,
        i_gc3hdrpassthrudate01,
        i_gc3hdrpassthrudate02,
        i_gc3hdrpassthrudate03,
        i_gc3hdrpassthrudate04,
        i_gc3hdrpassthrudoll01,
        i_gc3hdrpassthrudoll02,
        i_asnhdrpassthruchar01,
        i_asnhdrpassthruchar02,
        i_asnhdrpassthruchar03,
        i_asnhdrpassthruchar04,
        i_asnhdrpassthruchar05,
        i_asnhdrpassthruchar06,
        i_asnhdrpassthruchar07,
        i_asnhdrpassthruchar08,
        i_asnhdrpassthruchar09,
        i_asnhdrpassthruchar10,
        i_asnhdrpassthruchar11,
        i_asnhdrpassthruchar12,
        i_asnhdrpassthruchar13,
        i_asnhdrpassthruchar14,
        i_asnhdrpassthruchar15,
        i_asnhdrpassthruchar16,
        i_asnhdrpassthruchar17,
        i_asnhdrpassthruchar18,
        i_asnhdrpassthruchar19,
        i_asnhdrpassthruchar20,
        i_asnhdrpassthrunum01,
        i_asnhdrpassthrunum02,
        i_asnhdrpassthrunum03,
        i_asnhdrpassthrunum04,
        i_asnhdrpassthrunum05,
        i_asnhdrpassthrunum06,
        i_asnhdrpassthrunum07,
        i_asnhdrpassthrunum08,
        i_asnhdrpassthrunum09,
        i_asnhdrpassthrunum10,
        i_asnhdrpassthrudate01,
        i_asnhdrpassthrudate02,
        i_asnhdrpassthrudate03,
        i_asnhdrpassthrudate04,
        i_asnhdrpassthrudoll01,
        i_asnhdrpassthrudoll02,
        i_subtask_seq,
        i_batch,
        i_location,
        i_route,
        i_replacement_order_yn,
        i_returntoname,
        i_returntocontact,
        i_returntoaddr1,
        i_returntoaddr2,
        i_returntoaddr3,
        i_returntocity,
        i_returntostate,
        i_returntopostalcode,
        i_returntocountrycode,
        i_returntophone,
        i_returntofax,
        i_returntoemail,
        i_returndcname,
        i_returndccontact,
        i_returndcaddr1,
        i_returndcaddr2,
        i_returndcaddr3,
        i_returndccity,
        i_returndcstate,
        i_returndcpostalcode,
        i_returndccountrycode,
        i_returndcphone,
        i_returndcfax,
        i_returndcemail,
        i_labelimagename2,
        i_return_trackingnumber,
        i_dtlpassthruchar02,
        i_dtlpassthruchar03,
        i_dtlpassthruchar05,
        i_dtlpassthruchar11,
        i_dtlpassthruchar13,
        i_dtlpassthruchar16,
        i_dtlpassthruchar17,
        i_dtlpassthruchar18,
        i_dtlpassthruchar19,
        i_dtlpassthrunum04,
        i_dtlpassthrudate01,
        i_ediauditchar16,
        i_ediauditchar17,
        i_itemdescr,
        i_bolcomment,
        i_msn_id_rtn,
        i_dtlpassthrunum01,
        i_dtlpassthrunum02,
        i_dtlpassthrunum03,
        i_dtlpassthrunum05,
        i_dtlpassthrunum06,
        i_dtlpassthrunum07,
        i_dtlpassthrunum08,
        i_dtlpassthrunum09,
        i_dtlpassthrunum10,
        i_dtlpassthruchar01,
        i_dtlpassthruchar04,
        i_dtlpassthruchar06,
        i_dtlpassthruchar07,
        i_dtlpassthruchar08,
        i_dtlpassthruchar09,
        i_dtlpassthruchar10,
        i_dtlpassthruchar12,
        i_dtlpassthruchar14,
        i_dtlpassthruchar15,
        i_dtlpassthruchar20,
        i_dtlpassthrudate02,
        i_dtlpassthrudate03,
        i_dtlpassthrudate04,
        i_dtlpassthrudoll01,
        i_dtlpassthrudoll02,
        i_baseuomunitweight,
        i_trackingnumber2,
        i_trackingnumber3,
        i_return_trackingnumber2,
        i_return_trackingnumber3,
        i_zone,
        i_aisle,
        i_printgroupid,
        i_battery_count,
        i_cell_count,
        i_lithium_weight,
        i_battery_watthrs,
        i_iataprimarychemcode,
        i_primaryhazardclass,
        i_hazmat,
        i_ediauditchar01,
        i_ediauditchar02,
        i_ediauditchar03,
        i_ediauditchar04,
        i_ediauditchar05,
        i_ediauditchar06,
        i_ediauditchar07,
        i_ediauditchar08,
        i_ediauditchar09,
        i_ediauditchar10,
        i_ediauditchar11,
        i_ediauditchar12,
        i_ediauditchar13,
        i_ediauditchar14,
        i_ediauditchar15,
        i_ediauditchar18,
        i_ediauditchar19,
        i_ediauditchar20,
        i_labelimagename3,
        i_labelimagename4,
        i_labelimagename5,
        i_labelimagename6,
        i_transmit_to_packsize_yn,
        i_wcsstatus,
        i_wcsprocessdatetime,
        i_premnfst_attempts,
        i_hazmatind,
        i_toteid,
        i_return_only_yn,
        i_return_only_lbl_printed,
        i_return_only_cartonid

   );
    
    out_success:=sql%rowcount;
EXCEPTION WHEN OTHERS THEN
    out_success := -1;
END WVR_CONNECTSHIPHDR_I_P;

