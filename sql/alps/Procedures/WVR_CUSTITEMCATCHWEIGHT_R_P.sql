CREATE OR REPLACE PROCEDURE WVR_CUSTITEMCATCHWEIGHT_R_P( 
  i_custid IN VARCHAR2, 
  i_item IN VARCHAR2,
  c_catchweight OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_catchweight FOR
    	select uom,
      		 nvl(totqty, 0) as qty,
             totweight as weight
         from custitemcatchweight
         where custid = i_custid
           and item = i_item;

END WVR_CUSTITEMCATCHWEIGHT_R_P;





 