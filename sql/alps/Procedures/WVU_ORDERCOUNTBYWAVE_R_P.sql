CREATE OR REPLACE PROCEDURE WVU_ORDERCOUNTBYWAVE_R_P ( 
  i_wave IN NUMBER, 
  o_rowscount OUT NUMBER
) AS
BEGIN
 	SELECT COUNT(1)
      INTO o_rowscount
      FROM orderhdr
    WHERE wave = i_wave;
	EXCEPTION WHEN OTHERS THEN
    	o_rowscount := 0;

END WVU_ORDERCOUNTBYWAVE_R_P;



 