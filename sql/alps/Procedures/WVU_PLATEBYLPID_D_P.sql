CREATE OR REPLACE PROCEDURE WVU_PLATEBYLPID_D_P (
  i_lpid IN varchar2,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  delete from plate
     where lpid = i_lpid;

  o_rowsaffected := SQL%ROWCOUNT;

END WVU_PLATEBYLPID_D_P;