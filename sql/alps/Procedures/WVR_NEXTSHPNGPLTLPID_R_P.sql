CREATE OR REPLACE PROCEDURE WVR_NEXTSHPNGPLTLPID_R_P ( 
  o_shippinglpid OUT VARCHAR2
) AS
msg VARCHAR2(80);
BEGIN

  zsp.get_next_shippinglpid(o_shippinglpid, msg);

END WVR_NEXTSHPNGPLTLPID_R_P;