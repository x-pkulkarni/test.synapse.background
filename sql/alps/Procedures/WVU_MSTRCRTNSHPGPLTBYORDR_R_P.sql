CREATE OR REPLACE PROCEDURE WVU_MSTRCRTNSHPGPLTBYORDR_R_P ( 
  i_orderid IN NUMBER, 
  i_shipid IN NUMBER, 
  c_shippingplates OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_shippingplates FOR
    select lpid, fromlpid
              from shippingplate Q
             where orderid = i_orderid
               and shipid = i_shipid
               and type in ('M','C')
               and fromlpid is not null
               and not exists
                (select *
                   from shippingplate
                  where orderid = i_orderid
                    and shipid = i_shipid
                    and type in ('F','P')
                    and parentlpid = Q.lpid);

END WVU_MSTRCRTNSHPGPLTBYORDR_R_P;



 