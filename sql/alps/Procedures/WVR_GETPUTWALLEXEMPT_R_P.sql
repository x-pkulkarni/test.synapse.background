CREATE OR REPLACE PROCEDURE WVR_GETPUTWALLEXEMPT_R_P( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  o_exempt OUT VARCHAR2
 ) AS
 
BEGIN
    
      select nvl(PUTWALL_EXEMPT_YN, 'N')
        into o_exempt
        from custitem 
      where custid = i_custid
        and item = i_item;

EXCEPTION WHEN OTHERS THEN
  		o_exempt := 'N';

END WVR_GETPUTWALLEXEMPT_R_P;
