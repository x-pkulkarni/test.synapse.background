CREATE OR REPLACE PROCEDURE RGP_BATCHTASKSBYTSKID_D_P (
  i_taskid IN NUMBER,  
  i_facility IN VARCHAR2,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

delete batchtasks
      where facility = i_facility
       and taskid = i_taskid;

  o_rowsaffected := SQL%ROWCOUNT;


END RGP_BATCHTASKSBYTSKID_D_P;