CREATE OR REPLACE PROCEDURE WVR_STAGELOCBYCARRIER_R_P( 
  i_carrier IN VARCHAR2,
  i_facility IN VARCHAR2,
  i_shiptype IN VARCHAR2,
  o_stageloc OUT VARCHAR2
) AS
BEGIN
      
  select stageloc into o_stageloc
    from carrierstageloc
   where carrier = i_carrier
     and facility = i_facility
     and shiptype = i_shiptype;
   EXCEPTION WHEN OTHERS THEN
  	o_stageloc := '';

END WVR_STAGELOCBYCARRIER_R_P;