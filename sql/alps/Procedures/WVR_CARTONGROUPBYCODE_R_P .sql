create or replace PROCEDURE WVR_CARTONGROUPBYCODE_R_P ( 
  i_code IN VARCHAR2,
  o_cartongroup OUT VARCHAR2
) AS
BEGIN

     select cartongroup
  		into o_cartongroup 
  	from cartongroups
	where code = i_code 
   		and rownum < 2;
	EXCEPTION WHEN OTHERS THEN
  		o_cartongroup := '';


END WVR_CARTONGROUPBYCODE_R_P;