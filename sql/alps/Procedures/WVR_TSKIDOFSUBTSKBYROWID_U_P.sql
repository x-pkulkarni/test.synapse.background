CREATE OR REPLACE PROCEDURE WVR_TSKIDOFSUBTSKBYROWID_U_P (
  i_taskid IN NUMBER,  
  i_lastuser IN VARCHAR2,
  i_rowid IN rowid,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

UPDATE SUBTASKS
  SET TASKID  = i_taskid,
    LASTUSER   = i_lastuser,
    LASTUPDATE = SYSDATE
  WHERE ROWID = i_rowid;

  o_rowsaffected := SQL%ROWCOUNT;


END WVR_TSKIDOFSUBTSKBYROWID_U_P;