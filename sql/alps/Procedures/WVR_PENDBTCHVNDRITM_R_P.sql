CREATE OR REPLACE PROCEDURE WVR_PENDBTCHVNDRITM_R_P ( 
  i_orderid IN NUMBER, 
  i_orderitem IN VARCHAR2,
  i_shipid IN NUMBER,
  i_orderlot IN VARCHAR2,
  i_item IN VARCHAR2,
  i_invstatus IN VARCHAR2,
  o_quantity OUT NUMBER
) AS
BEGIN

SELECT NVL(SUM(qty),0) INTO o_quantity
    FROM batchtasks
	WHERE orderid = i_orderid
     AND shipid = i_shipid
     AND orderitem = i_orderitem
     AND NVL(orderlot,'(none)') = NVL(i_orderlot,'(none)')
     and item = i_item
     and invstatus = i_invstatus;
	 

END WVR_PENDBTCHVNDRITM_R_P;