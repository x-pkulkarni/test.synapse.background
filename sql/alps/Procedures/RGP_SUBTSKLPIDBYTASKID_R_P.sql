CREATE OR REPLACE PROCEDURE RGP_SUBTSKLPIDBYTASKID_R_P ( 
  i_taskid IN NUMBER, 
  i_facility IN VARCHAR2, 
  c_subtasks OUT SYS_REFCURSOR
) AS
BEGIN
	  
 OPEN c_subtasks FOR
	 select lpid, sum(qty) as Quantity
        from subtasks
      where facility = i_facility
        and tasktype = 'BP'
        and taskid = i_taskid
      group by lpid
      order by lpid;

END RGP_SUBTSKLPIDBYTASKID_R_P;