CREATE OR REPLACE PROCEDURE WVR_GETPLATEPROFID_R_P (  
    i_event   IN VARCHAR2,
    i_lpid  IN VARCHAR2,   
    out_uom    OUT VARCHAR2,
    out_profid OUT VARCHAR2,
    out_msg    OUT VARCHAR2
) AS
BEGIN

 zlbl.get_plate_profid(i_event, i_lpid, 'S', 'A', out_uom, out_profid, out_msg);

END WVR_GETPLATEPROFID_R_P;



 