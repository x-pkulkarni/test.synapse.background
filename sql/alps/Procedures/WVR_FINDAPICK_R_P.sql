CREATE OR REPLACE PROCEDURE WVR_FINDAPICK_R_P (
  i_fromfacility IN VARCHAR2,  
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_lotnumber IN VARCHAR2,
  i_invstatus IN VARCHAR2,
  i_inventoryclass IN VARCHAR2,
  i_qty IN NUMBER,
  i_qtytype IN VARCHAR2,
  i_variancepct IN NUMBER,
  i_pickuom IN VARCHAR2,
  i_repl_req_yn IN VARCHAR2,
  i_storage_or_stage IN VARCHAR2,
  i_expdaterequired IN VARCHAR2,
  i_enter_min_days_to_expire_yn IN VARCHAR2,
  i_min_days_to_expiration IN NUMBER,
  i_preferred_pickzone IN VARCHAR2,
  i_vendor IN VARCHAR2,
  i_trace IN VARCHAR2,
  i_orderid IN NUMBER default null,
  i_shipid IN NUMBER default null,
  i_pfloc IN VARCHAR2 default null,
  i_slotwave_req_yn IN VARCHAR2 default 'N',
  i_dynamic_alloc_mode IN VARCHAR2 default 'N',
  i_allocneed IN VARCHAR2,
  i_picktotype_rule IN VARCHAR2,
  c_pick_details OUT SYS_REFCURSOR
) AS
	o_lpid  shippingplate.lpid%type;
    	o_baseuom tasks.pickuom%type;
    	o_baseqty plate.quantity%type;
    	o_pickuom tasks.pickuom%type;
    	o_pickqty plate.quantity%type;
	o_pickfront  char(1);
	o_picktotype waves.picktype%type;
	o_cartontype custitem.cartontype%type;
	o_picktype  waves.picktype%type;
	o_wholeunitsonly  char(1);
	o_inventoryclass plate.inventoryclass%type;
	o_Prevent_FPlates char(1);
	o_msg VARCHAR2(2000);

BEGIN

 
ALPS.ZWAVE.find_a_pick(in_fromfacility => i_fromfacility,
                        in_custid => i_custid,
                        in_item => i_item,
                        in_lotnumber => i_lotnumber,
                        in_invstatus => i_invstatus,
                        in_inventoryclass => i_inventoryclass,
                        in_qty => i_qty,
                        in_qtytype => i_qtytype,
                        in_variancepct => i_variancepct,
                        in_pickuom => i_pickuom,
                        in_repl_req_yn => i_repl_req_yn, 
                        in_storage_or_stage => i_storage_or_stage,
                        in_expdaterequired => i_expdaterequired,
                        in_enter_min_days_to_expire_yn => i_enter_min_days_to_expire_yn,
                        in_min_days_to_expiration => i_min_days_to_expiration,
                        in_wave_pickzone => i_preferred_pickzone,
                        in_vendor => i_vendor,
                        out_lpid_or_loc => o_lpid,
                        out_baseuom => o_baseuom,
                        out_baseqty => o_baseqty,
                        out_pickuom => o_pickuom,
                        out_pickqty => o_pickqty,
                        out_pickfront => o_pickfront,
                        out_picktotype => o_picktotype,
                        out_cartontype => o_cartontype,
                        out_picktype => o_picktype,
                        out_wholeunitsonly => o_wholeunitsonly,
                        out_inventoryclass => o_inventoryclass,
                        in_trace => i_trace,
                        out_Prevent_FPlates => o_Prevent_FPlates,
                        out_msg => o_msg,
                        in_orderid => i_orderid,
                        in_shipid => i_shipid,
                        in_pfloc => i_pfloc,
                        in_slotwave_req_yn => i_slotwave_req_yn,
                        in_dynamic_alloc_mode => i_dynamic_alloc_mode,
                        in_allocneed => i_allocneed,
			in_picktotype_rule => i_picktotype_rule);


 OPEN c_pick_details FOR
    SELECT  o_lpid AS LpIdOrLoc,
		o_baseuom AS BaseUom,
		o_baseqty AS BaseQty,
		o_pickuom AS PickUom,
		o_pickqty AS PickQty,
		o_pickfront AS PickFront,
		o_picktotype AS PickToType,
		o_cartontype AS CartonType,
		o_picktype AS PickType,
		Decode(nvl(o_wholeunitsonly,'N'),'Y',1,0) as IsWholeUnitsOnly,
		o_inventoryclass AS InventoryClass,
		o_Prevent_FPlates AS PreventFPlates,
		o_msg AS Message
	FROM DUAL;

END WVR_FINDAPICK_R_P;