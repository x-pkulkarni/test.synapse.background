CREATE OR REPLACE PROCEDURE WVR_GETNEXTHIGHUOM_R_P( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  o_uom OUT VARCHAR2
 ) AS
 
BEGIN
    
select cio.touom into o_uom
      from custitem ci, custitemuom cio  
      where ci.custid = i_custid 
        and ci.item   = i_item 
        and ci.custid = cio.custid (+)
        and ci.item   = cio.item (+)
        and ci.baseuom = cio.fromuom (+)
      order by cio.qty asc;    
      
            
EXCEPTION WHEN OTHERS THEN
  		o_uom := '';

END WVR_GETNEXTHIGHUOM_R_P;
