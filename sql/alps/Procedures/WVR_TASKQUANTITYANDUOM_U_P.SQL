CREATE OR REPLACE PROCEDURE WVR_TASKQUANTITYANDUOM_U_P (
  i_taskid IN NUMBER,  
  i_qty IN NUMBER,   
  i_uom IN varchar2,  
  i_pickqty IN NUMBER,  
  i_pickuom IN varchar2,
  i_weight IN NUMBER,  
  i_cube IN NUMBER,    
  i_staffhrs IN NUMBER,   
  o_rowsaffected OUT NUMBER
) AS
BEGIN

 update tasks
       set pickqty = i_pickqty,
           pickuom = i_pickuom,
           qty = i_qty,
           uom = i_uom,
           weight = i_weight,
           cube = i_cube,
           staffhrs = i_staffhrs           
       where taskid = i_taskid; 

  o_rowsaffected := SQL%ROWCOUNT;


END WVR_TASKQUANTITYANDUOM_U_P;