CREATE OR REPLACE PROCEDURE WVR_COMMITMENTNOTVNDRTRKD_R_P ( 
  	i_orderid IN NUMBER, 
	i_shipid IN NUMBER,
	i_orderitem IN VARCHAR2,
	i_orderlot IN VARCHAR2,
  	c_commitment OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_commitment FOR
     select item, 
		nvl(lotnumber,'(none)') AS LOTNUMBER, 
		invstatus, 
		inventoryclass, 
		nvl(qty,0) AS QUANTITY, 
		uom
	from commitments
	where orderid = i_orderid
      		AND shipid = i_shipid
	  	AND orderitem = i_orderitem
	  	AND nvl(orderlot,'(none)') = nvl(i_orderlot,'(none)')
	order by item, lotnumber, invstatus,inventoryclass;

END WVR_COMMITMENTNOTVNDRTRKD_R_P;



 