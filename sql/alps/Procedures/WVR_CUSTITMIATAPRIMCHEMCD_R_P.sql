CREATE OR REPLACE PROCEDURE WVR_CUSTITMIATAPRIMCHEMCD_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  c_custitem OUT SYS_REFCURSOR
) AS
BEGIN
OPEN c_custitem FOR
     SELECT item, iataprimarychemcode
  	FROM custitem
  	WHERE custid = i_custid
  		AND   item = i_item;

END WVR_CUSTITMIATAPRIMCHEMCD_R_P;