CREATE OR REPLACE PROCEDURE RGP_RESETBATCHTASKS_U_P (
  i_taskid IN NUMBER,  
  i_facility IN VARCHAR2,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

update batchtasks
        set taskid = 0,
            facility = null,
            lpid = null
      where facility = i_facility
       and taskid = i_taskid;

  o_rowsaffected := SQL%ROWCOUNT;


END RGP_RESETBATCHTASKS_U_P;