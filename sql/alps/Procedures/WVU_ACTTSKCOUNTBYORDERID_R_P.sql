CREATE OR REPLACE PROCEDURE WVU_ACTTSKCOUNTBYORDERID_R_P ( 
  i_orderid IN NUMBER, 
  i_shipid IN NUMBER, 
  o_rowscount OUT NUMBER
) AS
BEGIN

    SELECT count(1)
      INTO o_rowscount
      FROM subtasks ST INNER JOIN tasks TK
      ON TK.taskid = ST.taskid
   WHERE ST.orderid = i_orderid
     and ST.shipid = i_shipid
     and TK.priority = '0';
EXCEPTION WHEN OTHERS THEN
    o_rowscount := 1;

END WVU_ACTTSKCOUNTBYORDERID_R_P;