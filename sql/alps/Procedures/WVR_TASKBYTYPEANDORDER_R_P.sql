CREATE OR REPLACE PROCEDURE WVR_TASKBYTYPEANDORDER_R_P ( 
  i_wave IN NUMBER,
  i_orderid IN NUMBER,
  i_shipid IN NUMBER,
  i_tasktype IN VARCHAR2,
  c_tasks OUT SYS_REFCURSOR
)  AS
BEGIN

OPEN c_tasks FOR
  SELECT TaskId, TaskType, Weight, Cube, staffhrs, pickqty, qty, WAVE, OrderID, shipid 
    FROM TASKS 
    WHERE WAVE = i_wave 
        AND OrderID = i_orderid 
        AND shipid = i_shipid 
        AND TaskType = i_tasktype;

END WVR_TASKBYTYPEANDORDER_R_P;