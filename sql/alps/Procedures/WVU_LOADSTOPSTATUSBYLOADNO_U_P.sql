CREATE OR REPLACE PROCEDURE WVU_LOADSTOPSTATUSBYLOADNO_U_P (
  i_userid IN varchar2,
  i_loadno IN NUMBER, 
  i_stopno IN NUMBER, 
  i_status IN varchar2,     
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  update loadstop
       set loadstopstatus = i_status,
           lastuser = i_userid,
           lastupdate = sysdate
     where loadno = i_loadno
        and stopno = i_stopno
        and loadstopstatus < i_status;

  o_rowsaffected := SQL%ROWCOUNT;

END WVU_LOADSTOPSTATUSBYLOADNO_U_P;