CREATE OR REPLACE PROCEDURE WVR_GETORDERBYTRIGGERVALUE_R_P (  
    i_wave   IN NUMBER,
    i_custid   IN VARCHAR2,
    i_standard_label_override_column   IN VARCHAR2,
    i_labeltrigger  IN VARCHAR2,   
    c_rec OUT SYS_REFCURSOR
) AS
v_sql VARCHAR2(4000);
BEGIN

    
    v_sql := 'select orderid,shipid from orderhdr where wave ='''|| i_wave || ''' and custid='''|| i_custid ||''' and '|| 
    i_standard_label_override_column || ' = ''' || i_labeltrigger || '''';
    
     OPEN c_rec FOR v_sql;
    
     
END WVR_GETORDERBYTRIGGERVALUE_R_P;



 