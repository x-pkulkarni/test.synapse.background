create or replace PROCEDURE WVR_SUBTSKLTLBYORDR_R_P ( 
  i_orderid IN NUMBER,  
  i_shipid IN NUMBER,  
  i_picktotype IN VARCHAR2, 
  i_cartontype IN VARCHAR2, 
  i_cartonseq IN NUMBER
  c_subtasks OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_subtasks FOR
         select ST.wave, ST.taskid, ST.facility, ST.custid, ST.item, ST.rowid,
             ST.uom, ST.qty, ST.pickuom, ST.pickqty,
             OH.carrier, OH.deliveryservice
         from subtasks ST, orderhdr OH
         where ST.orderid = i_orderid
           and ST.shipid = i_shipid
           and ST.picktotype = i_picktotype
           and ST.cartontype = i_cartontype
           and ST.cartonseq = i_cartonseq
           and OH.orderid = ST.orderid
           and OH.shipid = ST.shipid;

END WVR_SUBTSKLTLBYORDR_R_P;