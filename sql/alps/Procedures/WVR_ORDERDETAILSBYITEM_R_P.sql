CREATE OR REPLACE PROCEDURE WVR_ORDERDETAILSBYITEM_R_P ( 
  i_orderid IN NUMBER,
  i_item IN VARCHAR2,
  i_shipid IN NUMBER,
  c_orderdtls OUT SYS_REFCURSOR
) AS
BEGIN
OPEN c_orderdtls FOR
      SELECT ORDERID,
        SHIPID,
        ITEM,
        LOTNUMBER,
        INVSTATUS,
        QTYTYPE AS QUANTITYTYPE,
        INVENTORYCLASS,
        CUSTID AS CUSTOMERID,
        FROMFACILITY,
        UOM,
        LINESTATUS,
        COMMITSTATUS,
        QTYENTERED,
        ITEMENTERED,
        UOMENTERED,
        QTYORDER,
        WEIGHTORDER,
        CUBEORDER,
        AMTORDER,
        QTYCOMMIT,
        VENDOR,
        QTYPICK,
        XDOCKORDERID,
        XDOCKSHIPID,
        PRIORITY,
        STAFFHRS,
        LASTUSER,
        MIN_DAYS_TO_EXPIRATION,
        IATAPRIMARYCHEMCODE,
        DYNAMIC_INV_USED_YN  AS DYNAMICINVUSEDFLAG
  		FROM orderdtl
  		WHERE orderid = i_orderid
  			AND   shipid = i_shipid
  			AND   item = i_item;

END WVR_ORDERDETAILSBYITEM_R_P;