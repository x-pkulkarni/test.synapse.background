CREATE OR REPLACE PROCEDURE WVR_GETSHIPLIPBYWAVE_R_P ( 
  i_wave IN NUMBER, 
  c_shiplip OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_shiplip FOR
    select taskid,shippinglpid,ltl_fel_seq as ltlfelseq,custid,facility
      from SUBTASKS
      where WAVE =  i_wave;       

END WVR_GETSHIPLIPBYWAVE_R_P;



 