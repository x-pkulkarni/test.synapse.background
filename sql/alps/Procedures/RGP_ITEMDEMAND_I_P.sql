CREATE OR REPLACE PROCEDURE RGP_ITEMDEMAND_I_P (
  i_facility IN VARCHAR2,
  i_item IN VARCHAR2,
  i_lotnumber IN VARCHAR2,
  i_priority IN VARCHAR2,
  i_invstatusind IN VARCHAR2,
  i_invclassind IN VARCHAR2,
  i_invstatus IN VARCHAR2,
  i_inventoryclass IN VARCHAR2,
  i_demandtype IN VARCHAR2,
  i_orderid IN NUMBER,
  i_shipid IN NUMBER,
  i_loadno IN NUMBER,
  i_stopno IN NUMBER,
  i_shipno IN NUMBER,
  i_orderitem IN VARCHAR2,
  i_orderlot IN VARCHAR2,
  i_qty IN NUMBER,
  i_userid IN VARCHAR2,
  i_custid IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

insert into ITEMDEMAND
  (
      FACILITY,
      ITEM,
      LOTNUMBER,
      PRIORITY,
      INVSTATUSIND,
      INVCLASSIND,
      INVSTATUS,
      INVENTORYCLASS,
      DEMANDTYPE,
      ORDERID,
      SHIPID,
      LOADNO,
      STOPNO,
      SHIPNO,
      ORDERITEM,
      ORDERLOT,
      QTY,
      LASTUSER,
      LASTUPDATE,
      CUSTID
  )
  values
  (
      i_facility,
      i_item,
      i_lotnumber,
      i_priority,
      i_invstatusind,
      i_invclassind,
      i_invstatus,
      i_inventoryclass,
      i_demandtype,
      i_orderid,
      i_shipid,
      i_loadno,
      i_stopno,
      i_shipno,
      i_orderitem,
      i_orderlot,
      i_qty,
      i_userid,
      sysdate,
      i_custid
  );

  o_rowsaffected := SQL%ROWCOUNT;


END RGP_ITEMDEMAND_I_P;