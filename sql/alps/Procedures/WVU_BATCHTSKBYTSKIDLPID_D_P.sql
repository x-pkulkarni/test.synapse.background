CREATE OR REPLACE PROCEDURE WVU_BATCHTSKBYTSKIDLPID_D_P (
  i_taskid IN NUMBER,  
  i_custid IN VARCHAR2,  
  i_orderitem IN VARCHAR2, 
  i_orderlot IN VARCHAR2, 
  i_lpid IN VARCHAR2, 
  i_item IN VARCHAR2, 
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  DELETE 
    FROM BATCHTASKS
    WHERE TASKID = i_taskid
      AND CUSTID = i_custid
      AND NVL(ORDERITEM,'(none)') = NVL(i_orderitem, '(none)')
      AND NVL(ORDERLOT,'(none)') = NVL(i_orderlot, '(none)')
      AND NVL(LPID,'(none)') = NVL(i_lpid, '(none)')
      AND ITEM = i_item;

  o_rowsaffected := SQL%ROWCOUNT;


END WVU_BATCHTSKBYTSKIDLPID_D_P;