CREATE OR REPLACE PROCEDURE WVR_VENDORTRACKEDFLAG_R_P (
i_custid IN varchar2,
i_item IN varchar2,
o_flag OUT varchar2
) 
AS
BEGIN

  select vendor_tracked_yn
    into o_flag 
    from custitemview
   where custid = i_custid
     and item = i_item;

    exception when others then
     o_flag := 'N';
  
END WVR_VENDORTRACKEDFLAG_R_P;