CREATE OR REPLACE PROCEDURE RGP_CUSTITEMSUBBYITEM_R_P ( 
  i_orderitem IN VARCHAR2, 
  i_custid IN NUMBER, 
  c_custitemsub OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_custitemsub FOR
    SELECT itemsub,
         seq
    FROM custitemsubs
   	WHERE custid = i_custid
     		AND item = i_orderitem
   	ORDER BY 2,1;

END RGP_CUSTITEMSUBBYITEM_R_P;



 