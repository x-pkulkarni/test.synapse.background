CREATE OR REPLACE PROCEDURE WVR_GETTASKLIPBYORDERANDSHIP_R_P ( 
  i_orderid IN NUMBER, 
  i_shipid IN NUMBER, 
  c_tasklip OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_tasklip FOR
     select taskid,shippinglpid,ltl_fel_seq as ltlfelseq,custid,facility
      from subtasks
      where orderid = i_orderid and shipid = i_shipid;      

END WVR_GETTASKLIPBYORDERANDSHIP_R_P;



 