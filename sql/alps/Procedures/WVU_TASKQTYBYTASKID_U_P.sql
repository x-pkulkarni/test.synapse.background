create or replace PROCEDURE WVU_TASKQTYBYTASKID_U_P (
  i_taskid IN NUMBER,  
  i_qty IN NUMBER,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN




   update tasks
     set qty = qty - i_qty
   where taskid = i_taskid
     and qty > i_qty;

  o_rowsaffected := SQL%ROWCOUNT;

END WVU_TASKQTYBYTASKID_U_P;