CREATE OR REPLACE PROCEDURE WVU_MSTRCRTNSHPGPLTBYORDR_D_P (
  i_orderid IN NUMBER,  
  i_shipid IN NUMBER,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  delete from shippingplate Q
 where orderid = i_orderid
   and shipid = i_shipid
   and type in ('M','C')
   and not exists
    (select *
       from shippingplate
      where orderid = i_orderid
        and shipid = i_shipid
        and type in ('F','P')
        and parentlpid = Q.lpid);

  o_rowsaffected := SQL%ROWCOUNT;

END WVU_MSTRCRTNSHPGPLTBYORDR_D_P;