CREATE OR REPLACE PROCEDURE WVR_GETPROFLINE_R_P ( 
  i_event IN VARCHAR2, 
  i_prof_id IN VARCHAR2, 
  i_uom IN VARCHAR2, 
  i_key_origin IN VARCHAR2, 
  c_profline OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_profline FOR
   select upper(printerstock) AS printerstock, upper(print) AS print, upper(apply) AS apply, rfline1, rfline2,
      rfline3, rfline4, rowidtochar(rowid) AS rowid, viewname, viewkeycol, labeltrigger, seq
    from labelprofileline
      where profid = i_prof_id
        and businessevent = i_event
        and upper(labeltrigger) is not null
        and (uom = i_uom or uom is null)
        and nvl(viewkeyorigin,'?') = i_key_origin
      order by seq;      

END WVR_GETPROFLINE_R_P;



 