create or replace PROCEDURE WVU_SUBTASKSBYORDER_R_P ( 
  i_orderid IN NUMBER,  
  i_shipid IN NUMBER, 
  c_subtasks OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_subtasks FOR
      select rowid,
         taskid,
         custid,
         facility,
		 Qty as Quantity,
		 loadno,
		 shipno,
		 stopno,
         lpid,
         shippinglpid
    from subtasks
   where orderid = i_orderid
     and shipid = i_shipid
     and not exists
       (select * from tasks
         where subtasks.taskid = tasks.taskid
           and tasks.priority = '0');

END WVU_SUBTASKSBYORDER_R_P;