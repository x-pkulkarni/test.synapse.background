CREATE OR REPLACE PROCEDURE WVR_CUSTAUXITEMUNCODEFLAG_R_P ( 
  i_custid IN VARCHAR2,
  c_customer OUT SYS_REFCURSOR
) AS
BEGIN
OPEN c_customer FOR
     SELECT custid,
    		nvl(useitemuncodefromorder_yn, 'N') as useitemuncodefromorder_yn
  	FROM customer_aux
  	WHERE custid = i_custid;

END WVR_CUSTAUXITEMUNCODEFLAG_R_P;