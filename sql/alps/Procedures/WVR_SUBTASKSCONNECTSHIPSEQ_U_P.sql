CREATE OR REPLACE PROCEDURE WVR_SUBTASKSCONNECTSHIPSEQ_U_P
(
i_strowid IN VARCHAR2,
i_subtaskseq IN NUMBER
) AS 
BEGIN  

    update subtasks
      set connectship_seq = i_subtaskseq
      where rowid = i_strowid;

END WVR_SUBTASKSCONNECTSHIPSEQ_U_P;
  