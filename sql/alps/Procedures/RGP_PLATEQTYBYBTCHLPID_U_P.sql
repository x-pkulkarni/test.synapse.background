CREATE OR REPLACE PROCEDURE RGP_PLATEQTYBYBTCHLPID_U_P (
  i_batchqty IN NUMBER,  
  i_lpid IN VARCHAR2,
  i_facility IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

UPDATE PLATE
  SET QTYTASKED  =  CASE 
                      WHEN (nvl(qtytasked,0) - i_batchqty) > 0 THEN 
                          (nvl(qtytasked,0) - i_batchqty) 
                      ELSE 
                        NULL 
                      END
  WHERE LPID = i_lpid
		and facility = i_facility
        and type = 'PA';

  o_rowsaffected := SQL%ROWCOUNT;


END RGP_PLATEQTYBYBTCHLPID_U_P;