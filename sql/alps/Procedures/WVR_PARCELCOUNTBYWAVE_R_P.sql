CREATE OR REPLACE PROCEDURE WVR_PARCELCOUNTBYWAVE_R_P ( 
  i_wave IN NUMBER, 
  o_rowscount OUT NUMBER
) AS
BEGIN
 	SELECT COUNT(1)
      INTO o_rowscount
      FROM parcelstagingtable
    WHERE wave = i_wave 
	     AND status = 'S';
	EXCEPTION WHEN OTHERS THEN
    	o_rowscount := 0;

END WVR_PARCELCOUNTBYWAVE_R_P;
