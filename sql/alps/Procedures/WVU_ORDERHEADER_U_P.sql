CREATE OR REPLACE PROCEDURE WVU_ORDERHEADER_U_P
(
i_orderstatus IN VARCHAR2,
i_commitstatus IN VARCHAR2,
i_wave IN NUMBER,
i_priority IN VARCHAR2,
i_userid IN VARCHAR2,
i_orderid IN NUMBER,
i_shipid IN NUMBER,
o_rowsaffected OUT NUMBER
) AS 
BEGIN  

update orderhdr
   set orderstatus = NVL(i_orderstatus,orderstatus),
       commitstatus = NVL(i_commitstatus,commitstatus),
       wave = NVL(i_wave,wave),
       priority  = NVL(i_priority,priority),
       lastuser = i_userid,
       lastupdate = sysdate
 where ORDERID = i_orderid
    AND SHIPID = i_shipid;

o_rowsaffected := SQL%ROWCOUNT;

END WVU_ORDERHEADER_U_P;
  