CREATE OR REPLACE PROCEDURE WVU_ORDRCOUNTWITHPICKS_R_P ( 
  i_wave IN NUMBER, 
  o_rowscount OUT NUMBER
) AS
BEGIN

     SELECT COUNT(1)
      INTO o_rowscount
      FROM orderhdr
    WHERE wave = i_wave
        and NVL(QTYPICK,0) != 0;
EXCEPTION WHEN OTHERS THEN
    o_rowscount := 1;
END WVU_ORDRCOUNTWITHPICKS_R_P;