create or replace PROCEDURE WVU_PARCELSTAGING_U_P (
  i_rowid IN ROWID,  
  i_user IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

        update parcelstagingtable
        set status = 'W',
          canceldate = sysdate,
          cancelledby = i_user
      where rowid = i_rowid;
  o_rowsaffected := SQL%ROWCOUNT;


END WVU_PARCELSTAGING_U_P;