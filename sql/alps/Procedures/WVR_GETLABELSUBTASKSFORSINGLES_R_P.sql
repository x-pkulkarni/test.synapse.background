create or replace PROCEDURE WVR_GETLABELSUBTASKSFORSINGLES_R_P ( 
  i_orderid IN NUMBER, 
  i_shipid IN NUMBER, 
  i_taskid IN NUMBER,
  c_lbl OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_lbl FOR
  select ST.taskid,
             ST.pickqty,
             ST.item,
             ST.orderitem,
             ST.orderlot,
             ST.weight,
             ST.pickuom,
             ST.uom,
             ST.rowid as strowid,
             CI.descr,
             OD.qtyorder,
             PL.countryof,
             ST.fromloc,
             OD.dtlpassthruchar02,
             OD.dtlpassthruchar03,
             OD.dtlpassthruchar05,
             OD.dtlpassthruchar11,
             OD.dtlpassthruchar13,
             OD.dtlpassthruchar16,
             OD.dtlpassthruchar17,
             OD.dtlpassthruchar18,
             OD.dtlpassthruchar19,
             OD.dtlpassthrunum04,
             OD.dtlpassthrudate01,
             OD.dtlpassthrunum01,
             OD.dtlpassthrunum02,
             OD.dtlpassthrunum03,
             OD.dtlpassthrunum05,
             nvl(OD.dtlpassthrunum06,1) dtlpassthrunum06,
             OD.dtlpassthrunum07,
             OD.dtlpassthrunum08,
             OD.dtlpassthrunum09,
             OD.dtlpassthrunum10,
             OD.dtlpassthruchar01,
             OD.dtlpassthruchar04,
             OD.dtlpassthruchar06,
             OD.dtlpassthruchar07,
             OD.dtlpassthruchar08,
             OD.dtlpassthruchar09,
             OD.dtlpassthruchar10,
             OD.dtlpassthruchar12,
             OD.dtlpassthruchar14,
             OD.dtlpassthruchar15,
             OD.dtlpassthruchar20,
             OD.dtlpassthrudate02,
             OD.dtlpassthrudate03,
             OD.dtlpassthrudate04,
             OD.dtlpassthrudoll01,
             OD.dtlpassthrudoll02,
             OB.bolcomment,
	     CI.battery_count custitembatterycount,
           CI.cell_count custitemcellcount,
           CI.lithium_weight custitemlithiumweight,
           CI.battery_watthrs custitembatterywatthrs,
          CI.iataprimarychemcode custitemiataprimarychemcode, 
          CI.primaryhazardclass  custitemprimaryhazardclass,
            OD.battery_count orderDTLbatterycount,
            OD.cell_count orderDTLcellcount,
            OD.lithium_weight orderDTLlithiumweight,
            OD.battery_watthrs orderDTLbatterywatthrs,
            OD.iataprimarychemcode orderDTLiataprimarychemcode
         from subtasks ST, custitem CI, orderdtl OD, plate PL, orderdtlbolcomments OB
         where ST.taskid = i_taskid           
           and nvl(ST.picktotype,'x') = 'LBL'
           and ST.tasktype ='BS'
           and CI.custid = ST.custid
           and CI.item = ST.item
           and OD.orderid = i_orderid
           and OD.shipid = i_shipid
           and OD.item = ST.orderitem
           and nvl(OD.lotnumber,'x') = nvl(ST.orderlot,'x')
           and PL.lpid (+) = ST.lpid
           and OB.orderid (+) = OD.orderid
           and OB.shipid (+) = OD.shipid
           and OB.item (+) = ST.orderitem
           and nvl(OB.lotnumber(+),'x') = nvl(ST.orderlot,'x');


END WVR_GETLABELSUBTASKSFORSINGLES_R_P;