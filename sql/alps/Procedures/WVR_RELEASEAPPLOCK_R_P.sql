CREATE OR REPLACE PROCEDURE WVR_RELEASEAPPLOCK_R_P (
 in_lockid IN varchar2
,in_facility IN varchar2
,in_userid IN varchar2
,out_msg  IN OUT varchar2
) AS
BEGIN

 
ALPS.zapplocks.release_app_lock(in_lockid,in_facility,in_userid,out_msg);

END WVR_RELEASEAPPLOCK_R_P;