CREATE OR REPLACE PROCEDURE WVR_CUSTCONNECTSHIP_R_P ( 
  i_custid IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

        select count(1) into o_rowsaffected
               from custconnectship
               where custid = i_custid;
EXCEPTION WHEN OTHERS THEN
    o_rowsaffected := 0;

END WVR_CUSTCONNECTSHIP_R_P;