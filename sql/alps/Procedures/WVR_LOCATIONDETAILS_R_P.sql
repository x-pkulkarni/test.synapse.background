CREATE OR REPLACE PROCEDURE WVR_LOCATIONDETAILS_R_P ( 
  i_facility IN VARCHAR2, 
  i_locid IN VARCHAR2,
  c_locdetails OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_locdetails FOR
    select section,
         equipprof,
         pickingzone,
         nvl(pickingseq,0) as pickingseq
    from location
   where facility = i_facility
     and locid = i_locid;

END WVR_LOCATIONDETAILS_R_P;





 