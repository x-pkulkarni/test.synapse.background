CREATE OR REPLACE PROCEDURE RGP_CUSTITEMVIEWISKITFLAG_R_P ( 
  i_orderitem IN VARCHAR2, 
  i_custid IN VARCHAR2, 
  o_iskit OUT number
) AS
BEGIN

    SELECT Decode(NVL(iskit, 'N'),'Y',1,0)  
            INTO o_iskit
    FROM custitemview
    WHERE custid = i_custid
            AND item = i_orderitem;

EXCEPTION WHEN OTHERS THEN
    o_iskit := 0;
END RGP_CUSTITEMVIEWISKITFLAG_R_P;