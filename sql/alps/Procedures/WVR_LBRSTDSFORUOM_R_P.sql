create or replace PROCEDURE WVR_LBRSTDSFORUOM_R_P ( 
  i_facility IN VARCHAR2,
  i_custid IN VARCHAR2,
  i_category IN VARCHAR2,
  i_zoneid IN VARCHAR2,
  i_uom IN VARCHAR2,
  c_qtyperhour OUT SYS_REFCURSOR
) AS
BEGIN
     OPEN c_qtyperhour FOR
     SELECT decode(nvl(qtyperhour,0),0,12,qtyperhour) AS qtyperhour, 
	uom
    	FROM laborstandards
   	WHERE facility = i_facility
     		and custid = i_custid
     		and category = i_category
     		and ( (nvl(zoneid,'x') = nvl(i_zoneid,'x')) or
           		( zoneid is null) )
     		and uom = i_uom
   	ORDER BY zoneid;

END WVR_LBRSTDSFORUOM_R_P;