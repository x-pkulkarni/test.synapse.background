CREATE OR REPLACE PROCEDURE WVU_MARKLOADASPLANNED_U_P (
  i_userid IN varchar2,
  i_loadno IN NUMBER,    
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  update loads
       set loadstatus = '2',
           lastuser = i_userid,
           lastupdate = sysdate
     where loadno = i_loadno
       and loadstatus > '2';

END WVU_MARKLOADASPLANNED_U_P;