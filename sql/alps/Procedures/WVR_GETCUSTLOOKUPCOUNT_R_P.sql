CREATE OR REPLACE PROCEDURE WVR_GETCUSTLOOKUPCOUNT_R_P(
i_custid IN VARCHAR2,
i_code IN VARCHAR2,
o_cust_lookup_count OUT NUMBER
) AS 
BEGIN

    select count(*)
        into o_cust_lookup_count
    from cust_lookup_dtl
    where custid = i_custid
        and tabletype='FELALLOWED'
        and code = i_code
        and value = 'Y';  
 
END WVR_GETCUSTLOOKUPCOUNT_R_P;
 
