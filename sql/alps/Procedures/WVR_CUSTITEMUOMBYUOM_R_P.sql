CREATE OR REPLACE PROCEDURE WVR_CUSTITEMUOMBYUOM_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_fromuom IN VARCHAR2,
  c_custitemuom OUT SYS_REFCURSOR
) AS
BEGIN
OPEN c_custitemuom FOR
    select fromuom, qty, touom
         from custitemuom
         where custid = i_custid
           and item = i_item
           and (fromuom = i_fromuom or touom = i_fromuom );

END WVR_CUSTITEMUOMBYUOM_R_P;