create or replace PROCEDURE WVU_SUBTASKSBYWAVEID_R_P ( 
  i_wave IN NUMBER, 	
  c_subtasks OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_subtasks FOR
    select ROWID,
         TASKID,
         CUSTID,
         FACILITY,
		 LOADNO,
		 SHIPNO,
		 STOPNO,
         LPID, 
         TASKTYPE,
         ORDERITEM,
         ITEM,
         ORDERLOT,
         QTY Quantity,
         UOM,
         PICKQTY,
         PICKUOM
    from SUBTASKS
   where wave = i_wave;

END WVU_SUBTASKSBYWAVEID_R_P;