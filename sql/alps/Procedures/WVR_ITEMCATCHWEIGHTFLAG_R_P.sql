CREATE OR REPLACE PROCEDURE WVR_ITEMCATCHWEIGHTFLAG_R_P (
i_custid IN varchar2,
i_item IN varchar2,
o_flag OUT number
) 
AS
BEGIN

  select Decode(NVL(use_catch_weights,'N'),'Y',1,0)
    into o_flag
    from custitemview
   where custid = i_custid
     and item = i_item;
exception
  when NO_DATA_FOUND then
    o_flag := 0;
  
END WVR_ITEMCATCHWEIGHTFLAG_R_P;