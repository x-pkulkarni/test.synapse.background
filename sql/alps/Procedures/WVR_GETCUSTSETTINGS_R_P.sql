CREATE OR REPLACE PROCEDURE WVR_GETCUSTSETTINGS_R_P(
  i_custid IN VARCHAR2,
  c_customer OUT SYS_REFCURSOR
) AS
BEGIN

OPEN c_customer FOR
     SELECT custid,
        Decode(NVL(useitemuncodefromorder_yn, 'N'),'Y',1,0) as IsUseitemuncodefromorder,                                               
        Decode(NVL(iata_sngl_unt_ok,'N'),'Y',1,0) as IsIataSingleUnitOk,
        Decode(NVL(iata_carrier_override,'N'),'Y',1,0) as IsIataCarrierOverride,
        NVL((carton_fill_percent/100), 1) AS CartonFillPercentage, 
        Decode(NVL((carton_fill_percent_yn), 'N'),'Y',1,0) AS IsCartonFillPercentageFlag,	
        Decode(NVL(carton_standard_yn ,'N'),'Y',1,0) as IsCartonEnabled,   
		Decode(NVL(fel_connectship, 'N'),'Y',1,0) as IsFelConnectship,
        NVL(standard_label_override_column,'hdrpassthruchar12') as StandardLabelOverrideColumn
  	FROM customer_aux
  	WHERE custid = i_custid;
 
END WVR_GETCUSTSETTINGS_R_P;
 
 
