CREATE OR REPLACE PROCEDURE WVU_ORDERHISTORY_I_P (
  i_orderid IN NUMBER,
  i_shipid IN NUMBER,
  i_action IN VARCHAR2,
  i_lpid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_user IN VARCHAR2,
  i_lotnumber IN VARCHAR2,
  i_msg IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

INSERT
  INTO ORDERHISTORY
    (
    	chgdate,
    	orderid,
	    shipid,
	    userid,
	    action,
      lpid,
      item,
      lot,
	    msg
    )
    VALUES
    (
      SYSDATE,
      i_orderid,
      i_shipid,
      i_user,
      i_action,
      i_lpid,
      i_item,
      i_lotnumber,
      i_msg
    );

  o_rowsaffected := SQL%ROWCOUNT;


END WVU_ORDERHISTORY_I_P;