CREATE OR REPLACE PROCEDURE WVR_PLATEBYLPID_R_P  (
  i_lpid IN VARCHAR2, 
  c_plate OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_plate FOR
  select lpid,
    custid as CustomerId,
    location,
    weight, 
    quantity, 
    unitofmeasure,
    item,  
    facility
    from plate
       where lpid = i_lpid OR location = i_lpid;

END WVR_PLATEBYLPID_R_P;
