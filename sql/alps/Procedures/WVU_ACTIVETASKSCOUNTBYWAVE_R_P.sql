create or replace PROCEDURE WVU_ACTIVETASKSCOUNTBYWAVE_R_P ( 
  i_wave IN NUMBER, 
  o_rowscount OUT NUMBER
) AS
BEGIN

    SELECT count(1)
      INTO o_rowscount
      FROM tasks
    WHERE wave = i_wave
       AND priority = '0';
EXCEPTION WHEN OTHERS THEN
    o_rowscount := 0;
END WVU_ACTIVETASKSCOUNTBYWAVE_R_P;