CREATE OR REPLACE PROCEDURE WVR_RELEASEORDER_U_P
(
i_orderstatus IN VARCHAR2,
i_commitstatus IN VARCHAR2,
i_priority IN VARCHAR2,
i_userid IN VARCHAR2,
i_orderid IN NUMBER,
i_shipid IN NUMBER,
o_rowsaffected OUT NUMBER
) AS 
BEGIN  

update orderhdr
   set orderstatus = i_orderstatus ,
       commitstatus = i_commitstatus,
       priority = i_priority,
       lastuser = i_userid,
       lastupdate = sysdate
 where orderid = i_orderid
   and shipid = i_shipid
   and orderstatus < i_orderstatus;

o_rowsaffected := SQL%ROWCOUNT;

END WVR_RELEASEORDER_U_P;
  