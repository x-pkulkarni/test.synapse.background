CREATE OR REPLACE PROCEDURE WVU_ORDERCOUNTBYLOADNO_R_P ( 
  i_loadno IN NUMBER,
  o_rowscount OUT NUMBER
) AS
BEGIN

    SELECT count(1)
      INTO o_rowscount
      FROM orderhdr
   WHERE loadno = i_loadno
     AND orderstatus > '2';
EXCEPTION WHEN OTHERS THEN
    o_rowscount := 0;

END WVU_ORDERCOUNTBYLOADNO_R_P;