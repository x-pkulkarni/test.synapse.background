CREATE OR REPLACE PROCEDURE WVR_CUSTITEMCUBE_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  c_cube OUT SYS_REFCURSOR
) AS
BEGIN
 OPEN c_cube FOR
    SELECT NVL(cube,0) as cube, baseuom 
	 FROM custitem
   	 WHERE custid = i_custid
     		AND item = i_item;

END WVR_CUSTITEMCUBE_R_P;