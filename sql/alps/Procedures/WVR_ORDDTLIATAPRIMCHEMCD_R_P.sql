CREATE OR REPLACE PROCEDURE WVR_ORDDTLIATAPRIMCHEMCD_R_P ( 
  i_orderid IN NUMBER,
  i_item IN VARCHAR2,
  i_shipid IN NUMBER,
  c_orderdtls OUT SYS_REFCURSOR
) AS
BEGIN
OPEN c_orderdtls FOR
      SELECT orderid, shipid, iataprimarychemcode
  		FROM orderdtl
  		WHERE orderid = i_orderid
  			AND   shipid = i_shipid
  			AND   item = i_item;

END WVR_ORDDTLIATAPRIMCHEMCD_R_P;