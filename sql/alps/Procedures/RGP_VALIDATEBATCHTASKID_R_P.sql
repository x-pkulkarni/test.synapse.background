CREATE OR REPLACE PROCEDURE RGP_VALIDATEBATCHTASKID_R_P ( 
  i_taskid IN NUMBER, 
  o_batchtaskid OUT NUMBER
) AS
BEGIN
	  
	   select taskid
        into o_batchtaskid
        from batchtasks
       where taskid = i_taskid
       group by taskid;

EXCEPTION WHEN OTHERS THEN
    o_batchtaskid := 0;
END RGP_VALIDATEBATCHTASKID_R_P;