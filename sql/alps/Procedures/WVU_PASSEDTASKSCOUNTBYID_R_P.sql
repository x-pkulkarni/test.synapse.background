create or replace PROCEDURE WVU_PASSEDTASKSCOUNTBYID_R_P ( 
  i_taskid IN NUMBER, 
  o_rowscount OUT NUMBER
) AS
BEGIN

    SELECT count(1)
      INTO o_rowscount
      FROM tasks
    WHERE taskid = i_taskid
       AND priority = '8';
EXCEPTION WHEN OTHERS THEN
    o_rowscount := 0;
END WVU_PASSEDTASKSCOUNTBYID_R_P;