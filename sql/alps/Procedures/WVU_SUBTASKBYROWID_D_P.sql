create or replace PROCEDURE WVU_SUBTASKBYROWID_D_P (
  i_rowid IN ROWID,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  DELETE 
    FROM SUBTASKS
    WHERE ROWID = i_rowid;
  o_rowsaffected := SQL%ROWCOUNT;


END WVU_SUBTASKBYROWID_D_P;