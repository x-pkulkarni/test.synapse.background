CREATE OR REPLACE PROCEDURE WVR_TASKSTOUSERID_U_P
(
i_taskid IN NUMBER
) AS 
BEGIN  

    update tasks
      set touserid = zfel.ID_TOUSER
      where taskid = i_taskid
      and touserid is null;

END WVR_TASKSTOUSERID_U_P;
  