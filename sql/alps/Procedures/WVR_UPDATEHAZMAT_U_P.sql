CREATE OR REPLACE PROCEDURE WVR_UPDATEHAZMAT_U_P
(
i_orderid IN NUMBER,
i_shipid IN NUMBER
) AS 
BEGIN  

     UPDATE connectshiphdr
              SET hazmat = 'Y'
              WHERE orderid = i_orderid
              AND   shipid  = i_shipid
              AND   hazmat  = 'N';

END WVR_UPDATEHAZMAT_U_P;
  