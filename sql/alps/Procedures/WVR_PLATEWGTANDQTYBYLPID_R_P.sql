CREATE OR REPLACE PROCEDURE WVR_PLATEWGTANDQTYBYLPID_R_P ( 
  i_lpid IN VARCHAR2, 
  c_plate OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_plate FOR
  select 
    weight, 
    quantity, 
    unitofmeasure
    item,  
    lpid
    from plate
       where lpid = i_lpid;
			 
END WVR_PLATEWGTANDQTYBYLPID_R_P;