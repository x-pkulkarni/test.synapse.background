CREATE OR REPLACE PROCEDURE WVR_GETCUSTLOOKUPDTL_R_P(
i_custid IN VARCHAR2,
i_ship_to_country_code IN VARCHAR2,
c_cust_lookup_dtl OUT SYS_REFCURSOR
) AS 
hazmatctcode cust_lookup_dtl.code%type;
hazmatinvalidst  cust_lookup_dtl.value%type;
BEGIN
	
	begin
		select nvl(code,'X')  , 
			nvl(value,'X')  
            into hazmatctcode,hazmatinvalidst
		from cust_lookup_dtl
		where custid = i_custid
			and tabletype='HAZMATINVALIDST'
			and code = i_ship_to_country_code;  
	exception when no_data_found then
     hazmatctcode := 'X';
     hazmatinvalidst := 'X';
   end; 
   
    OPEN c_cust_lookup_dtl FOR
    select hazmatctcode as hazmatctcode, hazmatinvalidst as hazmatinvalidst from DUAL;
    
END WVR_GETCUSTLOOKUPDTL_R_P;
 
