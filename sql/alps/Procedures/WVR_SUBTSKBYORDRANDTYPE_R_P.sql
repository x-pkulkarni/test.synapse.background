create or replace PROCEDURE WVR_SUBTSKBYORDRANDTYPE_R_P ( 
  i_wave IN NUMBER,  
  i_orderid IN NUMBER,  
  i_shipid IN NUMBER,  
  i_tasktype IN VARCHAR2, 
  c_subtasks OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_subtasks FOR
      select rowid,
         taskid,
         TaskType,
         FACILITY,
         FROMSECTION,
         FROMLOC as FROMLOCATION,
         FROMPROFILE,
         TOSECTION,
         TOLOC as TOLOCATION,
         TOPROFILE,
         CUSTID,
         ITEM,
         LPID,
         UOM,         
         QTY as Quantity,
         LOCSEQ,
		 LOADNO,
		 STOPNO,
		 SHIPNO,
         ORDERID,
         SHIPID,
         OrderItem,
         OrderLOT,         
         PRIORITY,
         PREVPRIORITY,
         LastUser,         
         PickUom,         
         WAVE,
         PICKINGZONE,
         CartonType,
		 PICKTOTYPE,
         WEIGHT,
         CUBE,
         STAFFHRS,
         CartonSeq,         
         ShippingType,
         CartonGroup,
         LabelUom,         
         PICKQTY,
         shippinglpid
    from subtasks
    where WAVE = i_wave 
            AND orderid = i_orderid
            AND shipid = i_shipid
            AND TASKTYPE = i_tasktype;

END WVR_SUBTSKBYORDRANDTYPE_R_P;

