CREATE OR REPLACE PROCEDURE WVR_TASKBYORDERIDTSKTYPE_D_P (
  i_orderid IN NUMBER,  
  i_shipid IN NUMBER,  
  i_tasktype IN VARCHAR2,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  delete from tasks
     where orderid = i_orderid
            AND shipid = i_shipid
            AND tasktype = i_tasktype;

  o_rowsaffected := SQL%ROWCOUNT;

END WVR_TASKBYORDERIDTSKTYPE_D_P;