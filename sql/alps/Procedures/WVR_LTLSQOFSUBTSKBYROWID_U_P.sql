CREATE OR REPLACE PROCEDURE WVR_LTLSQOFSUBTSKBYROWID_U_P (
  i_ltlseq IN NUMBER,  
  i_lastuser IN VARCHAR2,
  i_rowid IN rowid,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

UPDATE SUBTASKS
  SET ltl_fel_seq  = i_ltlseq,
    LASTUSER   = i_lastuser,
    LASTUPDATE = SYSDATE
  WHERE ROWID = i_rowid;

  o_rowsaffected := SQL%ROWCOUNT;


END WVR_LTLSQOFSUBTSKBYROWID_U_P;