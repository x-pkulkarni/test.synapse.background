create or replace PROCEDURE WVU_CONNECTSHIP_U_P (
  i_lpid IN VARCHAR2, 
  i_user IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

         update connectshiphdr
            set opcode = 'CANCEL',
              cancelledby = i_user
          where lpid = pst.lpid;
  o_rowsaffected := SQL%ROWCOUNT;


END WVU_CONNECTSHIP_U_P;