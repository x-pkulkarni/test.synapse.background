create or replace PROCEDURE WVR_FELWAVEITEMS_R_P ( 
  i_wave IN NUMBER, 
  c_citems OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_citems FOR
	select distinct 
                 CI.custid as CustomerId, 
                 CI.item,
                 od.qtyentered as qtyentered,
                (OH.carrier || ' ' || OH.deliveryservice) as CarrierCode,
                 OH.orderid as orderid,OH.shipid as shipid,   
                 OH.shiptostate,
                 OH.shiptocountrycode,
                 nvl(CI.picksetqty,0) as PicksetQty,
                 nvl(CI.hazardous,'N') as hazardous,
                 nvl(CA.multiship,'N') as multiship
               from orderhdr OH, orderdtl OD, custitem CI, carrier CA
               where OH.wave = i_wave
                and OH.orderstatus != 'X'
                and OD.orderid = OH.orderid
                and OD.shipid = OH.shipid
                and OD.linestatus != 'X'
                and CI.custid = OD.custid
                and CI.item = OD.item                
                and CA.carrier (+) = OH.carrier
                and not exists
                    (select allow_hazmat
                       from custconnectship CC
                      where CC.custid = OH.custid
                        and CC.carrier = OH.carrier
                        and CC.allow_hazmat = 'Y'
                    )

END WVR_FELWAVEITEMS_R_P;