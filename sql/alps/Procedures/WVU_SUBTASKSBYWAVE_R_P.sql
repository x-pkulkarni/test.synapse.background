create or replace PROCEDURE WVU_SUBTASKSBYWAVE_R_P ( 
  i_wave IN NUMBER, 
	i_tasktype IN VARCHAR2, 
  c_subtasks OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_subtasks FOR
    select ROWID,
         TASKID,
         CUSTID,
         FACILITY,
		 LOADNO,
		 SHIPNO,
		 STOPNO,
         LPID, 
         TASKTYPE,
         ORDERITEM,
         ITEM,
         ORDERLOT
    from SUBTASKS
   where wave = i_wave
     and ((TASKTYPE is NULL AND i_tasktype is NULL ) OR TASKTYPE = i_tasktype);

END WVU_SUBTASKSBYWAVE_R_P;