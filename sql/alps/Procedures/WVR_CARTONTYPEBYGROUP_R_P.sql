create or replace PROCEDURE WVR_CARTONTYPEBYGROUP_R_P ( 
  i_group IN VARCHAR2,
  o_cartontype OUT VARCHAR2
) AS
BEGIN

     select code
  		into o_cartontype 
  	from cartongroups
	where cartongroup = i_group 
   		and rownum < 2;
	EXCEPTION WHEN OTHERS THEN
  		o_cartontype := '';


END WVR_CARTONTYPEBYGROUP_R_P;