CREATE OR REPLACE PROCEDURE RGP_COMMITMENTQTY_U_P
(
i_orderid IN NUMBER,
i_shipid IN NUMBER,
i_qty IN NUMBER,
i_orderitem IN VARCHAR2,
i_item IN VARCHAR2,
i_orderlot IN VARCHAR2,
i_lotnumber IN VARCHAR2,
i_inventoryclass IN VARCHAR2,
i_invstatus IN VARCHAR2,
o_quantity OUT NUMBER
) AS 
BEGIN  

    update commitments
        set qty = qty - i_qty
      where orderid = i_orderid
        and shipid = i_shipid
        and orderitem = i_orderitem
        and nvl(orderlot,'(none)') = nvl(i_orderlot,'(none)')
        and item = i_item
        and nvl(lotnumber,'(none)') = nvl(i_lotnumber,'(none)')
        and inventoryclass = i_inventoryclass
        and invstatus = i_invstatus
      returning qty into o_quantity;

END RGP_COMMITMENTQTY_U_P;
  