CREATE OR REPLACE PROCEDURE WVR_PARCELSTAGINGBYWAVE_R_P ( 
  i_wave IN NUMBER,
  i_orderid IN NUMBER, 
  i_shipid IN NUMBER, 
  c_parcelstaging OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_parcelstaging FOR
    select PS.lpid,
      PS.facility,
      PS.custid,
      PS.rowid,
      CF.canmap,
	  CX.fel_connectship,
      nvl(CA.multiship,'N') as multiship
    from parcelstagingtable PS, custfrontendlabel CF, customer_aux CX,
      orderhdr OH, carrier CA
    where PS.wave = i_wave
      and PS.orderid = i_orderid
      and PS.shipid = i_shipid
      and PS.status not in ('C', 'W')
      and CF.custid (+) = PS.custid
      and CF.facility (+) = PS.facility
      and CF.carrier (+) = PS.carrier
	  and CX.custid (+) = PS.custid
      and OH.orderid = PS.orderid
      and OH.shipid = PS.shipid
      and CA.carrier (+) = OH.carrier;

END WVR_PARCELSTAGINGBYWAVE_R_P;