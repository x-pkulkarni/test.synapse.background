CREATE OR REPLACE PROCEDURE WVU_LOADSTATUSBYLOADNO_U_P (
  i_userid IN varchar2,
  i_loadno IN NUMBER, 
  i_status IN varchar2,     
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  update loads
       set loadstatus = i_status,
           lastuser = i_userid,
           lastupdate = sysdate
     where loadno = i_loadno
       and loadstatus < i_status;

  o_rowsaffected := SQL%ROWCOUNT;

END WVU_LOADSTATUSBYLOADNO_U_P;