CREATE OR REPLACE PROCEDURE RGP_TASKBYIDANDFACILITY_D_P (
  i_facility IN VARCHAR2,  
  i_taskid IN NUMBER,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  delete from tasks
        where facility = i_facility
          and taskid = i_taskid;

  o_rowsaffected := SQL%ROWCOUNT;


END RGP_TASKBYIDANDFACILITY_D_P;