CREATE OR REPLACE PROCEDURE WVR_CSHIPORDERHEADER_R_P ( 
  i_wave IN NUMBER, 
  c_orderHeader OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_orderHeader FOR
    select OH.orderid, OH.shipid, 
      Decode(nvl(CA.multiship,'N'),'Y',1,0) as ismultiship,
      Decode(nvl(CX.fel_connectship,'N'),'Y',1,0) as isfelconnectship,
      Decode(nvl(CX.ltl_fel,'N'),'Y',1,0) as isltlfel
      from orderhdr OH, customer_aux CX, carrier CA
      where OH.wave = i_wave
        and OH.orderstatus != 'X'
        and CX.custid (+) = OH.custid
        and CA.carrier (+) = OH.carrier;

END WVR_CSHIPORDERHEADER_R_P;



 