CREATE OR REPLACE PROCEDURE WVR_ORDERHEADERBYORDERID_R_P ( 
  i_orderid IN NUMBER, 
  i_shipid IN NUMBER, 
  c_orderHeader OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_orderHeader FOR
    SELECT ORDERID,
    	SHIPID,
    	CUSTID AS CUSTOMERID,
    	ORDERTYPE,
    	ORDERSTATUS,
    	COMMITSTATUS,
    	FROMFACILITY,
    	TOFACILITY,
    	LOADNO,
    	STOPNO,
   		SHIPNO,
   		SHIPTO,
    	STAGELOC,
    	WAVE,
    	PRIORITY,
    	LASTUSER,
		CARRIER,
		SHIPTYPE,
         QTYPICK,
		 COMPONENTTEMPLATE
	FROM 
    ORDERHDR 
	WHERE ORDERID = i_orderid
		AND SHIPID = i_shipid;

END WVR_ORDERHEADERBYORDERID_R_P;



 