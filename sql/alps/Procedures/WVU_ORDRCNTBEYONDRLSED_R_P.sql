CREATE OR REPLACE PROCEDURE WVU_ORDRCNTBEYONDRLSED_R_P ( 
  i_wave IN NUMBER, 
  o_rowscount OUT NUMBER
) AS
BEGIN

     SELECT COUNT(1)
      INTO o_rowscount
      FROM orderhdr
    WHERE wave = i_wave
        AND orderstatus > '4'
        AND orderstatus < 'X';
EXCEPTION WHEN OTHERS THEN
    o_rowscount := 1;
END WVU_ORDRCNTBEYONDRLSED_R_P;