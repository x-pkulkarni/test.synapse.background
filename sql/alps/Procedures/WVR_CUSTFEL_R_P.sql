CREATE OR REPLACE PROCEDURE WVR_CUSTFEL_R_P ( 
  i_custid IN VARCHAR2,
  o_rowscount OUT NUMBER
) AS
BEGIN

        select count(1) into o_rowscount
               from custfrontendlabel
               where custid = i_custid;
EXCEPTION WHEN OTHERS THEN
    o_rowscount := 0;

END WVR_CUSTFEL_R_P;