create or replace PROCEDURE WVR_SENDIEREQUEST_I_P
(   
    in_lpid in VARCHAR2,
	in_edimap in  VARCHAR2,
    v_errno out number,
    v_errmsg OUT VARCHAR2
) AS
v_errno number;
v_errmsg VARCHAR2(255);
BEGIN

 ziem.impexp_request_na('E', null, null, in_edimap, null, 'NOW', 0, 0, 0,
               in_lpid, null, null, null, null, null, null, null, l_errno => v_errno, l_msg => v_errmsg);
      
    
END WVR_SENDIEREQUEST_I_P;