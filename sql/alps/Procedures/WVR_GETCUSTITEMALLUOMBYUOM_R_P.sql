CREATE OR REPLACE PROCEDURE WVR_GETCUSTITEMALLUOMBYUOM_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  c_custitemuom OUT SYS_REFCURSOR
) AS
BEGIN
OPEN c_custitemuom FOR
    select  CIU.sequence,CIU.fromuom, CIU.qty, CIU.touom
         from custitemuom CIU
         where custid = i_custid
           and item = i_item
		 ORDER by sequence;

END WVR_GETCUSTITEMALLUOMBYUOM_R_P;