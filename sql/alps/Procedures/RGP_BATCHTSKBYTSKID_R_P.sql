CREATE OR REPLACE PROCEDURE RGP_BATCHTSKBYTSKID_R_P (
  i_taskid IN NUMBER,
  i_facility IN VARCHAR2, 
  c_batchtasks OUT SYS_REFCURSOR
) AS
BEGIN

   
 OPEN c_batchtasks FOR
          SELECT BT.TASKID,
                 BT.TASKTYPE,
                 BT.FACILITY,
                 BT.FROMLOC as FromLocation,
                 BT.CUSTID AS CustomerId,
                 BT.ITEM,
                 nvl(BT.lpid,'(none)') as lpid,
                 BT.UOM,
                 BT.QTY as Quantity,
                 BT.ORDERID,
                 BT.SHIPID,
                 BT.ORDERITEM,
                 BT.ORDERLOT,
                 BT.PICKUOM,
                 BT.PICKQTY,
                 BT.PICKTOTYPE,
                 BT.WAVE,
                 BT.INVSTATUS,
                 BT.INVENTORYCLASS,
                 BT.QTYTYPE,
                 BT.CARTONTYPE
            FROM BATCHTASKS BT
           WHERE BT.FACILITY = i_facility AND BT.TASKID = i_taskid
             ORDER BY BT.ORDERID,BT.SHIPID;
  
  END RGP_BATCHTSKBYTSKID_R_P;