create or replace PROCEDURE WVR_GETPACKSUBTASKSDETAILFORSINGLES_R_P ( 
  i_orderid IN NUMBER, 
  i_shipid IN NUMBER, 
  i_taskid IN NUMBER,
  c_pak OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_pak FOR
 select ST.taskid,
             ST.pickqty,
             ST.item,
             ST.orderitem,
             ST.orderlot,
             ST.weight,
             ST.pickuom,
             ST.cartontype,
             ST.uom,
             ST.cartonseq,
             ST.qty,
             ST.rowid as strowid,
             ST.cpack_job_id cpackjobid,  -- DR-5483 [DMB] 03-APR-2017
             CT.abbrev as containertype,
             (CASE CT.code WHEN 'SNGL' THEN CI.length ELSE CT.length END) as containerlength,
             (CASE CT.code WHEN 'SNGL' THEN CI.width ELSE CT.width END) as containerwidth,
             (CASE CT.code WHEN 'SNGL' THEN CI.height ELSE CT.height END) as containerheight,
             (CASE CT.code WHEN 'SNGL' THEN 0 ELSE CT.cartonweight END) as containerweight,
             CI.descr,
             OD.qtyorder,
             PL.countryof,
-- syn-259: add location to connectshiphdr
-- add fromloc to cursor
             ST.fromloc,
-- syn-2799: add the following 12 columns for apple care returns
             OD.dtlpassthruchar02,
             OD.dtlpassthruchar03,
             OD.dtlpassthruchar05,
             OD.dtlpassthruchar11,
             OD.dtlpassthruchar13,
             OD.dtlpassthruchar16,
             OD.dtlpassthruchar17,
             OD.dtlpassthruchar18,
             OD.dtlpassthruchar19,
             OD.dtlpassthrunum04,
             OD.dtlpassthrudate01,
--2014-05-19 [MZ] SYN-3931: Add new fields to connectshiphdr table for use as return weight for FEL orders - BM
             OD.dtlpassthrunum01,
             OD.dtlpassthrunum02,
             OD.dtlpassthrunum03,
             OD.dtlpassthrunum05,
             nvl(OD.dtlpassthrunum06,1) dtlpassthrunum06,
             OD.dtlpassthrunum07,
             OD.dtlpassthrunum08,
             OD.dtlpassthrunum09,
             OD.dtlpassthrunum10,
             OD.dtlpassthruchar01,
             OD.dtlpassthruchar04,
             OD.dtlpassthruchar06,
             OD.dtlpassthruchar07,
             OD.dtlpassthruchar08,
             OD.dtlpassthruchar09,
             OD.dtlpassthruchar10,
             OD.dtlpassthruchar12,
             OD.dtlpassthruchar14,
             OD.dtlpassthruchar15,
             OD.dtlpassthruchar20,
             OD.dtlpassthrudate02,
             OD.dtlpassthrudate03,
             OD.dtlpassthrudate04,
             OD.dtlpassthrudoll01,
             OD.dtlpassthrudoll02,
             OB.bolcomment,
--2015-05-04 [BK] DR-4371: Add new fields to connectshiphdtl
          NVL(OD.HTC,CI.NMFC_ARTICLE) as HARMONIZEDCODE,
           CC.PROPERSHIPPINGNAME1 AS HAZMATDESCRIPTION,
           CC.UNNUM AS HAZMATID,
           CC.CHEMICALCONSTITUENTS AS HAZMATTECHNICALNAME,
           CC.PACKINGGROUP AS HAZMATPACKINGGROUP,
           CC.ABBREV AS HAZMATABBREV,
           CI.PrimaryHazardClass HAZMATCLASS,
--2015-07-24 [BK] DR-4600: Add two more fields to view for connectship(hdr)
           ST.pickingzone,
           (select aisle from location where locid = st.fromloc and facility = st.facility) as aisle,
             CI.battery_count custitembatterycount,
           CI.cell_count custitemcellcount,
           CI.lithium_weight custitemlithiumweight,
           CI.battery_watthrs custitembatterywatthrs,
             CI.iataprimarychemcode custitemiataprimarychemcode,  --DR 4978 ends
          -- DR-5494 2017-04-04 [IB] Apple Request- AppleCare FY17 Battery Shipping Compliance - begin
          OD.battery_count orderDTLbatterycount,
          OD.cell_count orderDTLcellcount,
          OD.lithium_weight orderDTLlithiumweight,
          OD.battery_watthrs orderDTLbatterywatthrs,
          OD.iataprimarychemcode orderDTLiataprimarychemcode
          -- DR-5494 2017-04-04 [IB] Apple Request- AppleCare FY17 Battery Shipping Compliance - end
         from subtasks ST, cartontypes CT, custitem CI, orderdtl OD, plate PL,
              orderdtlbolcomments OB, CHEMICALCODES CC
         where ST.taskid = i_taskid           
           and nvl(ST.picktotype,'x') = 'PACK'
           and ST.tasktype ='BS'
           and CT.code (+) = ST.cartontype
           and CI.custid = ST.custid
           and CI.item = ST.item
           and OD.orderid = i_orderid
           and OD.shipid = i_shipid
           and OD.item = ST.orderitem
           and nvl(OD.lotnumber,'x') = nvl(ST.orderlot,'x')
           and PL.lpid (+) = ST.lpid
           and OB.orderid (+) = OD.orderid
           and OB.shipid (+) = OD.shipid
           and OB.item (+) = ST.orderitem
           and nvl(OB.lotnumber(+),'x') = nvl(ST.orderlot,'x')
           and CC.CHEMCODE(+) = CI.PRIMARYCHEMCODE
         order by ST.taskid, ST.cartontype, ST.cartonseq, ST.item;

END WVR_GETPACKSUBTASKSDETAILFORSINGLES_R_P;