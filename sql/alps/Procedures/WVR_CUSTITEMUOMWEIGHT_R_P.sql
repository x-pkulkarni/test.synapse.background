CREATE OR REPLACE PROCEDURE WVR_CUSTITEMUOMWEIGHT_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_uom IN VARCHAR2,
  o_weight OUT NUMBER
) AS
BEGIN

    SELECT NVL(WEIGHT,0) INTO o_weight
	 FROM custitemuom
   	 WHERE custid = i_custid
     		AND item = i_item
     		AND touom = i_uom;
EXCEPTION WHEN OTHERS THEN
  		o_weight:=0;

END WVR_CUSTITEMUOMWEIGHT_R_P;