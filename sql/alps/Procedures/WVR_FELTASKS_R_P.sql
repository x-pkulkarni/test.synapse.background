CREATE OR REPLACE PROCEDURE  WVR_FELTASKS_R_P ( 
  i_orderid IN NUMBER, 
  i_shipid IN NUMBER, 
  c_felTasks OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_felTasks FOR
    select ST.taskid,
             ST.pickqty,
             ST.facility,
             ST.custid,
             ST.item,
             ST.wave,
             CF.meternumber,
             CF.edimap,
             CF.carrier,
             OH.deliveryservice
         from tasks T, subtasks ST, orderhdr OH,
              custfrontendlabel CF, carrier CA
         where ST.orderid = i_orderid
           and ST.shipid = i_shipid
           and T.taskid = ST.taskid
           and (nvl(ST.picktotype,'x') = 'LBL'
              or (nvl(ST.picktotype,'x') = 'PACK' and T.pickqty = 1))
           and ST.tasktype = 'PK'
           and OH.orderid = ST.orderid
           and OH.shipid = ST.shipid
           and CF.custid = ST.custid
           and CF.facility = ST.facility
           and CF.carrier = OH.carrier
           and CA.carrier = OH.carrier
           and nvl(CA.multiship,'N') = 'Y';

END WVR_FELTASKS_R_P;