CREATE OR REPLACE PROCEDURE WVU_TASKBYTASKID_D_P (
  i_taskid IN NUMBER,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  delete from tasks
     where taskid = i_taskid;

  o_rowsaffected := SQL%ROWCOUNT;

END WVU_TASKBYTASKID_D_P;