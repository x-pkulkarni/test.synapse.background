
/* Sql Scripts - 1/22/2019 11:34:00 AM */


--###############################
--##  Tables
--###############################



--###############################
--##  Indexes
--###############################



--###############################
--##  Permissions
--###############################



--###############################
--##  Views
--###############################



--###############################
--##  Functions
--###############################



--###############################
--##  Procedures
--###############################


--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\RGP_BATCHTASKSBYTSKID_D_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE RGP_BATCHTASKSBYTSKID_D_P (
  i_taskid IN NUMBER,  
  i_facility IN VARCHAR2,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

delete batchtasks
      where facility = i_facility
       and taskid = i_taskid;

  o_rowsaffected := SQL%ROWCOUNT;


END RGP_BATCHTASKSBYTSKID_D_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\RGP_BATCHTSKBYTSKID_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE RGP_BATCHTSKBYTSKID_R_P (
  i_taskid IN NUMBER,
  i_facility IN VARCHAR2, 
  c_batchtasks OUT SYS_REFCURSOR
) AS
BEGIN

   
 OPEN c_batchtasks FOR
          SELECT BT.TASKID,
                 BT.TASKTYPE,
                 BT.FACILITY,
                 BT.FROMLOC as FromLocation,
                 BT.CUSTID AS CustomerId,
                 BT.ITEM,
                 nvl(BT.lpid,'(none)') as lpid,
                 BT.UOM,
                 BT.QTY as Quantity,
                 BT.ORDERID,
                 BT.SHIPID,
                 BT.ORDERITEM,
                 BT.ORDERLOT,
                 BT.PICKUOM,
                 BT.PICKQTY,
                 BT.PICKTOTYPE,
                 BT.WAVE,
                 BT.INVSTATUS,
                 BT.INVENTORYCLASS,
                 BT.QTYTYPE,
                 BT.CARTONTYPE
            FROM BATCHTASKS BT
           WHERE BT.FACILITY = i_facility AND BT.TASKID = i_taskid
             ORDER BY BT.ORDERID,BT.SHIPID;
  
  END RGP_BATCHTSKBYTSKID_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\RGP_COMMITMENTQTY_U_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE RGP_COMMITMENTQTY_U_P
(
i_orderid IN NUMBER,
i_shipid IN NUMBER,
i_qty IN NUMBER,
i_orderitem IN VARCHAR2,
i_item IN VARCHAR2,
i_orderlot IN VARCHAR2,
i_lotnumber IN VARCHAR2,
i_inventoryclass IN VARCHAR2,
i_invstatus IN VARCHAR2,
o_quantity OUT NUMBER
) AS 
BEGIN  

    update commitments
        set qty = qty - i_qty
      where orderid = i_orderid
        and shipid = i_shipid
        and orderitem = i_orderitem
        and nvl(orderlot,'(none)') = nvl(i_orderlot,'(none)')
        and item = i_item
        and nvl(lotnumber,'(none)') = nvl(i_lotnumber,'(none)')
        and inventoryclass = i_inventoryclass
        and invstatus = i_invstatus
      returning qty into o_quantity;

END RGP_COMMITMENTQTY_U_P;
  
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\RGP_CUSTITEMSUBBYITEM_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE RGP_CUSTITEMSUBBYITEM_R_P ( 
  i_orderitem IN VARCHAR2, 
  i_custid IN NUMBER, 
  c_custitemsub OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_custitemsub FOR
    SELECT itemsub,
         seq
    FROM custitemsubs
   	WHERE custid = i_custid
     		AND item = i_orderitem
   	ORDER BY 2,1;

END RGP_CUSTITEMSUBBYITEM_R_P;



 
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\RGP_CUSTITEMVIEWISKITFLAG_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE RGP_CUSTITEMVIEWISKITFLAG_R_P ( 
  i_orderitem IN VARCHAR2, 
  i_custid IN VARCHAR2, 
  o_iskit OUT VARCHAR2
) AS
BEGIN

    SELECT iskit 
            INTO o_iskit
    FROM custitemview
    WHERE custid = i_custid
            AND item = i_orderitem;

EXCEPTION WHEN OTHERS THEN
    o_iskit := '';
END RGP_CUSTITEMVIEWISKITFLAG_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\RGP_ITEMDEMAND_I_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE RGP_ITEMDEMAND_I_P (
  i_facility IN VARCHAR2,
  i_item IN VARCHAR2,
  i_lotnumber IN VARCHAR2,
  i_priority IN VARCHAR2,
  i_invstatusind IN VARCHAR2,
  i_invclassind IN VARCHAR2,
  i_invstatus IN VARCHAR2,
  i_inventoryclass IN VARCHAR2,
  i_demandtype IN VARCHAR2,
  i_orderid IN NUMBER,
  i_shipid IN NUMBER,
  i_loadno IN NUMBER,
  i_stopno IN NUMBER,
  i_shipno IN NUMBER,
  i_orderitem IN VARCHAR2,
  i_orderlot IN VARCHAR2,
  i_qty IN NUMBER,
  i_userid IN VARCHAR2,
  i_custid IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

insert into ITEMDEMAND
  (
      FACILITY,
      ITEM,
      LOTNUMBER,
      PRIORITY,
      INVSTATUSIND,
      INVCLASSIND,
      INVSTATUS,
      INVENTORYCLASS,
      DEMANDTYPE,
      ORDERID,
      SHIPID,
      LOADNO,
      STOPNO,
      SHIPNO,
      ORDERITEM,
      ORDERLOT,
      QTY,
      LASTUSER,
      LASTUPDATE,
      CUSTID
  )
  values
  (
      i_facility,
      i_item,
      i_lotnumber,
      i_priority,
      i_invstatusind,
      i_invclassind,
      i_invstatus,
      i_inventoryclass,
      i_demandtype,
      i_orderid,
      i_shipid,
      i_loadno,
      i_stopno,
      i_shipno,
      i_orderitem,
      i_orderlot,
      i_qty,
      i_userid,
      sysdate,
      i_custid
  );

  o_rowsaffected := SQL%ROWCOUNT;


END RGP_ITEMDEMAND_I_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\RGP_PLATEQTYBYBTCHLPID_U_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE RGP_PLATEQTYBYBTCHLPID_U_P (
  i_batchqty IN NUMBER,  
  i_lpid IN VARCHAR2,
  i_facility IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

UPDATE PLATE
  SET QTYTASKED  =  CASE 
                      WHEN (nvl(qtytasked,0) - i_batchqty) > 0 THEN 
                          (nvl(qtytasked,0) - i_batchqty) 
                      ELSE 
                        NULL 
                      END
  WHERE LPID = i_lpid
		and facility = i_facility
        and type = 'PA';

  o_rowsaffected := SQL%ROWCOUNT;


END RGP_PLATEQTYBYBTCHLPID_U_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\RGP_RESETBATCHTASKS_U_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE RGP_RESETBATCHTASKS_U_P (
  i_taskid IN NUMBER,  
  i_facility IN VARCHAR2,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

update batchtasks
        set taskid = 0,
            facility = null,
            lpid = null
      where facility = i_facility
       and taskid = i_taskid;

  o_rowsaffected := SQL%ROWCOUNT;


END RGP_RESETBATCHTASKS_U_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\RGP_SUBTSKLPIDBYTASKID_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE RGP_SUBTSKLPIDBYTASKID_R_P ( 
  i_taskid IN NUMBER, 
  i_facility IN VARCHAR2, 
  c_subtasks OUT SYS_REFCURSOR
) AS
BEGIN
	  
 OPEN c_subtasks FOR
	 select lpid, sum(qty) as Quantity
        from subtasks
      where facility = i_facility
        and tasktype = 'BP'
        and taskid = i_taskid
      group by lpid
      order by lpid;

END RGP_SUBTSKLPIDBYTASKID_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\RGP_TASKBYIDANDFACILITY_D_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE RGP_TASKBYIDANDFACILITY_D_P (
  i_facility IN VARCHAR2,  
  i_taskid IN NUMBER,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  delete from tasks
        where facility = i_facility
          and taskid = i_taskid;

  o_rowsaffected := SQL%ROWCOUNT;


END RGP_TASKBYIDANDFACILITY_D_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\RGP_VALIDATEBATCHTASKID_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE RGP_VALIDATEBATCHTASKID_R_P ( 
  i_taskid IN NUMBER, 
  o_batchtaskid OUT NUMBER
) AS
BEGIN
	  
	   select taskid
        into o_batchtaskid
        from batchtasks
       where taskid = i_taskid
       group by taskid;

EXCEPTION WHEN OTHERS THEN
    o_batchtaskid := 0;
END RGP_VALIDATEBATCHTASKID_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_BATCHTASKBYWAVE_R_P.SQL
--*****************************************************

create or replace PROCEDURE WVR_BATCHTASKBYWAVE_R_P ( 
  i_wave IN NUMBER, 
  c_bactchtasks OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_bactchtasks FOR
   select custid AS CustomerId,
         orderitem,
         item,
         orderlot,
         nvl(lpid,'(none)') as lpid,
         fromloc AS FromLocation,
         invstatus,
         inventoryclass,
         uom,
         pickuom,
         picktotype,
         facility,
         cartontype,
         qtytype,
         pickingzone,
         sum(qty) as Quantity,
         sum(pickqty) as pickqty
    from batchtasks
   where wave = i_wave
     and taskid = 0
     and tasktype = 'BP'
   group by custid,orderitem,item,orderlot,lpid,fromloc,invstatus,
         inventoryclass,uom,pickuom,picktotype,facility,cartontype,qtytype,pickingzone
   order by custid,orderitem,item,orderlot,lpid,fromloc,invstatus,
         inventoryclass,uom,pickuom,picktotype,facility,cartontype,qtytype,pickingzone;

END WVR_BATCHTASKBYWAVE_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_BATCHTASKID_U_P.SQL
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_BATCHTASKID_U_P (
  i_taskid IN NUMBER,  
  i_lastuser IN VARCHAR2,
  i_wave IN NUMBER,  
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_orderitem IN VARCHAR2,
  i_orderlot IN VARCHAR2,
  i_lpid IN VARCHAR2,
  i_fromloc IN VARCHAR2,
  i_invstatus IN VARCHAR2,
  i_inventoryclass IN VARCHAR2,
  i_uom IN VARCHAR2,
  i_pickuom IN VARCHAR2,
  i_picktotype IN VARCHAR2,
  i_facility IN VARCHAR2,
  i_cartontype IN VARCHAR2,
  i_qtytype IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

UPDATE BATCHTASKS
  SET TASKID  = i_taskid,
    LASTUSER   = i_lastuser,
    LASTUPDATE = SYSDATE
  WHERE WAVE= i_wave AND custid = i_custid AND item = i_item AND orderitem = i_orderitem AND 
        nvl(orderlot,'(none)') = nvl(i_orderlot,'(none)')
        AND lpid = i_lpid AND fromloc = i_fromloc AND invstatus = i_invstatus AND inventoryclass = i_inventoryclass
        AND uom = i_uom AND pickuom = i_pickuom AND picktotype = i_picktotype AND facility = i_facility
        AND cartontype = i_cartontype AND qtytype = i_qtytype;

  o_rowsaffected := SQL%ROWCOUNT;


END WVR_BATCHTASKID_U_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_BATCHTASK_I_P.SQL
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_BATCHTASK_I_P (
  i_taskid IN NUMBER,
  i_tasktype IN VARCHAR2,
  i_facility IN VARCHAR2,
  i_fromlocation IN VARCHAR2,
  i_tosection IN VARCHAR2,
  i_tolocation IN VARCHAR2,
  i_toprofile IN VARCHAR2,
  i_touserid IN VARCHAR2,
  i_customerid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_lpid IN VARCHAR2,
  i_uom IN VARCHAR2,
  i_quantity IN NUMBER,
  i_orderid IN NUMBER,
  i_shipid IN NUMBER,
  i_orderitem IN VARCHAR2,
  i_orderlot IN VARCHAR2,
  i_priority IN VARCHAR2,
  i_prevpriority IN VARCHAR2,
  i_currentuserid IN VARCHAR2,
  i_lastuser IN VARCHAR2,
  i_pickuom IN VARCHAR2,
  i_pickqty IN NUMBER,
  i_picktotype IN VARCHAR2,
  i_wave IN NUMBER,
  i_cartontype IN VARCHAR2,
  i_weight IN NUMBER,
  i_cube IN NUMBER,
  i_staffhrs IN NUMBER,
  i_cartonseq IN NUMBER,
  i_shippinglpid IN VARCHAR2,
  i_shippingtype IN VARCHAR2,
  i_invstatus IN VARCHAR2,
  i_inventoryclass IN VARCHAR2,
  i_qtytype IN VARCHAR2,
  i_lotnumber IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

INSERT
  INTO BATCHTASKS
    (
    TASKID,
    TASKTYPE,
	FACILITY,
	FROMLOC,
	TOSECTION,
	TOLOC,
	TOPROFILE,
	TOUSERID,
	CUSTID,
	ITEM,
	LPID,
	UOM,
	QTY,
	ORDERID,
	SHIPID,
	ORDERITEM,
	ORDERLOT,
	PRIORITY,
	PREVPRIORITY,
	CURRUSERID,
	LASTUSER,
	LASTUPDATE,
	PICKUOM,
	PICKQTY,
	PICKTOTYPE,
	WAVE,
	CARTONTYPE,
	WEIGHT,
	CUBE,
	STAFFHRS,
	CARTONSEQ,
	SHIPPINGLPID,
	SHIPPINGTYPE,
	INVSTATUS,
	INVENTORYCLASS,
	QTYTYPE,
	LOTNUMBER    
    )
    VALUES
    (
	i_taskid,
	i_tasktype,
	i_facility,
	i_fromlocation,
	i_tosection,
	i_tolocation,
	i_toprofile,
	i_touserid,
	i_customerid,
	i_item,
	i_lpid,
	i_uom,
	i_quantity,
	i_orderid,
	i_shipid,
	i_orderitem,
	i_orderlot,
	i_priority,
	i_prevpriority,
	i_currentuserid,
	i_lastuser,
	SYSDATE,
	i_pickuom,
	i_pickqty,
	i_picktotype,
	i_wave,
	i_cartontype,
	i_weight,
	i_cube,
	i_staffhrs,
	i_cartonseq,
	i_shippinglpid,
	i_shippingtype,
	i_invstatus,
	i_inventoryclass,
	i_qtytype,
	i_lotnumber
    );

  o_rowsaffected := SQL%ROWCOUNT;


END WVR_BATCHTASK_I_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CARTONFILLPERCENTAGE_R_P.SQL
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_CARTONFILLPERCENTAGE_R_P ( 
  i_custid IN VARCHAR2,
  c_cartonfill OUT SYS_REFCURSOR
) AS
BEGIN
OPEN c_cartonfill FOR
    SELECT 
	NVL((carton_fill_percent/100), 1) AS CartonFillPercentage, 
	NVL((carton_fill_percent_yn), 'N') AS CartonFillPercentageFlag		
    FROM customer_aux
    WHERE custid=i_custid;

END WVR_CARTONFILLPERCENTAGE_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_COMMITMENTNOTVNDRTRKD_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_COMMITMENTNOTVNDRTRKD_R_P ( 
  	i_orderid IN NUMBER, 
	i_shipid IN NUMBER,
	i_orderitem IN VARCHAR2,
	i_orderlot IN VARCHAR2,
  	c_commitment OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_commitment FOR
     select item, 
		nvl(lotnumber,'(none)') AS LOTNUMBER, 
		invstatus, 
		inventoryclass, 
		nvl(qty,0) AS QUANTITY, 
		uom
	from commitments
	where orderid = i_orderid
      		AND shipid = i_shipid
	  	AND orderitem = i_orderitem
	  	AND nvl(orderlot,'(none)') = nvl(i_orderlot,'(none)')
	order by item, lotnumber, invstatus,inventoryclass;

END WVR_COMMITMENTNOTVNDRTRKD_R_P;



 
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_COMMITMENTSVNDRTRKD_R_P.SQL
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_COMMITMENTSVNDRTRKD_R_P ( 
  	i_orderid IN NUMBER, 
	i_shipid IN NUMBER,
	i_orderitem IN VARCHAR2,
	i_orderlot IN VARCHAR2,
  	c_commitment OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_commitment FOR
    select item, nvl(lotnumber,'(none)'), invstatus, max(inventoryclass), sum(nvl(qty,0)), max(uom)
	from commitments
	where orderid = i_orderid
      		AND shipid = i_shipid
	  	AND orderitem = i_orderitem
	  	AND nvl(orderlot,'(none)') = nvl(i_orderlot,'(none)')
	 group by item, lotnumber, invstatus
	 order by item, lotnumber, invstatus;

END WVR_COMMITMENTSVNDRTRKD_R_P;



 
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_COMMITMENTS_R_P.sql
--*****************************************************

create or replace PROCEDURE WVR_COMMITMENTS_R_P ( 
  i_item IN NUMBER, 
  c_commitments OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_commitments FOR
    SELECT ITEM AS ITEM,
		NVL(LOTNUMBER,'(NONE)') AS LOTNUMBER,
		INVSTATUS,
		INVENTORYCLASS,
		NVL(QTY,0) AS QUANTITY,
		UOM		
	FROM COMMITMENTS
	WHERE ITEM = i_item;

END WVR_COMMITMENTS_R_P;

/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CUSTAUXITEMUNCODEFLAG_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_CUSTAUXITEMUNCODEFLAG_R_P ( 
  i_custid IN VARCHAR2,
  c_customer OUT SYS_REFCURSOR
) AS
BEGIN
OPEN c_customer FOR
     SELECT custid,
    		nvl(useitemuncodefromorder_yn, 'N') as useitemuncodefromorder_yn
  	FROM customer_aux
  	WHERE custid = i_custid;

END WVR_CUSTAUXITEMUNCODEFLAG_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CUSTIDBYWAVE_R_P.SQL
--*****************************************************

create or replace PROCEDURE WVR_CUSTIDBYWAVE_R_P ( 
  i_wave IN NUMBER, 
  o_custid OUT NVARCHAR2
) AS
BEGIN

	SELECT custid
      INTO o_custid
      FROM orderhdr
    WHERE wave = i_wave
              and rownum = 1;
	EXCEPTION WHEN OTHERS THEN
    	o_custid := '';

END WVR_CUSTIDBYWAVE_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CUSTITEMCATCHWEIGHT_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_CUSTITEMCATCHWEIGHT_R_P( 
  i_custid IN VARCHAR2, 
  i_item IN VARCHAR2,
  c_catchweight OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_catchweight FOR
    	select uom,
      		 nvl(totqty, 0) as qty,
             totweight as weight
         from custitemcatchweight
         where custid = i_custid
           and item = i_item;

END WVR_CUSTITEMCATCHWEIGHT_R_P;





 
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CUSTITEMCUBE_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_CUSTITEMCUBE_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  c_cube OUT SYS_REFCURSOR
) AS
BEGIN
 OPEN c_cube FOR
    SELECT NVL(cube,0) as cube, baseuom 
	 FROM custitem
   	 WHERE custid = i_custid
     		AND item = i_item;

END WVR_CUSTITEMCUBE_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CUSTITEMUOMBYEQUIPDOWN_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_CUSTITEMUOMBYEQUIPDOWN_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_fromseq IN NUMBER,
  i_toseq IN NUMBER,
  c_qty OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_qty FOR
      select qty 
    	from custitemuom
   	where custid = i_custid
     		and item = i_item
     		and sequence <= i_fromseq
     		and sequence >= i_toseq
   	order by sequence;

END WVR_CUSTITEMUOMBYEQUIPDOWN_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CUSTITEMUOMBYEQUIPUP_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_CUSTITEMUOMBYEQUIPUP_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_fromseq IN NUMBER,
  i_toseq IN NUMBER,
  c_qty OUT SYS_REFCURSOR
) AS
BEGIN
 OPEN c_qty FOR
     select qty 
    	from custitemuom
   	where custid = i_custid
     		and item = i_item
     		and sequence >= i_fromseq
     		and sequence <= i_toseq
   	order by sequence desc;

END WVR_CUSTITEMUOMBYEQUIPUP_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CUSTITEMUOMBYFROMUOM_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_CUSTITEMUOMBYFROMUOM_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_fromuom IN VARCHAR2,
  o_sequence OUT NUMBER
) AS
BEGIN

    select sequence into o_sequence 
         from custitemuom
         where custid = i_custid
           and item = i_item
           and fromuom = i_fromuom;
EXCEPTION WHEN OTHERS THEN
  		o_sequence := 0;

END WVR_CUSTITEMUOMBYFROMUOM_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CUSTITEMUOMBYUOM_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_CUSTITEMUOMBYUOM_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_fromuom IN VARCHAR2,
  c_custitemuom OUT SYS_REFCURSOR
) AS
BEGIN
OPEN c_custitemuom FOR
    select fromuom, qty, touom
         from custitemuom
         where custid = i_custid
           and item = i_item
           and (fromuom = i_fromuom or touom = i_fromuom );

END WVR_CUSTITEMUOMBYUOM_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CUSTITEMUOMCUBE_R_P.SQL
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_CUSTITEMUOMCUBE_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_uom IN VARCHAR2,
  o_cube OUT NUMBER
) AS
BEGIN    
     SELECT NVL(cube,0) INTO o_cube
	 FROM custitemuom
   	 WHERE custid = i_custid
     		AND item = i_item
     		AND touom = i_uom;
EXCEPTION
   WHEN NO_DATA_FOUND THEN 
    o_cube := 0;

END WVR_CUSTITEMUOMCUBE_R_P;


/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CUSTITEMUOMWEIGHT_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_CUSTITEMUOMWEIGHT_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_uom IN VARCHAR2,
  o_weight OUT NUMBER
) AS
BEGIN

    SELECT NVL(WEIGHT,0) INTO o_weight
	 FROM custitemuom
   	 WHERE custid = i_custid
     		AND item = i_item
     		AND touom = i_uom;
EXCEPTION WHEN OTHERS THEN
  		o_weight:=0;

END WVR_CUSTITEMUOMWEIGHT_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CUSTITEMVIEW_R_P.SQL
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_CUSTITEMVIEW_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  c_custitem OUT SYS_REFCURSOR
) AS
BEGIN
OPEN c_custitem FOR
     SELECT decode(variancepct,0,1,variancepct) AS variancepct,
         iskit,
         baseuom,
         expdaterequired
     FROM custitemview
  	WHERE custid = i_custid
  		AND   item = i_item;

END WVR_CUSTITEMVIEW_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CUSTITEMWEIGHT_R_P.SQL
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_CUSTITEMWEIGHT_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  c_weight OUT SYS_REFCURSOR
) AS
BEGIN
 OPEN c_weight FOR
    SELECT NVL(WEIGHT,0) as WEIGHT, baseuom 
	 FROM custitem
   	 WHERE custid = i_custid
     		AND item = i_item;

END WVR_CUSTITEMWEIGHT_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CUSTITMIATAPRIMCHEMCD_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_CUSTITMIATAPRIMCHEMCD_R_P ( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  c_custitem OUT SYS_REFCURSOR
) AS
BEGIN
OPEN c_custitem FOR
     SELECT item, iataprimarychemcode
  	FROM custitem
  	WHERE custid = i_custid
  		AND   item = i_item;

END WVR_CUSTITMIATAPRIMCHEMCD_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CUSTPCKBYLINE_R_P.SQL
--*****************************************************

create or replace PROCEDURE WVR_CUSTPCKBYLINE_R_P ( 
  i_custid IN VARCHAR2,
  c_customer OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_customer FOR
      SELECT pick_by_line_number_yn 
	 AS PickByLineNumber,
         nvl(enter_min_days_to_expire_yn,'N') 
	 AS EnterMinDaysToExpireFlag
    FROM customer
    WHERE custid = i_custid;

END WVR_CUSTPCKBYLINE_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_DEFAULTVALUEBYID_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_DEFAULTVALUEBYID_R_P( 
  i_defaultid IN VARCHAR2,
  o_defaultvalue OUT VARCHAR2
) AS
BEGIN
       select defaultvalue into o_defaultvalue
     	   from systemdefaults
   	   where defaultid = i_defaultid;
	EXCEPTION WHEN OTHERS THEN
  		o_defaultvalue:='';


END WVR_DEFAULTVALUEBYID_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_FINDAPICK_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_FINDAPICK_R_P (
  i_fromfacility IN VARCHAR2,  
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_lotnumber IN VARCHAR2,
  i_invstatus IN VARCHAR2,
  i_inventoryclass IN VARCHAR2,
  i_qty IN NUMBER,
  i_qtytype IN VARCHAR2,
  i_variancepct IN NUMBER,
  i_pickuom IN VARCHAR2,
  i_repl_req_yn IN VARCHAR2,
  i_storage_or_stage IN VARCHAR2,
  i_expdaterequired IN VARCHAR2,
  i_enter_min_days_to_expire_yn IN VARCHAR2,
  i_min_days_to_expiration IN NUMBER,
  i_preferred_pickzone IN VARCHAR2,
  i_vendor IN VARCHAR2,
  i_trace IN VARCHAR2,
  i_orderid IN NUMBER default null,
  i_shipid IN NUMBER default null,
  i_pfloc IN VARCHAR2 default null,
  i_slotwave_req_yn IN VARCHAR2 default 'N',
  i_dynamic_alloc_mode IN VARCHAR2 default 'N',
  i_allocneed IN VARCHAR2,
  i_picktotype_rule IN VARCHAR2,
  c_pick_details OUT SYS_REFCURSOR
) AS
	o_lpid  shippingplate.lpid%type;
    	o_baseuom tasks.pickuom%type;
    	o_baseqty plate.quantity%type;
    	o_pickuom tasks.pickuom%type;
    	o_pickqty plate.quantity%type;
	o_pickfront  char(1);
	o_picktotype waves.picktype%type;
	o_cartontype custitem.cartontype%type;
	o_picktype  waves.picktype%type;
	o_wholeunitsonly  char(1);
	o_inventoryclass plate.inventoryclass%type;
	o_Prevent_FPlates char(1);
	o_msg VARCHAR2(2000);

BEGIN

 
ALPS.ZWAVE.find_a_pick(in_fromfacility => i_fromfacility,
                        in_custid => i_custid,
                        in_item => i_item,
                        in_lotnumber => i_lotnumber,
                        in_invstatus => i_invstatus,
                        in_inventoryclass => i_inventoryclass,
                        in_qty => i_qty,
                        in_qtytype => i_qtytype,
                        in_variancepct => i_variancepct,
                        in_pickuom => i_pickuom,
                        in_repl_req_yn => i_repl_req_yn, 
                        in_storage_or_stage => i_storage_or_stage,
                        in_expdaterequired => i_expdaterequired,
                        in_enter_min_days_to_expire_yn => i_enter_min_days_to_expire_yn,
                        in_min_days_to_expiration => i_min_days_to_expiration,
                        in_wave_pickzone => i_preferred_pickzone,
                        in_vendor => i_vendor,
                        out_lpid_or_loc => o_lpid,
                        out_baseuom => o_baseuom,
                        out_baseqty => o_baseqty,
                        out_pickuom => o_pickuom,
                        out_pickqty => o_pickqty,
                        out_pickfront => o_pickfront,
                        out_picktotype => o_picktotype,
                        out_cartontype => o_cartontype,
                        out_picktype => o_picktype,
                        out_wholeunitsonly => o_wholeunitsonly,
                        out_inventoryclass => o_inventoryclass,
                        in_trace => i_trace,
                        out_Prevent_FPlates => o_Prevent_FPlates,
                        out_msg => o_msg,
                        in_orderid => i_orderid,
                        in_shipid => i_shipid,
                        in_pfloc => i_pfloc,
                        in_slotwave_req_yn => i_slotwave_req_yn,
                        in_dynamic_alloc_mode => i_dynamic_alloc_mode,
                        in_allocneed => i_allocneed,
			in_picktotype_rule => i_picktotype_rule);


 OPEN c_pick_details FOR
    SELECT  o_lpid AS LpIdOrLoc,
		o_baseuom AS BaseOum,
		o_baseqty AS BaseQty,
		o_pickuom AS PickUom,
		o_pickqty AS PickQty,
		o_pickfront AS PickFront,
		o_picktotype AS PickToType,
		o_cartontype AS CartonType,
		o_picktype AS PickType,
		o_wholeunitsonly AS WholeUnitsOnly,
		o_inventoryclass AS InventoryClass,
		o_Prevent_FPlates AS PreventFPlates,
		o_msg AS Message
	FROM DUAL;

END WVR_FINDAPICK_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_GETALLOCRULECNT_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_GETALLOCRULECNT_R_P( 
	 i_facility IN varchar2
	,i_custid IN varchar2
	,i_item IN varchar2
	,i_allocneed IN varchar2
	,o_bcount OUT NUMBER
 ) AS
    lv_allocrule varchar2(100) := ''; 
	l_baseuom custitem.baseuom%type;	
BEGIN   
	
	select nvl(baseuom,'?')
  into l_baseuom
  from custitem
 where custid = i_custid
   and item = i_item ;
   
   select 
      decode(altallocrule,null,allocrule,altallocrule) allocrule into lv_allocrule
    from ALPS.custitemfacilityview
    where custid = i_custid
    and   item = i_item
    and   facility = i_facility;
	
	select count(*) INTO o_bcount
    from allocrulesdtl
    where allocrule = lv_allocrule
    and   facility = i_facility
    and   uom = l_baseuom
    and allocneed = i_allocneed;
    
              
EXCEPTION WHEN no_data_found THEN    
        o_bcount := 0;


END WVR_GETALLOCRULECNT_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_GETAPPLOCK_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_GETAPPLOCK_R_P (
 in_lockid IN varchar2
,in_facility IN varchar2
,in_userid IN varchar2
,out_msg  IN OUT varchar2
) AS
BEGIN

 
ALPS.zapplocks.get_app_lock(in_lockid,in_facility,in_userid,out_msg);

END WVR_GETAPPLOCK_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_GETNEXTHIGHUOM_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_GETNEXTHIGHUOM_R_P( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  o_uom OUT VARCHAR2
 ) AS
 
BEGIN
    
select cio.touom into o_uom
      from custitem ci, custitemuom cio  
      where ci.custid = i_custid 
        and ci.item   = i_item 
        and ci.custid = cio.custid (+)
        and ci.item   = cio.item (+)
        and ci.baseuom = cio.fromuom (+)
      order by cio.qty asc;    
      
            
EXCEPTION WHEN OTHERS THEN
  		o_uom := '';

END WVR_GETNEXTHIGHUOM_R_P;

/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_GETPUTWALLCOUNT_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_GETPUTWALLCOUNT_R_P( 
  i_custid IN VARCHAR2,
  i_facility IN VARCHAR2,
  o_pwcnt OUT VARCHAR2
 ) AS
 
BEGIN
    
 select count(1)
           into o_pwcnt
           from putwall pw join putwallcubby pc
             on pw.facility = pc.facility
            and pw.putwall_id = pc.putwall_id
            join store_location_xref xref
              on pc.locid = xref.location
             and pc.facility = xref.facility
          where pw.facility = i_facility
            and xref.custid = i_custid;
          
EXCEPTION WHEN OTHERS THEN
  		o_pwcnt := 0;

END WVR_GETPUTWALLCOUNT_R_P;

/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_GETPUTWALLEXEMPT_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_GETPUTWALLEXEMPT_R_P( 
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  o_exempt OUT VARCHAR2
 ) AS
 
BEGIN
    
      select nvl(PUTWALL_EXEMPT_YN, 'N')
        into o_exempt
        from custitem 
      where custid = i_custid
        and item = i_item;

EXCEPTION WHEN OTHERS THEN
  		o_exempt := 'N';

END WVR_GETPUTWALLEXEMPT_R_P;

/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_ITEMCATCHWEIGHTFLAG_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_ITEMCATCHWEIGHTFLAG_R_P (
i_custid IN varchar2,
i_item IN varchar2,
o_flag OUT varchar2
) 
AS
BEGIN

  select use_catch_weights
    into o_flag
    from custitemview
   where custid = i_custid
     and item = i_item;
exception
  when NO_DATA_FOUND then
    o_flag := 'N';
  
END WVR_ITEMCATCHWEIGHTFLAG_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_LBRSTDSFORDIFFUOM_R_P.sql
--*****************************************************

create or replace PROCEDURE WVR_LBRSTDSFORDIFFUOM_R_P ( 
  i_facility IN VARCHAR2,
  i_custid IN VARCHAR2,
  i_category IN VARCHAR2,
  i_zoneid IN VARCHAR2,
  c_laborstd OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_laborstd FOR
      select uom,
         qtyperhour
    	from laborstandards
   	where facility = i_facility
     		and custid = i_custid
     		and category = i_category
     		and ( (nvl(zoneid,'x') = nvl(i_zoneid,'x')) or
           		( zoneid is null) )
   	order by zoneid,uom;

END WVR_LBRSTDSFORDIFFUOM_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_LBRSTDSFORUOM_R_P.sql
--*****************************************************

create or replace PROCEDURE WVR_LBRSTDSFORUOM_R_P ( 
  i_facility IN VARCHAR2,
  i_custid IN VARCHAR2,
  i_category IN VARCHAR2,
  i_zoneid IN VARCHAR2,
  i_uom IN VARCHAR2,
  c_qtyperhour OUT SYS_REFCURSOR
) AS
BEGIN
     OPEN c_qtyperhour FOR
     SELECT decode(nvl(qtyperhour,0),0,12,qtyperhour) AS qtyperhour, 
	uom
    	FROM laborstandards
   	WHERE facility = i_facility
     		and custid = i_custid
     		and category = i_category
     		and ( (nvl(zoneid,'x') = nvl(i_zoneid,'x')) or
           		( zoneid is null) )
     		and uom = i_uom
   	ORDER BY zoneid;

END WVR_LBRSTDSFORUOM_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_LOCATIONDETAILS_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_LOCATIONDETAILS_R_P ( 
  i_facility IN VARCHAR2, 
  i_locid IN VARCHAR2,
  c_locdetails OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_locdetails FOR
    select section,
         equipprof,
         pickingzone,
         nvl(pickingseq,0) as pickingseq
    from location
   where facility = i_facility
     and locid = i_locid;

END WVR_LOCATIONDETAILS_R_P;





 
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_NEXTSHPNGPLTLPID_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_NEXTSHPNGPLTLPID_R_P ( 
  o_shippinglpid OUT VARCHAR2
) AS
msg VARCHAR2(80);
BEGIN

  zsp.get_next_shippinglpid(o_shippinglpid, msg);

END WVR_NEXTSHPNGPLTLPID_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_NEXTTASKID_R_P.SQL
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_NEXTTASKID_R_P ( 
  o_taskid OUT NUMBER
) AS
msg VARCHAR2(80);
BEGIN

  ztsk.get_next_taskid(o_taskid, msg);

END WVR_NEXTTASKID_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_ODRHAZMATFLAG_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_ODRHAZMATFLAG_R_P ( 
  i_orderid IN NUMBER, 
  i_shipid IN NUMBER,
  o_flag OUT VARCHAR2
) AS
BEGIN

  SELECT hazmatind into o_flag 
  	FROM orderhdr
  	WHERE orderid = i_orderid
  	  AND shipid = i_shipid;

END WVR_ODRHAZMATFLAG_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_ORDDTLIATAPRIMCHEMCD_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_ORDDTLIATAPRIMCHEMCD_R_P ( 
  i_orderid IN NUMBER,
  i_item IN VARCHAR2,
  i_shipid IN NUMBER,
  c_orderdtls OUT SYS_REFCURSOR
) AS
BEGIN
OPEN c_orderdtls FOR
      SELECT orderid, shipid, iataprimarychemcode
  		FROM orderdtl
  		WHERE orderid = i_orderid
  			AND   shipid = i_shipid
  			AND   item = i_item;

END WVR_ORDDTLIATAPRIMCHEMCD_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_ORDERDETAILSBYITEM_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_ORDERDETAILSBYITEM_R_P ( 
  i_orderid IN NUMBER,
  i_item IN VARCHAR2,
  i_shipid IN NUMBER,
  c_orderdtls OUT SYS_REFCURSOR
) AS
BEGIN
OPEN c_orderdtls FOR
      SELECT ORDERID,
        SHIPID,
        ITEM,
        LOTNUMBER,
        INVSTATUS,
        QTYTYPE AS QUANTITYTYPE,
        INVENTORYCLASS,
        CUSTID AS CUSTOMERID,
        FROMFACILITY,
        UOM,
        LINESTATUS,
        COMMITSTATUS,
        QTYENTERED,
        ITEMENTERED,
        UOMENTERED,
        QTYORDER,
        WEIGHTORDER,
        CUBEORDER,
        AMTORDER,
        QTYCOMMIT,
        VENDOR,
        QTYPICK,
        XDOCKORDERID,
        XDOCKSHIPID,
        PRIORITY,
        STAFFHRS,
        LASTUSER,
        MIN_DAYS_TO_EXPIRATION,
        IATAPRIMARYCHEMCODE,
        DYNAMIC_INV_USED_YN  AS DYNAMICINVUSEDFLAG
  		FROM orderdtl
  		WHERE orderid = i_orderid
  			AND   shipid = i_shipid
  			AND   item = i_item;

END WVR_ORDERDETAILSBYITEM_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_ORDERDETAILS_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_ORDERDETAILS_R_P ( 
  i_orderid IN NUMBER, 
  i_shipid IN NUMBER, 
  c_orderdetails OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_orderdetails FOR
    SELECT ORDERID,
    	SHIPID,
    	ITEM,
    	LOTNUMBER,
    	INVSTATUS,
    	QTYTYPE AS QUANTITYTYPE,
    	INVENTORYCLASS,
    	CUSTID AS CUSTOMERID,
    	FROMFACILITY,
    	UOM,
    	LINESTATUS,
    	COMMITSTATUS,
    	QTYENTERED,
    	ITEMENTERED,
    	UOMENTERED,
    	QTYORDER,
    	WEIGHTORDER,
    	CUBEORDER,
    	AMTORDER,
    	QTYCOMMIT,
    	VENDOR,
    	QTYPICK,
    	XDOCKORDERID,
    	XDOCKSHIPID,
    	PRIORITY,
	STAFFHRS,
	LASTUSER,
	MIN_DAYS_TO_EXPIRATION,
	IATAPRIMARYCHEMCODE,
	DYNAMIC_INV_USED_YN  AS DYNAMICINVUSEDFLAG
	FROM ORDERDTL D
	WHERE ORDERID = i_orderId
		AND SHIPID = i_shipid;

END WVR_ORDERDETAILS_R_P;



 
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_ORDERHEADERBYORDERID_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_ORDERHEADERBYORDERID_R_P ( 
  i_orderid IN NUMBER, 
  i_shipid IN NUMBER, 
  c_orderHeader OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_orderHeader FOR
    SELECT ORDERID,
    	SHIPID,
    	CUSTID AS CUSTOMERID,
    	ORDERTYPE,
    	ORDERSTATUS,
    	COMMITSTATUS,
    	FROMFACILITY,
    	TOFACILITY,
    	LOADNO,
    	STOPNO,
   		SHIPNO,
   		SHIPTO,
    	STAGELOC,
    	WAVE,
    	PRIORITY,
    	LASTUSER,
		CARRIER,
		SHIPTYPE,
         QTYPICK,
		 COMPONENTTEMPLATE
	FROM 
    ORDERHDR 
	WHERE ORDERID = i_orderid
		AND SHIPID = i_shipid;

END WVR_ORDERHEADERBYORDERID_R_P;



 
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_ORDERHEADER_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_ORDERHEADER_R_P ( 
  i_wave IN NUMBER, 
  c_orderHeader OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_orderHeader FOR
    SELECT ORDERID,
    	SHIPID,
    	CUSTID AS CUSTOMERID,
    	ORDERTYPE,
    	ORDERSTATUS,
    	COMMITSTATUS,
    	FROMFACILITY,
    	TOFACILITY,
    	LOADNO,
    	STOPNO,
   		SHIPNO,
   		SHIPTO,
    	STAGELOC,
    	WAVE,
    	PRIORITY,
    	LASTUSER,
		CARRIER,
		SHIPTYPE,
         QTYPICK
	FROM 
    ORDERHDR 
	WHERE WAVE = i_wave;

END WVR_ORDERHEADER_R_P;



 
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_ORDERHEADER_U_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_ORDERHEADER_U_P
(
i_orderstatus IN VARCHAR2,
i_commitstatus IN VARCHAR2,
i_wave IN NUMBER,
i_priority IN VARCHAR2,
i_userid IN VARCHAR2,
i_orderid IN NUMBER,
i_shipid IN NUMBER,
o_rowsaffected OUT NUMBER
) AS 
BEGIN  

update orderhdr
   set orderstatus = NVL(i_orderstatus,orderstatus),
       commitstatus = NVL(i_commitstatus,commitstatus),
       wave = NVL(i_wave,wave),
       priority  = NVL(i_priority,priority),
       lastuser = i_userid,
       lastupdate = sysdate
 where ORDERID = i_orderid
    AND SHIPID = i_shipid;

o_rowsaffected := SQL%ROWCOUNT;

END WVR_ORDERHEADER_U_P;
  
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_PENDBTCHVNDRITM_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_PENDBTCHVNDRITM_R_P ( 
  i_orderid IN NUMBER, 
  i_orderitem IN VARCHAR2,
  i_shipid IN NUMBER,
  i_orderlot IN VARCHAR2,
  i_item IN VARCHAR2,
  i_invstatus IN VARCHAR2,
  o_quantity OUT NUMBER
) AS
BEGIN

SELECT NVL(SUM(qty),0) INTO o_quantity
    FROM batchtasks
	WHERE orderid = i_orderid
     AND shipid = i_shipid
     AND orderitem = i_orderitem
     AND NVL(orderlot,'(none)') = NVL(i_orderlot,'(none)')
     and item = i_item
     and invstatus = i_invstatus;
	 

END WVR_PENDBTCHVNDRITM_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_PENDINGBATCHITEM_R_P.sql
--*****************************************************

create or replace PROCEDURE WVR_PENDINGBATCHITEM_R_P ( 
  i_orderid IN NUMBER, 
  i_orderitem IN VARCHAR2,
  i_shipid IN NUMBER,
  i_orderlot IN VARCHAR2,
  i_item IN VARCHAR2,
  i_invstatus IN VARCHAR2,
  i_inventoryclass IN VARCHAR2,
  o_quantity OUT NUMBER
) AS
BEGIN

SELECT NVL(SUM(qty),0) INTO o_quantity
    FROM batchtasks
	WHERE orderid = i_orderid
     AND shipid = i_shipid
     AND orderitem = i_orderitem
     AND NVL(orderlot,'(none)') = NVL(i_orderlot,'(none)')
     AND item = i_item
     AND invstatus = i_invstatus
     AND inventoryclass = i_inventoryclass;
EXCEPTION WHEN OTHERS THEN
  	o_quantity := 0;


END WVR_PENDINGBATCHITEM_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_PENDINGBATCHLINE_R_P.SQL
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_PENDINGBATCHLINE_R_P ( 
  i_orderid IN NUMBER, 
  i_orderitem IN VARCHAR2,
  i_shipid IN NUMBER,
  i_orderlot IN VARCHAR2,
  o_quantity OUT NUMBER
) AS
BEGIN

SELECT NVL(SUM(qty),0) INTO o_quantity
    FROM batchtasks
	WHERE orderid = i_orderid
     AND shipid = i_shipid
     AND orderitem = i_orderitem
     AND NVL(orderlot,'(none)') = NVL(i_orderlot,'(none)');

END WVR_PENDINGBATCHLINE_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_PENDINGPICKSITEM_R_P.sql
--*****************************************************

create or replace PROCEDURE WVR_PENDINGPICKSITEM_R_P ( 
  i_orderid IN NUMBER, 
  i_orderitem IN VARCHAR2,
  i_shipid IN NUMBER,
  i_orderlot IN VARCHAR2,
  i_item IN VARCHAR2,
  i_invstatus IN VARCHAR2,
  i_inventoryclass IN VARCHAR2,
  o_quantity OUT NUMBER
) AS
BEGIN

 SELECT NVL(SUM(quantity),0) INTO o_quantity
    FROM shippingplate
    WHERE orderid = i_orderid
     AND shipid = i_shipid
     AND orderitem = i_orderitem
     AND nvl(orderlot,'(none)') = NVL(i_orderlot,'(none)')
     AND item = i_item
     AND invstatus = i_invstatus
     AND inventoryclass = i_inventoryclass
     AND type in ('F','P')
     AND status = 'U';

END WVR_PENDINGPICKSITEM_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_PENDINGPICKSLINE_R_P.SQL
--*****************************************************

create or replace PROCEDURE WVR_PENDINGPICKSLINE_R_P ( 
  i_orderid IN NUMBER, 
  i_orderitem IN VARCHAR2,
  i_shipid IN NUMBER,
  i_orderlot IN VARCHAR2,
  o_quantity OUT NUMBER
) AS
BEGIN

 SELECT NVL(SUM(quantity),0) INTO o_quantity
    FROM shippingplate
    WHERE orderid = i_orderid
     AND shipid = i_shipid
     AND orderitem = i_orderitem
     AND nvl(orderlot,'(none)') = NVL(i_orderlot,'(none)')
     AND type IN ('F','P')
     AND status = 'U';

END WVR_PENDINGPICKSLINE_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_PENDINGPICKSVNDRITM_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_PENDINGPICKSVNDRITM_R_P ( 
  i_orderid IN NUMBER, 
  i_orderitem IN VARCHAR2,
  i_shipid IN NUMBER,
  i_orderlot IN VARCHAR2,
  i_item IN VARCHAR2,
  i_invstatus IN VARCHAR2,
  o_quantity OUT NUMBER
) AS
BEGIN

 SELECT NVL(SUM(quantity),0) INTO o_quantity
    FROM shippingplate
    WHERE orderid = i_orderid
     AND shipid = i_shipid
     AND orderitem = i_orderitem
     AND nvl(orderlot,'(none)') = NVL(i_orderlot,'(none)')
     AND item = i_item
     AND invstatus = i_invstatus
     AND type IN ('F','P')
     AND status = 'U';

END WVR_PENDINGPICKSVNDRITM_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_PLATEBYLPID_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_PLATEBYLPID_R_P  (
  i_lpid IN VARCHAR2, 
  c_plate OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_plate FOR
  select lpid,
    custid as CustomerId,
    location,
    weight, 
    quantity, 
    unitofmeasure,
    item,  
    facility
    from plate
       where lpid = i_lpid OR location = i_lpid;

END WVR_PLATEBYLPID_R_P;

/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_PLATEQTYTASKED_U_P.SQL
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_PLATEQTYTASKED_U_P (
  i_qtytasked IN NUMBER,  
  i_lpid IN VARCHAR2,
  i_lastuser IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

UPDATE PLATE
  SET QTYTASKED  =  CASE 
                      WHEN (nvl(qtytasked,0) + i_qtytasked) > -1 THEN 
                          (nvl(qtytasked,0) + i_qtytasked) 
                      ELSE 
                        NULL 
                      END ,
    LASTUSER   = i_lastuser,
    LASTUPDATE = SYSDATE
  WHERE LPID = i_lpid
	 AND parentfacility IS NOT NULL;

  o_rowsaffected := SQL%ROWCOUNT;


END WVR_PLATEQTYTASKED_U_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_PLATEWGTANDQTYBYLPID_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_PLATEWGTANDQTYBYLPID_R_P ( 
  i_lpid IN VARCHAR2, 
  c_plate OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_plate FOR
  select 
    weight, 
    quantity, 
    unitofmeasure
    item,  
    lpid
    from plate
       where lpid = i_lpid;
			 
END WVR_PLATEWGTANDQTYBYLPID_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_PROCESSINGIND_U_P.SQL
--*****************************************************

CREATE OR REPLACE PROCEDURE  WVR_PROCESSINGIND_U_P
(
i_wave IN NUMBER,
o_rowsaffected OUT NUMBER
) AS 
BEGIN  

UPDATE waves
  SET processing_ind = null, process_start_tm = null
  WHERE wave = i_wave;

o_rowsaffected := SQL%ROWCOUNT;

END WVR_PROCESSINGIND_U_P;
  
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_RELEASEAPPLOCK_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_RELEASEAPPLOCK_R_P (
 in_lockid IN varchar2
,in_facility IN varchar2
,in_userid IN varchar2
,out_msg  IN OUT varchar2
) AS
BEGIN

 
ALPS.zapplocks.release_app_lock(in_lockid,in_facility,in_userid,out_msg);

END WVR_RELEASEAPPLOCK_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_RELEASEORDER_U_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_RELEASEORDER_U_P
(
i_orderstatus IN VARCHAR2,
i_commitstatus IN VARCHAR2,
i_priority IN VARCHAR2,
i_userid IN VARCHAR2,
i_orderid IN NUMBER,
i_shipid IN NUMBER,
o_rowsaffected OUT NUMBER
) AS 
BEGIN  

update orderhdr
   set orderstatus = i_orderstatus ,
       commitstatus = i_commitstatus,
       priority = i_priority,
       lastuser = i_userid,
       lastupdate = sysdate
 where orderid = i_orderid
   and shipid = i_shipid
   and orderstatus < i_orderstatus;

o_rowsaffected := SQL%ROWCOUNT;

END WVR_RELEASEORDER_U_P;
  
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_RESETORDERDTLS_U_P.SQL
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_RESETORDERDTLS_U_P
(
i_custid IN VARCHAR2,
i_priority IN VARCHAR2,
i_fromfacility IN VARCHAR2,
i_orderid IN NUMBER,
i_shipid IN NUMBER,
o_rowsaffected OUT NUMBER
) AS 
BEGIN  

  update orderdtl
     set fromfacility = i_fromfacility,
         custid = i_custid,
         priority = i_priority
   where orderid = i_orderid
     and shipid = i_shipid
    and ( (i_fromfacility is null) or
           (i_custid is null) or
           (i_priority is null) );

O_rowsaffected := SQL%ROWCOUNT;

END WVR_RESETORDERDTLS_U_P;
  
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_SHIPPINGPLATE_I_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_SHIPPINGPLATE_I_P(
  i_lpid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_custid IN VARCHAR2,
  i_Facility IN VARCHAR2,
  i_location IN VARCHAR2,
  i_status IN VARCHAR2,
  i_holdreason IN VARCHAR2,
  i_unitofmeasure IN VARCHAR2,
  i_quantity IN VARCHAR2,
  i_type IN VARCHAR2,
  i_fromlpid IN VARCHAR2,
  i_lotnumber IN VARCHAR2,
  i_parentlpid IN VARCHAR2,
  i_useritem1 IN VARCHAR2,
  i_lastuser IN VARCHAR2,
  i_invstatus IN VARCHAR2,
  i_qtyentered IN NUMBER,
  i_orderitem IN VARCHAR2,
  i_uomentered IN VARCHAR2,
  i_inventoryclass IN VARCHAR2,
  i_orderid IN NUMBER,
  i_shipid IN NUMBER,
  i_weight IN NUMBER,
  i_ucc128 IN VARCHAR2,	
  i_labelformat IN VARCHAR2,
  i_taskid IN NUMBER,
  i_pickuom IN VARCHAR2,
  i_pickqty IN NUMBER,
  i_cartonseq IN NUMBER,
  i_hazmatind IN VARCHAR2,
  i_iataprimarychemcode IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

INSERT
  INTO shippingplate
    (
    	lpid, 
	item, 
	custid, 
	facility, 
	location, 
	status, 
	holdreason,
    unitofmeasure, 
	quantity, 
	type, 
	fromlpid, 
    lotnumber, 
	parentlpid, 
	useritem1, 
    lastuser, 
	lastupdate, 
	invstatus, 
	qtyentered, 
	orderitem,
    uomentered, 
	inventoryclass, 
    orderid, 
	shipid, 
	weight, 
	ucc128, 
	labelformat, 
	taskid, 
    pickuom, 
	pickqty, 
	cartonseq,
    hazmatind,
    iataprimarychemcode 
    )
    VALUES
    (
	i_lpid,
	i_item,
	i_custid,
	i_facility,
	i_location,
	i_status,
	i_holdreason,
	i_unitofmeasure,
	i_quantity,
	i_type,
	i_fromlpid,
	i_lotnumber,
	i_parentlpid,
	i_useritem1,
	i_lastuser,
	SYSDATE,
	i_invstatus,
	i_qtyentered,
	i_orderitem,
	i_uomentered,
	i_inventoryclass,
	i_orderid,
	i_shipid,
	i_weight,
	i_ucc128,	
	i_labelformat,
	i_taskid,
	i_pickuom,
	i_pickqty,
	i_cartonseq,
	i_hazmatind,
	i_iataprimarychemcode
    );

  o_rowsaffected := SQL%ROWCOUNT;


END WVR_SHIPPINGPLATE_I_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_STAGELOCBYCARRIER_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_STAGELOCBYCARRIER_R_P( 
  i_carrier IN VARCHAR2,
  i_facility IN VARCHAR2,
  i_shiptype IN VARCHAR2,
  o_stageloc OUT VARCHAR2
) AS
BEGIN
      
  select stageloc into o_stageloc
    from carrierstageloc
   where carrier = i_carrier
     and facility = i_facility
     and shiptype = i_shiptype;
   EXCEPTION WHEN OTHERS THEN
  	o_stageloc := '';

END WVR_STAGELOCBYCARRIER_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_SUBTASK_I_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_SUBTASK_I_P (
  i_taskid IN NUMBER,
  i_tasktype IN VARCHAR2,
  i_facility IN VARCHAR2,
  i_fromsection IN VARCHAR2,
  i_fromlocation IN VARCHAR2,
  i_fromprofile IN VARCHAR2,
  i_tosection IN VARCHAR2,
  i_tolocation IN VARCHAR2,
  i_toprofile IN VARCHAR2,
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_lpid IN VARCHAR2,
  i_uom IN VARCHAR2,
  i_quantity IN NUMBER,
  i_locseq IN NUMBER,
  i_orderid IN NUMBER,
  i_shipid IN NUMBER,
  i_orderitem IN VARCHAR2,
  i_orderlot IN VARCHAR2,
  i_priority IN VARCHAR2,
  i_prevpriority IN VARCHAR2,
  i_lastuser IN VARCHAR2,
  i_pickuom IN VARCHAR2,
  i_pickqty IN NUMBER,
  i_picktotype IN VARCHAR2,
  i_wave IN NUMBER,
  i_pickingzone IN VARCHAR2,
  i_cartontype IN VARCHAR2,
  i_weight IN NUMBER,
  i_cube IN NUMBER,
  i_staffhrs IN NUMBER,
  i_cartonseq IN NUMBER,
  i_shippinglpid IN VARCHAR2,
  i_shippingtype IN VARCHAR2,
  i_cartongroup IN VARCHAR2,
  i_labeluom IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

INSERT
  INTO SUBTASKS
    (
    	TASKID,
    	TASKTYPE,
	FACILITY,
	FROMSECTION,
	FROMLOC,
	FROMPROFILE,
	TOSECTION,
	TOLOC,
	TOPROFILE,
	CUSTID,
	ITEM,
	LPID,
	UOM,
	QTY,
	LOCSEQ,
	ORDERID,
	SHIPID,
	ORDERITEM,
	ORDERLOT,
	PRIORITY,
	PREVPRIORITY,
	LASTUSER,
	LASTUPDATE,
	PICKUOM,
	PICKQTY,
	PICKTOTYPE,
	WAVE,
	PICKINGZONE,
	CARTONTYPE,
	WEIGHT,
	CUBE,
	STAFFHRS, 
	cartonseq,
    shippinglpid, 
	shippingtype, 
	cartongroup, 
	labeluom
    )
    VALUES
    (
	i_taskid,
	i_tasktype,
	i_facility,
	i_fromsection,
	i_fromlocation,
	i_fromprofile,
	i_tosection,
	i_tolocation,
	i_toprofile,
	i_custid,
	i_item,
	i_lpid,
	i_uom,
	i_quantity,
	i_locseq,
	i_orderid,
	i_shipid,
	i_orderitem,
	i_orderlot,
	i_priority,
	i_prevpriority,
	i_lastuser,
	SYSDATE,
	i_pickuom,
	i_pickqty,
	i_picktotype,
	i_wave,
    i_pickingzone,
	i_cartontype,
	i_weight,
	i_cube,
	i_staffhrs,
	i_cartonseq,
	i_shippinglpid,
	i_shippingtype,
	i_cartongroup,
	i_labeluom
    );

  o_rowsaffected := SQL%ROWCOUNT;


END WVR_SUBTASK_I_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_SUBTSKBYORDRANDTYPE_R_P.sql
--*****************************************************

create or replace PROCEDURE WVR_SUBTSKBYORDRANDTYPE_R_P ( 
  i_wave IN NUMBER,  
  i_orderid IN NUMBER,  
  i_shipid IN NUMBER,  
  i_tasktype IN VARCHAR2, 
  c_subtasks OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_subtasks FOR
      select rowid,
         taskid,
         TaskType,
         FACILITY,
         FROMSECTION,
         FROMLOC as FROMLOCATION,
         FROMPROFILE,
         TOSECTION,
         TOLOC as TOLOCATION,
         TOPROFILE,
         CUSTID,
         QTY as Quantity,
         LOCSEQ,
         ORDERID,
         SHIPID,
         PRIORITY,
         PREVPRIORITY,
         WAVE,
         PICKINGZONE,
         WEIGHT,
         CUBE,
         STAFFHRS,
         PICKQTY         
    from subtasks
    where WAVE = i_wave 
            AND orderid = i_orderid
            AND shipid = i_shipid
            AND TASKTYPE = i_tasktype;

END WVR_SUBTSKBYORDRANDTYPE_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_TASKBYORDERIDTSKTYPE_D_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_TASKBYORDERIDTSKTYPE_D_P (
  i_orderid IN NUMBER,  
  i_shipid IN NUMBER,  
  i_tasktype IN VARCHAR2,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  delete from tasks
     where orderid = i_orderid
            AND shipid = i_shipid
            AND tasktype = i_tasktype;

  o_rowsaffected := SQL%ROWCOUNT;

END WVR_TASKBYORDERIDTSKTYPE_D_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_TASKBYTYPEANDORDER_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_TASKBYTYPEANDORDER_R_P ( 
  i_wave IN NUMBER,
  i_orderid IN NUMBER,
  i_shipid IN NUMBER,
  i_tasktype IN VARCHAR2,
  c_tasks OUT SYS_REFCURSOR
)  AS
BEGIN

OPEN c_tasks FOR
  SELECT TaskId, TaskType, Weight, Cube, staffhrs, pickqty, qty, WAVE, OrderID, shipid 
    FROM TASKS 
    WHERE WAVE = i_wave 
        AND OrderID = i_orderid 
        AND shipid = i_shipid 
        AND TaskType = i_tasktype;

END WVR_TASKBYTYPEANDORDER_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_TASKGROUPINGBYZONE_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_TASKGROUPINGBYZONE_R_P ( 
  i_facility IN VARCHAR2,
  i_zoneid IN VARCHAR2,
  c_zone OUT SYS_REFCURSOR
)  AS
BEGIN

OPEN c_zone FOR
  select ZONEID, FACILITY, SEPARATE_BATCH_TASKS_YN AS SEPARATEBATCHTASKSFLAG,
    BATCH_TASKS_LIMIT AS BATCHTASKSLIMIT,SEPARATE_LINE_TASKS_YN AS SEPARATELINETASKSFLAG, 
    LINE_TASKS_LIMIT AS LINETASKSLIMIT, SEPARATE_ORDER_TASKS_YN AS SEPARATEORDERTASKSFLAG, 
    ORDER_TASKS_LIMIT AS ORDERTASKSLIMIT
    from ZONE 
    where  FACILITY = i_facility and ZONEID =i_zoneid;

END WVR_TASKGROUPINGBYZONE_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_TASK_I_P.SQL
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_TASK_I_P (
  i_taskid IN NUMBER,
  i_tasktype IN VARCHAR2,
  i_facility IN VARCHAR2,
  i_fromsection IN VARCHAR2,
  i_fromlocation IN VARCHAR2,
  i_fromprofile IN VARCHAR2,
  i_tosection IN VARCHAR2,
  i_tolocation IN VARCHAR2,
  i_toprofile IN VARCHAR2,
  i_custid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_lpid IN VARCHAR2,
  i_uom IN VARCHAR2,
  i_quantity IN NUMBER,
  i_locseq IN NUMBER,
  i_orderid IN NUMBER,
  i_shipid IN NUMBER,
  i_orderitem IN VARCHAR2,
  i_orderlot IN VARCHAR2,
  i_priority IN VARCHAR2,
  i_prevpriority IN VARCHAR2,
  i_lastuser IN VARCHAR2,
  i_pickuom IN VARCHAR2,
  i_pickqty IN NUMBER,
  i_picktotype IN VARCHAR2,
  i_wave IN NUMBER,
  i_pickingzone IN VARCHAR2,
  i_cartontype IN VARCHAR2,
  i_weight IN NUMBER,
  i_cube IN NUMBER,
  i_staffhrs IN NUMBER,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

INSERT
  INTO TASKS
    (
    	TASKID,
    	TASKTYPE,
	FACILITY,
	FROMSECTION,
	FROMLOC,
	FROMPROFILE,
	TOSECTION,
	TOLOC,
	TOPROFILE,
	CUSTID,
	ITEM,
	LPID,
	UOM,
	QTY,
	LOCSEQ,
	ORDERID,
	SHIPID,
	ORDERITEM,
	ORDERLOT,
	PRIORITY,
	PREVPRIORITY,
	LASTUSER,
	LASTUPDATE,
	PICKUOM,
	PICKQTY,
	PICKTOTYPE,
	WAVE,
	PICKINGZONE,
	CARTONTYPE,
	WEIGHT,
	CUBE,
	STAFFHRS  
    )
    VALUES
    (
	i_taskid,
	i_tasktype,
	i_facility,
	i_fromsection,
	i_fromlocation,
	i_fromprofile,
	i_tosection,
	i_tolocation,
	i_toprofile,
	i_custid,
	i_item,
	i_lpid,
	i_uom,
	i_quantity,
	i_locseq,
	i_orderid,
	i_shipid,
	i_orderitem,
	i_orderlot,
	i_priority,
	i_prevpriority,
	i_lastuser,
	SYSDATE,
	i_pickuom,
	i_pickqty,
	i_picktotype,
	i_wave,
    i_pickingzone,
	i_cartontype,
	i_weight,
	i_cube,
	i_staffhrs
    );

  o_rowsaffected := SQL%ROWCOUNT;


END WVR_TASK_I_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_TASK_U_P.SQL
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_TASK_U_P (
  i_taskid IN NUMBER,  
  i_qty IN NUMBER,   
  i_pickqty IN NUMBER,  
  i_weight IN NUMBER,  
  i_cube IN NUMBER,  
  i_staffhrs IN NUMBER, 
  o_rowsaffected OUT NUMBER
) AS
BEGIN

 update tasks
       set pickqty = i_pickqty,
           qty = i_qty,
           weight = i_weight,
           cube = i_cube,
           staffhrs = i_staffhrs
       where taskid = i_taskid; 

  o_rowsaffected := SQL%ROWCOUNT;


END WVR_TASK_U_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_TSKIDOFSUBTSKBYROWID_U_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_TSKIDOFSUBTSKBYROWID_U_P (
  i_taskid IN NUMBER,  
  i_lastuser IN VARCHAR2,
  i_rowid IN rowid,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

UPDATE SUBTASKS
  SET TASKID  = i_taskid,
    LASTUSER   = i_lastuser,
    LASTUPDATE = SYSDATE
  WHERE ROWID = i_rowid;

  o_rowsaffected := SQL%ROWCOUNT;


END WVR_TSKIDOFSUBTSKBYROWID_U_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_VENDORTRACKEDFLAG_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVR_VENDORTRACKEDFLAG_R_P (
i_custid IN varchar2,
i_item IN varchar2,
o_flag OUT varchar2
) 
AS
BEGIN

  select vendor_tracked_yn
    into o_flag 
    from custitemview
   where custid = i_custid
     and item = i_item;

    exception when others then
     o_flag := 'N';
  
END WVR_VENDORTRACKEDFLAG_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_WAVESTATUS_U_P.SQL
--*****************************************************

create or replace PROCEDURE WVR_WAVESTATUS_U_P (
  i_id IN NUMBER,  
  i_status IN VARCHAR2, 
  i_lastuser IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

UPDATE WAVES
  SET WAVESTATUS  = i_status,
    LASTUSER   = i_lastuser,
    LASTUPDATE = SYSDATE,
    actualrelease = SYSDATE,
    schedrelease = null
  WHERE WAVE = i_id;

  o_rowsaffected := SQL%ROWCOUNT;


END WVR_WAVESTATUS_U_P;


/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_WAVES_R_P.SQL
--*****************************************************

create or replace PROCEDURE WVR_WAVES_R_P ( 
  i_id IN NUMBER, 
  c_waves OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_waves FOR
    SELECT WAVE AS ID,
		DESCR AS DESCRIPTION,
		WAVESTATUS AS STATUS,
		SCHEDRELEASE AS SCHEDULERELEASE,
		ACTUALRELEASE,
		FACILITY,
		LASTUSER,
		LASTUPDATE,
		STAGELOC AS STAGELOCATION,
		PICKTYPE,
		TASKPRIORITY,
		SORTLOC AS SORTLOCATION,
		JOB,
		nvl(CONSOLIDATED, 'N') CONSOLIDATED,
		PICKTOTYPE_RULE AS PICKTOTYPERULE,
		PREFERRED_PICKZONE AS PREFERREDPICKZONE,
                BATCHCARTONTYPE 
	FROM WAVES
	WHERE WAVE = i_id;

END WVR_WAVES_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_ACTIVETASKSCOUNTBYWAVE_R_P.sql
--*****************************************************

create or replace PROCEDURE WVU_ACTIVETASKSCOUNTBYWAVE_R_P ( 
  i_wave IN NUMBER, 
  o_rowscount OUT NUMBER
) AS
BEGIN

    SELECT count(1)
      INTO o_rowscount
      FROM tasks
    WHERE wave = i_wave
       AND priority = '0';
EXCEPTION WHEN OTHERS THEN
    o_rowscount := 0;
END WVU_ACTIVETASKSCOUNTBYWAVE_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_BATCHTSKBYTSKIDLPID_D_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_BATCHTSKBYTSKIDLPID_D_P (
  i_taskid IN NUMBER,  
  i_custid IN VARCHAR2,  
  i_orderitem IN VARCHAR2, 
  i_orderlot IN VARCHAR2, 
  i_lpid IN VARCHAR2, 
  i_item IN VARCHAR2, 
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  DELETE 
    FROM BATCHTASKS
    WHERE TASKID = i_taskid
      AND CUSTID = i_custid
      AND NVL(ORDERITEM,'(none)') = NVL(i_orderitem, '(none)')
      AND NVL(ORDERLOT,'(none)') = NVL(i_orderlot, '(none)')
      AND NVL(LPID,'(none)') = NVL(i_lpid, '(none)')
      AND ITEM = i_item;

  o_rowsaffected := SQL%ROWCOUNT;


END WVU_BATCHTSKBYTSKIDLPID_D_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_LOADSTATUSBYLOADNO_U_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_LOADSTATUSBYLOADNO_U_P (
  i_userid IN varchar2,
  i_loadno IN NUMBER, 
  i_status IN varchar2,     
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  update loads
       set loadstatus = i_status,
           lastuser = i_userid,
           lastupdate = sysdate
     where loadno = i_loadno
       and loadstatus < i_status;

  o_rowsaffected := SQL%ROWCOUNT;

END WVU_LOADSTATUSBYLOADNO_U_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_LOADSTOPSTATUSBYLOADNO_U_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_LOADSTOPSTATUSBYLOADNO_U_P (
  i_userid IN varchar2,
  i_loadno IN NUMBER, 
  i_stopno IN NUMBER, 
  i_status IN varchar2,     
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  update loadstop
       set loadstopstatus = i_status,
           lastuser = i_userid,
           lastupdate = sysdate
     where loadno = i_loadno
        and stopno = i_stopno
        and loadstopstatus < i_status;

  o_rowsaffected := SQL%ROWCOUNT;

END WVU_LOADSTOPSTATUSBYLOADNO_U_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_MARKLOADASPLANNED_U_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_MARKLOADASPLANNED_U_P (
  i_userid IN varchar2,
  i_loadno IN NUMBER,    
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  update loads
       set loadstatus = '2',
           lastuser = i_userid,
           lastupdate = sysdate
     where loadno = i_loadno
       and loadstatus > '2';

END WVU_MARKLOADASPLANNED_U_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_MSTRCRTNSHPGPLTBYORDR_D_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_MSTRCRTNSHPGPLTBYORDR_D_P (
  i_orderid IN NUMBER,  
  i_shipid IN NUMBER,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  delete from shippingplate Q
 where orderid = i_orderid
   and shipid = i_shipid
   and type in ('M','C')
   and not exists
    (select *
       from shippingplate
      where orderid = i_orderid
        and shipid = i_shipid
        and type in ('F','P')
        and parentlpid = Q.lpid);

  o_rowsaffected := SQL%ROWCOUNT;

END WVU_MSTRCRTNSHPGPLTBYORDR_D_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_MSTRCRTNSHPGPLTBYORDR_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_MSTRCRTNSHPGPLTBYORDR_R_P ( 
  i_orderid IN NUMBER, 
  i_shipid IN NUMBER, 
  c_shippingplates OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_shippingplates FOR
    select lpid, fromlpid
              from shippingplate Q
             where orderid = i_orderid
               and shipid = i_shipid
               and type in ('M','C')
               and fromlpid is not null
               and not exists
                (select *
                   from shippingplate
                  where orderid = i_orderid
                    and shipid = i_shipid
                    and type in ('F','P')
                    and parentlpid = Q.lpid);

END WVU_MSTRCRTNSHPGPLTBYORDR_R_P;



 
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_ORDERCOUNTBYLOADNO_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_ORDERCOUNTBYLOADNO_R_P ( 
  i_loadno IN NUMBER,
  o_rowscount OUT NUMBER
) AS
BEGIN

    SELECT count(1)
      INTO o_rowscount
      FROM orderhdr
   WHERE loadno = i_loadno
     AND orderstatus > '2';
EXCEPTION WHEN OTHERS THEN
    o_rowscount := 0;

END WVU_ORDERCOUNTBYLOADNO_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_ORDERCOUNTBYWAVE_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_ORDERCOUNTBYWAVE_R_P ( 
  i_wave IN NUMBER, 
  o_rowscount OUT NUMBER
) AS
BEGIN
 	SELECT COUNT(1)
      INTO o_rowscount
      FROM orderhdr
    WHERE wave = i_wave;
	EXCEPTION WHEN OTHERS THEN
    	o_rowscount := 0;

END WVU_ORDERCOUNTBYWAVE_R_P;



 
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_ORDERHEADER_U_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_ORDERHEADER_U_P
(
i_orderstatus IN VARCHAR2,
i_commitstatus IN VARCHAR2,
i_wave IN NUMBER,
i_priority IN VARCHAR2,
i_userid IN VARCHAR2,
i_orderid IN NUMBER,
i_shipid IN NUMBER,
o_rowsaffected OUT NUMBER
) AS 
BEGIN  

update orderhdr
   set orderstatus = NVL(i_orderstatus,orderstatus),
       commitstatus = NVL(i_commitstatus,commitstatus),
       wave = NVL(i_wave,wave),
       priority  = NVL(i_priority,priority),
       lastuser = i_userid,
       lastupdate = sysdate
 where ORDERID = i_orderid
    AND SHIPID = i_shipid;

o_rowsaffected := SQL%ROWCOUNT;

END WVU_ORDERHEADER_U_P;
  
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_ORDERHISTORY_I_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_ORDERHISTORY_I_P (
  i_orderid IN NUMBER,
  i_shipid IN NUMBER,
  i_action IN VARCHAR2,
  i_lpid IN VARCHAR2,
  i_item IN VARCHAR2,
  i_user IN VARCHAR2,
  i_lotnumber IN VARCHAR2,
  i_msg IN VARCHAR2,
  o_rowsaffected OUT NUMBER
) AS
BEGIN

INSERT
  INTO ORDERHISTORY
    (
    	chgdate,
    	orderid,
	    shipid,
	    userid,
	    action,
      lpid,
      item,
      lot,
	    msg
    )
    VALUES
    (
      SYSDATE,
      i_orderid,
      i_shipid,
      i_user,
      i_action,
      i_lpid,
      i_item,
      i_lotnumber,
      i_msg
    );

  o_rowsaffected := SQL%ROWCOUNT;


END WVU_ORDERHISTORY_I_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_ORDRCOUNTWITHPICKS_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_ORDRCOUNTWITHPICKS_R_P ( 
  i_wave IN NUMBER, 
  o_rowscount OUT NUMBER
) AS
BEGIN

     SELECT COUNT(1)
      INTO o_rowscount
      FROM orderhdr
    WHERE wave = i_wave
        and NVL(QTYPICK,0) != 0;
EXCEPTION WHEN OTHERS THEN
    o_rowscount := 1;
END WVU_ORDRCOUNTWITHPICKS_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_PASSEDTASKSCOUNTBYID_R_P.sql
--*****************************************************

create or replace PROCEDURE WVU_PASSEDTASKSCOUNTBYID_R_P ( 
  i_taskid IN NUMBER, 
  o_rowscount OUT NUMBER
) AS
BEGIN

    SELECT count(1)
      INTO o_rowscount
      FROM tasks
    WHERE taskid = i_taskid
       AND priority = '8';
EXCEPTION WHEN OTHERS THEN
    o_rowscount := 0;
END WVU_PASSEDTASKSCOUNTBYID_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_PLATEBYLPID_D_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_PLATEBYLPID_D_P (
  i_lpid IN varchar2,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  delete from plate
     where lpid = i_lpid;

  o_rowsaffected := SQL%ROWCOUNT;

END WVU_PLATEBYLPID_D_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_SUBTASKBYROWID_D_P.sql
--*****************************************************

create or replace PROCEDURE WVU_SUBTASKBYROWID_D_P (
  i_rowid IN ROWID,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  DELETE 
    FROM SUBTASKS
    WHERE ROWID = i_rowid;
  o_rowsaffected := SQL%ROWCOUNT;


END WVU_SUBTASKBYROWID_D_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_SUBTASKSBYORDER_R_P.sql
--*****************************************************

create or replace PROCEDURE WVU_SUBTASKSBYORDER_R_P ( 
  i_orderid IN NUMBER,  
  i_shipid IN NUMBER, 
  c_subtasks OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_subtasks FOR
      select rowid,
         taskid,
         custid,
         facility,
         lpid,
         shippinglpid
    from subtasks
   where orderid = i_orderid
     and shipid = i_shipid
     and not exists
       (select * from tasks
         where subtasks.taskid = tasks.taskid
           and tasks.priority = '0');

END WVU_SUBTASKSBYORDER_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_SUBTASKSBYWAVE_R_P.sql
--*****************************************************

create or replace PROCEDURE WVU_SUBTASKSBYWAVE_R_P ( 
  i_wave IN NUMBER, 
	i_tasktype IN VARCHAR2, 
  c_subtasks OUT SYS_REFCURSOR
) AS
BEGIN

 OPEN c_subtasks FOR
    select ROWID,
         TASKID,
         CUSTID,
         FACILITY,
         LPID, 
         TASKTYPE,
         ORDERITEM,
         ITEM,
         ORDERLOT
    from SUBTASKS
   where wave = i_wave
     and ((TASKTYPE is NULL AND i_tasktype is NULL ) OR TASKTYPE = i_tasktype);

END WVU_SUBTASKSBYWAVE_R_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_SUBTSKCOUNTBYTSKID_R_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_SUBTSKCOUNTBYTSKID_R_P ( 
  i_taskid IN NUMBER, 
  o_rowscount OUT NUMBER
) AS
BEGIN
 	SELECT COUNT(1)
      INTO o_rowscount
      FROM SUBTASKS
    WHERE taskid = i_taskid;
	EXCEPTION WHEN OTHERS THEN
    	o_rowscount := 0;

END WVU_SUBTSKCOUNTBYTSKID_R_P;



 
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_TASKBYTASKID_D_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_TASKBYTASKID_D_P (
  i_taskid IN NUMBER,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

  delete from tasks
     where taskid = i_taskid;

  o_rowsaffected := SQL%ROWCOUNT;

END WVU_TASKBYTASKID_D_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVU_TASKQTYBYTASKID_U_P.sql
--*****************************************************

CREATE OR REPLACE PROCEDURE WVU_TASKQTYBYTASKID_U_P (
  i_taskid IN NUMBER,  
  i_qty IN NUMBER,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN

   update tasks
     set qty = qty - i_qty
   where taskid = i_taskid
     and qty > i_qty;

  o_rowsaffected := SQL%ROWCOUNT;

END WVU_TASKQTYBYTASKID_U_P;
/

--*****************************************************
-- Procedures
-- @version 1.0.0
-- @File C:/Synapse.Backgrounds/Synapse.Backgrounds/sql\alps\Procedures\WVR_CARTONGROUPBYCODE_R_P .sql
--*****************************************************

create or replace PROCEDURE WVR_CARTONGROUPBYCODE_R_P ( 
  i_code IN VARCHAR2,
  o_cartongroup OUT VARCHAR2
) AS
BEGIN

     select cartongroup
  		into o_cartongroup 
  	from cartongroups
	where code = i_code 
   		and rownum < 2;
	EXCEPTION WHEN OTHERS THEN
  		o_cartongroup := '';


END WVR_CARTONGROUPBYCODE_R_P;
/


--###############################
--##  Data
--###############################

--******************************************
--****  Total Tables Count : 0
--****  Total Indexes Count : 0
--****  Total Permissions Count : 0
--****  Total Views Count : 0
--****  Total Functions Count : 0
--****  Total Procedures Count : 99
--****  Total Data Count : 0
--******************************************
