
/* Sql Scripts - 8/14/2019 2:41:31 PM */


--###############################
--##  Tables
--###############################



--###############################
--##  Indexes
--###############################



--###############################
--##  Permissions
--###############################



--###############################
--##  Views
--###############################



--###############################
--##  Functions
--###############################



--###############################
--##  Procedures
--###############################


--*****************************************************
-- Procedures
-- @version 1.3.0
-- @File C:/Projects/test.synapse.background/sql\alps\Procedures\WVU_TASKQTYBYTASKID_U_P.sql
--*****************************************************

create or replace PROCEDURE WVU_TASKQTYBYTASKID_U_P (
  i_taskid IN NUMBER,  
  i_qty IN NUMBER,  
  o_rowsaffected OUT NUMBER
) AS
BEGIN




   update tasks
     set qty = qty - i_qty
   where taskid = i_taskid
     and qty > i_qty;

  o_rowsaffected := SQL%ROWCOUNT;

END WVU_TASKQTYBYTASKID_U_P;
/


--###############################
--##  Data
--###############################

--******************************************
--****  Total Tables Count : 0
--****  Total Indexes Count : 0
--****  Total Permissions Count : 0
--****  Total Views Count : 0
--****  Total Functions Count : 0
--****  Total Procedures Count : 1
--****  Total Data Count : 0
--******************************************
