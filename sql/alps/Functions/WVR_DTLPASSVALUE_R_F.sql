create or replace FUNCTION WVR_DTLPASSVALUE_R_F (
  i_field IN VARCHAR2,
  i_orderid IN number,
  i_shipid IN number,
  i_item      varchar2,
  i_lotnumber varchar2
) RETURN number
IS
    l_sql varchar2(1024) := null;
    l_value varchar2(255) := null;
BEGIN


   if lower(substr(i_field, 1, 6)) in ('gc3dtl', 'asndtl') then
      l_sql := 'select ' || i_field
            || ' from orderdtlpassthrus'
            || ' where orderid = ' || i_orderid
            || ' and shipid = ' || i_shipid
            || ' and item = ''' || i_item || ''''
            || ' and nvl(lotnumber,''x'') = ''' || nvl(i_lotnumber,'x') || '''';
   elsif lower(substr(i_field, 1, 3)) = 'dtl' then
      l_sql := 'select ' || i_field
            || ' from orderdtl'
            || ' where orderid = ' || i_orderid
            || ' and shipid = ' || i_shipid
            || ' and item = ''' || i_item || ''''
            || ' and nvl(lotnumber,''x'') = ''' || nvl(i_lotnumber,'x') || '''';
   end if;

   if l_sql is not null then
      execute immediate l_sql into l_value;
   end if;

   return l_value;

exception
   when OTHERS then
      return 0;

END WVR_DTLPASSVALUE_R_F;