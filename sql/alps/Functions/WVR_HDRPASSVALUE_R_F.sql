create or replace FUNCTION WVR_HDRPASSVALUE_R_F (
  i_field IN VARCHAR2,
  i_orderid IN VARCHAR2,
  i_shipid IN VARCHAR2
) RETURN varchar2
IS
    l_sql varchar2(1024) := null;
    l_value varchar2(255) := null;
BEGIN

 if lower(substr(i_field, 1, 6)) in ('gc3hdr', 'asnhdr', 'ediaud') then
      l_sql := 'select ' || i_field
            || ' from orderhdrpassthrus'
            || ' where orderid = ' || i_orderid
            || ' and shipid = ' || i_shipid;
   elsif lower(substr(i_field, 1, 3)) = 'hdr' then
      l_sql := 'select ' || i_field
            || ' from orderhdr'
            || ' where orderid = ' || i_orderid
            || ' and shipid = ' || i_shipid;
   end if;

   if l_sql is not null then
      execute immediate l_sql into l_value;
   end if;

   return l_value;

exception
   when OTHERS then
      return null;

END WVR_HDRPASSVALUE_R_F;