﻿// <copyright file="ConfigurationDependencyModule.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2017-07-06</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2017-07-06</lastchangeddate>
namespace Synapse.Backgrounds.Configuration.DependencyResolver
{
    using System.Reflection;
    using Autofac;
    using Module = Autofac.Module;

    public class ConfigurationDependencyModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ConfigService>().As<IConfigService>().SingleInstance();
        }
    }
}