﻿// <copyright file="IConfigService.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2017-07-06</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2017-07-06</lastchangeddate>
namespace Synapse.Backgrounds.Configuration
{
    using System.Collections.Generic;
    using ControlBus;
    using EnvironmentContexts;
    using Integration;
    using MessageQueueContexts;

    public interface IConfigService
    {
        string LoggingDateTimeZoneId();
        string GetAppSetting(string settingKey);
        string GetConnectionString(string connectionKey, bool isAppSettingKey);
        string GetConnectionString(string environment);
        IList<ServiceContext> GetServiceContexts();
        IEnumerable<EnvironmentContext> GetEnvironmentContexts();
        ControlBusSetting GetControlBusSetting();
        IList<QueueContext> GetQueueContexts();
    }
}