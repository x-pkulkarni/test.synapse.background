﻿// <copyright file="ConfigService.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2017-07-06</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2017-07-06</lastchangeddate>
namespace Synapse.Backgrounds.Configuration
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using ControlBus;
    using EnvironmentContexts;
    using Integration;
    using MessageQueueContexts;

    public class ConfigService : IConfigService
    {
        private static readonly ConcurrentDictionary<string, string> AppSettings = new ConcurrentDictionary<string, string>();
        private static readonly ConcurrentDictionary<string, string> ConnectionStrings = new ConcurrentDictionary<string, string>();
        private IList<ServiceContext> _serviceContexts;
        private bool? _enableSerilogFileLogging;
        private const string TimeZoneId = "Central Standard Time";
        private static IList<QueueContext> _queueContexts;

        public string LoggingDateTimeZoneId()
        {
            return TimeZoneId;
        }

        public string GetAppSetting(string settingKey)
        {
            if (!AppSettings.ContainsKey(settingKey))
            {
                if (ConfigurationManager.AppSettings.Count > 0 && ConfigurationManager.AppSettings[settingKey] != null)
                {
                    AppSettings.TryAdd(settingKey, ConfigurationManager.AppSettings[settingKey]);
                }
                else
                {
                    AppSettings.TryAdd(settingKey, string.Empty);
                }
            }

            return AppSettings.FirstOrDefault(s => s.Key.Equals(settingKey)).Value;
        }

        public string GetConnectionString(string connectionKey, bool isAppSettingKey)
        {
            var connectionName = isAppSettingKey ? GetAppSetting(connectionKey) : connectionKey;
            if (!ConnectionStrings.ContainsKey(connectionName))
            {
                if (ConfigurationManager.ConnectionStrings.Count > 0 &&
                    ConfigurationManager.ConnectionStrings[connectionName] != null)
                {
                    ConnectionStrings.TryAdd(connectionName,
                        ConfigurationManager.ConnectionStrings[connectionName]
                            .ConnectionString);
                }
                else
                {
                    ConnectionStrings.TryAdd(connectionName, string.Empty);
                }
            }

            return ConnectionStrings.FirstOrDefault(s => s.Key.Equals(connectionName)).Value;
        }

        public string GetConnectionString(string environment)
        {
            var environmentContextSection =
                ConfigurationManager.GetSection("Environments") as EnvironmentContextSection;

            if (environmentContextSection?.EnvironmentContexts == null ||
                environmentContextSection.EnvironmentContexts.Count < 1)
            {
                return string.Empty;
            }

            var connectionKey =
            (from EnvironmentContext currentContext in environmentContextSection.EnvironmentContexts
             select new
             {
                 ConnectionKey = currentContext.ConnectionKey,
                 IsActive = currentContext.IsActive
             }).First(p => p.IsActive).ConnectionKey;

            return GetConnectionString(connectionKey, false);

        }

        public IList<ServiceContext> GetServiceContexts()
        {
            if (_serviceContexts != null)
            {
                return _serviceContexts;
            }

            GetQueueContexts();
            return GetActiveEnvironmentContexts();
        }

        public IEnumerable<EnvironmentContext> GetEnvironmentContexts()
        {
            var environmentContextSection =
                ConfigurationManager.GetSection("Environments") as EnvironmentContextSection;

            if (environmentContextSection?.EnvironmentContexts == null ||
                environmentContextSection.EnvironmentContexts.Count < 1)
            {
                return new List<EnvironmentContext>();
            }

            var contextCollection = environmentContextSection.EnvironmentContexts;
            return from EnvironmentContext currentContext in contextCollection
                   select currentContext;
        }

        public ControlBusSetting GetControlBusSetting()
        {
            return ConfigurationManager.GetSection("ControlBus") as ControlBusSetting;
        }

        public IList<QueueContext> GetQueueContexts()
        {
            return _queueContexts ?? GetActiveQueueContexts();
        }

        #region Private Methods

        private IList<ServiceContext> GetActiveEnvironmentContexts()
        {
            var environmentContextSection =
                ConfigurationManager.GetSection("Environments") as EnvironmentContextSection;

            if (environmentContextSection?.EnvironmentContexts == null ||
                environmentContextSection.EnvironmentContexts.Count < 1)
            {
                return new List<ServiceContext>();
            }

            _serviceContexts = (from EnvironmentContext currentContext in environmentContextSection.EnvironmentContexts
                                select new ServiceContext
                                {
                                    Key = currentContext.Key,
                                    Environment = currentContext.Name,
                                    ConnectionKey = currentContext.ConnectionKey,
                                    OracleQueue = currentContext.OracleQueue,
                                    CanDequeueArray = currentContext.CanDequeueArray,
                                    DequeueMessageCount = currentContext.DequeueMessageCount,
                                    NoOfThreads = currentContext.NoOfThreads,
                                    SleepInSeconds = currentContext.SleepInSeconds,
                                    IsActive = currentContext.IsActive,
                                    ServiceProcessors = (from ProcessorContext processorContext in currentContext.ProcessorContexts
                                                         select new ServiceProcessor
                                                         {
                                                             QueueConnectionKey = processorContext.QueueConnectionKey,
                                                             IsActive = processorContext.IsActive,
                                                             QueueContext = _queueContexts.FirstOrDefault(q => string.Equals(q.Name, processorContext.QueueConnectionKey,
                                                                 StringComparison.InvariantCultureIgnoreCase)),
                                                             CustomerQueues = (from CustomerContext customerContext in processorContext.CustomerContexts
                                                                               select new CustomerQueue
                                                                               {
                                                                                   CustId = customerContext.CustId,
                                                                                   SubscriptionId = customerContext.SubscriptionId,
                                                                                   QueueTopic = customerContext.QueueTopic,
                                                                                   NoOfSubscriptions = customerContext.NoOfSubscriptions,
                                                                                   IsActive = customerContext.IsActive
                                                                               }).Where(c => c.IsActive).ToList()
                                                         }).Where(p => p.IsActive).ToList(),
                                    ApiEndPoints = (from ApiEndPointElement apiEndPoint in currentContext.ApiEndPoints
                                                    select new ApiEndPoint
                                                    {
                                                        Key = apiEndPoint.Key,
                                                        Url = apiEndPoint.Url
                                                    }).ToList()
                                }).Where(x => x.IsActive).ToList();

            return _serviceContexts;
        }

        private static IList<QueueContext> GetActiveQueueContexts()
        {
            var queueContextSection =
                ConfigurationManager.GetSection("QueueContextSection") as QueueContextSection;

            if (queueContextSection?.QueueContexts == null ||
                queueContextSection.QueueContexts.Count < 1)
            {
                return new List<QueueContext>();
            }

            _queueContexts = (from QueueContextElement context in queueContextSection.QueueContexts
                select new QueueContext
                {
                    Name = context.Name,
                    PublisherConnectionString = context.PublisherConnectionString,
                    Subscribers = (from SubscriberElement node in context.Subscribers
                        select new Subscriber
                        {
                            Id = node.Id,
                            ConnectionString = node.ConnectionString,
                            IsActive = node.IsActive
                        }).Where(n => n.IsActive).ToList(),
                    IsActive = context.IsActive
                }).Where(x => x.IsActive).ToList();

            return _queueContexts;
        }

        #endregion
    }
}