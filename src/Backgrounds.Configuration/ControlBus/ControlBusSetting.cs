﻿// <copyright file="ControlBusSetting.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2017-07-25</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2017-07-25</lastchangeddate>
namespace Synapse.Backgrounds.Configuration.ControlBus
{
    using System.Configuration;
    public class ControlBusSetting : ConfigurationSection
    {
        [ConfigurationProperty("Key", IsKey = true, IsRequired = true)]
        public string Key => (string)this["Key"];

        [ConfigurationProperty("MessageQueueKey", IsRequired = false)]
        public string MessageQueueKey => (string)this["MessageQueueKey"];

        [ConfigurationProperty("SubscriptionKey", IsRequired = false)]
        public string SubscriptionKey => (string)this["SubscriptionKey"];

        [ConfigurationProperty("Topic", IsRequired = false)]
        public string Topic => (string)this["Topic"];

        [ConfigurationProperty("PrefetchCount", IsRequired = false, DefaultValue = (ushort)1)]
        public ushort PrefetchCount => (ushort)this["PrefetchCount"];

        [ConfigurationProperty("IsActive", IsRequired = false, DefaultValue = true)]
        public bool IsActive => (bool)this["IsActive"];
    }
}