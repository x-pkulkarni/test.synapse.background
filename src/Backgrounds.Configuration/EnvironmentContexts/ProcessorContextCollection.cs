﻿// <copyright file="ProcessorContextCollection.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2017-12-20</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2017-12-20</lastchangeddate>
namespace Synapse.Backgrounds.Configuration.EnvironmentContexts
{
    using System.Configuration;
    public class ProcessorContextCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ProcessorContext();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ProcessorContext) element).Key;
        }

        protected override string ElementName => "ProcessorContext";

        protected override bool IsElementName(string elementName)
        {
            return !string.IsNullOrEmpty(elementName) && elementName == "ProcessorContext";
        }

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.BasicMap;

        public ProcessorContext this[int idx] => BaseGet(idx) as ProcessorContext;

        public new ProcessorContext this[string key] => BaseGet(key) as ProcessorContext;
    }
}