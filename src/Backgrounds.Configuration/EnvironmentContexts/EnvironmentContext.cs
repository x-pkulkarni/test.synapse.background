﻿// <copyright file="EnvironmentContext.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2017-07-06</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2017-07-06</lastchangeddate>
namespace Synapse.Backgrounds.Configuration.EnvironmentContexts
{
    using System.Configuration;
    public class EnvironmentContext: ConfigurationElement
    {
        [ConfigurationProperty("Key", IsKey = true, IsRequired = true)]
        public int Key => (int)this["Key"];

        [ConfigurationProperty("Name", IsRequired = true)]
        public string Name => (string)this["Name"];

        [ConfigurationProperty("ConnectionKey", IsRequired = true)]
        public string ConnectionKey => (string)this["ConnectionKey"];

        [ConfigurationProperty("OracleQueue", IsRequired = true)]
        public string OracleQueue => (string)this["OracleQueue"];

        [ConfigurationProperty("CanDequeueArray", IsRequired = false, DefaultValue = false)]
        public bool CanDequeueArray => (bool)this["CanDequeueArray"];

        [ConfigurationProperty("DequeueMessageCount", IsRequired = false, DefaultValue = 1)]
        public int DequeueMessageCount => (int)this["DequeueMessageCount"];

        [ConfigurationProperty("NoOfThreads", IsRequired = false, DefaultValue = 1)]
        public int NoOfThreads => (int)this["NoOfThreads"];

        [ConfigurationProperty("SleepInSeconds", IsRequired = false, DefaultValue = 0)]
        public int SleepInSeconds => (int)this["SleepInSeconds"];

        [ConfigurationProperty("IsActive", IsRequired = false, DefaultValue = true)]
        public bool IsActive => (bool)this["IsActive"];

        [ConfigurationProperty("ProcessorContexts", IsDefaultCollection = false)]
        public ProcessorContextCollection ProcessorContexts => (ProcessorContextCollection)base["ProcessorContexts"];

        [ConfigurationProperty("ApiEndPoints", IsDefaultCollection = false)]
        public ApiEndPointCollection ApiEndPoints => (ApiEndPointCollection)base["ApiEndPoints"];
    }
}