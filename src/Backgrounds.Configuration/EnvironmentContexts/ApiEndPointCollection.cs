﻿// <copyright file="ApiEndPointCollection.cs" company="GEODIS">
// Copyright (c) 2019 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2019-06-10</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2019-06-10</lastchangeddate>

namespace Synapse.Backgrounds.Configuration.EnvironmentContexts
{
    using System.Configuration;

    public class ApiEndPointCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new ApiEndPointElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ApiEndPointElement)element).Key;
        }

        protected override string ElementName => "ApiEndPoint";

        protected override bool IsElementName(string elementName)
        {
            return !string.IsNullOrEmpty(elementName) && elementName == "ApiEndPoint";
        }

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.BasicMap;

        public ApiEndPointElement this[int idx] => BaseGet(idx) as ApiEndPointElement;

        public new ApiEndPointElement this[string key] => BaseGet(key) as ApiEndPointElement;
    }
}
