﻿// <copyright file="CustomerContext.cs" company="GEODIS">
// Copyright (c) 2019 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2019-06-06</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2019-06-06</lastchangeddate>

namespace Synapse.Backgrounds.Configuration.EnvironmentContexts
{
    using System.Configuration;

    public class CustomerContext : ConfigurationElement
    {
        [ConfigurationProperty("CustId", IsKey = true, IsRequired = true)]
        public string CustId => (string)this["CustId"];

        [ConfigurationProperty("SubscriptionId", IsRequired = false, DefaultValue = "")]
        public string SubscriptionId => (string)this["SubscriptionId"];

        [ConfigurationProperty("QueueTopic", IsRequired = true)]
        public string QueueTopic => (string)this["QueueTopic"];

        [ConfigurationProperty("NoOfSubscriptions", IsRequired = false, DefaultValue = 1)]
        public int NoOfSubscriptions => (int)this["NoOfSubscriptions"];

        [ConfigurationProperty("IsActive", IsRequired = false, DefaultValue = true)]
        public bool IsActive => (bool)this["IsActive"];
    }
}