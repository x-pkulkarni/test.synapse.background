﻿// <copyright file="EnvironmentContextCollection.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2017-07-06</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2017-07-06</lastchangeddate>

namespace Synapse.Backgrounds.Configuration.EnvironmentContexts
{
    using System.Configuration;

    public class EnvironmentContextCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new EnvironmentContext();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((EnvironmentContext)element).Name;
        }

        protected override string ElementName => "EnvironmentContext";

        protected override bool IsElementName(string elementName)
        {
            return !string.IsNullOrEmpty(elementName) && elementName == "EnvironmentContext";
        }

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.BasicMap;

        public EnvironmentContext this[int idx] => BaseGet(idx) as EnvironmentContext;

        public new EnvironmentContext this[string key] => BaseGet(key) as EnvironmentContext;
    }
}
