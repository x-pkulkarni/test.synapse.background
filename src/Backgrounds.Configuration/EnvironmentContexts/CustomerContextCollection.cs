﻿// <copyright file="CustomerContextCollection.cs" company="GEODIS">
// Copyright (c) 2019 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2019-06-06</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2019-06-06</lastchangeddate>

namespace Synapse.Backgrounds.Configuration.EnvironmentContexts
{
    using System.Configuration;

    public class CustomerContextCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new CustomerContext();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((CustomerContext)element).CustId;
        }

        protected override string ElementName => "CustomerContext";

        protected override bool IsElementName(string elementName)
        {
            return !string.IsNullOrEmpty(elementName) && elementName == "CustomerContext";
        }

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.BasicMap;

        public CustomerContext this[int idx] => BaseGet(idx) as CustomerContext;

        public new CustomerContext this[string key] => BaseGet(key) as CustomerContext;
    }
}