﻿// <copyright file="ApiEndPointElement.cs" company="GEODIS">
// Copyright (c) 2019 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2019-06-10</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2019-06-10</lastchangeddate>

namespace Synapse.Backgrounds.Configuration.EnvironmentContexts
{
    using System.Configuration;

    public class ApiEndPointElement : ConfigurationElement
    {
        [ConfigurationProperty("Key", IsKey = true, IsRequired = true)]
        public string Key => (string)this["Key"];

        [ConfigurationProperty("Url", IsRequired = true)]
        public string Url => (string)this["Url"];
    }
}