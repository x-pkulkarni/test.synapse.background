﻿// <copyright file="ProcessorContext.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2017-12-20</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2017-12-20</lastchangeddate>
namespace Synapse.Backgrounds.Configuration.EnvironmentContexts
{
    using System.Configuration;
    public class ProcessorContext : ConfigurationElement
    {
        [ConfigurationProperty("Key", IsKey = true, IsRequired = true)]
        public int Key => (int)this["Key"];

        [ConfigurationProperty("QueueConnectionKey", IsRequired = true)]
        public string QueueConnectionKey => (string)this["QueueConnectionKey"];

        [ConfigurationProperty("IsActive", IsRequired = false, DefaultValue = true)]
        public bool IsActive => (bool)this["IsActive"];

        [ConfigurationProperty("CustomerContexts", IsDefaultCollection = false)]
        public CustomerContextCollection CustomerContexts => (CustomerContextCollection)base["CustomerContexts"];
    }
}