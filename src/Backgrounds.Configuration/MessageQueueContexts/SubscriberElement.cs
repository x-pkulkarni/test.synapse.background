﻿// <copyright file="QueueNodeElement.cs" company="GEODIS">
// Copyright (c) 2019 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2019-06-26</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2019-06-26</lastchangeddate>
namespace Synapse.Backgrounds.Configuration.MessageQueueContexts
{
    using System.Configuration;

    public class SubscriberElement : ConfigurationElement
    {
        [ConfigurationProperty("Id", IsRequired = true, IsKey = true)]
        public int Id => (int)base["Id"];

        [ConfigurationProperty("ConnectionString", IsRequired = true)]
        public string ConnectionString => (string)base["ConnectionString"];

        [ConfigurationProperty("IsActive", IsRequired = false, DefaultValue = true)]
        public bool IsActive => (bool)base["IsActive"];
    }
}