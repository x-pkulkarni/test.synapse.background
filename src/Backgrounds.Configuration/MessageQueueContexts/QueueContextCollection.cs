﻿// <copyright file="QueueContextCollection.cs" company="GEODIS">
// Copyright (c) 2019 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2019-06-26</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2019-06-26</lastchangeddate>
namespace Synapse.Backgrounds.Configuration.MessageQueueContexts
{
    using System.Configuration;

    [ConfigurationCollection(typeof(QueueContextElement))]
    public class QueueContextCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new QueueContextElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((QueueContextElement)element).Name;
        }

        protected override string ElementName => "QueueContext";

        protected override bool IsElementName(string elementName)
        {
            return !string.IsNullOrEmpty(elementName) && elementName == "QueueContext";
        }

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.BasicMap;

        public QueueContextElement this[int idx] => (QueueContextElement)BaseGet(idx);

        public new QueueContextElement this[string key] => (QueueContextElement)BaseGet(key);
    }
}