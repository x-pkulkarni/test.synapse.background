﻿// <copyright file="QueueContextElement.cs" company="GEODIS">
// Copyright (c) 2019 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2019-06-26</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2019-06-26</lastchangeddate>
namespace Synapse.Backgrounds.Configuration.MessageQueueContexts
{
    using System.Configuration;

    public class QueueContextElement : ConfigurationElement
    {
        [ConfigurationProperty("Name", IsRequired = true, IsKey = true)]
        public string Name => (string)base["Name"];

        [ConfigurationProperty("PublisherConnectionString", IsRequired = true)]
        public string PublisherConnectionString => (string)base["PublisherConnectionString"];

        [ConfigurationProperty("IsActive", IsRequired = false, DefaultValue = true)]
        public bool IsActive => (bool)base["IsActive"];

        [ConfigurationProperty("Subscribers", IsDefaultCollection = false)]
        public SubscriberCollection Subscribers => (SubscriberCollection)base["Subscribers"];
    }
}