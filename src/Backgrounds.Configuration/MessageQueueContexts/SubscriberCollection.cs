﻿// <copyright file="QueueNodeCollection.cs" company="GEODIS">
// Copyright (c) 2019 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Configuration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2019-06-26</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2019-06-26</lastchangeddate>
namespace Synapse.Backgrounds.Configuration.MessageQueueContexts
{
    using System.Configuration;

    [ConfigurationCollection(typeof(SubscriberElement))]
    public class SubscriberCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new SubscriberElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((SubscriberElement)element).Id;
        }

        protected override string ElementName => "Subscriber";

        protected override bool IsElementName(string elementName)
        {
            return !string.IsNullOrEmpty(elementName) && elementName == "Subscriber";
        }

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.BasicMap;

        public SubscriberElement this[int idx] => (SubscriberElement)BaseGet(idx);

        public new SubscriberElement this[string key] => (SubscriberElement)BaseGet(key);
    }
}
