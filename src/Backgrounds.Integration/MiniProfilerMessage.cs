﻿// <copyright file="MiniProfilerMessage.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Integration</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-27</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-27</lastchangeddate>

using System;
using System.Collections.Generic;

namespace Synapse.Backgrounds.Integration
{
    public class MiniProfilerMessage
    {
        public string RouteId { get; set; }
        public string RouteName { get; set; }
        public DateTime Started { get; set; }
        public decimal DurationMilliseconds { get; set; }
        public string MachineName { get; set; }
        public Dictionary<string, string> CustomLinks { get; set; }
        public List<DataBaseTiming> DataBaseTimings { get; set; }
    }
}