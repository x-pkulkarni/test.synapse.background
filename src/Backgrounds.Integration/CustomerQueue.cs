﻿// <copyright file="CustomerQueue.cs" company="GEODIS">
// Copyright (c) 2019 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Integration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2019-06-06</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2019-06-06</lastchangeddate>

namespace Synapse.Backgrounds.Integration
{
    using System.Collections.Generic;

    public class CustomerQueue
    {
        public string CustId { get; set; }
        public string SubscriptionId { get; set; }
        public string QueueTopic { get; set; }
        public int NoOfSubscriptions { get; set; }
        public bool IsActive { get; set; }

        public override string ToString()
        {
            return
                $"Customer Id: {CustId}, Subscription: { SubscriptionId }, Queue Topic: {QueueTopic}, No. Of Subscriptions: {NoOfSubscriptions}";
        }
    }
}