﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Backgrounds.Integration
{
    public class Enums
    {
        public enum RequestType
        {
            Undefined = 0,
            RELWAV,
            RESETWAV,
            UNRELWAV,
            GENLIPBP
        }
    }
}
