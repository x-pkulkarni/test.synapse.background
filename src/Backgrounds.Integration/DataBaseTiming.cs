﻿// <copyright file="DataBaseTiming.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Integration</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-27</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-27</lastchangeddate>

using System.Collections.Generic;

namespace Synapse.Backgrounds.Integration
{
    public class DataBaseTiming
    {
        public string DataBaseTimingId { get; set; }
        public string CommandString { get; set; }
        public string ExecuteType { get; set; }
        public string ParentDataBaseTimingId { get; set; }
        public string StackTraceSnippet { get; set; }
        public decimal StartMilliseconds { get; set; }
        public decimal DurationMilliseconds { get; set; }
        public decimal FirstFetchDurationMilliseconds { get; set; }
        public bool Errored { get; set; }
        public Dictionary<string, List<DataBaseTiming>> DataBaseTimings { get; set; }

    }
}