﻿// <copyright file="QueueContext.cs" company="GEODIS">
// Copyright (c) 2019 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Integration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2019-06-26</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2019-06-26</lastchangeddate>
namespace Synapse.Backgrounds.Integration
{
    using System.Collections.Generic;

    public class QueueContext
    {
        public string Name { get; set; }
        public string PublisherConnectionString { get; set; }
        public IList<Subscriber> Subscribers { get; set; }
        public bool IsActive { get; set; }
    }
}