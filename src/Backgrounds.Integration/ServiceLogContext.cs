﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Backgrounds.Integration
{
   public class ServiceLogContext
    {
        public string Facility { get; set; }
        public long SessionId { get; set; }
        public string CustomerId { get; set; }
        public string Environment { get; set; }
    }
}
