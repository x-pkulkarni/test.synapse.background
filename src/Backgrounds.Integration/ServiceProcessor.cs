﻿// <copyright file="ServiceProcessor.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Integration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2017-12-20</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2017-12-20</lastchangeddate>
namespace Synapse.Backgrounds.Integration
{
    using System.Collections.Generic;

    public class ServiceProcessor
    {
        public string QueueConnectionKey { get; set; }
        public bool IsActive { get; set; }
        public IList<CustomerQueue> CustomerQueues { get; set; }
        public QueueContext QueueContext { get; set; }
    }
}