﻿// <copyright file="ServiceTask.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Integration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2017-07-26</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2017-07-26</lastchangeddate>

namespace Synapse.Backgrounds.Integration
{
    using System.Threading;
    using System.Threading.Tasks;

    public class ServiceTask
    {
        public int TaskNumber { get; set; }
        public string TaskName { get; set; }
        public Task CurrentTask { get; set; }
        public CancellationTokenSource TaskCancellationTokenSource { get; set; }
        public CancellationTokenSource LinkedCancellationTokenSource { get; set; }
    }
}
