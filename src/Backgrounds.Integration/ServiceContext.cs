﻿// <copyright file="ServiceContext.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Integration</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2017-07-24</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2017-07-24</lastchangeddate>

namespace Synapse.Backgrounds.Integration
{
    using System.Collections.Generic;

    public class ServiceContext
    {
        public int Key { get; set; }
        public string Environment { get; set; }
        public string ConnectionKey { get; set; }
        public string OracleQueue { get; set; }
        public bool CanDequeueArray { get; set; } = false;
        public int DequeueMessageCount { get; set; }
        public int NoOfThreads { get; set; }
        public int SleepInSeconds { get; set; }
        public bool IsActive { get; set; }
        public IList<ServiceProcessor> ServiceProcessors { get; set; }
        public IList<ApiEndPoint> ApiEndPoints { get; set; }
        public ServiceStatus Status { get; set; } = ServiceStatus.Running;
        public IList<ServiceTask> ActiveTasks { get; set; }
        public ushort PrefetchCount { get; set; }

        public ServiceContext()
        {
            ServiceProcessors = new List<ServiceProcessor>();
            ApiEndPoints = new List<ApiEndPoint>();
            ActiveTasks = new List<ServiceTask>();
        }

        public override string ToString()
        {
            return $"Key: {Key}, Environment: {Environment}, ConnectionKey: {ConnectionKey}, IsActive: {IsActive}";
        }
    }

    public enum ServiceStatus
    {
        Running = 1,
        Stopped
    }
}
