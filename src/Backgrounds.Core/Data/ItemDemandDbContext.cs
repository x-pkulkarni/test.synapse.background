﻿// <copyright file="ItemDemandDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-03-07</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-03-07</lastchangeddate>

using System.Data;
using Dapper;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data
{
    public class ItemDemandDbContext :IItemDemandDbContext
    {
        public bool Insert(IDbConnection connection, ItemDemand itemDemand)
        {
            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@i_facility", itemDemand.Facility);
            parameters.Add("@i_item", itemDemand.Item);
            parameters.Add("@i_lotnumber", itemDemand.LotNumber);
            parameters.Add("@i_priority", itemDemand.Priority);
            parameters.Add("@i_invstatusind", itemDemand.InvStatusInd);
            parameters.Add("@i_invclassind", itemDemand.InvClassInd);
            parameters.Add("@i_invstatus", itemDemand.InvStatus);
            parameters.Add("@i_inventoryclass", itemDemand.InventoryClass);
            parameters.Add("@i_demandtype", itemDemand.DemandType);
            parameters.Add("@i_orderid", itemDemand.OrderId);
            parameters.Add("@i_shipid", itemDemand.ShipId);
            parameters.Add("@i_loadno", itemDemand.LoadNo);
            parameters.Add("@i_stopno", itemDemand.StopNo);
            parameters.Add("@i_shipno", itemDemand.ShipNo);
            parameters.Add("@i_orderitem", itemDemand.OrderItem);
            parameters.Add("@i_orderlot", itemDemand.OrderLot);
            parameters.Add("@i_qty", itemDemand.Qty);
            parameters.Add("@i_userid", itemDemand.LastUser);
            parameters.Add("@i_custid", itemDemand.CustId);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);

            connection.Execute("RGP_ITEMDEMAND_I_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowsaffected") > 0;
        }
    }
}