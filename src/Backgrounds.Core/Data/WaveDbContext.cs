﻿// <copyright file="WaveDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-10</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-10</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data
{
    public class WaveDbContext : IWaveDbContext
    {
        public IEnumerable<Wave> GetWaveById(IDbConnection connection, long waveId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_id", waveId);
            parameters.Add("@c_waves", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<Wave>("WVR_WAVES_R_P", parameters, commandType: CommandType.StoredProcedure);
        }

        public bool UpdateWaveStatus(IDbConnection connection, long waveId, string status, string lastUser)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_id", waveId);
            parameters.Add("@i_status", status);
            parameters.Add("@i_lastuser", lastUser);
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_WAVESTATUS_U_P", parameters, commandType: CommandType.StoredProcedure);

            return (int) parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public bool UpdateProcessingIndicatorStart(IDbConnection connection, long wave)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@i_processing_ind", null);
            parameters.Add("@i_process_start_tm", null);
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_PROCESSINGINDSTART_U_P", parameters, commandType: CommandType.StoredProcedure);
            return (int) parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public bool UpdateProcessingIndicatorEnd(IDbConnection connection, long wave)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@i_processing_ind", "N");
            parameters.Add("@i_process_end_tm", DateTime.Now);
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_PROCESSINGINDEND_U_P", parameters, commandType: CommandType.StoredProcedure);
            return (int)parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public bool IsCustomerSetupForCShip(IDbConnection connection, string custid)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custid);
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_CUSTCONNECTSHIP_R_P", parameters, commandType: CommandType.StoredProcedure);
            return (int)parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public bool IsCustomerSetupForFel(IDbConnection connection, string custid)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custid);
            parameters.Add("@o_rowscount", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_CUSTFEL_R_P", parameters, commandType: CommandType.StoredProcedure);
            return (int)parameters.Get<OracleDecimal>("o_rowscount") > 0;
        }

        public IEnumerable<WaveItem> GetItemsInWave(IDbConnection connection, long wave)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@c_citems", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<WaveItem>("WVR_FELWAVEITEMS_R_P", parameters, commandType: CommandType.StoredProcedure);
        }


        public int GetCustLookupCount(IDbConnection connection, string custId, string code)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custId);
            parameters.Add("@i_code", code);
            parameters.Add("@o_cust_lookup_count", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_GETCUSTLOOKUPCOUNT_R_P", parameters, commandType: CommandType.StoredProcedure);
            return (int) parameters.Get<OracleDecimal>("o_cust_lookup_count");
        }

     
        public IEnumerable<CustLookupDtl> GetCustLookupDtl(IDbConnection connection, string custId, string shipToCountryCode)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custId);
            parameters.Add("@i_ship_to_country_code", shipToCountryCode);
            parameters.Add("@c_cust_lookup_dtl", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<CustLookupDtl>("WVR_GETCUSTLOOKUPDTL_R_P", parameters, commandType: CommandType.StoredProcedure);
        }

    }
}
