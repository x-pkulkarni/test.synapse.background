﻿// <copyright file="CustomerAuxDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-19</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Dapper;
using Oracle.DataAccess.Client;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data
{
    public class CustomerAuxDbContext : ICustomerAuxDbContext
    {
        public IEnumerable<CustomerAux> GetCustomerSettings(IDbConnection connection, string custId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custId);
            parameters.Add("@c_customer", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<CustomerAux>("WVR_GETCUSTSETTINGS_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public string GetPutwallExemptFlag(IDbConnection connection, string custId, string item)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", custId);
            parameters.Add("@i_item", item);
            parameters.Add("@o_exempt", dbType: DbType.String, direction: ParameterDirection.Output, size: 2);
            connection.Execute("WVR_GETPUTWALLEXEMPT_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<string>("o_exempt");
        }

        public int GetPutwallCount(IDbConnection connection, string custId, string facility)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", custId);
            parameters.Add("@i_facility", facility);
            parameters.Add("@o_pwcnt", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_GETPUTWALLCOUNT_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_pwcnt");
        }


        public int GetAllocRuleCount(IDbConnection connection, string custId, string facility,string item,string allocNeed)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", custId);
            parameters.Add("@i_facility", facility);
            parameters.Add("@i_item", item);
            parameters.Add("@i_allocneed", allocNeed);
            parameters.Add("@o_bcount", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_GETALLOCRULECNT_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_bcount");
        }
    }
}