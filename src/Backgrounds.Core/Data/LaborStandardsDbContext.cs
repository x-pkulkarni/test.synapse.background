﻿// <copyright file="LaborStandardsDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-26</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Dapper;
using Oracle.DataAccess.Client;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data
{
    public class LaborStandardsDbContext : ILaborStandardsDbContext
    {
        public IEnumerable<LaborStandard> GetLaborStdQtyPerHourForUom(IDbConnection connection,
            LaborStandard laborStandard)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_facility", laborStandard.Facility);
            parameters.Add("@i_custid", laborStandard.CustId);
            parameters.Add("@i_category", laborStandard.Category);
            parameters.Add("@i_zoneid", laborStandard.ZoneId);
            parameters.Add("@i_uom", laborStandard.Uom);
            parameters.Add("@c_qtyperhour", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<LaborStandard>("WVR_LBRSTDSFORUOM_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<LaborStandard> GetLaborStdQtyPerHourForDiffUom(IDbConnection connection,
            LaborStandard laborStandard)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_facility", laborStandard.Facility);
            parameters.Add("@i_custid", laborStandard.CustId);
            parameters.Add("@i_category", laborStandard.Category);
            parameters.Add("@i_zoneid", laborStandard.ZoneId);
            parameters.Add("@c_laborstd", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<LaborStandard>("WVR_LBRSTDSFORDIFFUOM_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }
    }
}