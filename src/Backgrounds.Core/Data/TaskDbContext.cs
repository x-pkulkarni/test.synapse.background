﻿// <copyright file="TaskDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-11</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-26</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Dapper;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data
{
    public class TaskDbContext : ITaskDbContext
    {
        public bool Insert(IDbConnection connection, Task task)
        {
            #region Parameters

            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@i_taskid", task.TaskId);
            parameters.Add("@i_tasktype", task.TaskType);
            parameters.Add("@i_facility",  task.Facility);
            parameters.Add("@i_fromsection",  task.FromSection);
            parameters.Add("@i_fromlocation", task.FromLocation);
            parameters.Add("@i_fromprofile", task.FromProfile);
            parameters.Add("@i_tosection", task.ToSection);
            parameters.Add("@i_tolocation", task.ToLocation);
            parameters.Add("@i_toprofile", task.ToProfile);
            parameters.Add("@i_custid", task.CustomerId);
            parameters.Add("@i_item", task.Item);
            parameters.Add("@i_lpid", task.LpId);
            parameters.Add("@i_uom", task.Uom);
            parameters.Add("@i_quantity", task.Quantity, DbType.Decimal);
            parameters.Add("@i_locseq", task.LocSeq, DbType.Decimal);
            parameters.Add("@i_loadno", task.LoadNo);
            parameters.Add("@i_shipno", task.ShipNo);
            parameters.Add("@i_stopno", task.StopNo);
            parameters.Add("@i_orderid", task.OrderId, DbType.Decimal);
            parameters.Add("@i_shipid", task.ShipId, DbType.Decimal);
            parameters.Add("@i_orderitem", task.OrderItem);
            parameters.Add("@i_orderlot", task.OrderLot);
            parameters.Add("@i_priority", task.Priority);
            parameters.Add("@i_prevpriority", task.PrevPriority);
            parameters.Add("@i_lastuser", task.LastUser);
            parameters.Add("@i_pickuom", task.PickUom);
            parameters.Add("@i_pickqty", task.PickQty, DbType.Decimal);
            parameters.Add("@i_picktotype", task.PickToType);
            parameters.Add("@i_wave", task.Wave);
            parameters.Add("@i_pickingzone", task.PickingZone);
            parameters.Add("@i_cartontype", task.CartonType);
            parameters.Add("@i_weight", task.Weight);
            parameters.Add("@i_cube", task.Cube);
            parameters.Add("@i_staffhrs", task.StaffHrs);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);

            #endregion

            connection.Execute("WVR_Task_I_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        public IEnumerable<Task> GetByTypeAndOrder(IDbConnection connection, string tasktype, long wave, long orderId, int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@i_tasktype", tasktype);
            parameters.Add("@c_tasks", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<Task>("WVR_TASKBYTYPEANDORDER_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public bool Delete(IDbConnection connection, long taskId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_taskid", taskId);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_TASKBYTASKID_D_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        public bool DeleteByOrderAndType(IDbConnection connection, long orderId, int shipId, string taskType)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@i_tasktype", taskType);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_TASKBYORDERIDTSKTYPE_D_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        public bool UpdateTaskQuatity(IDbConnection connection, long taskId, int quantity)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_taskid", taskId);
            parameters.Add("@i_qty", quantity);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_TASKQTYBYTASKID_U_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        public long GetNextTaskId(IDbConnection connection)
        {
            #region Parameters

            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@o_taskid ", dbType: DbType.Int64, direction: ParameterDirection.Output);

            #endregion

            connection.Execute("WVR_NEXTTASKID_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<long>("o_taskid ");
        }

        public bool UpdateTask(IDbConnection connection, Task task)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_taskid", task.TaskId);
            parameters.Add("@i_qty", task.Quantity);
            parameters.Add("@i_pickqty", task.PickQty);
            parameters.Add("@i_weight", task.Weight);
            parameters.Add("@i_cube", task.Cube);
            parameters.Add("@i_staffhrs", task.StaffHrs);
            parameters.Add("@i_tasktype",task.TaskType);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_Task_U_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        public bool UpdateTaskQuantityAndUom(IDbConnection connection, Task task)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_taskid", task.TaskId);
            parameters.Add("@i_qty", task.Quantity);
            parameters.Add("@i_uom", task.Uom);
            parameters.Add("@i_pickqty", task.PickQty);
            parameters.Add("@i_pickuom", task.PickUom);
            parameters.Add("@i_weight", task.Weight);
            parameters.Add("@i_cube", task.Cube);
            parameters.Add("@i_staffhrs", task.StaffHrs);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_TASKQUANTITYANDUOM_U_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        public int GetActiveTasksCount(IDbConnection connection, long wave)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@o_rowscount", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_ACTIVETASKSCOUNTBYWAVE_R_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowscount");
        }

        public int GetPassedTasksCountById(IDbConnection connection, long taskId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_taskid", taskId);
            parameters.Add("@o_rowscount", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_PASSEDTASKSCOUNTBYID_R_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowscount");
        }

        public int GetActiveTasksCountByOrderId(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@o_rowscount", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_ACTTSKCOUNTBYORDERID_R_P", parameters,
                commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowscount");
        }

        public int GetBatchTypeTasksCount(IDbConnection connection, long wave)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@o_rowscount", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_BATTYPETSKSCNTBYWAVE_R_P", parameters,
                commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowscount");
        }

        public bool DeleteByIdAndFacility(IDbConnection connection, long taskId, string facility)
        {
            var parameters = new OracleDynamicParameters();
            //Input parameter
            parameters.Add("@i_taskid", taskId);
            parameters.Add("@i_facility", facility);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("RGP_TASKBYIDANDFACILITY_D_P", parameters, commandType: CommandType.StoredProcedure);

            return (int)parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public IEnumerable<Zone> GetTaskGroupingRulesByZone(IDbConnection connection, string zoneId, string facility)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_facility", facility);
            parameters.Add("@i_zoneid", zoneId);
            parameters.Add("@c_zone", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<Zone>("WVR_TASKGROUPINGBYZONE_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }
    }
}