﻿// <copyright file="SystemDefaultDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-26</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using System.Data;
using Dapper;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Data
{
    public class SystemDefaultDbContext : ISystemDefaultDbContext
    {
        public string GetDefaultValueByDefaultId(IDbConnection connection, string defaultId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_defaultid", defaultId, DbType.String, ParameterDirection.Input);
            parameters.Add("@o_defaultvalue", dbType: DbType.String, direction: ParameterDirection.Output, size: 255);
            connection.Execute("WVR_DEFAULTVALUEBYID_R_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<string>("o_defaultvalue");
        }
    }
}