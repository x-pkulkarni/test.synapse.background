﻿// <copyright file="CartonGroupDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-29</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using System.Data;
using Dapper;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Data
{
    using System.Collections;
    using System.Collections.Generic;
    using Model;
    using Oracle.DataAccess.Client;

    public class CartonGroupDbContext : ICartonGroupDbContext
    {
        public string GetCartonGroupByCode(IDbConnection connection, string code)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_code", code);
            parameters.Add("@o_cartongroup", dbType: DbType.String, direction: ParameterDirection.Output, size:5);
            connection.Execute("WVR_CARTONGROUPBYCODE_R_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<string>("o_cartongroup");
        }

        public string GetCartonTypeByGroup(IDbConnection connection, string group)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_group", group);
            parameters.Add("@o_cartontype", dbType: DbType.String, direction: ParameterDirection.Output, size: 5);
            connection.Execute("WVR_CARTONTYPEBYGROUP_R_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<string>("o_cartontype");
        }

        public IEnumerable<Container> GetCartonTypesByGroup(IDbConnection connection, string group)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_group", group);
            parameters.Add("@c_cartontypedetails", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<Container>("WVR_GETCARTONTYPESBYGRP_R_P", parameters,
                commandType: CommandType.StoredProcedure);
           
        }
    }
}