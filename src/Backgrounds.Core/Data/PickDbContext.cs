﻿// <copyright file="PickDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-11</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-11</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Dapper;
using Oracle.DataAccess.Client;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data
{
    public class PickDbContext : IPickDbContext
    {
        public IEnumerable<FindAPickResponse> FindAPick(IDbConnection connection, FindAPickRequest findAPickRequest)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_fromfacility", findAPickRequest.FromFacility);
            parameters.Add("@i_custid", findAPickRequest.CustomerId);
            parameters.Add("@i_item", findAPickRequest.Item);
            parameters.Add("@i_lotnumber", findAPickRequest.LotNumber);
            parameters.Add("@i_invstatus", findAPickRequest.InvStatus);
            parameters.Add("@i_inventoryclass", findAPickRequest.InventoryClass);
            parameters.Add("@i_qty", findAPickRequest.Quantity);
            parameters.Add("@i_qtytype", findAPickRequest.QuantityType);
            parameters.Add("@i_variancepct", findAPickRequest.Variancepct);
            parameters.Add("@i_pickuom", findAPickRequest.PickUom);
            parameters.Add("@i_repl_req_yn", findAPickRequest.ForReplenishmentRequest);
            parameters.Add("@i_storage_or_stage", findAPickRequest.StorageOrStage);
            parameters.Add("@i_expdaterequired", findAPickRequest.ExpirationDtRequired);
            parameters.Add("@i_enter_min_days_to_expire_yn", findAPickRequest.IsEnterMindDaysToExpire ? "Y" : "N");
            parameters.Add("@i_min_days_to_expiration", findAPickRequest.MindDaysToExpiration);
            parameters.Add("@i_preferred_pickzone", findAPickRequest.WavePickZone);
            parameters.Add("@i_vendor", findAPickRequest.Vendor);
            parameters.Add("@i_trace", findAPickRequest.Trace);
            //parameters.Add("@i_orderid", findAPickRequest.OrderId);
            //parameters.Add("@i_shipid", findAPickRequest.ShipId);
            parameters.Add("@i_pfloc", findAPickRequest.PfLoc);
            parameters.Add("@i_slotwave_req_yn", findAPickRequest.SlotWaveRequired);
            parameters.Add("@i_dynamic_alloc_mode", findAPickRequest.DynamicAllocMode);
            parameters.Add("@i_allocneed", findAPickRequest.AllocNeed);
            parameters.Add("@i_picktotype_rule", findAPickRequest.PickToTypeRule);
            //Out parameters
            parameters.Add("@c_pick_details", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            return connection.Query<FindAPickResponse>("WVR_FINDAPICK_R_P", parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
