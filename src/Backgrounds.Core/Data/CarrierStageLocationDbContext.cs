﻿// <copyright file="OrderDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-24</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-24</lastchangeddate>

using System.Data;
using Dapper;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Data
{
    public class CarrierStageLocationDbContext : ICarrierStageLocationDbContext
    {
        public string GetStageLocationByCarrier(IDbConnection connection, string carrier, string facility, string shipType)
        {

            var parameters = new DynamicParameters();
            parameters.Add("@i_carrier", carrier);
            parameters.Add("@i_facility", facility);
            parameters.Add("@i_shiptype", shipType);
            parameters.Add("@o_stageloc", dbType: DbType.String, direction: ParameterDirection.Output,size:10);
            connection.Execute("WVR_STAGELOCBYCARRIER_R_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<string>("o_stageloc");

        }
    }
}
