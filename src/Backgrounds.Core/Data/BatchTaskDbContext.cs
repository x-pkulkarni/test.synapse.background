﻿// <copyright file="BatchTaskDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-11</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-23</lastchangeddate>


using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data
{
    public class BatchTaskDbContext : IBatchTaskDbContext
    {
        public bool Insert(IDbConnection connection, BatchTask batchTask)
        {
            #region Parameters

            var parameters = new OracleDynamicParameters();
            //Input parameter
            parameters.Add("@i_taskid", batchTask.TaskId);
            parameters.Add("@i_tasktype", batchTask.TaskType);
            parameters.Add("@i_facility", batchTask.Facility);
            parameters.Add("@i_fromlocation", batchTask.FromLocation);
            parameters.Add("@i_tosection", batchTask.ToSection);
            parameters.Add("@i_tolocation", batchTask.ToLocation);
            parameters.Add("@i_toprofile", batchTask.ToProfile);
            parameters.Add("@i_touserid", batchTask.ToUserId);
            parameters.Add("@i_customerid", batchTask.CustomerId);
            parameters.Add("@i_item", batchTask.Item);
            parameters.Add("@i_lpid", batchTask.LpId);
            parameters.Add("@i_uom", batchTask.UOM);
            parameters.Add("@i_quantity", batchTask.Quantity);
            parameters.Add("@i_loadno", batchTask.LoadNo);
            parameters.Add("@i_shipno", batchTask.ShipNo);
            parameters.Add("@i_stopno", batchTask.StopNo);
            parameters.Add("@i_orderid", batchTask.TaskType?.Equals("BS") ?? false ? 0 : batchTask.OrderId);
            parameters.Add("@i_shipid", batchTask.TaskType?.Equals("BS") ?? false ? 0 : batchTask.ShipId);
            parameters.Add("@i_orderitem", batchTask.OrderItem);
            parameters.Add("@i_orderlot", batchTask.OrderLot);
            parameters.Add("@i_priority", batchTask.Priority);
            parameters.Add("@i_prevpriority", batchTask.PrevPriority);
            parameters.Add("@i_currentuserid", batchTask.CurrentUserId);
            parameters.Add("@i_lastuser", batchTask.LastUser);
            parameters.Add("@i_pickuom", batchTask.PickUom);
            parameters.Add("@i_pickqty", batchTask.PickQty);
            parameters.Add("@i_picktotype", batchTask.BatchPickToType);
            parameters.Add("@i_wave", batchTask.Wave);
            parameters.Add("@i_cartontype", batchTask.CartonType);
            parameters.Add("@i_weight", batchTask.BatchTaskWeight);
            parameters.Add("@i_cube", batchTask.BatchTaskCube);
            parameters.Add("@i_staffhrs", batchTask.BatchTaskStaffHrs);
            parameters.Add("@i_cartonseq", batchTask.CartonSeq);
            parameters.Add("@i_shippinglpid", batchTask.ShippingLpId);
            parameters.Add("@i_shippingtype", batchTask.ShippingType);
            parameters.Add("@i_invstatus", batchTask.InvStatus);
            parameters.Add("@i_inventoryclass", batchTask.InventoryClass);
            parameters.Add("@i_qtytype", batchTask.QtyType);
            parameters.Add("@i_lotnumber", batchTask.LotNumber);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);

            #endregion

            connection.Execute("WVR_BATCHTASK_I_P", parameters, commandType: CommandType.StoredProcedure);
            return (int) parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public IEnumerable<BatchTask> GetBatchTasksByWave(IDbConnection connection, long waveId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_wave", waveId);
            parameters.Add("@c_bactchtasks", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<BatchTask>("WVR_BATCHTASKBYWAVE_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public bool DeleteByTaskIdAndLpId(IDbConnection connection, SubTask task)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_taskid", task.TaskId);
            parameters.Add("@i_custid", task.CustId);
            parameters.Add("@i_orderitem", task.OrderItem);
            parameters.Add("@i_orderlot", task.OrderLot);
            parameters.Add("@i_lpid", task.LpId);
            parameters.Add("@i_item", task.Item);
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_BATCHTSKBYTSKIDLPID_D_P", parameters, commandType: CommandType.StoredProcedure);

            return (int)parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public bool UpdateTaskId(IDbConnection connection, long wave, long taskId, string lastUser, BatchTask batchTask)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@i_taskid", taskId);
            parameters.Add("@i_lastuser", lastUser);
            parameters.Add("@i_custid", batchTask.CustomerId);
            parameters.Add("@i_item", batchTask.Item);
            parameters.Add("@i_orderitem", batchTask.OrderItem);
            parameters.Add("@i_orderlot", batchTask.OrderLot);
            parameters.Add("@i_lpid", batchTask.LpId);
            parameters.Add("@i_fromloc", batchTask.FromLocation);
            parameters.Add("@i_invstatus", batchTask.InvStatus);
            parameters.Add("@i_inventoryclass", batchTask.InventoryClass);
            parameters.Add("@i_uom", batchTask.UOM);
            parameters.Add("@i_picktotype", batchTask.PickToType);
            parameters.Add("@i_facility", batchTask.Facility);
            parameters.Add("@i_cartontype", batchTask.CartonType);
            parameters.Add("@i_qtytype", batchTask.QtyType);
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_BATCHTASKID_U_P", parameters, commandType: CommandType.StoredProcedure);

            return (int) parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public int GetPendingBatchPicksLine(IDbConnection connection, PendingPickQtyRequest pendingBatchPickQtyRequest)
        {
            #region Parameters

            var parameters = new OracleDynamicParameters();
            //Input parameter
            parameters.Add("@i_orderid", pendingBatchPickQtyRequest.OrderId);
            parameters.Add("@i_orderitem", pendingBatchPickQtyRequest.OrderItem);
            parameters.Add("@i_shipid", pendingBatchPickQtyRequest.ShipId);
            parameters.Add("@i_orderlot", pendingBatchPickQtyRequest.OrderLot);
            parameters.Add("@o_quantity", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);

            #endregion

            connection.Execute("WVR_PENDINGBATCHLINE_R_P", parameters, commandType: CommandType.StoredProcedure);
            return (int) parameters.Get<OracleDecimal>("o_quantity");
        }

        public int GetPendingBatchVendorItem(IDbConnection connection, PendingPickQtyRequest pendingBatchPickQtyRequest)
        {
            #region Parameters

            var parameters = new OracleDynamicParameters();
            //Input parameter
            parameters.Add("@i_orderid", pendingBatchPickQtyRequest.OrderId);
            parameters.Add("@i_orderitem", pendingBatchPickQtyRequest.OrderItem);
            parameters.Add("@i_shipid", pendingBatchPickQtyRequest.ShipId);
            parameters.Add("@i_orderlot", pendingBatchPickQtyRequest.OrderLot);
            parameters.Add("@i_item", pendingBatchPickQtyRequest.Item);
            parameters.Add("@i_invstatus", pendingBatchPickQtyRequest.InvStatus);
            parameters.Add("@o_quantity", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);

            #endregion

            connection.Execute("WVR_PENDBTCHVNDRITM_R_P", parameters, commandType: CommandType.StoredProcedure);
            return (int) parameters.Get<OracleDecimal>("o_quantity");
        }

        public int GetPendingBatchItem(IDbConnection connection, PendingPickQtyRequest pendingBatchPickQtyRequest)
        {
            #region Parameters

            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@i_orderid", pendingBatchPickQtyRequest.OrderId);
            parameters.Add("@i_orderitem", pendingBatchPickQtyRequest.OrderItem);
            parameters.Add("@i_shipid", pendingBatchPickQtyRequest.ShipId);
            parameters.Add("@i_orderlot", pendingBatchPickQtyRequest.OrderLot);
            parameters.Add("@i_item", pendingBatchPickQtyRequest.Item);
            parameters.Add("@i_invstatus", pendingBatchPickQtyRequest.InvStatus);
            parameters.Add("@i_inventoryclass", pendingBatchPickQtyRequest.InventoryClass);
            parameters.Add("@o_quantity", dbType: DbType.Int32, direction: ParameterDirection.Output);

            #endregion

            connection.Execute("WVR_PENDINGBATCHITEM_R_P", parameters, commandType: CommandType.StoredProcedure);
            return  parameters.Get<int>("o_quantity");
        }

        public bool ValidateTaskId(IDbConnection connection, long taskId)
        {
            #region Parameters

            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@i_taskid", taskId);
            parameters.Add("@o_batchtaskid", dbType: DbType.Int64, direction: ParameterDirection.Output);

            #endregion

            connection.Execute("RGP_VALIDATEBATCHTASKID_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<long>("o_batchtaskid").Equals(taskId);
        }
      

        public bool ResetBatchTasksByTaskId(IDbConnection connection, long taskid, string facility)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_taskid", taskid);
            parameters.Add("@i_facility", facility);
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("RGP_RESETBATCHTASKS_U_P", parameters, commandType: CommandType.StoredProcedure);

            return (int)parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public bool DeleteBatchTasksByTaskId(IDbConnection connection, long taskid, string facility)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_taskid", taskid);
            parameters.Add("@i_facility", facility);
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("RGP_BATCHTASKSBYTSKID_D_P", parameters, commandType: CommandType.StoredProcedure);

            return (int)parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public IEnumerable<BatchTask> GetBatchTasksByTaskId(IDbConnection connection, long taskId, string facility)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_taskid", taskId);
            parameters.Add("@i_facility", facility);
            parameters.Add("@c_batchtasks", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<BatchTask>("RGP_BATCHTSKBYTSKID_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }
        public bool IsBatchPickSinglesOrder(IDbConnection connection, long wave, string item)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@in_wave", wave);
            parameters.Add("@in_item", item);
            parameters.Add("@out_count", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);
            connection.Execute("WVR_ISBATCHPICKSINGLEORDER_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("out_count") > 0;
        }

    }
}
