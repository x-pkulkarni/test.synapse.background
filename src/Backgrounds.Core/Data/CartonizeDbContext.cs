﻿// <copyright file="CartonizeDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Kathiresan, Murugan</author>  
// <createddate>2019-01-30</createddate>
namespace Synapse.Backgrounds.Core.Data
{
    using System.Collections.Generic;
    using System.Data;
    using Dapper;
    using Interface;
    using Oracle.DataAccess.Client;
    using Model;

    public class CartonizeDbContext : ICartonizeDbContext
    {
        public IEnumerable<Container> GetCartonDetails(IDbConnection connection, string custid, long orderid, string cartonGroup)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custid);
            parameters.Add("@i_orderid", orderid);
            parameters.Add("@i_cartongroup", cartonGroup);
            parameters.Add("@c_cartondetails", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<Container>("WVR_GETCARTONTYPES_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<Item> GetItemDetails(IDbConnection connection, string custid, long orderid)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custid);
            parameters.Add("@i_orderid", orderid);
            parameters.Add("@c_itemdetails", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<Item>("WVR_ITEMDETAILS_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<Item> GetSubTaskItemDetails(IDbConnection connection, string taskType, long orderid)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_tasktype", taskType);
            parameters.Add("@i_orderid", orderid);
            parameters.Add("@c_itemdetails", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<Item>("WVR_SUBTASKITEMDETAILS_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }
    }

}
