﻿// <copyright file="LoadDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-02-26</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-26</lastchangeddate>

using System.Data;
using Dapper;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Data
{
    public class LoadDbContext : ILoadDbContext
    {
        public bool UpdateStatusByLoadNo(IDbConnection connection, string userId, int? loadNo, string status)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_loadno", loadNo);
            parameters.Add("@i_userid", userId);
            parameters.Add("@i_status", status);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_LOADSTATUSBYLOADNO_U_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowsaffected") > 0;
        }


        public bool UpdateStopStatusByLoadNo(IDbConnection connection, string userId, int? loadNo, string status,
            int? stopNo)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_loadno", loadNo);
            parameters.Add("@i_userid", userId);
            parameters.Add("@i_stopno", stopNo);
            parameters.Add("@i_status", status);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_LOADSTOPSTATUSBYLOADNO_U_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        public bool UpdateLoadStatusToPlanned(IDbConnection connection, string userId, int? loadNo)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_loadno", loadNo);
            parameters.Add("@i_userid", userId);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_MARKLOADASPLANNED_U_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowsaffected") > 0;
        }
    }
}