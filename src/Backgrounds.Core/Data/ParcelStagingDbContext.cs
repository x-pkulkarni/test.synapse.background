﻿// <copyright file="ParcelStagingDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Kathiresan, Murugan</author> 
// <createddate>2019-04-25</createddate>
namespace Synapse.Backgrounds.Core.Data
{
    using System.Data;
    using Dapper;
    using Interface;
    using Model;
    using Oracle.DataAccess.Client;
    using System.Collections.Generic;
    public class ParcelStagingDbContext: IParcelStagingDbContext
    {
        public bool Insert(IDbConnection connection, ParcelStaging task)
        {
            #region Parameters

            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@i_lpid", task.LpId);
            parameters.Add("@i_orderid", task.OrderId);
            parameters.Add("@i_shipid", task.ShipId);
            parameters.Add("@i_wave", task.Wave);
            parameters.Add("@i_taskid", task.TaskId);
            parameters.Add("@i_status", task.Status);
            parameters.Add("@i_facility", task.Facility);
            parameters.Add("@i_carrier", task.Carrier);
            parameters.Add("@i_custid", task.CustId);
            parameters.Add("@i_deliveryservice", task.DeliveryService);
            parameters.Add("@i_item", task.Item);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);

            #endregion

            connection.Execute("WVR_WRITEPARCEL_I_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        public bool GetRecordCountByWave(IDbConnection connection, long waveId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_wave", waveId);
            parameters.Add("@o_rowscount", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_PARCELCOUNTBYWAVE_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowscount") > 0;
        }


        public IEnumerable<ParcelStaging> GetParcelStagingByWave(IDbConnection connection, long waveId, long orderId,
            int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_wave", waveId);
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@c_parcelstaging", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<ParcelStaging>("WVR_PARCELSTAGINGBYWAVE_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public bool UpdatePsRecord(IDbConnection connection, string userId, string rowId)
        {
            #region Parameters
            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@i_user", userId);
            parameters.Add("@i_rowid", rowId);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            #endregion
            connection.Execute("WVU_PARCELSTAGING_U_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowsaffected") > 0;
        }


        public bool UpdateCShipRecords(IDbConnection connection, string lpId, string userId)
        {
            #region Parameters
            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@i_lpid", lpId);
            parameters.Add("@i_user", userId);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            #endregion
            connection.Execute("WVU_CONNECTSHIP_U_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        public bool DeleteCaseAndUccLabels(IDbConnection connection, long orderId,int shipId)
        {
            #region Parameters
            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipId", shipId);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            #endregion
            connection.Execute("WVU_CASEANDUCCLABELS_D_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowsaffected") > 0;
        }
    }
}
