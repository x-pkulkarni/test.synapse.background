﻿// <copyright file="ShippingPlateDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-11</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-26</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Dapper;
using Oracle.DataAccess.Client;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data
{
    public class ShippingPlateDbContext : IShippingPlateDbContext
    {
        public bool Insert(IDbConnection connection, ShippingPlate shippingPlate)
        {
            #region Parameters

            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@i_lpid", shippingPlate.LpId);
            parameters.Add("@i_item", shippingPlate.Item);
            parameters.Add("@i_custid", shippingPlate.CustomerId);
            parameters.Add("@i_Facility", shippingPlate.Facility);
            parameters.Add("@i_location", shippingPlate.Location);
            parameters.Add("@i_status", shippingPlate.Status);
            parameters.Add("@i_holdreason", shippingPlate.HoldReason);
            parameters.Add("@i_unitofmeasure", shippingPlate.UnitOfMeasure);
            parameters.Add("@i_quantity", shippingPlate.Quantity);
            parameters.Add("@i_type", shippingPlate.Type);
            parameters.Add("@i_fromlpid", shippingPlate.FromLpId);
            parameters.Add("@i_lotnumber", shippingPlate.LotNumber);
            parameters.Add("@i_parentlpid", shippingPlate.ParentLpId);
            parameters.Add("@i_useritem1", shippingPlate.UserItem1);
            parameters.Add("@i_lastuser", shippingPlate.LastUser);
            parameters.Add("@i_invstatus", shippingPlate.InvStatus);
            parameters.Add("@i_qtyentered", shippingPlate.QtyEntered);
            parameters.Add("@i_orderitem", shippingPlate.OrderItem);
            parameters.Add("@i_uomentered", shippingPlate.UomEntered);
            parameters.Add("@i_inventoryclass", shippingPlate.InventoryClass);
            parameters.Add("@i_orderid", shippingPlate.OrderId);
            parameters.Add("@i_shipid", shippingPlate.ShipId);
            parameters.Add("@i_weight", shippingPlate.Weight);
            parameters.Add("@i_ucc128", shippingPlate.Ucc128);
            parameters.Add("@i_labelformat", shippingPlate.LabelFormat);
            parameters.Add("@i_taskid", shippingPlate.TaskId);
            parameters.Add("@i_pickuom", shippingPlate.PickUom);
            parameters.Add("@i_pickqty", shippingPlate.PickQty);
            parameters.Add("@i_cartonseq", shippingPlate.CartonSeq);
            parameters.Add("@i_hazmatind", shippingPlate.HazmatInd);
            parameters.Add("@i_iataprimarychemcode", shippingPlate.IataPrimaryChemCode);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);

            #endregion

            connection.Execute("WVR_SHIPPINGPLATE_I_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        public int GetPendingPlatePicksLine(IDbConnection connection, PendingPickQtyRequest pendingPlatePickQtyRequest)
        {
            #region Parameters

            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@i_orderid", pendingPlatePickQtyRequest.OrderId);
            parameters.Add("@i_orderitem", pendingPlatePickQtyRequest.OrderItem);
            parameters.Add("@i_shipid", pendingPlatePickQtyRequest.ShipId);
            parameters.Add("@i_orderlot", pendingPlatePickQtyRequest.OrderLot);
            //Output parameter
            parameters.Add("@o_quantity", dbType: DbType.Int32, direction: ParameterDirection.Output);

            #endregion

            connection.Execute("WVR_PENDINGPICKSLINE_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_quantity");
        }

        public int GetPendingPicksVendorItem(IDbConnection connection, PendingPickQtyRequest pendingPlatePickQtyRequest)
        {
            #region Parameters

            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@i_orderid", pendingPlatePickQtyRequest.OrderId);
            parameters.Add("@i_orderitem", pendingPlatePickQtyRequest.OrderItem);
            parameters.Add("@i_shipid", pendingPlatePickQtyRequest.ShipId);
            parameters.Add("@i_orderlot", pendingPlatePickQtyRequest.OrderLot);
            parameters.Add("@i_item", pendingPlatePickQtyRequest.Item);
            parameters.Add("@i_invstatus", pendingPlatePickQtyRequest.InvStatus);
            //Output parameter
            parameters.Add("@o_quantity", dbType: DbType.Int32, direction: ParameterDirection.Output);

            #endregion

            connection.Execute("WVR_PENDINGPICKSVNDRITM_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_quantity");
        }

        public int GetPendingPicksItem(IDbConnection connection, PendingPickQtyRequest pendingPlatePickQtyRequest)
        {
            #region Parameters

            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@i_orderid", pendingPlatePickQtyRequest.OrderId);
            parameters.Add("@i_orderitem", pendingPlatePickQtyRequest.OrderItem);
            parameters.Add("@i_shipid", pendingPlatePickQtyRequest.ShipId);
            parameters.Add("@i_orderlot", pendingPlatePickQtyRequest.OrderLot);
            parameters.Add("@i_item", pendingPlatePickQtyRequest.Item);
            parameters.Add("@i_invstatus", pendingPlatePickQtyRequest.InvStatus);
            parameters.Add("@i_inventoryclass", pendingPlatePickQtyRequest.InventoryClass);
            //Output parameter
            parameters.Add("@o_quantity", dbType: DbType.Int32, direction: ParameterDirection.Output);

            #endregion

            connection.Execute("WVR_PENDINGPICKSITEM_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_quantity");
        }

        public string GetNextShippingLpId(IDbConnection connection)
        {
            #region Parameters

            var parameters = new DynamicParameters();
            //Output parameter
            parameters.Add("@o_shippinglpid", dbType: DbType.String, direction: ParameterDirection.Output, size: 15);

            #endregion

            connection.Execute("WVR_NEXTSHPNGPLTLPID_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<string>("o_shippinglpid");
        }

        public bool DeleteByLpId(IDbConnection connection, string lpId)
        {
            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@i_lpid", lpId);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);

            connection.Execute("WVU_SHIPPINGPLTBYLPID_D_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        /// <summary>
        ///     Clean up empty master/carton shipping plates for given order where the master/carton has no full or partial
        ///     children shipping plates associated.
        /// </summary>
        /// <returns></returns>
        public bool DeleteMasterAndCartonByOrderId(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new DynamicParameters();
            //Input parameter
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);

            connection.Execute("WVU_MSTRCRTNSHPGPLTBYORDR_D_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        public IEnumerable<ShippingPlate> GetMasterAndCartonByOrderId(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@c_shippingplates", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<ShippingPlate>("WVU_MSTRCRTNSHPGPLTBYORDR_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public bool UpdateTaskIdInShippingPlate(IDbConnection connection, string shippingPlateId, long taskId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_taskid", taskId);
            parameters.Add("@i_shippinglpid", shippingPlateId);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_UPDATETASKIDINSHIPPINGPLATE_U_P", parameters, commandType: CommandType.StoredProcedure);

            return (int)parameters.Get<int>("o_rowsaffected") > 0;
        }

    }
}