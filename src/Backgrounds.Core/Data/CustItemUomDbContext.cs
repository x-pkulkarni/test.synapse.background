﻿// <copyright file="CustItemUomDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-17</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Dapper;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data
{
    public class CustItemUomDbContext : ICustItemUomDbContext
    {
        

        public double GetCubeFromCustItemUom(IDbConnection connection, CustItemUom custItemUomModel)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custItemUomModel.CustId);
            parameters.Add("@i_item", custItemUomModel.Item);
            parameters.Add("@i_uom", custItemUomModel.ToUom);
            parameters.Add("@o_cube", dbType: OracleDbType.Decimal, direction: ParameterDirection.Output);
            connection.Execute("WVR_CUSTITEMUOMCUBE_R_P", parameters, commandType: CommandType.StoredProcedure);
            return (double)parameters.Get<OracleDecimal>("o_cube");
        }

        public double GetWeightFromCustItemUom(IDbConnection connection, CustItemUom custItemUomModel)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custItemUomModel.CustId);
            parameters.Add("@i_item", custItemUomModel.Item);
            parameters.Add("@i_uom", custItemUomModel.ToUom);
            parameters.Add("@o_weight", dbType: OracleDbType.Decimal, direction: ParameterDirection.Output, size: 10);
            connection.Execute("WVR_CUSTITEMUOMWEIGHT_R_P", parameters, commandType: CommandType.StoredProcedure);
            return (double)parameters.Get<OracleDecimal>("o_weight");
        }

        public IEnumerable<CustItemUom> GetCustItemUomByUom(IDbConnection connection, CustItemUom custItemUomModel)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custItemUomModel.CustId);
            parameters.Add("@i_item", custItemUomModel.Item);
            parameters.Add("@i_fromuom", custItemUomModel.FromUom);
            parameters.Add("@c_custitemuom", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<CustItemUom>("WVR_CUSTITEMUOMBYUOM_R_P", parameters, commandType: CommandType.StoredProcedure);
        }

        public int GetCustItemSequenceByFromUom(IDbConnection connection, CustItemUom custItemUomModel)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custItemUomModel.CustId);
            parameters.Add("@i_item", custItemUomModel.Item);
            parameters.Add("@i_fromuom", custItemUomModel.FromUom);
            parameters.Add("@o_sequence", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_CUSTITEMUOMBYFROMUOM_R_P", parameters, commandType: CommandType.StoredProcedure);
            return (int)parameters.Get<OracleDecimal>("o_sequence");
        }

        public int GetCustItemSequenceByToUom(IDbConnection connection, CustItemUom custItemUomModel)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custItemUomModel.CustId);
            parameters.Add("@i_item", custItemUomModel.Item);
            parameters.Add("@i_touom", custItemUomModel.ToUom);
            parameters.Add("@o_sequence", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_CUSTITEMUOMBYTOUOM_R_P", parameters, commandType: CommandType.StoredProcedure);
            return (int)parameters.Get<OracleDecimal>("o_sequence");
        }

        public IEnumerable<CustItemUom> GetCustItemQtyByEquivUp(IDbConnection connection, CustItemQtyBySeqRequest custItemQtyBySeqRequest)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custItemQtyBySeqRequest.CustId);
            parameters.Add("@i_item", custItemQtyBySeqRequest.Item);
            parameters.Add("@i_fromseq", custItemQtyBySeqRequest.FromSequence);
            parameters.Add("@i_toseq", custItemQtyBySeqRequest.ToSequence);
            parameters.Add("@c_qty", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<CustItemUom>("WVR_CUSTITEMUOMBYEQUIPUP_R_P", parameters, commandType: CommandType.StoredProcedure);
         
        }

        public IEnumerable<CustItemUom> GetCustItemQtyByEquivDown(IDbConnection connection, CustItemQtyBySeqRequest custItemQtyBySeqRequest)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custItemQtyBySeqRequest.CustId);
            parameters.Add("@i_item", custItemQtyBySeqRequest.Item);
            parameters.Add("@i_fromseq", custItemQtyBySeqRequest.FromSequence);
            parameters.Add("@i_toseq", custItemQtyBySeqRequest.ToSequence);
            parameters.Add("@c_qty", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
           return connection.Query<CustItemUom>("WVR_CUSTITEMUOMBYEQUIPDOWN_R_P", parameters, commandType: CommandType.StoredProcedure);
           
        }

        public string GetNextHighUom(IDbConnection connection, string custId, string item)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", custId);
            parameters.Add("@i_item", item);
            parameters.Add("@o_uom", dbType: DbType.String, direction: ParameterDirection.Output,size:10);
            connection.Execute("WVR_GETNEXTHIGHUOM_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<string>("o_uom");
        }

        public IEnumerable<CustItemUom> GetCustItemAllUomByUom(IDbConnection connection, string custId, string item)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custId);
            parameters.Add("@i_item", item);
            parameters.Add("@c_custitemuom", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<CustItemUom>("WVR_GETCUSTITEMALLUOMBYUOM_R_P", parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
