﻿// <copyright file="CommitmentsDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-15</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-15</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Dapper;
using Oracle.DataAccess.Client;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data
{
    public class CommitmentDbContext : ICommitmentDbContext
    {
        public IEnumerable<Commitment> GetCommitmentsByItem(IDbConnection connection, string item)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_item", item);
            parameters.Add("@c_commitments", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<Commitment>("WVR_COMMITMENTS_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<Commitment> GetVendorTrackedCommitments(IDbConnection connection, Commitment commitment)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", commitment.OrderId);
            parameters.Add("@i_shipid", commitment.ShipId);
            parameters.Add("@i_orderitem", commitment.OrderItem);
            parameters.Add("@i_orderlot", commitment.OrderLot);
            parameters.Add("@c_commitment", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<Commitment>("WVR_COMMITMENTSVNDRTRKD_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<Commitment> GetNonVendorTrackedCommitments(IDbConnection connection, Commitment commitment)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", commitment.OrderId);
            parameters.Add("@i_shipid", commitment.ShipId);
            parameters.Add("@i_orderitem", commitment.OrderItem);
            parameters.Add("@i_orderlot", commitment.OrderLot);
            parameters.Add("@c_commitment", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<Commitment>("WVR_COMMITMENTNOTVNDRTRKD_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public bool ResetPlateLine(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_COMTSPLTLINERESET_U_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        /// <summary>
        /// Update commitment to reduce the quantity & returns new quantity
        /// </summary>
        /// <returns>Updated Commitment quantity</returns>
        public int? UpdateAndReturnNewQuantity(IDbConnection connection, SubTask task, string inventoryClass,
            string invStatus, string lotNumber)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_orderid", task.OrderId);
            parameters.Add("@i_shipid", task.ShipId);
            parameters.Add("@i_qty", task.Quantity);
            parameters.Add("@i_orderitem", task.OrderItem);
            parameters.Add("@i_orderlot", task.OrderLot);
            parameters.Add("@i_item", task.Item);
            parameters.Add("@i_lotnumber", lotNumber);
            parameters.Add("@i_inventoryclass", inventoryClass);
            parameters.Add("@i_invstatus", invStatus);
            parameters.Add("@o_quantity", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("RGP_COMMITMENTQTY_U_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int?>("o_quantity");
        }

        public bool DeleteBySubTask(IDbConnection connection, SubTask task, string inventoryClass, string invStatus,
            string lotNumber)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_orderid", task.OrderId);
            parameters.Add("@i_shipid", task.ShipId);
            parameters.Add("@i_qty", task.Quantity);
            parameters.Add("@i_orderitem", task.OrderItem);
            parameters.Add("@i_orderlot", task.OrderLot);
            parameters.Add("@i_item", task.Item);
            parameters.Add("@i_lotnumber", lotNumber);
            parameters.Add("@i_inventoryclass", inventoryClass);
            parameters.Add("@i_invstatus", invStatus);
            parameters.Add("@o_quantity", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("RGP_COMMITMENTBYORDER_D_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_quantity") > 0;
        }
    }
}
