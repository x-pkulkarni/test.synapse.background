﻿// <copyright file="OrderDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-12</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-26</lastchangeddate>

namespace Synapse.Backgrounds.Core.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using Dapper;
    using Interface;
    using Model;
    using Oracle.DataAccess.Client;
    using Oracle.DataAccess.Types;

    public class OrderDbContext : IOrderDbContext
    {
        public IEnumerable<OrderHeader> GetOrderHeaderByWave(IDbConnection connection, long waveId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_wave", waveId);
            parameters.Add("@c_orderHeader", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<OrderHeader>("WVR_ORDERHEADER_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<OrderHeader> GetCShipOrderHeaderByWave(IDbConnection connection, long waveId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_wave", waveId);
            parameters.Add("@c_orderHeader", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<OrderHeader>("WVR_CSHIPORDERHEADER_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public int GetOrderCountByWave(IDbConnection connection, long waveId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_wave", waveId);
            parameters.Add("@o_rowscount", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_ORDERCOUNTBYWAVE_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowscount");
        }

        public IEnumerable<OrderHeader> GetOrderHeaderByOrderId(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@c_orderHeader", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<OrderHeader>("WVR_ORDERHEADERBYORDERID_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<ParcelStaging> GetFelTaskDtls(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@c_felTasks", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<ParcelStaging>("WVR_FELTASKS_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public bool UpdateByOrderId(IDbConnection connection, OrderHeader orderHeader)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_orderstatus", orderHeader.OrderStatus);
            parameters.Add("@i_commitstatus", orderHeader.CommitStatus);
            parameters.Add("@i_priority", (object) orderHeader.Priority ?? DBNull.Value);
            parameters.Add("@i_wave", orderHeader.Wave.Equals(0) ? (object) DBNull.Value : orderHeader.Wave);
            parameters.Add("@i_userid", orderHeader.LastUser);
            parameters.Add("@i_orderid", orderHeader.OrderId);
            parameters.Add("@i_shipid", orderHeader.ShipId);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_ORDERHEADER_U_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        public IEnumerable<OrderDetail> GetOrderDetailsByItem(IDbConnection connection, long orderId, int shipId,
            string item)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@i_item", item);
            parameters.Add("@c_orderdtls", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<OrderDetail>("WVR_ORDERDETAILSBYITEM_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<OrderDetail> GetOrderDetailsById(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@c_orderdetails", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<OrderDetail>("WVR_ORDERDETAILS_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<ParcelStaging> GetLtlSubTasks(IDbConnection connection, long orderId, int shipId,
            string pickToType, string cartonType, int cartonSeq)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@i_picktotype", pickToType);
            parameters.Add("@i_cartontype", cartonType);
            parameters.Add("@i_cartonseq", cartonSeq);
            parameters.Add("@c_orderdetails", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<ParcelStaging>("WVR_SUBTSKLTLBYORDR_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public bool SendImportExportRequest(IDbConnection connection, string lpid, string edimap)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@in_lpid", lpid);
            parameters.Add("@in_edimap", edimap);
            parameters.Add("@v_errno", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_GETCUSTLOOKUPCOUNT_R_P", parameters, commandType: CommandType.StoredProcedure);
            return (int) parameters.Get<OracleDecimal>("v_errno") > 0;
        }

        public bool ResetOrderDetails(IDbConnection connection, OrderHeader orderHeader)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", orderHeader.CustomerId);
            parameters.Add("@i_priority", orderHeader.Priority);
            parameters.Add("@i_fromfacility", orderHeader.FromFacility);
            parameters.Add("@i_orderid", orderHeader.OrderId);
            parameters.Add("@i_shipid", orderHeader.ShipId);
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_RESETORDERDTLS_U_P", parameters, commandType: CommandType.StoredProcedure);

            return (int) parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public IEnumerable<OrderDetail> GetIataPrimaryChemCode(IDbConnection connection, string item, long orderId,
            int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_item", item);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@c_orderdtls", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<OrderDetail>("WVR_ORDDTLIATAPRIMCHEMCD_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public string GetHazmatFlag(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@o_flag", dbType: DbType.String, direction: ParameterDirection.Output, size: 1);
            connection.Execute("WVR_ODRHAZMATFLAG_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<string>("o_flag");
        }

        public int GetOrderCountBeyondReleased(IDbConnection connection, long wave)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@o_rowscount", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_ORDRCNTBEYONDRLSED_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowscount");
        }

        public int GetOrderCountNotReleasedOrCancelled(IDbConnection connection, long wave)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@o_rowscount", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_ORDRCNTNOTRLSDORCNCLD_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowscount");
        }

        public int GetOrderCountWithAssocaitedPicks(IDbConnection connection, long wave)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@o_rowscount", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_ORDRCOUNTWITHPICKS_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowscount");
        }

        public int GetOrderCountWithAssocaitedLoad(IDbConnection connection, int? loadNo)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_loadno", loadNo);
            parameters.Add("@o_rowscount", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_ORDERCOUNTBYLOADNO_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowscount");
        }

        public bool AddOrderHistory(IDbConnection connection, OrderHistory history)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_action", history.Action);
            parameters.Add("@i_lpid", history.LpId);
            parameters.Add("@i_user", history.UserId);
            parameters.Add("@i_lotnumber", history.LotNumber);
            parameters.Add("@i_msg", history.Message);
            parameters.Add("@i_orderid", history.OrderId);
            parameters.Add("@i_shipid", history.ShipId);
            parameters.Add("@i_item", history.Item);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_ORDERHISTORY_I_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowsaffected") > 0;
        }

        public ConnectShipOrderHdr GetConnectShipOrderHdr(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@c_oh", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.QueryFirstOrDefault<ConnectShipOrderHdr>("WVR_GETORDERHEADERINFO_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<ConnectShipPackLable> GetConnectShipLbl(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@c_lbl", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<ConnectShipPackLable>("WVR_GETLABELSUBTASKS_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }
        public IEnumerable<ConnectShipPackLable> GetConnectShipLableForSingles(IDbConnection connection, long orderId, int shipId,long taskId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@i_taskid", taskId);
            parameters.Add("@c_lbl", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<ConnectShipPackLable>("WVR_GETLABELSUBTASKSFORSINGLES_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<ConnectShipPackLable> GetConnectShipPack(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@c_pak", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<ConnectShipPackLable>("WVR_GETPACKSUBTASKSDETAIL_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<ConnectShipPackLable> GetConnectShipPackForSingles(IDbConnection connection, 
                                                long orderId, int shipId, long taskId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@i_taskid", taskId);
            parameters.Add("@c_pak", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<ConnectShipPackLable>("WVR_GETPACKSUBTASKSDETAILFORSINGLES_R_P ", parameters,
                commandType: CommandType.StoredProcedure);
        }
        public IEnumerable<ConnectShipPackLable> GetConnectShipReturnLabel(IDbConnection connection, long orderId,
            int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@c_return_label", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<ConnectShipPackLable>("WVR_GETRETURNLABEL_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public ConnectShipSubTaskQtyDetails GetConnectShipSubTaskQty(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@c_shipqty", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.QueryFirstOrDefault<ConnectShipSubTaskQtyDetails>("WVR_GETSUBTASK_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public string GetHdrPassValue(IDbConnection connection, long orderId, int shipId, string field)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_field", field);
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@out_value", dbType: DbType.String, direction: ParameterDirection.ReturnValue);
            var ret = connection.ExecuteScalar("WVR_HDRPASSVALUE_R_F", parameters,
                commandType: CommandType.StoredProcedure);
            //return parameters.Get<string>("@out_value");
            return Convert.ToString(ret);
        }

        public double GetDetailPassValue(IDbConnection connection, long orderId, int shipId, string field, string item,
            string lotnumber)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_field", field);
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@i_item", item);
            parameters.Add("@i_lotnumber", lotnumber);
            parameters.Add("@out_value", dbType: DbType.Double, direction: ParameterDirection.ReturnValue);
            var ret = connection.ExecuteScalar("WVR_DTLPASSVALUE_R_F", parameters,
                commandType: CommandType.StoredProcedure);
            //return parameters.Get<double>("@out_value");
            return ret == null ? 0 : Convert.ToDouble(ret);
        }

        public string GetNextLicensePlate(IDbConnection connection)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@out_next_license_plate", null, DbType.String, ParameterDirection.Output, 16);
            connection.Execute("WVR_GETNEXTLPID_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<string>("@out_next_license_plate");
        }

        public double GetItemUomLength(IDbConnection connection, string custid, string item, string pickuom)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", custid);
            parameters.Add("@i_item", item);
            parameters.Add("@i_pickuom", pickuom);
            parameters.Add("@out_length", null, DbType.Double, ParameterDirection.Output, 16);
            connection.Execute("WVR_GETITEMUOMLENGTH_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<double>("@out_length");
        }

        public double GetItemUomWidth(IDbConnection connection, string custid, string item, string pickuom)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", custid);
            parameters.Add("@i_item", item);
            parameters.Add("@i_pickuom", pickuom);
            parameters.Add("@out_width", null, DbType.Double, ParameterDirection.Output);
            connection.Execute("WVR_GETITEMUOMWIDTH_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<double>("@out_width");
        }

        public double GetItemUomHeight(IDbConnection connection, string custid, string item, string pickuom)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", custid);
            parameters.Add("@i_item", item);
            parameters.Add("@i_pickuom", pickuom);
            parameters.Add("@out_height", null, DbType.Double, ParameterDirection.Output);
            connection.Execute("WVR_GETITEMUOMHEIGHT_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<double>("@out_height");
        }

        public double GetItemWeight(IDbConnection connection, string custid, string item, string uom)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", custid);
            parameters.Add("@i_item", item);
            parameters.Add("@i_uom", uom);
            parameters.Add("@out_weight", null, DbType.Double, ParameterDirection.Output);
            connection.Execute("WVR_GETITEMWEIGHT_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<double>("@out_weight");
        }

        public int GetHazmatCount(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@out_hazmatcount", null, DbType.Int32, ParameterDirection.Output);
            connection.Execute("WVR_GETHAZMATCOUNT_R_P", parameters,
                commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("@out_hazmatcount");
        }

        public bool GetVHazFlag(IDbConnection connection, string custid, string item)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", custid);
            parameters.Add("@i_item", item);
            parameters.Add("@out_vhazflag", null, DbType.Int32, ParameterDirection.Output, 1);
            connection.Execute("WVR_GETVHAZFLAG_R_P", parameters,
                commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("@out_vhazflag") == 1;
        }

        public void UpdateConnectShipHdrHazmat(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            connection.Execute("WVR_UPDATEHAZMAT_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public string GetItemHazmatIndicator(IDbConnection connection, string custid, long orderId, int shipId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", custid);
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@out_hazmatind", null, DbType.String, ParameterDirection.Output, 1);
            connection.Execute("WVR_GETITEMHAZMATIND_R_P", parameters,
                commandType: CommandType.StoredProcedure);
            return parameters.Get<string>("@out_hazmatind");
        }

        public int GetCustLookupDtlMaxLength(IDbConnection connection, string custid)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", custid);
            parameters.Add("@out_max_length", null, DbType.Int32, ParameterDirection.Output);
            connection.Execute("WVR_GETCUSTLOOKUPDTLMAXLENGTH_R_P", parameters,
                commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("@out_max_length");
        }

        public int InsertConnectShipHdr(IDbConnection connection, ConnectShipHeader cHdr)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", cHdr.CustId);
            parameters.Add("@i_opcode", cHdr.OpCode);
            parameters.Add("@i_facility", cHdr.Facility);
            parameters.Add("@i_lpid", cHdr.LpId);
            parameters.Add("@i_orderid", cHdr.OrderId);
            parameters.Add("@i_shipid", cHdr.ShipId);
            parameters.Add("@i_wave", cHdr.Wave);
            parameters.Add("@i_taskid", cHdr.TaskId);
            parameters.Add("@i_carrier", cHdr.Carrier);
            parameters.Add("@i_deliveryservice", cHdr.DeliveryService);
            parameters.Add("@i_sscc", cHdr.Sscc);
            parameters.Add("@i_reference", cHdr.Reference);
            parameters.Add("@i_po", cHdr.Po);
            parameters.Add("@i_connectshipservice", cHdr.ConnectShipService);
            parameters.Add("@i_shiptoname", cHdr.ShipToName);
            parameters.Add("@i_shiptocontact", cHdr.ShipToContact);
            parameters.Add("@i_shiptoaddr1", cHdr.ShipToAddr1);
            parameters.Add("@i_shiptoaddr2", cHdr.ShipToAddr2);
            parameters.Add("@i_shiptoaddr3", cHdr.ShipToAddr3);
            parameters.Add("@i_shiptocity", cHdr.ShipToCity);
            parameters.Add("@i_shiptostate", cHdr.ShipToState);
            parameters.Add("@i_shiptopostalcode", cHdr.ShipToPostalCode);
            parameters.Add("@i_shiptocountry", cHdr.ShipToCountry);
            parameters.Add("@i_shiptophone", cHdr.ShipToPhone);
            parameters.Add("@i_shiptoemail", cHdr.ShipToEmail);
            parameters.Add("@i_residentialflag", cHdr.ResidentialFlag);
            parameters.Add("@i_saturdaydeliveryflag", cHdr.SaturdayDeliveryFlag);
            parameters.Add("@i_signaturerequiredflag", cHdr.SignatureRequiredFlag);
            parameters.Add("@i_terms", cHdr.Terms);
            parameters.Add("@i_seq", cHdr.Seq);
            parameters.Add("@i_seqof", cHdr.SeqOf);
            parameters.Add("@i_billtoname", cHdr.BillToName);
            parameters.Add("@i_billtocontact", cHdr.BillToContact);
            parameters.Add("@i_billtoaddr1", cHdr.BillToAddr1);
            parameters.Add("@i_billtoaddr2", cHdr.BillToAddr2);
            parameters.Add("@i_billtoaddr3", cHdr.BillToAddr3);
            parameters.Add("@i_billtocity", cHdr.BillToCity);
            parameters.Add("@i_billtostate", cHdr.BillToState);
            parameters.Add("@i_billtopostalcode", cHdr.BillToPostalCode);
            parameters.Add("@i_billtocountry", cHdr.BillToCountry);
            parameters.Add("@i_billtophone", cHdr.BillToPhone);
            parameters.Add("@i_billtoemail", cHdr.BillToEmail);
            parameters.Add("@i_thirdpartynumber", cHdr.ThirdPartyNumber);
            parameters.Add("@i_shippernumber", cHdr.ShipperNumber);
            parameters.Add("@i_accountnumber", cHdr.AccountNumber);
            parameters.Add("@i_containertype", cHdr.ContainerType);
            parameters.Add("@i_length", cHdr.Length);
            parameters.Add("@i_width", cHdr.Width);
            parameters.Add("@i_height", cHdr.Height);
            parameters.Add("@i_weight", cHdr.Weight);
            parameters.Add("@i_shipdate", cHdr.ShipDate);
            parameters.Add("@i_packaging", cHdr.Packaging);
            parameters.Add("@i_codamount", cHdr.CodAmount);
            parameters.Add("@i_codmethod", cHdr.CodMethod);
            parameters.Add("@i_declaredvalue", cHdr.DeclaredValue);
            parameters.Add("@i_description", cHdr.Description);
            parameters.Add("@i_sedmethod", cHdr.SedMethod);
            parameters.Add("@i_ultimatecountry", cHdr.UltimateCountry);
            parameters.Add("@i_carrierinstructions", cHdr.CarrierInstructions);
            parameters.Add("@i_ci_method", cHdr.CiMethod);
            parameters.Add("@i_printer", cHdr.Printer);
            parameters.Add("@i_trackingnumber", cHdr.TrackingNumber);
            parameters.Add("@i_cost", cHdr.Cost);
            parameters.Add("@i_labelimagename", cHdr.LabelImageName);
            parameters.Add("@i_errormessage", cHdr.ErrorMessage);
            parameters.Add("@i_msn_id", cHdr.MsnId);
            parameters.Add("@i_creationdate", cHdr.CreationDate);
            parameters.Add("@i_canceldate", cHdr.CancelDate);
            parameters.Add("@i_cancelledby", cHdr.CancelledBy);
            parameters.Add("@i_bypassdate", cHdr.ByPassDate);
            parameters.Add("@i_bypassedby", cHdr.ByPassedBy);
            parameters.Add("@i_receivedate", cHdr.ReceiveDate);
            parameters.Add("@i_status", cHdr.Status);
            parameters.Add("@i_item", cHdr.Item);
            parameters.Add("@i_used", cHdr.Used);
            parameters.Add("@i_hdrpassthruchar01", cHdr.HdrPassThruChar01);
            parameters.Add("@i_hdrpassthruchar02", cHdr.HdrPassThruChar02);
            parameters.Add("@i_hdrpassthruchar03", cHdr.HdrPassThruChar03);
            parameters.Add("@i_hdrpassthruchar04", cHdr.HdrPassThruChar04);
            parameters.Add("@i_hdrpassthruchar05", cHdr.HdrPassThruChar05);
            parameters.Add("@i_hdrpassthruchar06", cHdr.HdrPassThruChar06);
            parameters.Add("@i_hdrpassthruchar07", cHdr.HdrPassThruChar07);
            parameters.Add("@i_hdrpassthruchar08", cHdr.HdrPassThruChar08);
            parameters.Add("@i_hdrpassthruchar09", cHdr.HdrPassThruChar09);
            parameters.Add("@i_hdrpassthruchar10", cHdr.HdrPassThruChar10);
            parameters.Add("@i_hdrpassthruchar11", cHdr.HdrPassThruChar11);
            parameters.Add("@i_hdrpassthruchar12", cHdr.HdrPassThruChar12);
            parameters.Add("@i_hdrpassthruchar13", cHdr.HdrPassThruChar13);
            parameters.Add("@i_hdrpassthruchar14", cHdr.HdrPassThruChar14);
            parameters.Add("@i_hdrpassthruchar15", cHdr.HdrPassThruChar15);
            parameters.Add("@i_hdrpassthruchar16", cHdr.HdrPassThruChar16);
            parameters.Add("@i_hdrpassthruchar17", cHdr.HdrPassThruChar17);
            parameters.Add("@i_hdrpassthruchar18", cHdr.HdrPassThruChar18);
            parameters.Add("@i_hdrpassthruchar19", cHdr.HdrPassThruChar19);
            parameters.Add("@i_hdrpassthruchar20", cHdr.HdrPassThruChar20);
            parameters.Add("@i_hdrpassthrunum01", cHdr.HdrPassThruNum01);
            parameters.Add("@i_hdrpassthrunum02", cHdr.HdrPassThruNum02);
            parameters.Add("@i_hdrpassthrunum03", cHdr.HdrPassThruNum03);
            parameters.Add("@i_hdrpassthrunum04", cHdr.HdrPassThruNum04);
            parameters.Add("@i_hdrpassthrunum05", cHdr.HdrPassThruNum05);
            parameters.Add("@i_hdrpassthrunum06", cHdr.HdrPassThruNum06);
            parameters.Add("@i_hdrpassthrunum07", cHdr.HdrPassThruNum07);
            parameters.Add("@i_hdrpassthrunum08", cHdr.HdrPassThruNum08);
            parameters.Add("@i_hdrpassthrunum09", cHdr.HdrPassThruNum09);
            parameters.Add("@i_hdrpassthrunum10", cHdr.HdrPassThruNum10);
            parameters.Add("@i_hdrpassthrudate01", cHdr.HdrPassThruDate01);
            parameters.Add("@i_hdrpassthrudate02", cHdr.HdrPassThruDate02);
            parameters.Add("@i_hdrpassthrudate03", cHdr.HdrPassThruDate03);
            parameters.Add("@i_hdrpassthrudate04", cHdr.HdrPassThruDate04);
            parameters.Add("@i_hdrpassthrudoll01", cHdr.HdrPassThruDoll01);
            parameters.Add("@i_hdrpassthrudoll02", cHdr.HdrPassThruDoll02);
            parameters.Add("@i_gc3hdrpassthruchar01", cHdr.Gc3HdrPassThruChar01);
            parameters.Add("@i_gc3hdrpassthruchar02", cHdr.Gc3HdrPassThruChar02);
            parameters.Add("@i_gc3hdrpassthruchar03", cHdr.Gc3HdrPassThruChar03);
            parameters.Add("@i_gc3hdrpassthruchar04", cHdr.Gc3HdrPassThruChar04);
            parameters.Add("@i_gc3hdrpassthruchar05", cHdr.Gc3HdrPassThruChar05);
            parameters.Add("@i_gc3hdrpassthruchar06", cHdr.Gc3HdrPassThruChar06);
            parameters.Add("@i_gc3hdrpassthruchar07", cHdr.Gc3HdrPassThruChar07);
            parameters.Add("@i_gc3hdrpassthruchar08", cHdr.Gc3HdrPassThruChar08);
            parameters.Add("@i_gc3hdrpassthruchar09", cHdr.Gc3HdrPassThruChar09);
            parameters.Add("@i_gc3hdrpassthruchar10", cHdr.Gc3HdrPassThruChar10);
            parameters.Add("@i_gc3hdrpassthruchar11", cHdr.Gc3HdrPassThruChar11);
            parameters.Add("@i_gc3hdrpassthruchar12", cHdr.Gc3HdrPassThruChar12);
            parameters.Add("@i_gc3hdrpassthruchar13", cHdr.Gc3HdrPassThruChar13);
            parameters.Add("@i_gc3hdrpassthruchar14", cHdr.Gc3HdrPassThruChar14);
            parameters.Add("@i_gc3hdrpassthruchar15", cHdr.Gc3HdrPassThruChar15);
            parameters.Add("@i_gc3hdrpassthruchar16", cHdr.Gc3HdrPassThruChar16);
            parameters.Add("@i_gc3hdrpassthruchar17", cHdr.Gc3HdrPassThruChar17);
            parameters.Add("@i_gc3hdrpassthruchar18", cHdr.Gc3HdrPassThruChar18);
            parameters.Add("@i_gc3hdrpassthruchar19", cHdr.Gc3HdrPassThruChar19);
            parameters.Add("@i_gc3hdrpassthruchar20", cHdr.Gc3HdrPassThruChar20);
            parameters.Add("@i_gc3hdrpassthrunum01", cHdr.Gc3HdrPassThruNum01);
            parameters.Add("@i_gc3hdrpassthrunum02", cHdr.Gc3HdrPassThruNum02);
            parameters.Add("@i_gc3hdrpassthrunum03", cHdr.Gc3HdrPassThruNum03);
            parameters.Add("@i_gc3hdrpassthrunum04", cHdr.Gc3HdrPassThruNum04);
            parameters.Add("@i_gc3hdrpassthrunum05", cHdr.Gc3HdrPassThruNum05);
            parameters.Add("@i_gc3hdrpassthrunum06", cHdr.Gc3HdrPassThruNum06);
            parameters.Add("@i_gc3hdrpassthrunum07", cHdr.Gc3HdrPassThruNum07);
            parameters.Add("@i_gc3hdrpassthrunum08", cHdr.Gc3HdrPassThruNum08);
            parameters.Add("@i_gc3hdrpassthrunum09", cHdr.Gc3HdrPassThruNum09);
            parameters.Add("@i_gc3hdrpassthrunum10", cHdr.Gc3HdrPassThruNum10);
            parameters.Add("@i_gc3hdrpassthrudate01", cHdr.Gc3HdrPassThruDate01);
            parameters.Add("@i_gc3hdrpassthrudate02", cHdr.Gc3HdrPassThruDate02);
            parameters.Add("@i_gc3hdrpassthrudate03", cHdr.Gc3HdrPassThruDate03);
            parameters.Add("@i_gc3hdrpassthrudate04", cHdr.Gc3HdrPassThruDate04);
            parameters.Add("@i_gc3hdrpassthrudoll01", cHdr.Gc3HdrPassThruDoll01);
            parameters.Add("@i_gc3hdrpassthrudoll02", cHdr.Gc3HdrPassThruDoll02);
            parameters.Add("@i_asnhdrpassthruchar01", cHdr.AsnHdrPassThruChar01);
            parameters.Add("@i_asnhdrpassthruchar02", cHdr.AsnHdrPassThruChar02);
            parameters.Add("@i_asnhdrpassthruchar03", cHdr.AsnHdrPassThruChar03);
            parameters.Add("@i_asnhdrpassthruchar04", cHdr.AsnHdrPassThruChar04);
            parameters.Add("@i_asnhdrpassthruchar05", cHdr.AsnHdrPassThruChar05);
            parameters.Add("@i_asnhdrpassthruchar06", cHdr.AsnHdrPassThruChar06);
            parameters.Add("@i_asnhdrpassthruchar07", cHdr.AsnHdrPassThruChar07);
            parameters.Add("@i_asnhdrpassthruchar08", cHdr.AsnHdrPassThruChar08);
            parameters.Add("@i_asnhdrpassthruchar09", cHdr.AsnHdrPassThruChar09);
            parameters.Add("@i_asnhdrpassthruchar10", cHdr.AsnHdrPassThruChar10);
            parameters.Add("@i_asnhdrpassthruchar11", cHdr.AsnHdrPassThruChar11);
            parameters.Add("@i_asnhdrpassthruchar12", cHdr.AsnHdrPassThruChar12);
            parameters.Add("@i_asnhdrpassthruchar13", cHdr.AsnHdrPassThruChar13);
            parameters.Add("@i_asnhdrpassthruchar14", cHdr.AsnHdrPassThruChar14);
            parameters.Add("@i_asnhdrpassthruchar15", cHdr.AsnHdrPassThruChar15);
            parameters.Add("@i_asnhdrpassthruchar16", cHdr.AsnHdrPassThruChar16);
            parameters.Add("@i_asnhdrpassthruchar17", cHdr.AsnHdrPassThruChar17);
            parameters.Add("@i_asnhdrpassthruchar18", cHdr.AsnHdrPassThruChar18);
            parameters.Add("@i_asnhdrpassthruchar19", cHdr.AsnHdrPassThruChar19);
            parameters.Add("@i_asnhdrpassthruchar20", cHdr.AsnHdrPassThruChar20);
            parameters.Add("@i_asnhdrpassthrunum01", cHdr.AsnHdrPassThruNum01);
            parameters.Add("@i_asnhdrpassthrunum02", cHdr.AsnHdrPassThruNum02);
            parameters.Add("@i_asnhdrpassthrunum03", cHdr.AsnHdrPassThruNum03);
            parameters.Add("@i_asnhdrpassthrunum04", cHdr.AsnHdrPassThruNum04);
            parameters.Add("@i_asnhdrpassthrunum05", cHdr.AsnHdrPassThruNum05);
            parameters.Add("@i_asnhdrpassthrunum06", cHdr.AsnHdrPassThruNum06);
            parameters.Add("@i_asnhdrpassthrunum07", cHdr.AsnHdrPassThruNum07);
            parameters.Add("@i_asnhdrpassthrunum08", cHdr.AsnHdrPassThruNum08);
            parameters.Add("@i_asnhdrpassthrunum09", cHdr.AsnHdrPassThruNum09);
            parameters.Add("@i_asnhdrpassthrunum10", cHdr.AsnHdrPassThruNum10);
            parameters.Add("@i_asnhdrpassthrudate01", cHdr.AsnHdrPassThruDate01);
            parameters.Add("@i_asnhdrpassthrudate02", cHdr.AsnHdrPassThruDate02);
            parameters.Add("@i_asnhdrpassthrudate03", cHdr.AsnHdrPassThruDate03);
            parameters.Add("@i_asnhdrpassthrudate04", cHdr.AsnHdrPassThruDate04);
            parameters.Add("@i_asnhdrpassthrudoll01", cHdr.AsnHdrPassThruDoll01);
            parameters.Add("@i_asnhdrpassthrudoll02", cHdr.AsnHdrPassThruDoll02);
            parameters.Add("@i_subtask_seq", cHdr.SubtaskSeq);
            parameters.Add("@i_batch", cHdr.Batch);
            parameters.Add("@i_location", cHdr.Location);
            parameters.Add("@i_route", cHdr.Route);
            parameters.Add("@i_replacement_order_yn", cHdr.IsReplacementOrder ? "Y" : "N");
            parameters.Add("@i_returntoname", cHdr.ReturnToName);
            parameters.Add("@i_returntocontact", cHdr.ReturnToContact);
            parameters.Add("@i_returntoaddr1", cHdr.ReturnToAddr1);
            parameters.Add("@i_returntoaddr2", cHdr.ReturnToAddr2);
            parameters.Add("@i_returntoaddr3", cHdr.ReturnToAddr3);
            parameters.Add("@i_returntocity", cHdr.ReturnToCity);
            parameters.Add("@i_returntostate", cHdr.ReturnToState);
            parameters.Add("@i_returntopostalcode", cHdr.ReturnToPostalCode);
            parameters.Add("@i_returntocountrycode", cHdr.ReturnToCountryCode);
            parameters.Add("@i_returntophone", cHdr.ReturnToPhone);
            parameters.Add("@i_returntofax", cHdr.ReturnToFax);
            parameters.Add("@i_returntoemail", cHdr.ReturnToEmail);
            parameters.Add("@i_returndcname", cHdr.ReturndCName);
            parameters.Add("@i_returndccontact", cHdr.ReturndCContact);
            parameters.Add("@i_returndcaddr1", cHdr.ReturndCAddr1);
            parameters.Add("@i_returndcaddr2", cHdr.ReturndCAddr2);
            parameters.Add("@i_returndcaddr3", cHdr.ReturndCAddr3);
            parameters.Add("@i_returndccity", cHdr.ReturndCCity);
            parameters.Add("@i_returndcstate", cHdr.ReturndCState);
            parameters.Add("@i_returndcpostalcode", cHdr.ReturndCPostalCode);
            parameters.Add("@i_returndccountrycode", cHdr.ReturndCCountryCode);
            parameters.Add("@i_returndcphone", cHdr.ReturndCPhone);
            parameters.Add("@i_returndcfax", cHdr.ReturndCFax);
            parameters.Add("@i_returndcemail", cHdr.ReturndCEmail);
            parameters.Add("@i_labelimagename2", cHdr.LabelImageName2);
            parameters.Add("@i_return_trackingnumber", cHdr.ReturnTrackingNumber);
            parameters.Add("@i_dtlpassthruchar02", cHdr.DtlPassThruChar02);
            parameters.Add("@i_dtlpassthruchar03", cHdr.DtlPassThruChar03);
            parameters.Add("@i_dtlpassthruchar05", cHdr.DtlPassThruChar05);
            parameters.Add("@i_dtlpassthruchar11", cHdr.DtlPassThruChar11);
            parameters.Add("@i_dtlpassthruchar13", cHdr.DtlPassThruChar13);
            parameters.Add("@i_dtlpassthruchar16", cHdr.DtlPassThruChar16);
            parameters.Add("@i_dtlpassthruchar17", cHdr.DtlPassThruChar17);
            parameters.Add("@i_dtlpassthruchar18", cHdr.DtlPassThruChar18);
            parameters.Add("@i_dtlpassthruchar19", cHdr.DtlPassThruChar19);
            parameters.Add("@i_dtlpassthrunum04", cHdr.DtlPassThruNum04);
            parameters.Add("@i_dtlpassthrudate01", cHdr.DtlPassThruDate01);
            parameters.Add("@i_ediauditchar16", cHdr.EdiAuditChar16);
            parameters.Add("@i_ediauditchar17", cHdr.EdiAuditChar17);
            parameters.Add("@i_itemdescr", cHdr.ItemDescr);
            parameters.Add("@i_bolcomment", cHdr.BolComment);
            parameters.Add("@i_msn_id_rtn", cHdr.MsnIdRtn);
            parameters.Add("@i_dtlpassthrunum01", cHdr.DtlPassThruNum01);
            parameters.Add("@i_dtlpassthrunum02", cHdr.DtlPassThruNum02);
            parameters.Add("@i_dtlpassthrunum03", cHdr.DtlPassThruNum03);
            parameters.Add("@i_dtlpassthrunum05", cHdr.DtlPassThruNum05);
            parameters.Add("@i_dtlpassthrunum06", cHdr.DtlPassThruNum06);
            parameters.Add("@i_dtlpassthrunum07", cHdr.DtlPassThruNum07);
            parameters.Add("@i_dtlpassthrunum08", cHdr.DtlPassThruNum08);
            parameters.Add("@i_dtlpassthrunum09", cHdr.DtlPassThruNum09);
            parameters.Add("@i_dtlpassthrunum10", cHdr.DtlPassThruNum10);
            parameters.Add("@i_dtlpassthruchar01", cHdr.DtlPassThruChar01);
            parameters.Add("@i_dtlpassthruchar04", cHdr.DtlPassThruChar04);
            parameters.Add("@i_dtlpassthruchar06", cHdr.DtlPassThruChar06);
            parameters.Add("@i_dtlpassthruchar07", cHdr.DtlPassThruChar07);
            parameters.Add("@i_dtlpassthruchar08", cHdr.DtlPassThruChar08);
            parameters.Add("@i_dtlpassthruchar09", cHdr.DtlPassThruChar09);
            parameters.Add("@i_dtlpassthruchar10", cHdr.DtlPassThruChar10);
            parameters.Add("@i_dtlpassthruchar12", cHdr.DtlPassThruChar12);
            parameters.Add("@i_dtlpassthruchar14", cHdr.DtlPassThruChar14);
            parameters.Add("@i_dtlpassthruchar15", cHdr.DtlPassThruChar15);
            parameters.Add("@i_dtlpassthruchar20", cHdr.DtlPassThruChar20);
            parameters.Add("@i_dtlpassthrudate02", cHdr.DtlPassThruDate02);
            parameters.Add("@i_dtlpassthrudate03", cHdr.DtlPassThruDate03);
            parameters.Add("@i_dtlpassthrudate04", cHdr.DtlPassThruDate04);
            parameters.Add("@i_dtlpassthrudoll01", cHdr.DtlPassThruDoll01);
            parameters.Add("@i_dtlpassthrudoll02", cHdr.DtlPassThruDoll02);
            parameters.Add("@i_baseuomunitweight", cHdr.BaseUomUnitWeight);
            parameters.Add("@i_trackingnumber2", cHdr.TrackingNumber2);
            parameters.Add("@i_trackingnumber3", cHdr.TrackingNumber3);
            parameters.Add("@i_return_trackingnumber2", cHdr.ReturnTrackingNumber2);
            parameters.Add("@i_return_trackingnumber3", cHdr.ReturnTrackingNumber3);
            parameters.Add("@i_zone", cHdr.Zone);
            parameters.Add("@i_aisle", cHdr.Aisle);
            parameters.Add("@i_printgroupid", cHdr.PrintGroupId);
            parameters.Add("@i_battery_count", cHdr.BatteryCount);
            parameters.Add("@i_cell_count", cHdr.CellCount);
            parameters.Add("@i_lithium_weight", cHdr.LithiumWeight);
            parameters.Add("@i_battery_watthrs", cHdr.BatteryWattHrs);
            parameters.Add("@i_iataprimarychemcode", cHdr.IataPrimaryChemCode);
            parameters.Add("@i_primaryhazardclass", cHdr.PrimaryHazardClass);
            parameters.Add("@i_hazmat", cHdr.Hazmat);
            parameters.Add("@i_ediauditchar01", cHdr.EdiAuditChar01);
            parameters.Add("@i_ediauditchar02", cHdr.EdiAuditChar02);
            parameters.Add("@i_ediauditchar03", cHdr.EdiAuditChar03);
            parameters.Add("@i_ediauditchar04", cHdr.EdiAuditChar04);
            parameters.Add("@i_ediauditchar05", cHdr.EdiAuditChar05);
            parameters.Add("@i_ediauditchar06", cHdr.EdiAuditChar06);
            parameters.Add("@i_ediauditchar07", cHdr.EdiAuditChar07);
            parameters.Add("@i_ediauditchar08", cHdr.EdiAuditChar08);
            parameters.Add("@i_ediauditchar09", cHdr.EdiAuditChar09);
            parameters.Add("@i_ediauditchar10", cHdr.EdiAuditChar10);
            parameters.Add("@i_ediauditchar11", cHdr.EdiAuditChar11);
            parameters.Add("@i_ediauditchar12", cHdr.EdiAuditChar12);
            parameters.Add("@i_ediauditchar13", cHdr.EdiAuditChar13);
            parameters.Add("@i_ediauditchar14", cHdr.EdiAuditChar14);
            parameters.Add("@i_ediauditchar15", cHdr.EdiAuditChar15);
            parameters.Add("@i_ediauditchar18", cHdr.EdiAuditChar18);
            parameters.Add("@i_ediauditchar19", cHdr.EdiAuditChar19);
            parameters.Add("@i_ediauditchar20", cHdr.EdiAuditChar20);
            parameters.Add("@i_labelimagename3", cHdr.LabelImageName3);
            parameters.Add("@i_labelimagename4", cHdr.LabelImageName4);
            parameters.Add("@i_labelimagename5", cHdr.LabelImageName5);
            parameters.Add("@i_labelimagename6", cHdr.LabelImageName6);
            parameters.Add("@i_transmit_to_packsize_yn", cHdr.IsTransmitToPackSizeYn ? "Y" : "N");
            parameters.Add("@i_wcsstatus", cHdr.WcsStatus);
            parameters.Add("@i_wcsprocessdatetime", cHdr.WcsProcessDateTime);
            parameters.Add("@i_premnfst_attempts", cHdr.PremnfstAttempts);
            parameters.Add("@i_hazmatind", cHdr.Hazmatind);
            parameters.Add("@i_toteid", cHdr.ToteId);
            parameters.Add("@i_return_only_yn", cHdr.IsReturnOnly ? "Y" : "N");
            parameters.Add("@i_return_only_lbl_printed", cHdr.ReturnOnlyLblPrinted);
            parameters.Add("@i_return_only_cartonid", cHdr.ReturnOnlyCartonid);

            parameters.Add("@out_success", null, DbType.Int32, ParameterDirection.Output);
            connection.Execute("WVR_CONNECTSHIPHDR_I_P", parameters,
                commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("@out_success");
        }

        public int InsertConnectShipDtl(IDbConnection connection, ConnectShipDtl cDtl)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", cDtl.CustId);
            parameters.Add("@i_facility", cDtl.Facility);
            parameters.Add("@i_lpid", cDtl.LpId);
            parameters.Add("@i_orderid", cDtl.OrderId);
            parameters.Add("@i_shipid", cDtl.ShipId);
            parameters.Add("@i_wave", cDtl.Wave);
            parameters.Add("@i_taskid", cDtl.TaskId);
            parameters.Add("@i_item", cDtl.Item);
            parameters.Add("@i_itemdescription", cDtl.ItemDescription);
            parameters.Add("@i_unitvalue", cDtl.UnitValue);
            parameters.Add("@i_harmonizedtariff", cDtl.HarmonizedTariff);
            parameters.Add("@i_countryoforigin", cDtl.CountryOfOrigin);
            parameters.Add("@i_basequantity", cDtl.BaseQuantity);
            parameters.Add("@i_baseuom", cDtl.BaseUom);
            parameters.Add("@i_baseuomunitweight", cDtl.BaseUomUnitWeight);
            parameters.Add("@i_pickquantity", cDtl.PickQuantity);
            parameters.Add("@i_pickuom", cDtl.PickUom);
            parameters.Add("@i_pickuomunitweight", cDtl.PickUomUnitWeight);
            parameters.Add("@i_licensenum", cDtl.LicenseNum);
            parameters.Add("@i_licenseexpdate", cDtl.LicenseExpDate);
            parameters.Add("@i_userid", cDtl.UserId);
            parameters.Add("@i_harmonized_code", cDtl.HarmonizedCode);
            parameters.Add("@i_hazmat_description", cDtl.HazmatDescription);
            parameters.Add("@i_hazmat_class", cDtl.HazmatClass);
            parameters.Add("@i_hazmat_id", cDtl.HazmatId);
            parameters.Add("@i_hazmat_technical_name", cDtl.HazmatTechnicalName);
            parameters.Add("@i_hazmat_packing_group", cDtl.HazmatPackingGroup);
            parameters.Add("@i_hazmat_abbrev", cDtl.HazmatAbbrev);
            parameters.Add("@i_battery_count", cDtl.BatteryCount);
            parameters.Add("@i_cell_count", cDtl.CellCount);
            parameters.Add("@i_lithium_weight", cDtl.LithiumWeight);
            parameters.Add("@i_battery_watthrs", cDtl.BatteryWattHrs);

            parameters.Add("@out_success", null, DbType.Int32, ParameterDirection.Output);
            connection.Execute("WVR_CONNECTSHIPDTL_I_P", parameters,
                commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("@out_success");
        }

        public void UpdateTasksToUserId(IDbConnection connection, long taskId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_taskid", taskId);
            connection.Execute("WVR_TASKSTOUSERID_U_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public void UpdateSubTasksConnectShipSeq(IDbConnection connection, string stRowId, long subTaskSeq)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_strowid", stRowId);
            parameters.Add("@i_subtaskseq", subTaskSeq);
            connection.Execute("WVR_SUBTASKSCONNECTSHIPSEQ_U_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public Bounding GetRightSizeDims(IDbConnection connection, long cPackJobId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_cpackjobid", cPackJobId);
            parameters.Add("@c_bounding", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.QueryFirstOrDefault<Bounding>("WVR_GETRIGHTSIZEDIMS_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public string GetCshipBarCode(IDbConnection connection, string custid, string type, string pos_11_12)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", custid);
            parameters.Add("@i_type", type);
            parameters.Add("@i_pos_11_12", pos_11_12);
            parameters.Add("@out_barcode", null, DbType.String, ParameterDirection.Output, 20);
            connection.Execute("WVR_GETCSHIPBARCODE_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<string>("@out_barcode");
        }

        public string GetAbbrev(IDbConnection connection, string inEvent)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_event", inEvent);
            parameters.Add("@out_abbrev", null, DbType.String, ParameterDirection.Output, 12);
            connection.Execute("WVR_GETBUSINESSEVENTSABBREV_R_P", parameters,
                commandType: CommandType.StoredProcedure);
            return parameters.Get<string>("@out_abbrev");
        }

        public SubTask GetShipLipByWave(IDbConnection connection, long wave)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@c_shiplip", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.QueryFirstOrDefault<SubTask>("WVR_GETSHIPLIPBYWAVE_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public SubTask GetShipLipByOrderAndShip(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@c_tasklip", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.QueryFirstOrDefault<SubTask>("WVR_GETTASKLIPBYORDERANDSHIP_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<LabelProfileLine> GetProfLine(IDbConnection connection, string inEvent, string profId,
            string uom, string keyOrigin)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_event", inEvent);
            parameters.Add("@i_prof_id", profId);
            parameters.Add("@i_uom", uom);
            parameters.Add("@i_key_origin", keyOrigin);
            parameters.Add("@c_profline", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<LabelProfileLine>("WVR_GETPROFLINE_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public void GetPlateProfId(IDbConnection connection, string inEvent, string lpId, out string uom,
            out string profId, out string outMsg)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_event", inEvent);
            parameters.Add("@i_lpid", lpId);
            parameters.Add("@out_uom", null, DbType.String, ParameterDirection.Output, 4);
            parameters.Add("@out_profid", null, DbType.String, ParameterDirection.Output, 4);
            parameters.Add("@out_msg", null, DbType.String, ParameterDirection.Output, 500);
            connection.Execute("WVR_GETPLATEPROFID_R_P", parameters,
                commandType: CommandType.StoredProcedure);
            uom = parameters.Get<string>("@out_uom");
            profId = parameters.Get<string>("@out_profid");
            outMsg = parameters.Get<string>("@out_msg");
        }

        public OrderHeader GetOrderByTriggerValue(IDbConnection connection, long wave, string custid,
            string standardLabelOverrideColumn, string labelTrigger)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@i_custid", custid);
            parameters.Add("@i_standard_label_override_column", standardLabelOverrideColumn);
            parameters.Add("@i_labeltrigger", labelTrigger);
            parameters.Add("@c_rec", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.QueryFirstOrDefault<OrderHeader>("WVR_GETORDERBYTRIGGERVALUE_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public string ExecuteViewOrProcedure(IDbConnection connection, string viewName, string lpId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_viewname", viewName);
            parameters.Add("@i_lpid", lpId);
            parameters.Add("@out_msg", null, DbType.String, ParameterDirection.Output, 500);
            connection.Execute("WVR_EXECUTEVIEWORPROCEDURE_R_P", parameters,
                commandType: CommandType.StoredProcedure);
            return parameters.Get<string>("@out_msg");
        }

        public string SendZQM(IDbConnection connection)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@out_msg", null, DbType.String, ParameterDirection.Output, 500);
            connection.Execute("WVR_ZQMSEND_I_P", parameters,
                commandType: CommandType.StoredProcedure);
            return parameters.Get<string>("@out_msg");
        }

        public string SetupConveyor(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@out_msg", null, DbType.String, ParameterDirection.Output, 500);
            connection.Execute("WVR_ZCNVSETUPCONVEYOR_U_P", parameters,
                commandType: CommandType.StoredProcedure);
            return parameters.Get<string>("@out_msg");
        }
       
    }
}