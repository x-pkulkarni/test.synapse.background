﻿// <copyright file="PlateDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-11</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-26</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Dapper;
using Oracle.DataAccess.Client;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data
{
    public class PlateDbContext : IPlateDbContext
    {
        public bool UpdateQtyTasked(IDbConnection connection, UpdateQtyTaskedRequest updateQtyTasked)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_qtytasked", updateQtyTasked.QtyAsked);
            parameters.Add("@i_lpid", updateQtyTasked.LpId);
            parameters.Add("@i_lastuser", updateQtyTasked.LastUser);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_PLATEQTYTASKED_U_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowsaffected") > 0;
        }
        
        public IEnumerable<Plate> GetPlateWeightAndQtyByLpId(IDbConnection connection, string lpId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_lpid", lpId);
            parameters.Add("@c_plate", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<Plate>("WVR_PLATEWGTANDQTYBYLPID_R_P", parameters,
                commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<Plate> GetPlateByLpId(IDbConnection connection, string lpId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_lpid", lpId);
            parameters.Add("@c_plate", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<Plate>("WVR_PLATEBYLPID_R_P", parameters, commandType: CommandType.StoredProcedure);
        }

        public bool DeletePlateByLpId(IDbConnection connection, string lpId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_lpid", lpId);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_PLATEBYLPID_D_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowsaffected") > 0;
        }
      
        public bool UpdateQtyTaskedByBatchLpId(IDbConnection connection, UpdateQtyTaskedRequest updateQtyTasked)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_batchqty", updateQtyTasked.QtyAsked);
            parameters.Add("@i_lpid", updateQtyTasked.LpId);
            parameters.Add("@i_facility", updateQtyTasked.Facility);
            parameters.Add("@o_rowsaffected", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("RGP_PLATEQTYBYBTCHLPID_U_P", parameters, commandType: CommandType.StoredProcedure);

            return parameters.Get<int>("o_rowsaffected") > 0;
        }
    }
}