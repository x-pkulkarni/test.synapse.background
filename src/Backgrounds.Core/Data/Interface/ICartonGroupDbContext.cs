﻿// <copyright file="ICartonGroupDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-29</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-07</lastchangeddate>

using System.Data;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    using System.Collections.Generic;
    using Model;

    public interface ICartonGroupDbContext
    {
        string GetCartonGroupByCode(IDbConnection connection, string code);
        string GetCartonTypeByGroup(IDbConnection connection, string group);
        IEnumerable<Container> GetCartonTypesByGroup(IDbConnection connection, string group);
    }
}