﻿using System.Data;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface ISystemDefaultDbContext
    {
        string GetDefaultValueByDefaultId(IDbConnection connection, string defaultId);
    }
}
