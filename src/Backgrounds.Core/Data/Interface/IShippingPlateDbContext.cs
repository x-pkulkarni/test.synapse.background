﻿// <copyright file="IShippingPlateDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-11</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-26</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface IShippingPlateDbContext
    {
        bool Insert(IDbConnection connection, ShippingPlate shippingPlate);

        int GetPendingPlatePicksLine(IDbConnection connection, PendingPickQtyRequest pendingPlatePickQtyRequest);

        int GetPendingPicksVendorItem(IDbConnection connection, PendingPickQtyRequest pendingPlatePickQtyRequest);

        int GetPendingPicksItem(IDbConnection connection, PendingPickQtyRequest pendingPlatePickQtyRequest);

        string GetNextShippingLpId(IDbConnection connection);

        bool DeleteByLpId(IDbConnection connection, string lpId);

        bool DeleteMasterAndCartonByOrderId(IDbConnection connection, long orderId, int shipId);

        IEnumerable<ShippingPlate> GetMasterAndCartonByOrderId(IDbConnection connection, long orderId, int shipId);
        bool UpdateTaskIdInShippingPlate(IDbConnection connection, string shippingPlateId, long taskId);
    }
}