﻿// <copyright file="ICartonGroupDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Kathiresan, Murugan</author> 
// <createddate>2019-01-30</createddate>
namespace Synapse.Backgrounds.Core.Data.Interface
{
    using System.Collections.Generic;
    using System.Data;
    using Model;

    public interface ICartonizeDbContext
    {
        IEnumerable<Container>  GetCartonDetails(IDbConnection connection, string custid, long orderid, string cartonGroup);
        IEnumerable<Item> GetItemDetails(IDbConnection connection, string custid, long orderid);
        IEnumerable<Item> GetSubTaskItemDetails(IDbConnection connection, string taskType, long orderid);
    }
}
