﻿// <copyright file="ITaskDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-11</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-26</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface ITaskDbContext
    {
        bool Insert(IDbConnection connection, Task task);

        IEnumerable<Task> GetByTypeAndOrder(IDbConnection connection, string tasktype, long wave, long orderId,
            int shipId);

        bool Delete(IDbConnection connection, long taskId);

        bool DeleteByOrderAndType(IDbConnection connection, long orderId, int shipId, string taskType);

        bool UpdateTaskQuatity(IDbConnection connection, long taskId, int quantity);

        bool UpdateTask(IDbConnection connection, Task task);
        bool UpdateTaskQuantityAndUom(IDbConnection connection, Task task);
        long GetNextTaskId(IDbConnection connection);

        int GetActiveTasksCount(IDbConnection connection, long wave);

        int GetActiveTasksCountByOrderId(IDbConnection connection, long orderId, int shipId);

        int GetBatchTypeTasksCount(IDbConnection connection, long wave);

        int GetPassedTasksCountById(IDbConnection connection, long taskId);

        bool DeleteByIdAndFacility(IDbConnection connection, long taskId, string facility);

        IEnumerable<Zone> GetTaskGroupingRulesByZone(IDbConnection connection, string zoneId, string facility);
    }
}