﻿// <copyright file="ICommitmentsDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-15</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-15</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface ICommitmentDbContext
    {
        IEnumerable<Commitment> GetCommitmentsByItem(IDbConnection connection, string item);

        IEnumerable<Commitment> GetVendorTrackedCommitments(IDbConnection connection, Commitment commitment);

        IEnumerable<Commitment> GetNonVendorTrackedCommitments(IDbConnection connection, Commitment commitment);

        int? UpdateAndReturnNewQuantity(IDbConnection connection, SubTask task, string inventoryClass, string invStatus,
            string lotNumber);

        bool DeleteBySubTask(IDbConnection connection, SubTask task, string inventoryClass, string invStatus,
            string lotNumber);

        bool ResetPlateLine(IDbConnection connection, long orderId, int shipId);
    }
}
