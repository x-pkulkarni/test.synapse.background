﻿// <copyright file="IParcelStagingDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Kathiresan, Murugan</author> 
// <createddate>2019-04-25</createddate>
namespace Synapse.Backgrounds.Core.Data.Interface
{
    using System.Data;
    using Model;
    using System.Collections.Generic;
    public interface IParcelStagingDbContext
    {
        bool Insert(IDbConnection connection, ParcelStaging task);
        bool GetRecordCountByWave(IDbConnection connection, long waveId);

        IEnumerable<ParcelStaging> GetParcelStagingByWave(IDbConnection connection, long waveId, long orderId,
            int shipId);

        bool UpdatePsRecord(IDbConnection connection, string userId, string rowId);

        bool UpdateCShipRecords(IDbConnection connection, string lpId, string userId);

        bool DeleteCaseAndUccLabels(IDbConnection connection, long orderId, int shipId);
    }
}
