﻿// <copyright file="IBatchTaskDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-11</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-23</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface IBatchTaskDbContext
    {
        bool Insert(IDbConnection connection, BatchTask task);
        IEnumerable<BatchTask> GetBatchTasksByWave(IDbConnection connection, long waveId);
        bool DeleteByTaskIdAndLpId(IDbConnection connection, SubTask task);
        bool DeleteBatchTasksByTaskId(IDbConnection connection, long taskid, string facility);
        bool UpdateTaskId(IDbConnection connection, long wave, long taskId, string lastUser, BatchTask batchTask);
        int GetPendingBatchPicksLine(IDbConnection connection, PendingPickQtyRequest pendingBatchPickQtyRequest);
        int GetPendingBatchVendorItem(IDbConnection connection, PendingPickQtyRequest pendingBatchPickQtyRequest);
        int GetPendingBatchItem(IDbConnection connection, PendingPickQtyRequest pendingBatchPickQtyRequest);
        bool ValidateTaskId(IDbConnection connection, long taskId);        
        bool ResetBatchTasksByTaskId(IDbConnection connection, long taskid, string facility);
        IEnumerable<BatchTask> GetBatchTasksByTaskId(IDbConnection connection, long taskId, string facility);
        bool IsBatchPickSinglesOrder(IDbConnection connection, long wave, string item);
    }
}