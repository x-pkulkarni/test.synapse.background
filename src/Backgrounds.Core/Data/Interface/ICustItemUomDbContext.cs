﻿// <copyright file="ICustItemUomDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-17</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface ICustItemUomDbContext
    {
        double GetCubeFromCustItemUom(IDbConnection connection, CustItemUom custItemUomModel);

        IEnumerable<CustItemUom> GetCustItemUomByUom(IDbConnection connection, CustItemUom custItemUomModel);

        double GetWeightFromCustItemUom(IDbConnection connection, CustItemUom custItemUomModel);

        int GetCustItemSequenceByFromUom(IDbConnection connection, CustItemUom custItemUomModel);

        int GetCustItemSequenceByToUom(IDbConnection connection, CustItemUom custItemUomModel);

        IEnumerable<CustItemUom> GetCustItemQtyByEquivUp(IDbConnection connection, CustItemQtyBySeqRequest custItemQtyBySeqRequest);

        IEnumerable<CustItemUom> GetCustItemQtyByEquivDown(IDbConnection connection, CustItemQtyBySeqRequest custItemQtyBySeqRequest);

        string GetNextHighUom(IDbConnection connection, string custId, string item);

        IEnumerable<CustItemUom> GetCustItemAllUomByUom(IDbConnection connection, string custId, string item);

    }
}
