﻿// <copyright file="IOrderDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-12</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-26</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface IOrderDbContext
    {
        IEnumerable<OrderHeader> GetOrderHeaderByWave(IDbConnection connection, long waveId);

        int GetOrderCountByWave(IDbConnection connection, long waveId);

        bool SendImportExportRequest(IDbConnection connection, string lpid, string edimap);

        IEnumerable<OrderHeader> GetOrderHeaderByOrderId(IDbConnection connection, long orderId, int shipId);

        bool UpdateByOrderId(IDbConnection connection, OrderHeader orderHeader);

        IEnumerable<OrderDetail> GetOrderDetailsById(IDbConnection connection, long orderId, int shipId);

        IEnumerable<OrderDetail> GetOrderDetailsByItem(IDbConnection connection, long orderId, int shipId, string item);

        bool ResetOrderDetails(IDbConnection connection, OrderHeader orderHeader);

        IEnumerable<OrderDetail> GetIataPrimaryChemCode(IDbConnection connection, string item, long orderId, int shipId);

        string GetHazmatFlag(IDbConnection connection, long orderId, int shipId);

        int GetOrderCountBeyondReleased(IDbConnection connection, long wave);

        int GetOrderCountNotReleasedOrCancelled(IDbConnection connection, long wave);

        int GetOrderCountWithAssocaitedPicks(IDbConnection connection, long wave);

        bool AddOrderHistory(IDbConnection connection, OrderHistory history);
        IEnumerable<ParcelStaging> GetFelTaskDtls(IDbConnection connection, long orderId, int shipId);
        int GetOrderCountWithAssocaitedLoad(IDbConnection connection, int? loadNo);
        IEnumerable<ConnectShipPackLable> GetConnectShipLbl(IDbConnection connection, long orderId, int shipId);
        IEnumerable<OrderHeader> GetCShipOrderHeaderByWave(IDbConnection connection, long waveId);
        ConnectShipOrderHdr GetConnectShipOrderHdr(IDbConnection connection, long orderId, int shipId);
        IEnumerable<ConnectShipPackLable> GetConnectShipPack(IDbConnection connection, long orderId, int shipId);
        IEnumerable<ConnectShipPackLable> GetConnectShipReturnLabel(IDbConnection connection, long orderId, int shipId);
        ConnectShipSubTaskQtyDetails GetConnectShipSubTaskQty(IDbConnection connection, long orderId, int shipId);
        string GetHdrPassValue(IDbConnection connection, long orderId, int shipId, string field);

        double GetDetailPassValue(IDbConnection connection, long orderId, int shipId, string field, string item,
            string lotnumber);
        string GetNextLicensePlate(IDbConnection connection);
        double GetItemUomLength(IDbConnection connection, string custid, string item, string pickuom);
        double GetItemUomWidth(IDbConnection connection, string custid, string item, string pickuom);
        double GetItemUomHeight(IDbConnection connection, string custid, string item, string pickuom);
        double GetItemWeight(IDbConnection connection, string custid, string item, string uom);
        //int GetHdrPassValue(IDbConnection connection, long orderId, int shipId);
        int GetHazmatCount(IDbConnection connection, long orderId, int shipId);
        bool GetVHazFlag(IDbConnection connection, string custid, string item);
        void UpdateConnectShipHdrHazmat(IDbConnection connection, long orderId, int shipId);
        string GetItemHazmatIndicator(IDbConnection connection, string custid, long orderId, int shipId);

        IEnumerable<ParcelStaging> GetLtlSubTasks(IDbConnection connection, long orderId, int shipId,string pickToType,string cartonType,int cartonSeq);
        int GetCustLookupDtlMaxLength(IDbConnection connection, string custid);
        int InsertConnectShipHdr(IDbConnection connection, ConnectShipHeader cHdr);
        int InsertConnectShipDtl(IDbConnection connection, ConnectShipDtl cDtl);
        void UpdateTasksToUserId(IDbConnection connection, long taskId);
        void UpdateSubTasksConnectShipSeq(IDbConnection connection, string stRowId, long subTaskSeq);
        Bounding GetRightSizeDims(IDbConnection connection, long cPackJobId);
        string GetCshipBarCode(IDbConnection connection, string custid, string type, string pos_11_12);
        string GetAbbrev(IDbConnection connection, string inEvent);
        SubTask GetShipLipByWave(IDbConnection connection, long wave);
        SubTask GetShipLipByOrderAndShip(IDbConnection connection, long orderId, int shipId);
        IEnumerable<LabelProfileLine> GetProfLine(IDbConnection connection, string inEvent, string profId, string uom,
            string keyOrigin);
        void GetPlateProfId(IDbConnection connection, string inEvent, string lpId, out string uom, out string profId,
            out string outMsg);

        OrderHeader GetOrderByTriggerValue(IDbConnection connection, long wave, string custid,
            string standardLabelOverrideColumn, string labelTrigger);

        string ExecuteViewOrProcedure(IDbConnection connection, string viewName, string lpId);

        string SendZQM(IDbConnection connection);

        string SetupConveyor(IDbConnection connection, long orderId, int shipId);

        IEnumerable<ConnectShipPackLable> GetConnectShipLableForSingles(IDbConnection connection, long orderId,
            int shipId, long taskId);

        IEnumerable<ConnectShipPackLable> GetConnectShipPackForSingles(IDbConnection connection,
            long orderId, int shipId, long taskId);


    }
}