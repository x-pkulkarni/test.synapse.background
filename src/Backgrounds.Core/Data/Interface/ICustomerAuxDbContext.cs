﻿using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface ICustomerAuxDbContext
    {
        IEnumerable<CustomerAux> GetCustomerSettings(IDbConnection connection, string custId);

        string GetPutwallExemptFlag(IDbConnection connection, string custId, string item);

        int GetPutwallCount(IDbConnection connection, string custId, string facility);

        int GetAllocRuleCount(IDbConnection connection, string custId, string facility, string item, string allocNeed);
    }
}
