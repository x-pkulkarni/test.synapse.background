﻿using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface ICustItemCatchWeightDbContext
    {
        IEnumerable<CustItemCatchWeight> GetCustItemCatchWeight(IDbConnection connection, string custId, string item);
    }
}
