﻿// <copyright file="ILoadDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-02-26</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-26</lastchangeddate>

using System.Data;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface ILoadDbContext
    {
        bool UpdateLoadStatusToPlanned(IDbConnection connection, string userId, int? loadNo);

        bool UpdateStatusByLoadNo(IDbConnection connection, string userId, int? loadNo, string status);

        bool UpdateStopStatusByLoadNo(IDbConnection connection, string userId, int? loadNo, string status, int? stopNo);
    }
}