﻿// <copyright file="ICustItemDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-17</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface ICustItemDbContext
    {
        IEnumerable<CustItem> GetCubeFromCustItem(IDbConnection connection, CustItem custItemModel);

        string GetVendorTrackedFlag(IDbConnection connection, string customerId, string item);

        IEnumerable<CustItem> GetWeightFromCustItem(IDbConnection connection, CustItem custItemModel);

        bool GetItemCatchWeightFlag(IDbConnection connection, string customerId, string item);

        IEnumerable<CustItem> GetIataprimaryChemCode(IDbConnection connection, string custId, string item);

        IEnumerable<CustItem> GetCustItemView(IDbConnection connection, string custId, string item);

        bool GetCustItemIsKitFlag(IDbConnection connection, string custId, string item);

        IEnumerable<CustItemSubstitute> GetCustItemSubstituteByItem(IDbConnection connection, string custId,
            string item);
    }
}
