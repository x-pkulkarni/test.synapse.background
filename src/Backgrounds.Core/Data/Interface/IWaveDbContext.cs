﻿// <copyright file="IWaveDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-10</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-10</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface IWaveDbContext
    {
        IEnumerable<Wave> GetWaveById(IDbConnection connection, long waveId);

        bool UpdateWaveStatus(IDbConnection connection, long waveId, string status, string lastUser);

        bool UpdateProcessingIndicatorStart(IDbConnection connection, long wave);
        bool UpdateProcessingIndicatorEnd(IDbConnection connection, long wave);


        bool IsCustomerSetupForCShip(IDbConnection connection, string custid);

        bool IsCustomerSetupForFel(IDbConnection connection, string custid);

      
        IEnumerable<WaveItem> GetItemsInWave(IDbConnection connection, long wave);

        int GetCustLookupCount(IDbConnection connection, string custId, string code);

        IEnumerable<CustLookupDtl> GetCustLookupDtl(IDbConnection connection, string custId,
            string shipToCountryCode);

      }
}
