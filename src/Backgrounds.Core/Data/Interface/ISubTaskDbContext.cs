﻿// <copyright file="ISubTaskDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-11</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-11</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface ISubTaskDbContext
    {
        bool Insert(IDbConnection connection, SubTask task);

        IEnumerable<SubTask> GetSubTasksByWave(IDbConnection connection, long wave, string taskType = null);
        IEnumerable<SubTask> GetSubTasksByWaveId(IDbConnection connection, long wave);

        IEnumerable<SubTask> GetSubTasksByOrder(IDbConnection connection, long orderId, int shipId);

        IEnumerable<SubTask> GetSubTasksByTaskTypeAndOrder(IDbConnection connection, long wave, string taskType,
            long orderId, int shipId);

        int GetSubTaskCountByTaskId(IDbConnection connection, long taskId);

        bool DeleteByRowId(IDbConnection connection, string rowId);

        bool DeleteByTaskIdAndFacility(IDbConnection connection, string facility, long taskId);

        bool UpdateTaskId(IDbConnection connection, string subTaskRecordId, long taskId, string lastUser);

        bool UpdateLtlSeqId(IDbConnection connection, string subTaskRecordId, int? ltlSeq, string lastUser);

        IEnumerable<SubTask> GetLpIdByTaskId(IDbConnection connection, long taskId, string facility);
    }
}
