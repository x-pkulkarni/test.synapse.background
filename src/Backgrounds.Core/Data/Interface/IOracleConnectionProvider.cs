﻿// <copyright file="IOracleConnection.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2018-01-08</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2018-01-08</lastchangeddate>

using System;
using System.Data;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface IOracleConnectionProvider : IDisposable
    {
        IDbConnection GetDbConnection(string key);
        IDbConnection GetProfiledDbConnection(string key);
    }
}