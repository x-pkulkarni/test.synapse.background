﻿using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data.Interface
{
    public interface ILaborStandardsDbContext
    {
        IEnumerable<LaborStandard> GetLaborStdQtyPerHourForUom(IDbConnection connection, LaborStandard laborStandard);

        IEnumerable<LaborStandard> GetLaborStdQtyPerHourForDiffUom(IDbConnection connection, LaborStandard laborStandard);
    }
}
