﻿// <copyright file="CustomerDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-23</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-23</lastchangeddate>


using System.Collections.Generic;
using System.Data;
using Dapper;
using Oracle.DataAccess.Client;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data
{
    public class CustomerDbContext : ICustomerDbContext
    {
        public IEnumerable<Customer> GetCustomerPickByLine(IDbConnection connection, string custId)
        {
            #region Parameters
            var parameters = new OracleDynamicParameters();
            //Input parameter
            parameters.Add("@i_custid", custId);
            parameters.Add("@c_customer", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            #endregion

           return connection.Query<Customer>("WVR_CUSTPCKBYLINE_R_P", parameters, commandType: CommandType.StoredProcedure);
        }
    }
}
