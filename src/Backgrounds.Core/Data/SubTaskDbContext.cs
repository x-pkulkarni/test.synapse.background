﻿// <copyright file="SubTaskDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-11</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-11</lastchangeddate>


using System.Collections.Generic;
using System.Data;
using Dapper;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data
{
    public class SubTaskDbContext : ISubTaskDbContext
    {
        public bool Insert(IDbConnection connection, SubTask subTask)
        {
            #region Parameters
            var parameters = new OracleDynamicParameters();
            //Input parameter
            parameters.Add("@i_taskid", subTask.TaskId);
            parameters.Add("@i_tasktype", subTask.TaskType);
            parameters.Add("@i_facility", subTask.Facility);
            parameters.Add("@i_fromsection", subTask.FromSection);
            parameters.Add("@i_fromlocation", subTask.FromLocation);
            parameters.Add("@i_fromprofile", subTask.FromProfile);
            parameters.Add("@i_tosection", subTask.ToSection);
            parameters.Add("@i_tolocation", subTask.ToLocation);
            parameters.Add("@i_toprofile", subTask.ToProfile);
            parameters.Add("@i_custid", subTask.CustId);
            parameters.Add("@i_item", subTask.Item);
            parameters.Add("@i_lpid", subTask.LpId);
            parameters.Add("@i_uom", subTask.UOM);
            parameters.Add("@i_quantity", subTask.Quantity);
            parameters.Add("@i_locseq", subTask.LocSeq);
            parameters.Add("@i_loadno", subTask.LoadNo);
            parameters.Add("@i_shipno", subTask.ShipNo);
            parameters.Add("@i_stopno", subTask.StopNo);
            parameters.Add("@i_orderid", subTask.OrderId);
            parameters.Add("@i_shipid", subTask.ShipId);
            parameters.Add("@i_orderitem", subTask.OrderItem);
            parameters.Add("@i_orderlot", subTask.OrderLot);
            parameters.Add("@i_priority", subTask.Priority);
            parameters.Add("@i_prevpriority", subTask.PrevPriority);
            parameters.Add("@i_lastuser", subTask.LastUser);
            parameters.Add("@i_pickuom", subTask.PickUom);
            parameters.Add("@i_pickqty", subTask.PickQty);
            parameters.Add("@i_picktotype", subTask.PickToType);
            parameters.Add("@i_wave", subTask.Wave);
            parameters.Add("@i_pickingzone", subTask.PickingZone);
            parameters.Add("@i_cartontype", subTask.CartonType);
            parameters.Add("@i_weight", subTask.Weight);
            parameters.Add("@i_cube", subTask.Cube);
            parameters.Add("@i_staffhrs", subTask.StaffHrs);
            parameters.Add("@i_cartonseq", subTask.CartonSeq);
            parameters.Add("@i_shippinglpid", subTask.ShippingLpId);
            parameters.Add("@i_shippingtype", subTask.ShippingType);
            parameters.Add("@i_cartongroup", subTask.CartonGroup);
            parameters.Add("@i_labeluom", subTask.LabelUom);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            #endregion
            connection.Execute("WVR_SUBTASK_I_P", parameters, commandType: CommandType.StoredProcedure);
            return (int)parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        /// <summary>
        /// Fetches Subtasks by wave. If tasktype parameter is not passed, then it fetches all subtasks for given wave
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="wave"></param>
        /// <param name="taskType">this is an optional field</param>
        /// <returns></returns>
        public IEnumerable<SubTask> GetSubTasksByWave(IDbConnection connection, long wave, string taskType = null)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@i_tasktype", taskType);
            parameters.Add("@c_subtasks", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<SubTask>("WVU_SUBTASKSBYWAVE_R_P", parameters, commandType: CommandType.StoredProcedure);
        }

        /// <summary>
        /// Fetches Subtasks by wave. If tasktype parameter is not passed, then it fetches all subtasks for given wave
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="wave"></param>
        /// <param name="taskType">this is an optional field</param>
        /// <returns></returns>
        public IEnumerable<SubTask> GetSubTasksByWaveId(IDbConnection connection, long wave)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@c_subtasks", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<SubTask>("WVU_SUBTASKSBYWAVEID_R_P", parameters, commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<SubTask> GetSubTasksByOrder(IDbConnection connection, long orderId, int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@c_subtasks", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<SubTask>("WVU_SUBTASKSBYORDER_R_P", parameters, commandType: CommandType.StoredProcedure);
        }
        public IEnumerable<SubTask> GetSubTasksByTaskTypeAndOrder(IDbConnection connection, long wave, string taskType, long orderId, int shipId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_wave", wave);
            parameters.Add("@i_orderid", orderId);
            parameters.Add("@i_shipid", shipId);
            parameters.Add("@i_tasktype", taskType);
            parameters.Add("@c_subtasks", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<SubTask>("WVR_SUBTSKBYORDRANDTYPE_R_P", parameters, commandType: CommandType.StoredProcedure);
        }

        public int GetSubTaskCountByTaskId(IDbConnection connection, long taskId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_taskid", taskId);
            parameters.Add("@o_rowscount", dbType: DbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVU_SUBTSKCOUNTBYTSKID_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_rowscount");
        }

        public bool DeleteByRowId(IDbConnection connection, string rowId)
        {
            var parameters = new OracleDynamicParameters();
            //Input parameter
            parameters.Add("@i_rowid", rowId);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);

            connection.Execute("WVU_SUBTASKBYROWID_D_P", parameters, commandType: CommandType.StoredProcedure);
            return (int)parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public bool DeleteByTaskIdAndFacility(IDbConnection connection, string facility, long taskId)
        {
            var parameters = new OracleDynamicParameters();
            //Input parameter
            parameters.Add("@i_taskid", taskId);
            parameters.Add("@i_facility", facility);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("RGP_SUBTSKBYTSKIDANDFAC_D_P", parameters, commandType: CommandType.StoredProcedure);

            return (int)parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public bool UpdateTaskId(IDbConnection connection, string subTaskRecordId, long taskId, string lastUser)
        {
            var parameters = new OracleDynamicParameters();
            //Input parameter
            parameters.Add("@i_taskid", taskId);
            parameters.Add("@i_rowid", subTaskRecordId);
            parameters.Add("@i_lastuser", lastUser);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_TSKIDOFSUBTSKBYROWID_U_P", parameters, commandType: CommandType.StoredProcedure);

            return (int)parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public bool UpdateLtlSeqId(IDbConnection connection, string subTaskRecordId, int? ltlSeq, string lastUser)
        {
            var parameters = new OracleDynamicParameters();
            //Input parameter
            parameters.Add("@i_ltlseq", ltlSeq);
            parameters.Add("@i_rowid", subTaskRecordId);
            parameters.Add("@i_lastuser", lastUser);
            //Out parameter
            parameters.Add("@o_rowsaffected", dbType: OracleDbType.Int32, direction: ParameterDirection.Output);
            connection.Execute("WVR_LTLSQOFSUBTSKBYROWID_U_P", parameters, commandType: CommandType.StoredProcedure);

            return (int)parameters.Get<OracleDecimal>("o_rowsaffected") > 0;
        }

        public IEnumerable<SubTask> GetLpIdByTaskId(IDbConnection connection, long taskId, string facility)
        {
            #region Parameters

            var parameters = new OracleDynamicParameters();
            //Input parameter
            parameters.Add("@i_taskid", taskId);
            parameters.Add("@i_facility", facility);
            parameters.Add("@c_subtasks", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);

            #endregion

            return connection.Query<SubTask>("RGP_SUBTSKLPIDBYTASKID_R_P", parameters, commandType: CommandType.StoredProcedure);
        }

    }
}
