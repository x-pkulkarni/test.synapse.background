﻿// <copyright file="CustItemDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-17</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Dapper;
using Oracle.DataAccess.Client;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Data
{
    public class CustItemDbContext : ICustItemDbContext
    {
        public IEnumerable<CustItem> GetCubeFromCustItem(IDbConnection connection, CustItem custItemModel)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custItemModel.CustId);
            parameters.Add("@i_item", custItemModel.Item);
            parameters.Add("@c_cube", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<CustItem>("WVR_CUSTITEMCUBE_R_P", parameters, commandType: CommandType.StoredProcedure);
        }
        public IEnumerable<CustItem> GetWeightFromCustItem(IDbConnection connection, CustItem custItemModel)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custItemModel.CustId);
            parameters.Add("@i_item", custItemModel.Item);
            parameters.Add("@c_weight", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<CustItem>("WVR_CUSTITEMWEIGHT_R_P", parameters, commandType: CommandType.StoredProcedure);
        }

        public bool GetItemCatchWeightFlag(IDbConnection connection, string customerId, string item)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", customerId);
            parameters.Add("@i_item", item);
            parameters.Add("@o_flag", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 1);
            connection.Execute("WVR_ITEMCATCHWEIGHTFLAG_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_flag") == 1;
        }

        public string GetVendorTrackedFlag(IDbConnection connection, string customerId, string item)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", customerId);
            parameters.Add("@i_item", item);
            parameters.Add("@o_flag", dbType: DbType.String, direction: ParameterDirection.Output, size: 1);
            connection.Execute("WVR_VENDORTRACKEDFLAG_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<string>("o_flag");
        }

        public IEnumerable<CustItem> GetIataprimaryChemCode(IDbConnection connection, string custId, string item)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custId);
            parameters.Add("@i_item", item);
            parameters.Add("@c_custitem", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<CustItem>("WVR_CUSTITMIATAPRIMCHEMCD_R_P", parameters, commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<CustItem> GetCustItemView(IDbConnection connection, string custId, string item)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custId);
            parameters.Add("@i_item", item);
            parameters.Add("@c_custitem", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<CustItem>("WVR_CUSTITEMVIEW_R_P", parameters, commandType: CommandType.StoredProcedure);
        }

        public bool GetCustItemIsKitFlag(IDbConnection connection, string custId, string item)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@i_custid", custId);
            parameters.Add("@i_orderitem", item);
            parameters.Add("@o_iskit", dbType: DbType.Int32, direction: ParameterDirection.Output, size: 1);
            connection.Execute("RGP_CUSTITEMVIEWISKITFLAG_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<int>("o_iskit") == 1;
        }

        public IEnumerable<CustItemSubstitute> GetCustItemSubstituteByItem(IDbConnection connection, string custId, string item)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@i_custid", custId);
            parameters.Add("@i_orderitem", item);
            parameters.Add("@c_custitemsub", dbType: OracleDbType.RefCursor, direction: ParameterDirection.Output);
            return connection.Query<CustItemSubstitute>("RGP_CUSTITEMSUBBYITEM_R_P", parameters, commandType: CommandType.StoredProcedure);
        }

    }
}
