﻿// <copyright file="OracleConnectionProvider.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2018-01-08</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using System;
using System.Data;
using Oracle.DataAccess.Client;
using StackExchange.Profiling;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Data
{
    public class OracleConnectionProvider : IOracleConnectionProvider
    {
        private bool _disposed;
        private readonly IConfigService _configService;

        public OracleConnectionProvider(IConfigService configService)
        {
            _configService = configService;
        }

        public IDbConnection GetDbConnection(string key)
        {
            var connectionString = _configService.GetConnectionString(key, false);

            if (string.IsNullOrEmpty(connectionString))
            {
                throw new Exception("Connection string not set for environment");
            }

            return new OracleConnection(connectionString);
        }

        public IDbConnection GetProfiledDbConnection(string environment)
        {
            var connectionString = _configService.GetConnectionString(environment, false);
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new Exception("Connection string is not set for the environment");
            }

            return new StackExchange.Profiling.Data.ProfiledDbConnection(new OracleConnection(connectionString), MiniProfiler.Current);

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~OracleConnectionProvider()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing)
            {
                // Do any dispose of Managed components
            }

            // Release any unmanaged
            _disposed = true;
        }
    }
}