﻿// <copyright file="Enums.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-02-07</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

namespace Synapse.Backgrounds.Core
{
    public static class Enums
    {
        public enum CommitStatus
        {
            Uncommitted = 0,
            Selected = 1,
            Planned = 2,
            Released = 3
        }

        public enum WaveStatus
        {
            Committed = 1,
            ReadyForRelease = 2,
            Released = 3,
            Completed = 4
        }
    }
}