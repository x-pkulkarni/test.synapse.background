﻿// <copyright file="IKeyedTypeFactory.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-08-07</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-07</lastchangeddate>

namespace Synapse.Backgrounds.Core.DependencyResolver.Interface
{
    public interface IKeyedTypeFactory
    {
        TImplemation Invoke<TImplemation, TEnum>(TEnum key);
    }
}