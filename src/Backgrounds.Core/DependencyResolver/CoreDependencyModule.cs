﻿using System.Reflection;
using Autofac;
using AutoMapper;
using Synapse.Backgrounds.Core.Handlers;
using Synapse.Backgrounds.Core.Handlers.Interface;
using Synapse.Backgrounds.Core.Profile;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Grains.Contract.Command;
using Module = Autofac.Module;

namespace Synapse.Backgrounds.Core.DependencyResolver
{
    public class CoreDependencyModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).PropertiesAutowired()
                .AsImplementedInterfaces()
                .Except<ReleaseWaveCommandHandler>()
                .Except<UnreleaseWaveCommandHandler>()
                .Except<ResetWaveCommandHandler>()
                .Except<RegenerateBatchCommand>()
                .Except<MapperConfiguration>()
                .Except<IMapper>();
            builder.RegisterType<ReleaseWaveCommandHandler>().Keyed<ICommandHandler>(Integration.Enums.RequestType.RELWAV);
            builder.RegisterType<UnreleaseWaveCommandHandler>().Keyed<ICommandHandler>(Integration.Enums.RequestType.UNRELWAV);
            builder.RegisterType<ResetWaveCommandHandler>().Keyed<ICommandHandler>(Integration.Enums.RequestType.RESETWAV);
            builder.RegisterType<RegenerateBatchCommandHandler>().Keyed<ICommandHandler>(Integration.Enums.RequestType.GENLIPBP);
            
            builder.Register(c => new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(typeof(AutoMapperProfile));

            })).AsSelf().SingleInstance();

            builder.Register(ctx => ctx.Resolve<MapperConfiguration>().CreateMapper()).As<AutoMapper.IMapper>();
            builder.RegisterGeneric(typeof(MappingEngine<>));
        }
    }
}
