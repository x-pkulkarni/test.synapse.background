﻿// <copyright file="KeyedTypeFactory.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-08-07</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-07</lastchangeddate>

using System;
using Autofac;
using Synapse.Backgrounds.Core.DependencyResolver.Interface;

namespace Synapse.Backgrounds.Core.DependencyResolver
{
    public class KeyedTypeFactory : IKeyedTypeFactory
    {
        #region Private Member(S)

        private readonly IComponentContext _autofacContext;

        #endregion

        #region Public Constructor(S)

        public KeyedTypeFactory(IComponentContext autofacContext)
        {
            _autofacContext = autofacContext;
        }

        #endregion

        #region Public Method(S)

        public TImplemation Invoke<TImplemation, TEnum>(TEnum key)
        {
            return _autofacContext.ResolveKeyed<TImplemation>(_autofacContext.IsRegisteredWithKey<TImplemation>(key)
                ? key
                : Enum.IsDefined(typeof(TEnum), "Deafult")
                    ? (TEnum) Enum.Parse(typeof(TEnum), "Deafult")
                    : throw new NotImplementedException());
        }

        #endregion
    }
}