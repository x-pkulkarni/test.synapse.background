﻿// <copyright file="CommandHandlerExecutor.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System.Data;
using System.Threading.Tasks;
using Autofac;
using Synapse.Backgrounds.Core.DependencyResolver.Interface;
using Synapse.Backgrounds.Core.Factories.Interface;
using Synapse.Backgrounds.Core.Handlers.Interface;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Factories
{
    internal class CommandHandlerExecutor : ICommandHandlerExecutor
    {
        private readonly IKeyedTypeFactory _keyedTypeFactory;

        public CommandHandlerExecutor(IKeyedTypeFactory keyedTypeFactory)
        {
            _keyedTypeFactory = keyedTypeFactory;
        }

        public Task ExecuteAsync<TCommand>(IDbConnection connection, TCommand command) where TCommand : class, ICommand
        {
           var objHandler = _keyedTypeFactory.Invoke<ICommandHandler, Integration.Enums.RequestType>(command.Type);
            return objHandler.HandleAsync(connection, command);
        }
    }
}