﻿// <copyright file="Constants.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-02-07</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-26</lastchangeddate>

using System;
using System.Collections.Generic;

namespace Synapse.Backgrounds.Core
{
    public static class Constants
    {
        public class PickType
        {
            public const string Batch = "BAT";
            public const string BatchSingles = "BATS";
            public const string Order = "ORDR";
        }

        public class TaskType
        {
            public const string PickTask = "PK";
            public const string OrderPick = "OP";
            public const string BatchPick = "BP";
            public const string BatchPickSingles = "BS";
        }

        public class PickToType
        {
            public const string FullPallet = "FULL";
            public const string Pallet = "PAL";
            public const string PalletWithLabel = "LBL";
            public const string PickAndPack = "PACK";
            public const string Tote = "TOTE";
        }

        public class TaskPriorities
        {
            public const string Active = "0";
            public const string Immediate = "1";
            public const string High = "2";
            public const string Normal = "3";
            public const string Low = "4";
            public const string PassPending = "7";
            public const string Passed = "8";
            public const string OnHold = "9";
        }

        public class SystemDefaultId
        {
            public const string LaborQtyPerHour = "LABORQTYPERHOUR";
        }

        public class AllocNeeded
        {
            public const string FullPick = "FP";
            public const string BatchPick = "BP";
        }

        public class Flag
        {
            public const string Yes = "Y";
            public const string No = "N";
            public const string Okay = "OKAY";
        }

        public static class OrderStatus 
        {
            public const string Hold = "0";
            public const string Entered = "1";
            public const string Committed = "2";
            public const string Planned = "3";
            public const string Released = "4";
            public const string Picking = "5";
            public const string Picked = "6";
            public const string Loading = "7";
            public const string Loaded = "8";
            public const string Shipped = "9";
            public const string Arrived = "A";
            public const string Cancelled = "X";
            public const string Received = "R";

            public static int Compare(string x, string y)
            {
                var orderSort = new Dictionary<string, int>
                {
                    {Hold, 0},
                    {Entered, 1},
                    {Committed, 2},
                    {Planned, 3},
                    {Released, 4},
                    {Picking, 5},
                    {Picked, 6},
                    {Loading, 7},
                    {Loaded, 8},
                    {Shipped, 9},
                    {Arrived, 10},
                    {Received, 11},
                    {Cancelled, 12}
                };
                if (string.IsNullOrWhiteSpace(x) || string.IsNullOrWhiteSpace(y))
                {
                    throw new NullReferenceException("Input parameters are null or empty!");
                }

                if (!orderSort.ContainsKey(x.ToUpper()) || !orderSort.ContainsKey(y.ToUpper()))
                {
                    throw new NullReferenceException("Input parameters are Invalid!");
                }

                return orderSort[x].CompareTo(orderSort[y]);
            }
        }

        public static class LoadStatus
        {
            public const string Entered = "1";
            public const string Planned = "2";
            public const string PartiallyReleased = "3";
            public const string FullyReleased = "4";
            public const string Picking = "5";
            public const string Picked = "6";
            public const string Loading = "7";
            public const string Loaded = "8";
            public const string Shipped = "9";
            public const string Arrived = "A";
            public const string Received = "R";
            public const string Unloaded = "E";
            public const string Cancelled = "X";
        }
    }
}