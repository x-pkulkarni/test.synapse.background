﻿// <copyright file="CatchWeightAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-19</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using System;
using System.Data;
using System.Linq;
using Autofac;
using AutoMapper;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;

namespace Synapse.Backgrounds.Core.Aggregates
{
    public class CatchWeightAggregate : ICatchWeightAggregate
    {
        #region Constructor(S)

        public CatchWeightAggregate(ILoggerService loggerService, IConfigService configService,
            IOracleConnectionProvider oracleConnection, IComponentContext icoContext)
        {
            _oracleConnection = oracleConnection;
            _configService = configService;
            _custItemDbContext = icoContext.Resolve<ICustItemDbContext>();
            _custItemUomDbContext = icoContext.Resolve<ICustItemUomDbContext>();
            _custItemCatchWeightDbContext = icoContext.Resolve<ICustItemCatchWeightDbContext>();
            _custItemDomain = icoContext.Resolve<ICustItemAggregate>();
            _plateDbContext = icoContext.Resolve<IPlateDbContext>();
            _mapper = icoContext.Resolve<IMapper>();
        }

        #endregion

        #region Private Member(S)

        private readonly IOracleConnectionProvider _oracleConnection;
        private readonly IConfigService _configService;
        private readonly ICustItemDbContext _custItemDbContext;
        private readonly ICustItemUomDbContext _custItemUomDbContext;
        private readonly ICustItemCatchWeightDbContext _custItemCatchWeightDbContext;
        private readonly IPlateDbContext _plateDbContext;
        private readonly ICustItemAggregate _custItemDomain;
        private readonly IMapper _mapper;

        #endregion

        #region Public Method(S)

        /// <summary>
        ///     This function return the weight of the item in the plate
        /// </summary>
        public double GetItemWeightInPlate(IDbConnection connection, string lpId, string custId, string item,
            string uom)
        {
            double lpItemWeight = 0;
            if (_custItemDbContext.GetItemCatchWeightFlag(connection, custId, item))
            {
                var plateInfo = _plateDbContext.GetPlateByLpId(connection, lpId).FirstOrDefault();
                //Calculate the total weight of the quantity processed
                if (plateInfo != null && plateInfo.Quantity > 0)
                {
                    var custItemUomRequest =
                        _custItemDomain.ToCustItemUom(plateInfo.UnitOfMeasure, uom, custId, item, 1);
                    var factor = _custItemDomain.GetUotmQuantity(connection, custItemUomRequest);
                    if (factor != -1)
                    {
                        lpItemWeight = factor * plateInfo.Weight / plateInfo.Quantity;
                    }
                }

                return lpItemWeight;
            }

            // If the quantity is zero find the weight of the item
            return lpItemWeight.Equals(0) ? _custItemDomain.GetItemWeight(connection, custId, item, uom) : lpItemWeight;
        }

        #endregion

        #region Private Method(S)

        #endregion
    }
}