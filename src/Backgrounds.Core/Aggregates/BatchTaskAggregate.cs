﻿// <copyright file="BatchTaskAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-14</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-21</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Core.Profile;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Aggregates
{
    using StackExchange.Profiling.Internal;

    public class BatchTaskAggregate : IBatchTaskAggregate
    {
        #region Constructor(S)

        public BatchTaskAggregate(IBatchTaskAggregateInitializer batchTaskAggregateInitializer)
        {
            _batchTaskDbContext = batchTaskAggregateInitializer.BatchTaskDbContext;
            _subTaskDomain = batchTaskAggregateInitializer.SubTaskDomain;
            _plateAggregate = batchTaskAggregateInitializer.PlateAggregate;
            _taskService = batchTaskAggregateInitializer.TaskService;
            _locationDbContext = batchTaskAggregateInitializer.LocationDbContext;
            _catchWeightDomain = batchTaskAggregateInitializer.CatchWeightDomain;
            _custItem = batchTaskAggregateInitializer.CustItem;
            _laborStandardDoamin = batchTaskAggregateInitializer.LaborStandardDoamin;
        }

        #endregion

        #region Private Member(S)

        private readonly IBatchTaskDbContext _batchTaskDbContext;
        private readonly ISubTaskAggregate _subTaskDomain;
        private readonly IPlateAggregate _plateAggregate;
        private readonly ITaskService _taskService;
        private readonly ILocationDbContext _locationDbContext;
        private readonly ICatchWeightAggregate _catchWeightDomain;
        private readonly ICustItemAggregate _custItem;
        private readonly ILaborStandardAggregate _laborStandardDoamin;

        #endregion

        #region Public Member(S)


        public bool GenerateBatchTasks(IDbConnection connection, Wave wave, ICommand commandRequest, List<BatchTask> batchTasks = null)
        {
            var groupedBatchTasks = new List<BatchTask>();
            if (batchTasks == null)
            {
                groupedBatchTasks = _batchTaskDbContext.GetBatchTasksByWave(connection, commandRequest.Wave).ToList();
            }
            else
            {
                groupedBatchTasks = (from b in batchTasks
                                     group b by new { b.CustomerId, b.OrderItem, b.Item, b.OrderLot, b.LpId, b.FromLocation, b.InvStatus, b.InventoryClass, b.UOM, b.LoadNo, b.ShipNo, b.StopNo, b.PickUom, b.PickToType, b.Facility, b.CartonType, b.QtyType, b.PickingZone, b.PicktaskQty, b.PicktaskUom }
                                     into grpBatch
                                     select new BatchTask
                                     {
                                         CustomerId = grpBatch.Key.CustomerId,
                                         OrderItem = grpBatch.Key.OrderItem,
                                         Item = grpBatch.Key.Item,
                                         OrderLot = grpBatch.Key.OrderLot,
                                         LpId = grpBatch.Key.LpId,
                                         FromLocation = grpBatch.Key.FromLocation,
                                         InvStatus = grpBatch.Key.InvStatus,
                                         InventoryClass = grpBatch.Key.InventoryClass,
                                         UOM = grpBatch.Key.UOM,
                                         PickUom = grpBatch.Key.PicktaskUom,
                                         PickToType = grpBatch.Key.PickToType,
                                         Facility = grpBatch.Key.Facility,
                                         CartonType = grpBatch.Key.CartonType,
                                         QtyType = grpBatch.Key.QtyType,
                                         PickingZone = grpBatch.Key.PickingZone,
                                         Quantity = grpBatch.Sum(t => t.Quantity),
                                         PickQty = grpBatch.Key.PicktaskQty

                                     }
                                     ).ToList();
            }

            var batchLimitCounter = new List<TaskGroupingCounter>();

            groupedBatchTasks.ForEach(b =>
            {
                var plate = _plateAggregate.GetPlateByLpId(connection, b.LpId).FirstOrDefault();

                #region Fetch location details

                var fromLocation = GetLocationdetails(connection, plate?.Location ?? b.FromLocation, commandRequest.Facility);
                var toLocation = GetLocationdetails(connection, wave.SortLocation,
                    commandRequest.Facility);
                #endregion

                var currentCounter = batchLimitCounter.FirstOrDefault(bc => bc.ZoneId == b.PickingZone);
                long taskId = 0;
                if (currentCounter == null || currentCounter.Counter <= 0)
                {
                    taskId = CreateTask(connection, b, commandRequest, fromLocation, toLocation, plate, wave);
                    if (currentCounter != null)
                    {
                        currentCounter.Task = new Task { TaskId = taskId };

                    }
                }

                if (currentCounter == null)
                {
                    var zone = _taskService.GetTaskGroupingRulesByZone(connection, b.PickingZone, b.Facility);
                    var batchTasklimit =
                        zone.SeparateBatchTasksFlag.Equals("Y", StringComparison.CurrentCultureIgnoreCase)
                            ? zone.BatchTasksLimit
                            : 0;
                    currentCounter = new TaskGroupingCounter
                    {
                        Facility = b.Facility,
                        BaseLimit = batchTasklimit,
                        Counter = batchTasklimit,
                        Task = new Task { TaskId = taskId },
                        ZoneId = b.PickingZone
                    };
                    batchLimitCounter.Add(currentCounter);
                }

                b.TaskType = "BP";
                
                _subTaskDomain.CreateSubTaskForBatchPick(connection, b, commandRequest, fromLocation, toLocation, plate,
                     wave,
                     currentCounter.Task);
                currentCounter.Counter -= 1;

              
                _taskService.UpdateTask(connection, currentCounter.Task);
                _batchTaskDbContext.UpdateTaskId(connection, commandRequest.Wave, currentCounter.Task.TaskId,
                    commandRequest.UserId, b);
            });

            return true;
        }

        public bool GenerateBatchTasksForSingles(IDbConnection connection, Wave wave, ICommand commandRequest,
            List<BatchTask> batchTasks)
        {
            var groupedBatchTasks = new List<BatchTask>();
            if (batchTasks == null)
            {
                groupedBatchTasks = _batchTaskDbContext.GetBatchTasksByWave(connection, commandRequest.Wave).ToList();
            }
            else
            {
                groupedBatchTasks = (from b in batchTasks
                                     group b by new { b.CustomerId, b.OrderItem, b.Item, b.OrderLot, b.LpId, b.FromLocation, b.InvStatus, b.InventoryClass, b.UOM, b.LoadNo, b.ShipNo, b.StopNo, b.PickUom,b.PickQty, b.PickToType, b.Facility, b.CartonType, b.QtyType, b.PickingZone, b.PicktaskQty, b.PicktaskUom }
                                     into grpBatch
                                     select new BatchTask
                                     {
                                         CustomerId = grpBatch.Key.CustomerId,
                                         OrderItem = grpBatch.Key.OrderItem,
                                         Item = grpBatch.Key.Item,
                                         OrderLot = grpBatch.Key.OrderLot,
                                         LpId = grpBatch.Key.LpId,
                                         FromLocation = grpBatch.Key.FromLocation,
                                         InvStatus = grpBatch.Key.InvStatus,
                                         InventoryClass = grpBatch.Key.InventoryClass,
                                         UOM = grpBatch.Key.UOM,
                                         PickUom = grpBatch.Key.PicktaskUom,
                                         PickToType = grpBatch.Key.PickToType,
                                         Facility = grpBatch.Key.Facility,
                                         CartonType = grpBatch.Key.CartonType,
                                         QtyType = grpBatch.Key.QtyType,
                                         PickingZone = grpBatch.Key.PickingZone,
                                         Quantity = grpBatch.Sum(t => t.Quantity),
                                         PickQty = grpBatch.Key.PicktaskQty
                                     }
                                     ).ToList();
            }
            var batchLimitCounter = new List<TaskGroupingCounter>();
            groupedBatchTasks.ForEach(b =>
            {
                var plate = _plateAggregate.GetPlateByLpId(connection, b.LpId).FirstOrDefault(x => x.Item == b.Item);

                #region Fetch location details

                var fromLocation = GetLocationdetails(connection, b.FromLocation, commandRequest.Facility);
                var toLocation = GetLocationdetails(connection, wave.SortLocation,
                    commandRequest.Facility);
                #endregion

                var currentCounter = batchLimitCounter.FirstOrDefault(bc => bc.ZoneId == b.PickingZone);
                long taskId = 0;
                if (currentCounter == null || currentCounter.Counter <= 0)
                {
                    taskId = CreateTask(connection, b, commandRequest, fromLocation, toLocation, plate, wave);
                    if (currentCounter != null)
                    {
                        currentCounter.Task = new Task { TaskId = taskId };

                    }
                }

                if (currentCounter == null)
                {
                    var zone = _taskService.GetTaskGroupingRulesByZone(connection, b.PickingZone, b.Facility);
                    var batchTasklimit =
                        zone.SeparateBatchTasksFlag.Equals("Y", StringComparison.CurrentCultureIgnoreCase)
                            ? zone.BatchTasksLimit
                            : 0;
                    currentCounter = new TaskGroupingCounter
                    {
                        Facility = b.Facility,
                        BaseLimit = batchTasklimit,
                        Counter = batchTasklimit,
                        Task = new Task { TaskId = taskId },
                        ZoneId = b.PickingZone
                    };
                    batchLimitCounter.Add(currentCounter);
                }
               
                b.TaskType = "BS";
                b.OrderId = 0;
                _subTaskDomain.CreateSubTaskForBatchPick(connection, b, commandRequest, fromLocation, toLocation, plate,
                      wave,
                      currentCounter.Task);
                currentCounter.Counter -= 1;
               
                _taskService.UpdateTask(connection, currentCounter.Task);
                _batchTaskDbContext.UpdateTaskId(connection, commandRequest.Wave, currentCounter.Task.TaskId,
                    commandRequest.UserId, b);
                
                batchTasks.FindAll(bt => bt.Wave == commandRequest.Wave && bt.CustomerId == b.CustomerId && bt.Item == b.Item && bt.OrderItem == b.OrderItem
                && bt.FromLocation == b.FromLocation && bt.LpId == b.LpId && bt.Facility == b.Facility
                && bt.CartonType == b.CartonType).ForEach(bts => { bts.TaskId = currentCounter.Task.TaskId; });
            });

            return true;
        }

        public int GetPendingBatchVendorItem(IDbConnection connection, PendingPickQtyRequest pendingBatchPickQtyRequest)
        {
            return _batchTaskDbContext.GetPendingBatchVendorItem(connection, pendingBatchPickQtyRequest);
        }

        public int GetPendingBatchItem(IDbConnection connection, PendingPickQtyRequest pendingBatchPickQtyRequest)
        {
            return _batchTaskDbContext.GetPendingBatchItem(connection, pendingBatchPickQtyRequest);
        }

        public bool DeleteByTaskIdAndLpId(IDbConnection connection, SubTask task)
        {
            return _batchTaskDbContext.DeleteByTaskIdAndLpId(connection, task);
        }

        public bool CreateBatchTask(IDbConnection connection, BatchTask task)
        {
            return _batchTaskDbContext.Insert(connection, task);
        }


        public BatchTask CreateBatchTaskObject(IDbConnection connection, OrderTo order, Commitment commitment,
            Location fromLocation, Location toLocation, FindAPickResponse pickResponse, ICommand commandRequest)
        {
            var sources = new Tuple<OrderTo, Commitment, ICommand, Location, Location>(order,
                commitment, commandRequest, fromLocation, toLocation);
            var mapper = new MappingEngine<ItemAggregateMapperProfile>().CreateMapper();
            var request = mapper.Map<BatchTask>(sources);
            if (pickResponse != null)
            {
                if (pickResponse.PickFront.Equals("Y", StringComparison.CurrentCultureIgnoreCase))
                {
                    request.FromLocation = pickResponse.LpIdOrLoc;
                    request.LpId = null;
                }
                else
                {
                    request.LpId = pickResponse.LpIdOrLoc;
                }

                if (request.UOM.IsNullOrWhiteSpace())
                    request.UOM = pickResponse.BaseUom;
                request.CartonType = pickResponse.CartonType;
                request.PickToType = pickResponse.PickToType;
                request.Quantity = pickResponse.BaseQty;
                request.PickQty = pickResponse.PickQty;
                request.PickUom = pickResponse.PickUom;
                request.PicktaskUom = pickResponse.PickUom;
                request.PicktaskQty = pickResponse.PickQty;
                request.LoadNo = order.HeaderInfo.LoadNo;
                request.ShipNo = order.HeaderInfo.ShipNo;
                request.StopNo = order.HeaderInfo.StopNo;
                request.TaskType = "BP";
                var getSingleItemWeight = _catchWeightDomain.GetItemWeightInPlate(connection,
                    pickResponse.LpIdOrLoc,
                    order.HeaderInfo.CustomerId, commitment.Item, commitment.Uom);
                var getSingleItemCube = _custItem.GetItemCube(connection, order.HeaderInfo.CustomerId, commitment.Item, commitment.Uom);
                if (commitment != null)
                {
                    request.Weight = getSingleItemWeight * pickResponse.BaseQty;
                    request.BatchTaskWeight = getSingleItemWeight;
                    request.Cube = getSingleItemCube * pickResponse.BaseQty;
                    request.BatchTaskCube = getSingleItemCube;
                    request.BatchTaskStaffHrs = GetStaffHours(connection, commandRequest.Facility,
                        order.HeaderInfo.CustomerId,
                        commitment.Item, Constants.TaskType.BatchPick, fromLocation?.PickingZone, 1,
                        pickResponse.PickUom);
                    request.StaffHrs = GetStaffHours(connection, commandRequest.Facility, order.HeaderInfo.CustomerId,
                        commitment.Item, Constants.TaskType.BatchPick, fromLocation?.PickingZone, pickResponse.PickQty,
                        pickResponse.PickUom);
                }
            }

            return request;
        }

        public int GetPendingBatchPicksLine(IDbConnection connection, PendingPickQtyRequest pendingBatchPickQtyRequest)
        {
            return _batchTaskDbContext.GetPendingBatchPicksLine(connection, pendingBatchPickQtyRequest);
        }

        #endregion

        #region Private Member(S)

        private Location GetLocationdetails(IDbConnection connection, string locId, string facility)
        {
            var location = new Location
            {
                LocId = locId,
                Facility = facility
            };
            var resLocation = _locationDbContext.GetLocDetailsByLocId(connection, location).ToList();
            return resLocation.Any() ? resLocation.First() : default(Location);
        }

        private long CreateTask(IDbConnection connection, BatchTask batchTask, ICommand waveReleaseRequest,
            Location fromLocation, Location toLocation, Plate plate, Wave waveInfo)
        {
            var source = new Tuple<BatchTask, Plate, ICommand, Location, Location, Wave>(batchTask, plate,
                waveReleaseRequest, fromLocation, toLocation, waveInfo);
            return _taskService.CreateTaskForBatchPick(connection, source);
        }

        private double GetStaffHours(IDbConnection connection,
            string facility, string custId, string item, string category, string pickingZone, int pickQty,
            string pickUom)
        {
            var staffhoursRequest = new StaffHoursRequest
            {
                Facility = facility,
                CustId = custId,
                Item = item,
                Category = category,
                ZoneId = pickingZone,
                Quantity = pickQty,
                Uom = pickUom
            };
            return _laborStandardDoamin.GetStaffHours(connection, staffhoursRequest);
        }

        #endregion
    }
}