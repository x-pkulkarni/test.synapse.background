﻿// <copyright file="SubTaskAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-14</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-21</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Synapse.Backgrounds.Core.Profile;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Aggregates
{
    using System.Collections;
    using AutoMapper;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using StackExchange.Profiling.Internal;

    public class SubTaskAggregate : ISubTaskAggregate
    {

        #region Constructor(S)

        public SubTaskAggregate(ISubTaskAggregateInitializer subTaskAggregateInitializer)
        {
            _batchTaskDbContext = subTaskAggregateInitializer.BatchTaskDbContext;
            _taskService = subTaskAggregateInitializer.TaskService;
            _subTaskDbContext = subTaskAggregateInitializer.SubTaskDbContext;
            _catchWeightDomain = subTaskAggregateInitializer.CatchWeightDomain;
            _custItem = subTaskAggregateInitializer.CustItem;
            _cartonGroupDbContext = subTaskAggregateInitializer.CartonGroupDbContext;
            _laborStandardDoamin = subTaskAggregateInitializer.LaborStandardDoamin;
            _cartonService = subTaskAggregateInitializer.CartonService;
            _plateAggregate = subTaskAggregateInitializer.PlateAggregate;
            _cartonizeDbContext = subTaskAggregateInitializer.CartonizeDbContext;
            _shippingPlateDbContext = subTaskAggregateInitializer.ShippingPlateDbContext;
            _catchWeightAggregate = subTaskAggregateInitializer.CatchWeightAggregate;
            _custItemAggregate = subTaskAggregateInitializer.CustItemAggregate;
            _mapper = new MappingEngine<ItemAggregateMapperProfile>().CreateMapper();
        }

        #endregion

        #region Private Member(S)

        private readonly IBatchTaskDbContext _batchTaskDbContext;
        private readonly ITaskService _taskService;
        private readonly ICartonService _cartonService;
        private readonly ISubTaskDbContext _subTaskDbContext;
        private readonly ICatchWeightAggregate _catchWeightDomain;
        private readonly ICustItemAggregate _custItem;
        private readonly ICartonGroupDbContext _cartonGroupDbContext;
        private readonly ILaborStandardAggregate _laborStandardDoamin;
        private readonly IPlateAggregate _plateAggregate;
        private readonly ICartonizeDbContext _cartonizeDbContext;
        private readonly IShippingPlateDbContext _shippingPlateDbContext;
        private readonly ICatchWeightAggregate _catchWeightAggregate;
        private readonly ICustItemAggregate _custItemAggregate;
        private readonly IMapper _mapper;


        #endregion

        #region Public Member(S)

        public void ResetTasksForUnrelease(IDbConnection connection, OrderUnreleaseRequest request)
        {
            var subTasks = _subTaskDbContext.GetSubTasksByOrder(connection, request.Order.OrderId, request.Order.ShipId)
                .ToList();
            var qtyPerTask = new Dictionary<long, int>();
            subTasks.ForEach(st =>
            {
                _subTaskDbContext.DeleteByRowId(connection, st.RowId);
                _plateAggregate.DeleteShippingPlateByLpId(connection, st.ShippingLpId);
                _plateAggregate.UpdatePlateQtyAsked(connection, st.LastUser, -st.Quantity, st.LpId);
                if (!qtyPerTask.ContainsKey(st.TaskId))
                    qtyPerTask.Add(st.TaskId, st.Quantity);
                else
                    qtyPerTask[st.TaskId] = qtyPerTask[st.TaskId] + st.Quantity;
            });

            //Cleanup batch tasks
            // Subtask of type "BP" will have facility as null
            var batchSubTasks = _subTaskDbContext
                .GetSubTasksByWave(connection, request.WaveId ?? 0, Constants.TaskType.BatchPick).ToList();
            foreach (var bst in batchSubTasks.Where(b => b.TaskType.Equals(Constants.TaskType.BatchPick)))
            {
                _subTaskDbContext.DeleteByRowId(connection, bst.RowId);
                _batchTaskDbContext.DeleteByTaskIdAndLpId(connection, bst);
                _plateAggregate.UpdatePlateQtyAsked(connection, bst.LastUser, bst.Quantity, bst.LpId);
                if (!qtyPerTask.ContainsKey(bst.TaskId))
                    qtyPerTask.Add(bst.TaskId, bst.Quantity);
                else
                    qtyPerTask[bst.TaskId] = qtyPerTask[bst.TaskId] + bst.Quantity;
            }

            //Cleanup batch singles tasks
            // Subtask of type "BS" will have facility as null
            var batchSubTasksSingles = _subTaskDbContext
                .GetSubTasksByWave(connection, request.WaveId ?? 0, Constants.TaskType.BatchPickSingles).ToList();
            foreach (var bst in batchSubTasksSingles.Where(b => b.TaskType.Equals(Constants.TaskType.BatchPickSingles)))
            {
                _subTaskDbContext.DeleteByRowId(connection, bst.RowId);
                _batchTaskDbContext.DeleteByTaskIdAndLpId(connection, bst);
                _plateAggregate.UpdatePlateQtyAsked(connection, bst.LastUser, bst.Quantity, bst.LpId);
                if (!qtyPerTask.ContainsKey(bst.TaskId))
                    qtyPerTask.Add(bst.TaskId, bst.Quantity);
                else
                    qtyPerTask[bst.TaskId] = qtyPerTask[bst.TaskId] + bst.Quantity;
            }

            foreach (var task in qtyPerTask)
            {
                var remainingSubTaskCount = _subTaskDbContext.GetSubTaskCountByTaskId(connection, task.Key);
                if (remainingSubTaskCount.Equals(0) &&
                    _taskService.GetPassedTasksCountById(connection, task.Key).Equals(0))
                    _taskService.Delete(connection, task.Key);
                else
                    _taskService.UpdateTaskQuatity(connection, task.Key, task.Value);
            }
        }

        public bool CreateSubTaskForBatchPick(IDbConnection connection, BatchTask batchTask,
            ICommand waveReleaseRequest,
            Location fromLocation, Location toLocation, Plate plate, Wave waveInfo, Task task)
        {
            var sources = new Tuple<BatchTask, Plate, ICommand, Location, Location, Wave>(batchTask, plate,
                waveReleaseRequest, fromLocation, toLocation, waveInfo);
            var mapper = new MappingEngine<OrderAggregateMapperProfile>().CreateMapper();
            var request = mapper.Map<SubTask>(sources);

            request.TaskId = task.TaskId;
            if (plate != null)
                request.Weight = _catchWeightDomain.GetItemWeightInPlate(connection, plate.LpId,
                                     batchTask.CustomerId, batchTask.Item, batchTask.PickUom) *
                                 batchTask.PickQty;
            request.LoadNo = null;
            request.StopNo = null;
            request.ShipNo = null;
            request.Cube =
                _custItem.GetItemCube(connection, batchTask.CustomerId, batchTask.Item,
                    batchTask.PickUom) * batchTask.PickQty;
            request.StaffHrs = GetStaffHours(connection, waveReleaseRequest.Facility, batchTask.CustomerId,
                batchTask.Item, Constants.TaskType.BatchPick, fromLocation.PickingZone,
                batchTask.PickQty, batchTask.PickUom);
            var newCustitemUom = new CustItemUom
            {
                CustId = batchTask.CustomerId,
                FromUom = batchTask.UOM,
                Item = batchTask.Item,
                Qty = batchTask.PickQty,
                ToUom = batchTask.PickUom
            };
            request.Quantity = batchTask.TaskType.Equals("BP") ? _custItem.GetUotmQuantity(connection, newCustitemUom) * batchTask.PickQty :
                               batchTask.Quantity;
            request.CartonGroup = _cartonGroupDbContext.GetCartonGroupByCode(connection, batchTask.CartonType);
            request.CartonSeq = 1;
            request.TaskType = batchTask.TaskType;

            //var custItemUoms = _custItem.GetItemByUom(connection, batchTask.CustomerId, batchTask.Item,
            //    request.Quantity, batchTask.UOM);
            //foreach (var custItemUom in custItemUoms)
            //{
            //    request.PickQty = custItemUom.Qty;
            //    request.PickUom = custItemUom.ToUom;
            //    request.Quantity = custItemUom.BaseQty;
            //    _subTaskDbContext.Insert(connection, request);

            //}
            var result = _subTaskDbContext.Insert(connection, request);

            #region populate aggregates for task update
            task.PickQty += request.PickQty;
            task.Quantity += request.Quantity;
            task.Weight += request.Weight;
            task.Cube += request.Cube;
            task.StaffHrs += request.StaffHrs;
            task.TaskType = request.TaskType;
            #endregion

            return result;
        }

        public bool CreateSubTask(IDbConnection connection, SubTask request)
        {
            return _subTaskDbContext.Insert(connection, request);
        }

        public bool DeleteByRowId(IDbConnection connection, string rowId)
        {
            return _subTaskDbContext.DeleteByRowId(connection, rowId);
        }

        public int GetSubTaskCountByTaskId(IDbConnection connection, long taskId)
        {
            return _subTaskDbContext.GetSubTaskCountByTaskId(connection, taskId);
        }

        public bool CreateSubTaskForFullPick(IDbConnection connection, OrderTo order, Commitment commitment,
            Location fromLocation, Location toLocation, FindAPickResponse pickResponse, Customer customer,
            ICommand commandRequest, string locId,
            Task task, string taskType, string fromLpId)
        {
            var sources =
                new Tuple<OrderTo, Commitment, ICommand, Location, Location, FindAPickResponse>(
                    order, commitment, commandRequest, fromLocation, toLocation, pickResponse);
            var mapper = new MappingEngine<ItemAggregateMapperProfile>().CreateMapper();
            var request = mapper.Map<SubTask>(sources);
            var result = true;

            SetLabelUom(order.OrderDetail.UomEntered, pickResponse, customer, request);
            request.TaskId = task.TaskId;

            request.FromLocation = locId;
            request.LpId = fromLpId;

            task.PickQty += request.PickQty;
            task.Quantity += request.Quantity;
            task.Weight += request.Weight;
            task.Cube += request.Cube;
            task.StaffHrs += request.StaffHrs;
           
            request.LoadNo = order.HeaderInfo.LoadNo;
            request.ShipNo = order.HeaderInfo.ShipNo;
            request.StopNo = order.HeaderInfo.StopNo;


            try
            {
                CreateShippingPlateAndUpdateRequest(connection, request, order, commitment,
                    fromLocation, pickResponse, commandRequest, task, locId, taskType);

                request.CartonSeq = 1;
                result = _subTaskDbContext.Insert(connection, request);
            }
            catch (Exception e)
            {
                CreateShippingPlateAndUpdateRequest(connection, request, order, commitment,
                    fromLocation, pickResponse, commandRequest, task, locId, taskType);

                request.CartonType = "PAL";
                request.CartonSeq = 1;
                result = _subTaskDbContext.Insert(connection, request);
            }

            return result;
        }

        public void CreateSubTaskForCarton(IDbConnection connection, SubTask findsubtask,Item item,
            OrderHeader orderHeader, Commitment commitment, ICommand commandRequest,
            string cartonType,int sequenceCounter)
        {
            
            if (findsubtask != null)
            {
                var subTask = new SubTask();
                var mapper = new MappingEngine<ItemAggregateMapperProfile>().CreateMapper();
                subTask = mapper.Map<SubTask>(findsubtask);

                subTask.CartonType = cartonType;
                subTask.CartonSeq = sequenceCounter;

                subTask.Quantity = (subTask.Quantity / subTask.PickQty) * item.ItemQuantity;
                subTask.PickQty = item.ItemQuantity;
                subTask.Weight = (subTask.Weight / subTask.PickQty) * item.ItemQuantity;
                subTask.Cube = (subTask.Cube / subTask.PickQty) * item.ItemQuantity;
                subTask.StaffHrs = (subTask.StaffHrs / subTask.PickQty) * item.ItemQuantity;

                findsubtask.PickQty = findsubtask.PickQty - subTask.PickQty;
                findsubtask.Quantity = findsubtask.Quantity - subTask.Quantity;
                var lpId = CreateShippingPlate(connection, orderHeader, commitment,
                    commandRequest, subTask);

                subTask.ShippingLpId = lpId;
                subTask.Weight = _catchWeightDomain.GetItemWeightInPlate(connection, lpId,
                                     orderHeader.CustomerId, subTask.Item, subTask.PickUom) *
                                 subTask.PickQty;

                findsubtask.PickQty = findsubtask.PickQty - subTask.PickQty;
                findsubtask.Quantity = findsubtask.Quantity - subTask.Quantity;

                CreateSubTask(connection, subTask);
            }
        }
        public void CreateShippingPlateAndUpdateRequest(IDbConnection connection, SubTask request, OrderTo order, Commitment commitment,
            Location fromLocation, FindAPickResponse pickResponse, ICommand commandRequest, Task task, string locId, string taskType)
        {

            if (!_custItem.GetCartonizationFlag(connection, order.HeaderInfo.CustomerId))
            {
                var lpId = CreateShippingPlate(connection, order.HeaderInfo, commitment,
                     commandRequest, request);
                request.ShippingLpId = lpId;
                request.Weight = _catchWeightDomain.GetItemWeightInPlate(connection, lpId,
                                     order.HeaderInfo.CustomerId, commitment.Item, pickResponse.PickUom) *
                                 request.PickQty;
            }

           

            request.StaffHrs = GetStaffHours(connection, commandRequest.Facility, order.HeaderInfo.CustomerId,
                commitment.Item, taskType, fromLocation?.PickingZone, request.PickQty, pickResponse.PickUom);
            request.Cube =
                _custItem.GetItemCube(connection, order.HeaderInfo.CustomerId, commitment.Item,
                    pickResponse.PickUom) * request.PickQty;

            #region populate aggregates for task update

            task.PickQty += request.PickQty;
            task.Quantity += request.Quantity;
            task.Weight += request.Weight;
            task.Cube += request.Cube;
            task.StaffHrs += request.StaffHrs;

            #endregion
        }


        public List<SubTask> GetSubTasksByTaskTypeAndOrder(IDbConnection connection, long wave, string taskType,
            long orderId, int shipId)
        {
            return _subTaskDbContext.GetSubTasksByTaskTypeAndOrder(connection, wave, taskType, orderId, shipId)
                .ToList();
        }

        public bool UpdateTaskIdByRowId(IDbConnection connection, string subTaskRecordId, long taskId, string lastUser)
        {
            return _subTaskDbContext.UpdateTaskId(connection, subTaskRecordId, taskId, lastUser);
        }
        #endregion

        public string CreateShippingPlate(IDbConnection connection, OrderHeader order, Commitment commitment,
            ICommand commandRequest, SubTask subTask)
        {
            #region shipping plate request generation

            var sources =
                new Tuple<OrderHeader, Commitment, ICommand, SubTask>(order, commitment,
                    commandRequest, subTask);
            var request = _mapper.Map<ShippingPlate>(sources);
            request.LpId = _shippingPlateDbContext.GetNextShippingLpId(connection);
            request.TaskId = subTask.TaskId;
            request.CartonSeq = 1;
            request.Location = subTask.FromLocation;
            request.FromLpId = subTask.LpId;
            request.Weight = _catchWeightAggregate.GetItemWeightInPlate(connection, request.LpId,
                                 order.CustomerId, commitment.Item, subTask.PickUom) *
                             subTask.PickQty;

            request.HazmatInd = _custItemAggregate.GetItemHazmatIndicator(connection, order.CustomerId,
                order.OrderId, order.ShipId);

            request.IataPrimaryChemCode = _custItemAggregate.GetItemUnCode(connection, order.CustomerId,
                commitment.Item, order.OrderId, order.ShipId);

            #endregion

            _shippingPlateDbContext.Insert(connection, request);
            return request.LpId;
        }


        public IEnumerable<SubTask> GetSubTasksByWaveId(IDbConnection connection, long wave)
        {
            return _subTaskDbContext.GetSubTasksByWaveId(connection, wave);
        }

        #region Private Member(S)



            private double GetStaffHours(IDbConnection connection,
            string facility, string custId, string item, string category, string pickingZone, int pickQty,
            string pickUom)
        {
            var staffhoursRequest = new StaffHoursRequest
            {
                Facility = facility,
                CustId = custId,
                Item = item,
                Category = category,
                ZoneId = pickingZone,
                Quantity = pickQty,
                Uom = pickUom
            };
            return _laborStandardDoamin.GetStaffHours(connection, staffhoursRequest);
        }

        private void SetLabelUom(string uomEntered, FindAPickResponse pickResponse, Customer customer, SubTask task)
        {
            if (customer.IsPickByLineNumber)
            {
                task.LabelUom = uomEntered;
                task.PickToType = Constants.PickToType.PalletWithLabel;
            }
            else if (pickResponse.IsWholeUnitsOnly)
            {
                task.LabelUom = pickResponse.PickUom;
            }
            else
            {
                task.LabelUom = null;
            }
        }

        #endregion
    }
}