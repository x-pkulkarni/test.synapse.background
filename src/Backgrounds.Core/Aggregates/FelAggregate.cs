﻿// <copyright file="FelAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Kathiresan, Murugan</author> 
// <createddate>2019-04-25</createddate>

namespace Synapse.Backgrounds.Core.Aggregates
{
    using System;
    using Data.Interface;
    using Interface;
    using Infrastructure.Logging.Interface;
    using System.Data;
    using System.Linq;
    using AutoMapper;
    using StackExchange.Profiling.Internal;
    using Common;
    using Model;
    using Profile;
    using Grains.Contract;

    #region IFelAggregateInitializer
    public interface IFelAggregateInitializer
    {
       
        IOrderDbContext OrderDbContext { get; }
        ICustItemAggregate CustItemAggregate { get; }
        ITaskService TaskService { get; }
        ICustomerAuxDbContext CustomerAuxDbContext { get; }
        Lazy<ILoggerService> LoggerService { get; }
        IParcelStagingDbContext ParcelStagingDbContext { get; }
        ISubTaskDbContext SubTaskDbContext { get; }
    }

    public class FelAggregateInitializer : IFelAggregateInitializer
    {
        #region Constructor(S)

        public FelAggregateInitializer(IOrderDbContext orderDbContext,
             ITaskService taskService, 
            ICustItemAggregate custItemAggregate, ISubTaskDbContext subTaskDbContext,
            ICustomerAuxDbContext customerAuxDbContext, Lazy<ILoggerService> loggerService, IParcelStagingDbContext parcelStagingDbContext)
        {
            OrderDbContext = orderDbContext;
            TaskService = taskService;
            CustItemAggregate = custItemAggregate;
            CustomerAuxDbContext = customerAuxDbContext;
            LoggerService = loggerService;
            ParcelStagingDbContext = parcelStagingDbContext;
            SubTaskDbContext = subTaskDbContext;
        }

        #endregion

        #region Public Member(S)

       
        public IOrderDbContext OrderDbContext { get; }
        public ITaskService TaskService { get; }
        public ICustItemAggregate CustItemAggregate { get; }
        public ICustomerAuxDbContext CustomerAuxDbContext { get; }
        public Lazy<ILoggerService> LoggerService { get; }
        public IParcelStagingDbContext ParcelStagingDbContext { get; }
        public ISubTaskDbContext SubTaskDbContext { get; }
        #endregion
    }
    #endregion

    public class FelAggregate:IFelAggregate
    {
        #region Constructor(S)
        public FelAggregate(IFelAggregateInitializer felAggregateInitializer)
        {
            _orderDbContext = felAggregateInitializer.OrderDbContext;
            _taskService = felAggregateInitializer.TaskService;
            _parcelStagingDbContext = felAggregateInitializer.ParcelStagingDbContext;
            _mapper = new MappingEngine<OrderAggregateMapperProfile>().CreateMapper();
            _loggerService = felAggregateInitializer.LoggerService;
            _subTaskDbContext = felAggregateInitializer.SubTaskDbContext;
            _custItemAggregate = felAggregateInitializer.CustItemAggregate;
            _customerAuxDbContext = felAggregateInitializer.CustomerAuxDbContext;

        }
        #endregion
        #region Private Member(S)
        private readonly IOrderDbContext _orderDbContext;
        private readonly ITaskService _taskService;
        private readonly IMapper _mapper;
        private readonly IParcelStagingDbContext _parcelStagingDbContext;
        private readonly Lazy<ILoggerService> _loggerService;
        private readonly ISubTaskDbContext _subTaskDbContext;
        private readonly ICustItemAggregate _custItemAggregate;
        private readonly ICustomerAuxDbContext _customerAuxDbContext;
        #endregion

        private const string FelSx = "x";
        private const string FelSy = "y";
        private const string FelNone = "(??)";
        private const string FelN = "N";
        private const string FelY = "Y";
        private const string FelProcess = "PROCESS";
        private const string FelReady = "READY";
        private const string FelCustom = "CUSTOM";
        private const string FelDangerousGoods = "DANGEROUS GOODS";
        private const string FelMultipleLocation = "multiple";
        private const string FelToUser = "(FrntEndLbl)";

        public void ExecuteUnReleaseFelOrder(IDbConnection connection, Wave wave,ICommand unReleaseCommand)
        {
            var lstOrderHeader = _orderDbContext.GetOrderHeaderByWave(connection, unReleaseCommand.Wave).ToList();

            foreach (var orderHeader in lstOrderHeader)
            {
                var orderUnReleaseRequest = new OrderUnreleaseRequest
                {
                    Order = orderHeader,
                    WaveId = wave.Id,
                    Facility = wave.Facility,
                    UserId = unReleaseCommand.UserId,
                    Command = unReleaseCommand
                };
                HandleUnRelease(connection, orderUnReleaseRequest,wave, unReleaseCommand.UserId);
            }
        }

        private void HandleUnRelease(IDbConnection connection, OrderUnreleaseRequest orderUnReleaseRequest,Wave wave,string userId)
        {
            var parcelStagingRows = _parcelStagingDbContext.GetParcelStagingByWave(connection, wave.Id,
                orderUnReleaseRequest.OrderId, orderUnReleaseRequest.ShipId);

            foreach (var parcelStagingRow in parcelStagingRows)
            {
                _parcelStagingDbContext.UpdatePsRecord(connection, userId, parcelStagingRow.RowId);
                
                if (parcelStagingRow.FelConnectShip.Equals("Y"))
                {
                    _parcelStagingDbContext.UpdateCShipRecords(connection, parcelStagingRow.LpId, userId);

                    var request = _mapper.Map<Task>(parcelStagingRow);
                    request.ToUserId = null;
                    request.TaskId = parcelStagingRow.TaskId;
                    _taskService.UpdateTask(connection, request);
                }
                else if(parcelStagingRow.MultiShip.Equals("Y"))
                {
                    _orderDbContext.SendImportExportRequest(connection, parcelStagingRow.LpId, parcelStagingRow.CanMap);
                }
               
            }

            if (wave.IsGenWfelLabels)
            {
                _parcelStagingDbContext.DeleteCaseAndUccLabels(connection, orderUnReleaseRequest.OrderId,
                    orderUnReleaseRequest.ShipId);
            }

        }


        public void TransmitConnectShipOrder(IDbConnection connection, OrderHeader orderHeader, Wave wave)
        {
            _loggerService.Value.Debug("START zfrontendlabeling.transmit_connectship_order");
            _loggerService.Value.Debug("100:Beginning zfrontendlabeling.transmit_connectship_order");
            _loggerService.Value.Debug("zfrontendlabeling.transmit_connectship_order invoked with input parameters:");
            _loggerService.Value.Debug($"  in_orderid= #{orderHeader.OrderId}");
            _loggerService.Value.Debug($"  in_shipid=#{orderHeader.ShipId}");


            var connectShipOrderHdr = _orderDbContext.GetConnectShipOrderHdr(connection, orderHeader.OrderId, orderHeader.ShipId);

            var isInternational = !connectShipOrderHdr.ShipToCountryCode.ToUpper().In("US", "USA");


            if (!connectShipOrderHdr.IsMultiShip)
            {
                return;
            }

            var subTaskQtyDtls =
                _orderDbContext.GetConnectShipSubTaskQty(connection, connectShipOrderHdr.OrderId, connectShipOrderHdr.ShipId);

            var connectShipHdr = GetConnectShipHeader(connection, wave, connectShipOrderHdr, subTaskQtyDtls);

            var connectShipDetails = GetConnectShipDetails(connection, connectShipOrderHdr);

            var lstCustomerAux = _customerAuxDbContext.GetCustomerSettings(connection, connectShipOrderHdr.CustId).ToList()
                .FirstOrDefault();

            if (connectShipHdr.IsReturnOnly)
            {
                connectShipHdr.SeqOf = subTaskQtyDtls.PickQuantity;

                var returnLabel =
                    _orderDbContext.GetConnectShipReturnLabel(connection, connectShipOrderHdr.OrderId, connectShipOrderHdr.ShipId);
                _loggerService.Value.Debug($"  100:fetch c_return_label(#{connectShipOrderHdr.OrderId},#{connectShipOrderHdr.ShipId}");

                long lastTaskId = 0;
                var lastItem = FelSx;
                var lastOrderLot = FelSx;
                var lastPickUom = FelSx;
                var chdrCtr = 0;

                foreach (var retlabel in returnLabel)
                {
                    if (!lastTaskId.Equals(retlabel.TaskId)
                        || !lastItem.Equals(retlabel.Item)
                        || !lastOrderLot.Equals(retlabel.OrderLot)
                        || !lastPickUom.Equals(retlabel.PickUom)
                    )
                    {
                        lastTaskId = retlabel.TaskId;
                        lastItem = retlabel.Item;
                        lastOrderLot = retlabel.OrderLot;
                        lastPickUom = retlabel.PickUom;
                        chdrCtr = 1;
                    }
                    else if (retlabel.PickQty < retlabel.QtyOrder)
                    {
                        chdrCtr++;
                        if (chdrCtr > retlabel.PickQty)
                        {
                            continue;
                        }
                    }

                    connectShipHdr.SubtaskSeq = connectShipHdr.Seq + retlabel.PickQty;
                    var retLblLoopMax = retlabel.OdlDtlPassThruChar17.IsNullOrWhiteSpace() ? retlabel.PickQty : 1;


                    for (var i = 0; i < retLblLoopMax; i++)
                    {
                        InsertConnectShip(connection, ref connectShipHdr, 
                                            ref connectShipDetails, 
                                            connectShipOrderHdr, 
                                            retlabel, lstCustomerAux,
                                            isInternational);
                    }
                }
            }
            else
            {
                _loggerService.Value.Debug($"110:for lbl in c_lbl(#{orderHeader.OrderId},#{orderHeader.ShipId})");
                var lstLbl = _orderDbContext.GetConnectShipLbl(connection, orderHeader.OrderId, orderHeader.ShipId);
                foreach (var lbl in lstLbl)
                {
                    connectShipHdr.SubtaskSeq = connectShipHdr.Seq + lbl.PickQty;

                    for (var i = 0; i < lbl.PickQty; i++)
                    {
                        InsertConnectShip(connection, ref connectShipHdr, ref connectShipDetails, connectShipOrderHdr, lbl, lstCustomerAux,
                            isInternational);
                    }

                    _orderDbContext.UpdateSubTasksConnectShipSeq(connection, lbl.StRowId, connectShipHdr.SubtaskSeq);
                    _orderDbContext.UpdateTasksToUserId(connection, lbl.TaskId);
                }

                var cnt = 0;

                _loggerService.Value.Debug($"140:for pak in c_pak(#{orderHeader.OrderId},#{orderHeader.ShipId})");
                var lstPack = _orderDbContext.GetConnectShipPack(connection, orderHeader.OrderId, orderHeader.ShipId);
                long taskId = -1;
                var cartonType = FelNone;
                var cartonSeq = -1;
                var item = FelNone;

                foreach (var pak in lstPack)
                {
                    if (pak.TaskId != taskId || pak.CartonType != cartonType || pak.Cartonseq != cartonSeq ||
                        pak.Item !=
                        item)
                    {
                        if (cnt == 0)
                        {
                            taskId = pak.TaskId;
                            connectShipHdr.LpId = _orderDbContext.GetNextLicensePlate(connection);
                            _loggerService.Value.Debug($"  101:Get next retlbl lpid #{connectShipHdr.LpId}");
                            if (connectShipHdr.LpId.IsNullOrWhiteSpace())
                            {
                                _loggerService.Value.Debug("zrf.get_next_lpid error");
                                throw new Exception("zrf.get_next_lpid error");
                            }

                            InitPackHeader(connection, pak, lstCustomerAux, connectShipOrderHdr, ref connectShipHdr);
                            cartonType = pak.CartonType;
                            cartonSeq = pak.Cartonseq;

                            InitPackDetails(connection, pak, connectShipOrderHdr, ref connectShipDetails);
                            item = pak.Item;
                        }
                        else if (pak.TaskId.Equals(taskId) && pak.CartonType.Equals(cartonType) &&
                                 pak.Cartonseq.Equals(cartonSeq))
                        {
                            UpdatePackHeader(connection, pak, lstCustomerAux, connectShipOrderHdr, ref connectShipHdr);
                            connectShipDetails.HarmonizedCode = pak.HarmonizedCode;
                            connectShipDetails.HazmatDescription = pak.HazmatDescription;
                            connectShipDetails.HazmatClass = pak.HazmatClass;
                            connectShipDetails.HazmatId = pak.HazmatId;
                            connectShipDetails.HazmatTechnicalName = pak.HazmatTechnicalName;
                            connectShipDetails.HazmatPackingGroup = pak.HazmatPackingGroup;
                            connectShipDetails.HazmatAbbrev = pak.HazmatAbbrev;

                            if (isInternational)
                            {
                                _orderDbContext.InsertConnectShipDtl(connection, connectShipDetails);
                            }

                            InitPackDetails(connection, pak, connectShipOrderHdr, ref connectShipDetails);
                            item = pak.Item;
                        }
                        else
                        {
                            taskId = pak.TaskId;
                            _loggerService.Value.Debug("190:INSERT pak into connectshiphdr");
                            _orderDbContext.InsertConnectShipHdr(connection, connectShipHdr);

                            WriteParcel(connection, connectShipHdr);
                            InitPackHeader(connection, pak,lstCustomerAux, connectShipOrderHdr, ref connectShipHdr);
                            connectShipHdr.LpId = _orderDbContext.GetNextLicensePlate(connection);
                            _loggerService.Value.Debug($"  101:Get next retlbl lpid #{connectShipHdr.LpId}");
                            if (connectShipHdr.LpId.IsNullOrWhiteSpace())
                            {
                                _loggerService.Value.Debug("zrf.get_next_lpid error");
                                throw new Exception("zrf.get_next_lpid error");
                            }

                            cartonType = pak.CartonType;
                            cartonSeq = pak.Cartonseq;

                            connectShipDetails.HarmonizedCode = pak.HarmonizedCode;
                            connectShipDetails.HazmatDescription = pak.HazmatDescription;
                            connectShipDetails.HazmatClass = pak.HazmatClass;
                            connectShipDetails.HazmatId = pak.HazmatId;
                            connectShipDetails.HazmatTechnicalName = pak.HazmatTechnicalName;
                            connectShipDetails.HazmatPackingGroup = pak.HazmatPackingGroup;
                            connectShipDetails.HazmatAbbrev = pak.HazmatAbbrev;
                            if (isInternational)
                            {
                                _orderDbContext.InsertConnectShipDtl(connection, connectShipDetails);
                            }

                            InitPackDetails(connection, pak, connectShipOrderHdr, ref connectShipDetails);
                            item = pak.Item;
                        }
                    }
                    else
                    {
                        UpdatePackHeader(connection, pak, lstCustomerAux, connectShipOrderHdr, ref connectShipHdr);
                        UpdatePackDetail(connection, pak, connectShipOrderHdr, ref connectShipDetails);
                    }

                    cnt++;

                    _orderDbContext.UpdateSubTasksConnectShipSeq(connection, pak.StRowId, connectShipHdr.SubtaskSeq);
                    _orderDbContext.UpdateTasksToUserId(connection, pak.TaskId);
                }

                if (cnt <= 0)
                {
                    return;
                }

                _orderDbContext.InsertConnectShipHdr(connection, connectShipHdr);

                if (isInternational)
                {
                    _orderDbContext.InsertConnectShipDtl(connection, connectShipDetails);
                }

                WriteParcel(connection, connectShipHdr);

            }
        }
        public ConnectShipHeader GetConnectShipHeader(IDbConnection connection, Wave wave,
            ConnectShipOrderHdr connectShipOrderHdr, ConnectShipSubTaskQtyDetails subTaskQty)
        {
            var connectShipHeader = _mapper.Map<ConnectShipHeader>(connectShipOrderHdr);
            connectShipHeader.OrderId = connectShipOrderHdr.OrderId;
            connectShipHeader.ShipId = connectShipOrderHdr.ShipId;
            connectShipHeader.OpCode = FelProcess;
            connectShipHeader.Seq = 0;

            connectShipHeader.IsTransmitToPackSizeYn = wave.IsTransmitToPackSize;
            connectShipHeader.WcsStatus = FelReady;
            connectShipHeader.WcsProcessDateTime = DateTime.Now;
            _loggerService.Value.Debug($"  transmit_to_packsize_yn=#{wave.IsTransmitToPackSize}");


            connectShipHeader.SeqOf = subTaskQty.PickQuantity + subTaskQty.CartonCount;
            connectShipHeader.ThirdPartyNumber = _orderDbContext.GetHdrPassValue(connection, connectShipOrderHdr.OrderId, connectShipHeader.ShipId,
                connectShipOrderHdr.ThirdPartyNumberPassThru);

            connectShipHeader.ShipperNumber = _orderDbContext.GetHdrPassValue(connection, connectShipOrderHdr.OrderId, connectShipHeader.ShipId,
                connectShipOrderHdr.ShipperNumberPassThru);

            connectShipHeader.ShipDate = connectShipOrderHdr.ShipDate.HasValue ? Convert.ToDateTime(connectShipOrderHdr.ShipDate) : DateTime.Now;

            connectShipHeader.Packaging = FelCustom;

            connectShipHeader.CodAmount = connectShipOrderHdr.AmtCod / connectShipHeader.SeqOf;

            if (!connectShipOrderHdr.Cod.IsNullOrWhiteSpace())
            {
                if (connectShipOrderHdr.IsCodCashierCheckMoOnly)
                {
                    connectShipHeader.CodMethod = "2";
                }
                else if (connectShipOrderHdr.IsCompanyCheckOk)
                {
                    connectShipHeader.CodMethod = "4";
                }
                else if (!connectShipOrderHdr.CodCollectAccount.IsNullOrWhiteSpace())
                {
                    connectShipHeader.CodMethod = "1";
                }
                else
                {
                    connectShipHeader.CodMethod = "31";
                }
            }

            connectShipHeader.SedMethod = _orderDbContext.GetHdrPassValue(connection, connectShipHeader.OrderId, connectShipHeader.ShipId,
                connectShipOrderHdr.SedMethodPassThru);
            connectShipHeader.UltimateCountry = _orderDbContext.GetHdrPassValue(connection, connectShipHeader.OrderId, connectShipHeader.ShipId,
                connectShipOrderHdr.UltimateCountryPassThru);
            connectShipHeader.CarrierInstructions = _orderDbContext.GetHdrPassValue(connection, connectShipHeader.OrderId,
                connectShipHeader.ShipId,
                connectShipOrderHdr.CarrierInstructionsPassThru);
            connectShipHeader.CiMethod = _orderDbContext.GetHdrPassValue(connection, connectShipHeader.OrderId, connectShipHeader.ShipId,
                connectShipOrderHdr.CiMethodPassThru);

            connectShipHeader.CreationDate = DateTime.Now;

            connectShipHeader.Status = FelReady;
            //cHdr.Zone = pack.PickingZone;
            //cHdr.Aisle = pack.Aisle;
            connectShipHeader.IsReturnOnly = connectShipOrderHdr.IsBulkReturn;
            connectShipHeader.ReturnOnlyLblPrinted = FelN;
            connectShipHeader.ReturnOnlyCartonid = null;

            return connectShipHeader;
        }

        public void InsertConnectShip(IDbConnection connection, 
            ref ConnectShipHeader connectShipHeader, ref ConnectShipDtl connectShipDtl,
            ConnectShipOrderHdr connectShipOrderHdr, ConnectShipPackLable pak, CustomerAux lstCustomerAux,
            bool isInternational)
        {
            
            connectShipHeader = _mapper.Map(pak, connectShipHeader);
            connectShipDtl = _mapper.Map(pak, connectShipDtl);

            connectShipHeader.LpId = _orderDbContext.GetNextLicensePlate(connection);
            _loggerService.Value.Debug($"  101:Get next retlbl lpid #{connectShipHeader.LpId}");
            if (connectShipHeader.LpId.IsNullOrWhiteSpace())
            {
                _loggerService.Value.Debug("zrf.get_next_lpid error");
                throw new Exception("zrf.get_next_lpid error");
            }

            connectShipHeader.Sscc = _orderDbContext.GetCshipBarCode(connection, connectShipOrderHdr.CustId, "0", connectShipOrderHdr.BarCodePos_11_12);

            connectShipHeader.Seq = connectShipHeader.Seq + 1;
            connectShipHeader.Length =
                _orderDbContext.GetItemUomLength(connection, connectShipOrderHdr.CustId, pak.Item, pak.PickUom);
            connectShipHeader.Width =
                _orderDbContext.GetItemUomWidth(connection, connectShipOrderHdr.CustId, pak.Item, pak.PickUom);
            connectShipHeader.Height =
                _orderDbContext.GetItemUomHeight(connection, connectShipOrderHdr.CustId, pak.Item, pak.PickUom);

            connectShipHeader.ContainerType = connectShipHeader.Length + FelSx + connectShipHeader.Width + FelSx + connectShipHeader.Height;

            if (pak.PickQty > 0)
            {
                connectShipHeader.Weight = pak.Weight / pak.PickQty;
            }

            _loggerService.Value.Debug($"102:retlbl weight per unit = #{connectShipHeader.Weight}");

            connectShipHeader.DeclaredValue = _orderDbContext.GetDetailPassValue(connection, connectShipOrderHdr.OrderId,
                connectShipOrderHdr.ShipId,
                connectShipOrderHdr.DeclaredValuePassThru, pak.OrderItem, pak.OrderLot);
            connectShipHeader.BaseUomUnitWeight =
                _orderDbContext.GetItemWeight(connection, connectShipOrderHdr.CustId, pak.Item, pak.Uom);

            _loggerService.Value.Debug($"103:retlbl connectshphdr custid <#{connectShipOrderHdr.CustId}>, item <#{pak.Item}");

            var vhazmat= Fillhazmat(connection, connectShipOrderHdr.OrderId,connectShipOrderHdr.ShipId, connectShipOrderHdr.CustId, pak.Item);
            connectShipHeader.Hazmatind = _orderDbContext.GetItemHazmatIndicator(connection, connectShipOrderHdr.CustId, connectShipHeader.OrderId,
                connectShipHeader.ShipId);

            var iataPrimaryChemCode = lstCustomerAux.IsUseItemUncodeFromOrder
                ? pak.OrderDtlIataPrimaryChemCode
                : pak.CustItemIataPrimaryChemCode;


            var maxLength = _orderDbContext.GetCustLookupDtlMaxLength(connection, connectShipOrderHdr.CustId);

            connectShipHeader.IataPrimaryChemCode = iataPrimaryChemCode?.Length > maxLength ? FelDangerousGoods : iataPrimaryChemCode;


            connectShipHeader.PrimaryHazardClass = pak.CustItemPrimaryHazardClass;
            connectShipHeader.Hazmat = vhazmat;
            _loggerService.Value.Debug("105:retlbl insert into connectshiphdr");
            _orderDbContext.InsertConnectShipHdr(connection, connectShipHeader);

            if (isInternational)
            {
                connectShipDtl.LpId = connectShipHeader.LpId;
                connectShipDtl.UnitValue = _orderDbContext.GetDetailPassValue(connection, connectShipHeader.OrderId,
                                               connectShipHeader.ShipId, connectShipOrderHdr.UnitValuePassThru, pak.OrderItem,
                                     pak.OrderLot) / pak.QtyOrder;
                connectShipDtl.BaseQuantity = 1;
                connectShipDtl.BaseUomUnitWeight =
                    _orderDbContext.GetItemWeight(connection, connectShipOrderHdr.CustId, pak.Item, pak.Uom);
                connectShipDtl.PickQuantity = 1;
                connectShipDtl.PickUomUnitWeight =
                    _orderDbContext.GetItemWeight(connection, connectShipOrderHdr.CustId, pak.Item, pak.PickUom);

                _orderDbContext.InsertConnectShipDtl(connection, connectShipDtl);
            }

            _loggerService.Value.Debug("106:retlbl write_parcel");
            WriteParcel(connection, connectShipHeader);
        }
        public void WriteParcel(IDbConnection connection, ConnectShipHeader cHdr)
        {
            _loggerService.Value.Debug("200:pak write_parcel");
            var parcelStaging = new ParcelStaging();
            parcelStaging.LpId = cHdr.LpId;
            parcelStaging.OrderId = cHdr.OrderId;
            parcelStaging.ShipId = cHdr.ShipId;
            parcelStaging.Wave = cHdr.Wave;
            parcelStaging.TaskId = cHdr.TaskId;
            parcelStaging.Status = 'S';
            parcelStaging.Facility = cHdr.Facility;
            parcelStaging.Carrier = cHdr.Carrier;
            parcelStaging.CustId = cHdr.CustId;
            parcelStaging.DeliveryService = cHdr.DeliveryService;
            parcelStaging.Item = cHdr.Item;
            _parcelStagingDbContext.Insert(connection, parcelStaging);
        }
        public void InsertParcelStaging(IDbConnection connection, int taskSequence, ParcelStaging ltlTask, int qty)
        {
            if (taskSequence.Equals(3))
            {
                _parcelStagingDbContext.Insert(connection, ltlTask);
            }
            else
            {
                var lCount = ltlTask.PickUom.In("PL") && qty > 0 ? qty : 1;
                if (!ltlTask.PickUom.Equals("PL"))
                {
                    var custItemUomRequest = new CustItemUom
                    {
                        CustId = ltlTask.CustId,
                        Item = ltlTask.Item,
                        FromUom = ltlTask.PickUom,
                        ToUom = ltlTask.Uom,
                        Qty = ltlTask.Qty
                    };
                    lCount = _custItemAggregate.GetUotmQuantity(connection, custItemUomRequest);
                }

                for (var i = 0; i < lCount; i++)
                {
                    _parcelStagingDbContext.Insert(connection, ltlTask);
                }
            }

            var request = _mapper.Map<Task>(ltlTask);
            request.ToUserId = FelToUser;
            _taskService.UpdateTask(connection, request);
        }
        public void TransmitLtlOrder(IDbConnection connection, OrderHeader cShipOrder)
        {
            var subTasks = _subTaskDbContext.GetSubTasksByOrder(connection, cShipOrder.OrderId, cShipOrder.ShipId);
            foreach (var subTask in subTasks)
            {
                if (subTask.PickToType.In("PACK", "FULL"))
                {
                    var ltlTask = _orderDbContext.GetLtlSubTasks(connection, cShipOrder.OrderId, cShipOrder.ShipId,
                        subTask.TaskType, subTask.CartonType, subTask.CartonSeq).FirstOrDefault();

                    if (ltlTask == null)
                    {
                        continue;
                    }

                    ltlTask.LtlSeq += 1;
                    ltlTask.Status = 'R';
                    InsertParcelStaging(connection, 3, ltlTask, 0);

                    _subTaskDbContext.UpdateLtlSeqId(connection, subTask.RowId, ltlTask.LtlSeq, FelToUser);
                }
                else
                {
                    var ltlTasks = _orderDbContext.GetLtlSubTasks(connection, cShipOrder.OrderId, cShipOrder.ShipId,
                        subTask.TaskType, subTask.CartonType, subTask.CartonSeq).ToList();

                    if (!ltlTasks.Any())
                    {
                        break;
                    }

                    foreach (var ltlTask in ltlTasks)
                    {
                        ltlTask.LtlSeq += 1;
                        ltlTask.Status = 'R';
                        if (subTask.PickToType.Equals("LBL") && ltlTask.PickUom.Equals("PL") && ltlTask.PickQty > 1)
                        {
                            InsertParcelStaging(connection, 2, ltlTask, ltlTask.PickQty);
                        }
                        else
                        {
                            InsertParcelStaging(connection, 0, ltlTask, 0);
                        }

                        _subTaskDbContext.UpdateLtlSeqId(connection, subTask.RowId, ltlTask.LtlSeq, FelToUser);
                    }
                }
            }
        }
        public void TransmitFelOrder(IDbConnection connection, OrderHeader cShipOrder)
        {
            var felTasks = _orderDbContext.GetFelTaskDtls(connection, cShipOrder.OrderId, cShipOrder.ShipId);
            //Loop through associated subtasks for this order 
            foreach (var felTask in felTasks)
            {
                for (var i = 0; i < felTask.PickQty; i++)
                {
                    felTask.LpId = _orderDbContext.GetNextLicensePlate(connection);
                    felTask.Status = 'S';
                    /* Create a new record using our new plate ID we just retrieved.  This is our initial record inserted into
                 the parcelstagingtable, they status is set to 'S' the initial status.  The needs_eod is set to
                 Y to indicate that this process will still require end of day processing.  Once this is processed the value will
                 be set to N and the eod_pending will be flipped to Y.
                     */

                    _parcelStagingDbContext.Insert(connection, felTask);

                    /*This is the import/export request we make.  It is a "NOW" export request using the
                      custfrontendlabel EDI map for the customer ID on teh current subtask we are working with.
                      */
                    _orderDbContext.SendImportExportRequest(connection, felTask.LpId, felTask.EdiMap);
                }

                /*[JM] After we process all the subtasks for this order, we need to update the associated task
                  to indicate to the system that this task can only be processed using the FEL RF picking
                  screen.  FEL enforces the plate ID's on the picks given the fact that labels are already
                  generated for them.
                */
                var request = _mapper.Map<Task>(felTask);
                request.ToUserId = FelToUser;
                _taskService.UpdateTask(connection, request);
            }
        }
        public string Fillhazmat(IDbConnection connection, long orderId,int shipId, string custId, string item)
        {
            string vhazmat;
            var hazmatCount=0;
            var vhazFlag = _orderDbContext.GetVHazFlag(connection, custId, item);
            _loggerService.Value.Debug($"103:retlbl connectshphdr vhazFlag <#{vhazFlag}>");

            hazmatCount = _orderDbContext.GetHazmatCount(connection, orderId,shipId);

            if (vhazFlag && hazmatCount > 0)
            {
                vhazmat = FelY;
                _orderDbContext.UpdateConnectShipHdrHazmat(connection, orderId,shipId);
            }
            else if (vhazFlag)
            {
                vhazmat = FelY;
            }
            else
            {
                vhazmat = FelN;
            }
            return vhazmat;
        }
        public ConnectShipDtl GetConnectShipDetails(IDbConnection connection, ConnectShipOrderHdr connectShipOrderHdr)
        {
            var connectShipDetail = _mapper.Map<ConnectShipDtl>(connectShipOrderHdr);
            connectShipDetail.OrderId = connectShipOrderHdr.OrderId;
            connectShipDetail.ShipId = connectShipOrderHdr.ShipId;

            connectShipDetail.HarmonizedTariff = _orderDbContext.GetHdrPassValue(connection, connectShipOrderHdr.OrderId, connectShipOrderHdr.ShipId,
                connectShipOrderHdr.HarmonizedTariffPassThru);
            connectShipDetail.LicenseNum = _orderDbContext.GetHdrPassValue(connection, connectShipOrderHdr.OrderId, connectShipOrderHdr.ShipId,
                connectShipOrderHdr.LicenseNumPassThru);
            connectShipDetail.LicenseExpDate = _orderDbContext.GetHdrPassValue(connection, connectShipOrderHdr.OrderId, connectShipOrderHdr.ShipId,
                connectShipOrderHdr.LicenseExpDatePassThru);

            return connectShipDetail;
        }
        public void InitPackHeader(IDbConnection connection, ConnectShipPackLable connectShipPackLable,
            CustomerAux lstCustomerAux, ConnectShipOrderHdr connectShipOrderHdr, ref ConnectShipHeader connectShipHeader)
        {
            string vhazmat;
            _loggerService.Value.Debug("  150:InitPakHdr");
            connectShipHeader = _mapper.Map(connectShipPackLable, connectShipHeader);
            connectShipHeader.TaskId = connectShipPackLable.TaskId;
            connectShipHeader.Sscc = _orderDbContext.GetCshipBarCode(connection, 
                connectShipOrderHdr.CustId, "0", connectShipOrderHdr.BarCodePos_11_12);
            connectShipHeader.Seq = connectShipHeader.Seq + 1;
            connectShipHeader.SubtaskSeq = connectShipHeader.Seq;
            connectShipHeader.ContainerType = connectShipPackLable.ContainerType;

            if (connectShipHeader.IsTransmitToPackSizeYn)
            {
                var bounding = _orderDbContext.GetRightSizeDims(connection, connectShipPackLable.CpackJobId);
                if (bounding != null && bounding.RightSized)
                {
                    _loggerService.Value.Debug($"RIGHTSIZE:job_id(#{connectShipPackLable.CpackJobId}) length(#{bounding.Legnth}) width(#{bounding.Width}) height(#{bounding.Height})");
                   
                    connectShipHeader.Length = bounding.Legnth;
                    connectShipHeader.Width = bounding.Width;
                    connectShipHeader.Height = bounding.Height;
                }
                else
                {
                    _loggerService.Value.Debug($"UNABLE TO RIGHTSIZE: Cannot find a result set for job_id(#{connectShipPackLable.CpackJobId})");
                    
                    connectShipHeader.Length = connectShipPackLable.ContainerLength;
                    connectShipHeader.Width = connectShipPackLable.ContainerWidth;
                    connectShipHeader.Height = connectShipPackLable.ContainerHeight;
                }
            }

            connectShipHeader.Weight = connectShipPackLable.Weight + connectShipPackLable.ContainerWeight;
            connectShipHeader.BaseUomUnitWeight = connectShipHeader.BaseUomUnitWeight +
                                                  _orderDbContext.GetItemWeight(connection, connectShipOrderHdr.CustId, connectShipPackLable.Item, connectShipPackLable.Uom);
            connectShipHeader.DeclaredValue = connectShipPackLable.Qty * _orderDbContext.GetDetailPassValue(connection, connectShipOrderHdr.OrderId,
                                                  connectShipOrderHdr.ShipId, connectShipOrderHdr.DeclaredValuePassThru, connectShipPackLable.OrderItem, connectShipPackLable.OrderLot) /
                                              connectShipPackLable.QtyOrder;

            connectShipHeader.Hazmatind = _orderDbContext.GetItemHazmatIndicator(connection, connectShipOrderHdr.CustId, connectShipOrderHdr.OrderId,
                connectShipOrderHdr.ShipId);

            string iataPrimaryChemCode = lstCustomerAux.IsUseItemUncodeFromOrder
                ? connectShipPackLable.OrderDtlIataPrimaryChemCode
                : connectShipPackLable.CustItemIataPrimaryChemCode;


            connectShipHeader.BatteryCount = connectShipPackLable.CustItemBatteryCount * connectShipPackLable.PickQty;
            connectShipHeader.CellCount = connectShipPackLable.CustItemCellCount * connectShipPackLable.PickQty;
            connectShipHeader.LithiumWeight = connectShipPackLable.CustItemLithiumWeight * connectShipPackLable.PickQty;
            connectShipHeader.BatteryWattHrs = connectShipPackLable.CustItemBatteryWattHrs * connectShipPackLable.PickQty;

            vhazmat=Fillhazmat(connection, connectShipOrderHdr.OrderId, connectShipOrderHdr.ShipId, connectShipOrderHdr.CustId, connectShipPackLable.Item);

            var maxLength = _orderDbContext.GetCustLookupDtlMaxLength(connection, connectShipOrderHdr.CustId);
            int iataPrimaryChemCodeLength;
            iataPrimaryChemCodeLength = iataPrimaryChemCode.IsNullOrWhiteSpace() ? 0 : iataPrimaryChemCode.Length;
            connectShipHeader.IataPrimaryChemCode = iataPrimaryChemCodeLength > maxLength ?
                connectShipHeader.IataPrimaryChemCode = FelDangerousGoods : iataPrimaryChemCode;


            connectShipHeader.PrimaryHazardClass = connectShipPackLable.HazmatClass;
            connectShipHeader.Hazmat = vhazmat;
        }
        private void InitPackDetails(IDbConnection connection, ConnectShipPackLable pak, 
            ConnectShipOrderHdr connectShipOrderHdr, ref ConnectShipDtl connectShipDtl)
        {
            _loggerService.Value.Debug("  160:InitPakDtl");
            connectShipDtl = _mapper.Map(pak, connectShipDtl);
            connectShipDtl.UnitValue = pak.Qty * _orderDbContext.GetDetailPassValue(connection, connectShipOrderHdr.OrderId,
                                 connectShipOrderHdr.ShipId, connectShipOrderHdr.UnitValuePassThru, pak.OrderItem, pak.OrderLot) / pak.QtyOrder;
            connectShipDtl.BaseUomUnitWeight = _orderDbContext.GetItemWeight(connection, connectShipOrderHdr.CustId, pak.Item, pak.Uom);
            connectShipDtl.PickUomUnitWeight = _orderDbContext.GetItemWeight(connection, connectShipOrderHdr.CustId, pak.Item, pak.PickUom);
        }
        public void UpdatePackHeader(IDbConnection connection, ConnectShipPackLable connectShipPackLable, 
            CustomerAux lstCustomerAux, ConnectShipOrderHdr connectShipOrderHdr, ref ConnectShipHeader connectShipHeader)
        {
            string vhazmat;

            _loggerService.Value.Debug("  170:UpdPakHdr");
            string iataPrimaryChemCode;

            connectShipHeader.Weight = connectShipPackLable.Weight + connectShipPackLable.ContainerWeight;
            connectShipHeader.BaseUomUnitWeight = connectShipHeader.BaseUomUnitWeight +
                                     _orderDbContext.GetItemWeight(connection, connectShipOrderHdr.CustId, connectShipPackLable.Item, connectShipPackLable.Uom);
            connectShipHeader.DeclaredValue = connectShipHeader.DeclaredValue + connectShipPackLable.Qty * _orderDbContext.GetDetailPassValue(connection,
                                                  connectShipOrderHdr.OrderId,
                                                  connectShipOrderHdr.ShipId, connectShipOrderHdr.DeclaredValuePassThru, connectShipPackLable.OrderItem, connectShipPackLable.OrderLot) /
                                 connectShipPackLable.QtyOrder;

            if (!connectShipHeader.Item.Equals(connectShipPackLable.Item))
            {
                connectShipHeader.Item = null;
            }

            if (!connectShipHeader.Location.Equals(connectShipPackLable.FromLoc))
            {
                connectShipHeader.Location = FelMultipleLocation;
            }

            iataPrimaryChemCode = lstCustomerAux.IsUseItemUncodeFromOrder
                ? connectShipPackLable.OrderDtlIataPrimaryChemCode
                : connectShipPackLable.CustItemIataPrimaryChemCode;


            connectShipHeader.BatteryCount = connectShipHeader.BatteryCount + connectShipPackLable.CustItemBatteryCount * connectShipPackLable.PickQty;
            connectShipHeader.CellCount = connectShipHeader.CellCount + connectShipPackLable.CustItemCellCount * connectShipPackLable.PickQty;
            connectShipHeader.LithiumWeight = connectShipHeader.LithiumWeight + connectShipPackLable.CustItemLithiumWeight * connectShipPackLable.PickQty;
            connectShipHeader.BatteryWattHrs = connectShipHeader.BatteryWattHrs + connectShipPackLable.CustItemBatteryWattHrs * connectShipPackLable.PickQty;

            vhazmat =Fillhazmat(connection, connectShipOrderHdr.OrderId, connectShipOrderHdr.ShipId, connectShipOrderHdr.CustId, connectShipPackLable.Item);

            var maxLength = _orderDbContext.GetCustLookupDtlMaxLength(connection, connectShipOrderHdr.CustId);

            if (connectShipHeader.IataPrimaryChemCode.IsNullOrWhiteSpace() && iataPrimaryChemCode.IsNullOrWhiteSpace())
            {
                connectShipHeader.IataPrimaryChemCode =
                    (iataPrimaryChemCode.Length > maxLength) ? FelDangerousGoods : iataPrimaryChemCode;

            }
            else
            {
                if (!connectShipPackLable.CustItemIataPrimaryChemCode.IsNullOrWhiteSpace() &&
                    !connectShipHeader.IataPrimaryChemCode.Contains(iataPrimaryChemCode))
                {
                    if (connectShipHeader.IataPrimaryChemCode.Length + iataPrimaryChemCode.Length + 1 <= maxLength)
                    {
                        connectShipHeader.IataPrimaryChemCode = connectShipHeader.IataPrimaryChemCode + "," + iataPrimaryChemCode;
                    }
                }
            }


            connectShipHeader.PrimaryHazardClass = connectShipPackLable.HazmatClass;
            connectShipHeader.Hazmat = vhazmat;
        }

        public void UpdatePackDetail(IDbConnection connection, ConnectShipPackLable pak,
            ConnectShipOrderHdr connectShipOrderHdr, ref ConnectShipDtl connectShipDtl)
        {
            _loggerService.Value.Debug("  180:UpdPakHdr");
            connectShipDtl.UnitValue = connectShipDtl.UnitValue + pak.Qty * _orderDbContext.GetDetailPassValue(connection, connectShipOrderHdr.OrderId,
                                           connectShipOrderHdr.ShipId, connectShipOrderHdr.UnitValuePassThru, pak.OrderItem, pak.OrderLot) / pak.QtyOrder;
            connectShipDtl.BaseQuantity = connectShipDtl.BaseQuantity * pak.Qty;
            var dtlPickUom = connectShipDtl.PickUom.IsNullOrWhiteSpace() ? FelSx : connectShipDtl.PickUom;
            var pakPickUom = pak.PickUom.IsNullOrWhiteSpace() ? FelSy : pak.PickUom;

            if (dtlPickUom.Equals(pakPickUom))
            {
                connectShipDtl.PickQuantity = connectShipDtl.PickQuantity + pak.PickQty;
            }
            else
            {
                connectShipDtl.PickQuantity = null;
                connectShipDtl.PickUom = null;
            }
        }

        public void TransmitConnectShipForSingle(IDbConnection connection, Wave wave, BatchTask batchTask)
        {
            var connectShipOrderHdr = _orderDbContext.GetConnectShipOrderHdr(connection, batchTask.OrderId, batchTask.ShipId);

            var isInternational = !connectShipOrderHdr.ShipToCountryCode.ToUpper().In("US", "USA");
            var connectShipHdr = GetConnectShipHeader(connection, wave, connectShipOrderHdr, new ConnectShipSubTaskQtyDetails
                                                        {
                                                            PickQuantity = 1
                                                        });
            var connectShipDetails = GetConnectShipDetails(connection, connectShipOrderHdr);
            var lstCustomerAux = _customerAuxDbContext.GetCustomerSettings(connection, connectShipOrderHdr.CustId).ToList()
                .FirstOrDefault();
            var lstLbl = _orderDbContext.GetConnectShipLableForSingles(connection, connectShipOrderHdr.OrderId, connectShipOrderHdr.ShipId,batchTask.TaskId);
            //TODO: Check if this is loop
            foreach (var lbl in lstLbl)
            {
                connectShipHdr.SubtaskSeq = connectShipHdr.Seq + lbl.PickQty;

                for (var i = 0; i < lbl.PickQty; i++)
                {
                    InsertConnectShip(connection, ref connectShipHdr, ref connectShipDetails, connectShipOrderHdr, lbl, lstCustomerAux,
                        isInternational);
                }

                _orderDbContext.UpdateSubTasksConnectShipSeq(connection, lbl.StRowId, connectShipHdr.SubtaskSeq);
                _orderDbContext.UpdateTasksToUserId(connection, lbl.TaskId);
            }

            var cnt = 0;

            _loggerService.Value.Debug($"140:for pak in c_pak(#{connectShipOrderHdr.OrderId},#{connectShipOrderHdr.ShipId})");
            var lstPack = _orderDbContext.GetConnectShipPackForSingles(connection, connectShipOrderHdr.OrderId, connectShipOrderHdr.ShipId,batchTask.TaskId);
            long taskId = -1;
            var cartonType = FelNone;
            var cartonSeq = -1;
            var item = FelNone;
            foreach (var pak in lstPack)
            {
                if (pak.TaskId != taskId || pak.CartonType != cartonType || pak.Cartonseq != cartonSeq ||
                    pak.Item !=
                    item)
                {
                    if (cnt == 0)
                    {
                        taskId = pak.TaskId;
                        connectShipHdr.LpId = _orderDbContext.GetNextLicensePlate(connection);
                        _loggerService.Value.Debug($"  101:Get next retlbl lpid #{connectShipHdr.LpId}");
                        if (connectShipHdr.LpId.IsNullOrWhiteSpace())
                        {
                            _loggerService.Value.Debug("zrf.get_next_lpid error");
                            throw new Exception("zrf.get_next_lpid error");
                        }

                        InitPackHeader(connection, pak, lstCustomerAux, connectShipOrderHdr, ref connectShipHdr);
                        cartonType = pak.CartonType;
                        cartonSeq = pak.Cartonseq;

                        InitPackDetails(connection, pak,connectShipOrderHdr, ref connectShipDetails);
                        item = pak.Item;
                    }
                    else if (pak.TaskId.Equals(taskId) && pak.CartonType.Equals(cartonType) &&
                             pak.Cartonseq.Equals(cartonSeq))
                    {
                        UpdatePackHeader(connection, pak, lstCustomerAux, connectShipOrderHdr, ref connectShipHdr);
                        connectShipDetails.HarmonizedCode = pak.HarmonizedCode;
                        connectShipDetails.HazmatDescription = pak.HazmatDescription;
                        connectShipDetails.HazmatClass = pak.HazmatClass;
                        connectShipDetails.HazmatId = pak.HazmatId;
                        connectShipDetails.HazmatTechnicalName = pak.HazmatTechnicalName;
                        connectShipDetails.HazmatPackingGroup = pak.HazmatPackingGroup;
                        connectShipDetails.HazmatAbbrev = pak.HazmatAbbrev;

                        if (isInternational)
                        {
                            _orderDbContext.InsertConnectShipDtl(connection, connectShipDetails);
                        }

                        InitPackDetails(connection, pak, connectShipOrderHdr, ref connectShipDetails);
                        item = pak.Item;
                    }
                    else
                    {
                        taskId = pak.TaskId;
                        _loggerService.Value.Debug("190:INSERT pak into connectshiphdr");
                        _orderDbContext.InsertConnectShipHdr(connection, connectShipHdr);

                        WriteParcel(connection, connectShipHdr);
                        InitPackHeader(connection, pak, lstCustomerAux, connectShipOrderHdr, ref connectShipHdr);
                        connectShipHdr.LpId = _orderDbContext.GetNextLicensePlate(connection);
                        _loggerService.Value.Debug($"  101:Get next retlbl lpid #{connectShipHdr.LpId}");
                        if (connectShipHdr.LpId.IsNullOrWhiteSpace())
                        {
                            _loggerService.Value.Debug("zrf.get_next_lpid error");
                            throw new Exception("zrf.get_next_lpid error");
                        }

                        cartonType = pak.CartonType;
                        cartonSeq = pak.Cartonseq;

                        connectShipDetails.HarmonizedCode = pak.HarmonizedCode;
                        connectShipDetails.HazmatDescription = pak.HazmatDescription;
                        connectShipDetails.HazmatClass = pak.HazmatClass;
                        connectShipDetails.HazmatId = pak.HazmatId;
                        connectShipDetails.HazmatTechnicalName = pak.HazmatTechnicalName;
                        connectShipDetails.HazmatPackingGroup = pak.HazmatPackingGroup;
                        connectShipDetails.HazmatAbbrev = pak.HazmatAbbrev;
                        if (isInternational)
                        {
                            _orderDbContext.InsertConnectShipDtl(connection, connectShipDetails);
                        }

                        InitPackDetails(connection, pak, connectShipOrderHdr, ref connectShipDetails);
                        item = pak.Item;
                    }
                }
                else
                {
                    UpdatePackHeader(connection, pak, lstCustomerAux, connectShipOrderHdr, ref connectShipHdr);
                    UpdatePackDetail(connection, pak, connectShipOrderHdr, ref connectShipDetails);
                }

                cnt++;

                _orderDbContext.UpdateSubTasksConnectShipSeq(connection, pak.StRowId, connectShipHdr.SubtaskSeq);
                _orderDbContext.UpdateTasksToUserId(connection, pak.TaskId);
            }

            if (cnt <= 0)
            {
                return;
            }

            _orderDbContext.InsertConnectShipHdr(connection, connectShipHdr);

            if (isInternational)
            {
                _orderDbContext.InsertConnectShipDtl(connection, connectShipDetails);
            }

            WriteParcel(connection, connectShipHdr);
        }
    }
}
