﻿// <copyright file="ItemAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-18</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-22</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using Newtonsoft.Json.Linq;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Aggregates
{
    public class ItemAggregate : IItemAggregate
    {

        #region Constructor(S)

        public ItemAggregate(IItemAggregateInitializer itemDomainInitializer)
        {
            _orderDbContext = itemDomainInitializer.OrderDbContext;
            _taskAggregate = itemDomainInitializer.TaskAggregate;
            _custItemDbContext = itemDomainInitializer.CustItemDbContext;
            _customerDbContext = itemDomainInitializer.CustomerDbContext;
            _itemDemandDbContext = itemDomainInitializer.ItemDemandDbContext;
            _mapper = itemDomainInitializer.MapperEngine.CreateMapper();
            _findAPickAggregate = itemDomainInitializer.FindAPickAggregate;

            #region Conditional execution stacks

            _commitmentFuncStack = new Dictionary<string, Func<IDbConnection, Commitment, IEnumerable<Commitment>>>
            {
                {Constants.Flag.Yes, itemDomainInitializer.CommitmentDbContext.GetVendorTrackedCommitments},
                {Constants.Flag.No, itemDomainInitializer.CommitmentDbContext.GetNonVendorTrackedCommitments}
            };

            #endregion
        }

        #endregion

        #region Private Member(S)

        private readonly IOrderDbContext _orderDbContext;
        private readonly ITaskAggregate _taskAggregate;
        private readonly IBatchTaskAggregate _batchTaskAggregate;
        private readonly ICustItemDbContext _custItemDbContext;
        private readonly ICustomerDbContext _customerDbContext;
        private readonly IMapper _mapper;
        private readonly IItemDemandDbContext _itemDemandDbContext;
        private readonly Lazy<IFindAPickAggregate> _findAPickAggregate;

        private readonly Dictionary<string, Func<IDbConnection, Commitment, IEnumerable<Commitment>>>
            _commitmentFuncStack;

        #endregion

        #region Public Member(S)

        private ICommand WaveRequest { get; set; }

        #endregion

        #region Public Method(S)

        public List<PickTaskRequest> ExecuteReleasLine(IDbConnection connection, OrderTo order, ICommand request)
        {
           return  ReleaseLine(connection, order, request.PickType, request);
        }

        public List<PickTaskRequest> ExecuteSingleReleasLine(IDbConnection connection, OrderTo order, ICommand request)
        {
            return ReleaseSingleLine(connection, order, request.PickType, request);
        }



        #endregion

        #region Private Method(S)
        private List<PickTaskRequest> ReleaseSingleLine(IDbConnection connection, OrderTo order, string requestPickType, ICommand request)
        {
            var orderDetail = order.OrderDetail;
            var response = new List<PickTaskRequest>();

            var customer = _customerDbContext.GetCustomerPickByLine(connection, order.HeaderInfo.CustomerId)
                .FirstOrDefault();

            #region Qty remaining recalculation after commit line

            var remainingQty = _taskAggregate.CalculateQtyRemain(connection, orderDetail);

            #endregion

            if (remainingQty <= 0)
            {
                return response;
            }

            var lstcommitment = GetCommitmentByVendorTracked(connection, orderDetail, order.HeaderInfo);
            foreach (var commitment in lstcommitment)
            {
                var baseQty = order.Quantity;
                    //_taskAggregate.CalculateQtyForTaskGeneration(connection, orderDetail, order.HeaderInfo, commitment);
                var orderQty = orderDetail.QtyOrder;
                var custItem = _custItemDbContext.GetCustItemView(connection,
                        orderDetail.CustomerId,
                        commitment.Item)
                    .FirstOrDefault();
                var pickTaskRequest = new PickTaskRequest()
                {
                    BaseQty = baseQty,
                    OrderQty = orderQty,
                    Order = order,
                    Customer = customer,
                    Commitment = commitment,
                    CustItem = custItem,
                    Command = request
                };
    
              //  if (!requestPickType.Equals(Constants.PickType.BatchSingles) || pickTaskRequest.BaseQty <= 0) continue;
                pickTaskRequest.Item = commitment.Item;
                response.Add(pickTaskRequest);
            }

            return response;
        }

        private List<PickTaskRequest> ReleaseLine(IDbConnection connection, OrderTo order, string pickType, ICommand request)
        {
            var orderDetail = order.OrderDetail;
            var response= new List<PickTaskRequest>();
                        
            var customer = _customerDbContext.GetCustomerPickByLine(connection, order.HeaderInfo.CustomerId)
                .FirstOrDefault();
            
            #region Qty remaining recalculation after commit line

            var remainingQty = _taskAggregate.CalculateQtyRemain(connection, orderDetail);

            #endregion

            if (remainingQty <= 0)
            {
                return response;
            }

            var lstcommitment = GetCommitmentByVendorTracked(connection, orderDetail, order.HeaderInfo);
            foreach (var commitment in lstcommitment)
            {
                var baseQty =
                    _taskAggregate.CalculateQtyForTaskGeneration(connection, orderDetail, order.HeaderInfo, commitment);
                var orderQty = orderDetail.QtyOrder;
                var custItem = _custItemDbContext.GetCustItemView(connection,
                        orderDetail.CustomerId,
                        commitment.Item)
                    .FirstOrDefault();
                var pickTaskRequest = new PickTaskRequest()
                {
                    BaseQty = baseQty,
                    OrderQty= orderQty,
                    Order = order,
                    Customer=customer,
                    Commitment=commitment,
                    CustItem=custItem,
                    Command= request
                };
                _findAPickAggregate.Value.ExecuteFullPick(connection, pickTaskRequest);

                if (!(pickType.Equals(Constants.PickType.Batch) ||
                      pickType.Equals(Constants.PickType.BatchSingles)) || pickTaskRequest.BaseQty <= 0) continue;
                pickTaskRequest.Item = commitment.Item;
                response.Add(pickTaskRequest);
            }

            CreateItemDemandForShortage(connection, orderDetail, order.HeaderInfo, request.UserId);
            return response;
        }
       

        public List<Commitment> GetCommitmentByVendorTracked(IDbConnection connection, OrderDetail orderDetail,
            OrderHeader orderHeader)
        {
            var commitmentRequest = _mapper.Map<Commitment>(orderDetail);
            var vendorFlag =
                _custItemDbContext.GetVendorTrackedFlag(connection, orderHeader.CustomerId,
                    orderDetail.Item);
            var lstcommitment = _commitmentFuncStack[vendorFlag](connection, commitmentRequest).ToList();

            return !lstcommitment.Any() ? default(List<Commitment>) : lstcommitment.ToList();
        }
        

        private void CreateItemDemandForShortage(IDbConnection connection, OrderDetail orderDetail,
            OrderHeader orderHeader, string userId)
        {
            if (!string.IsNullOrWhiteSpace(orderHeader.ComponentTemplate))
                return;
            var qtyDemand = orderDetail.QtyOrder - orderDetail.QtyPick - orderDetail.QtyCommit;
            if (qtyDemand <= 0)
                return;
            
            if (_custItemDbContext.GetCustItemIsKitFlag(connection, orderHeader.CustomerId, orderDetail.Item))
                return;

            _itemDemandDbContext.Insert(connection,
                PopulateItemDemand(orderDetail, orderHeader, orderDetail.Item, qtyDemand, userId));

            var substitutes =
                _custItemDbContext.GetCustItemSubstituteByItem(connection, orderHeader.CustomerId, orderDetail.Item)
                    .ToList();
            substitutes.ForEach(s =>
            {
                _itemDemandDbContext.Insert(connection,
                    PopulateItemDemand(orderDetail, orderHeader, s.ItemSub, qtyDemand, userId));
            });
        }

        private static ItemDemand PopulateItemDemand(OrderDetail orderDetail, OrderHeader orderHeader, string itemSub,
            int qtyDemand, string userId)
        {
            var demand = new ItemDemand()
            {
                Facility = orderHeader.FromFacility,
                Item = itemSub,
                LotNumber = orderDetail.LotNumber,
                Priority = orderHeader.Priority,
                InvStatusInd = orderDetail.InvStatusInd,
                InvClassInd = orderDetail.InvClassInd,
                InvStatus = orderDetail.InvStatus,
                InventoryClass = orderDetail.InventoryClass,
                DemandType = "O",
                OrderId = orderHeader.OrderId,
                ShipId = orderHeader.ShipId,
                LoadNo = orderHeader.LoadNo,
                StopNo = orderHeader.StopNo,
                ShipNo = orderHeader.ShipNo,
                OrderItem = orderDetail.Item,
                OrderLot = orderDetail.LotNumber,
                Qty = qtyDemand,
                LastUser = userId,
                CustId = orderHeader.CustomerId
            };
            return demand;
        }

        #endregion
    }
}