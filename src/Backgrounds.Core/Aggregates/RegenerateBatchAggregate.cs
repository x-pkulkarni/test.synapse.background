﻿// <copyright file="RegenerateBatchAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-08-06</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-23</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Newtonsoft.Json.Linq;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Aggregates.Validation;
using Synapse.Backgrounds.Core.Aggregates.Validation.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Aggregates
{
    public class RegenerateBatchAggregate :IRegenerateBatchAggregate
    {

        #region Constructor(S)

        public RegenerateBatchAggregate(IRegenerateBatchAggregateInitializer regenerateBatchAggregateInitializer)
        {
            _validator = regenerateBatchAggregateInitializer.Validator;
            _batchTaskDbContext = regenerateBatchAggregateInitializer.BatchTaskDbContext;
            _plateDbContext = regenerateBatchAggregateInitializer.PlateDbContext;
            _subTaskDbContext = regenerateBatchAggregateInitializer.SubTaskDbContext;
            _taskDbContext = regenerateBatchAggregateInitializer.TaskDbContext;
            _waveDbContext = regenerateBatchAggregateInitializer.WaveDbContext;
            _orderAggregate = regenerateBatchAggregateInitializer.OrderAggregate;
            _itemAggregate = regenerateBatchAggregateInitializer.ItemAggregate;
            _customerDbContext = regenerateBatchAggregateInitializer.CustomerDbContext;
            _custItemDbContext = regenerateBatchAggregateInitializer.CustItemDbContext;
            _custItemAggregate = regenerateBatchAggregateInitializer.CustItemAggregate;
            _cartonService = regenerateBatchAggregateInitializer.CartonService;
            _waveAggregate = regenerateBatchAggregateInitializer.WaveAggregate;
        }
        #endregion


        #region Private Member(S)
        private readonly IValidator<OrderValidator> _validator;
        private readonly IBatchTaskDbContext _batchTaskDbContext;
        private readonly IPlateDbContext _plateDbContext;
        private readonly ISubTaskDbContext _subTaskDbContext;
        private readonly ITaskDbContext _taskDbContext;
        private readonly IWaveDbContext _waveDbContext;
        private readonly IOrderAggregate _orderAggregate;
        private readonly IItemAggregate _itemAggregate;
        private readonly ICustItemDbContext _custItemDbContext;
        private readonly ICustomerDbContext _customerDbContext;
        private readonly ICustItemAggregate _custItemAggregate;
        private readonly ICartonService _cartonService;
        private ValidationContext _validationRequest;
        private readonly IWaveAggregate _waveAggregate;
        #endregion


        #region Public Method(S)

        public void ExecuteRegenerateBatchTask(IDbConnection connection, ICommand request)
        {
            _validationRequest = new ValidationContext { Command = request };
            if (!_validator.IsValid(connection, _validationRequest)) return;
            var subtasks = _subTaskDbContext.GetLpIdByTaskId(connection, request.TaskId, request.Facility);

            foreach (var subtask in subtasks)
            {
                //LpId is null, go to next item
                if (string.IsNullOrEmpty(subtask.LpId)) continue;

                var updateQtyTaskedRequest = new UpdateQtyTaskedRequest()
                {
                    Facility = request.Facility,
                    LpId = subtask.LpId,
                    QtyAsked = subtask.Quantity
                };
                //Decrement taskedqty by qty
                _plateDbContext.UpdateQtyTaskedByBatchLpId(connection, updateQtyTaskedRequest);
            }
            
            
            GenerateTasks(connection, request);
        }

        #endregion


        #region Private Method(S)

        private void CleanUpTasks(IDbConnection connection, ICommand request)
        {
            _subTaskDbContext.DeleteByTaskIdAndFacility(connection, request.Facility, request.TaskId);
            _taskDbContext.DeleteByIdAndFacility(connection, request.TaskId, request.Facility);
            //_batchTaskDbContext.ResetBatchTasksByTaskId(connection, request.TaskId, request.Facility);
        }

        private void GenerateTasks(IDbConnection connection, ICommand request)
        {
            var wave = _waveDbContext.GetWaveById(connection, request.Wave).First();
            var batchTasks = _batchTaskDbContext.GetBatchTasksByTaskId(connection, request.TaskId, request.Facility);
            _batchTaskDbContext.DeleteBatchTasksByTaskId(connection, request.TaskId, request.Facility);

            var pickTaskRequests = new List<PickTaskRequest>();
            var isRegenRequired = false;

            foreach (var batch in batchTasks)
            {
                var orderDetails = _orderAggregate
                    .GetOrderDetailsByItem(connection, batch.OrderId, batch.ShipId, batch.Item).First();
                var orderHeader = _orderAggregate.GetOrderHeaderByOrderId(connection, batch.OrderId, batch.ShipId)
                    .First();
                
                //Skip cancelled or shipped orders
                if (orderHeader.OrderStatus.Contains('X') || orderHeader.OrderStatus.Contains('9'))
                {
                    isRegenRequired = true;
                    continue;
                }
               
                var order = new OrderTo { OrderDetail = orderDetails, HeaderInfo = orderHeader, WaveInfo = wave };

                var customer = _customerDbContext.GetCustomerPickByLine(connection, order.HeaderInfo.CustomerId)
                .FirstOrDefault();
                var commitment = _itemAggregate.GetCommitmentByVendorTracked(connection, orderDetails, orderHeader).First();
                var custItem = _custItemDbContext.GetCustItemView(connection,
                        orderDetails.CustomerId,
                        commitment.Item)
                    .FirstOrDefault();
                custItem.Item = order.OrderDetail.Item;
                var taskRequest = new PickTaskRequest()
                {
                    BaseQty = batch.Quantity,
                    OrderQty = orderDetails.QtyOrder,
                    Order = order,
                    Customer = customer,
                    Commitment = commitment,
                    CustItem = custItem,
                    Command = request
                };
                taskRequest.Item = order.OrderDetail.Item;
                pickTaskRequests.Add(taskRequest);
            }
            // if there are no cancelled or shipped orders skip regenerate.
            if (!isRegenRequired)
            {
                return;
            }
            CleanUpTasks(connection, request);

            var singlePickOrders = new List<OrderTo>();

            _orderAggregate.GeneratePickTasks(connection, pickTaskRequests, wave, request, singlePickOrders);
            var isFel = _waveAggregate.GetFelType(wave) > 0;
            _orderAggregate.GeneratePickTasksForSingles(connection, singlePickOrders, wave, request, isFel);
        }
        #endregion
    }
}