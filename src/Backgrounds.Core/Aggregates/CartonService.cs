﻿// <copyright file="CartonService.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Kathiresan, Murugan</author> 
// <createddate>2019-01-30</createddate>

namespace Synapse.Backgrounds.Core.Aggregates
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Configuration;
    using Data;
    using Data.Interface;
    using Interface;
    using Model;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public class CartonService: ICartonService
    {
        private readonly Lazy<IConfigService> _configService;
        private readonly ICartonizeDbContext _cartonizeDbContext;

       public CartonService(Lazy<IConfigService> configService,ICartonizeDbContext cartonizeDbContext)
        {
            _configService = configService;
            _cartonizeDbContext = cartonizeDbContext;
        }

        public async Task<string> PackOrder(PackingRequest packingRequest, string cartonizationApiEndpoint)
        {
            using (var client = new HttpClient())
            {
                var content = new StringContent(JsonConvert.SerializeObject(packingRequest), Encoding.UTF8, "application/json");
                var response = await client.PostAsync(cartonizationApiEndpoint, content);
                var responseJson = await response.Content.ReadAsStringAsync();
                return responseJson;
            }
        }

        public IEnumerable<Item> GetItemDetails(IDbConnection connection, string custid, long orderid)
        {
            return _cartonizeDbContext.GetItemDetails(connection, custid, orderid);
        }

        public IEnumerable<Item> GetSubTaskItemDetails(IDbConnection connection, string taskType, long orderid)
        {
            return _cartonizeDbContext.GetSubTaskItemDetails(connection, taskType, orderid);
        }

        public IEnumerable<Container> GetCartonDetails(IDbConnection connection, string custid, long orderid, string cartonGroup)
        {
            return _cartonizeDbContext.GetCartonDetails(connection, custid, orderid, cartonGroup).GroupBy(x => x.ContainerDescription).Select(g => g.FirstOrDefault()).ToList();
        }

        public List<KeyValuePair<string, List<Item>>> GetPackedItemsList(JArray containerPackingResults)
        {
            var packedContainerItemMapping = new List<KeyValuePair<string, List<Item>>>();
            
            foreach (var containerPackingResult in containerPackingResults)
            {
                var packedItemsList = new List<Item>();
                var containerType = (string)containerPackingResult["containerDescription"];
                var packedItems = (JArray)containerPackingResult["algorithmPackingResult"]["packedItems"];
                foreach (var item in packedItems)
                {
                    var itemID = (string)item["itemID"];
                    var itemDesc = (string)item["itemDescription"];
                    var itemPackingGroup = (string)item["itemPackingGroup"];
                    var itemUom = (string)item["itemUom"];
                    var itemPacked = new Item
                    {
                        ItemID = new Guid(itemID),
                        ItemDescription = itemDesc,
                        ItemQuantity = 1,
                        ItemPackingGroup = itemPackingGroup,
                        ItemUom = itemUom
                    };
                    if (packedItemsList.Exists(x => x.ItemDescription == itemPacked.ItemDescription && x.ItemUom == itemPacked.ItemUom))
                    {
                        var updateItem = packedItemsList.Find(x => x.ItemDescription == itemPacked.ItemDescription);
                        updateItem.ItemQuantity += 1;
                    }
                    else
                    {
                        packedItemsList.Add(itemPacked);
                    }

                }
                packedContainerItemMapping.Add(new KeyValuePair<string, List<Item>>(containerType, packedItemsList));

            }

            return packedContainerItemMapping;
        }
    }
}
