﻿// <copyright file="ISubTaskAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-14</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-14</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    public interface ISubTaskAggregate
    {
        void ResetTasksForUnrelease(IDbConnection connection, OrderUnreleaseRequest request);

        bool CreateSubTaskForBatchPick(IDbConnection connection, BatchTask batchTask, ICommand waveReleaseRequest,
            Location fromLocation, Location toLocation, Plate plate, Wave waveInfo, Task task);

        bool CreateSubTask(IDbConnection connection, SubTask request);
        bool DeleteByRowId(IDbConnection connection, string rowId);
        bool CreateSubTaskForFullPick(IDbConnection connection, OrderTo order, Commitment commitment,
            Location fromLocation, Location toLocation, FindAPickResponse pickResponse, Customer customer,
            ICommand commandRequest, string locId,
            Task task, string taskType, string fromLpId);

        List<SubTask> GetSubTasksByTaskTypeAndOrder(IDbConnection connection, long wave, string taskType,
            long orderId, int shipId);

        bool UpdateTaskIdByRowId(IDbConnection connection, string subTaskRecordId, long taskId, string lastUser);
        
        void CreateSubTaskForCarton(IDbConnection connection, SubTask findsubtask, Item item,
            OrderHeader orderHeader, Commitment commitment, ICommand commandRequest, 
            string cartonType, int sequenceCounter);

        int GetSubTaskCountByTaskId(IDbConnection connection, long taskId);

        IEnumerable<SubTask> GetSubTasksByWaveId(IDbConnection connection, long wave);
    }
}