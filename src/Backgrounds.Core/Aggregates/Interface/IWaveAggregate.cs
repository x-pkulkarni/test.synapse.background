﻿// <copyright file="IWaveDomain.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using System.Data;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Integration;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    using Model;
    using Task = System.Threading.Tasks.Task;

    public interface IWaveAggregate
    {
        Task ExecuteReleaseWave(IDbConnection connection, ICommand releaseCommand);

        Task ExecuteUnreleaseWave(IDbConnection connection, ICommand unreleaseCommand);

        Task ExecuteResetWave(IDbConnection connection, long wave);
        int GetFelType(Wave wave);
    }
}