﻿// <copyright file="ITaskService.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-14</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-14</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    public interface ITaskService
    {
        long GenerateNewTaskId(IDbConnection connection);

        IEnumerable<Task> GetByTypeAndOrder(IDbConnection connection, string tasktype, long wave, long orderId,
            int shipId);

        int GetPassedTasksCountById(IDbConnection connection, long taskId);

        bool Delete(IDbConnection connection, long taskId);

        bool DeleteByOrderAndType(IDbConnection connection, long orderId, int shipId, string taskType);

        bool UpdateTaskQuatity(IDbConnection connection, long taskId, int quantity);
        bool UpdateTaskQuantityAndUom(IDbConnection connection, Task task);
        long GetNextTaskId(IDbConnection connection);

        Task CreateTaskForFullPick(IDbConnection connection,
            Tuple<OrderTo, Commitment, ICommand, Location, Location, FindAPickResponse> source, string plateFromLoc);

        Task CreateTaskFromSubTask(IDbConnection connection, SubTask subTask, string lastUser);
        long CreateTaskForBatchPick(IDbConnection connection,
            Tuple<BatchTask, Plate, ICommand, Location, Location, Wave> source);

        bool UpdateTask(IDbConnection connection, Task task);

        Zone GetTaskGroupingRulesByZone(IDbConnection connection, string zoneId, string facility);
    }
}