﻿// <copyright file="IBatchTaskAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-14</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-14</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    public interface IBatchTaskAggregate
    {
        bool GenerateBatchTasks(IDbConnection connection, Wave wave, ICommand commandRequest, List<BatchTask> batchTasks);

        bool GenerateBatchTasksForSingles(IDbConnection connection, Wave wave, ICommand commandRequest, List<BatchTask> batchTasks);
        int GetPendingBatchVendorItem(IDbConnection connection, PendingPickQtyRequest pendingBatchPickQtyRequest);

        int GetPendingBatchItem(IDbConnection connection, PendingPickQtyRequest pendingBatchPickQtyRequest);
        
        bool DeleteByTaskIdAndLpId(IDbConnection connection, SubTask task);

        bool CreateBatchTask(IDbConnection connection, BatchTask task);

        BatchTask CreateBatchTaskObject(IDbConnection connection, OrderTo order, Commitment commitment,
            Location fromLocation, Location toLocation, FindAPickResponse pickResponse, ICommand commandRequest);

        int GetPendingBatchPicksLine(IDbConnection connection, PendingPickQtyRequest pendingBatchPickQtyRequest);
    }
}