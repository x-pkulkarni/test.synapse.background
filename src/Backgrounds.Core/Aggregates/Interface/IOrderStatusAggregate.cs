﻿// <copyright file="IOrderStatusAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-15</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-15</lastchangeddate>

using System.Data;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    public interface IOrderStatusAggregate
    {
        void UpdateOrderAndPlateInfo(IDbConnection connection, OrderUnreleaseRequest request, long? currentWave,
            long? waveToUnrelease);

        void UpdateOrderHeaderStatusToReleased(IDbConnection connection, OrderHeader orderHeader,
            ICommand waveReleaseRequest);

        void UpdateLoadStatus(IDbConnection connection, int? loadNo, string userId, int? stopNo);

        void AddOrderHistory(IDbConnection connection, OrderHeader order, string userId, string requestType);
    }
}