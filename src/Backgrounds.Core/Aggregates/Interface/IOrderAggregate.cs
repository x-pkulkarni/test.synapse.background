﻿// <copyright file="IOrderDomain.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-18</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-23</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    public interface IOrderAggregate
    {
        void ExecuteReleaseOrder(IDbConnection connection, Wave wave, ICommand request);

        void ExecuteUnreleaseOrder(IDbConnection connection, Wave wave, ICommand request);

        void GeneratePickTasks(IDbConnection connection, List<PickTaskRequest> pickTaskRequests, Wave wave,
            ICommand request, List<OrderTo> singlePickOrders);

        IEnumerable<OrderDetail> GetOrderDetailsByItem(IDbConnection connection, long orderId, int shipId, string item);
        IEnumerable<OrderHeader> GetOrderHeaderByOrderId(IDbConnection connection, long orderId, int shipId);
        void ExecuteTransmitCshipOrder(IDbConnection connection, Wave wave, int felType);
        void GeneratePickTasksForSingles(IDbConnection connection, List<OrderTo> singlePicks, Wave wave,
            ICommand request,bool isFel);
        
    }
}