﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    using System.Data;
    using Model;
    using Newtonsoft.Json.Linq;

    public interface ICartonService
    {
        Task<string> PackOrder(PackingRequest packingRequest, string cartonizationApiEndpoint);
        IEnumerable<Item> GetItemDetails(IDbConnection connection, string custid, long orderid);
        IEnumerable<Item> GetSubTaskItemDetails(IDbConnection connection, string taskType, long orderid);
        IEnumerable<Container> GetCartonDetails(IDbConnection connection, string custid, long orderid, string cartonGroup);
        List<KeyValuePair<string, List<Item>>> GetPackedItemsList(JArray containerPackingResults);
    }
}
