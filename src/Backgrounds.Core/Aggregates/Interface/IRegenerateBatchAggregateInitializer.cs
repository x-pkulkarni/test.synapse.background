﻿// <copyright file="IRegenerateBatchAggregateInitializer.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-08-06</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-06</lastchangeddate>

using Synapse.Backgrounds.Core.Aggregates.Validation;
using Synapse.Backgrounds.Core.Aggregates.Validation.Interface;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    public interface IRegenerateBatchAggregateInitializer
    {
        IValidator<OrderValidator> Validator { get; }
        IBatchTaskDbContext BatchTaskDbContext { get; }
        IPlateDbContext PlateDbContext { get; }
        ISubTaskDbContext SubTaskDbContext { get; }
        ITaskDbContext TaskDbContext { get; }
        IWaveDbContext WaveDbContext { get; }
        IOrderAggregate OrderAggregate { get; }
        IItemAggregate ItemAggregate { get; }
        IBatchTaskAggregate BatchTaskAggregate { get; }
        ICustomerDbContext CustomerDbContext { get; }
        ICustItemDbContext CustItemDbContext { get; }
        ICustItemAggregate CustItemAggregate { get; }
        ICartonService CartonService { get; }
        IWaveAggregate WaveAggregate { get; }
    }
}