﻿// <copyright file="IOrderDomainInitializer.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-02-08</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-15</lastchangeddate>

using System;
using Synapse.Backgrounds.Core.Aggregates.Validation;
using Synapse.Backgrounds.Core.Aggregates.Validation.Interface;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    using Infrastructure.Logging.Interface;
    public interface IOrderAggregateInitializer
    {
        IValidator<OrderValidator> Validator { get; }
        IOrderDbContext OrderDbContext { get; }
        Lazy<IItemAggregate> ItemDomain { get; }
        IBatchTaskAggregate BatchTaskAggregate { get; }
        ISubTaskAggregate SubTaskAggregate { get; }
        ICustItemAggregate CustItemAggregate { get; }
        ITaskService TaskService { get; }
        ICartonService CartonService { get; }
        IFindAPickAggregate FindAPickAggregate { get; }
        IPlateAggregate PlateAggregate { get; }
        Lazy<IOrderStatusAggregate> OrderStatusAggregate { get; }
        ICustomerAuxDbContext CustomerAuxDbContext { get; }
        Lazy<ILoggerService> LoggerService { get; }
        IFelAggregate FelAggregate { get; }
        IWaveAggregate WaveAggregate { get; }
        IShippingPlateDbContext ShippingPlateDbContext { get; }
    }
}