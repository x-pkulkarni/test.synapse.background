﻿// <copyright file="IPlateAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-14</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-14</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    public interface IPlateAggregate
    {
        bool UpdatePlateQtyAsked(IDbConnection connection, string lastUser, int baseQty, string lpId);

        IList<Plate> GetPlateByLpId(IDbConnection connection, string lpId);

        bool DeletePlateByLpId(IDbConnection connection, string lpId);

        int GetPendingPlatePicksLine(IDbConnection connection, PendingPickQtyRequest pendingPlatePickQtyRequest);

        int GetPendingPicksVendorItem(IDbConnection connection, PendingPickQtyRequest pendingPlatePickQtyRequest);

        int GetPendingPicksItem(IDbConnection connection, PendingPickQtyRequest pendingPlatePickQtyRequest);
        
        bool DeleteShippingPlateByLpId(IDbConnection connection, string lpId);
    }
}