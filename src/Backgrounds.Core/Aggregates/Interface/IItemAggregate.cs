﻿// <copyright file="IItemDomain.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-18</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-03-23</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Integration;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    public interface IItemAggregate
    {
        List<PickTaskRequest> ExecuteReleasLine(IDbConnection connection, OrderTo order, ICommand waveRequest);
        List<PickTaskRequest> ExecuteSingleReleasLine(IDbConnection connection, OrderTo order, ICommand request);
        List<Commitment> GetCommitmentByVendorTracked(IDbConnection connection, OrderDetail orderDetail,
            OrderHeader orderHeader);
    }
}