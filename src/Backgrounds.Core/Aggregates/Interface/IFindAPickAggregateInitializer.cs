﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    public interface IFindAPickAggregateInitializer
    {
        IPickDbContext PickDbContext { get;  }
        IBatchTaskAggregate BatchTaskAggregate { get;  }
        IShippingPlateDbContext ShippingPlateDbContext { get;  }
        ILocationDbContext LocationDbContext { get;  }
        ITaskService TaskDomain { get;  }
        ISubTaskAggregate SubTaskAggregate { get;  }
        ICustItemAggregate CustItemAggregate { get;  }
        ICatchWeightAggregate CatchWeightAggregate { get;  }
        IPlateAggregate PlateAggregate { get;  }
    }
}
