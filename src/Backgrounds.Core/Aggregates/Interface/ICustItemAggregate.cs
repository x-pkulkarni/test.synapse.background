﻿// <copyright file="ICustItemAggregate.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-17</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    public interface ICustItemAggregate
    {
        string GetVendorTrackedFlag(IDbConnection connection, string customerId, string item);

        double GetItemCube(IDbConnection connection, string custId, string item, string uom);

        double GetItemWeight(IDbConnection connection, string custId, string item, string uom);

        int GetUotmQuantity(IDbConnection connection, CustItemUom custItemRequest);

        CustItemUom ToCustItemUom(string fromUom, string toUom, string custId, string item, int quantity);

        string GetItemHazmatIndicator(IDbConnection connection, string custId, long orderId, int shipId);

        string GetItemUnCode(IDbConnection connection, string custId, string item, long orderId, int shipId);

        string GetNextHighUom(IDbConnection connection, string custId, string item);

        string GetPutwallExemptFlag(IDbConnection connection, string custId, string item);

        bool GetCartonizationFlag(IDbConnection connection, string custId);

        int GetPutwallCount(IDbConnection connection, string custId, string facility);

        bool GetAllocRuleCount(IDbConnection connection, string custId, string facility, string item, string allocNeed);

        List<CustItemUom> GetItemByUom(IDbConnection connection, string custId, string item, int qty, string BaseUom);
    }
}
