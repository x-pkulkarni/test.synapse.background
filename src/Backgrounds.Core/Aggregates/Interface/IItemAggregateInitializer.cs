﻿// <copyright file="IItemDomainInitializer.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-02-08</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-08</lastchangeddate>

using System;
using Synapse.Backgrounds.Core.Profile;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    public interface IItemAggregateInitializer
    {
         IOrderDbContext OrderDbContext { get; }
         MappingEngine<ItemAggregateMapperProfile> MapperEngine { get; }
         ITaskAggregate TaskAggregate { get; }
         ICommitmentDbContext CommitmentDbContext { get; }
         ICustItemDbContext CustItemDbContext { get; }
         ICustomerDbContext CustomerDbContext { get; }
         IItemDemandDbContext ItemDemandDbContext { get; }
         Lazy<IFindAPickAggregate> FindAPickAggregate { get; }
    }
}