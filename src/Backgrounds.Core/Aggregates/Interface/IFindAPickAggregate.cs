﻿// <copyright file="IFindAPickAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-14</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-14</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json.Linq;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    public interface IFindAPickAggregate
    {
        void ExecuteFullPick(IDbConnection connection, PickTaskRequest pickTaskRequest);

        void ExecuteFindAPickForRemainders(IDbConnection connection, PickTaskRequest pickTaskRequest);
        List<BatchTask> ExecuteBatchPick(IDbConnection connection, PickTaskRequest pickTaskRequest);
        List<BatchTask> ExecuteBatchPickOnce(IDbConnection connection, PickTaskRequest pickTaskRequest);
        
        //void ExecuteBatchPick(IDbConnection connection, PickTaskRequest pickTaskRequest);
    }
}