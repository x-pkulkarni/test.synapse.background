﻿// <copyright file="IFelAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Kathiresan, Murugan</author> 
// <createddate>2019-04-25</createddate>
namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    using System.Data;
    using Grains.Contract;
    using Model;

    public interface IFelAggregate
    {
        void ExecuteUnReleaseFelOrder(IDbConnection connection, Wave wave,ICommand unReleaseCommand);
        void TransmitConnectShipOrder(IDbConnection connection, OrderHeader cShipOrder, Wave wave);

        
        void WriteParcel(IDbConnection connection, ConnectShipHeader cHdr);
        void TransmitLtlOrder(IDbConnection connection, OrderHeader cShipOrder);
        void InsertParcelStaging(IDbConnection connection, int taskSequence, 
            ParcelStaging ltlTask, int qty);
        void TransmitFelOrder(IDbConnection connection, OrderHeader cShipOrder);

        void UpdatePackDetail(IDbConnection connection, ConnectShipPackLable pak,
            ConnectShipOrderHdr connectShipOrderHdr, ref ConnectShipDtl connectShipDtl);

        void UpdatePackHeader(IDbConnection connection, ConnectShipPackLable connectShipPackLable,
            CustomerAux lstCustomerAux, ConnectShipOrderHdr connectShipOrderHdr,
            ref ConnectShipHeader connectShipHeader);

        void TransmitConnectShipForSingle(IDbConnection connection, Wave wave, BatchTask batchTask);
        ConnectShipHeader GetConnectShipHeader(IDbConnection connection, Wave wave,
            ConnectShipOrderHdr connectShipOrderHdr, ConnectShipSubTaskQtyDetails subTaskQty);
        ConnectShipDtl GetConnectShipDetails(IDbConnection connection, ConnectShipOrderHdr connectShipOrderHdr);
        string Fillhazmat(IDbConnection connection, long orderId, int shipId, string custId, string item);

        void InitPackHeader(IDbConnection connection, ConnectShipPackLable connectShipPackLable,
            CustomerAux lstCustomerAux, ConnectShipOrderHdr connectShipOrderHdr,
            ref ConnectShipHeader connectShipHeader);

    }
}
