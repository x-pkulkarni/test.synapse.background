﻿// <copyright file="IBatchTaskAggregateInitializer.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-08-03</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-03</lastchangeddate>

using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Aggregates.Interface
{
    public interface IBatchTaskAggregateInitializer
    {
        IBatchTaskDbContext BatchTaskDbContext { get; }
        ISubTaskAggregate SubTaskDomain { get; }
        IPlateAggregate PlateAggregate { get; }
        ITaskService TaskService { get; }
        ILocationDbContext LocationDbContext { get; }
        ICatchWeightAggregate CatchWeightDomain { get; }
        ICustItemAggregate CustItem { get; }
        ILaborStandardAggregate LaborStandardDoamin { get; }
    }
}