﻿// <copyright file="OrderAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-23</lastchangeddate>

namespace Synapse.Backgrounds.Core.Aggregates
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using AutoMapper;
    using Common;
    using Data.Interface;
    using Grains.Contract;
    using Infrastructure.Logging.Interface;
    using Integration;
    using Interface;
    using Model;
    using Newtonsoft.Json.Linq;
    using Profile;
    using StackExchange.Profiling.Internal;
    using Validation;
    using Validation.Interface;
    using ValidationContext = Model.ValidationContext;

    public class OrderAggregate : IOrderAggregate
    {

        #region Constructor(S)

        public OrderAggregate(IOrderAggregateInitializer orderAggregateInitializer)
        {
            _findAPickAggregate = orderAggregateInitializer.FindAPickAggregate;
            _orderStatusAggregate = orderAggregateInitializer.OrderStatusAggregate;
            _orderDbContext = orderAggregateInitializer.OrderDbContext;
            _itemAggregate = orderAggregateInitializer.ItemDomain;
            _batchTaskAggregate = orderAggregateInitializer.BatchTaskAggregate;
            _subTaskAggregate = orderAggregateInitializer.SubTaskAggregate;
            _validator = orderAggregateInitializer.Validator;
            _taskService = orderAggregateInitializer.TaskService;
            _plateAggregate = orderAggregateInitializer.PlateAggregate;
            _custItemAggregate = orderAggregateInitializer.CustItemAggregate;
            _cartonService = orderAggregateInitializer.CartonService;
            _customerAuxDbContext = orderAggregateInitializer.CustomerAuxDbContext;
            _mapper = new MappingEngine<OrderAggregateMapperProfile>().CreateMapper();
            _loggerService = orderAggregateInitializer.LoggerService;
            _felAggregate = orderAggregateInitializer.FelAggregate;
            _waveAggregate = orderAggregateInitializer.WaveAggregate;
            _shippingPlateDbContext = orderAggregateInitializer.ShippingPlateDbContext;
        }

        #endregion

        #region Private Member(S)

        private readonly IOrderDbContext _orderDbContext;
        private readonly IValidator<OrderValidator> _validator;
        private readonly Lazy<IItemAggregate> _itemAggregate;
        private readonly ISubTaskAggregate _subTaskAggregate;
        private readonly ICustItemAggregate _custItemAggregate;
        private readonly IBatchTaskAggregate _batchTaskAggregate;
        private readonly Lazy<IOrderStatusAggregate> _orderStatusAggregate;
        private ValidationContext _validationRequest;
        private readonly IFindAPickAggregate _findAPickAggregate;
        private readonly IPlateAggregate _plateAggregate;
        private readonly ITaskService _taskService;
        private readonly ICartonService _cartonService;
        private readonly IMapper _mapper;
        private readonly ICustomerAuxDbContext _customerAuxDbContext;
        private readonly Lazy<ILoggerService> _loggerService;
        private readonly IFelAggregate _felAggregate;
        private readonly IWaveAggregate _waveAggregate;
        private readonly IShippingPlateDbContext _shippingPlateDbContext;
        private const string UOMEA = "EA";
        private const string TYPELBL = "LBL";
        private const string WFELMSGOKAY = "OKAY";

        #endregion

        #region Public Method(S)

        public void ExecuteReleaseOrder(IDbConnection connection, Wave wave, ICommand request)
        {
            var lstOrderHeader = _orderDbContext.GetOrderHeaderByWave(connection, request.Wave).ToList();
            var pickTaskRequests = new List<PickTaskRequest>();

            var singlePickOrders = new List<OrderTo>();
            //var orderCartons = new List<OrderCartons>();

            if (request.PickType.Equals(Constants.PickType.BatchSingles))
            {
                singlePickOrders = GetSingleOrders(connection, lstOrderHeader, wave, request);
            }
            foreach (var orderHeader in lstOrderHeader)
            {
                var orderDetails = _orderDbContext
                    .GetOrderDetailsById(connection, orderHeader.OrderId, orderHeader.ShipId).ToList();

                _validationRequest = new ValidationContext
                {
                    Waves = new List<Wave> { wave },
                    OrderHeader = orderHeader,
                    OrderDetails = orderDetails,
                    Command = request
                };

                if (!_validator.IsValid(connection, _validationRequest))
                {
                    continue;
                }

                orderDetails.ForEach(o =>
                {
                    var order = new OrderTo();
                    var orderDtl = singlePickOrders.FindAll(x => x.OrderDetail.Item == o.Item && x.OrderDetail.OrderId == o.OrderId);
                    if (orderDtl.Any())
                    {
                        int countSingleItems = orderDtl.Sum(od => od.Quantity);
                        o.QtyOrder = o.QtyOrder - countSingleItems;
                        if (o.QtyOrder == 0)
                        {
                            return;
                        }
                    }

                    order = new OrderTo { OrderDetail = o, HeaderInfo = orderHeader, WaveInfo = wave };

                    var pickTaskRequest = _itemAggregate.Value.ExecuteReleasLine(connection, order, request);
                    if (pickTaskRequest.Any())
                    {
                        pickTaskRequests.AddRange(pickTaskRequest);
                    }
                });
            }

            //Defect 1230 Ideally it should allocate the entire qty from highest order.
            pickTaskRequests = pickTaskRequests.OrderByDescending(x => x.OrderQty).ToList();
            GeneratePickTasks(connection, pickTaskRequests, wave, request, singlePickOrders);
            var isFel = _waveAggregate.GetFelType(wave) > 0;
            //Generate batch pick tasks for singles
            GeneratePickTasksForSingles(connection, singlePickOrders, wave, request, isFel);

            if (wave.IsSplitTasksForMultiPickers)
            {
                ZoneBasedTasksGrouping(connection, lstOrderHeader, wave.Id, Constants.TaskType.OrderPick,
                    request.UserId);
            }

            ZoneBasedTasksGrouping(connection, lstOrderHeader, wave.Id, Constants.TaskType.PickTask, request.UserId);

            UpdateTaskQuantityForSubTasks(connection, wave.Id);

            //Order status/Load status & stop status Update
            lstOrderHeader.ForEach(orderHeader =>
            {
                _orderStatusAggregate.Value.UpdateOrderHeaderStatusToReleased(connection, orderHeader, request);
                _orderStatusAggregate.Value.AddOrderHistory(connection, orderHeader, request.UserId,
                    request.CommandText);
                _orderStatusAggregate.Value.UpdateLoadStatus(connection, orderHeader.LoadNo, request.UserId,
                    orderHeader.StopNo);
            });
        }

        public void UpdateTaskQuantityForSubTasks(IDbConnection connection, long waveId)
        {
            var subTasks = _subTaskAggregate.GetSubTasksByWaveId(connection, waveId).ToList();

            var tasks = subTasks.Select(x => x.TaskId).Distinct();

            foreach (var taskId in tasks)
            {
                var groupSubTasks = subTasks.FindAll(st => st.TaskId == taskId);
                var task = new Task();
                task.TaskId = taskId;

                groupSubTasks.ForEach(subTask =>
                 {

                     if (task.Quantity == 0)
                     {
                         task.Uom = subTask.UOM;
                         task.PickUom = subTask.PickUom;
                     }
                     else
                     {
                         task.Uom = task.Uom.Equals(subTask.UOM) ? task.Uom : "";
                         task.PickUom = task.PickUom.Equals(subTask.PickUom) ? task.PickUom : "";
                     }

                     task.Quantity += subTask.Quantity;
                     task.PickQty += subTask.PickQty;
                 });

                _taskService.UpdateTaskQuantityAndUom(connection, task);
            }
        }

        public void GeneratePickTasksForSingles(IDbConnection connection, List<OrderTo> singlePicks, Wave wave,
            ICommand request, bool isFel)
        {
            if (!singlePicks.Any())
            {
                return;
            }


            var batchPickRequests = new List<PickTaskRequest>();
            foreach (var singleLine in singlePicks)
            {
                var pickTaskRequest = _itemAggregate.Value.ExecuteSingleReleasLine(connection, singleLine, request);
                batchPickRequests.AddRange(pickTaskRequest);
            }

            var disctinctOrderItems = batchPickRequests.GroupBy(x => x.Item).Select(group =>
                new BatchTask { Item = group.First().Item, Quantity = group.Sum(c => c.BaseQty) });
            //var batchedItemdetails = new Dictionary<string, int>();
            foreach (var item in disctinctOrderItems)
            {
                var batchPickRequest = batchPickRequests.First(p => p.Item == item.Item).ShallowCopy();
                batchPickRequest.Command.PickType = Constants.PickType.BatchSingles;
                batchPickRequest.BaseQty = item.Quantity;
                batchPickRequest.OrderQty = batchPickRequest.Commitment.Quantity;
                var qty = batchPickRequest.BaseQty;
                do
                {
                    var batchTasksList = new List<BatchTask>();

                    var result = _findAPickAggregate.ExecuteBatchPickOnce(connection, batchPickRequest);

                    var batchedQty = result.Sum(r => r.Quantity);
                    qty = qty - batchedQty;
                    batchPickRequest.BaseQty = batchPickRequest.BaseQty - batchedQty;

                    var selectRequest = batchPickRequests.Where(p => p.Item == item.Item).Take(batchedQty).ToList();

                    if (!selectRequest.Any())
                    {
                        qty = 0;
                        continue;
                    }

                    //Below snippet will be executed only if there is only an order with current item.
                    if (selectRequest.Count == 1)
                    {
                        result.First().OrderId = selectRequest.First().Order.HeaderInfo.OrderId;
                        result.First().ShipId = selectRequest.First().Order.HeaderInfo.ShipId;
                        batchTasksList.AddRange(result);
                        if (batchPickRequests.Any())
                            batchPickRequests.RemoveRange(0, 1);
                    }
                    else
                    {
                        var mapper = new MappingEngine<OrderAggregateMapperProfile>().CreateMapper();
                        // Below snippet match up the number of batch tasks record with each order of the current item. 
                        foreach (var t in selectRequest)
                        {
                            batchedQty = batchedQty - t.BaseQty;

                            var referenceBatchDetails = new BatchTask();
                            mapper.Map(result.First(), referenceBatchDetails);
                            referenceBatchDetails.OrderId = t.Order.HeaderInfo.OrderId;
                            referenceBatchDetails.ShipId = t.Order.HeaderInfo.ShipId;
                            referenceBatchDetails.PickUom = result.First().UOM;

                            referenceBatchDetails.Quantity = t.BaseQty;
                            referenceBatchDetails.PickQty = t.BaseQty;
                            batchTasksList.Add(referenceBatchDetails);
                            if (batchPickRequests.Any())
                                batchPickRequests.Remove(t);

                        }
                    }

                    batchTasksList.ForEach(b =>
                    {
                        b.TaskType = "BS";
                        _batchTaskAggregate.CreateBatchTask(connection, b);
                        if (!b.LpId.IsNullOrWhiteSpace())
                            _plateAggregate.UpdatePlateQtyAsked(connection, request.UserId,
                                b.Quantity,
                                b.LpId);
                    });

                    _batchTaskAggregate.GenerateBatchTasksForSingles(connection, wave, request, batchTasksList);
                    if (isFel)
                    {
                        batchTasksList.ForEach(b =>
                        {
                            _felAggregate.TransmitConnectShipForSingle(connection, wave, b);
                        });
                    }
                } while (qty > 0);
            }
        }

        private List<OrderTo> GetSingleOrders(IDbConnection connection, List<OrderHeader> lstOrderHeader, Wave wave,
            ICommand request)
        {
            var singlePickOrders = new List<OrderTo>();

            foreach (var orderHeader in lstOrderHeader)
            {
                var orderDetails = _orderDbContext
                    .GetOrderDetailsById(connection, orderHeader.OrderId, orderHeader.ShipId).ToList();

                if (orderDetails.Count == 1)
                {
                    var orderDetail = orderDetails.FirstOrDefault();

                    if (orderDetail.QtyOrder == 1 && orderDetail.UomEntered == UOMEA) // need to add OR shipalone
                    {
                        singlePickOrders.Add(new OrderTo
                        {
                            HeaderInfo = orderHeader,
                            OrderDetail = orderDetail,
                            WaveInfo = wave,
                            Quantity = 1
                        });

                        continue;
                    }
                }


                var itemsToPack = _cartonService
                    .GetItemDetails(connection, orderHeader.CustomerId, orderHeader.OrderId)
                    .ToList();

                List<Item> itemstoremove = new List<Item>();
                
                foreach (var itemToPack in itemsToPack)
                {
                    if (itemToPack.PickToType == TYPELBL)
                    {
                        var orderDetail = orderDetails.Where(o => o.Item == itemToPack.ItemDescription)
                            .Select(o => o).FirstOrDefault();
                        if (orderDetail.UomEntered == UOMEA)
                        {
                            itemstoremove.Add(itemToPack);
                            singlePickOrders.Add(new OrderTo
                            {
                                HeaderInfo = orderHeader,
                                OrderDetail = orderDetail,
                                WaveInfo = wave,
                                Quantity = orderDetail.QtyOrder
                            });
                        }

                    }
                }
            }

            return singlePickOrders;
        }

        /// <summary>
        ///     Unrelease entire wave
        /// </summary>
        public void ExecuteUnreleaseOrder(IDbConnection connection, Wave wave, ICommand request)
        {
            var lstOrderHeader = _orderDbContext.GetOrderHeaderByWave(connection, request.Wave).ToList();

            foreach (var orderHeader in lstOrderHeader)
            {
                var orderUnreleaseRequest = new OrderUnreleaseRequest
                {
                    Order = orderHeader,
                    WaveId = wave.Id,
                    Facility = wave.Facility,
                    UserId = request.UserId,
                    Command = request
                };
                HandleUnrelease(connection, orderUnreleaseRequest);
            }
        }


        /// <summary>
        ///     This method creates batch task records and related tasks & subtasks. This also carries out task grouping based on
        ///     picking zone batch task limitations
        /// </summary>
        public void GeneratePickTasks(IDbConnection connection, List<PickTaskRequest> pickTaskRequests, Wave wave,
            ICommand request, List<OrderTo> singlePickOrders)
        {
            if (!pickTaskRequests.Any())
            {
                return;
            }

            var disctinctOrderItems = pickTaskRequests.GroupBy(x => x.Item).Select(group =>
                new BatchTask { Item = group.First().Item, Quantity = group.Sum(c => c.BaseQty) });
            var batchTasks = new List<BatchTask>();
            var batchedItemdetails = new Dictionary<string, int>();

            foreach (var item in disctinctOrderItems)
            {
                var pickTaskRequest = pickTaskRequests.First(p => p.Item == item.Item).ShallowCopy();
                pickTaskRequest.BaseQty = item.Quantity;
                pickTaskRequest.OrderQty = pickTaskRequest.Commitment.Quantity;

                //check if the item is exempted from Putwall
                if (request.PickType.Equals(Constants.PickType.BatchSingles) ||
                        ValidatePutwallExempt(connection, pickTaskRequest.Order.OrderDetail.CustomerId,
                    pickTaskRequest.Order.OrderDetail.Item, pickTaskRequest.Command.Facility))
                {
                    batchedItemdetails.Add(item.Item, 0);
                    continue;
                }

                //DR - 6420 Run a check to validate that the qty/ uoms for this item grouping
                //has a usable allocation rule detail.If not then we can skip over the call to find_a_pick and
                //save some time.
                if (!_custItemAggregate.GetAllocRuleCount(connection, pickTaskRequest.Order.OrderDetail.CustomerId,
                    pickTaskRequest.Command.Facility
                    , pickTaskRequest.Order.OrderDetail.Item, Constants.AllocNeeded.BatchPick))
                {
                    //if we get here then we did not find any allocation rule support for baseUOM.
                    // now check if the qty for this item is enough to even reach the next level UOM.
                    // if not, then we are going to indicate there is no reason to expect there will an allocation
                    //rule to support this quantity of item and can be skipped over.
                    if (!IsHighLevelUomAvailable(connection, pickTaskRequest, disctinctOrderItems))
                    {
                        batchedItemdetails.Add(item.Item, 0);
                        continue;
                    }
                }


                var result = _findAPickAggregate.ExecuteBatchPick(connection, pickTaskRequest);
                if (!result.Any())
                {
                    batchedItemdetails.Add(item.Item, 0);
                    continue;
                }

                var batchedQty = result.Sum(r => r.Quantity);
                batchedItemdetails.Add(item.Item, batchedQty);

                // More than one batch tasks will exist only if items are not available in single lpid. So just add up batch tasks with out any changes.
                if (result.Count > 1)
                {
                    batchTasks.AddRange(result);
                    continue;
                }

                var selectRequest = pickTaskRequests.Where(p => p.Item == item.Item).ToList();
                if (!selectRequest.Any())
                {
                    continue;
                }

                //Below snippet will be executed only if there is only an order with current item.
                if (selectRequest.Count == 1)
                {
                    result.First().OrderId = selectRequest.First().Order.HeaderInfo.OrderId;
                    result.First().ShipId = selectRequest.First().Order.HeaderInfo.ShipId;
                    batchTasks.AddRange(result);
                    continue;
                }

                // Below snippet match up the number of batch tasks record with each order of the current item. 
                foreach (var t in selectRequest)
                {
                    batchedQty = batchedQty - t.BaseQty;
                    var mapper = new MappingEngine<OrderAggregateMapperProfile>().CreateMapper();
                    var referenceBatchDetails = new BatchTask();
                    mapper.Map(result.First(), referenceBatchDetails);
                    referenceBatchDetails.OrderId = t.Order.HeaderInfo.OrderId;
                    referenceBatchDetails.ShipId = t.Order.HeaderInfo.ShipId;
                    referenceBatchDetails.PickUom = result.First().UOM;
                    if (batchedQty > 0)
                    {
                        referenceBatchDetails.Quantity = t.BaseQty;
                        referenceBatchDetails.PickQty = t.BaseQty;
                        batchTasks.Add(referenceBatchDetails);
                    }
                    else if (batchedQty == 0)
                    {
                        referenceBatchDetails.Quantity = t.BaseQty;
                        referenceBatchDetails.PickQty = t.BaseQty;
                        batchTasks.Add(referenceBatchDetails);
                        break;
                    }
                    else
                    {
                        referenceBatchDetails.Quantity = t.OrderQty + batchedQty;
                        referenceBatchDetails.PickQty = t.OrderQty + batchedQty;
                        batchTasks.Add(referenceBatchDetails);
                        break;
                    }
                }
            }

            // Generate OP tasks for remainders from Batch find a pick execution
            foreach (var itemDetails in batchedItemdetails)
            {
                var isRemainderAvailable = false;
                var itemQty = itemDetails.Value;
                foreach (var pickRequest in pickTaskRequests.Where(p => p.Item == itemDetails.Key))
                {
                    if (!isRemainderAvailable)
                    {
                        itemQty = itemQty - pickRequest.BaseQty;
                        if (itemQty >= 0)
                        {
                            continue;
                        }

                        isRemainderAvailable = true;
                        pickRequest.BaseQty = itemQty * -1;
                    }
                    //var algorithmResult = orderCartons.FirstOrDefault(oc => oc.OrderId == pickRequest.Order.HeaderInfo.OrderId).cartonsResult;
                    //var orderCarton =
                    //    orderCartons.FirstOrDefault(oc => oc.OrderId == pickRequest.Order.HeaderInfo.OrderId);
                    //var algorithmResult = orderCarton?.cartonsResult ?? new JObject();


                    _findAPickAggregate.ExecuteFindAPickForRemainders(connection, pickRequest);
                }
            }


            CartonizeBasedTasksGrouping(connection, wave, Constants.TaskType.OrderPick, request, singlePickOrders, pickTaskRequests);


            if (!batchTasks.Any())
            {
                return;
            }

            // Creates batch task
            batchTasks.ForEach(b =>
            {
                b.BatchTaskWeight = b.BatchTaskWeight * b.PickQty;
                b.BatchTaskCube = b.BatchTaskCube * b.PickQty;
                b.BatchTaskStaffHrs = b.BatchTaskStaffHrs * b.PickQty;
                b.BatchPickToType = b.PickToType == "PACK" ? "TOTE" : "PAL";
                _batchTaskAggregate.CreateBatchTask(connection, b);
                if (b.LpId != null)
                    _plateAggregate.UpdatePlateQtyAsked(connection, request.UserId,
                        b.Quantity,
                        b.LpId);
            });

            //Creates tasks & subtasks for batch record. Executes task grouping based on picking zone
            _batchTaskAggregate.GenerateBatchTasks(connection, wave, request, batchTasks);
        }

        public void CartonizeBasedTasksGrouping(IDbConnection connection, Wave wave,
            string taskType, ICommand request, List<OrderTo> singlePickOrders, List<PickTaskRequest> pickTaskRequests)
        {
            var lstOrderHeader = _orderDbContext.GetOrderHeaderByWave(connection, request.Wave).ToList();
            var mapper = new MappingEngine<OrderAggregateMapperProfile>().CreateMapper();
            foreach (var orderHeader in lstOrderHeader)
            {
                var cartonizationFlag = _custItemAggregate.GetCartonizationFlag(connection, orderHeader.CustomerId);

                if (cartonizationFlag)
                {
                    var itemsToPack = _cartonService
                        .GetSubTaskItemDetails(connection, taskType, orderHeader.OrderId)
                        .ToList();

                    var groupedBatchTasks = itemsToPack.Select(x => x.ItemPackingGroup).Distinct();

                    var subTasks = _subTaskAggregate.GetSubTasksByTaskTypeAndOrder(connection, request.Wave,
                        taskType,
                        orderHeader.OrderId,
                        orderHeader.ShipId);

                    foreach (var packingGroup in groupedBatchTasks)
                    {
                        var itemToPack = itemsToPack.FindAll(i => i.ItemPackingGroup == packingGroup);


                        var cartons = _cartonService
                            .GetCartonDetails(connection, orderHeader.CustomerId, orderHeader.OrderId, packingGroup)
                            .ToList();

                        if (!subTasks.Any())
                        {
                            continue;
                        }

                        var packingRequest = new PackingRequest()
                        {
                            AlgorithmCode = "3D",
                            Containers = cartons,
                            ItemsToPack = itemToPack
                        };

                        //We are here when have a full pick and there are more carton types for a given carton group associated to subtask
                        var cartonizationApiEndpoint = request.ApiEndPoints.ToList().Find(e =>
                            string.Equals(e.Key, "CARTONIZATION", StringComparison.InvariantCultureIgnoreCase))?.Url;

                        var algorithmResult =
                            JObject.Parse(_cartonService.PackOrder(packingRequest, cartonizationApiEndpoint)
                                .GetAwaiter()
                                .GetResult());

                        var containerPackingResults = (JArray)algorithmResult["containerPackingResults"];
                        if (containerPackingResults.Count <= 0)
                        {
                            foreach (var item in itemToPack)
                            {
                                if (item.ItemQuantity == 1 && item.ItemUom == UOMEA)
                                {
                                    AddIntosinglePickOrders(connection, wave, orderHeader, item, singlePickOrders);
                                }
                                else
                                {
                                    var findsubtask = subTasks.Find(st =>
                                        st.Item == item.ItemDescription && st.CartonType == item.ItemPackingGroup
                                                                        && st.PickQty == item.ItemQuantity &&
                                                                        st.PickUom == item.ItemUom);

                                    if (findsubtask != null)
                                    {
                                        var commitment = pickTaskRequests.Find(p => p.Order.HeaderInfo.OrderId == orderHeader.OrderId && p.Item == item.ItemDescription)
                                            .Commitment;

                                        _subTaskAggregate.CreateSubTaskForCarton(connection, findsubtask, item, orderHeader,
                                            commitment, request,
                                            "PAL",
                                            1);
                                    }
                                }
                            }
                        }
                        else if (containerPackingResults.Count == 1) //if we have packed everything in one container type
                        {
                            foreach (var item in itemToPack)
                            {
                                if (item.ItemQuantity == 1 && item.ItemUom == UOMEA)
                                {
                                    AddIntosinglePickOrders(connection, wave, orderHeader, item, singlePickOrders);
                                }
                                else
                                {
                                    var findsubtask = subTasks.Find(st =>
                                        st.Item == item.ItemDescription && st.CartonType == item.ItemPackingGroup
                                                                        && st.PickQty == item.ItemQuantity &&
                                                                        st.PickUom == item.ItemUom);
                                    if (findsubtask != null)
                                    {
                                        var commitment = pickTaskRequests.Find(p => p.Order.HeaderInfo.OrderId == orderHeader.OrderId && p.Item == item.ItemDescription)
                                            .Commitment;

                                        _subTaskAggregate.CreateSubTaskForCarton(connection, findsubtask, item, orderHeader,
                                            commitment, request,
                                            (string)algorithmResult["containerPackingResults"][0][
                                                "containerDescription"],
                                            1);
                                    }
                                }
                            }
                        }
                        else
                        {
                            var containerItemsPacked = _cartonService.GetPackedItemsList(containerPackingResults);
                            var sequenceCounter = 0;
                            foreach (var contItem in containerItemsPacked)
                            {
                                foreach (var item in contItem.Value)
                                {
                                    if (item.ItemQuantity == 1 && item.ItemUom == UOMEA)
                                    {
                                        AddIntosinglePickOrders(connection, wave, orderHeader, item, singlePickOrders);
                                    }
                                    else
                                    {
                                        sequenceCounter++;
                                        var subTask = new SubTask();
                                        var findsubtask = subTasks.Find(st =>
                                            st.Item == item.ItemDescription && st.CartonType == item.ItemPackingGroup
                                                                            && st.PickUom == item.ItemUom);
                                        var commitment = pickTaskRequests.Find(p => p.Order.HeaderInfo.OrderId == orderHeader.OrderId && p.Item == item.ItemDescription)
                                            .Commitment;

                                        _subTaskAggregate.CreateSubTaskForCarton(connection, findsubtask, item, orderHeader,
                                            commitment, request,
                                            contItem.Key,
                                            sequenceCounter);
                                    }
                                }
                            }
                        }
                    }

                    foreach (var subTask in subTasks)
                    {
                        _subTaskAggregate.DeleteByRowId(connection, subTask.RowId);

                        var remainingSubTaskCount = _subTaskAggregate.GetSubTaskCountByTaskId(connection, subTask.TaskId);
                        if (remainingSubTaskCount.Equals(0))
                            _taskService.Delete(connection, subTask.TaskId);
                    }
                }
            }
        }

        public void AddIntosinglePickOrders(IDbConnection connection, Wave wave, OrderHeader orderHeader, Item item,
            List<OrderTo> singlePickOrders)
        {
            var orderDetails = _orderDbContext
                .GetOrderDetailsById(connection, orderHeader.OrderId, orderHeader.ShipId).ToList();

            var orderDetail = orderDetails.Where(o => o.Item == item.ItemDescription)
                .Select(o => o).FirstOrDefault();

            singlePickOrders.Add(new OrderTo
            {
                HeaderInfo = orderHeader,
                OrderDetail = orderDetail,
                WaveInfo = wave,
                Quantity = item.ItemQuantity
            });
        }

        public void ZoneBasedTasksGrouping(IDbConnection connection, List<OrderHeader> orders, long wave,
            string taskType, string lastUser)
        {
            foreach (var orderHeader in orders)
            {
                var subTasks = _subTaskAggregate.GetSubTasksByTaskTypeAndOrder(connection, wave, taskType,
                    orderHeader.OrderId,
                    orderHeader.ShipId);
                if (subTasks.Count == 0)
                {
                    continue;
                }

                _taskService.DeleteByOrderAndType(connection, orderHeader.OrderId, orderHeader.ShipId,
                    taskType);
                var taskCounter = new List<TaskGroupingCounter>();
                foreach (var subtask in subTasks)
                {
                    var currentCounter = taskCounter.FirstOrDefault(bc =>
                        bc.ZoneId == subtask.PickingZone && bc.Task.PickToType == subtask.PickToType &&
                        bc.Task.FromLocation == subtask.FromLocation);
                    Task task = null;
                    if (currentCounter == null || currentCounter.Counter <= 0)
                    {
                        task = _taskService.CreateTaskFromSubTask(connection, subtask, lastUser);
                        if (currentCounter != null)
                        {
                            currentCounter.Task = task;
                        }
                    }
                    else
                    {
                        if (currentCounter.Task != null)
                        {
                            currentCounter.Task.Quantity += subtask.Quantity;
                            currentCounter.Task.PickQty += subtask.PickQty;
                            currentCounter.Task.Weight += subtask.Weight;
                            currentCounter.Task.Cube += subtask.Cube;
                            currentCounter.Task.StaffHrs += subtask.StaffHrs;
                            _taskService.UpdateTask(connection, currentCounter.Task);
                        }
                    }

                    if (currentCounter == null)
                    {
                        var zone = _taskService.GetTaskGroupingRulesByZone(connection, subtask.PickingZone,
                            subtask.Facility);
                        var pickTasklimit = 0;
                        if (zone != null)
                        {
                            if (taskType == Constants.TaskType.OrderPick)
                            {
                                pickTasklimit =
                                    zone.SeparateOrderTasksFlag.Equals("Y", StringComparison.CurrentCultureIgnoreCase)
                                        ? zone.OrderTasksLimit
                                        : 0;
                            }
                            else
                            {
                                pickTasklimit =
                                    zone.SeparateLineTasksFlag.Equals("Y", StringComparison.CurrentCultureIgnoreCase)
                                        ? zone.LineTasksLimit
                                        : 0;
                            }
                        }

                        currentCounter = new TaskGroupingCounter
                        {
                            Facility = subtask.Facility,
                            BaseLimit = pickTasklimit,
                            Counter = pickTasklimit,
                            Task = task,
                            ZoneId = subtask.PickingZone
                        };
                        taskCounter.Add(currentCounter);
                    }

                    currentCounter.Counter -= 1;
                    _subTaskAggregate.UpdateTaskIdByRowId(connection, subtask.RowId, currentCounter.Task.TaskId,
                        lastUser);

                    _shippingPlateDbContext.UpdateTaskIdInShippingPlate(connection, subtask.ShippingLpId,
                        currentCounter.Task.TaskId);

                }
            }
        }

        public IEnumerable<OrderDetail> GetOrderDetailsByItem(IDbConnection connection, long orderId, int shipId,
            string item)
        {
            return _orderDbContext.GetOrderDetailsByItem(connection, orderId, shipId, item);
        }

        public IEnumerable<OrderHeader> GetOrderHeaderByOrderId(IDbConnection connection, long orderId, int shipId)
        {
            return _orderDbContext.GetOrderHeaderByOrderId(connection, orderId, shipId);
        }

        public void ExecuteTransmitCshipOrder(IDbConnection connection, Wave wave, int felType)
        {
            var orderHeaders = _orderDbContext.GetCShipOrderHeaderByWave(connection, wave.Id).ToList();
            var sendConnectShip = false;
            if (!orderHeaders.Any())
            {
                return;
            }

            foreach (var orderHeader in orderHeaders)
            {
                if (orderHeader.IsMultiShip)
                {
                    if (wave.IsBulkReturn && orderHeader.IsFelConnectShip)
                    {
                        _felAggregate.TransmitConnectShipOrder(connection, orderHeader, wave);
                        sendConnectShip = true;
                    }
                    else if (felType.In(1, 3, 4, 5, 6, 7))
                    {
                        if (orderHeader.IsFelConnectShip && !felType.Equals(6))
                        {
                            _felAggregate.TransmitConnectShipOrder(connection, orderHeader, wave);
                            sendConnectShip = true;

                            _orderDbContext.SetupConveyor(connection, orderHeader.OrderId, orderHeader.ShipId);
                        }
                        else
                        {
                            if (felType.In(4, 5, 6, 7))
                            {
                                _felAggregate.TransmitFelOrder(connection, orderHeader);
                            }
                            else
                            {
                                _felAggregate.TransmitLtlOrder(connection, orderHeader);
                            }
                        }
                    }
                }
                else if (orderHeader.IsLtlFel && felType.In(2, 3))
                {
                    _felAggregate.TransmitLtlOrder(connection, orderHeader);
                }
            }

            //WFEL Handling
            if (wave.IsGenWfelLabels)
            {
                _loggerService.Value.Debug("  Calling generate_wfel_labels()");
                var msg = GenerateWfelLabels(connection, wave.Id, "WFEL");
                _loggerService.Value.Debug($"After generate_wfel_labels():out_msg =#{msg}");
                if (msg.Equals(WFELMSGOKAY))
                {
                    _loggerService.Value.Debug($"NOTICE: generate_wfel_labels():out_msg =#{msg}");
                }
            }

            //If any orders in the wave were to be transmitted with connectship, we send a message to wake up the connectship process.
            if (sendConnectShip)
            {
                _orderDbContext.SendZQM(connection);
            }
        }

        #endregion

        #region Private Method(S)     

        private string GenerateWfelLabels(IDbConnection connection, long wave, string inevent)
        {
            var outMsg = "";
            _loggerService.Value.Debug("Entering GENERATE_WFEL_LABELS");
            _loggerService.Value.Debug($"Parm in_wave=#{wave}");
            _loggerService.Value.Debug($"Parm in_event=#{inevent}");

            var abbrev = _orderDbContext.GetAbbrev(connection, inevent);
            if (abbrev.IsNullOrWhiteSpace())
            {
                _loggerService.Value.Debug($"Undefined Event:#{inevent}");
                outMsg = $"Undefined Event:#{inevent}";
                _loggerService.Value.Debug("Leaving GENERATE_WFEL_LABELS:UNDEFINED EVENT");
                return outMsg;
            }

            var slip = _orderDbContext.GetShipLipByWave(connection, wave);
            if (slip.ShippingLpId.IsNullOrWhiteSpace())
            {
                _loggerService.Value.Debug("Leaving GENERATE_WFEL_LABELS:NOTHING FOR WAVE");
                _loggerService.Value.Debug($"Can not find a shippingplate associated with wave:#{wave}");
                outMsg = $"Can not find a shippingplate associated with wave:#{wave}";
                return outMsg;
            }

            _loggerService.Value.Debug($"Using shippingplate =#{slip.ShippingLpId}");

            string uom, profId, msg;

            _loggerService.Value.Debug("Before call to zlbl.get_plate_profid()");
            _orderDbContext.GetPlateProfId(connection, inevent, slip.ShippingLpId, out uom, out profId, out msg);
            _loggerService.Value.Debug($"After call to zlbl.get_plate_profid(): ProfID(#{profId}) uom(#{uom})");

            var customerAux = _customerAuxDbContext.GetCustomerSettings(connection, slip.CustId).ToList()
                .FirstOrDefault();
            if (customerAux == null)
            {
                _loggerService.Value.Debug("Customer does not have a customer_aux record.");
                _loggerService.Value.Debug("Leaving GENERATE_WFEL_LABELS:NO CUST_AUX");
                outMsg = "Customer does not have a customer_aux record.";
                return outMsg;
            }

            _loggerService.Value.Debug(
                $"Customer will use (#{customerAux.StandardLabelOverrideColumn}) as their trigger column.");

            var lstProfLine = _orderDbContext.GetProfLine(connection, inevent, profId, uom, "S").ToList();
            foreach (var profLine in lstProfLine)
            {
                _loggerService.Value.Debug(
                    $"Working with Profile Line: prof id(#{profId}) event(#{inevent}) trigger(#{profLine.LabelTrigger}) view/proc(#{profLine.ViewName})");

                var ordHdr = _orderDbContext.GetOrderByTriggerValue(connection, wave, slip.CustId,
                    customerAux.StandardLabelOverrideColumn, profLine.LabelTrigger);

                if (ordHdr.OrderId.Equals(0))
                {
                    _loggerService.Value.Debug(
                        $"Skipping Profile Line: No orders on this wave(#{wave}) tied to trigger(#{profLine.LabelTrigger})");

                }
                else
                {
                    _loggerService.Value.Debug($"Using order(#{ordHdr.OrderId}/#{ordHdr.ShipId})");

                    var taskLip = _orderDbContext.GetShipLipByOrderAndShip(connection, ordHdr.OrderId, ordHdr.ShipId);
                    if (taskLip.ShippingLpId.IsNullOrWhiteSpace())
                    {
                        _loggerService.Value.Debug(
                            $"Can not find a shippingplate associated with order:#{ordHdr.OrderId} shipid:#{ordHdr.ShipId}");
                        _loggerService.Value.Debug("Leaving GENERATE_WFEL_LABELS:NO SHIPPING PLATE FOUND");
                        outMsg =
                            $"Can not find a shippingplate associated with order:#{ordHdr.OrderId} shipid:#{ordHdr.ShipId}";
                        return outMsg;
                    }

                    _loggerService.Value.Debug($"Using shippingplate =#{taskLip.ShippingLpId}");

                    msg = _orderDbContext.ExecuteViewOrProcedure(connection, profLine.ViewName, taskLip.ShippingLpId);
                    _loggerService.Value.Debug($"After inline call to (#{profLine.ViewName}) results:(#{msg})");
                }

            }

            outMsg = WFELMSGOKAY;
            _loggerService.Value.Debug("Leaving GENERATE_WFEL_LABELS:OKAY");
            return outMsg;
        }


        private void HandleUnrelease(IDbConnection connection, OrderUnreleaseRequest request)
        {
            var currentWave = request.Order?.Wave;
            var waveToUnrelease = request.WaveId;

            _validationRequest = new ValidationContext { OrderHeader = request.Order, Command = request.Command };
            var validationResult = _validator.IsValid(connection, _validationRequest);
            if (!validationResult)
            {
                return;
            }

            _subTaskAggregate.ResetTasksForUnrelease(connection, request);

            _orderStatusAggregate.Value.UpdateOrderAndPlateInfo(connection, request, currentWave, waveToUnrelease);

            _orderStatusAggregate.Value.AddOrderHistory(connection, request.Order, request.UserId,
                Enums.RequestType.UNRELWAV.ToString());
        }

        private bool ValidatePutwallExempt(IDbConnection connection, string customerId, string item, string facility)
        {
            if (_custItemAggregate.GetPutwallExemptFlag(connection, customerId, item).Contains('Y'))
            {
                var putWallCount = _custItemAggregate.GetPutwallCount(connection, customerId, facility);
                if (putWallCount > 0)
                {
                    return true;
                }
            }

            return false;
        }

        private bool IsHighLevelUomAvailable(IDbConnection connection, PickTaskRequest pickTaskRequest,
            IEnumerable<BatchTask> groupedItems)
        {
            //check if find a pick call is required.
            //To speed things up,if the quantity on the item ordered does not equal or exceed one higher uom level
            //above the base unit for the item, we can move on to the next item record.Take the baseUOM and find a conversion
            //from the baseUOM to another UOM with the least qty.So if the qty on the item is not at least as much as
            //this conversion, then we can move on because it will never qualify as a full of anything other than base UOM.
            var nextUom = _custItemAggregate.GetNextHighUom(connection, pickTaskRequest.Order.HeaderInfo.CustomerId,
                pickTaskRequest.Order.OrderDetail.Item);

            var first = groupedItems.FirstOrDefault(x =>
                x.Item != null && x.Item == pickTaskRequest.Order.OrderDetail.Item);

            if (first == null)
            {
                return true;
            }

            var groupedQty = first.Quantity;
            if (!string.IsNullOrEmpty(nextUom))
            {
                var custItemUomRequest = new CustItemUom
                {
                    CustId = pickTaskRequest.Order.HeaderInfo.CustomerId,
                    Item = pickTaskRequest.Order.OrderDetail.Item,
                    FromUom = pickTaskRequest.Order.OrderDetail.UomEntered,
                    ToUom = nextUom,
                    Qty = groupedQty
                };
                var factor = _custItemAggregate.GetUotmQuantity(connection, custItemUomRequest);
                var isFullPick = factor != 0 ? (double)groupedQty / factor : 0;
                if (isFullPick < 1)
                {
                    // _loggerService.Value.Debug("Not enough base UOM qty to reach the next level UOM. Skip over.");
                    return false;
                }
            }
            else
            {
                //_loggerService.Value.Debug("Not enough base UOM qty to reach the next level UOM. Skip over.");
                return false;
            }

            return true;
        }

        #endregion
    }
}