﻿// <copyright file="TaskService.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-14</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-21</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Synapse.Backgrounds.Core.Profile;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Aggregates
{
    public class TaskService : ITaskService
    {

        #region Constructor(S)

        public TaskService(ITaskDbContext taskDbContext)
        {
            _taskDbContext = taskDbContext;
        }

        #endregion

        #region Private Member(S)

        private readonly ITaskDbContext _taskDbContext;

        #endregion

        #region Public Member(S)

        public long GenerateNewTaskId(IDbConnection connection)
        {
            return _taskDbContext.GetNextTaskId(connection);
        }

        public IEnumerable<Task> GetByTypeAndOrder(IDbConnection connection, string tasktype, long wave, long orderId,
            int shipId)
        {
           return _taskDbContext.GetByTypeAndOrder(connection, tasktype, wave, orderId, shipId);
        }

        public int GetPassedTasksCountById(IDbConnection connection, long taskId)
        {
            return _taskDbContext.GetPassedTasksCountById(connection, taskId);
        }

        public bool Delete(IDbConnection connection, long taskId)
        {
            return _taskDbContext.Delete(connection, taskId);
        }
        public bool DeleteByOrderAndType(IDbConnection connection, long orderId, int shipId, string taskType)
        {
            return _taskDbContext.DeleteByOrderAndType(connection, orderId, shipId, taskType);
        }

        public bool UpdateTaskQuatity(IDbConnection connection, long taskId, int quantity)
        {
            return _taskDbContext.UpdateTaskQuatity(connection, taskId, quantity);
        }

        public long GetNextTaskId(IDbConnection connection)
        {
            return _taskDbContext.GetNextTaskId(connection);
        }

        public Task CreateTaskForFullPick(IDbConnection connection,
            Tuple<OrderTo, Commitment, ICommand, Location, Location, FindAPickResponse> source, string plateFromLoc)
        {
            var mapper = new MappingEngine<ItemAggregateMapperProfile>().CreateMapper();
            var request = mapper.Map<Task>(source);
            request.TaskId = GetNextTaskId(connection);
            request.FromLocation = plateFromLoc;
            _taskDbContext.Insert(connection, request);
            return request;
        }

        public Task CreateTaskFromSubTask(IDbConnection connection, SubTask subTask, string lastUser)
        {
            var mapper = new MappingEngine<OrderAggregateMapperProfile>().CreateMapper();
            var request = mapper.Map<Task>(subTask);
            request.TaskId = GetNextTaskId(connection);
            request.LastUser = lastUser;
            request.LoadNo = subTask.LoadNo;
            request.ShipNo = subTask.ShipNo;
            request.StopNo = subTask.StopNo;
            _taskDbContext.Insert(connection, request);
            return request;
        }
        public long CreateTaskForBatchPick(IDbConnection connection,
            Tuple<BatchTask, Plate, ICommand, Location, Location, Wave> source)
        {
            var mapper = new MappingEngine<OrderAggregateMapperProfile>().CreateMapper();
            var request = mapper.Map<Task>(source);
            request.TaskId = GetNextTaskId(connection);
            if (request.TaskType != "BP")
                return _taskDbContext.Insert(connection, request) ? request.TaskId : default(long);
            request.LoadNo = null;
            request.ShipNo = null;
            request.StopNo = null;
            return _taskDbContext.Insert(connection, request) ? request.TaskId : default(long);
        }

        public bool UpdateTask(IDbConnection connection, Task task)
        {
            return _taskDbContext.UpdateTask(connection, task);
        }

        public bool UpdateTaskQuantityAndUom(IDbConnection connection, Task task)
        {
            return _taskDbContext.UpdateTaskQuantityAndUom(connection, task);
        }

        public Zone GetTaskGroupingRulesByZone(IDbConnection connection, string zoneId, string facility)
        {
            return _taskDbContext.GetTaskGroupingRulesByZone(connection, zoneId, facility).FirstOrDefault();
        }
        #endregion

        #region Private Member(S)

        #endregion
    }
}