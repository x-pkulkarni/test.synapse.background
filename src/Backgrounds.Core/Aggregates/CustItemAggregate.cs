﻿// <copyright file="CustItemAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Autofac;
using AutoMapper;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;

namespace Synapse.Backgrounds.Core.Aggregates
{
    public class CustItemAggregate : ICustItemAggregate
    {
        #region Constructor(S)

        public CustItemAggregate(ILoggerService loggerService, IConfigService configService,
            IOracleConnectionProvider oracleConnection, IComponentContext icoContext)
        {
            _oracleConnection = oracleConnection;
            _configService = configService;
            _custItemDbContext = icoContext.Resolve<ICustItemDbContext>();
            _custItemUomDbContext = icoContext.Resolve<ICustItemUomDbContext>();
            _customerAuxDbContext = icoContext.Resolve<ICustomerAuxDbContext>();
            _orderDbContext = icoContext.Resolve<IOrderDbContext>();
            _custItemCatchWeightDbContext = icoContext.Resolve<ICustItemCatchWeightDbContext>();
            _mapper = icoContext.Resolve<IMapper>();
        }

        #endregion

        #region Private Member(S)

        private readonly IOracleConnectionProvider _oracleConnection;
        private readonly IConfigService _configService;
        private readonly ICustItemDbContext _custItemDbContext;
        private readonly ICustItemUomDbContext _custItemUomDbContext;
        private readonly ICustomerAuxDbContext _customerAuxDbContext;
        private readonly ICustItemCatchWeightDbContext _custItemCatchWeightDbContext;
        private readonly IOrderDbContext _orderDbContext;
        private readonly IMapper _mapper;

        #endregion

        #region Public Method(S)

        public string GetVendorTrackedFlag(IDbConnection connection, string customerId, string item)
        {
            return _custItemDbContext.GetVendorTrackedFlag(connection, customerId, item);
        }

        public double GetItemCube(IDbConnection connection, string custId, string item, string uom)
        {
            var itemCube = default(double);
            try
            {
                if (connection != null && connection.State != ConnectionState.Open) { connection.Open(); }

                var custItemUomRequest = new CustItemUom { CustId = custId, Item = item, ToUom = uom };
                var custItemRequest = new CustItem { CustId = custId, Item = item };
                var uomCube = _custItemUomDbContext.GetCubeFromCustItemUom(connection, custItemUomRequest);
                if (uomCube.Equals(default(double)))
                {
                    var custItemCube = _custItemDbContext.GetCubeFromCustItem(connection, custItemRequest)
                        .FirstOrDefault();
                    if (custItemCube != null)
                    {
                        if (custItemCube.Cube < 1)
                        { return 1; }
                        if (custItemCube.BaseUom.Equals(uom))
                        {
                            itemCube = custItemCube.Cube;
                        }
                        else
                        {
                            custItemUomRequest.FromUom = custItemCube.BaseUom;
                            var factor = GetUotmQuantity(connection, custItemUomRequest);
                            if (new[] { -1, 0 }.Contains(factor))
                            { return 1; }
                            itemCube = custItemCube.Cube * factor;
                        }
                    }
                    else
                    {
                        return 1;
                    }
                }
                else
                {
                    itemCube = uomCube;
                }

                try
                {
                    var cartonFill = _customerAuxDbContext.GetCustomerSettings(connection, custId).FirstOrDefault();

                    if (cartonFill != null)
                    {
                        if (cartonFill.IsCartonFillPercentageFlag)
                        {
                            itemCube = cartonFill.CartonFillPercentage * itemCube / 1728;
                        }
                        else
                        {
                            itemCube = itemCube / 1728;
                        }
                    }
                }
                catch
                {
                    itemCube = itemCube / 1728;
                }

                if (itemCube > 999999.9999)
                { itemCube = 1; }
            }
            catch
            {
                itemCube = 1;
            }

            return itemCube;
        }


        public double GetItemWeight(IDbConnection connection, string custId, string item, string uom)
        {
            double itemWeight = 0;
            try
            {
                if (_custItemDbContext.GetItemCatchWeightFlag(connection, custId, item))
                {
                    itemWeight = GetItemAverageWeight(connection, uom, custId, item);
                    if (itemWeight > 0)
                    {
                        return itemWeight;
                    }
                }

                var custItemUomRequest = ToCustItemUom(null, uom, custId, item, 0);
                var uomWeight = _custItemUomDbContext.GetWeightFromCustItemUom(connection, custItemUomRequest);
                if (uomWeight.Equals(0))
                {
                    var custItemRequest = _mapper.Map<CustItem>(custItemUomRequest);
                    var custItemWeight = _custItemDbContext.GetWeightFromCustItem(connection, custItemRequest)
                        .FirstOrDefault();
                    if (custItemWeight != null)
                    {
                        if (string.Equals(uom, custItemWeight.BaseUom, StringComparison.CurrentCultureIgnoreCase))
                        {
                            itemWeight = custItemWeight.weight;
                        }
                        else
                        {
                            custItemUomRequest.FromUom = custItemWeight.BaseUom;
                            var factor = GetUotmQuantity(connection, custItemUomRequest);
                            if (new[] { -1, 0 }.Contains(factor))
                                return 1;
                            itemWeight = custItemWeight.weight * factor;
                        }
                    }
                    else
                    {
                        return 1;
                    }
                }
                else
                {
                    itemWeight = uomWeight;
                }

                if (itemWeight > 999999.9999)
                {
                    itemWeight = 1;
                }
            }
            catch
            {
                itemWeight = 1;
            }

            return itemWeight;
        }


        /// <summary>
        ///     This function returns the avg weight of 1 in_uom of a catch weight item. Return info from custitemcatchweight
        ///     table.
        /// </summary>
        /// <returns>
        ///     (number of baseuom in 1 in_uom) x (cwt.totalweight/cwt.totalqty) (i.e. need to find baseuom qty in 1 in_uom),
        ///     or cwt.totalweight if totalqty is 0
        /// </returns>
        public double GetItemAverageWeight(IDbConnection connection, string uom, string custId, string item)
        {
            double averageWeight = 0;
            try
            {
                var custItemCatchWeight = _custItemCatchWeightDbContext.GetCustItemCatchWeight(connection, custId, item)
                    .FirstOrDefault();

                if (custItemCatchWeight != null && custItemCatchWeight.Qty > 0)
                {
                    var custItemUomRequest = ToCustItemUom(custItemCatchWeight.Uom, uom, custId, item, 1);
                    var factor = GetUotmQuantity(connection, custItemUomRequest);
                    if (factor != -1)
                    { averageWeight = factor * custItemCatchWeight.Weight / custItemCatchWeight.Qty; }
                }
            }
            catch
            {
                averageWeight = 0;
            }

            return averageWeight;
        }


        public int GetUotmQuantity(IDbConnection connection, CustItemUom custItemRequest)
        {
            var custItemUomResponse = _custItemUomDbContext.GetCustItemUomByUom(connection, custItemRequest).ToList();

            return UomtoUom(custItemUomResponse, custItemRequest.CustId, custItemRequest.Item, 1,
                custItemRequest.FromUom, custItemRequest.ToUom, string.Empty, 1);
        }

        /// <summary>
        ///     This function will return the IATAPRIMARYCHEMCODE value from either ORDERDTL or CUSTITEM, depending on the setting
        ///     of CUSTOMER_AUX.USEITEMUNCODEFROMORDER_YN.
        /// </summary>
        /// <returns>Function returns NULL if IN_ITEM is NULL or in case of error.</returns>
        public string GetItemHazmatIndicator(IDbConnection connection, string custId, long orderId, int shipId)
        {
            string hazmatIndicator;

            try
            {
                if (string.IsNullOrWhiteSpace(custId) || orderId.Equals(0) || shipId.Equals(0))
                { return null; }

                var lstCustomerAux = _customerAuxDbContext.GetCustomerSettings(connection, custId).ToList();
                if (!lstCustomerAux.Any() || lstCustomerAux.FirstOrDefault() == null)
                { return null; }
                var customerAux = lstCustomerAux.First();

                if (customerAux.IsUseItemUncodeFromOrder)
                {
                    var order = _orderDbContext.GetHazmatFlag(connection, orderId, shipId);

                    if (string.IsNullOrWhiteSpace(order))
                    { return null; }
                    hazmatIndicator = order;
                }
                else
                {
                    hazmatIndicator = null;
                }
            }
            catch
            {
                hazmatIndicator = null;
            }

            return hazmatIndicator;
        }

        public string GetItemUnCode(IDbConnection connection, string custId, string item, long orderId, int shipId)
        {
            string iataPrimaryChemCode;
            try
            {
                if (string.IsNullOrWhiteSpace(custId) || string.IsNullOrWhiteSpace(item) || orderId.Equals(0) ||
                    shipId.Equals(0))
                { return null; }

                var lstCustomerAux = _customerAuxDbContext.GetCustomerSettings(connection, custId).ToList();
                if (!lstCustomerAux.Any() || lstCustomerAux.FirstOrDefault() == null)
                { return null; }
                var customerAux = lstCustomerAux.First();
                if (customerAux.IsUseItemUncodeFromOrder)
                {
                    var order = _orderDbContext.GetIataPrimaryChemCode(connection, item, orderId, shipId).ToList();

                    if (!order.Any() || order.FirstOrDefault() == null)
                    { return null; }
                    iataPrimaryChemCode = order.First().IataPrimaryChemCode;
                }
                else
                {
                    var custItem = _custItemDbContext.GetIataprimaryChemCode(connection, custId, item).ToList();

                    if (!custItem.Any() || custItem.FirstOrDefault() == null)
                    { return null; }
                    iataPrimaryChemCode = custItem.First().IataprimaryChemCode;
                }
            }
            catch
            {
                iataPrimaryChemCode = null;
            }

            return iataPrimaryChemCode;
        }

        public CustItemUom ToCustItemUom(string fromUom, string toUom, string custId, string item, int quantity)
        {
            return new CustItemUom { CustId = custId, Item = item, FromUom = fromUom, ToUom = toUom, Qty = quantity };
        }

        public string GetNextHighUom(IDbConnection connection, string custId, string item)
        {
            return _custItemUomDbContext.GetNextHighUom(connection, custId, item);
        }

        public string GetPutwallExemptFlag(IDbConnection connection, string custId, string item)
        {
            return _customerAuxDbContext.GetPutwallExemptFlag(connection, custId, item);
        }

        public bool GetCartonizationFlag(IDbConnection connection, string custId)
        {
            var lstCustomerAux = _customerAuxDbContext.GetCustomerSettings(connection, custId).ToList();
            if (!lstCustomerAux.Any() || lstCustomerAux.FirstOrDefault() == null)
            {
                return false;
            }
            var customerAux = lstCustomerAux.First();
            return customerAux.IsCartonEnabled;

        }

        public int GetPutwallCount(IDbConnection connection, string custId, string facility)
        {
            return _customerAuxDbContext.GetPutwallCount(connection, custId, facility);
        }

        public bool GetAllocRuleCount(IDbConnection connection, string custId, string facility, string item, string allocNeed)
        {
            return _customerAuxDbContext.GetAllocRuleCount(connection, custId, facility, item, allocNeed) > 0;
        }

        #endregion

        #region Private Method(S)

        private int UomtoUom(List<CustItemUom> custItemUom, string custId, string item, int qty, string fromUom,
            string toUom, string skips, int level)
        {
            var uomQuantity = -1;
            string vFromUom;
            string vSkips;

            if (level >= 10)
            {
                uomQuantity = -1;
            }
            else if (string.Equals(fromUom, toUom, StringComparison.CurrentCultureIgnoreCase))
            {
                uomQuantity = qty;
            }
            else if (skips.IndexOf("|" + fromUom + "|", StringComparison.OrdinalIgnoreCase) >= 0)
            {
                uomQuantity = -1;
            }
            else
            {
                vSkips = (skips ?? "|") + fromUom + "|";

                custItemUom.ForEach(crec =>
                {
                    if (string.Equals(crec.FromUom, fromUom, StringComparison.CurrentCultureIgnoreCase))
                    {
                        uomQuantity = qty * crec.Qty;
                        vFromUom = crec.ToUom;
                    }
                    else
                    {
                        uomQuantity = qty / crec.Qty;
                        vFromUom = crec.FromUom;
                    }

                    if (string.Equals(crec.ToUom, toUom, StringComparison.CurrentCultureIgnoreCase))
                        return;
                    uomQuantity = UomtoUom(custItemUom, custId, item, uomQuantity, vFromUom, toUom, vSkips, level + 1);
                    if (!uomQuantity.Equals(-1))
                    { return; }
                });
            }

            return uomQuantity;
        }



        #endregion

        public List<CustItemUom> GetItemByUom(IDbConnection connection, string custId, string item, int qty,string BaseUom)
        {
            List<CustItemUom> rtnCustItemUoms = new List<CustItemUom>();
            var custItemUoms = _custItemUomDbContext.GetCustItemAllUomByUom(connection, custId, item);

            foreach (var custItemUom in custItemUoms)
            {
                custItemUom.BaseQty = GetQtyInBase(custItemUoms.ToList(), BaseUom, custItemUom.ToUom);
            }

            foreach (var custItemUom in custItemUoms.OrderByDescending(c => c.BaseQty))
            {
                if (qty >= custItemUom.BaseQty)
                {
                    var rtnCustItemUom = new CustItemUom();
                    rtnCustItemUom.Qty = qty / custItemUom.BaseQty;
                    rtnCustItemUom.BaseQty = rtnCustItemUom.Qty * custItemUom.BaseQty;
                    qty = qty % custItemUom.BaseQty;
                    rtnCustItemUom.ToUom = custItemUom.ToUom;
                    rtnCustItemUoms.Add(rtnCustItemUom);
                }
            }

            if (qty > 0)
            {
                var rtnCustItemUom = new CustItemUom();
                rtnCustItemUom.Qty = qty;
                rtnCustItemUom.BaseQty = qty;
                rtnCustItemUom.ToUom = BaseUom;
                rtnCustItemUoms.Add(rtnCustItemUom);
            }

            return rtnCustItemUoms;
        }

        private int GetQtyInBase(List<CustItemUom> custItemUoms, string BaseUom, string Uom)
        {
            var custItemUom = custItemUoms.FirstOrDefault(u => u.ToUom == Uom);
            if (custItemUom.FromUom == null)
            {
                return 1;
            }
            else if (custItemUom.FromUom == BaseUom)
            {
                return custItemUom.Qty;
            }
            else
            {
                return GetQtyInBase(custItemUoms, BaseUom, custItemUom.FromUom) * custItemUom.Qty;
            }
        }
    }
}