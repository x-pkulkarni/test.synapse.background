﻿// <copyright file="RegenerateBatchAggregateInitializer.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-08-06</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-06</lastchangeddate>

using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Aggregates.Validation;
using Synapse.Backgrounds.Core.Aggregates.Validation.Interface;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Aggregates.Initializer
{
    public class RegenerateBatchAggregateInitializer : IRegenerateBatchAggregateInitializer
    {
        public IValidator<OrderValidator> Validator { get; }
        public IBatchTaskDbContext BatchTaskDbContext { get; }
        public IPlateDbContext PlateDbContext { get; }
        public ISubTaskDbContext SubTaskDbContext { get; }
        public ITaskDbContext TaskDbContext { get; }
        public IWaveDbContext WaveDbContext { get; }
        public IOrderDbContext OrderDbContext { get; }
        public IItemAggregate ItemAggregate { get; }
        public IBatchTaskAggregate BatchTaskAggregate { get; }
        public IOrderAggregate OrderAggregate { get; }
        public ICustomerDbContext CustomerDbContext { get; }
        public ICustItemDbContext CustItemDbContext { get; }
        public ICustItemAggregate CustItemAggregate { get; }
        public ICartonService CartonService { get; }
        public IWaveAggregate WaveAggregate { get; set; }

        public RegenerateBatchAggregateInitializer(IValidator<OrderValidator> validator,
            IBatchTaskDbContext batchTaskDbContext, IPlateDbContext plateDbContext, ISubTaskDbContext subTaskDbContext, 
            ITaskDbContext taskDbContext, IWaveDbContext waveDbContext, IOrderDbContext orderDbContext, 
            IItemAggregate itemAggregate, IBatchTaskAggregate batchTaskAggregate, IOrderAggregate orderAggregate, ICustomerDbContext customerDbContext,
            ICustItemDbContext custItemDbContext, ICustItemAggregate custItemAggregate, ICartonService cartonService, IWaveAggregate waveAggregate)
        {
            Validator = validator;
            BatchTaskDbContext = batchTaskDbContext;
            PlateDbContext = plateDbContext;
            SubTaskDbContext = subTaskDbContext;
            TaskDbContext = taskDbContext;
            WaveDbContext = waveDbContext;
            OrderDbContext = orderDbContext;
            ItemAggregate = itemAggregate;
            BatchTaskAggregate = batchTaskAggregate;
            OrderAggregate = orderAggregate;
            CustomerDbContext = customerDbContext;
            CustItemDbContext = custItemDbContext;
            CustItemAggregate = custItemAggregate;
            CartonService = cartonService;
            WaveAggregate = waveAggregate;
        }
    }
}