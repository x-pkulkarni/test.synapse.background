﻿// <copyright file="SubTaskAggregateInitializer.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-07-25</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-07-25</lastchangeddate>

using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Aggregates.Initializer
{
    public class SubTaskAggregateInitializer : ISubTaskAggregateInitializer
    {
        #region Public Member(S)

        public IBatchTaskDbContext BatchTaskDbContext { get; }
        public ITaskService TaskService { get; }
        public ICartonService CartonService { get; }
        public ISubTaskDbContext SubTaskDbContext { get; }
        public ICatchWeightAggregate CatchWeightDomain { get; }
        public ICustItemAggregate CustItem { get; }
        public ICartonGroupDbContext CartonGroupDbContext { get; }
        public ILaborStandardAggregate LaborStandardDoamin { get; }
        public IPlateAggregate PlateAggregate { get; }
        public ICartonizeDbContext CartonizeDbContext { get; }
        public IShippingPlateDbContext ShippingPlateDbContext { get; }
        public ICatchWeightAggregate CatchWeightAggregate { get; }
        public ICustItemAggregate CustItemAggregate { get; }

        #endregion

        #region Constructor(S)

        public SubTaskAggregateInitializer(IBatchTaskDbContext batchTaskDbContext, ITaskService taskService,
            ISubTaskDbContext subTaskDbContext, ICartonService cartonService,
            ICatchWeightAggregate catchWeightDomain, ICustItemAggregate custItem,
            ICartonGroupDbContext cartonGroupDbContext,
            ILaborStandardAggregate laborStandardDoamin, IPlateAggregate plateAggregate, ICartonizeDbContext cartonizeDbContext,
            IShippingPlateDbContext shippingPlateDbContext, ICatchWeightAggregate catchWeightAggregate, ICustItemAggregate custItemAggregate)
        {
            BatchTaskDbContext = batchTaskDbContext;
            TaskService = taskService;
            CartonService = cartonService;
            SubTaskDbContext = subTaskDbContext;
            CatchWeightDomain = catchWeightDomain;
            CustItem = custItem;
            CartonGroupDbContext = cartonGroupDbContext;
            LaborStandardDoamin = laborStandardDoamin;
            PlateAggregate = plateAggregate;
            CartonizeDbContext = cartonizeDbContext;
            ShippingPlateDbContext = shippingPlateDbContext;
            CatchWeightAggregate = catchWeightAggregate;
            CustItemAggregate = custItemAggregate;
        }

        #endregion
    }
}