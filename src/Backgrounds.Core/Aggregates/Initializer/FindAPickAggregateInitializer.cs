﻿// <copyright file="FindAPickAggregateInitializer.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-24</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-24</lastchangeddate>

using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Aggregates.Initializer
{
    public class FindAPickAggregateInitializer : IFindAPickAggregateInitializer
    {
        #region Public Member(S)

        public IPickDbContext PickDbContext { get; }
        public IBatchTaskAggregate BatchTaskAggregate { get; }

        public IShippingPlateDbContext ShippingPlateDbContext { get; }
        public ILocationDbContext LocationDbContext { get; }
        public ITaskService TaskDomain { get; }
        public ISubTaskAggregate SubTaskAggregate { get; }
        public ICustItemAggregate CustItemAggregate { get; }
        public ICatchWeightAggregate CatchWeightAggregate { get; }
        public IPlateAggregate PlateAggregate { get; }

        #endregion

        #region Constructor(S)
        public FindAPickAggregateInitializer(IPickDbContext pickDbContext,
            IBatchTaskAggregate batchTaskAggregate,
            IShippingPlateDbContext shippingPlateDbContext, ILocationDbContext locationDbContext,
            ITaskService taskDomain, ISubTaskAggregate subTaskAggregate,
            ICustItemAggregate custItemAggregate,
            ICatchWeightAggregate catchWeightAggregate, IPlateAggregate plateAggregate)
        {
            PickDbContext = pickDbContext;
            BatchTaskAggregate = batchTaskAggregate;
            ShippingPlateDbContext = shippingPlateDbContext;
            LocationDbContext = locationDbContext;
            TaskDomain = taskDomain;
            SubTaskAggregate = subTaskAggregate;
            CustItemAggregate = custItemAggregate;
            CatchWeightAggregate = catchWeightAggregate;
            PlateAggregate = plateAggregate;
        }
        #endregion

    }
}