﻿// <copyright file="WaveDomainInitializer.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-02-08</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using System;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Aggregates.Validation;
using Synapse.Backgrounds.Core.Aggregates.Validation.Interface;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Aggregates.Initializer
{
    public class WaveAggregateInitializer : IWaveAggregateInitializer
    {
        #region Constructor(S)

        public WaveAggregateInitializer( IWaveDbContext waveDbContext,
            Lazy<IOrderAggregate> orderAggregate, IValidator<WaveValidator> validator)
        {
            WaveDbContext = waveDbContext;
            OrderAggregate = orderAggregate;
            Validator = validator;
        }

        #endregion

        #region Public Member(S)
        
        public IWaveDbContext WaveDbContext { get; }
        public Lazy<IOrderAggregate> OrderAggregate { get; }
        public IValidator<WaveValidator> Validator { get; }

        #endregion
    }
}