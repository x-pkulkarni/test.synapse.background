﻿// <copyright file="InfrastructureInitializer.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-15</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-15</lastchangeddate>

using System;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;

namespace Synapse.Backgrounds.Core.Aggregates.Initializer
{
    public class InfrastructureInitializer : IInfrastructureInitializer
    {

        #region Constructor(S)

        public InfrastructureInitializer(Lazy<IOracleConnectionProvider> oracleConnection, Lazy<IConfigService> configService,
            Lazy<ILoggerService> loggerService, IParcelStagingDbContext parcelStagingDbContext)
        {
            OracleConnection = oracleConnection;
            ConfigService = configService;
            LoggerService = loggerService;
            ParcelStagingDbContext = parcelStagingDbContext;
        }

        #endregion

        #region Public Member(S)

        public Lazy<IOracleConnectionProvider> OracleConnection { get; }
        public Lazy<IConfigService> ConfigService { get; }
        public Lazy<ILoggerService> LoggerService { get; }
        public IParcelStagingDbContext ParcelStagingDbContext { get; }
       #endregion
    }
}