﻿// <copyright file="ItemDomainInitializer.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-02-08</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using System;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Profile;

namespace Synapse.Backgrounds.Core.Aggregates.Initializer
{
    public class ItemAggregateInitializer : IItemAggregateInitializer
    {
        #region Constructor(S)

        public ItemAggregateInitializer( IOrderDbContext orderDbContext,
            MappingEngine<ItemAggregateMapperProfile> mapperEngine,
            ITaskAggregate taskAggregate, ICommitmentDbContext commitmentDbContext,
            ICustItemDbContext custItemDbContext,
            ICustomerDbContext customerDbContext, IItemDemandDbContext itemDemandDbContext,
            Lazy<IFindAPickAggregate> findAPickAggregate)
        {
            OrderDbContext = orderDbContext;
            MapperEngine = mapperEngine;
            TaskAggregate = taskAggregate;
            CommitmentDbContext = commitmentDbContext;
            CustItemDbContext = custItemDbContext;
            CustomerDbContext = customerDbContext;
            ItemDemandDbContext = itemDemandDbContext;
            FindAPickAggregate = findAPickAggregate;
        }

        #endregion

        #region Public Member(S)
        public IOrderDbContext OrderDbContext { get; }
        public MappingEngine<ItemAggregateMapperProfile> MapperEngine { get; }
        public ITaskAggregate TaskAggregate { get; }
        public ICommitmentDbContext CommitmentDbContext { get; }
        public ICustItemDbContext CustItemDbContext { get; }
        public ICustomerDbContext CustomerDbContext { get; }
        public IItemDemandDbContext ItemDemandDbContext { get; }
        public Lazy<IFindAPickAggregate> FindAPickAggregate { get; }

        #endregion
    }
}