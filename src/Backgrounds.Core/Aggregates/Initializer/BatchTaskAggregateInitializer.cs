﻿// <copyright file="BatchTaskAggregateInitializer.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-07-25</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-07-25</lastchangeddate>

using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Aggregates.Initializer
{
    public class BatchTaskAggregateInitializer : IBatchTaskAggregateInitializer
    {
        #region Public Member(S)

        public IBatchTaskDbContext BatchTaskDbContext { get; }
        public ISubTaskAggregate SubTaskDomain { get; }
        public IPlateAggregate PlateAggregate { get; }
        public ITaskService TaskService { get; }
        public ILocationDbContext LocationDbContext { get; }
        public ICatchWeightAggregate CatchWeightDomain { get; }
        public ICustItemAggregate CustItem { get; }
        public ILaborStandardAggregate LaborStandardDoamin { get; }

        #endregion

        #region Constructor(S)

        public BatchTaskAggregateInitializer(IBatchTaskDbContext batchTaskDbContext, ISubTaskAggregate subTaskDomain,
            IPlateAggregate plateAggregate, ITaskService taskService, ILocationDbContext locationDbContext,
            ICatchWeightAggregate catchWeightDomain, ICustItemAggregate custItem,
            ILaborStandardAggregate laborStandardDoamin)
        {
            BatchTaskDbContext = batchTaskDbContext;
            SubTaskDomain = subTaskDomain;
            PlateAggregate = plateAggregate;
            TaskService = taskService;
            LocationDbContext = locationDbContext;
            CatchWeightDomain = catchWeightDomain;
            CustItem = custItem;
            LaborStandardDoamin = laborStandardDoamin;
        }

        #endregion
    }
}