﻿// <copyright file="TaskAggregateInitializer.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-07-25</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-07-25</lastchangeddate>

using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;

namespace Synapse.Backgrounds.Core.Aggregates.Initializer
{
    public class TaskAggregateInitializer :ITaskAggregateInitializer
    {
        #region Public Member(S)

        public IBatchTaskDbContext BatchTaskDbContext { get; }
        public ITaskService TaskDomain { get; }
        public IPlateAggregate PlateAggregate { get; }
        public ICommitmentDbContext CommitmentsDbContext { get; }
        public ICustItemAggregate CustItem { get; }
        public ISubTaskDbContext SubTaskDbContext { get; }

        #endregion

        #region Constructor(S)

        public TaskAggregateInitializer(ISubTaskDbContext subTaskDbContext, IBatchTaskDbContext batchTaskDbContext,
            ITaskService taskDomain, IPlateAggregate plateAggregate, ICommitmentDbContext commitmentsDbContext,
            ICustItemAggregate custItem)
        {
            BatchTaskDbContext = batchTaskDbContext;
            TaskDomain = taskDomain;
            PlateAggregate = plateAggregate;
            CommitmentsDbContext = commitmentsDbContext;
            CustItem = custItem;
            SubTaskDbContext = subTaskDbContext;
        }

        #endregion
    }
}