﻿// <copyright file="OrderDomainInitializer.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-15</lastchangeddate>

using System;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Aggregates.Validation;
using Synapse.Backgrounds.Core.Aggregates.Validation.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;

namespace Synapse.Backgrounds.Core.Aggregates.Initializer
{
    public class OrderAggregateInitializer : IOrderAggregateInitializer
    {
        #region Constructor(S)

        public OrderAggregateInitializer(IOrderDbContext orderDbContext,
            Lazy<IItemAggregate> itemDomain, IValidator<OrderValidator> validator,
            IBatchTaskAggregate batchTaskDomain,
            ISubTaskAggregate subTaskDomain,
            Lazy<IOrderStatusAggregate> orderStatusAggregate, ITaskService taskService, ICartonService cartonService,
        IFindAPickAggregate findAPickAggregate, IPlateAggregate plateAggregate,ICustItemAggregate custItemAggregate, 
            ICustomerAuxDbContext customerAuxDbContext, Lazy<ILoggerService> loggerService,
            IFelAggregate felAggregate,IWaveAggregate waveAggregate, IShippingPlateDbContext shippingPlateDbContext)
        {
            OrderDbContext = orderDbContext;
            ItemDomain = itemDomain;
            Validator = validator;
            BatchTaskAggregate = batchTaskDomain;
            SubTaskAggregate = subTaskDomain;
            OrderStatusAggregate = orderStatusAggregate;
            TaskService = taskService;
            CartonService = cartonService;
            FindAPickAggregate = findAPickAggregate;
            PlateAggregate = plateAggregate;
            CustItemAggregate = custItemAggregate;
            CustomerAuxDbContext = customerAuxDbContext;
            LoggerService = loggerService;
            FelAggregate = felAggregate;
            WaveAggregate = waveAggregate;
            ShippingPlateDbContext = shippingPlateDbContext;
        }

        #endregion

        #region Public Member(S)

        public IValidator<OrderValidator> Validator { get; }
        public IOrderDbContext OrderDbContext { get; }
        public Lazy<IItemAggregate> ItemDomain { get; }
        public IBatchTaskAggregate BatchTaskAggregate { get; }
        public ISubTaskAggregate SubTaskAggregate { get; }
        public ITaskService TaskService { get; }
        public ICartonService CartonService { get; }
        public IFindAPickAggregate FindAPickAggregate { get; }
        public ICustItemAggregate CustItemAggregate { get; }
        public IPlateAggregate PlateAggregate { get; }
        public Lazy<IOrderStatusAggregate> OrderStatusAggregate { get; }
        public ICustomerAuxDbContext CustomerAuxDbContext { get; }
        public Lazy<ILoggerService> LoggerService { get; }
        public IFelAggregate FelAggregate { get; }
        public IWaveAggregate WaveAggregate { get; set; }
        public IShippingPlateDbContext ShippingPlateDbContext { get; set; }

        #endregion
    }
}