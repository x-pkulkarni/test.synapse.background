﻿// <copyright file="OrderStatusAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-15</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-15</lastchangeddate>

using System.Data;
using System.Linq;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Aggregates
{
    public class OrderStatusAggregate : IOrderStatusAggregate
    {

        #region Constructor(S)
        public OrderStatusAggregate(IOrderDbContext orderDbContext, IPlateAggregate plateAggregate,
            IShippingPlateDbContext shippingPlateDbContext, ICommitmentDbContext commitmentDbContext,
            ILoadDbContext loadDbContext)
        {
            _orderDbContext = orderDbContext;
            _plateAggregate = plateAggregate;
            _shippingPlateDbContext = shippingPlateDbContext;
            _commitmentDbContext = commitmentDbContext;
            _loadDbContext = loadDbContext;
        }
        #endregion

        #region Private Member(S)

        private readonly IOrderDbContext _orderDbContext;
        private readonly IPlateAggregate _plateAggregate;
        private readonly IShippingPlateDbContext _shippingPlateDbContext;
        private readonly ICommitmentDbContext _commitmentDbContext;
        private readonly ILoadDbContext _loadDbContext;

        #endregion

        #region Public Member(S)

        public void UpdateOrderAndPlateInfo(IDbConnection connection, OrderUnreleaseRequest request, long? currentWave,
            long? waveToUnrelease)
        {
            //Reset Order status
            UpdateOrderHeaderToCommit(connection, request.Order, request.UserId, waveToUnrelease);


            //If this order was assigned a load id, Update the load status.
            if (request.Order.LoadNo != null)
            {
                if (!request.Order.LoadNo.Equals(0) && _orderDbContext
                        .GetOrderCountWithAssocaitedLoad(connection, request.Order.LoadNo).Equals(0))
                    _loadDbContext.UpdateLoadStatusToPlanned(connection, request.UserId, request.Order.LoadNo);
            }

            //Clean up fromlpid plate records associated with master or carton plates where there aren't any associated FULL/PARTIAL child plates for this order/shipid
            var masterplates = _shippingPlateDbContext
                .GetMasterAndCartonByOrderId(connection, request.Order.OrderId, request.Order.ShipId).ToList();
            masterplates.ForEach(p => { _plateAggregate.DeletePlateByLpId(connection, p.FromLpId); });

            //Clean up empty master/carton shipping plates for given order where the master/carton has no full or partial children shipping plates associated.
            _shippingPlateDbContext.DeleteMasterAndCartonByOrderId(connection, request.Order.OrderId,
                request.Order.ShipId);

            //Reset PTLLINE field in commitments
            _commitmentDbContext.ResetPlateLine(connection, request.Order.OrderId, request.Order.ShipId);
        }


        /// <summary>
        ///     This method is called to set header to Released status and Released commit status, as we have released all lines
        ///     under given order header
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="orderHeader"></param>
        /// <param name="waveReleaseRequest"></param>
        public void UpdateOrderHeaderStatusToReleased(IDbConnection connection, OrderHeader orderHeader,
            ICommand waveReleaseRequest)
        {
            orderHeader.OrderStatus = Constants.OrderStatus.Released;
            orderHeader.CommitStatus = Enums.CommitStatus.Released.ToString("D");
            orderHeader.LastUser = waveReleaseRequest.UserId;

            _orderDbContext.UpdateByOrderId(connection, orderHeader);
        }

        public void UpdateLoadStatus(IDbConnection connection, int? loadNo, string userId, int? stopNo)
        {
            if (loadNo.Equals(0)) return;

            _loadDbContext.UpdateStatusByLoadNo(connection, userId, loadNo, Constants.LoadStatus.FullyReleased);
            _loadDbContext.UpdateStopStatusByLoadNo(connection, userId, loadNo, Constants.LoadStatus.FullyReleased,
                stopNo);
        }

        public void AddOrderHistory(IDbConnection connection, OrderHeader order, string userId, string requestType)
        {
            var orderRequest = "Order " + (requestType.Equals(Integration.Enums.RequestType.RELWAV.ToString())
                                   ? "Released"
                                   : "Unreleased");
            var history = new OrderHistory
            {
                Action = orderRequest,
                Message = orderRequest + " Wave:" + order.Wave,
                OrderId = order.OrderId,
                ShipId = order.ShipId,
                UserId = userId
            };
            _orderDbContext.AddOrderHistory(connection, history);
        }

        #endregion

        #region Private Member(S)
        private void UpdateOrderHeaderToCommit(IDbConnection connection, OrderHeader orderHeader, string userId,
            long? wave)
        {
            orderHeader.OrderStatus = Constants.OrderStatus.Committed;
            orderHeader.CommitStatus = Enums.CommitStatus.Selected.ToString("D");
            orderHeader.LastUser = userId;
            orderHeader.Wave = wave.Value;
            _orderDbContext.UpdateByOrderId(connection, orderHeader);
        }
        #endregion
    }
}