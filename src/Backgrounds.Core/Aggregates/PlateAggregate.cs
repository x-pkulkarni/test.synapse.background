﻿// <copyright file="PlateAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-14</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-14</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using System.Linq;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Aggregates
{
    public class PlateAggregate : IPlateAggregate
    {

        #region Constructor(S)

        public PlateAggregate(IPlateDbContext plateDbContext, IShippingPlateDbContext shippingPlateDbContext)
        {
            _plateDbContext = plateDbContext;
            _shippingPlateDbContext = shippingPlateDbContext;
        }

        #endregion

        #region Private Member(S)

        private readonly IPlateDbContext _plateDbContext;
        private readonly IShippingPlateDbContext _shippingPlateDbContext;

        #endregion

        #region Public Member(S)

        public bool UpdatePlateQtyAsked(IDbConnection connection, string lastUser, int baseQty, string lpId)
        {
            var request = new UpdateQtyTaskedRequest
            {
                LastUser = lastUser,
                QtyAsked = baseQty,
                LpId = lpId
            };
            return _plateDbContext.UpdateQtyTasked(connection, request);
        }

        public IList<Plate> GetPlateByLpId(IDbConnection connection, string lpId)
        {
            return _plateDbContext.GetPlateByLpId(connection, lpId).ToList();
        }

        public bool DeletePlateByLpId(IDbConnection connection, string lpId)
        {
            return _plateDbContext.DeletePlateByLpId(connection, lpId);
        }

        public int GetPendingPlatePicksLine(IDbConnection connection, PendingPickQtyRequest pendingPlatePickQtyRequest)
        {
            return _shippingPlateDbContext.GetPendingPlatePicksLine(connection, pendingPlatePickQtyRequest);
        }

        public int GetPendingPicksVendorItem(IDbConnection connection, PendingPickQtyRequest pendingPlatePickQtyRequest)
        {
            return _shippingPlateDbContext.GetPendingPicksVendorItem(connection, pendingPlatePickQtyRequest);
        }

        public int GetPendingPicksItem(IDbConnection connection, PendingPickQtyRequest pendingPlatePickQtyRequest)
        {
            return _shippingPlateDbContext.GetPendingPicksItem(connection, pendingPlatePickQtyRequest);
        }
        
        public bool DeleteShippingPlateByLpId(IDbConnection connection, string lpId)
        {
            return _shippingPlateDbContext.DeleteByLpId(connection, lpId);
        }
        #endregion
    }
}