﻿// <copyright file="OrderValidator.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-18</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-18</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Aggregates.Validation.Interface;
using Synapse.Backgrounds.Core.Data;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;

namespace Synapse.Backgrounds.Core.Aggregates.Validation
{
    public class OrderValidator : IValidator<OrderValidator>
    {
        #region Private Member(S)
        
        private readonly Lazy<ILoggerService> _loggerService;
        private readonly IOrderDbContext _orderDbContext;
        private readonly ICarrierStageLocationDbContext _carrierStageLocationDbContext;
        private readonly Dictionary<Integration.Enums.RequestType, Func<IDbConnection, ValidationContext, bool>> _validationFuncStack;

        #endregion

        #region Constructor(S)

        public OrderValidator(IInfrastructureInitializer infrastructureInitializer, IOrderDbContext orderDbContext,
            ICarrierStageLocationDbContext carrierDbContext)
        {
            _loggerService = infrastructureInitializer.LoggerService;
            _orderDbContext = orderDbContext;
            _carrierStageLocationDbContext = carrierDbContext;
            _validationFuncStack = new Dictionary<Integration.Enums.RequestType, Func<IDbConnection, ValidationContext, bool>>
            {
                {Integration.Enums.RequestType.RELWAV ,this.IsValidForRelease},
                {Integration.Enums.RequestType.UNRELWAV ,this.IsValidForUnrelease},
                {Integration.Enums.RequestType.GENLIPBP ,this.IsValidForBatchTaskRegeneration}
            };
        }

        #endregion

        #region Public Method(S)

        public bool IsValid(IDbConnection connection, ValidationContext request)
        {
           return  _validationFuncStack[request.Command.Type](connection, request);
        }

        public bool IsValidForRelease(IDbConnection connection, ValidationContext request)
        {
            if (!request.OrderDetails.Any())
            {
                _loggerService.Value.Warning($"Wave #{ request.Command.Wave }: No order not found!");
                return false;
            }

            var ordetail = request.OrderDetails.First();

            //Populate Orderdetails if FromFacility, CustomerId & Priority don't contain a value
            if (string.IsNullOrWhiteSpace(ordetail.FromFacility) || string.IsNullOrWhiteSpace(ordetail.CustomerId) ||
                string.IsNullOrWhiteSpace(ordetail.Priority))
            {
                _orderDbContext.ResetOrderDetails(connection, request.OrderHeader);
            }

            //Default the stage location, first try order header, then wave then curcarrierstageloc record
            if (string.IsNullOrWhiteSpace(request.OrderHeader.StageLoc))
            {
                if (!string.IsNullOrWhiteSpace(request.Waves.First().StageLocation))
                {
                    request.OrderHeader.StageLoc = request.Waves.First().StageLocation;
                }
                else
                {
                    var stageLoc = _carrierStageLocationDbContext.GetStageLocationByCarrier(connection,
                        request.OrderHeader.Carrier, request.OrderHeader.FromFacility, request.OrderHeader.ShipType);
                    if (!string.IsNullOrWhiteSpace(stageLoc))
                        request.OrderHeader.StageLoc = stageLoc;
                }
            }
            
            return true;
        }

        public bool IsValidForUnrelease(IDbConnection connection, ValidationContext request)
        {
            if (request.OrderHeader == null)
            {
                _loggerService.Value.Warning($"Wave #{ request.Command.Wave }: No order not found!");
                return false;
            }
            
            if (_orderDbContext.GetOrderCountBeyondReleased(connection, request.OrderHeader.Wave) > 0)
            {
                return false;
            }

            if (_orderDbContext.GetOrderCountWithAssocaitedPicks(connection, request.OrderHeader.Wave) > 0)
            {
                return false;
            }

            return true;
        }

        public bool IsValidForBatchTaskRegeneration(IDbConnection connection, ValidationContext request)
        {
            if (request.Command.TaskId == 0 || !new BatchTaskDbContext().ValidateTaskId(connection, request.Command.TaskId))
            {
                _loggerService.Value.Warning($"No task id found in the request!");
                return false;
            }
            return true;
        }
        
        #endregion
    }
}
