﻿// <copyright file="WaveValidator.cs" company="GEODIS">
// Copyright (c) 2017 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-18</lastchangeddate>

namespace Synapse.Backgrounds.Core.Aggregates.Validation
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using Aggregates.Interface;
    using Data.Interface;
    using Infrastructure.Logging.Interface;
    using Interface;
    using Model;

    public class WaveValidator : IValidator<WaveValidator>
    {
        #region Constructor(S)

        public WaveValidator(IInfrastructureInitializer infrastructureInitializer, IWaveDbContext waveDbContext,
            ITaskDbContext taskDbContext, ICustomerAuxDbContext customerAuxDbContext, IOrderDbContext orderDbContext)
        {
            _waveDbContext = waveDbContext;
            _taskDbContext = taskDbContext;
            _customerAuxDbContext = customerAuxDbContext;
            _orderDbContext = orderDbContext;
            _loggerService = infrastructureInitializer.LoggerService;
            _parcelStagingDbContext = infrastructureInitializer.ParcelStagingDbContext;
            _validationFuncStack = new Dictionary<string, Func<IDbConnection, ValidationContext, bool>>
            {
                {"Release", IsValidForRelease},
                {"Unrelease", IsValidForUnrelease}
            };
        }

        #endregion

        #region Private Member(S)

        private const string SingleCartonGroup = "SNGG";
        private readonly Lazy<ILoggerService> _loggerService;
        private readonly IWaveDbContext _waveDbContext;
        private readonly ICustomerAuxDbContext _customerAuxDbContext;
        private readonly IOrderDbContext _orderDbContext;
        private readonly ITaskDbContext _taskDbContext;
        private readonly IParcelStagingDbContext _parcelStagingDbContext;
        private readonly Dictionary<string, Func<IDbConnection, ValidationContext, bool>> _validationFuncStack;

        #endregion

        #region Public Method(S)

        public bool IsValid(IDbConnection connection, ValidationContext request)
        {
            return _validationFuncStack[request.Command.CommandText](connection, request);
        }

        public bool IsValidForRelease(IDbConnection connection, ValidationContext request)
        {
            if (!request.Waves.Any())
            {
                _loggerService.Value.Warning($"Wave #{request.Command.Wave} not found!");
                return false;
            }

            // Check for Invalid wave and status (other than Released 3 & Completed 4)
            if (new[] {Enums.WaveStatus.Released.ToString("D"), Enums.WaveStatus.Completed.ToString("D")}
                .Contains(request.Waves.First().Status))
            {
                _loggerService.Value.Warning(
                    $"Wave #{request.Command.Wave} is not valid for release.#{request.Command.CustomerId}");
                _waveDbContext.UpdateProcessingIndicatorStart(connection, request.Waves.First().Id);
                return false;
            }

            //determine if this wave is set for FEL(Front End Labeling).IF so we need to check a few things.
            if (!IsValidFel(connection, request))
            {
                return false;
            }


            //Default task priority, if not passed in.. to the taskpriority on the wave.  If wave is blank, set to 3 (default) 'Normal Priority'
            if (string.IsNullOrWhiteSpace(request.Command.TaskPriority))
            {
                request.Command.TaskPriority =
                    request.Waves.First().TaskPriority ?? Constants.TaskPriorities.Normal;
            }

            // If pick type not passed in, use picktype off wave
            if (string.IsNullOrWhiteSpace(request.Command.PickType) ||
                string.Equals(request.Command.PickType, "(none)", StringComparison.CurrentCultureIgnoreCase))
            {
                request.Command.PickType = request.Waves.First().PickType;
            }

            return true;
        }

        public bool IsValidForUnrelease(IDbConnection connection, ValidationContext request)
        {
            // Check for Invalid wave and status ( > 3 i.e., beyond released)
            if (!request.Waves.Any() || int.Parse(request.Waves.First().Status) != (int) Enums.WaveStatus.Released)
            {
                _loggerService.Value.Warning(
                    $"Wave #{request.Command.Wave} is not valid for Unrelease.#{request.Command.CustomerId}");
                return false;
            }

            //determine if this wave is set for FEL(Front End Labeling).IF so we need to check a few things.
            if (!request.Waves.First().IsFrontEndLabel)
            {
                return _taskDbContext.GetActiveTasksCount(connection, request.Waves.First().Id) <= 0;
            }

            if (_parcelStagingDbContext.GetRecordCountByWave(connection, request.Waves.First().Id))
            {
                _loggerService.Value.Warning(
                    $"Wave #{request.Command.Wave} Cannot be unreleased as it is still awaiting tracking information from connectship for some orders.");
                return false;
            }

            return _taskDbContext.GetActiveTasksCount(connection, request.Waves.First().Id) <= 0;
        }

        #endregion

        #region Private Method(S)

        private bool IsValidFel(IDbConnection connection, ValidationContext request)
        {
            
            if (!request.Waves.First().IsFrontEndLabel && !request.Waves.First().IsLtlFrontEndLabel)
            {
                return true;
            }

            var connectShipOrders =
                _orderDbContext.GetCShipOrderHeaderByWave(connection, request.Waves.First().Id);
            foreach (var connectShipOrder in connectShipOrders)
            {
                if (!connectShipOrder.IsMultiShip && !connectShipOrder.IsLtlFel)
                {
                    _loggerService.Value.Warning(
                        $"Order #{connectShipOrder.OrderId}-#{connectShipOrder.ShipId} is LTL, but customer #{connectShipOrder.CustomerId} is not setup for LTL front end labeling");
                    _waveDbContext.UpdateProcessingIndicatorStart(connection, request.Waves.First().Id);
                    return false;
                }

                if (connectShipOrder.IsFelConnectShip &&
                    !_waveDbContext.IsCustomerSetupForCShip(connection, request.Command.CustomerId))
                {
                    _loggerService.Value.Warning(
                        $"customer #{request.Command.CustomerId} is not setup for connectship labeling");
                    _waveDbContext.UpdateProcessingIndicatorStart(connection, request.Waves.First().Id);
                    return false;
                }

                if (!_waveDbContext.IsCustomerSetupForFel(connection, request.Command.CustomerId))
                {
                    _loggerService.Value.Warning($"customer #{request.Command.CustomerId} is not setup for FEL");
                    _waveDbContext.UpdateProcessingIndicatorStart(connection, request.Waves.First().Id);
                    return false;
                }
            }

            //FEL waves cannot have any items marked as hazardous.
            // --[PS] unless the customer / carrier is marked to allow hazmat on the custconnectship table.
            var custItems = _waveDbContext.GetItemsInWave(connection, request.Waves.First().Id);
            foreach (var item in custItems)
            {
                if (item.PickSetQty != 0)
                {
                    _loggerService.Value.Warning($"Item {item.Item} has a non-zero Pick Set Qty");
                    _waveDbContext.UpdateProcessingIndicatorStart(connection, request.Waves.First().Id);
                    return false;
                }

                var customerAux = _customerAuxDbContext.GetCustomerSettings(connection, item.CustomerId).ToList().FirstOrDefault();
                if (customerAux == null)
                {
                    return false;
                }

                if (CheckCarrierOverride(connection, item.CustomerId, item.CarrierCode, item.ShipToState,
                        item.ShipToCountryCode, customerAux)
                    && CheckSingleUnit(connection, request.Waves.First().Id, item.OrderId, item.ShipId, customerAux))
                {
                    _loggerService.Value.Warning($"Item {item.Item} is marked as hazardous");
                    _waveDbContext.UpdateProcessingIndicatorStart(connection, request.Waves.First().Id);
                    return false;
                }
            }

            return true;
        }

        private bool CheckCarrierOverride(IDbConnection connection, string custId, string code, string shipToState,
            string shipToCountryCode, CustomerAux customerAux)
        {
            var codeCnt = _waveDbContext.GetCustLookupCount(connection, custId, code);

            var hazmatDetail = _waveDbContext.GetCustLookupDtl(connection, custId, shipToCountryCode).FirstOrDefault();
            if (hazmatDetail == null)
                return false;
            
           return customerAux.IsIataCarrierOverride
                   && codeCnt > 0
                   && hazmatDetail.HazmatInValidSt.Contains(shipToState)
                   && hazmatDetail.HazmatCtCode.Contains(shipToCountryCode);
        }

        private bool CheckSingleUnit(IDbConnection connection, long waveId, long orderId, int shipId,
            CustomerAux customerAux)
        {
            var qtyEntered = 0;
            var orderCount = 0;

            var wave = _waveDbContext.GetWaveById(connection, waveId).FirstOrDefault();
            
            var lstOrderDetails = _orderDbContext.GetOrderDetailsById(connection, orderId, shipId).ToList();
            if (!lstOrderDetails.Any() || lstOrderDetails.FirstOrDefault() == null)
            {
                qtyEntered = 0;
                orderCount = 0;
            }
            else
            {
                orderCount = lstOrderDetails.Select(x => x.Item).Distinct().Count();
                qtyEntered = lstOrderDetails.Sum(x => x.QtyEntered);
            }

            return customerAux.IsIataSingleUnitOk
                   && (orderCount > 1 && wave.CartonGroup.Equals(SingleCartonGroup)
                       || qtyEntered == 1
                       || qtyEntered > 1 && wave.CartonGroup.Equals(SingleCartonGroup));
        }
        #endregion
    }
}