﻿// <copyright file="TaskAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-18</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-21</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Aggregates
{
    public class TaskAggregate : ITaskAggregate
    {
        #region Constructor(S)

        public TaskAggregate(ITaskAggregateInitializer taskAggregateInitializer)
        {
            _subTaskDbContext = taskAggregateInitializer.SubTaskDbContext;
            _batchTaskDbContext = taskAggregateInitializer.BatchTaskDbContext;
            _taskDomain = taskAggregateInitializer.TaskDomain;
            _plateAggregate = taskAggregateInitializer.PlateAggregate;
            _commitmentsDbContext = taskAggregateInitializer.CommitmentsDbContext;
            _custItem = taskAggregateInitializer.CustItem;

            _pendingPicksItemFuncStack = new Dictionary<string, Func<IDbConnection, PendingPickQtyRequest, int>>
            {
                {Constants.Flag.Yes, _plateAggregate.GetPendingPicksVendorItem},
                {Constants.Flag.No, _plateAggregate.GetPendingPicksItem}
            };
            _pendingBatchItemFuncStack = new Dictionary<string, Func<IDbConnection, PendingPickQtyRequest, int>>
            {
                {Constants.Flag.Yes, _batchTaskDbContext.GetPendingBatchVendorItem},
                {Constants.Flag.No, _batchTaskDbContext.GetPendingBatchItem}
            };
        }

        #endregion

        #region Private Member(S)

        private readonly ISubTaskDbContext _subTaskDbContext;
        private readonly IBatchTaskDbContext _batchTaskDbContext;
        private readonly ITaskService _taskDomain;
        private readonly IPlateAggregate _plateAggregate;
        private readonly ICommitmentDbContext _commitmentsDbContext;
        private readonly ICustItemAggregate _custItem;

        private readonly Dictionary<string, Func<IDbConnection, PendingPickQtyRequest, int>> _pendingPicksItemFuncStack;
        private readonly Dictionary<string, Func<IDbConnection, PendingPickQtyRequest, int>> _pendingBatchItemFuncStack;

        #endregion

        #region Public Member (S)

        public void CleanupInvalidTasksForRegenerateLine(IDbConnection connection, long wave, OrderDetail orderDetails)
        {
        }

        public int CalculateQtyRemain(IDbConnection connection, OrderDetail order)
        {
            var remainingQty = order.QtyCommit;

            var pendingPickQtyRequest = new PendingPickQtyRequest
            {
                OrderId = order.OrderId,
                OrderItem = order.Item,
                OrderLot = order.LotNumber,
                ShipId = order.ShipId
            };
            var pendingPicksQty = _plateAggregate.GetPendingPlatePicksLine(connection, pendingPickQtyRequest);
            var pendingBatchQty = _batchTaskDbContext.GetPendingBatchPicksLine(connection, pendingPickQtyRequest);
            remainingQty = remainingQty - pendingPicksQty - pendingBatchQty;
            remainingQty = remainingQty < 0 ? 0 : remainingQty;
            return remainingQty;
        }


        public int CalculateQtyForTaskGeneration(IDbConnection connection, OrderDetail orderDetail,
            OrderHeader orderHeader,
            Commitment commitment)
        {
            var qtyforTaskGen = orderDetail.QtyOrder;
            var pendingPickQtyRequest = new PendingPickQtyRequest
            {
                OrderId = orderDetail.OrderId,
                OrderItem = orderDetail.Item,
                OrderLot = orderDetail.LotNumber ?? "(none)",
                ShipId = orderDetail.ShipId,
                Item = commitment.Item,
                InvStatus = commitment.InvStatus,
                InventoryClass = commitment.InventoryClass
            };
            var vendorFlag =
                _custItem.GetVendorTrackedFlag(connection, orderHeader.CustomerId,
                    commitment.Item);
            var pendingPicksItem = _pendingPicksItemFuncStack[vendorFlag](connection, pendingPickQtyRequest);
            var pendingBatchItem = _pendingBatchItemFuncStack[vendorFlag](connection, pendingPickQtyRequest);

            qtyforTaskGen = qtyforTaskGen - pendingPicksItem - pendingBatchItem;
            qtyforTaskGen = qtyforTaskGen < 0 ? 0 : qtyforTaskGen;
            return qtyforTaskGen;
        }

        #endregion
    }
}