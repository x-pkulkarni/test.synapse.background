﻿// <copyright file="WaveAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-11</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-03-23</lastchangeddate>

using System;
using System.Data;
using System.Linq;
using System.Transactions;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Aggregates.Validation;
using Synapse.Backgrounds.Core.Aggregates.Validation.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;
using Task = System.Threading.Tasks.Task;

namespace Synapse.Backgrounds.Core.Aggregates
{
    public class WaveAggregate : IWaveAggregate
    {
        #region Constructor(S)

        public WaveAggregate(IInfrastructureInitializer infrastructureInitializer, IWaveAggregateInitializer waveAggregateInitializer, IFelAggregate FelAggregate)
        {
            _loggerService = infrastructureInitializer.LoggerService;
            _oracleConnection = infrastructureInitializer.OracleConnection;
            _configService = infrastructureInitializer.ConfigService;
            _waveDbContext = waveAggregateInitializer.WaveDbContext;
            _orderAggregate = waveAggregateInitializer.OrderAggregate;
            _felAggregate = FelAggregate;
            _validator = waveAggregateInitializer.Validator;
            _transactionScopeOption = new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted };
        }

        #endregion

        #region Private Member(S)
        private readonly Lazy<IOracleConnectionProvider> _oracleConnection;
        private readonly Lazy<IConfigService> _configService;
        private readonly IWaveDbContext _waveDbContext;
        private readonly Lazy<IOrderAggregate> _orderAggregate;
        private readonly IFelAggregate _felAggregate;
        private readonly Lazy<ILoggerService> _loggerService;
        private readonly IValidator<WaveValidator> _validator;
        private readonly TransactionOptions _transactionScopeOption;

        #endregion

        #region Public Method(S)

        public Task ExecuteReleaseWave(IDbConnection connection, ICommand releaseCommand)
        {
            var lstWaves = _waveDbContext.GetWaveById(connection, releaseCommand.Wave).ToList();
            var validationRequest = new ValidationContext()
            {
                Waves = lstWaves,
                Command = releaseCommand
            };
            if (!_validator.IsValid(connection, validationRequest))
            {
                throw new Exception("Invalid waves");
            }

            _orderAggregate.Value.ExecuteReleaseOrder(connection, lstWaves.First(), releaseCommand);

            _waveDbContext.UpdateWaveStatus(connection, releaseCommand.Wave,
                Enums.WaveStatus.Released.ToString("D"),
                releaseCommand.UserId);
            _loggerService.Value.Debug(
                $"Command processed successfully: #{releaseCommand.Id} of type {releaseCommand.CommandText} and wave #{releaseCommand.Wave}");
            //DR 6510 Fel changes
            TransmitWave(connection, lstWaves.FirstOrDefault());
            _waveDbContext.UpdateProcessingIndicatorEnd(connection, releaseCommand.Wave);
           
            return Task.CompletedTask;
        }

        public Task ExecuteResetWave(IDbConnection connection, long wave)
        {
            _waveDbContext.UpdateProcessingIndicatorStart(connection, wave);
            return Task.CompletedTask;
        }

        private void TransmitWave(IDbConnection connection, Wave wave)
        {
            var felType = GetFelType(wave);

            _loggerService.Value.Debug(
                $"Wave: #{wave.Id} of fel type {felType} ");

            if (felType > 0 || wave.IsBulkReturn.Equals("Y"))
            {
                _orderAggregate.Value.ExecuteTransmitCshipOrder(connection, wave, felType);

            }
        }

        public Task ExecuteUnreleaseWave(IDbConnection connection, ICommand unreleaseCommand)
        {
            var validationRequest = new ValidationContext()
            {
                Waves = _waveDbContext.GetWaveById(connection, unreleaseCommand.Wave).ToList(),
                Command = unreleaseCommand
            };
            if (!_validator.IsValid(connection, validationRequest))
            {
                return Task.CompletedTask;
            }

            _felAggregate.ExecuteUnReleaseFelOrder(connection, validationRequest.Waves.First(), unreleaseCommand);

            _orderAggregate.Value.ExecuteUnreleaseOrder(connection, validationRequest.Waves.First(),
                unreleaseCommand);


            _waveDbContext.UpdateWaveStatus(connection, unreleaseCommand.Wave,
                Enums.WaveStatus.Committed.ToString("D"),
                unreleaseCommand.UserId);
            _waveDbContext.UpdateProcessingIndicatorEnd(connection, unreleaseCommand.Wave);
            _loggerService.Value.Debug(
                $"Command processed successfully: #{unreleaseCommand.Id} of type {unreleaseCommand.CommandText} and wave #{unreleaseCommand.Wave}");

            return Task.CompletedTask;
        }

        #endregion


        #region Private Method(S)
        public int GetFelType(Wave wave)
        {
            var lValue = 0;

            if (wave.IsStationFel)
            {
                lValue += 4;
            }

            if (wave.IsLtlFrontEndLabel)
            {
                lValue += 2;
            }

            if (wave.IsFrontEndLabel)
            {
                lValue += 1;
            }

            return lValue;
        }

        #endregion
    }
}