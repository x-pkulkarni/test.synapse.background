﻿// <copyright file="FindAPickAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-14</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-21</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using AutoMapper;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Core.Profile;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Aggregates
{
    using Infrastructure.Logging.Interface;
    using Newtonsoft.Json.Linq;
    using StackExchange.Profiling.Internal;

    public class FindAPickAggregate : IFindAPickAggregate
    {
        #region Constructor(S)

        public FindAPickAggregate(IFindAPickAggregateInitializer findAPickAggregateInitializer, IInfrastructureInitializer infrastructureInitializer)
        {
            _pickDbContext = findAPickAggregateInitializer.PickDbContext;
            _batchTaskAggregate = findAPickAggregateInitializer.BatchTaskAggregate;
            _shippingPlateDbContext = findAPickAggregateInitializer.ShippingPlateDbContext;
            _locationDbContext = findAPickAggregateInitializer.LocationDbContext;
            _taskService = findAPickAggregateInitializer.TaskDomain;
            _subTaskAggregate = findAPickAggregateInitializer.SubTaskAggregate;
            _custItemAggregate = findAPickAggregateInitializer.CustItemAggregate;
            _catchWeightAggregate = findAPickAggregateInitializer.CatchWeightAggregate;
            _plateAggregate = findAPickAggregateInitializer.PlateAggregate;
            _loggerService = infrastructureInitializer.LoggerService;
            _mapper = new MappingEngine<ItemAggregateMapperProfile>().CreateMapper();
        }

        #endregion

        #region Private Member(S)

        private readonly IPickDbContext _pickDbContext;
        private readonly IBatchTaskAggregate _batchTaskAggregate;
        private readonly IShippingPlateDbContext _shippingPlateDbContext;
        private readonly ILocationDbContext _locationDbContext;
        private readonly ITaskService _taskService;
        private readonly ISubTaskAggregate _subTaskAggregate;
        private readonly IPlateAggregate _plateAggregate;
        private readonly ICatchWeightAggregate _catchWeightAggregate;
        private readonly ICustItemAggregate _custItemAggregate;
        private readonly Lazy<ILoggerService> _loggerService;
        private readonly IMapper _mapper;

        #endregion
        #region Public Member(S)
        public void ExecuteFullPick(IDbConnection connection, PickTaskRequest pickTaskRequest)
        {
            FindAPickResponse fullPickResponse;
            var orderDetail = pickTaskRequest.Order.OrderDetail;
            var task = new Task();
            do
            {
                if (!IsHighLevelUomAvailable(connection, pickTaskRequest))
                {
                    return;
                }
                fullPickResponse = FindAPick(connection, pickTaskRequest, Constants.AllocNeeded.FullPick);

                if (fullPickResponse == null) return;

                var plate = _plateAggregate.GetPlateByLpId(connection, fullPickResponse.LpIdOrLoc).FirstOrDefault();

                #region Fetch location details
                var locId = (plate == null ? fullPickResponse.LpIdOrLoc : plate.Location);
                var fromLocation = GetLocationdetails(connection, locId, pickTaskRequest.Command.Facility);
                var toLocation = GetLocationdetails(connection, pickTaskRequest.Order.HeaderInfo.StageLoc,
                    pickTaskRequest.Command.Facility);

                #endregion

                if (pickTaskRequest.Customer!= null && pickTaskRequest.Customer.IsPickByLineNumber)
                    pickTaskRequest.OrderQty = pickTaskRequest.OrderQty - fullPickResponse.BaseQty;

                pickTaskRequest.BaseQty = pickTaskRequest.BaseQty - fullPickResponse.BaseQty;


                #region Task creation

                var taskType = GetTaskType(fullPickResponse.PickType, pickTaskRequest.Order.HeaderInfo.OrderType);
                if (task.TaskId.Equals(0))
                    task = CreateTask(connection, pickTaskRequest.Order, pickTaskRequest.Commitment,
                        fromLocation, toLocation,
                        fullPickResponse, plate?.Location, pickTaskRequest.Command);

                _subTaskAggregate.CreateSubTaskForFullPick(connection, pickTaskRequest.Order,
                    pickTaskRequest.Commitment,
                    fromLocation, toLocation,
                    fullPickResponse, pickTaskRequest.Customer, pickTaskRequest.Command, locId, task, taskType,
                    plate?.LpId);

                #endregion

                if (!string.IsNullOrWhiteSpace(fullPickResponse.LpIdOrLoc))
                    _plateAggregate.UpdatePlateQtyAsked(connection, pickTaskRequest.Command.UserId,
                        fullPickResponse.BaseQty,
                        fullPickResponse.LpIdOrLoc);
            } while (pickTaskRequest.BaseQty > fullPickResponse.PickQty);

            _taskService.UpdateTask(connection, task);
        }

        public List<BatchTask> ExecuteBatchPickOnce(IDbConnection connection, PickTaskRequest pickTaskRequest)
        {
            var batchTasks = new List<BatchTask>();
            var batchPickResponse = new FindAPickResponse();
           
            batchPickResponse = FindAPick(connection, pickTaskRequest, pickTaskRequest.Command.PickType == Constants.PickType.BatchSingles ? "BP" : Constants.AllocNeeded.BatchPick);

            if (batchPickResponse == null) return batchTasks;
            var plate = new Plate();

            if (!batchPickResponse.PickFront.Equals("Y", StringComparison.CurrentCultureIgnoreCase))
            {
                plate = _plateAggregate.GetPlateByLpId(connection, batchPickResponse.LpIdOrLoc).FirstOrDefault(x => x.Item == pickTaskRequest.Item);
            }
            

            #region Fetch location details

            var fromBatchLocation =
                GetLocationdetails(connection, (plate.LpId.IsNullOrWhiteSpace() ? batchPickResponse.LpIdOrLoc : plate.Location), pickTaskRequest.Command.Facility);
            var toSortLocation = GetLocationdetails(connection,
                pickTaskRequest.Order.WaveInfo.SortLocation, pickTaskRequest.Command.Facility);

            #endregion

            var batch = _batchTaskAggregate.CreateBatchTaskObject(connection, pickTaskRequest.Order,
                pickTaskRequest.Commitment,
                fromBatchLocation, toSortLocation,
                batchPickResponse, pickTaskRequest.Command);

            if (!plate.LpId.IsNullOrWhiteSpace())
                batch.FromLocation = plate.Location;

            batchTasks.Add(batch);

            return batchTasks;
        }

        public List<BatchTask> ExecuteBatchPick(IDbConnection connection, PickTaskRequest pickTaskRequest)
        {
            var qty = pickTaskRequest.BaseQty;
            var batchTasks = new List<BatchTask>();
            var batchPickResponse = new FindAPickResponse();

            do
            {
                pickTaskRequest.BaseQty = qty;
                batchPickResponse = FindAPick(connection, pickTaskRequest, pickTaskRequest.Command.PickType == Constants.PickType.BatchSingles ? "BP" : Constants.AllocNeeded.BatchPick);

                if (batchPickResponse == null) return batchTasks;

                qty = qty - batchPickResponse.BaseQty;

                Plate plate = null;

                if (!batchPickResponse.PickFront.Equals("Y", StringComparison.CurrentCultureIgnoreCase))
                {
                    plate = _plateAggregate.GetPlateByLpId(connection, batchPickResponse.LpIdOrLoc)
                        .FirstOrDefault(x => x.Item == pickTaskRequest.Item);
                }

                #region Fetch location details

                var fromBatchLocation =
                    GetLocationdetails(connection, (plate == null ? batchPickResponse.LpIdOrLoc : plate.Location), pickTaskRequest.Command.Facility);
                var toSortLocation = GetLocationdetails(connection,
                    pickTaskRequest.Order.WaveInfo.SortLocation, pickTaskRequest.Command.Facility);

                #endregion

                var batch = _batchTaskAggregate.CreateBatchTaskObject(connection, pickTaskRequest.Order,
                    pickTaskRequest.Commitment,
                    fromBatchLocation, toSortLocation,
                    batchPickResponse, pickTaskRequest.Command);
              
                if (plate != null)
                    batch.FromLocation = plate.Location;
                batchTasks.Add(batch);
               
            } while (qty > 0);

            return batchTasks;
        }
        
        public void ExecuteFindAPickForRemainders(IDbConnection connection, PickTaskRequest pickTaskRequest)
        {
            FindAPickResponse fullPickResponse;

            //var taskResponse = _taskService.GetByTypeAndOrder(connection, "OP", pickTaskRequest.Command.Wave,
            //    pickTaskRequest.Order.OrderDetail.OrderId, pickTaskRequest.Order.OrderDetail.ShipId).ToList();
            //var task = taskResponse.Any() ? taskResponse.First() : new Task();
            var task =  new Task();
            do
            {
                fullPickResponse = FindAPick(connection, pickTaskRequest, "BP");

                if (fullPickResponse == null) return;

                var plate = _plateAggregate.GetPlateByLpId(connection, fullPickResponse.LpIdOrLoc).FirstOrDefault();

                #region Fetch location details

                var locId = (plate == null ? fullPickResponse.LpIdOrLoc : plate.Location);
                var fromLocation = GetLocationdetails(connection, locId, pickTaskRequest.Command.Facility);
                var toLocation = GetLocationdetails(connection, pickTaskRequest.Order.HeaderInfo.StageLoc,
                    pickTaskRequest.Command.Facility);

                #endregion

                if (pickTaskRequest.Customer !=null && pickTaskRequest.Customer.IsPickByLineNumber)
                    pickTaskRequest.OrderQty = pickTaskRequest.OrderQty - fullPickResponse.BaseQty;

                pickTaskRequest.BaseQty = pickTaskRequest.BaseQty - fullPickResponse.BaseQty;


                #region Task creation

                var taskType = GetTaskType(fullPickResponse.PickType, pickTaskRequest.Order.HeaderInfo.OrderType);
                if (task.TaskId.Equals(0))
                    task = CreateTask(connection, pickTaskRequest.Order, pickTaskRequest.Commitment,
                        fromLocation, toLocation,
                        fullPickResponse, locId, pickTaskRequest.Command);

                _subTaskAggregate.CreateSubTaskForFullPick(connection, pickTaskRequest.Order,
                    pickTaskRequest.Commitment,
                    fromLocation, toLocation,
                    fullPickResponse, pickTaskRequest.Customer, pickTaskRequest.Command, locId, task, taskType,
                    plate?.LpId);

                #endregion

                if (!string.IsNullOrWhiteSpace(fullPickResponse.LpIdOrLoc))
                    _plateAggregate.UpdatePlateQtyAsked(connection, pickTaskRequest.Command.UserId,
                        fullPickResponse.BaseQty,
                        fullPickResponse.LpIdOrLoc);
            } while (pickTaskRequest.BaseQty > 0);

            _taskService.UpdateTask(connection, task);
        }

        #endregion

        #region Private Member(S)
        private bool IsHighLevelUomAvailable(IDbConnection connection, PickTaskRequest pickTaskRequest)
        {
            //check if find a pick call is required.
            //To speed things up,if the quantity on the item ordered does not equal or exceed one higher uom level
            //above the base unit for the item, we can move on to the next item record.Take the baseUOM and find a conversion
            //from the baseUOM to another UOM with the least qty.So if the qty on the item is not at least as much as
            //this conversion, then we can move on because it will never qualify as a full of anything other than base UOM.
            var nextUom = _custItemAggregate.GetNextHighUom(connection, pickTaskRequest.Order.HeaderInfo.CustomerId,
                pickTaskRequest.Order.OrderDetail.Item);
            if (!string.IsNullOrEmpty(nextUom))
            {
                var custItemUomRequest = new CustItemUom
                {
                    CustId = pickTaskRequest.Order.HeaderInfo.CustomerId,
                    Item = pickTaskRequest.Order.OrderDetail.Item,
                    FromUom = pickTaskRequest.Order.OrderDetail.UomEntered,
                    ToUom = nextUom,
                    Qty = pickTaskRequest.Commitment.Quantity
                };
                var factor = _custItemAggregate.GetUotmQuantity(connection, custItemUomRequest);
                var isFullPick = factor != 0 ? (double)pickTaskRequest.Commitment.Quantity / factor : 0;
                if (isFullPick < 1)
                {
                    _loggerService.Value.Debug("Not enough base UOM qty to reach the next level UOM. Skip over.");
                    return false;
                }
            }
            else
            {
                _loggerService.Value.Debug("Not enough base UOM qty to reach the next level UOM. Skip over.");
                return false;
            }

            return true;
        }

        private FindAPickResponse FindAPick(IDbConnection connection, PickTaskRequest pickTaskRequest,
            string allocaNeed)
        {
            var sources =
                new Tuple<OrderTo, Commitment, ICommand, CustItem>(pickTaskRequest.Order, pickTaskRequest.Commitment,
                    pickTaskRequest.Command,
                    pickTaskRequest.CustItem);
            var request = _mapper.Map<FindAPickRequest>(sources);

            #region Customer data

            if (pickTaskRequest.Customer != null &&
                pickTaskRequest.Customer.IsPickByLineNumber)
            {
                request.Quantity = pickTaskRequest.OrderQty;
                request.PickUom = pickTaskRequest.CustItem != null && string.Equals(pickTaskRequest.CustItem.BaseUom,
                                      pickTaskRequest.Order.OrderDetail.UomEntered,
                                      StringComparison.CurrentCultureIgnoreCase)
                    ? pickTaskRequest.Order.OrderDetail.UomEntered
                    : string.Empty;
            }
            else
            {
                request.Quantity = pickTaskRequest.BaseQty;
                request.PickUom = default(string);
            }

            #endregion

            if (pickTaskRequest.Customer != null)
                request.IsEnterMindDaysToExpire = pickTaskRequest.Customer.IsEnterMinDaysToExpireFlag;
            request.AllocNeed = allocaNeed;
            if (allocaNeed != "BP" && pickTaskRequest.CustItem != null)
                request.Variancepct = pickTaskRequest.CustItem.Variancepct;
            var response = _pickDbContext.FindAPick(connection, request).ToList();
            return response.Any() && response.First().Message.StartsWith(Constants.Flag.Okay)
                ? response.First()
                : default(FindAPickResponse);
        }

        private string GetTaskType(string pickType, string ordertype)
        {
            if (string.Equals(pickType, Constants.PickType.Order, StringComparison.CurrentCultureIgnoreCase))
                return Constants.TaskType.OrderPick;
            if (string.Equals(pickType, Constants.PickType.Batch, StringComparison.CurrentCultureIgnoreCase) &&
                !string.Equals(ordertype, "K", StringComparison.CurrentCultureIgnoreCase))
                return Constants.TaskType.BatchPick;
            return Constants.TaskType.PickTask;
        }

       private Task CreateTask(IDbConnection connection, OrderTo order, Commitment commitment,
            Location fromLocation, Location toLocation, FindAPickResponse pickResponse, string fromLoc,
            ICommand commandRequest)
        {
            var source =
                new Tuple<OrderTo, Commitment, ICommand, Location, Location, FindAPickResponse>(
                    order, commitment, commandRequest, fromLocation, toLocation, pickResponse);
            return _taskService.CreateTaskForFullPick(connection, source, fromLoc);
        }

        private Location GetLocationdetails(IDbConnection connection, string locId, string facility)
        {
            var location = new Location
            {
                LocId = locId,
                Facility = facility
            };
            var resLocation = _locationDbContext.GetLocDetailsByLocId(connection, location).ToList();
            return resLocation.Any() ? resLocation.First() : default(Location);
        }
        #endregion
    }
}