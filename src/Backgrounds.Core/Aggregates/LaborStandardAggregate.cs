﻿// <copyright file = "LaborStandardAggregate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-26</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-13</lastchangeddate>

using System.Data;
using System.Linq;
using Autofac;
using AutoMapper;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;

namespace Synapse.Backgrounds.Core.Aggregates
{
    public class LaborStandardAggregate : ILaborStandardAggregate
    {

        #region Private Memb

        private readonly IOracleConnectionProvider _oracleConnection;
        private readonly IConfigService _configService;
        private readonly ILaborStandardsDbContext _laborStandardsDbContext;
        private readonly ICustItemUomDbContext _custItemUomDbContext;
        private readonly ISystemDefaultDbContext _systemDefaultDbContext;
        private readonly IMapper _mapper;

        #endregion

        #region Constructor(S)

        public LaborStandardAggregate(ILoggerService loggerService, IConfigService configService,
            IOracleConnectionProvider oracleConnection, IComponentContext icoContext)
        {
            _oracleConnection = oracleConnection;
            _configService = configService;
            _laborStandardsDbContext = icoContext.Resolve<ILaborStandardsDbContext>();
            _custItemUomDbContext = icoContext.Resolve<ICustItemUomDbContext>();
            _systemDefaultDbContext = icoContext.Resolve<ISystemDefaultDbContext>();
            _mapper = icoContext.Resolve<IMapper>();
        }

        #endregion

        #region Public Method(S)

        public double GetStaffHours(IDbConnection connection, StaffHoursRequest request)
        {
            var defaultQtyPerHour = 12.0;
            var staffHours = default(double);
            var defaultQtyHour = _systemDefaultDbContext.GetDefaultValueByDefaultId(connection, Constants.SystemDefaultId.LaborQtyPerHour);
            if (!string.IsNullOrWhiteSpace(defaultQtyHour) && int.TryParse(defaultQtyHour, out var iDefaultQtyHour))
            {
                if (iDefaultQtyHour > 0)
                    defaultQtyPerHour = iDefaultQtyHour;
            }

            var laborStandard = _mapper.Map<LaborStandard>(request);
            var lsQtyPerHour = _laborStandardsDbContext.GetLaborStdQtyPerHourForUom(connection, laborStandard).ToList();
            if (!lsQtyPerHour.Any())
            {
                var ciRequest = new CustItemUom() {CustId = request.CustId, Item = request.Item, ToUom = request.Uom};
                var tSequence = _custItemUomDbContext.GetCustItemSequenceByToUom(connection, ciRequest);
                if (tSequence == 0)
                {
                    ciRequest.FromUom = request.Uom;
                    tSequence = _custItemUomDbContext.GetCustItemSequenceByFromUom(connection, ciRequest);
                    if (tSequence == 0)
                    {
                        staffHours = request.Quantity / defaultQtyPerHour;
                        return staffHours;
                    }

                    tSequence = 0;
                }

                var qtyEquiv = 0;
                var qtyperhour = defaultQtyPerHour;
                var laborStdUom = _laborStandardsDbContext.GetLaborStdQtyPerHourForDiffUom(connection, laborStandard);

                laborStdUom.ToList().ForEach(o =>
                {
                    ciRequest.FromUom = o.Uom;
                    var fSequence = _custItemUomDbContext.GetCustItemSequenceByFromUom(connection, ciRequest);
                    if (fSequence == 0)
                    {
                        ciRequest.ToUom = request.Uom;
                        fSequence = _custItemUomDbContext.GetCustItemSequenceByToUom(connection, ciRequest);
                        if (fSequence == 0)
                        {
                            return;
                        }

                        fSequence = 999;
                    }

                    qtyEquiv = request.Quantity;
                    var ciQtyRequest = new CustItemQtyBySeqRequest()
                    {
                        CustId = request.CustId,
                        Item = request.Item,
                        FromSequence = fSequence,
                        ToSequence = tSequence
                    };
                    if (tSequence <= fSequence)
                    {
                        var equivUp = _custItemUomDbContext.GetCustItemQtyByEquivUp(connection, ciQtyRequest);
                        equivUp.ToList().ForEach(
                            e => { qtyEquiv = qtyEquiv * e.Qty; });

                    }
                    else
                    {
                        var equivdown = _custItemUomDbContext.GetCustItemQtyByEquivDown(connection, ciQtyRequest);
                        equivdown.ToList().ForEach(
                            e => { qtyEquiv = qtyEquiv * e.Qty; });
                    }

                    qtyperhour = o.QtyPerHour;
                });

                if (qtyEquiv != 0)
                    staffHours = qtyEquiv / qtyperhour;
                else
                    staffHours = request.Quantity / defaultQtyPerHour;

            }
            else
            {
                if (lsQtyPerHour.First().QtyPerHour < 0)
                    staffHours = request.Quantity / defaultQtyPerHour;
                else
                    staffHours = request.Quantity / lsQtyPerHour.First().QtyPerHour;
            }

            return staffHours;
        }

        #endregion
    }
}
