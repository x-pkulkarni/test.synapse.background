﻿// <copyright file="MappingEngine.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-29</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using AutoMapper;

namespace Synapse.Backgrounds.Core.Profile
{
    public class MappingEngine<T>
    {
        private readonly MapperConfiguration _config;

        public MappingEngine()
        {
            _config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(typeof(AutoMapperProfile));
                cfg.AddProfile(typeof(T));
            });
        }

        public IMapper CreateMapper()
        {
            return _config.CreateMapper();
        }
    }
}