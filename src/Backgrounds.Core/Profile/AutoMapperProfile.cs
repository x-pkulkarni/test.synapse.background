﻿// <copyright file="AutoMapperProfile.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-15</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-03-23</lastchangeddate>

using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Profile
{
    public class AutoMapperProfile : AutoMapper.Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<CustItem, CustItemUom>().ReverseMap();
            CreateMap<StaffHoursRequest, LaborStandard>();
            CreateMap<BatchTask, BatchTask>().IgnoreAllPropertiesWithAnInaccessibleSetter();
        }
    }
}