﻿// <copyright file="OrderDomainMapperProfile.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-30</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using System;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Integration;

namespace Synapse.Backgrounds.Core.Profile
{
    public class OrderAggregateMapperProfile : AutoMapper.Profile
    {
        public OrderAggregateMapperProfile()
        {
            BatchTaskMapping();
            SubTaskMapping();
            TaskMapping();
            ConnectShipMapping();
        }


        private void BatchTaskMapping()
        {
            CreateMap<Tuple<OrderTo, Commitment, ICommand, Location, Location>, BatchTask>()
                .ForMember(dest => dest.TaskType, opt => opt.MapFrom(src => Constants.TaskType.BatchPick))
                .ForMember(dest => dest.TaskId, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ShippingType, opt => opt.MapFrom(src => "P"))
                //Order header & details
                .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.Item1.HeaderInfo.CustomerId))
                .ForMember(dest => dest.LoadNo, opt => opt.MapFrom(src => src.Item1.HeaderInfo.LoadNo))
                .ForMember(dest => dest.StopNo, opt => opt.MapFrom(src => src.Item1.HeaderInfo.StopNo))
                .ForMember(dest => dest.ShipNo, opt => opt.MapFrom(src => src.Item1.HeaderInfo.ShipNo))
                .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Item1.HeaderInfo.OrderId))
                .ForMember(dest => dest.ShipId, opt => opt.MapFrom(src => src.Item1.HeaderInfo.ShipId))
                .ForMember(dest => dest.OrderLot, opt => opt.MapFrom(src => src.Item1.OrderDetail.LotNumber))
                .ForMember(dest => dest.QtyType, opt => opt.MapFrom(src => src.Item1.OrderDetail.QuantityType))
                //Commitments
                .ForMember(dest => dest.PickUom, opt => opt.MapFrom(src => src.Item2.Uom))
                .ForMember(dest => dest.LotNumber, opt => opt.MapFrom(src => src.Item2.LotNumber))
                .ForMember(dest => dest.InvStatus, opt => opt.MapFrom(src => src.Item2.InvStatus))
                .ForMember(dest => dest.InventoryClass, opt => opt.MapFrom(src => src.Item2.InventoryClass))
                .ForMember(dest => dest.OrderItem, opt => opt.MapFrom(src => src.Item2.Item))
                .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item2.Item))
                .ForMember(dest => dest.UOM, opt => opt.MapFrom(src => src.Item2.Uom))
                //Wave Release request object
                .ForMember(dest => dest.LastUser, opt => opt.MapFrom(src => src.Item3.UserId))
                .ForMember(dest => dest.Wave, opt => opt.MapFrom(src => src.Item3.Wave))
                .ForMember(dest => dest.Facility, opt => opt.MapFrom(src => src.Item3.Facility))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Item3.TaskPriority))
                .ForMember(dest => dest.PrevPriority, opt => opt.MapFrom(src => src.Item3.TaskPriority))
                //From Location object
                .ForMember(dest => dest.FromSection,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.Section : default(string)))
                .ForMember(dest => dest.FromProfile,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.Equipprof : default(string)))
                .ForMember(dest => dest.PickingZone,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickingZone : default(string)))
                .ForMember(dest => dest.LocSeq,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickingSeq : default(int)))
                //To Location object
                .ForMember(dest => dest.ToSection,
                    opt => opt.MapFrom(src => src.Item5 != null ? src.Item5.Section : default(string)))
                .ForMember(dest => dest.ToProfile,
                    opt => opt.MapFrom(src => src.Item5 != null ? src.Item5.Equipprof : default(string)))
                .ForAllOtherMembers(opt => opt.Ignore());
        }


        private void TaskMapping()
        {
            CreateMap<Tuple<BatchTask, Plate, ICommand, Location, Location, Wave>,
                    Task>().ForMember(dest => dest.TaskType, opt => opt.MapFrom(src => Constants.TaskType.BatchPick))
                .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.Item1.CustomerId))
                .ForMember(dest => dest.Facility, opt => opt.MapFrom(src => src.Item1.Facility))
                .ForMember(dest => dest.Uom, opt => opt.MapFrom(src => src.Item1.UOM))
                .ForMember(dest => dest.PickUom, opt => opt.MapFrom(src => src.Item1.PickUom))
                .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item1.Item))
                .ForMember(dest => dest.PickToType, opt => opt.MapFrom(src => src.Item1.PickToType))

                //Plate
                .ForMember(dest => dest.FromLocation, opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.Location : src.Item1.FromLocation))

                //Wave Release request object
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Item3.TaskPriority))
                .ForMember(dest => dest.PrevPriority, opt => opt.MapFrom(src => src.Item3.TaskPriority))
                .ForMember(dest => dest.LastUser, opt => opt.MapFrom(src => src.Item3.UserId))
                .ForMember(dest => dest.Wave, opt => opt.MapFrom(src => src.Item3.Wave))

                ////From Location object
                .ForMember(dest => dest.FromSection,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.Section : default(string)))
                .ForMember(dest => dest.FromProfile,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.Equipprof : default(string)))
                .ForMember(dest => dest.PickingZone,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickingZone : default(string)))
                .ForMember(dest => dest.LocSeq,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickingSeq : default(int)))

                //To Location object
                .ForMember(dest => dest.ToSection,
                    opt => opt.MapFrom(src => src.Item5 != null ? src.Item5.Section : default(string)))
                .ForMember(dest => dest.ToProfile,
                    opt => opt.MapFrom(src => src.Item5 != null ? src.Item5.Equipprof : default(string)))

                //Wave
                .ForMember(dest => dest.ToLocation, opt => opt.MapFrom(src => src.Item6.SortLocation))
                .ForAllOtherMembers(opt => opt.Ignore());


            CreateMap<SubTask, Task>()
                .ForMember(dest => dest.TaskType, opt => opt.MapFrom(src => src.TaskType))
                .ForMember(dest => dest.PickToType, opt => opt.MapFrom(src => src.PickToType))
               .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.CustId))
               .ForMember(dest => dest.Facility, opt => opt.MapFrom(src => src.Facility))
               .ForMember(dest => dest.FromLocation, opt => opt.MapFrom(src => src.FromLocation))
               .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Priority))
               .ForMember(dest => dest.PrevPriority, opt => opt.MapFrom(src => src.PrevPriority))
               .ForMember(dest => dest.Wave, opt => opt.MapFrom(src => src.Wave))
               .ForMember(dest => dest.FromSection,
                   opt => opt.MapFrom(src => src.FromSection))
               .ForMember(dest => dest.FromProfile,
                   opt => opt.MapFrom(src => src.FromProfile))
               .ForMember(dest => dest.PickingZone,
                   opt => opt.MapFrom(src => src.PickingZone))
               .ForMember(dest => dest.ToSection,
                   opt => opt.MapFrom(src => src.ToSection))
               .ForMember(dest => dest.ToProfile,
                   opt => opt.MapFrom(src => src.ToProfile))
               .ForMember(dest => dest.ToLocation, opt => opt.MapFrom(src => src.ToLocation))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Quantity))
                .ForMember(dest => dest.LocSeq, opt => opt.MapFrom(src => src.LocSeq))
                .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.OrderId))
                .ForMember(dest => dest.ShipId, opt => opt.MapFrom(src => src.ShipId))
                .ForMember(dest => dest.Wave, opt => opt.MapFrom(src => src.Wave))
                .ForMember(dest => dest.Weight, opt => opt.MapFrom(src => src.Weight))
                .ForMember(dest => dest.Cube, opt => opt.MapFrom(src => src.Cube))
                .ForMember(dest => dest.StaffHrs, opt => opt.MapFrom(src => src.StaffHrs))
                .ForMember(dest => dest.PickQty, opt => opt.MapFrom(src => src.PickQty))
               .ForAllOtherMembers(opt => opt.Ignore());

            //Feltask mapping
            CreateMap<ParcelStaging, Task>()
              .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.CustId))
               .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.OrderId))
               .ForMember(dest => dest.ShipId, opt => opt.MapFrom(src => src.ShipId))
               .ForMember(dest => dest.Wave, opt => opt.MapFrom(src => src.Wave))
               .ForMember(dest => dest.PickQty, opt => opt.MapFrom(src => src.PickQty))
              .ForAllOtherMembers(opt => opt.Ignore());
        }

        private void SubTaskMapping()
        {
            CreateMap<Tuple<BatchTask, Plate, ICommand, Location, Location, Wave>,
                    SubTask>().ForMember(dest => dest.TaskType, opt => opt.MapFrom(src => Constants.TaskType.BatchPick))
                .ForMember(dest => dest.ShippingType, opt => opt.MapFrom(src => "P"))

                //Batch Task
                .ForMember(dest => dest.CustId, opt => opt.MapFrom(src => src.Item1.CustomerId))
                .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item1.Item))
                .ForMember(dest => dest.Quantity, opt => opt.MapFrom(src => src.Item1.Quantity))
                .ForMember(dest => dest.OrderItem, opt => opt.MapFrom(src => src.Item1.OrderItem))
                .ForMember(dest => dest.OrderLot, opt => opt.MapFrom(src => src.Item1.OrderLot))
                .ForMember(dest => dest.PickUom, opt => opt.MapFrom(src => src.Item1.PickUom))
                .ForMember(dest => dest.PickQty, opt => opt.MapFrom(src => src.Item1.PickQty))
                .ForMember(dest => dest.PickToType, opt => opt.MapFrom(src => src.Item1.PickToType))
                .ForMember(dest => dest.CartonType, opt => opt.MapFrom(src => src.Item1.CartonType))
                .ForMember(dest => dest.Facility, opt => opt.MapFrom(src => src.Item1.Facility))

                //Plate
                .ForMember(dest => dest.FromLocation, opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.Location : src.Item1.FromLocation))
                .ForMember(dest => dest.LpId, opt => opt.MapFrom(src => src.Item2.LpId))
                .ForMember(dest => dest.UOM, opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.UnitOfMeasure : src.Item1.UOM))

                //Wave Release request object
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Item3.TaskPriority))
                .ForMember(dest => dest.PrevPriority, opt => opt.MapFrom(src => src.Item3.TaskPriority))
                .ForMember(dest => dest.LastUser, opt => opt.MapFrom(src => src.Item3.UserId))
                .ForMember(dest => dest.Wave, opt => opt.MapFrom(src => src.Item3.Wave))

                //From Location object
                .ForMember(dest => dest.FromSection,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.Section : default(string)))
                .ForMember(dest => dest.FromProfile,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.Equipprof : default(string)))
                .ForMember(dest => dest.PickingZone,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickingZone : default(string)))
                .ForMember(dest => dest.LocSeq,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickingSeq : default(int)))

                //To Location object
                .ForMember(dest => dest.ToSection,
                    opt => opt.MapFrom(src => src.Item5 != null ? src.Item5.Section : default(string)))
                .ForMember(dest => dest.ToProfile,
                    opt => opt.MapFrom(src => src.Item5 != null ? src.Item5.Equipprof : default(string)))

                //Wave Info
                .ForMember(dest => dest.ToLocation, opt => opt.MapFrom(src => src.Item6.SortLocation))
                .ForAllOtherMembers(opt => opt.Ignore());
        }

        private void ConnectShipMapping()
        {
            CreateMap<ConnectShipOrderHdr, ConnectShipHeader>()
                .ForMember(dest => dest.Facility, opt => opt.MapFrom(src => src.FromFacility))
                .ForMember(dest => dest.ResidentialFlag, opt => opt.MapFrom(src => src.Residential))
                .ForMember(dest => dest.SaturdayDeliveryFlag, opt => opt.MapFrom(src => src.SaturdayDelivery))
                .ForMember(dest => dest.SignatureRequiredFlag, opt => opt.MapFrom(src => src.SpecialService1))
                .ForMember(dest => dest.Terms, opt => opt.MapFrom(src => src.ShipTerms))
                .ForMember(dest => dest.BillToCountry, opt => opt.MapFrom(src => src.BillToCountryCode));

            CreateMap<ConnectShipOrderHdr, ConnectShipDtl>()
                .ForMember(dest => dest.Facility, opt => opt.MapFrom(src => src.FromFacility));

            CreateMap<ConnectShipPackLable, ConnectShipHeader>()
                .ForMember(dest => dest.ItemDescr, opt => opt.MapFrom(src => src.Descr))
                .ForMember(dest => dest.Location, opt => opt.MapFrom(src => src.FromLoc))
                .ForMember(dest => dest.Zone, opt => opt.MapFrom(src => src.PickingZone))
                .ForMember(dest => dest.Aisle, opt => opt.MapFrom(src => src.Aisle))
                .ForMember(dest => dest.BatteryCount, opt => opt.MapFrom(src => src.CustItemBatteryCount))
                .ForMember(dest => dest.CellCount, opt => opt.MapFrom(src => src.CustItemCellCount))
                .ForMember(dest => dest.LithiumWeight, opt => opt.MapFrom(src => src.CustItemLithiumWeight))
                .ForMember(dest => dest.BatteryWattHrs, opt => opt.MapFrom(src => src.CustItemBatteryWattHrs))
                .ForMember(dest => dest.PrimaryHazardClass, opt => opt.MapFrom(src => src.CustItemPrimaryHazardClass));

            CreateMap<ConnectShipPackLable, ConnectShipDtl>()
                .ForMember(dest => dest.ItemDescription, opt => opt.MapFrom(src => src.Descr))
                .ForMember(dest => dest.CountryOfOrigin, opt => opt.MapFrom(src => src.CountryOf))
                .ForMember(dest => dest.BaseQuantity, opt => opt.MapFrom(src => src.Qty))
                .ForMember(dest => dest.BaseUom, opt => opt.MapFrom(src => src.Uom))
                .ForMember(dest => dest.PickQuantity, opt => opt.MapFrom(src => src.PickQty))
                .ForMember(dest => dest.PickUom, opt => opt.MapFrom(src => src.PickUom));
        }

       

    }
}