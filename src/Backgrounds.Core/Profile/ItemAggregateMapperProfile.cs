﻿// <copyright file="ItemDomainMapperProfile.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-29</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-03-23</lastchangeddate>

using System;
using AutoMapper;
using Synapse.Backgrounds.Core;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Integration;

namespace Synapse.Backgrounds.Core.Profile
{
    public class ItemAggregateMapperProfile : AutoMapper.Profile
    {
        public ItemAggregateMapperProfile()
        {
            BatchTaskMapping();
            CommitmentMapping();
            SubTaskMapping();
            ShippingPlateMapping();
            TaskMapping();
            FindAPickMapping();
            CloneSubTaskMapping();
        }

        private void CommitmentMapping()
        {
            // var mapOrderDetail = CreateMap<OrderDetail, Commitment>();
            CreateMap<OrderDetail, Commitment>(MemberList.None)
                .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.OrderId))
                .ForMember(dest => dest.ShipId, opt => opt.MapFrom(src => src.ShipId))
                .ForMember(dest => dest.OrderItem, opt => opt.MapFrom(src => src.Item))
                .ForMember(dest => dest.OrderLot, opt => opt.MapFrom(src => src.LotNumber))
                .ForAllOtherMembers(opt => opt.Ignore());
        }

        private void FindAPickMapping()
        {
            Func<string, string> findLocType = k =>
                string.Equals(k, "O", StringComparison.CurrentCultureIgnoreCase) ? "STG" : "STO";

            Func<string,string> lotNumber = l =>
                string.Equals(l, "(none)", StringComparison.CurrentCultureIgnoreCase) ? l: string.Empty;

            CreateMap<Tuple<OrderTo, Commitment, ICommand, CustItem>, FindAPickRequest>()
                .ForMember(dest => dest.ForReplenishmentRequest, opt => opt.MapFrom(src => Constants.Flag.No))
                .ForMember(dest => dest.SlotWaveRequired, opt => opt.MapFrom(src => Constants.Flag.No))
                .ForMember(dest => dest.FromFacility, opt => opt.MapFrom(src => src.Item1.HeaderInfo.FromFacility))
                .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.Item1.HeaderInfo.CustomerId))
                .ForMember(dest => dest.QuantityType, opt => opt.MapFrom(src => src.Item1.OrderDetail.QuantityType))
                .ForMember(dest => dest.MindDaysToExpiration, opt => opt.MapFrom(src => src.Item1.OrderDetail.MinDaysToExpiration))
                .ForMember(dest => dest.WavePickZone,
                    opt => opt.MapFrom(src => src.Item1.WaveInfo.PreferredPickZone))
                .ForMember(dest => dest.Vendor, opt => opt.MapFrom(src => src.Item1.OrderDetail.Vendor))
                .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Item1.OrderDetail.OrderId))
                .ForMember(dest => dest.ShipId, opt => opt.MapFrom(src => src.Item1.OrderDetail.ShipId))
                .ForMember(dest => dest.DynamicAllocMode, opt => opt.MapFrom(src => string.IsNullOrEmpty(src.Item1.OrderDetail.DynamicInvUsedFlag)?"N": src.Item1.OrderDetail.DynamicInvUsedFlag))
                .ForMember(dest => dest.PickToTypeRule,
                    opt => opt.MapFrom(src => src.Item1.WaveInfo.PickToTypeRule))
                .ForMember(dest => dest.WavePickZone,
                    opt => opt.MapFrom(src => src.Item1.WaveInfo.PreferredPickZone))
                //Commitments
                .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item2.Item))
                .ForMember(dest => dest.LotNumber, opt => opt.MapFrom(src => lotNumber(src.Item2.LotNumber)))
                .ForMember(dest => dest.InvStatus, opt => opt.MapFrom(src => src.Item2.InvStatus))
                .ForMember(dest => dest.InventoryClass, opt => opt.MapFrom(src => src.Item2.InventoryClass))
                //Wave Release request object
                .ForMember(dest => dest.Trace, opt => opt.MapFrom(src => src.Item3.Trace))
                //Cust Item View
                .ForMember(dest => dest.StorageOrStage, opt => opt.MapFrom(src => findLocType(src.Item4.IsKit)))
                .ForMember(dest => dest.ExpirationDtRequired, opt => opt.MapFrom(src => src.Item4.ExpdateRequired))
                .ForAllOtherMembers(opt => opt.Ignore());
        }

        private void BatchTaskMapping()
        {
            CreateMap<Tuple<OrderTo, Commitment, ICommand, Location, Location>, BatchTask>()
                .ForMember(dest => dest.TaskType, opt => opt.MapFrom(src => Constants.TaskType.BatchPick))
                .ForMember(dest => dest.TaskId, opt => opt.MapFrom(src => 0))
                .ForMember(dest => dest.ShippingType, opt => opt.MapFrom(src => "P"))
                //Order header & details
                .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.Item1.HeaderInfo.CustomerId))
                .ForMember(dest => dest.LoadNo, opt => opt.MapFrom(src => src.Item1.HeaderInfo.LoadNo))
                .ForMember(dest => dest.StopNo, opt => opt.MapFrom(src => src.Item1.HeaderInfo.StopNo))
                .ForMember(dest => dest.ShipNo, opt => opt.MapFrom(src => src.Item1.HeaderInfo.ShipNo))
                .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Item1.HeaderInfo.OrderId))
                .ForMember(dest => dest.ShipId, opt => opt.MapFrom(src => src.Item1.HeaderInfo.ShipId))
                .ForMember(dest => dest.OrderLot, opt => opt.MapFrom(src => src.Item1.OrderDetail.LotNumber))
                .ForMember(dest => dest.QtyType, opt => opt.MapFrom(src => src.Item1.OrderDetail.QuantityType))
                //Commitments
                .ForMember(dest => dest.PickUom, opt => opt.MapFrom(src => src.Item2 !=null? src.Item2.Uom : default(string)))
                .ForMember(dest => dest.LotNumber, opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.LotNumber: default(string)))
                .ForMember(dest => dest.InvStatus, opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.InvStatus: default(string)))
                .ForMember(dest => dest.InventoryClass, opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.InventoryClass: default(string)))
                .ForMember(dest => dest.OrderItem, opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.Item: default(string)))
                .ForMember(dest => dest.Item, opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.Item: default(string)))
                .ForMember(dest => dest.UOM, opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.Uom: default(string)))
                //Wave Release request object
                .ForMember(dest => dest.LastUser, opt => opt.MapFrom(src => src.Item3.UserId))
                .ForMember(dest => dest.Wave, opt => opt.MapFrom(src => src.Item3.Wave))
                .ForMember(dest => dest.Facility, opt => opt.MapFrom(src => src.Item3.Facility))
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Item3.TaskPriority))
                .ForMember(dest => dest.PrevPriority, opt => opt.MapFrom(src => src.Item3.TaskPriority))
                //From Location object
                .ForMember(dest => dest.FromSection,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.Section : default(string)))
                .ForMember(dest => dest.FromProfile,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.Equipprof : default(string)))
                .ForMember(dest => dest.PickingZone,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickingZone : default(string)))
                .ForMember(dest => dest.LocSeq,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickingSeq : default(int)))
                //To Location object
                .ForMember(dest => dest.ToSection,
                    opt => opt.MapFrom(src => src.Item5 != null ? src.Item5.Section : default(string)))
                .ForMember(dest => dest.ToProfile,
                    opt => opt.MapFrom(src => src.Item5 != null ? src.Item5.Equipprof : default(string)))
                .ForAllOtherMembers(opt => opt.Ignore());
        }

        private void TaskMapping()
        {
            CreateMap<Tuple<OrderTo, Commitment, ICommand, Location, Location, FindAPickResponse>,
                    Task>()
                .ForMember(dest => dest.TaskType,
                    opt => opt.MapFrom(src => GetTaskType(src.Item6.PickType, src.Item1.HeaderInfo.OrderType)))
                .ForMember(dest => dest.ToLocation, opt => opt.MapFrom(src => src.Item1.HeaderInfo.StageLoc))
                .ForMember(dest => dest.CustomerId, opt => opt.MapFrom(src => src.Item1.HeaderInfo.CustomerId))
                .ForMember(dest => dest.LoadNo, opt => opt.MapFrom(src => src.Item1.HeaderInfo.LoadNo))
                .ForMember(dest => dest.StopNo, opt => opt.MapFrom(src => src.Item1.HeaderInfo.StopNo))
                .ForMember(dest => dest.ShipNo, opt => opt.MapFrom(src => src.Item1.HeaderInfo.ShipNo))
                .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Item1.HeaderInfo.OrderId))
                .ForMember(dest => dest.ShipId, opt => opt.MapFrom(src => src.Item1.HeaderInfo.ShipId))
                .ForMember(dest => dest.OrderLot, opt => opt.MapFrom(src => src.Item1.OrderDetail.LotNumber))
                .ForMember(dest => dest.Uom, opt => opt.MapFrom(src => src.Item1.OrderDetail.Uom))
                .ForMember(dest => dest.PickUom, opt => opt.MapFrom(src => src.Item1.OrderDetail.Uom))

                // FindAPick response
                .ForMember(dest => dest.Quantity,
                    opt => opt.MapFrom(src => src.Item6 != null ? src.Item6.BaseQty : default(int)))
                .ForMember(dest => dest.PickQty,
                    opt => opt.MapFrom(src => src.Item2 != null ? src.Item6.PickQty : default(int)))

                //Wave Release request object
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Item3.TaskPriority))
                .ForMember(dest => dest.PrevPriority, opt => opt.MapFrom(src => src.Item3.TaskPriority))
                .ForMember(dest => dest.LastUser, opt => opt.MapFrom(src => src.Item3.UserId))
                .ForMember(dest => dest.Wave, opt => opt.MapFrom(src => src.Item3.Wave))
                .ForMember(dest => dest.Facility, opt => opt.MapFrom(src => src.Item3.Facility))
                //From Location object
                .ForMember(dest => dest.FromSection,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.Section : default(string)))
                .ForMember(dest => dest.FromProfile,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.Equipprof : default(string)))
                .ForMember(dest => dest.PickingZone,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickingZone : default(string)))
                .ForMember(dest => dest.LocSeq,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickingSeq : default(int)))
                //To Location object
                .ForMember(dest => dest.ToSection,
                    opt => opt.MapFrom(src => src.Item5 != null ? src.Item5.Section : default(string)))
                .ForMember(dest => dest.ToProfile,
                    opt => opt.MapFrom(src => src.Item5 != null ? src.Item5.Equipprof : default(string)))
                .ForAllOtherMembers(opt => opt.Ignore());
        }

        private void SubTaskMapping()
        {
            CreateMap<Tuple<OrderTo, Commitment, ICommand, Location, Location, FindAPickResponse>,
                    SubTask>()
                .ForMember(dest => dest.TaskType,
                    opt => opt.MapFrom(src => GetTaskType(src.Item6.PickType, src.Item1.HeaderInfo.OrderType)))
                .ForMember(dest => dest.ShippingType, opt => opt.MapFrom(src => "P"))
                .ForMember(dest => dest.ToLocation, opt => opt.MapFrom(src => src.Item1.HeaderInfo.StageLoc))
                .ForMember(dest => dest.CustId, opt => opt.MapFrom(src => src.Item1.HeaderInfo.CustomerId))
                .ForMember(dest => dest.LoadNo, opt => opt.MapFrom(src => src.Item1.HeaderInfo.LoadNo))
                .ForMember(dest => dest.StopNo, opt => opt.MapFrom(src => src.Item1.HeaderInfo.StopNo))
                .ForMember(dest => dest.ShipNo, opt => opt.MapFrom(src => src.Item1.HeaderInfo.ShipNo))
                .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Item1.HeaderInfo.OrderId))
                .ForMember(dest => dest.ShipId, opt => opt.MapFrom(src => src.Item1.HeaderInfo.ShipId))
                .ForMember(dest => dest.OrderLot, opt => opt.MapFrom(src => src.Item1.OrderDetail.LotNumber))
                //Commitments
                .ForMember(dest => dest.UOM,
                    opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.Uom : default(string)))
                .ForMember(dest => dest.OrderItem,
                    opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.Item : default(string)))
                .ForMember(dest => dest.Item,
                    opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.Item : default(string)))
                //Wave Release request object
                .ForMember(dest => dest.Priority, opt => opt.MapFrom(src => src.Item3.TaskPriority))
                .ForMember(dest => dest.PrevPriority, opt => opt.MapFrom(src => src.Item3.TaskPriority))
                .ForMember(dest => dest.LastUser, opt => opt.MapFrom(src => src.Item3.UserId))
                .ForMember(dest => dest.Wave, opt => opt.MapFrom(src => src.Item3.Wave))
                .ForMember(dest => dest.Facility, opt => opt.MapFrom(src => src.Item3.Facility))
                //From Location object
                .ForMember(dest => dest.FromSection,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.Section : default(string)))
                .ForMember(dest => dest.FromProfile,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.Equipprof : default(string)))
                .ForMember(dest => dest.PickingZone,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickingZone : default(string)))
                .ForMember(dest => dest.LocSeq,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickingSeq : default(int)))
                //To Location object
                .ForMember(dest => dest.ToSection,
                    opt => opt.MapFrom(src => src.Item5 != null ? src.Item5.Section : default(string)))
                .ForMember(dest => dest.ToProfile,
                    opt => opt.MapFrom(src => src.Item5 != null ? src.Item5.Equipprof : default(string)))
                // FindAPick response
                .ForMember(dest => dest.Quantity,
                    opt => opt.MapFrom(src => src.Item6 != null ? src.Item6.BaseQty : default(int)))
                .ForMember(dest => dest.PickUom,
                    opt => opt.MapFrom(src => src.Item6 != null ? src.Item6.PickUom : default(string)))
                .ForMember(dest => dest.PickQty,
                    opt => opt.MapFrom(src => src.Item6 != null ? src.Item6.PickQty : default(int)))
                .ForMember(dest => dest.PickToType,
                    opt => opt.MapFrom(src => src.Item6 != null ? src.Item6.PickToType : default(string)))
                .ForMember(dest => dest.CartonType,
                    opt => opt.MapFrom(src => src.Item6 != null ? src.Item6.CartonType : default(string)))
                .ForMember(dest => dest.LpId,
                    opt => opt.MapFrom(src => src.Item6 != null ? src.Item6.LpIdOrLoc : default(string)))
                .ForAllOtherMembers(opt => opt.Ignore());
        }

        private void ShippingPlateMapping()
        {
            CreateMap<Tuple<OrderHeader, Commitment, ICommand, SubTask>, ShippingPlate>()
                .ForMember(
                    dest => dest.CustomerId,
                    opt => opt.MapFrom(src => src.Item1.CustomerId))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => "U"))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => "P"))
                .ForMember(dest => dest.QtyEntered, opt => opt.MapFrom(src => src.Item4.PickQty))
                .ForMember(dest => dest.LoadNo, opt => opt.MapFrom(src => src.Item1.LoadNo))
                .ForMember(dest => dest.StopNo, opt => opt.MapFrom(src => src.Item1.StopNo))
                .ForMember(dest => dest.ShipNo, opt => opt.MapFrom(src => src.Item1.ShipNo))
                .ForMember(dest => dest.OrderId, opt => opt.MapFrom(src => src.Item1.OrderId))
                .ForMember(dest => dest.ShipId, opt => opt.MapFrom(src => src.Item1.ShipId))
                // Commitments
                .ForMember(dest => dest.Item,
                    opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.Item : default(string)))
                .ForMember(dest => dest.OrderItem,
                    opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.Item : default(string)))
                .ForMember(dest => dest.InvStatus,
                    opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.InvStatus : default(string)))
                .ForMember(dest => dest.UnitOfMeasure,
                    opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.Uom : default(string)))
                .ForMember(dest => dest.InventoryClass, opt => opt.MapFrom(src => src.Item2 != null ? src.Item2.InventoryClass : default(string)))
                //Wave Release request object
                .ForMember(dest => dest.OrderLot, opt => opt.MapFrom(src => src.Item3.LotNumber))
                .ForMember(dest => dest.Facility, opt => opt.MapFrom(src => src.Item3.Facility))
                .ForMember(dest => dest.LastUser, opt => opt.MapFrom(src => src.Item3.UserId))
                // FindAPick response
                .ForMember(dest => dest.Quantity,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.Quantity : default(int)))
                .ForMember(dest => dest.PickQty,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickQty : default(int)))
                .ForMember(dest => dest.PickUom,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickUom : default(string)))
                .ForMember(dest => dest.UomEntered,
                    opt => opt.MapFrom(src => src.Item4 != null ? src.Item4.PickUom : default(string)))
                .ForAllOtherMembers(opt => opt.Ignore());
        }
     

        private string GetTaskType(string pickType, string ordertype)
        {
            if (string.Equals(pickType, Constants.PickType.Order, StringComparison.CurrentCultureIgnoreCase))
                return Constants.TaskType.OrderPick;
            if (string.Equals(pickType, Constants.PickType.Batch, StringComparison.CurrentCultureIgnoreCase) &&
                !string.Equals(ordertype, "K", StringComparison.CurrentCultureIgnoreCase))
                return Constants.TaskType.BatchPick;
            return Constants.TaskType.PickTask;
        }

        private void CloneSubTaskMapping()
        {
            CreateMap<SubTask, SubTask>();
        }
    }
}