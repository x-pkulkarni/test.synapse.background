﻿// <copyright file="Extension.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Mansoori, Imran</author> 
// <createddate>2019-04-19</createddate>
// <lastchangedby>Mansoori, Imran</lastchangedby>
// <lastchangeddate>2019-04-19</lastchangeddate>


namespace Synapse.Backgrounds.Core.Common
{
    using System.Collections.Generic;
    using System.Linq;

    public static class Extension
    {
        public static bool In<T>(this T o, params T[] values)
        {
            if (values == null)
            {
                return false;
            }

            return values.Contains(o);
        }

        public static bool In<T>(this T o, IEnumerable<T> values)
        {
            if (values == null)
            {
                return false;
            }

            return values.Contains(o);
        }
    }
}