﻿// <copyright file="ReleaseWaveCommandHandler.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using System.Data;
using System.Threading.Tasks;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Handlers.Interface;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Grains.Contract.Command;

namespace Synapse.Backgrounds.Core.Handlers
{
    internal class ReleaseWaveCommandHandler : ICommandHandler
    {
        private readonly IWaveAggregate _waveDomain;

        public ReleaseWaveCommandHandler(IWaveAggregate waveDomain)
        {
            _waveDomain = waveDomain;
        }

        public Task HandleAsync(IDbConnection connection, ICommand command)
        {
            _waveDomain.ExecuteReleaseWave(connection,command);
            return Task.CompletedTask;
        }

        public void Validate(ICommand command)
        {
            throw new NotImplementedException();
        }
    }
}