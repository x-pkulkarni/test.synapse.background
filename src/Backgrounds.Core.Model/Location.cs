﻿// <copyright file="Location.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-16</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-13</lastchangeddate>

namespace Synapse.Backgrounds.Core.Model
{
    public class Location
    {
        public string LocId { get; set; }

        public string Facility { get; set; }

        public string Section { get; set; }

        public string Equipprof { get; set; }

        public int PickingSeq { get; set; }

        public string PickingZone { get; set; }
    }
}
