﻿// <copyright file="SubTaskModel.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-11</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-11</lastchangeddate>


using System;

namespace Synapse.Backgrounds.Core.Model
{
    public class SubTask
    {
        public string RowId { get; set; }

        public long TaskId { get; set; }

        public string TaskType { get; set; }

        public string Facility { get; set; }

        public string FromSection { get; set; }

        public string FromLocation { get; set; }

        public string FromProfile { get; set; }

        public string ToSection { get; set; }

        public string ToLocation { get; set; }

        public string ToProfile { get; set; }

        public int ToUserId { get; set; }

        public string CustId { get; set; }

        public string Item { get; set; }

        public string LpId { get; set; }

        public string UOM { get; set; }

        public int Quantity { get; set; }

        public int LocSeq { get; set; }

        public int? LoadNo { get; set; }

        public int? StopNo { get; set; }

        public int? ShipNo { get; set; }
        
        public int OrderId { get; set; }

        public int ShipId { get; set; }

        public string OrderItem { get; set; }

        public string OrderLot { get; set; }

        public string Priority { get; set; }

        public string PrevPriority { get; set; }

        public int CurrentUserId { get; set; }

        public string LastUser { get; set; }

        public DateTime LastUpdate { get; set; }

        public string PickUom { get; set; }

        public int PickQty { get; set; }

        public string PickToType { get; set; }

        public long Wave { get; set; }

        public string PickingZone { get; set; }

        public string CartonType { get; set; }

        public double Weight { get; set; }

        public double Cube { get; set; }

        public double StaffHrs { get; set; }

        public int CartonSeq { get; set; }

        public string ShippingLpId { get; set; }

        public string ShippingType { get; set; }

        public string CartonGroup { get; set; }

        public string LabelUom { get; set; }
        public string LtlFelSeq { get; set; }

    }
}
