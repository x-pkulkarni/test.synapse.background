﻿// <copyright file="CustItem.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-13</lastchangeddate>

namespace Synapse.Backgrounds.Core.Model
{
    public class CustItem
    {
        public double Cube { get; set; }

        public string CustId { get; set; }

        public string Item { get; set; }

        public string BaseUom { get; set; }

        public double weight { get; set; }

        public string IataprimaryChemCode { get; set; }

        public int Variancepct { get; set; }

        public string IsKit { get; set; }

        public string ExpdateRequired { get; set; }
    }
}