﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Backgrounds.Core.Model
{
    public class AppLockRequest
    {
        public string LockId { get; set; }

        public string CustomerId { get; set; }

        public string Facility { get; set; }

        public long OrderId { get; set; }

        public string UserId { get; set; }

    }
}
