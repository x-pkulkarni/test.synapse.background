﻿// <copyright file="OrderHeaderModel.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-10</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-03-23</lastchangeddate>

using System;

namespace Synapse.Backgrounds.Core.Model
{
    public class OrderHeader
    {
        public long OrderId { get; set; }

        public int ShipId { get; set; }

        public string CustomerId { get; set; }

        public string OrderType { get; set; }

        public DateTime EntryDate { get; set; }

        public DateTime ApptDate { get; set; }

        public DateTime ShipDate { get; set; }

        public int PO { get; set; }

        public string RMA { get; set; }

        public string OrderStatus { get; set; }

        public string CommitStatus { get; set; }

        public string FromFacility { get; set; }

        public string ToFacility { get; set; }

        public long Wave { get; set; }

        public int? ShipNo { get; set; }

        public int? LoadNo { get; set; }

        public int? StopNo { get; set; }

        public string ShipTo { get; set; }

        public string StageLoc { get; set; }

        public string Priority { get; set; }

        public string LastUser { get; set; }

        public string Carrier { get; set; }

        public string ShipType { get; set; }

        public bool IsMultiShip { get; set; }

        public bool IsFelConnectShip { get; set; }

        public bool IsLtlFel { get; set; }

        public int QtyPick { get; set; }

        public string ComponentTemplate { get; set; }

    }
}
