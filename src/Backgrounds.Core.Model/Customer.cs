﻿// <copyright file="Customer.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-23</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

namespace Synapse.Backgrounds.Core.Model
{
    public class Customer
    {
        public string CustId { get; set; }

        public bool IsPickByLineNumber { get; set; }

        public bool IsEnterMinDaysToExpireFlag { get; set; }
    }
}