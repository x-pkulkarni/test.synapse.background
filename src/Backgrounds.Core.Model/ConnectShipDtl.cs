﻿// <copyright file="ConnectShipDtl.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Mansoori, Imran</author> 
// <createddate>2019-04-22</createddate>
// <lastchangedby>Mansoori, Imran</lastchangedby>
// <lastchangeddate>2019-04-22</lastchangeddate>



namespace Synapse.Backgrounds.Core.Model
{
    public class ConnectShipDtl
    {
        public string CustId { get; set; }
      public string Facility { get; set; }
        public string LpId { get; set; }
        public long OrderId { get; set; }
        public int ShipId { get; set; }
        public long Wave { get; set; }
        public long TaskId { get; set; }

        public string Item { get; set; }
        public string ItemDescription { get; set; }
        public double UnitValue { get; set; }
        public string HarmonizedTariff { get; set; }
        public string CountryOfOrigin { get; set; }
        public int BaseQuantity { get; set; }
        public string BaseUom { get; set; }
        public double BaseUomUnitWeight { get; set; }
        public int? PickQuantity { get; set; }
        public string PickUom { get; set; }
        public double PickUomUnitWeight { get; set; }
        public string LicenseNum { get; set; }
        public string LicenseExpDate { get; set; }
        public string UserId { get; set; }
        public string HarmonizedCode { get; set; }
        public string HazmatDescription { get; set; }
        public string HazmatClass { get; set; }
        public string HazmatId { get; set; }
        public string HazmatTechnicalName { get; set; }
        public string HazmatPackingGroup { get; set; }
        public string HazmatAbbrev { get; set; }
        public int BatteryCount { get; set; }
        public int CellCount { get; set; }
        public double LithiumWeight { get; set; }
        public double BatteryWattHrs { get; set; }

    }
}
