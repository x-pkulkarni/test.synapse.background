﻿// <copyright file="ItemDemand.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-03-07</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-03-07</lastchangeddate>

namespace Synapse.Backgrounds.Core.Model
{
    public class ItemDemand
    {
        public string Facility { get; set; }

        public string Item { get; set; }

        public string LotNumber { get; set; }

        public string Priority { get; set; }

        public string InvStatusInd { get; set; }

        public string InvClassInd { get; set; }

        public string InvStatus { get; set; }

        public string InventoryClass { get; set; }

        public string DemandType { get; set; }

        public long OrderId { get; set; }

        public int ShipId { get; set; }

        public int? LoadNo { get; set; }

        public int? StopNo { get; set; }

        public int? ShipNo { get; set; }

        public string OrderItem { get; set; }

        public string OrderLot { get; set; }

        public int Qty { get; set; }

        public string LastUser { get; set; }

        public string CustId { get; set; }
    }
}