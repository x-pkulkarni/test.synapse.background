﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Backgrounds.Core.Model
{
    public class ParcelStaging
    {
        public string CustId { get; set; }
        public string Facility { get; set; }
        public string LpId { get; set; }
        public long OrderId { get; set; }
        public int ShipId { get; set; }
        public long Wave { get; set; }
        public long TaskId { get; set; }
        public int PickQty { get; set; }
        public string Item { get; set; }
        public string MeterNumber { get; set; }
        public string EdiMap { get; set; }
        public string Carrier { get; set; }
        public string DeliveryService { get; set; }
        public char Status { get; set; }
        public string Uom { get; set; }
        public int Qty { get; set; }
        public int? LtlSeq { get; set; }
        public string PickUom { get; set; }
        public string RowId { get; set; }
        public string CanMap { get; set; }
        public string MultiShip { get; set; }
        public string FelConnectShip { get; set; }
    }
}
