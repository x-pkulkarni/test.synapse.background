﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Backgrounds.Core.Model
{
    public class WaveItem
    {
        public long OrderId { get; set; }

        public int ShipId { get; set; }

        public string CustomerId { get; set; }

        public int QtyEntered { get; set; }

        public string CarrierCode { get; set; }

        public string Item { get; set; }

        public string ShipToState { get; set; }

        public string ShipToCountryCode { get; set; }

        public int PickSetQty { get; set; }

        public string Hazardous { get; set; }

        public string MultiShip { get; set; }

    }
}
