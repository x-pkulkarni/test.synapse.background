﻿// <copyright file="TaskGroupingCounter.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-08-20</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-20</lastchangeddate>


namespace Synapse.Backgrounds.Core.Model
{
    public class TaskGroupingCounter
    {
        public string ZoneId { get; set; }
        public string Facility { get; set; }
        public int BaseLimit { get; set; }

        public int Counter { get; set; }
        public Task Task { get; set; }
    }
}