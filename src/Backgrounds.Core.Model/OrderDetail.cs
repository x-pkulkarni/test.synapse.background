﻿// <copyright file="OrderDetail.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-10</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-03-23</lastchangeddate>

namespace Synapse.Backgrounds.Core.Model
{
    public class OrderDetail
    {
        public long OrderId { get; set; }

        public int ShipId { get; set; }

        public string Item { get; set; }

        public string LotNumber { get; set; }

        public string InvStatus { get; set; }

        public string QuantityType { get; set; }

        public string InventoryClass { get; set; }

        public string CustomerId { get; set; }

        public string FromFacility { get; set; }

        public string Uom { get; set; }

        public string LineStatus { get; set; }

        public string CommitStatus { get; set; }

        public int QtyEntered { get; set; }

        public string ItemEntered { get; set; }

        public string UomEntered { get; set; }

        public int QtyOrder { get; set; }

        public double WeightOrder { get; set; }

        public double CubeOrder { get; set; }

        public int AmtOrder { get; set; }

        public int QtyCommit { get; set; }

        public string Vendor { get; set; }

        public int QtyPick { get; set; }

        public int XDockOrderId { get; set; }

        public int XDockShipId { get; set; }

        public string Priority { get; set; }

        public double StaffHrs { get; set; }

        public string LastUser { get; set; }

        public string IataPrimaryChemCode { get; set; }

        public int MinDaysToExpiration { get; set; }

        public string DynamicInvUsedFlag { get; set; }

        public string InvStatusInd { get; set; }

        public string InvClassInd { get; set; }
        
    }
}