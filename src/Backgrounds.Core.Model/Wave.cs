﻿// <copyright file="WaveModel.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-10</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-10</lastchangeddate>


using System;

namespace Synapse.Backgrounds.Core.Model
{
    public class Wave
    {
        public long Id { get; set; }

        public string Description { get; set; }

        public string Status { get; set; }

        public string ScheduleRelease { get; set; }

        public string ActualRelease { get; set; }

        public string Facility { get; set; }

        public string LastUser { get; set; }

        public DateTime  LastUpdate { get; set; }

        public string StageLocation { get; set; }

        public string PickType { get; set; }

        public string TaskPriority { get; set; }

        public string SortLocation { get; set; }

        public string Job { get; set; }

        public bool IsConsolidated { get; set; }

        public string PickToTypeRule { get; set; }

        public string PreferredPickZone { get; set; }

        public string BatchCartonType { get; set; }

        public bool IsFrontEndLabel { get; set; }

        public string CartonGroup { get; set; }

        public bool IsLtlFrontEndLabel { get; set; }

        public bool IsBulkReturn { get; set; }

        public bool IsStationFel { get; set; }

        public bool IsTransmitToPackSize { get; set; }

        public bool IsGenWfelLabels { get; set; }

        public bool IsSplitTasksForMultiPickers { get; set; }

    }
}
