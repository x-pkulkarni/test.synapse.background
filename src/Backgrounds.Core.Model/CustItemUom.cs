﻿// <copyright file="CustItemUom.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

namespace Synapse.Backgrounds.Core.Model
{
    public class CustItemUom
    {
        public int Cube { get; set; }

        public string CustId { get; set; }
        public string Sequence { get; set; }

        public string Item { get; set; }

        public string ToUom { get; set; }

        public string FromUom { get; set; }

        public int Qty { get; set; }
        public int BaseQty { get; set; }
    }
}