﻿// <copyright file="Container.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Kathiresan, Murugan</author> 
// <createddate>2019-01-30</createddate>
namespace Synapse.Backgrounds.Core.Model
{
    using System;

    public class Container
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the container ID.
        /// </summary>
        /// <value>
        ///     The container ID.
        /// </value>
        public Guid ContainerID { get; set; }

        /// <summary>
        ///     Gets or sets the container ID.
        /// </summary>
        /// <value>
        ///     The container code.
        /// </value>
        public string Code { get; set; }

        /// <summary>
        ///     Gets or sets the container ID.
        /// </summary>
        /// <value>
        ///     The container description.
        /// </value>
        public string ContainerDescription { get; set; }

        /// <summary>
        ///     Gets or sets the container length.
        /// </summary>
        /// <value>
        ///     The container length.
        /// </value>
        public decimal Length { get; set; }

        /// <summary>
        ///     Gets or sets the container width.
        /// </summary>
        /// <value>
        ///     The container width.
        /// </value>
        public decimal Width { get; set; }

        /// <summary>
        ///     Gets or sets the container height.
        /// </summary>
        /// <value>
        ///     The container height.
        /// </value>
        public decimal Height { get; set; }

        /// <summary>
        ///     Gets or sets the volume of the container.
        /// </summary>
        /// <value>
        ///     The volume of the container.
        /// </value>
        public decimal Volume { get; set; }


        /// <summary>
        ///     Gets or sets the container weight.
        /// </summary>
        /// <value>
        ///     The container weight.
        /// </value>
       // public decimal Weight { get; set; }


        /// <summary>
        ///     Gets or sets the container max weight.
        /// </summary>
        /// <value>
        ///     The container max weight.
        /// </value>
        public decimal MaxWeight { get; set; }


        /// <summary>
        ///     Gets or sets the max cube
        /// </summary>
        /// <value>
        ///     The container max cube.
        /// </value>
        public decimal MaxCube { get; set; }

        /// <summary>
        ///     Gets or sets the max qty.
        /// </summary>
        /// <value>
        ///     The container max qty.
        /// </value>
        //public decimal MaxQty { get; set; }

        #endregion Public Properties
    }
}
