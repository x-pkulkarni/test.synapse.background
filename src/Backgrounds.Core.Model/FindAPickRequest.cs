﻿// <copyright file="FindAPickRequest.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-11</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-11</lastchangeddate>



namespace Synapse.Backgrounds.Core.Model
{
    public class FindAPickRequest
    {
        public string FromFacility { get; set; }

        public string CustomerId { get; set; }

        public string Item { get; set; }

        public string LotNumber { get; set; }

        public string InvStatus { get; set; }

        public string InventoryClass { get; set; }

        public int Quantity { get; set; }

        public string QuantityType { get; set; }

        public int Variancepct { get; set; }

        public string PickUom { get; set; }

        public string ForReplenishmentRequest { get; set; }

        public string StorageOrStage { get; set; }

        public string ExpirationDtRequired { get; set; }

        public bool IsEnterMindDaysToExpire { get; set; }

        public int MindDaysToExpiration { get; set; }

        public string WavePickZone { get; set; }

        public string Vendor { get; set; }

        public int OrderId { get; set; }

        public int ShipId { get; set; }

        public string PfLoc { get; set; }

        public string SlotWaveRequired { get; set; }

        public string DynamicAllocMode { get; set; }

        public string AllocNeed { get; set; }

        public string PickToTypeRule { get; set; }

        public string Trace { get; set; }

    }
}
