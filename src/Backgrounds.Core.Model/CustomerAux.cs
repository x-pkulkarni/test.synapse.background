﻿// <copyright file="CustomerAux.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

namespace Synapse.Backgrounds.Core.Model
{
    public class CustomerAux
    {
        public int CartonFillPercentage { get; set; }

        public bool IsCartonFillPercentageFlag { get; set; }

        public bool IsUseItemUncodeFromOrder { get; set; }

        public string Custid { get; set; }
        public bool IsIataSingleUnitOk { get; set; }
        public bool IsIataCarrierOverride { get; set; }
        public bool IsCartonEnabled { get; set; }
        public bool ISFelConnectship { get; set; }
        public string StandardLabelOverrideColumn { get; set; }
    }
}