﻿// <copyright file="ConnectShipSubTaskQty.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Mansoori, Imran</author> 
// <createddate>2019-04-22</createddate>
// <lastchangedby>Mansoori, Imran</lastchangedby>
// <lastchangeddate>2019-04-22</lastchangeddate>



namespace Synapse.Backgrounds.Core.Model
{
    
    public class ConnectShipSubTaskQtyDetails
    {
        public int PickQuantity { get; set; }
        public int CartonCount { get; set; }

    }
}
