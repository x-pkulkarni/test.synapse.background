﻿// <copyright file="ConnectshipOrderHdr.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Mansoori, Imran</author> 
// <createddate>2019-04-19</createddate>
// <lastchangedby>Mansoori, Imran</lastchangedby>
// <lastchangeddate>2019-04-19</lastchangeddate>



namespace Synapse.Backgrounds.Core.Model
{
    using System;

    public class ConnectShipOrderHdr
    {
        public long OrderId { get; set; }
        public int ShipId { get; set; }
        public string CustId { get; set; }
        public string FromFacility { get; set; }
        public long Wave { get; set; }
        public string Carrier { get; set; }
        public string DeliveryService { get; set; }
        public string Reference { get; set; }
        public string Po { get; set; }
        public string ShipToName { get; set; }
        public string ShipToContact { get; set; }
        public string ShipToAddr1 { get; set; }
        public string ShipToAddr2 { get; set; }
        public string ShipToAddr3 { get; set; }
        public string ShipToCity { get; set; }
        public string ShipToState { get; set; }
        public string ShipToPostalCode { get; set; }
        public string ShipToCountryCode { get; set; }
        public string ShipToPhone { get; set; }
        public string ShipToEmail { get; set; }
        public string Residential { get; set; }
        public string SaturdayDelivery { get; set; }
        public string SpecialService1 { get; set; }
        public string ShipTerms { get; set; }
        public string BillToName { get; set; }
        public string BillToContact { get; set; }
        public string BillToAddr1 { get; set; }
        public string BillToAddr2 { get; set; }
        public string BillToAddr3 { get; set; }
        public string BillToCity { get; set; }
        public string BillToState { get; set; }
        public string BillToPostalCode { get; set; }
        public string BillToCountryCode { get; set; }
        public string BillToPhone { get; set; }
        public string BillToEmail { get; set; }
        public DateTime? ShipDate { get; set; }
        public string Cod { get; set; }
        public double AmtCod { get; set; }
        public bool IsCodCashierCheckMoOnly { get; set; }
        public string CodCollectAccount { get; set; }
        public bool IsCompanyCheckOk { get; set; }
        public string HdrPassThruChar01 { get; set; }
        public string HdrPassThruChar02 { get; set; }
        public string HdrPassThruChar03 { get; set; }
        public string HdrPassThruChar04 { get; set; }
        public string HdrPassThruChar05 { get; set; }
        public string HdrPassThruChar06 { get; set; }
        public string HdrPassThruChar07 { get; set; }
        public string HdrPassThruChar08 { get; set; }
        public string HdrPassThruChar09 { get; set; }
        public string HdrPassThruChar10 { get; set; }
        public string HdrPassThruChar11 { get; set; }
        public string HdrPassThruChar12 { get; set; }
        public string HdrPassThruChar13 { get; set; }
        public string HdrPassThruChar14 { get; set; }
        public string HdrPassThruChar15 { get; set; }
        public string HdrPassThruChar16 { get; set; }
        public string HdrPassThruChar17 { get; set; }
        public string HdrPassThruChar18 { get; set; }
        public string HdrPassThruChar19 { get; set; }
        public string HdrPassThruChar20 { get; set; }
        public double HdrPassThruNum01 { get; set; }
        public double HdrPassThruNum02 { get; set; }
        public double HdrPassThruNum03 { get; set; }
        public double HdrPassThruNum04 { get; set; }
        public double HdrPassThruNum05 { get; set; }
        public double HdrPassThruNum06 { get; set; }
        public double HdrPassThruNum07 { get; set; }
        public double HdrPassThruNum08 { get; set; }
        public double HdrPassThruNum09 { get; set; }
        public double HdrPassThruNum10 { get; set; }
        public DateTime HdrPassThruDate01 { get; set; }
        public DateTime HdrPassThruDate02 { get; set; }
        public DateTime HdrPassThruDate03 { get; set; }
        public DateTime HdrPassThruDate04 { get; set; }
        public double HdrPassThruDoll01 { get; set; }
        public double HdrPassThruDoll02 { get; set; }
        public bool IsMultiShip { get; set; }
        public string ConnectShipServiceCode { get; set; }
        public string ThirdPartyNumberPassThru { get; set; }
        public string ShipperNumberPassThru { get; set; }
        public string AccountNumber { get; set; }
        public string DeclaredValuePassThru { get; set; }
        public string Description { get; set; }
        public string SedMethodPassThru { get; set; }
        public string UltimateCountryPassThru { get; set; }
        public string CarrierInstructionsPassThru { get; set; }
        public string CiMethodPassThru { get; set; }
        public string Printer { get; set; }
        public string UnitValuePassThru { get; set; }
        public string HarmonizedTariffPassThru { get; set; }
        public string LicenseNumPassThru { get; set; }
        public string LicenseExpDatePassThru { get; set; }
        public string BarCodePos_11_12 { get; set; }
        public string Gc3HdrPassThruChar01 { get; set; }
        public string Gc3HdrPassThruChar02 { get; set; }
        public string Gc3HdrPassThruChar03 { get; set; }
        public string Gc3HdrPassThruChar04 { get; set; }
        public string Gc3HdrPassThruChar05 { get; set; }
        public string Gc3HdrPassThruChar06 { get; set; }
        public string Gc3HdrPassThruChar07 { get; set; }
        public string Gc3HdrPassThruChar08 { get; set; }
        public string Gc3HdrPassThruChar09 { get; set; }
        public string Gc3HdrPassThruChar10 { get; set; }
        public string Gc3HdrPassThruChar11 { get; set; }
        public string Gc3HdrPassThruChar12 { get; set; }
        public string Gc3HdrPassThruChar13 { get; set; }
        public string Gc3HdrPassThruChar14 { get; set; }
        public string Gc3HdrPassThruChar15 { get; set; }
        public string Gc3HdrPassThruChar16 { get; set; }
        public string Gc3HdrPassThruChar17 { get; set; }
        public string Gc3HdrPassThruChar18 { get; set; }
        public string Gc3HdrPassThruChar19 { get; set; }
        public string Gc3HdrPassThruChar20 { get; set; }
        public double Gc3HdrPassThruNum01 { get; set; }
        public double Gc3HdrPassThruNum02 { get; set; }
        public double Gc3HdrPassThruNum03 { get; set; }
        public double Gc3HdrPassThruNum04 { get; set; }
        public double Gc3HdrPassThruNum05 { get; set; }
        public double Gc3HdrPassThruNum06 { get; set; }
        public double Gc3HdrPassThruNum07 { get; set; }
        public double Gc3HdrPassThruNum08 { get; set; }
        public double Gc3HdrPassThruNum09 { get; set; }
        public double Gc3HdrPassThruNum10 { get; set; }
        public DateTime Gc3HdrPassThruDate01 { get; set; }
        public DateTime Gc3HdrPassThruDate02 { get; set; }
        public DateTime Gc3HdrPassThruDate03 { get; set; }
        public DateTime Gc3HdrPassThruDate04 { get; set; }
        public double Gc3HdrPassThruDoll01 { get; set; }
        public double Gc3HdrPassThruDoll02 { get; set; }
        public string AsnHdrPassThruChar01 { get; set; }
        public string AsnHdrPassThruChar02 { get; set; }
        public string AsnHdrPassThruChar03 { get; set; }
        public string AsnHdrPassThruChar04 { get; set; }
        public string AsnHdrPassThruChar05 { get; set; }
        public string AsnHdrPassThruChar06 { get; set; }
        public string AsnHdrPassThruChar07 { get; set; }
        public string AsnHdrPassThruChar08 { get; set; }
        public string AsnHdrPassThruChar09 { get; set; }
        public string AsnHdrPassThruChar10 { get; set; }
        public string AsnHdrPassThruChar11 { get; set; }
        public string AsnHdrPassThruChar12 { get; set; }
        public string AsnHdrPassThruChar13 { get; set; }
        public string AsnHdrPassThruChar14 { get; set; }
        public string AsnHdrPassThruChar15 { get; set; }
        public string AsnHdrPassThruChar16 { get; set; }
        public string AsnHdrPassThruChar17 { get; set; }
        public string AsnHdrPassThruChar18 { get; set; }
        public string AsnHdrPassThruChar19 { get; set; }
        public string AsnHdrPassThruChar20 { get; set; }
        public double AsnHdrPassThruNum01 { get; set; }
        public double AsnHdrPassThruNum02 { get; set; }
        public double AsnHdrPassThruNum03 { get; set; }
        public double AsnHdrPassThruNum04 { get; set; }
        public double AsnHdrPassThruNum05 { get; set; }
        public double AsnHdrPassThruNum06 { get; set; }
        public double AsnHdrPassThruNum07 { get; set; }
        public double AsnHdrPassThruNum08 { get; set; }
        public double AsnHdrPassThruNum09 { get; set; }
        public double AsnHdrPassThruNum10 { get; set; }
        public DateTime AsnHdrPassThruDate01 { get; set; }
        public DateTime AsnHdrPassThruDate02 { get; set; }
        public DateTime AsnHdrPassThruDate03 { get; set; }
        public DateTime AsnHdrPassThruDate04 { get; set; }
        public double AsnHdrPassThruDoll01 { get; set; }
        public double AsnHdrPassThruDoll02 { get; set; }

        public bool IsReplacementOrder { get; set; }
        public string ReturnToName { get; set; }
        public string ReturnToContact { get; set; }
        public string ReturnToAddr1 { get; set; }
        public string ReturnToAddr2 { get; set; }
        public string ReturnToAddr3 { get; set; }
        public string ReturnToCity { get; set; }
        public string ReturnToState { get; set; }
        public string ReturnToPostalCode { get; set; }
        public string ReturnToCountryCode { get; set; }
        public string ReturnToPhone { get; set; }
        public string ReturnToFax { get; set; }
        public string ReturnToEmail { get; set; }
        public string ReturndCName { get; set; }
        public string ReturndCContact { get; set; }
        public string ReturndCAddr1 { get; set; }
        public string ReturndCAddr2 { get; set; }
        public string ReturndCAddr3 { get; set; }
        public string ReturndCCity { get; set; }
        public string ReturndCState { get; set; }
        public string ReturndCPostalCode { get; set; }
        public string ReturndCCountryCode { get; set; }
        public string ReturndCPhone { get; set; }
        public string ReturndCFax { get; set; }
        public string ReturndCEmail { get; set; }

        public string EdiAuditChar01 { get; set; }
        public string EdiAuditChar02 { get; set; }
        public string EdiAuditChar03 { get; set; }
        public string EdiAuditChar04 { get; set; }
        public string EdiAuditChar05 { get; set; }
        public string EdiAuditChar06 { get; set; }
        public string EdiAuditChar07 { get; set; }
        public string EdiAuditChar08 { get; set; }
        public string EdiAuditChar09 { get; set; }
        public string EdiAuditChar10 { get; set; }
        public string EdiAuditChar11 { get; set; }
        public string EdiAuditChar12 { get; set; }
        public string EdiAuditChar13 { get; set; }
        public string EdiAuditChar14 { get; set; }
        public string EdiAuditChar15 { get; set; }
        public string EdiAuditChar16 { get; set; }
        public string EdiAuditChar17 { get; set; }
        public string EdiAuditChar18 { get; set; }
        public string EdiAuditChar19 { get; set; }
        public string EdiAuditChar20 { get; set; }
        public bool IsBulkReturn { get; set; }

    }
}
