﻿// <copyright file="ConnectShipPack.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Mansoori, Imran</author> 
// <createddate>2019-04-19</createddate>
// <lastchangedby>Mansoori, Imran</lastchangedby>
// <lastchangeddate>2019-04-19</lastchangeddate>



namespace Synapse.Backgrounds.Core.Model
{
    using System;

    public class ConnectShipPackLable
    {
        public long TaskId { get; set; }
        public int PickQty { get; set; }
        public string Item { get; set; }
        public string OrderItem { get; set; }
        public string OrderLot { get; set; }
        public double Weight { get; set; }
        public string PickUom { get; set; }
        public string CartonType { get; set; }
        public string Uom { get; set; }
        public string StRowId { get; set; }
        public int Cartonseq { get; set; }
        public int Qty { get; set; }
        public string AtRowId { get; set; }
        public long CpackJobId { get; set; }
        public string ContainerType { get; set; }
        public double ContainerLength { get; set; }
        public double ContainerWidth { get; set; }
        public double ContainerHeight { get; set; }
        public double ContainerWeight { get; set; }
        public string Descr { get; set; }
        public int QtyOrder { get; set; }
        public string CountryOf { get; set; }

        public string FromLoc { get; set; }

        public string DtlPassThruChar02 { get; set; }
        public string DtlPassThruChar03 { get; set; }
        public string DtlPassThruChar05 { get; set; }
        public string DtlPassThruChar11 { get; set; }
        public string DtlPassThruChar13 { get; set; }
        public string DtlPassThruChar16 { get; set; }
        public string DtlPassThruChar17 { get; set; }
        public string DtlPassThruChar18 { get; set; }
        public string DtlPassThruChar19 { get; set; }
        public double DtlPassThruNum04 { get; set; }
        public DateTime DtlPassThruDate01 { get; set; }

        public double DtlPassThruNum01 { get; set; }
        public double DtlPassThruNum02 { get; set; }
        public double DtlPassThruNum03 { get; set; }
        public double DtlPassThruNum05 { get; set; }
        public double DtlPassThruNum06 { get; set; }
        public double DtlPassThruNum07 { get; set; }
        public double DtlPassThruNum08 { get; set; }
        public double DtlPassThruNum09 { get; set; }
        public double DtlPassThruNum10 { get; set; }
        public string DtlPassThruChar01 { get; set; }
        public string DtlPassThruChar04 { get; set; }
        public string DtlPassThruChar06 { get; set; }
        public string DtlPassThruChar07 { get; set; }
        public string DtlPassThruChar08 { get; set; }
        public string DtlPassThruChar09 { get; set; }
        public string DtlPassThruChar10 { get; set; }
        public string DtlPassThruChar12 { get; set; }
        public string DtlPassThruChar14 { get; set; }
        public string DtlPassThruChar15 { get; set; }
        public string DtlPassThruChar20 { get; set; }
        public DateTime DtlPassThruDate02 { get; set; }
        public DateTime DtlPassThruDate03 { get; set; }
        public DateTime DtlPassThruDate04 { get; set; }
        public double DtlPassThruDoll01 { get; set; }
        public double DtlPassThruDoll02 { get; set; }
        public string BolComment { get; set; }
        public string HarmonizedCode { get; set; }
        public string HazmatDescription { get; set; }
        public string HazmatId { get; set; }
        public string HazmatTechnicalName { get; set; }
        public string HazmatPackingGroup { get; set; }
        public string HazmatAbbrev { get; set; }
        public string HazmatClass { get; set; }
        public string PickingZone { get; set; }
        public string Aisle { get; set; }
        public int CustItemBatteryCount { get; set; }
        public int CustItemCellCount { get; set; }
        public double CustItemLithiumWeight { get; set; }
        public double CustItemBatteryWattHrs { get; set; }
        public string CustItemIataPrimaryChemCode { get; set; }
        public string CustItemPrimaryHazardClass { get; set; }
        public int OrderDtlBatteryCount { get; set; }
        public int OrderDtlCellCount { get; set; }
        public double OrderDtlLithiumWeight { get; set; }
        public double OrderDtlBatteryWattHrs { get; set; }
        public string OrderDtlIataPrimaryChemCode { get; set; }
        public string OdlDtlPassThruChar13 { get; set; }
        public string OdlDtlPassThruChar16 { get; set; }
        public string OdlDtlPassThruChar17 { get; set; }

    }
}
