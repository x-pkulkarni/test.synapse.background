﻿// <copyright file="WaveDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-10</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-10</lastchangeddate>

namespace Synapse.Backgrounds.Core.Model
{
    public class ConnectShipOrder
    {
        public long OrderId { get; set; }

        public int ShipId { get; set; }

        public string CustomerId { get; set; }

        public string FelConnectShip { get; set; }

        public string Ltlfel { get; set; }

        public string MultiShip { get; set; }
    }
}