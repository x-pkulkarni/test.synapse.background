﻿// <copyright file="OrderTo.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-03-23</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-03-23</lastchangeddate>

namespace Synapse.Backgrounds.Core.Model
{
    public class OrderTo
    {
        public Wave WaveInfo { get; set; }
        public OrderHeader HeaderInfo { get; set; }
        public OrderDetail OrderDetail { get; set; }
        public int Quantity { get; set; }
    }
}