﻿// <copyright file="Item.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Kathiresan, Murugan</author> 
// <createddate>2019-01-30</createddate>
namespace Synapse.Backgrounds.Core.Model
{
    using System;

    /// <summary>
    ///     An item to be packed. Also used to hold post-packing details for the item.
    /// </summary>
    public class Item
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the item name.
        /// </summary>
        /// <value>
        ///     The item name.
        /// </value>
        public Guid ItemID { get; set; }

        /// <summary>
        ///     Gets or sets the item description.
        /// </summary>
        /// <value>
        ///     The item name.
        /// </value>
        public string ItemDescription { get; set; }

        /// <summary>
        ///     Gets or sets the item name.
        /// </summary>
        /// <value>
        ///     The item name.
        /// </value>
        public Guid CavityID { get; set; }

        /// <summary>
        ///     Gets or sets the item description.
        /// </summary>
        /// <value>
        ///     The item name.
        public string CavityDescription { get; set; }

        /// <summary>
        ///     Gets or sets the item ID.
        /// </summary>
        /// <value>
        ///     The item sequence.
        /// </value>
        public int ItemSequence { get; set; }

        /// <summary>
        ///     Gets or sets the item quantity.
        /// </summary>
        /// <value>
        ///     The item quantity.
        /// </value>
        public int ItemQuantity { get; set; }

        /// <summary>
        ///     Gets or sets the item pick type.
        /// </summary>
        /// <value>
        ///     The item pick type.
        /// </value>
        public string PickToType { get; set; }

        /// <summary>
        ///     Gets or sets the length of one of the item dimensions.
        /// </summary>
        /// <value>
        ///     The first item dimension.
        /// </value>
        public decimal Width { get; set; }

        /// <summary>
        ///     Gets or sets the length another of the item dimensions.
        /// </summary>
        /// <value>
        ///     The second item dimension.
        /// </value>
        public decimal Length { get; set; }

        /// <summary>
        ///     Gets or sets the third of the item dimensions.
        /// </summary>
        /// <value>
        ///     The third item dimension.
        /// </value>
        public decimal Height { get; set; }


        /// <summary>
        ///     Gets or sets the third of the item dimensions.
        /// </summary>
        /// <value>
        ///     The third item dimension.
        /// </value>
        public decimal Weight { get; set; }

        /// <summary>
        ///     Gets or sets the third of the item dimensions.
        /// </summary>
        /// <value>
        ///     The third item dimension.
        /// </value>
        public decimal MaxWeight { get; set; }


        /// <summary>
        ///     Gets or sets the volume of the container.
        /// </summary>
        /// <value>
        ///     The volume of the container.
        /// </value>
        public decimal Volume { get; set; }

        /// <summary>
        ///     Gets or sets the x coordinate of the location of the packed item within the container.
        /// </summary>
        /// <value>
        ///     The x coordinate of the location of the packed item within the container.
        /// </value>
        public decimal CoordX { get; set; }

        /// <summary>
        ///     Gets or sets the y coordinate of the location of the packed item within the container.
        /// </summary>
        /// <value>
        ///     The y coordinate of the location of the packed item within the container.
        /// </value>
        public decimal CoordY { get; set; }

        /// <summary>
        ///     Gets or sets the z coordinate of the location of the packed item within the container.
        /// </summary>
        /// <value>
        ///     The z coordinate of the location of the packed item within the container.
        /// </value>
        public decimal CoordZ { get; set; }

        /// <summary>
        ///     Gets or sets the x dimension of the orientation of the item as it has been packed.
        /// </summary>
        /// <value>
        ///     The x dimension of the orientation of the item as it has been packed.
        /// </value>
        public decimal PackDimX { get; set; }

        /// <summary>
        ///     Gets or sets the y dimension of the orientation of the item as it has been packed.
        /// </summary>
        /// <value>
        ///     The y dimension of the orientation of the item as it has been packed.
        /// </value>
        public decimal PackDimY { get; set; }

        /// <summary>
        ///     Gets or sets the z dimension of the orientation of the item as it has been packed.
        /// </summary>
        /// <value>
        ///     The z dimension of the orientation of the item as it has been packed.
        /// </value>
        public decimal PackDimZ { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating whether this item has already been packed.
        /// </summary>
        /// <value>
        ///     True if the item has already been packed; otherwise, false.
        /// </value>
        public bool IsPacked { get; set; }


        /// <summary>
        ///     Gets or sets a value indicating whether this item can be stacked
        /// </summary>
        /// <value>
        ///     True if the item is stackable; otherwise, false.
        /// </value>
        public bool IsStackable { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating if this item needs packing or shipped as it is.
        /// </summary>
        public bool ShipAsIs { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating if this item cannot mix with other items.
        /// </summary>
        public bool ShipAlone { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating how much cube my additional stack takes
        /// </summary>
        public string StackingCube { get; set; }


        /// <summary>
        ///     Gets or sets a value indicating the item can behave as a container
        /// </summary>
        public bool IsContainer { get; set; }

        /// <summary>
        ///     Gets or sets a value indicating the item can behave as a container
        /// </summary>
        public string ItemPackingGroup { get; set; }
        public string ItemUom { get; set; }

        #endregion Public Properties
    }
}
