﻿// <copyright file="ValidationRequest.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-02-21</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using System.Collections.Generic;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Model
{
    public class ValidationContext
    {
        public bool IsWaveLevel { get; set; }

        public OrderHeader OrderHeader { get; set; }

        public IList<Wave> Waves { get; set; }

        public IList<OrderDetail> OrderDetails { get; set; }

        public ICommand Command { get; set; }
    }
}