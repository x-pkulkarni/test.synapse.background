﻿// <copyright file="Plate.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-16</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using System;

namespace Synapse.Backgrounds.Core.Model
{
    public class Plate
    {
        public string LpId { get; set; }

        public string Item { get; set; }

        public string CustomerId { get; set; }

        public string Facility { get; set; }

        public string Location { get; set; }

        public string Status { get; set; }

        public string HoldReason { get; set; }

        public string UnitOfMeasure { get; set; }

        public int Quantity { get; set; }

        public string Type { get; set; }

        public long FromLpId { get; set; }

        public int SerialNumber { get; set; }

        public string LotNumber { get; set; }

        public int ParentLpId { get; set; }

        public int UserItem1 { get; set; }

        public int UserItem2 { get; set; }

        public int UserItem3 { get; set; }

        public string LastUser { get; set; }

        public DateTime LastUpdate { get; set; }

        public string InvStatus { get; set; }

        public int QtyEntered { get; set; }

        public string OrderItem { get; set; }

        public string UomEntered { get; set; }

        public string InventoryClass { get; set; }

        public int LoadNo { get; set; }

        public int StopNo { get; set; }

        public int ShipNo { get; set; }

        public long OrderId { get; set; }

        public int ShipId { get; set; }

        public double Weight { get; set; }

        public string Ucc128 { get; set; }

        public string LabelFormat { get; set; }

        public int TaskId { get; set; }

        public int DropSeq { get; set; }

        public string OrderLot { get; set; }

        public string PickUom { get; set; }

        public int PickQty { get; set; }

        public int CartonSeq { get; set; }

        public string HazmatInd { get; set; }

        public string IataPrimaryChemCode { get; set; }
    }
}