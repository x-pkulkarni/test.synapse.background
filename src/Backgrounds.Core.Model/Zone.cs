﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Backgrounds.Core.Model
{
    public class Zone
    {
        public string ZoneId { get; set; }

        public string Facility { get; set; }

        public string SeparateBatchTasksFlag { get; set; }

        public int BatchTasksLimit { get; set; }

        public string SeparateLineTasksFlag { get; set; }

        public int LineTasksLimit { get; set; }

        public string SeparateOrderTasksFlag { get; set; }

        public int OrderTasksLimit { get; set; }
    }
}
