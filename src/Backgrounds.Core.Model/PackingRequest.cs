﻿// <copyright file="PackingRequest.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Kathiresan, Murugan</author> 
// <createddate>2019-01-30</createddate>
namespace Synapse.Backgrounds.Core.Model
{
    using System.Collections.Generic;

    public class PackingRequest
    {
        #region Constructors

        public PackingRequest()
        {
            Containers = new List<Container>();
            ItemsToPack = new List<Item>();
           // PackingFlags = null;
        }

        #endregion Constructors
       
        public List<Container> Containers { get; set; }

        public List<Item> ItemsToPack { get; set; }

        public string AlgorithmCode { get; set; }
    }
}
