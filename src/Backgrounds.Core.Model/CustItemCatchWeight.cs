﻿// <copyright file="CustItemCatchWeight.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-19</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

namespace Synapse.Backgrounds.Core.Model
{
    public class CustItemCatchWeight
    {
        public string CustId { get; set; }

        public string Item { get; set; }

        public int ShipId { get; set; }

        public string Uom { get; set; }

        public int Qty { get; set; }

        public double Weight { get; set; }
    }
}