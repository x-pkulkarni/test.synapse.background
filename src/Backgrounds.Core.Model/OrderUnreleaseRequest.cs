﻿// <copyright file="OrderUnreleaseRequest.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-02-22</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-02-23</lastchangeddate>

using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Model
{
    public class OrderUnreleaseRequest
    {

        public long? WaveId { get; set; }

        public string Facility { get; set; }

        public string UserId { get; set; }

        public int OrderId { get; set; }

        public int ShipId { get; set; }

        public OrderHeader Order { get; set; }

        public ICommand Command { get; set; }
    }
}