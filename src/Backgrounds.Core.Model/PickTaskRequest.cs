﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Model
{
    public class PickTaskRequest
    {
        public OrderTo Order { get; set; }

        public Customer Customer { get; set; }
        public Commitment Commitment { get; set; }
        public CustItem CustItem { get; set; }
        public ICommand Command { get; set; }
        public int BaseQty { get; set; }
        public int OrderQty { get; set; }

        public string Item { get; set; }

        public PickTaskRequest ShallowCopy()
        {
            return (PickTaskRequest)this.MemberwiseClone();
        }
    }
}
