﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Backgrounds.Core.Model
{
    public class LtlSubTask
    {
        public long Wave { get; set; }
        public long TaskId { get; set; }
        public string Facility { get; set; }
        public string CustId { get; set; }
        public string Item { get; set; }
        public string Uom { get; set; }
        public int Qty { get; set; }
        public string PickUom { get; set; }
        public int PickQty { get; set; }
        public string Carrier { get; set; }
        public string DeliveryService { get; set; }
        public string RowId { get; set; }
    }
}
