﻿// <copyright file="FindAPickResponse.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-01-11</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-01-11</lastchangeddate>



namespace Synapse.Backgrounds.Core.Model
{
   public class FindAPickResponse
    {
        public string LpIdOrLoc { get; set; }

        public string BaseUom { get; set; }

        public int BaseQty { get; set; }

        public string PickUom { get; set; }

        public int PickQty { get; set; }

        public string PickFront { get; set; }

        public string PickToType { get; set; }

        public string CartonType { get; set; }

        public string PickType { get; set; }

        public bool IsWholeUnitsOnly { get; set; }

        public string InventoryClass { get; set; }

        public string PreventFPlates { get; set; }

        public string Message { get; set; }
    }
}
