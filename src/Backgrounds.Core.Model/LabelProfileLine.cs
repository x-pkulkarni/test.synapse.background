﻿// <copyright file="LabelProfileLine.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Model</module>
// <author>Mansoori, Imran</author> 
// <createddate>2019-04-25</createddate>
// <lastchangedby>Mansoori, Imran</lastchangedby>
// <lastchangeddate>2019-04-25</lastchangeddate>

namespace Synapse.Backgrounds.Core.Model
{
   public class LabelProfileLine
    {
        public string PrinterStock { get; set; }
        public string Print { get; set; }
        public string Apply { get; set; }
        public string RfLine1 { get; set; }
        public string RfLine2 { get; set; }
        public string RfLine3 { get; set; }
        public string RfLine4 { get; set; }
        public string RowId { get; set; }
        public string ViewName { get; set; }
        public string ViewKeyCol { get; set; }
        public string LabelTrigger { get; set; }
        public string Seq { get; set; }
    }
}
