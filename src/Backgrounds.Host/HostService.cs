﻿// <copyright file="HostService.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Host</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Host.Interface;
using Synapse.Backgrounds.Infrastructure.Logging;
using Topshelf;

namespace Synapse.Backgrounds.Host
{
    public static class HostService
    {
        private static void Main()
        {
            HostFactory.Run(c =>
            {
                var configService = new ConfigService();
                var displayName = configService.GetAppSetting("SERVICE_DISPLAY_NAME");
                var instanceName = configService.GetAppSetting("SERVICE_INSTANCE_NAME");
                var loggerService = new LoggerService(configService);
                c.UseSerilog(loggerService.GetLogger());
                c.Service<IHostServiceControl>(svc =>
                {
                    svc.ConstructUsing(s => new HostServiceControl(loggerService, configService));
                    svc.WhenStarted(async v => { await v.OnStart(); });
                    svc.WhenStopped(v => { v.OnStop(); });
                    svc.WhenPaused(v => v.OnPause());
                    svc.WhenContinued(v => { v.OnContinue(); });
                    svc.WhenShutdown(v => { v.OnShutDown(); });
                });
                c.RunAsLocalSystem();
                c.StartAutomatically();
                c.SetDescription(displayName);
                c.SetDisplayName(displayName);
                c.SetInstanceName(instanceName);
                c.SetServiceName(instanceName);
            });
        }
    }
}