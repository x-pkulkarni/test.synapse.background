﻿// <copyright file="HostServiceControl.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Host</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Host.Interface;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;

namespace Synapse.Backgrounds.Host
{
    public class HostServiceControl : IHostServiceControl
    {
        private bool _disposed;

        private readonly ILoggerService _loggerService;
        private readonly IConfigService _configService;
        private readonly SiloBuilder _siloBuilder;
        public
        HostServiceControl(ILoggerService loggerService,  IConfigService configService)
        {
            _loggerService = loggerService;
            _configService = configService;
            _siloBuilder = new SiloBuilder();
        }

        public async Task OnStart()
        {
            await _siloBuilder.StartAsync().ContinueWith(t =>
                _loggerService.Information($"Host {Dns.GetHostName()} is up and running!!"), CancellationToken.None);
        }

        public async Task OnStop()
        {
            await _siloBuilder.StopAsync().ContinueWith(t =>
                _loggerService.Information($"Host {Dns.GetHostName()} is Stopped!!"), CancellationToken.None);
            if (_siloBuilder != null)
            {
                _siloBuilder.Dispose();
                GC.SuppressFinalize(_siloBuilder);
            }
        }

        public async void OnPause()
        {
            await OnStop();
        }

        public async void OnContinue()
        {
            await OnStart();
        }

        public async void OnShutDown()
        {
            await OnStop();
        }

        #region Dispose Methods

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~HostServiceControl()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                if (_siloBuilder != null)
                {
                    _siloBuilder.Dispose();
                    GC.SuppressFinalize(_siloBuilder);
                }
            }

            _disposed = true;
        }

        #endregion
    }
}