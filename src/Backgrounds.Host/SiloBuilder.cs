﻿// <copyright file="SiloBuilder.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Host</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using Orleans.Versions.Compatibility;
using Orleans.Versions.Selector;
using Serilog;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Host.DependencyResolver;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;

namespace Synapse.Backgrounds.Host
{
    public class SiloBuilder : IDisposable
    {
        private static ISiloHost _host;
        private static bool _siloStopping;
        private static readonly object SyncLock = new object();
        private static ILoggerService _loggerService;

        public async Task StartAsync()
        {
            _host = await StartSilo();
        }

        public async Task StopAsync()
        {
            _loggerService.Debug("Stopping Silo host ....");
            await _host.StopAsync();
        }

        private async Task<ISiloHost> StartSilo()
        {
            var configService = new ConfigService();
            // define the cluster configuration
            var siloPort = Convert.ToInt32(configService.GetAppSetting("SILO_PORT"));
            var gatewayPort = Convert.ToInt32(configService.GetAppSetting("GATEWAY_PORT"));
            var hostName = Dns.GetHostName();

            var builder = new SiloHostBuilder()
                .Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = configService.GetAppSetting("CLUSTER_ID");
                    options.ServiceId = configService.GetAppSetting("SERVICE_ID");
                })
                .Configure<SiloOptions>(opt => opt.SiloName = hostName)
                .ConfigureEndpoints(siloPort: siloPort, gatewayPort: gatewayPort, hostname: hostName)
                .UseConsulClustering(gatewayOptions =>
                {
                    gatewayOptions.Address =
                        new Uri(configService.GetAppSetting("SERVICE_DISCOVERY_ENDPOINT"));
                    gatewayOptions.AclClientToken = configService.GetAppSetting("SERVICE_DISCOVERY_TOKEN");
                    gatewayOptions.KvRootFolder = configService.GetAppSetting("KV_FOLDER_NAME");
                })
                .ConfigureServices(services => services.Configure<GrainVersioningOptions>(options =>
                {
                    options.DefaultCompatibilityStrategy = nameof(BackwardCompatible);
                    options.DefaultVersionSelectorStrategy = nameof(AllCompatibleVersions);
                }))
                .ConfigureApplicationParts(parts =>
                {
                    parts.AddFromApplicationBaseDirectory().WithReferences();
                    parts.AddFromDependencyContext().WithReferences();
                })
                .UseServiceProviderFactory(AutofacBootstrap.ConfigureServices)
                .ConfigureLogging(log=>
                {
                    log.AddSerilog(Log.ForContext("Orleans", "SiloHostBuilder"));
                });

            var host = builder.Build();
            await host.StartAsync();
            SetupApplicationShutdown();
            _loggerService = (ILoggerService)host.Services.GetService(typeof(ILoggerService));
            _loggerService.Debug("Starting Silo host ...");
            Console.WriteLine($"Host {hostName} is running!!");
            return host;
        }

        private void SetupApplicationShutdown()
        {
            // Capture the user pressing Ctrl+C 
            Console.CancelKeyPress += (s, a) =>
            {
                // Prevent the application from crashing ungracefully. 
                a.Cancel = true;
                // Don't allow the following code to repeat if the user presses Ctrl+C repeatedly. 

                lock (SyncLock)
                {
                    if (!_siloStopping)
                    {
                        _siloStopping = true;
                        Task.Run(StopAsync);
                    }
                }

                // Event handler execution exits immediately, leaving the silo shutdown running on a background thread, 
                // but the app doesn't crash because a.Cancel has been set = true 
            };
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~SiloBuilder()
        {
            Dispose(false);
        }

        protected  virtual void Dispose(bool dispose)
        {
            if (_host == null) return;

            _host.Dispose();
            _host = null;
        }
    }
}