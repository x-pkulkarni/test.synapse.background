﻿// <copyright file="ICommand.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Grains.Contract</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using Synapse.Backgrounds.Integration;

namespace Synapse.Backgrounds.Grains.Contract
{
    using System.Collections.Generic;

    public interface ICommand
    {
        Guid Id { get; }
        Enums.RequestType Type { get; }
        string CommandText { get; }
        string Facility { get; set; }
        string CustomerId { get; set; }
        string UserId { get; set; }
        long Wave { get; set; }
        long OrderId { get; set; }
        int ShipId { get; set; }
        string Item { get; set; }
        string LotNumber { get; set; }
        double Quantity { get; set; }
        string TaskPriority { get; set; }
        string PickType { get; set; }
        string Trace { get; set; }
        long SessionId { get; set; }
        string Environment { get; set; }
        long TaskId { get; set; }
        IList<ApiEndPoint> ApiEndPoints { get; set; }
        string ToString();
    }
}