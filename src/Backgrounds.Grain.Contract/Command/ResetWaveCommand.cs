﻿// <copyright file="ResetWaveCommand.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Grains.Contract</module>
// <author>Mansoori, Imran</author> 
// <createddate>2019-07-31</createddate>
// <lastchangedby>Mansoori, Imran</lastchangedby>
// <lastchangeddate>2019-07-31</lastchangeddate>

using System;
using Synapse.Backgrounds.Integration;

namespace Synapse.Backgrounds.Grains.Contract.Command
{
    using System.Collections.Generic;

    public class ResetWaveCommand : ICommand
    {
        public ResetWaveCommand()
        {
            Id = Guid.NewGuid();
            CommandText = "Reset";
            Type = Enums.RequestType.RESETWAV;
            ApiEndPoints = new List<ApiEndPoint>();
        }

        public Guid Id { get; }
        public Enums.RequestType Type { get; }
        public string CommandText { get; }
        public string Facility { get; set; }
        public string CustomerId { get; set; }
        public string UserId { get; set; }
        public long Wave { get; set; }
        public long OrderId { get; set; }
        public int ShipId { get; set; }
        public string Item { get; set; }
        public string LotNumber { get; set; }
        public double Quantity { get; set; }
        public string TaskPriority { get; set; }
        public string PickType { get; set; }
        public string Trace { get; set; }
        public long SessionId { get; set; }
        public string Environment { get; set; }
        public long TaskId { get; set; }
        public IList<ApiEndPoint> ApiEndPoints { get; set; }

        public override string ToString()
        {
            return
                $"Command Id: {Id.ToString()}, Type: { CommandText }, Environment: {Environment}, Wave: {Wave}, Facility: {Facility}, UserId: {UserId}";
        }
    }
}