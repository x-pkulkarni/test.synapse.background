﻿// <copyright file="ICommandBus.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Grains.Contract</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;
using Orleans;
using Orleans.CodeGeneration;

namespace Synapse.Backgrounds.Grains.Contract
{
    [Version(1)]
    public interface ICommandBus : IGrainWithStringKey
    {
        [OneWay]
        Task Invoke(ICommand item);
    }
}