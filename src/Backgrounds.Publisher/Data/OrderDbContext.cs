﻿// <copyright file="OrderDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-07-27</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-07-27</lastchangeddate>

using System.Data;
using Dapper;
using Synapse.Backgrounds.Publisher.Data.Interface;

namespace Synapse.Backgrounds.Publisher.Data
{
    public class OrderDbContext : IOrderDbContext
    {
        public string GetCustIdByWave(IDbConnection connection, long waveId)
        {
            var parameters = new DynamicParameters();
            parameters.Add("i_wave", waveId);
            parameters.Add("o_custid", dbType: DbType.String, direction: ParameterDirection.Output, size: 15);
            connection.Execute("WVR_CUSTIDBYWAVE_R_P", parameters, commandType: CommandType.StoredProcedure);
            return parameters.Get<string>("o_custid");
        }
    }
}