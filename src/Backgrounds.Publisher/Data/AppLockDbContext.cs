﻿// <copyright file="AppLockDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-07-26</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-07-26</lastchangeddate>

using System.Data;
using Dapper;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using Synapse.Backgrounds.Publisher.Data.Interface;

namespace Synapse.Backgrounds.Publisher.Data
{
    public class AppLockDbContext : IAppLockDbContext
    {
        public string GetAppLock(IDbConnection connection, string lockId, string facility, string userId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@in_lockid", lockId);
            parameters.Add("@in_facility", facility);
            parameters.Add("@in_userid", userId);
            parameters.Add("@out_msg", dbType: OracleDbType.NVarchar2, direction: ParameterDirection.Output, size:100);
            connection.Execute("WVR_GETAPPLOCK_R_P", parameters, commandType: CommandType.StoredProcedure);
            return (string) parameters.Get<OracleString>("out_msg");
        }

        public string ReleaseAppLock(IDbConnection connection, string lockId, string facility, string userId)
        {
            var parameters = new OracleDynamicParameters();
            parameters.Add("@in_lockid", lockId);
            parameters.Add("@in_facility", facility);
            parameters.Add("@in_userid", userId);
            parameters.Add("@out_msg", dbType: OracleDbType.NVarchar2, direction: ParameterDirection.Output, size: 100);
            connection.Execute("WVR_RELEASEAPPLOCK_R_P", parameters, commandType: CommandType.StoredProcedure);
            return (string) parameters.Get<OracleString>("out_msg");
        }
    }
}