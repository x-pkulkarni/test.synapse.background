﻿// <copyright file="IRequestProvider.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Integration;

namespace Synapse.Backgrounds.Publisher.Data.Interface
{
    public interface IRequestProvider
    {
        IList<ICommand> GetNextRequest(IDbConnection connection, ServiceContext context);
    }
}