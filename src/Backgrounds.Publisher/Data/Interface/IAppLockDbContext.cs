﻿// <copyright file="IAppLockDbContext.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-07-26</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-07-26</lastchangeddate>

using System.Data;

namespace Synapse.Backgrounds.Publisher.Data.Interface
{
    public interface IAppLockDbContext
    {
        string GetAppLock(IDbConnection connection, string lockId, string facility, string userId);

        string ReleaseAppLock(IDbConnection connection, string lockId, string facility, string userId);
    }
}