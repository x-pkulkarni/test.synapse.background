﻿#region File Header
// <copyright file="OracleDynamicParameters.cs" company="GEODIS">
// Copyright (c) 2016 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.RF</application>
// <module>RF.Core</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2016-07-14</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2016-09-02</lastchangeddate>
// 
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Oracle.DataAccess.Client;
using StackExchange.Profiling.Data;

namespace Synapse.Backgrounds.Publisher.Data
{
    public class OracleDynamicParameters : SqlMapper.IDynamicParameters
    {
        private readonly Dictionary<SqlMapper.Identity, Action<IDbCommand, object>> _paramReaderCache;
        private readonly Dictionary<string, ParamInfo> _parameters;
        private List<object> _templates;

        public OracleDynamicParameters()
        {
            _paramReaderCache = new Dictionary<SqlMapper.Identity, Action<IDbCommand, object>>();
            _parameters = new Dictionary<string, ParamInfo>();
        }

        public OracleDynamicParameters(object template)
        {
            AddDynamicParams(template);
        }

        public IEnumerable<string> ParameterNames
        {
            get { return _parameters.Select(p => p.Key); }
        }

        void SqlMapper.IDynamicParameters.AddParameters(IDbCommand command, SqlMapper.Identity identity)
        {
            AddParameters(command, identity);
        }

        public void AddDynamicParams(dynamic param)
        {
            var obj = param as object;
            if (obj != null)
            {
                var subDynamic = obj as OracleDynamicParameters;
                if (subDynamic == null)
                {
                    var dictionary = obj as IEnumerable<KeyValuePair<string, object>>;
                    if (dictionary == null)
                    {
                        _templates = _templates ?? new List<object>();
                        _templates.Add(obj);
                    }
                    else
                    {
                        foreach (var kvp in dictionary)
                        {
                            Add(kvp.Key, kvp.Value);
                        }
                    }
                }
                else
                {
                    if (subDynamic._parameters != null)
                    {
                        foreach (var kvp in subDynamic._parameters)
                        {
                            _parameters.Add(kvp.Key, kvp.Value);
                        }
                    }

                    if (subDynamic._templates != null)
                    {
                        _templates = _templates ?? new List<object>();
                        foreach (var t in subDynamic._templates)
                        {
                            _templates.Add(t);
                        }
                    }
                }
            }
        }

        public void Add(string name, object value = null, OracleDbType? dbType = null,
            ParameterDirection? direction = null, int? size = null)
        {
            _parameters[Clean(name)] = new ParamInfo
            {
                Name = name,
                Value = value,
                ParameterDirection = direction ?? ParameterDirection.Input,
                DbType = dbType,
                Size = size
            };
        }

        private static string Clean(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                switch (name[0])
                {
                    case '@':
                    case ':':
                    case '?':
                        return name.Substring(1);
                }
            }
            return name;
        }

        protected void AddParameters(IDbCommand command, SqlMapper.Identity identity)
        {
            if (_templates != null)
            {
                foreach (var template in _templates)
                {
                    var newIdent = identity.ForDynamicParameters(template.GetType());
                    Action<IDbCommand, object> appender;

                    lock (_paramReaderCache)
                    {
                        if (!_paramReaderCache.TryGetValue(newIdent, out appender))
                        {
                            appender = SqlMapper.CreateParamInfoGenerator(newIdent, false, false);
                            _paramReaderCache[newIdent] = appender;
                        }
                    }

                    appender(command, template);
                }
            }

            foreach (var param in _parameters.Values)
            {
                var name = Clean(param.Name);
                var oracmd = command is OracleCommand ? command : ((ProfiledDbCommand)command).InternalCommand;
                var add = !oracmd.Parameters.Contains(name);
                OracleParameter p;
                if (add)
                {
                    p = ((OracleCommand)oracmd).CreateParameter();
                    p.ParameterName = name;
                }
                else
                {
                    p = ((OracleCommand)oracmd).Parameters[name];
                }
                var val = param.Value;
                p.Value = val ?? DBNull.Value;
                p.Direction = param.ParameterDirection;
                var s = val as string;
                if (s?.Length <= 4000)
                {
                    p.Size = 4000;
                }
                if (param.Size != null)
                {
                    p.Size = param.Size.Value;
                }
                if (param.DbType != null)
                {
                    p.OracleDbType = param.DbType.Value;
                }
                if (add)
                {
                    command.Parameters.Add(p);
                }
                param.AttachedParam = p;
            }
        }

        public T Get<T>(string name)
        {
            var val = _parameters[Clean(name)].AttachedParam.Value;
            if (val == DBNull.Value)
            {
                if (default(T) != null)
                {
                    throw new ApplicationException("Attempting to cast a DBNull to a non nullable type!");
                }
                return default(T);
            }
            return (T)val;
        }

        private class ParamInfo
        {
            public string Name { get; set; }
            public object Value { get; set; }
            public ParameterDirection ParameterDirection { get; set; }
            public OracleDbType? DbType { get; set; }
            public int? Size { get; set; }
            public IDbDataParameter AttachedParam { get; set; }
        }
    }
}