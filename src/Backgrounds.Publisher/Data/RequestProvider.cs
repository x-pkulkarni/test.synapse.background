﻿// <copyright file="RequestProvider.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2018-01-09</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Oracle.DataAccess.Client;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Integration;
using Synapse.Backgrounds.Publisher.Data.Interface;
using Synapse.Backgrounds.Publisher.Data.Mapper;

namespace Synapse.Backgrounds.Publisher.Data
{
    public class RequestProvider : IRequestProvider
    {
        private readonly CommandMapper _commandMapper;
        public RequestProvider(CommandMapper commandMapper)
        {
            _commandMapper = commandMapper;
        }

        public IList<ICommand> GetNextRequest(IDbConnection connection, ServiceContext context)
        {
            var request = new List<ICommand>();
            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }

            using (var queue = new OracleAQQueue(context.OracleQueue, connection as OracleConnection))
            {
                queue.MessageType = OracleAQMessageType.Udt;
                queue.UdtTypeName = "ALPS.QMSG";

                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        if (context.CanDequeueArray)
                        {
                            var aqMessages = queue.DequeueArray(context.DequeueMessageCount,
                                new OracleAQDequeueOptions
                                {
                                    Wait = 5,
                                    NavigationMode = OracleAQNavigationMode.NextMessage
                                });
                            request.AddRange(
                                aqMessages.Select(aqMessage => aqMessage.Payload as CustomQueueMessage)
                                    .Select(
                                        waveQueueMessage =>
                                            _commandMapper.Map(waveQueueMessage?.Message, context)));
                        }
                        else
                        {
                            var aqMessage =
                                queue.Dequeue(new OracleAQDequeueOptions
                                {
                                    DequeueMode = OracleAQDequeueMode.Remove,
                                    Wait = 0
                                }).Payload as CustomQueueMessage;
                            request.Add(_commandMapper
                                .Map(aqMessage?.Message, context));
                        }

                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        if (connection.State != ConnectionState.Open)
                            transaction.Rollback();
                        if (!ex.Message.StartsWith("ORA-25228:", StringComparison.OrdinalIgnoreCase))
                            throw;
                    }
                }
            }

            return request;
        }
    }
}