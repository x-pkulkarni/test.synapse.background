﻿// <copyright file="CustomQueueMessage.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace Synapse.Backgrounds.Publisher.Data
{
    [OracleCustomTypeMapping("ALPS.QMSG")]
    public class CustomQueueMessage : IOracleCustomType, INullable, IOracleCustomTypeFactory
    {
        [OracleObjectMapping("TRANS")] public string Transaction { get; set; }

        [OracleObjectMapping("MESSAGE")] public string Message { get; set; }


        public static CustomQueueMessage Null => new CustomQueueMessage {IsNull = true};

        public bool IsNull { get; private set; }

        public void FromCustomObject(OracleConnection con, IntPtr pUdt)
        {
            OracleUdt.SetValue(con, pUdt, "TRANS", Transaction);
            OracleUdt.SetValue(con, pUdt, "MESSAGE", Message);
        }

        public void ToCustomObject(OracleConnection con, IntPtr pUdt)
        {
            Transaction = (string) OracleUdt.GetValue(con, pUdt, "TRANS");
            Message = (string) OracleUdt.GetValue(con, pUdt, "MESSAGE");
        }

        public IOracleCustomType CreateObject()
        {
            return new CustomQueueMessage();
        }
    }
}