﻿// <copyright file="CommandMapper.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Linq;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Grains.Contract.Command;
using Synapse.Backgrounds.Integration;
using Synapse.Backgrounds.Publisher.DependencyModule.cs.Interface;

namespace Synapse.Backgrounds.Publisher.Data.Mapper
{
    public class CommandMapper
    {
        private readonly IKeyedTypeFactory _keyedTypeFactory;
        private readonly Dictionary<int, string> _propertyStack;

        public CommandMapper(IKeyedTypeFactory keyedTypeFactory)
        {
            _keyedTypeFactory = keyedTypeFactory;
            _propertyStack = new Dictionary<int, string>
            {
                {1, "facility"},
                {2, "userid"},
                {3, "wave"},
                {4, "orderid"},
                {5, "shipid"},
                {6, "item"},
                {7, "lotnumber"},
                {8, "quantity"},
                {9, "taskoriority"},
                {10, "PickType"},
                {11, "trace"},
                {12, "sessionid"}
            };
        }

        public ICommand Map(string content, ServiceContext context)
        {
            var dataList = content.Trim().Split(new[] {"\t"}, StringSplitOptions.None).ToList();

            if (dataList.Count < 1 || !Enum.TryParse(dataList[0], true, out Enums.RequestType requestType))
                return default(ICommand);

            var command = _keyedTypeFactory.Invoke<ICommand, Enums.RequestType>(requestType);
            var props = command.GetType().GetProperties();

            foreach (var kvp in props)
            {
                if (!_propertyStack.ContainsValue(kvp.Name.ToLower())) continue;
                var propValue = dataList[_propertyStack.First(v => v.Value.Equals(kvp.Name.ToLower())).Key];
                if (kvp.PropertyType == typeof(long)) kvp.SetValue(command, Convert.ToInt64(propValue));
                if (kvp.PropertyType == typeof(int)) kvp.SetValue(command, Convert.ToInt32(propValue));
                if (kvp.PropertyType == typeof(double)) kvp.SetValue(command, Convert.ToDouble(propValue));
                if (kvp.PropertyType == typeof(string)) kvp.SetValue(command, propValue);
            }

            var cmd = (ICommand) command;

            //In Genpicks.pc, taskid is assigned to orderid for regenerate batch tasks. So correcting the incorrect field mapping here
            if (requestType == Enums.RequestType.GENLIPBP)
            {
                cmd.TaskId = cmd.OrderId;
                cmd.OrderId = 0;
            }
            cmd.Environment = context.ConnectionKey;
            cmd.ApiEndPoints = context.ApiEndPoints;
            return cmd;
        }
    }
}