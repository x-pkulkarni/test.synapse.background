﻿// <copyright file="ClusterClientBuilder.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using System.Configuration;
using System.Threading.Tasks;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;
using Polly;
using Serilog;
using Synapse.Backgrounds.Configuration;

namespace Synapse.Backgrounds.Publisher
{
    public class ClusterClientBuilder : IDisposable
    {
        private static IClusterClient _consumer;

        public static async Task StartAsync(int initializeAttemptsBeforeFailing = 5)
        {
            var policy = Policy.Handle<Exception>().WaitAndRetryAsync(initializeAttemptsBeforeFailing,
                retryAttempt => TimeSpan.FromSeconds(4),
                (exception, timeSpan, retryCount, context) =>
                {
                    Console.WriteLine(
                        $"Attempt {retryCount} of {initializeAttemptsBeforeFailing} failed to initialize the Orleans client.");
                    if (retryCount.Equals(initializeAttemptsBeforeFailing))
                        throw exception;
                });
            await policy.ExecuteAsync(async () => { _consumer = await RunMainAsync(); });
        }

        public static async Task StopAsync()
        {
            await _consumer.Close();
        }

        public static void ExecuteTask(Action<IClusterClient> task)
        {
            task(_consumer);
        }

        private static async Task<IClusterClient> RunMainAsync()
        {
            var configService = new ConfigService();

            var client = new ClientBuilder()
                .Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = configService.GetAppSetting("CLUSTER_ID");
                    options.ServiceId = configService.GetAppSetting("SERVICE_ID");
                })
                .UseConsulClustering(gatewayOptions =>
                {
                    gatewayOptions.Address =
                        new Uri(ConfigurationManager.AppSettings["SERVICE_DISCOVERY_ENDPOINT"]);
                    gatewayOptions.AclClientToken = configService.GetAppSetting("SERVICE_DISCOVERY_TOKEN");
                    gatewayOptions.KvRootFolder = configService.GetAppSetting("KV_FOLDER_NAME");
                })
                .ConfigureApplicationParts(parts =>
                {
                    parts.AddFromApplicationBaseDirectory().WithReferences();
                    parts.AddFromDependencyContext().WithReferences();
                })
                .ConfigureLogging(log => { log.AddSerilog(Log.ForContext("Orleans", "SiloClientBuilder")); })
                .Build();

            await client.Connect();
            Console.WriteLine("Client successfully connect to silo host");
            return client;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        ~ClusterClientBuilder()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool dispose)
        {
            _consumer.Dispose();
            _consumer = null;
        }
    }
}