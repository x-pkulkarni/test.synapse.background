﻿// <copyright file="PublishService.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using Autofac;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;
using Synapse.Backgrounds.Publisher.DependencyModule.cs;
using Synapse.Backgrounds.Publisher.Interface;
using Topshelf;
using Topshelf.Autofac;

namespace Synapse.Backgrounds.Publisher
{
    using Configuration;

    public static class PublishService
    {
        private static void Main()
        {
            var container = AutofacBootstrap.BuildContainer();

            var displayName = container.Resolve<IConfigService>().GetAppSetting("SERVICE_DISPLAY_NAME");
            var instanceName = container.Resolve<IConfigService>().GetAppSetting("SERVICE_INSTANCE_NAME");

            HostFactory.Run(c =>
            {
                c.UseSerilog(container.Resolve<ILoggerService>().GetLogger());
                c.UseAutofacContainer(container);
                c.Service<IPublishServiceControl>(svc =>
                {
                    svc.ConstructUsingAutofacContainer();
                    svc.WhenStarted(v => v.OnStart());
                    svc.WhenStopped(v =>
                    {
                        v.OnStop();
                        container.Dispose();
                    });
                    svc.WhenPaused(v => v.OnPause());
                    svc.WhenContinued(v => v.OnContinue());
                    svc.WhenShutdown(v =>
                    {
                        v.OnShutDown();
                        container.Dispose();
                    });
                });
                c.RunAsLocalSystem();
                c.StartAutomatically();
                c.SetDescription(displayName);
                c.SetDisplayName(displayName);
                c.SetInstanceName(instanceName);
                c.SetServiceName(instanceName);
            });
        }
    }
}