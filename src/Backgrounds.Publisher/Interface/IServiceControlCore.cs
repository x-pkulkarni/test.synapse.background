﻿// <copyright file="IServiceControlCore.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-08-02</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-03</lastchangeddate>

using System.Threading;
using Synapse.Backgrounds.Integration;

namespace Synapse.Backgrounds.Publisher.Interface
{
    public interface IServiceControlCore
    {
        bool GenerateMessage(ServiceContext context, CancellationToken cancellationToken);
        //void ProcessMessage(ServiceContext context, CancellationToken cancellationToken);
        void ProcessMessage(ServiceContext selectedContext, ServiceProcessor selectedProcessor, CustomerQueue selectedCustomer, CancellationToken cancellationToken);

        bool CleanUp();
    }
}