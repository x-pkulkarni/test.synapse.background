﻿// <copyright file="GrainHandle.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-28</lastchangeddate>

using System.Threading.Tasks;
using Orleans;
using Synapse.Backgrounds.Grains.Contract;
using Topshelf.Logging;

namespace Synapse.Backgrounds.Publisher
{
    public static class GrainHandle
    {
        private static IClusterClient _client;

        private static readonly LogWriter Log = HostLogger.Get("GrainHandle");

        public static void Initialize(IClusterClient consumer)
        {
            _client = consumer;
            Log.Info("Command Bus Initializing!");
        }

        public static async Task Notify(ICommand command)
        {
            await _client.GetGrain<ICommandBus>(command.CustomerId).Invoke(command);
        }
    }
}