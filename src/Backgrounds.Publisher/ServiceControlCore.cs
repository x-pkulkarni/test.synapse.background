﻿// <copyright file="ServiceControlCore.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-08-02</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-03</lastchangeddate>

using System;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;
using Synapse.Backgrounds.Integration;
using Synapse.Backgrounds.Publisher.Interface;
using Synapse.Backgrounds.Publisher.Model;
using Synapse.Backgrounds.Publisher.Processor.Interface;

namespace Synapse.Backgrounds.Publisher
{
    public class ServiceControlCore : IServiceControlCore
    {
        private readonly IConfigService _configService;
        private readonly IRequestProcessor _processor;
        private readonly ILoggerService _loggerService;
        private readonly IAppLockService _appLockService;
        private readonly IBrokerService _brokerService;

        public ServiceControlCore(IConfigService configService, IRequestProcessor processor,
            ILoggerService loggerService, IAppLockService appLockService,
            IBrokerService brokerService)
        {
            _configService = configService;
            _processor = processor;
            _loggerService = loggerService;
            _appLockService = appLockService;
            _brokerService = brokerService;
        }

        public bool GenerateMessage(ServiceContext context, CancellationToken cancellationToken)
        {
            Task.Factory.StartNew(
                () => { _processor.RunDequeueDaemon(context, cancellationToken); },
                cancellationToken).ContinueWith(t =>
            {
                if (t.IsCompleted && !t.IsFaulted)
                {
                    Log.Logger.Debug($"RunDequeueDaemon task completed at: {DateTime.Now}");
                }
                else if (t.IsCanceled)
                {
                    Log.Logger.Debug($"RunDequeueDaemon task cancelled at: {DateTime.Now}");
                }
                else
                {
                    if (t.Exception != null)
                        Log.Logger.Error(t.Exception,
                            $"RunDequeueDaemon task errored at: {DateTime.Now} and exception details: {t.Exception.StackTrace}");
                }
            }, cancellationToken);

            return true;
        }

        public void ProcessMessage(ServiceContext selectedContext, ServiceProcessor selectedProcessor, CustomerQueue selectedCustomer,
            CancellationToken cancellationToken)
        {
            _brokerService.GetMessage(selectedContext, selectedProcessor, selectedCustomer, cancellationToken,
                NotifySilo);
        }

        public async Task NotifySilo(ICommand message, IDbConnection connection)
        {
            var appLockRequest = new AppLockRequest
            {
                CustomerId = message.CustomerId,
                Facility = message.Facility,
                OrderId = message.OrderId,
                UserId = message.UserId,
                LockId = $"WAVEPLAN{message.Facility}~{message.CustomerId}"
            };

            var appLockCheckFlag = string.Equals(_configService.GetAppSetting("EnableAppLock"), "Y",
                StringComparison.CurrentCultureIgnoreCase);

            if (!appLockCheckFlag || _appLockService.AcquireAppLock(connection, appLockRequest))
            {
                _loggerService.Information($"Starting to process request: {message}");
                await GrainHandle.Notify(message);
            }
        }

        public bool CleanUp()
        {
            return _brokerService.CleanUp();
        }
    }
}