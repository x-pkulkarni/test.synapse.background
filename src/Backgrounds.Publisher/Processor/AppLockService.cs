﻿// <copyright file="AppLockService.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-08-01</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-03</lastchangeddate>

using System;
using System.Data;
using Polly;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;
using Synapse.Backgrounds.Publisher.Data.Interface;
using Synapse.Backgrounds.Publisher.Model;
using Synapse.Backgrounds.Publisher.Processor.Interface;

namespace Synapse.Backgrounds.Publisher.Processor
{
    public class AppLockService : IAppLockService
    {
        private readonly IAppLockDbContext _appLockDbContext;
        private readonly ILoggerService _loggerService;

        public AppLockService(IAppLockDbContext appLockDbContext, ILoggerService loggerService)
        {
            _appLockDbContext = appLockDbContext;
            _loggerService = loggerService;
        }

        public bool AcquireAppLock(IDbConnection connection, AppLockRequest appLockRequest)
        {
            var response = false;
            var policy = Policy.Handle<Exception>().WaitAndRetryForever(
                attempt => TimeSpan.FromMilliseconds(200),
                (exception, calculatedWaitDuration) =>
                    _loggerService.Error(exception, "Unable to acquire lock:" + appLockRequest.LockId));

            policy.Execute(() => { response = ExecuteAppLock(connection, appLockRequest); });
            return response;
        }

        public bool ReleaseAppLock(IDbConnection connection, AppLockRequest appLockRequest)
        {
            try
            {
                var response =
                    _appLockDbContext.ReleaseAppLock(connection, appLockRequest.LockId, appLockRequest.Facility,
                        appLockRequest.UserId);
                return response.Equals("OKAY", StringComparison.CurrentCultureIgnoreCase);
            }
            catch (Exception e)
            {
                _loggerService.Error(e, "Unable to release lock:" + appLockRequest.LockId);
            }

            return false;
        }

        private bool ExecuteAppLock(IDbConnection connection, AppLockRequest appLockRequest)
        {
            var response =
                _appLockDbContext.GetAppLock(connection, appLockRequest.LockId, appLockRequest.Facility,
                    appLockRequest.UserId);
            if (response.Equals("OKAY", StringComparison.CurrentCultureIgnoreCase)) return true;

            throw new Exception(response);
        }
    }
}