﻿// <copyright file="IBrokerService.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-08-02</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-08-03</lastchangeddate>

using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Integration;

namespace Synapse.Backgrounds.Publisher.Processor.Interface
{
    public interface IBrokerService
    {
        //void GetMessage(ServiceContext context, CancellationToken cancellationToken, Func<ICommand, IDbConnection, Task> callback, string topic);

        void GetMessage(ServiceContext selectedContext, ServiceProcessor selectedProcessor, CustomerQueue selectedCustomer,
            CancellationToken cancellationToken, Func<ICommand, IDbConnection, Task> callback);

        void PushMessage(ServiceContext context, ICommand command);

        bool CleanUp();
    }
}