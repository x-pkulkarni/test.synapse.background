﻿// <copyright file="BrokerService.cs" company="GEODIS">
// Copyright (c) 2019 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2019-04-30</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2019-06-07</lastchangeddate>

namespace Synapse.Backgrounds.Publisher.Processor
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Configuration;
    using Data.Interface;
    using EasyNetQ;
    using Grains.Contract;
    using Infrastructure.Logging.Interface;
    using Infrastructure.MessageQueue.Interface;
    using Integration;
    using Interface;

    public class BrokerService : IBrokerService
    {
        private readonly IConfigService _configService;
        private readonly IMessageQueueFactory _messageQueueFactory;
        private readonly IAppLockService _appLockService;
        private readonly ILoggerService _loggerService;
        private readonly IList<ISubscriptionResult> _activeSubscriptions;
        private readonly IOracleConnectionProvider _oracleConnection;

        public BrokerService(IConfigService configService, IMessageQueueFactory messageQueueFactory,
            IAppLockService appLockService, ILoggerService loggerService, IOracleConnectionProvider oracleConnection)
        {
            _configService = configService;
            _messageQueueFactory = messageQueueFactory;
            _appLockService = appLockService;
            _loggerService = loggerService;
            _oracleConnection = oracleConnection;
            _activeSubscriptions = new List<ISubscriptionResult>();
        }

        public void GetMessage(ServiceContext selectedContext, ServiceProcessor selectedProcessor, CustomerQueue selectedCustomer,
            CancellationToken cancellationToken, Func<ICommand, IDbConnection, Task> callback)
        {
            for (var sCnt = 0; sCnt < selectedCustomer.NoOfSubscriptions; sCnt++)
            {
                foreach (var subscriber in selectedProcessor.QueueContext.Subscribers)
                {
                    var currentSubscription = _messageQueueFactory.CreateBus(subscriber.ConnectionString)
                        .SubscribeAsync<ICommand>(selectedCustomer.SubscriptionId, message => Task
                                .Factory.StartNew(
                                    () =>
                                    {
                                        using (var connection = _oracleConnection.GetDbConnection(selectedContext.ConnectionKey))
                                        {
                                            if (connection != null && connection.State != ConnectionState.Open)
                                            {
                                                connection.Open();
                                            }

                                            callback(message, connection);
                                        }
                                    }, TaskCreationOptions.PreferFairness).ContinueWith(
                                    t =>
                                    {
                                        if (t.IsFaulted || !t.IsCompleted)
                                        {
                                            _loggerService.Error(t.Exception,
                                                "Failed to send message. Hence lock has been released.");
                                        }
                                    }, cancellationToken),

                            cfg =>
                            {
                                cfg.WithTopic(selectedCustomer.QueueTopic);
                                cfg.WithPrefetchCount(1);
                            });

                    _activeSubscriptions.Add(currentSubscription);
                }
            }
        }

        public void PushMessage(ServiceContext context, ICommand command)
        {
            foreach (var contextServiceProcessor in context.ServiceProcessors)
            {
                var customerQueueInfo =
                    GetCustomerQueueInfo(command.CustomerId, contextServiceProcessor.CustomerQueues);
                _loggerService.Debug($"Selected customer info for command id: {command.Id} is {customerQueueInfo}");
                _messageQueueFactory.CreateBus(contextServiceProcessor.QueueContext.PublisherConnectionString).PublishAsync(
                    command, customerQueueInfo.QueueTopic);
            }
        }

        public bool CleanUp()
        {
            foreach (var subscription in _activeSubscriptions)
            {
                subscription.Dispose();
            }

            return true;
        }

        #region Private Methods

        private static CustomerQueue GetCustomerQueueInfo(string customerId, IList<CustomerQueue> customerQueues)
        {
            var selectedCustomerInfo = customerQueues.ToList().Find(c =>
                                           string.Equals(c.CustId, customerId, StringComparison.InvariantCultureIgnoreCase)) ??
                                       customerQueues.ToList().Find(c =>
                                           string.Equals(c.CustId, string.Empty, StringComparison.InvariantCultureIgnoreCase));

            return selectedCustomerInfo;
        }

        #endregion
    }
}