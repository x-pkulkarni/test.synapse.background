﻿// <copyright file="RequestProcessor.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using StackExchange.Profiling;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;
using Synapse.Backgrounds.Infrastructure.Profiler.Interface;
using Synapse.Backgrounds.Integration;
using Synapse.Backgrounds.Publisher.Data.Interface;
using Synapse.Backgrounds.Publisher.Processor.Interface;

namespace Synapse.Backgrounds.Publisher.Processor
{
    using System.Diagnostics;

    public class RequestProcessor : IRequestProcessor
    {
        private bool _disposed;
        private readonly IOracleConnectionProvider _oracleConnection;
        private readonly IProfileService _profileService;
        private readonly IRequestProvider _requestProvider;
        private readonly ILoggerService _loggerService;
        private readonly IOrderDbContext _orderDbContext;
        private readonly IBrokerService _brokerService;

        public RequestProcessor(IOracleConnectionProvider oracleConnection, IRequestProvider requestProvider,
            IProfileService profileService, ILoggerService loggerService, IOrderDbContext orderDbContext, IBrokerService brokerService)
        {
            _oracleConnection = oracleConnection;
            _profileService = profileService;
            _requestProvider = requestProvider;
            _loggerService = loggerService;
            _orderDbContext = orderDbContext;
            _brokerService = brokerService;
        }

        public void RunDequeueDaemon(ServiceContext context, CancellationToken cancellationToken)
        {
            using (var connection = _oracleConnection.GetDbConnection(context.ConnectionKey))
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    try
                    {
                        ReceiveMessage(connection, context);
                    }
                    catch (Exception ex)
                    {
                        _loggerService.Error(ex, new ServiceLogContext {Environment = context.Environment},
                            context,
                            $"Exception is thrown at RequestProcessor.ReceiveMessage");
                        Thread.Sleep(120000);
                    }
                    finally
                    {
                        connection?.Close();
                        Thread.Sleep(context.SleepInSeconds * 1000);
                    }
                }
            }
        }

        private void ReceiveMessage(IDbConnection connection, ServiceContext context)
        {
            var commands = _requestProvider.GetNextRequest(connection, context);

            if (commands == null || !commands.Any())
            {
                return;
            }

            var currentProfiler = _profileService.Start("RequestProcessor:RunDequeueDaemon");

            using (_profileService.Step(currentProfiler, "Rabbit MQ Queue process"))
            {
                foreach (var item in commands)
                {
                    if (item == null)
                    {
                        throw new Exception("Undefined message received from Oracle Queue!");
                    }

                    _loggerService.Information($"Received request: {item}");
                    item.CustomerId = _orderDbContext.GetCustIdByWave(connection, item.Wave);
                    _brokerService.PushMessage(context, item);
                }
            }

            _profileService.Stop(currentProfiler);
        }


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~RequestProcessor()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;
            if (disposing) _oracleConnection.Dispose();

            _disposed = true;
        }
    }
}