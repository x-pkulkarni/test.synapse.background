﻿// <copyright file="PublishServiceControl.cs" company="GEODIS">
// Copyright (c) 2019 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
//  any means, electronic, mechanical or otherwise, is prohibited without the 
//  prior written consent of the copyright owner.
// </copyright>
// 
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Rajasekharan, Rajesh</author> 
// <createddate>2019-04-30</createddate>
// <lastchangedby>Rajasekharan, Rajesh</lastchangedby>
// <lastchangeddate>2019-06-07</lastchangeddate>

namespace Synapse.Backgrounds.Publisher
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;
    using Configuration;
    using Infrastructure.Logging.Interface;
    using Integration;
    using Interface;

    public class PublishServiceControl : IPublishServiceControl
    {
        private readonly IList<ServiceContext> _serviceContexts;
        private readonly ILoggerService _loggerService;
        private readonly IServiceControlCore _serviceControlCore;
        private readonly IConfigService _configService;
        private bool _disposed;
        private CancellationTokenSource _cancellationTokenSource;
        private CancellationToken _cancellationToken;
        private Task _backgroundMainTask;

        public PublishServiceControl(IServiceControlCore serviceControlCore, IConfigService configService,
            ILoggerService loggerService)
        {
            _serviceControlCore = serviceControlCore;
            _configService = configService;
            _loggerService = loggerService;
            _serviceContexts = _configService.GetServiceContexts();
        }

        public void OnStart()
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = _cancellationTokenSource.Token;

            _backgroundMainTask = Task.Factory.StartNew(StartServices, _cancellationToken,
                TaskCreationOptions.LongRunning, TaskScheduler.Default);
            _backgroundMainTask.Wait(_cancellationToken);

            _loggerService.Information($"Started service: {_configService.GetAppSetting("SERVICE_INSTANCE_NAME")}");
        }

        private void StartServices()
        {
            int.TryParse(_configService.GetAppSetting("SERVICE_RETRY_BEFORE_FAILING"), out var initializeAttemptsBeforeFailing);
            ClusterClientBuilder.StartAsync(initializeAttemptsBeforeFailing).Wait(_cancellationToken);
            ClusterClientBuilder.ExecuteTask(GrainHandle.Initialize);

            foreach (var serviceContext in _serviceContexts)
            {
                try
                {
                    var contextTokenSource =
                        CancellationTokenSource.CreateLinkedTokenSource(_cancellationToken,
                            new CancellationTokenSource().Token);

                    foreach (var serviceProcessor in serviceContext.ServiceProcessors)
                    {
                        foreach (var customerQueue in serviceProcessor.CustomerQueues)
                        {
                            _serviceControlCore.ProcessMessage(serviceContext, serviceProcessor, customerQueue,
                                contextTokenSource.Token);
                        }
                    }

                    for (var tCnt = 0; tCnt < serviceContext.NoOfThreads; tCnt++)
                    {
                        Task.Factory.StartNew(() =>
                        {
                            _serviceControlCore.GenerateMessage(serviceContext, contextTokenSource.Token);
                        }, TaskCreationOptions.PreferFairness).ContinueWith(task =>
                        {
                            if (task.IsCompleted && !task.IsFaulted)
                            {
                                _loggerService.Information(
                                    $"{serviceContext.Environment} service started successfully.");
                            }
                            else
                            {
                                _loggerService.Error(task.Exception,
                                    $"{serviceContext.Environment} service start failed.");
                            }
                        }, contextTokenSource.Token);
                    }
                }
                catch (Exception e)
                {
                    _loggerService.Error(e, "Error occured in service.");
                }
            }
        }

        public void OnStop()
        {
            try
            {
                _cancellationTokenSource.Cancel();
                _backgroundMainTask.Wait(_cancellationToken);
                ClusterClientBuilder.StopAsync().Wait(_cancellationToken);
                _serviceControlCore.CleanUp();
            }
            catch (AggregateException e)
            {
                foreach (var innerException in e.Flatten().InnerExceptions)
                {
                    _loggerService.Error(e, e.Message + " - " + innerException.Message);
                }
                return;
            }
            finally
            {
                _cancellationTokenSource.Dispose();
            }

            _loggerService.Information($"Stopped {_configService.GetAppSetting("SERVICE_INSTANCE_NAME")} service.");
        }

        public void OnPause()
        {
            OnStop();
        }

        public void OnContinue()
        {
            OnStart();
        }

        public void OnShutDown()
        {
            OnStop();
        }

        #region Dispose Methods

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~PublishServiceControl()
        {
            Dispose(false);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing) _cancellationTokenSource.Dispose();

            _disposed = true;
        }

        #endregion
    }
}