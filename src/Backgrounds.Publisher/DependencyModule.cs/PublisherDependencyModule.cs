﻿// <copyright file="PublisherDependencyModule.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Publisher</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System.Reflection;
using Autofac;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Configuration.DependencyResolver;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Grains.Contract.Command;
using Synapse.Backgrounds.Infrastructure.DependencyResolver;
using Synapse.Backgrounds.Integration;
using Synapse.Backgrounds.Publisher.Data.Mapper;
using Module = Autofac.Module;

namespace Synapse.Backgrounds.Publisher.DependencyModule.cs
{
    public class PublisherDependencyModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).AsImplementedInterfaces();
            builder.RegisterType<CommandMapper>();

            builder.RegisterType<ReleaseWaveCommand>().Keyed<ICommand>(Enums.RequestType.RELWAV);
            builder.RegisterType<UnreleaseWaveCommand>().Keyed<ICommand>(Enums.RequestType.UNRELWAV);
            builder.RegisterType<RegenerateBatchCommand>().Keyed<ICommand>(Enums.RequestType.GENLIPBP);

            builder.RegisterModule<ConfigurationDependencyModule>();
            builder.RegisterModule<InfrastructureDependencyModule>();

        }
    }
}