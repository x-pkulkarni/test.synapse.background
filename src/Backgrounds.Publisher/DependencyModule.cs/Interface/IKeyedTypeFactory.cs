﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Backgrounds.Publisher.DependencyModule.cs.Interface
{
    public interface IKeyedTypeFactory
    {
        TImplemation Invoke<TImplemation, TEnum>(TEnum key);
    }
}
