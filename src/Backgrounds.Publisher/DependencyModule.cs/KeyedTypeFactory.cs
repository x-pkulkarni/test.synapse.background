﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Synapse.Backgrounds.Publisher.DependencyModule.cs.Interface;

namespace Synapse.Backgrounds.Publisher.DependencyModule.cs
{
    public class KeyedTypeFactory :IKeyedTypeFactory
    {
        #region Private Member(S)
        private readonly IComponentContext _autofacContext;
        #endregion

        #region Public Constructor(S)
        public KeyedTypeFactory(IComponentContext autofacContext)
        {
            _autofacContext = autofacContext;
        }
        #endregion

        #region Public Method(S)
        public TImplemation Invoke<TImplemation, TEnum>(TEnum key)
        {
            return _autofacContext.ResolveKeyed<TImplemation>(_autofacContext.IsRegisteredWithKey<TImplemation>(key)
                ? key
                : Enum.IsDefined(typeof(TEnum),"Deafult")?
                    (TEnum) Enum.Parse(typeof(TEnum), "Deafult")
                    : throw new NotImplementedException());
        }
        #endregion

    }
}
