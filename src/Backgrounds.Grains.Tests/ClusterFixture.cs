﻿// <copyright file="ClusterFixture.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Grains.Tests</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-03</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-22</lastchangeddate>

using System;
using Backgrounds.Grains.Tests.DependencyModule;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Orleans;
using Orleans.Hosting;
using Orleans.Streaming.RabbitMQ.Contracts;
using Orleans.Streaming.RabbitMQ.Hosting;
using Orleans.Streaming.RabbitMQ.Providers.Streams.RabbitMQ;
using Orleans.TestingHost;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Grains.Contract;

namespace Backgrounds.Grains.Tests
{
    public class ClusterFixture : IDisposable
    {
        public readonly TestCluster Cluster;
        public readonly IClusterClient Client;


        public ClusterFixture()
        {
            var configService = new ConfigService();
            Console.WriteLine("Initializing Orleans TestCluster");

            var builder = new TestClusterBuilder(1);
            builder.Options.ServiceId = configService.GetAppSetting("SERVICE_ID");
            builder.Options.ClusterId = configService.GetAppSetting("CLUSTER_ID");
            builder.AddSiloBuilderConfigurator<SiloConfigurator>();
            builder.AddClientBuilderConfigurator<ClientConfigurator>();
            Cluster = builder.Build();

            Cluster.Deploy();
            Cluster.InitializeClient();
            Client = Cluster.Client;
            Console.WriteLine("Initialized Orleans TestCluster");
        }

        public class SiloConfigurator : ISiloBuilderConfigurator
        {
            public readonly IConfigService ConfigService;

            public SiloConfigurator()
            {
                ConfigService = new ConfigService();
            }

            public void Configure(ISiloHostBuilder builder)
            {
                builder.ConfigureApplicationParts(parts =>
                    {
                        parts.AddFromApplicationBaseDirectory().WithReferences();
                        parts.AddFromDependencyContext().WithReferences();
                    })
                    .UseServiceProviderFactory(AutofacBootstrap.ConfigureServices);
            }
        }

        public class ClientConfigurator : IClientBuilderConfigurator
        {
            public readonly IConfigService ConfigService;

            public ClientConfigurator()
            {
                ConfigService = new ConfigService();
            }

            public void Configure(IConfiguration configuration, IClientBuilder clientBuilder)
            {
                clientBuilder.ConfigureApplicationParts(parts =>
                    {
                        parts.AddFromApplicationBaseDirectory().WithReferences();
                        parts.AddFromDependencyContext().WithReferences();
                    });
            }
        }

        public void Dispose()
        {
            Client.Dispose();
            Cluster.StopAllSilos();
        }
    }
}