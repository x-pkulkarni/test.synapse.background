﻿// <copyright file="CommandBusTest.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Grains.Tests</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-22</lastchangeddate>

using System;
using System.Threading.Tasks;
using Backgrounds.Grains.Tests.TestDataFactory;
using Orleans;
using Orleans.TestingHost;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Grains.Contract.Command;
using Xunit;

namespace Backgrounds.Grains.Tests
{
    public class CommandBusTest : IClassFixture<ClusterFixture>
    {
        #region Private Member(S)
        private readonly ClusterFixture _fixture;
        private TestCluster Cluster => _fixture.Cluster;
        private IClusterClient Client => _fixture.Client;
        private readonly IConfigService _configService;

        #endregion

        #region Constructor(S)
        public CommandBusTest(ClusterFixture fixture)
        {
            _fixture = fixture;
            _configService = new ConfigService();
        }
        #endregion

        [Theory]//(Skip = "Require Live data")]
        [MemberData(nameof(CommandDataSource.SingleReleaseCommand), MemberType =
             typeof(CommandDataSource))]
        public async Task ExecuteReleaseWave(ICommand command)
        {
            #region Arrange

            var client = _fixture.Client;
            #endregion

            #region Act
                await client.GetGrain<ICommandBus>(command.CustomerId).Invoke(command);
            #endregion

        }

        [Theory]//(Skip = "Require Live data")]
        [MemberData(nameof(CommandDataSource.SingleUnreleaseCommand), MemberType =
            typeof(CommandDataSource))]
        public async Task ExecuteUnreleaseWave(ICommand command)
        {
            #region Arrange

            var client = _fixture.Client;
            #endregion

            #region Act
            await client.GetGrain<ICommandBus>(command.CustomerId).Invoke(command);
            #endregion

        }

        [Theory]//(Skip = "Require Live data")]
        [MemberData(nameof(CommandDataSource.SingleRegenerateBatchCommand), MemberType =
            typeof(CommandDataSource))]
        public async Task ExecuteRegenerateBatchWave(ICommand command)
        {
            #region Arrange

            var client = _fixture.Client;
            #endregion

            #region Act
            await client.GetGrain<ICommandBus>(command.CustomerId ).Invoke(command);
            #endregion

        }
    }
}