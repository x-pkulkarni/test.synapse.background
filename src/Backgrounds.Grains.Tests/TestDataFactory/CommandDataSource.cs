﻿using System.Collections.Generic;
using Synapse.Backgrounds.Grains.Contract.Command;

namespace Backgrounds.Grains.Tests.TestDataFactory
{
    public static class CommandDataSource
    {
        public static IEnumerable<object[]> SingleReleaseCommand()
        {
            yield return new object[]
            {
                new ReleaseWaveCommand
                {
                    Environment = "SYNTSTBConn",
                    Facility = "BTF",
                    UserId = "Sgopinath",
                    Wave = 502784,
                    OrderId = 12345,
                    ShipId = 1,
                    PickType = "BAT",
                    TaskPriority = "2",
                    Trace = "Trace",
                    Item = "ITEM011",
                    LotNumber = "Lot011",
                    Quantity = 7,
                    SessionId = 1233123,
                    CustomerId= "08092018"

                }
            };
        }

        public static IEnumerable<object[]> MulitpleReleaseCommands()
        {
            yield return new object[]
            {
                new ReleaseWaveCommand
                {
                    Environment = "SYNTSTBConn",
                    Facility = "RHR",
                    UserId = "Sgopinath",
                    Wave = 502200,
                    OrderId = 12345,
                    ShipId = 1,
                    PickType = "BAT",
                    TaskPriority = "2",
                    Trace = "Trace",
                    Item = "ITEM011",
                    LotNumber = "Lot011",
                    Quantity = 2,
                    SessionId = 1233123,
                    CustomerId= "08092018"
                }
            };
            yield return new object[]
            {
                new ReleaseWaveCommand
                {
                    Environment = "SYNTSTBConn",
                    Facility = "RHR",
                    UserId = "Sgopinath",
                    Wave = 491791,
                    OrderId = 12345,
                    ShipId = 1,
                    PickType = "BAT",
                    TaskPriority = "2",
                    Trace = "Trace",
                    Item = "ITEM011",
                    LotNumber = "Lot011",
                    Quantity = 2,
                    SessionId = 1233123,
                    CustomerId= "08092018"
                }
            };
        }


        public static IEnumerable<object[]> SingleUnreleaseCommand()
        {
            yield return new object[]
            {
                new UnreleaseWaveCommand
                {
                    Environment = "SYNTSTBConn",
                    Facility = "RHR",
                    UserId = "Sgopinath",
                    Wave = 502200,
                    OrderId = 12345,
                    ShipId = 1,
                    PickType = "BAT",
                    TaskPriority = "2",
                    Trace = "Trace",
                    Item = "ITEM011",
                    LotNumber = "Lot011",
                    Quantity = 2,
                    SessionId = 1233123,
                    CustomerId= "08092018"
                }
            };
        }


        public static IEnumerable<object[]> MulitpleUnreleaseCommands()
        {
            yield return new object[]
            {
                new ReleaseWaveCommand
                {
                    Environment = "SYNTSTBConn",
                    Facility = "RHR",
                    UserId = "Sgopinath",
                    Wave = 319876,
                    OrderId = 12345,
                    ShipId = 1,
                    PickType = "BAT",
                    TaskPriority = "2",
                    Trace = "Trace",
                    Item = "ITEM011",
                    LotNumber = "Lot011",
                    Quantity = 2,
                    SessionId = 1233123,
                    CustomerId= "08092018"
                }
            };
            yield return new object[]
            {
                new ReleaseWaveCommand
                {
                    Environment = "SYNTSTBConn",
                    Facility = "RHR",
                    UserId = "Sgopinath",
                    Wave = 319876,
                    OrderId = 12345,
                    ShipId = 1,
                    PickType = "BAT",
                    TaskPriority = "2",
                    Trace = "Trace",
                    Item = "ITEM011",
                    LotNumber = "Lot011",
                    Quantity = 2,
                    SessionId = 1233123
                }
            };
        }

        public static IEnumerable<object[]> MulitpleRandomCommands()
        {
            for (var i = 0; i < 10; i++)
            {
                yield return new object[]
                {
                    new ReleaseWaveCommand
                    {
                        Environment = "SYNTSTBConn",
                        Facility = "RHR",
                        UserId = "Sgopinath",
                        Wave = 319876,
                        OrderId = 12345,
                        ShipId = 1,
                        PickType = "BAT",
                        TaskPriority = "2",
                        Trace = "Trace",
                        Item = "ITEM011",
                        LotNumber = "Lot011",
                        Quantity = 2,
                        SessionId = 1233123,
                    CustomerId= "08092018"
                    }
                };
                yield return new object[]
                {
                    new UnreleaseWaveCommand
                    {
                        Environment = "SYNTSTBConn",
                        Facility = "RHR",
                        UserId = "Sgopinath",
                        Wave = 319876,
                        OrderId = 12345,
                        ShipId = 1,
                        PickType = "BAT",
                        TaskPriority = "2",
                        Trace = "Trace",
                        Item = "ITEM011",
                        LotNumber = "Lot011",
                        Quantity = 2,
                        SessionId = 1233123,
                    CustomerId= "08092018"
                    }
                };
            }
        }

        public static IEnumerable<object[]> SingleRegenerateBatchCommand()
        {
            yield return new object[]
            {
                new RegenerateBatchCommand()
                {
                    Environment = "SYNTSTBConn",
                    Facility = "RHR",
                    UserId = "Sgopinath",
                    Wave = 502200,
                    OrderId = 19301663,
                    ShipId = 1,
                    PickType = "BAT",
                    TaskPriority = "2",
                    Trace = "Trace",
                    Item = "ITEM011",
                    TaskId=3092220,
                    LotNumber = "Lot011",
                    Quantity = 2,
                    SessionId = 1233123,
                    CustomerId= "08092018"
                }
            };
        }
    }
}
