@echo off
set consolDataPath=%~dp0\Data
echo %consolDataPath%
Consul.exe agent -server -bootstrap -data-dir %consolDataPath% -client=0.0.0.0
consul agent --dev
pause
exit /b 0 