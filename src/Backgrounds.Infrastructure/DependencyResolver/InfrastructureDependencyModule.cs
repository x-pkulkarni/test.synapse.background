﻿// <copyright file="InfrastructureDependencyModule.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Infrastructure</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System.Reflection;
using Autofac;
using Synapse.Backgrounds.Infrastructure.Logging;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;
using Synapse.Backgrounds.Infrastructure.MessageQueue;
using Synapse.Backgrounds.Infrastructure.MessageQueue.Interface;
using Synapse.Backgrounds.Infrastructure.Profiler;
using Synapse.Backgrounds.Infrastructure.Profiler.Interface;
using Synapse.Backgrounds.Infrastructure.Profiler.Mapper;
using Module = Autofac.Module;

namespace Synapse.Backgrounds.Infrastructure.DependencyResolver
{
    public class InfrastructureDependencyModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).AsImplementedInterfaces()
                .Except<ProfilerMapping>()
                .Except<ProfilerMappingEngine>()
                .Except<IProfileService>()
                .Except<IMessageQueueFactory>()
                .Except<ProfileStorageProvider>()
                .Except<ILoggerService>();
            builder.RegisterType<ProfileService>().AsImplementedInterfaces();
            builder.RegisterType<MessageQueueFactory>().AsImplementedInterfaces();
            builder.RegisterType<ProfileStorageProvider>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<LoggerService>().AsImplementedInterfaces().SingleInstance();
        }
    }
}