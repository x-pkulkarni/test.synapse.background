﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Profiling;
using Synapse.Backgrounds.Integration;

namespace Synapse.Backgrounds.Infrastructure.Profiler.Mapper
{
    public class ProfilerMapping : AutoMapper.Profile
    {
        public ProfilerMapping()
        {
            CreateMap<CustomTiming, DataBaseTiming>()
                .ForMember(dest => dest.DataBaseTimingId, opt => opt.MapFrom(src => src.Id));
            CreateMap<MiniProfiler, MiniProfilerMessage>()
                .ForMember(dest => dest.RouteId, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.RouteName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.CustomLinks, opt => opt.MapFrom(src => src.CustomLinks));
        }
    }
}
