﻿using System.Collections.Generic;
using AutoMapper;
using StackExchange.Profiling;
using Synapse.Backgrounds.Integration;

namespace Synapse.Backgrounds.Infrastructure.Profiler.Mapper
{
    public class ProfilerMappingEngine
    {
        private readonly MapperConfiguration _config;

        public ProfilerMappingEngine()
        {
            _config = new MapperConfiguration(cfg => { cfg.AddProfile(typeof(ProfilerMapping)); });
        }

        public MiniProfilerMessage Map(MiniProfiler profiler)
        {
            var mapper = _config.CreateMapper();
            var miniProfileMessage = mapper.Map<MiniProfiler, MiniProfilerMessage>(profiler, opt => opt.ConfigureMap());
            miniProfileMessage.DataBaseTimings = profiler.Root.CustomTimings != null
                ? mapper.Map<List<DataBaseTiming>>(profiler.Root.CustomTimings["sql"])
                : null;
            return miniProfileMessage;
        }
    }
}

