﻿// <copyright file="ProfileStorageProvider.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Infrastructure</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Serilog;
using StackExchange.Profiling;
using StackExchange.Profiling.Internal;
using StackExchange.Profiling.Storage;
using Synapse.Backgrounds.Infrastructure.MessageQueue.Interface;
using Synapse.Backgrounds.Infrastructure.Profiler.Mapper;

namespace Synapse.Backgrounds.Infrastructure.Profiler
{
    using System.Linq;
    using Configuration;
    using EasyNetQ;

    public class ProfileStorageProvider : IAsyncStorage
    {
        private readonly IBus _client;

        public ProfileStorageProvider(IMessageQueueFactory messageQueueFactory, IConfigService configService)
        {
            var logQueueContext = configService.GetQueueContexts().FirstOrDefault(q => string.Equals(q.Name,
                configService.GetAppSetting("LOG_MESSAGE_QUEUE"),
                StringComparison.InvariantCultureIgnoreCase));

            if (logQueueContext != null)
            {
                _client = messageQueueFactory.CreateBus(logQueueContext.PublisherConnectionString);
            }
        }

        public IEnumerable<Guid> List(int maxResults, DateTime? start = null, DateTime? finish = null,
            ListResultsOrder orderBy = ListResultsOrder.Descending)
        {
            throw new NotImplementedException();
        }

        public void Save(MiniProfiler profiler)
        {
            throw new NotImplementedException();
        }

        public MiniProfiler Load(Guid id)
        {
            throw new NotImplementedException();
        }

        public void SetUnviewed(string user, Guid id)
        {
            throw new NotImplementedException();
        }

        public void SetViewed(string user, Guid id)
        {
            throw new NotImplementedException();
        }

        public List<Guid> GetUnviewedIds(string user)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Guid>> ListAsync(int maxResults, DateTime? start = null, DateTime? finish = null,
            ListResultsOrder orderBy = ListResultsOrder.Descending)
        {
            throw new NotImplementedException();
        }

        public async Task SaveAsync(MiniProfiler profiler)
        {
            Log.Logger
                .ForContext("MiniProfiler", profiler.ToJson(), true)
                .Information($"Completed step in {MiniProfiler.Current.Root.DurationMilliseconds} ms");
            Task publish = Task.Run(() => { _client.PublishAsync(new ProfilerMappingEngine().Map(profiler)); });
            await publish;
        }

        public Task<MiniProfiler> LoadAsync(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task SetUnviewedAsync(string user, Guid id)
        {
            throw new NotImplementedException();
        }

        public Task SetViewedAsync(string user, Guid id)
        {
            throw new NotImplementedException();
        }

        public Task<List<Guid>> GetUnviewedIdsAsync(string user)
        {
            throw new NotImplementedException();
        }
    }
}