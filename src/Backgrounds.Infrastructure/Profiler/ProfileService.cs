﻿// <copyright file="ProfileService.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Infrastructure</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using System.Threading.Tasks;
using StackExchange.Profiling;
using StackExchange.Profiling.Storage;
using Synapse.Backgrounds.Infrastructure.Profiler.Interface;

namespace Synapse.Backgrounds.Infrastructure.Profiler
{
    public class ProfileService : IProfileService
    {
        public ProfileService(IAsyncStorage profileStorage)
        {
            MiniProfiler.Configure(new MiniProfilerOptions
            {
                Storage = profileStorage
            });
        }

        public MiniProfiler Start(string sessionName)
        {
            var profiler = MiniProfiler.StartNew(sessionName);
            return profiler;
        }

        public IDisposable Step(MiniProfiler profiler, string stepName)
        {
            return profiler.Step(stepName);
        }

        public MiniProfiler GetCurrentProfiler()
        {
            throw new NotImplementedException();
        }

        public async Task Stop(MiniProfiler profiler) => await profiler.StopAsync(false);

        public async Task Save(MiniProfiler profiler) => await profiler.Storage.SaveAsync(profiler);
    }
}