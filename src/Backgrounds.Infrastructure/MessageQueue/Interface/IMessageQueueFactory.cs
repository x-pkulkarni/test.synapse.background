﻿// <copyright file="IMessageQueueFactory.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Infrastructure</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-25</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

namespace Synapse.Backgrounds.Infrastructure.MessageQueue.Interface
{
    using EasyNetQ;

    public interface IMessageQueueFactory
    {
        IBus CreateBus(string connectionString);
    }
}