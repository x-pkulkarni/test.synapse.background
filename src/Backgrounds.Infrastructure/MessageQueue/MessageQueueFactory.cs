﻿// <copyright file="MessageQueueFactory.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Infrastructure</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>
using System.Collections.Concurrent;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Infrastructure.MessageQueue.Interface;


namespace Synapse.Backgrounds.Infrastructure.MessageQueue
{
    using EasyNetQ;

    public class MessageQueueFactory : IMessageQueueFactory
    {
        private readonly IConfigService _configService;

        private static readonly ConcurrentDictionary<string, IBus> MessageBusDictionary =
            new ConcurrentDictionary<string, IBus>();

        public MessageQueueFactory(IConfigService configService)
        {
            _configService = configService;
        }

        public IBus CreateBus(string connectionString)
        {
            // Check if the instance of a bus is already there with the same key. If yes return that.
            MessageBusDictionary.TryGetValue(connectionString, out var messageBusInstance);

            // if found return the instance.
            if (messageBusInstance != null) return messageBusInstance;

            // Else create a new instance , add to the dict and then return the same instance.
            messageBusInstance = RabbitHutch.CreateBus(connectionString);
            MessageBusDictionary.TryAdd(connectionString, messageBusInstance);
            return messageBusInstance;
        }
    }
}