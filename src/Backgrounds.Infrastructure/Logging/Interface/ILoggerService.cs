﻿// <copyright file="ILoggerService.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Infrastructure</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using Serilog;
using Synapse.Backgrounds.Integration;

namespace Synapse.Backgrounds.Infrastructure.Logging.Interface
{
    public interface ILoggerService
    {
        ILogger GetLogger();
        void Warning(string messageTemplate);
        void Information(string messageTemplate);
        void Debug(string messageTemplate);
        void Error(string messageTemplate);
        void Error(Exception exception, string messageTemplate);
        void Error(Exception exception, ServiceLogContext logContext, object reference, string customMessage);
        void Debug(ServiceLogContext context, object reference, string message);
    }
}