﻿// <copyright file="LoggerService.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Infrastructure</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using Serilog;
using Serilog.Context;
using Serilog.Core;
using Serilog.Events;
using Serilog.Exceptions;
using Serilog.Exceptions.Core;
using Serilog.Formatting.Json;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;
using Synapse.Backgrounds.Integration;

namespace Synapse.Backgrounds.Infrastructure.Logging
{
    using System.Linq;
    using Serilog.Sinks.MessageQueue;
    using Serilog.Sinks.MessageQueue.EasyNetQ;


    public class LoggerService : ILoggerService
    {
        private const string TimeZoneId = "Central Standard Time";
        private readonly IConfigService _configService;
        private readonly LoggingLevelSwitch _loggingLevelSwitch;
        private readonly TimeZoneInfo _apiLogTimeZone;
        private readonly bool _enableMessageQueueLogging;
        private readonly bool _enableFileLogging;
        private readonly LogEventLevel _minLogEventLevel;
        private readonly QueueContext _logQueueContext;

        public LoggerService(IConfigService configService)
        {
            _configService = configService;
            _logQueueContext = configService.GetQueueContexts().FirstOrDefault(q => string.Equals(q.Name,
                configService.GetAppSetting("LOG_MESSAGE_QUEUE"),
                StringComparison.InvariantCultureIgnoreCase));
            bool.TryParse(_configService.GetAppSetting("EnableMessageQueueLogging"), out _enableMessageQueueLogging);
            bool.TryParse(_configService.GetAppSetting("EnableSeriLogFileLogging"), out _enableFileLogging);
            Enum.TryParse(_configService.GetAppSetting("MinLogEventLevel"), out _minLogEventLevel);
            _loggingLevelSwitch = new LoggingLevelSwitch(_minLogEventLevel);
            var apiLogTimeZoneId = _configService.LoggingDateTimeZoneId();
            _apiLogTimeZone = TimeZoneInfo.FindSystemTimeZoneById(apiLogTimeZoneId);
            Log.Logger = CreateConfiguration();
        }

        public ILogger CreateConfiguration()
        {
            var options = new DestructuringOptionsBuilder()
                .WithDefaultDestructurers()
                .WithIgnoreStackTraceAndTargetSiteExceptionFilter();

            var loggerConfiguration = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .Enrich.WithExceptionDetails(options)
                .Enrich.WithEnvironmentUserName()
                .Enrich.WithMachineName()
                .Enrich.WithProcessId()
                .Enrich.WithProcessName()
                .Enrich.WithThreadId()
                .MinimumLevel.ControlledBy(_loggingLevelSwitch)
                .MinimumLevel.Override("EasyNetQ", LogEventLevel.Information);

            if (_enableMessageQueueLogging)
            {
                loggerConfiguration.WriteTo.Logger(
                    lc =>
                    {
                        lc.WriteTo.Async(r => r.MessageQueue<EasyNetQClient>(new MessageQueueConfiguration
                        {
                            ConnectionString = _logQueueContext.PublisherConnectionString,
                            Topic = "SynapseBG"
                        }, new JsonFormatter()));
                    });
            }

            if (_enableFileLogging)
            {
                loggerConfiguration
                    .WriteTo.Logger(lc =>
                    {
                        lc.Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Debug)
                            .Filter.ByExcluding(le => le.Properties.ContainsKey("MiniProfiler"))
                            .Filter.ByExcluding(le => le.Properties.ContainsKey("Orleans"))
                            .WriteTo.Async(a => a.RollingFile(new JsonFormatter(),
                                AppDomain.CurrentDomain.BaseDirectory +
                                _configService.GetAppSetting("Log_DEBUG_PATH")));
                    })
                    .WriteTo.Logger(lc =>
                    {
                        lc.Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Information)
                            .Filter.ByExcluding(le => le.Properties.ContainsKey("MiniProfiler"))
                            .Filter.ByExcluding(le => le.Properties.ContainsKey("Orleans"))
                            .WriteTo.Async(a => a.RollingFile(new JsonFormatter(),
                                AppDomain.CurrentDomain.BaseDirectory +
                                _configService.GetAppSetting("LOG_INFO_PATH")));
                    })
                    .WriteTo.Logger(lc =>
                    {
                        lc.Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Verbose)
                            .Filter.ByExcluding(le => le.Properties.ContainsKey("Orleans"))
                            .WriteTo.Async(a => a.RollingFile(new JsonFormatter(),
                                AppDomain.CurrentDomain.BaseDirectory +
                                _configService.GetAppSetting("LOG_VERBOSE_PATH")));
                    })
                    .WriteTo.Logger(lc =>
                    {
                        lc.Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Error)
                            .WriteTo.Async(a => a.RollingFile(new JsonFormatter(),
                                AppDomain.CurrentDomain.BaseDirectory +
                                _configService.GetAppSetting("LOG_ERROR_PATH")));
                    })
                    .WriteTo.Logger(lc =>
                    {
                        lc.Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Warning)
                            .Filter.ByExcluding(le => le.Properties.ContainsKey("Orleans"))
                            .WriteTo.Async(a => a.RollingFile(new JsonFormatter(),
                                AppDomain.CurrentDomain.BaseDirectory +
                                _configService.GetAppSetting("LOG_WARNING_PATH")));
                    })
                    .WriteTo.Logger(lc =>
                    {
                        lc.Filter.ByIncludingOnly(evt => evt.Level == LogEventLevel.Information)
                            .Filter.ByExcluding(le => le.Properties.ContainsKey("Orleans"))
                            .Filter.ByIncludingOnly(le => le.Properties.ContainsKey("MiniProfiler") &&
                                                          le.Properties["MiniProfiler"].ToString().Length > 12)
                            .WriteTo.Async(a => a.RollingFile(new JsonFormatter(),
                                AppDomain.CurrentDomain.BaseDirectory +
                                _configService.GetAppSetting("PROFILER_DATA_PATH")));
                    });
            }

            return loggerConfiguration.CreateLogger();
        }

        public ILogger GetLogger()
        {
            return Log.Logger;
        }

        public void Warning(string messageTemplate)
        {
            Log.Warning(messageTemplate);
        }

        public void Information(string messageTemplate)
        {
            Log.Information(messageTemplate);
        }

        public void Debug(string messageTemplate)
        {
            Log.Debug(messageTemplate);
        }

        public void Error(string messageTemplate)
        {
            Log.Error(messageTemplate);
        }

        public void Error(Exception exception, string messageTemplate)
        {
            Log.Error(exception, messageTemplate);
        }

        public void Error(Exception exception, ServiceLogContext logContext, object reference, string customMessage)
        {
            using (LogContext.PushProperty("MessageTime", TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, _apiLogTimeZone)))
            {
                using (LogContext.PushProperty("Environment", logContext.Environment))
                {
                    using (LogContext.PushProperty("CustomerId", logContext.CustomerId))
                    {
                        using (LogContext.PushProperty("Facility", logContext.Facility))
                        {
                            using (LogContext.PushProperty("CustomerId", logContext.SessionId))
                            {
                                Log.Error($"Exception message : {customMessage} {exception} with reference: {reference}");
                            }
                        }
                    }
                }
            }
        }

        public void Debug(ServiceLogContext context, object reference, string message)
        {
            using (LogContext.PushProperty("MessageTime", TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.Local, _apiLogTimeZone)))
            {
                using (LogContext.PushProperty("Environment", context.Environment))
                {
                    using (LogContext.PushProperty("CustomerId", context.CustomerId))
                    {
                        using (LogContext.PushProperty("Facility", context.Facility))
                        {
                            using (LogContext.PushProperty("CustomerId", context.SessionId))
                            {
                                Log.Debug($"Message : {message} with reference: {reference}");
                            }
                        }
                    }
                }
            }
        }
    }
}