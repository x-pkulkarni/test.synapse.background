﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Tests.MockObject.Factory
{
    public static class PickTaskRequestDataFactory
    {
        public static IEnumerable<PickTaskRequest> GetRandomPickTaskRequest(ICommand command)
        {
            yield return
                new PickTaskRequest
                {
                    Command = command,
                    Commitment = CommitmentDataFactory.GetRandomCommitment().FirstOrDefault(),
                    Customer = CustomerDataFactory.GetRandomCustomer().FirstOrDefault(),
                    CustItem = CustItemDataFactory.GetRandomCustItem().FirstOrDefault(),
                    Order = OrderDataFactory.GetCommitedOrderTo().FirstOrDefault(),
                    OrderQty = 2,
                    BaseQty = 2
                };
        }
    }
}
