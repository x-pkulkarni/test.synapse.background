﻿// <copyright file="CommitmentDataFactory.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core.Tests</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-23</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-23</lastchangeddate>

using System.Collections.Generic;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Tests.MockObject.Factory
{
    public static class CommitmentDataFactory
    {
        public static IEnumerable<Commitment> GetRandomCommitment()
        {
            yield return
                new Commitment
                {
                    Item = "100601",
                    InventoryClass = "RG",
                    InvStatus = "AV",
                    LotNumber = null,
                    OrderItem = "100601",
                    Quantity = 2,
                    OrderLot = null,
                    OrderId = 19071622,
                    ShipId = 1,
                    Uom = "EA"
                };
            yield return
                new Commitment
                {
                    Item = "BLC12650HB",
                    InventoryClass = "RG",
                    InvStatus = "AV",
                    LotNumber = null,
                    OrderItem = "100601",
                    Quantity = 3,
                    OrderLot = null,
                    OrderId = 19071623,
                    ShipId = 1,
                    Uom = "EA"
                };
        }
    }
}