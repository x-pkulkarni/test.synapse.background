﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Tests.MockObject.Factory
{
    public static class CustItemDataFactory
    {
        public static IEnumerable<CustItem> GetRandomCustItem()
        {
            yield return
                new CustItem
                {
                    Item = "100601",
                    Cube = 893.1,
                    weight = 11.35,
                    BaseUom = "EA",
                    CustId = "17543C",
                    ExpdateRequired = "Y",
                    IataprimaryChemCode = null,
                    IsKit = "P",
                    Variancepct = 0
                };
        }
    }
}
