﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Tests.MockObject.Factory
{
    public static class PlateDataFactory
    {
        public static IEnumerable<Plate> GetRandomPlates()
        {
            yield return
                new Plate
                {
                    OrderId = 19071622,
                    ShipId = 1,
                    CustomerId = "17543C",
                    LoadNo = 0,
                    StopNo = 0,
                    LastUser = "SGOPINATH",
                    Location = "30AG050C",
                    LpId = "139930163020781",
                    UnitOfMeasure = "EA",
                    InvStatus = "AV",
                    InventoryClass = "RG"
                };
        }
    }
}
