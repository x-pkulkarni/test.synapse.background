﻿// <copyright file="WaveDataFactory.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core.Tests</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-23</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-23</lastchangeddate>

using System.Collections.Generic;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Tests.MockObject.Factory
{
    public static class WaveDataFactory
    {
        public static IEnumerable<Wave> GetReleasedWaves()
        {
            yield return
                new Wave
                {
                    PickType = "BAT",
                    LastUser = "SGOPINATH",
                    Id = 410662,
                    SortLocation = "SORT",
                    TaskPriority = "3",
                    Facility = "RHR",
                    Status = Enums.WaveStatus.Released.ToString("D")
                };
        }

        public static IEnumerable<Wave> GetCompletedWaves()
        {
            yield return
                new Wave
                {
                    PickType = "BAT",
                    LastUser = "SGOPINATH",
                    Id = 410662,
                    SortLocation = "SORT",
                    TaskPriority = "3",
                    Facility = "RHR",
                    Status = Enums.WaveStatus.Completed.ToString("D")
                };
        }


        public static IEnumerable<Wave> GetCommittedWaves()
        {
            yield return
                new Wave
                {
                    PickType = "BAT",
                    LastUser = "SGOPINATH",
                    Id = 410662,
                    SortLocation = "SORT",
                    TaskPriority = "3",
                    Facility = "RHR",
                    Status = Enums.WaveStatus.Committed.ToString("D")
                };
        }
    }
}