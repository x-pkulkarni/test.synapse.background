﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Tests.MockObject.Factory
{
    internal class SubTaskDataFactory
    {
        public static IEnumerable<SubTask> GetRandomSubTasks()
        {
            yield return
                new SubTask
                {
                    OrderId = 19071622,
                    ShipId = 1,
                    CustId = "17543C",
                    LpId = "111001452110014",
                    OrderItem = "100601",
                    Item = "100601",
                    OrderLot = "100601",
                    UOM = "PK",
                    PickUom = "PK",
                    PickToType = "PAL",
                    CartonType = "PAL",
                    Wave = 415416,
                    Quantity = 2,
                    PickQty = 2,
                    LastUser = "SGOPINATH",
                    TaskId = 1234567890,
                    Facility = "RHR",
                    TaskType = "FP",
                    Priority = "2",
                    RowId = new Guid().ToString()
                };
            yield return
                new SubTask
                {
                    OrderId = 19071622,
                    ShipId = 1,
                    CustId = "17543C",
                    LpId = "111001452110014",
                    OrderItem = "100601",
                    Item = "100601",
                    OrderLot = "100601",
                    UOM = "PK",
                    PickUom = "PK",
                    PickToType = "PAL",
                    CartonType = "PAL",
                    Wave = 415416,
                    Quantity = 1,
                    PickQty = 1,
                    LastUser = "SGOPINATH",
                    TaskId = 987654321,
                    Facility = "RHR",
                    TaskType = "FP",
                    Priority = "2",
                    RowId = new Guid().ToString()
                };
        }
    }
}
