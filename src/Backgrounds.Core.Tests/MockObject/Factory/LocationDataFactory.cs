﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Tests.MockObject.Factory
{
    public static class LocationDataFactory
    {
        public static IEnumerable<Location> GetRandomLocation()
        {
            yield return
                new Location
                {
                    Facility = "RHR",
                    Equipprof = "AL",
                    LocId = "101",
                    PickingZone = "STO",
                    PickingSeq = 100,
                    Section = "01"
                };
        }
    }
}
