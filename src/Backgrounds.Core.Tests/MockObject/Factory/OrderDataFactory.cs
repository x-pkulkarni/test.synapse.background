﻿// <copyright file="OrderDataFactory.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core.Tests</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-23</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-23</lastchangeddate>

using System.Collections.Generic;
using System.Linq;
using Synapse.Backgrounds.Core.Model;

namespace Synapse.Backgrounds.Core.Tests.MockObject.Factory
{
    public static class OrderDataFactory
    {
        public static IEnumerable<OrderHeader> GetSingleCommittedOrder()
        {
            yield return
                new OrderHeader
                {
                    OrderId = 19071622,
                    ShipId = 1,
                    CustomerId = "17543C",
                    OrderType = "O",
                    OrderStatus = Constants.OrderStatus.Committed,
                    CommitStatus = Enums.CommitStatus.Released.ToString("D"),
                    FromFacility = "RHR",
                    ToFacility = null,
                    LoadNo = 30,
                    StopNo = 0,
                    ShipTo = "SAM",
                    StageLoc = null,
                    Wave = 415416,
                    Priority = "A",
                    LastUser = "SGOPINATH",
                    Carrier = "8PHT",
                    ShipType = "L"
                };
        }


        public static IEnumerable<OrderHeader> GetSingleReleasedOrder()
        {
            yield return
                new OrderHeader
                {
                    OrderId = 19071622,
                    ShipId = 1,
                    CustomerId = "17543C",
                    OrderType = "O",
                    OrderStatus = Constants.OrderStatus.Released,
                    CommitStatus = Enums.CommitStatus.Released.ToString("D"),
                    FromFacility = "RHR",
                    ToFacility = null,
                    LoadNo = 120,
                    StopNo = 0,
                    ShipTo = "SAM",
                    StageLoc = null,
                    Wave = 415416,
                    Priority = "A",
                    LastUser = "SGOPINATH",
                    Carrier = "8PHT",
                    ShipType = "L"
                };
        }

        public static IEnumerable<OrderHeader> GetCommitedOrders()
        {
            yield return
                new OrderHeader
                {
                    OrderId = 19071622,
                    ShipId = 1,
                    CustomerId = "17543C",
                    OrderType = "O",
                    OrderStatus = Constants.OrderStatus.Committed,
                    CommitStatus = Enums.CommitStatus.Released.ToString("D"),
                    FromFacility = "RHR",
                    ToFacility = null,
                    LoadNo = 123,
                    StopNo = 0,
                    ShipTo = "SAM",
                    StageLoc = null,
                    Wave = 415416,
                    Priority = "A",
                    LastUser = "SGOPINATH",
                    Carrier = "8PHT",
                    ShipType = "L"
                };
            yield return
                new OrderHeader
                {
                    OrderId = 19071646,
                    ShipId = 1,
                    CustomerId = "17543C",
                    OrderType = "O",
                    OrderStatus = Constants.OrderStatus.Committed,
                    CommitStatus = Enums.CommitStatus.Released.ToString("D"),
                    FromFacility = "RHR",
                    ToFacility = null,
                    LoadNo = 122,
                    StopNo = 0,
                    ShipTo = "SAM",
                    StageLoc = null,
                    Wave = 415416,
                    Priority = "A",
                    LastUser = "SGOPINATH",
                    Carrier = "8PHT",
                    ShipType = "L"
                };
        }

        public static IEnumerable<OrderDetail> GetCommitedOrderDetail()
        {
            yield return
                new OrderDetail
                {
                    OrderId = 19071622,
                    ShipId = 1,
                    Item = "100601",
                    LotNumber = null,
                    InvStatus = "AV",
                    QuantityType = "E",
                    InventoryClass = "RG",
                    CustomerId = "17543C",
                    FromFacility = "RHR",
                    Uom = "EA",
                    LineStatus = "A",
                    CommitStatus = null,
                    QtyEntered = 3,
                    ItemEntered = "100601",
                    UomEntered = "EA",
                    QtyOrder = 3,
                    WeightOrder = 34.05,
                    CubeOrder = 1.5504,
                    QtyCommit = 3,
                    Vendor = null,
                    QtyPick = 0,
                    Priority = "A",
                    StaffHrs = 0.0066,
                    LastUser = "SGOPINATH",
                    MinDaysToExpiration = 0,
                    IataPrimaryChemCode = null,
                    DynamicInvUsedFlag = null
                };
        }

        public static IEnumerable<OrderDetail> GetReleasedOrderDetail()
        {
            yield return
                new OrderDetail
                {
                    OrderId = 19071622,
                    ShipId = 1,
                    Item = "100601",
                    LotNumber = null,
                    InvStatus = "AV",
                    QuantityType = "E",
                    InventoryClass = "RG",
                    CustomerId = "17543C",
                    FromFacility = "RHR",
                    Uom = "EA",
                    LineStatus = "A",
                    CommitStatus = null,
                    QtyEntered = 3,
                    ItemEntered = "100601",
                    UomEntered = "EA",
                    QtyOrder = 3,
                    WeightOrder = 34.05,
                    CubeOrder = 1.5504,
                    QtyCommit = 3,
                    Vendor = null,
                    QtyPick = 0,
                    Priority = "A",
                    StaffHrs = 0.0066,
                    LastUser = "SGOPINATH",
                    MinDaysToExpiration = 0,
                    IataPrimaryChemCode = null,
                    DynamicInvUsedFlag = null
                };
        }

        public static IEnumerable<OrderTo> GetCommitedOrderTo()
        {
            yield return new OrderTo
            {
                HeaderInfo = GetCommitedOrders().First(),
                OrderDetail = GetCommitedOrderDetail().First(),
                WaveInfo = WaveDataFactory.GetCommittedWaves().First()
            };
        }
    }
}