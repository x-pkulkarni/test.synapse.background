﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Moq;
using Synapse.Backgrounds.Core.Aggregates.Initializer;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Tests.MockObject.Builder
{
    using Newtonsoft.Json.Linq;

    internal class FindAPickAggregateInitializerBuilder
    {

        #region Private Member(S)
        private IPickDbContext PickDbContext { get; set; }
        private IBatchTaskAggregate BatchTaskAggregate { get; set; }
        private IShippingPlateDbContext ShippingPlateDbContext { get; set; }
        private ILocationDbContext LocationDbContext { get; set; }
        private ITaskService TaskDomain { get; set; }
        private ISubTaskAggregate SubTaskAggregate { get; set; }
        private IPlateAggregate PlateAggregate { get; set; }
        private ICatchWeightAggregate CatchWeightAggregate { get; set; }
        private ICustItemAggregate CustItemAggregate { get; set; }
        #endregion

        internal FindAPickAggregateInitializerBuilder SetPickDbContext()
        {
            var fakePickDbContext = new Mock<IPickDbContext>();
            fakePickDbContext.Setup(w => w.FindAPick(It.IsAny<IDbConnection>(), It.IsAny<FindAPickRequest>()))
                .Returns(new List<FindAPickResponse>());
            PickDbContext = fakePickDbContext.Object;
            return this;
        }

        internal FindAPickAggregateInitializerBuilder SetBatchTaskAggregate()
        {
            var fakeBatchTaskAggregate = new Mock<IBatchTaskAggregate>();
            //fakeBatchTaskAggregate.Setup(w => w.CreateBatchTask(It.IsAny<IDbConnection>(), It.IsAny<OrderTo>(),
            //        It.IsAny<Commitment>(), It.IsAny<Location>(), It.IsAny<Location>(),
            //        It.IsAny<FindAPickResponse>(), It.IsAny<ICommand>()))
            //    .Returns(true);
            fakeBatchTaskAggregate.Setup(w => w.CreateBatchTask(It.IsAny<IDbConnection>(), It.IsAny<BatchTask>()))
                .Returns(true);
            BatchTaskAggregate = fakeBatchTaskAggregate.Object;
            return this;
        }

        internal FindAPickAggregateInitializerBuilder SetShippingPlateDbContext(string lpId = "123123123231")
        {
            var fakeShippingPlateDbContext = new Mock<IShippingPlateDbContext>();
            fakeShippingPlateDbContext.Setup(w => w.GetNextShippingLpId(It.IsAny<IDbConnection>()))
                .Returns(lpId);
            fakeShippingPlateDbContext.Setup(w => w.Insert(It.IsAny<IDbConnection>(), It.IsAny<ShippingPlate>()))
                .Returns(true);
            ShippingPlateDbContext = fakeShippingPlateDbContext.Object;
            return this;
        }

        internal FindAPickAggregateInitializerBuilder SetLocationDbContext(IEnumerable<Location> locations)
        {
            var fakeLocationDbContext = new Mock<ILocationDbContext>();
            fakeLocationDbContext.Setup(w => w.GetLocDetailsByLocId(It.IsAny<IDbConnection>(), It.IsAny<Location>()))
                .Returns(locations);
            LocationDbContext = fakeLocationDbContext.Object;
            return this;
        }

        internal FindAPickAggregateInitializerBuilder SetTaskDomain(long taskId = 123123)
        {
            var fakeTaskDomain = new Mock<ITaskService>();
            fakeTaskDomain.Setup(w => w.UpdateTask(It.IsAny<IDbConnection>(), It.IsAny<Model.Task>()))
                .Returns(true);
            fakeTaskDomain.Setup(w => w.CreateTaskForFullPick(It.IsAny<IDbConnection>(), 
                    It.IsAny<Tuple<OrderTo, Commitment, ICommand, Location, Location, FindAPickResponse>>(),
                    It.IsAny<string>()))
                .Returns(new Task());
            TaskDomain = fakeTaskDomain.Object;
            return this;
        }

        internal FindAPickAggregateInitializerBuilder SetSubTaskAggregate()
        {
            var fakeSubTaskAggregate = new Mock<ISubTaskAggregate>();
            fakeSubTaskAggregate.Setup(w => w.CreateSubTaskForFullPick(It.IsAny<IDbConnection>(), It.IsAny<OrderTo>(),
                    It.IsAny<Commitment>(), It.IsAny<Location>(), It.IsAny<Location>(), It.IsAny<FindAPickResponse>(),
                    It.IsAny<Customer>(), It.IsAny<ICommand>(), It.IsAny<string>(), It.IsAny<Task>(),
                    It.IsAny<string>(), It.IsAny<string>()))
                .Returns(true);
            SubTaskAggregate = fakeSubTaskAggregate.Object;
            return this;
        }

        internal FindAPickAggregateInitializerBuilder SetCatchWeightAggregate(double itemWeight = 12.9)
        {
            var fakeCatchWeightAggregate = new Mock<ICatchWeightAggregate>();
            fakeCatchWeightAggregate.Setup(w => w.GetItemWeightInPlate(It.IsAny<IDbConnection>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(itemWeight);
            CatchWeightAggregate = fakeCatchWeightAggregate.Object;
            return this;
        }

        internal FindAPickAggregateInitializerBuilder SetCustItemAggregate(double itemWeight = 12.9)
        {
            var fakeCustItemAggregate = new Mock<ICustItemAggregate>();
            fakeCustItemAggregate.Setup(w => w.GetItemHazmatIndicator(It.IsAny<IDbConnection>(), It.IsAny<string>(),
                    It.IsAny<long>(), It.IsAny<int>()))
                .Returns("Y");
            fakeCustItemAggregate.Setup(w => w.GetItemUnCode(It.IsAny<IDbConnection>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<long>(), It.IsAny<int>()))
                .Returns("Y");
            CustItemAggregate = fakeCustItemAggregate.Object;
            return this;
        }
        internal FindAPickAggregateInitializerBuilder SetPlateAggregate(IEnumerable<Plate> plates)
        {
            var fakePlateAggregate = new Mock<IPlateAggregate>();
            fakePlateAggregate.Setup(w => w.GetPlateByLpId(It.IsAny<IDbConnection>(), It.IsAny<string>()))
                .Returns(plates.ToList());
            fakePlateAggregate.Setup(w => w.UpdatePlateQtyAsked(It.IsAny<IDbConnection>(), It.IsAny<string>(), 
                    It.IsAny<int>(), It.IsAny<string>()))
                .Returns(true);
            PlateAggregate = fakePlateAggregate.Object;
            return this;
        }


        internal FindAPickAggregateInitializer Build()
        {
            return new FindAPickAggregateInitializer(PickDbContext,BatchTaskAggregate,ShippingPlateDbContext,LocationDbContext,
                TaskDomain,SubTaskAggregate,CustItemAggregate,CatchWeightAggregate,PlateAggregate);

        }
    }
}
