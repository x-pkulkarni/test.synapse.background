﻿// <copyright file="ItemAggregateInitializerBuilder.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core.Tests</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-23</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-23</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using Moq;
using Synapse.Backgrounds.Core.Aggregates;
using Synapse.Backgrounds.Core.Aggregates.Initializer;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Core.Profile;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Tests.MockObject.Builder
{
    using Newtonsoft.Json.Linq;

    internal class ItemAggregateInitializerBuilder
    {
        internal ItemAggregateInitializerBuilder()
        {
            MappingEngine = Mock.Of<MappingEngine<ItemAggregateMapperProfile>>();
        }

        #region Private Member(S)

        private MappingEngine<ItemAggregateMapperProfile> MappingEngine { get; }
        private IOrderDbContext OrderDbContext { get; set; }
        private ITaskAggregate TaskAggregate { get; set; }
        private ICommitmentDbContext CommitmentDbContext { get; set; }
        private ICustItemDbContext CustItemDbContext { get; set; }
        private ICustomerDbContext CustomerDbContext { get; set; }
        private IItemDemandDbContext ItemDemandDbContext { get; set; }
        private IFindAPickAggregate FindAPickAggregate { get; set; }

        #endregion

        internal ItemAggregateInitializerBuilder SetOrderDbContext(IEnumerable<OrderDetail> orderDetails)
        {
            var fakeOrderDbContext = new Mock<IOrderDbContext>();
            OrderDbContext = fakeOrderDbContext.Object;
            return this;
        }
        
        internal ItemAggregateInitializerBuilder SetCommitmentDbContext(IEnumerable<Commitment> commitments)
        {
            var fakeCommitmentDbContext = new Mock<ICommitmentDbContext>();
            fakeCommitmentDbContext.Setup(w =>
                    w.GetVendorTrackedCommitments(It.IsAny<IDbConnection>(), It.IsAny<Commitment>()))
                .Returns(commitments);
            fakeCommitmentDbContext.Setup(w =>
                    w.GetNonVendorTrackedCommitments(It.IsAny<IDbConnection>(), It.IsAny<Commitment>()))
                .Returns(commitments);
            CommitmentDbContext = fakeCommitmentDbContext.Object;
            return this;
        }


        internal ItemAggregateInitializerBuilder SetCustItemDbContext(long taskId = 1234567890)
        {
            var fakeCustItemDbContext = new Mock<ICustItemDbContext>();
            fakeCustItemDbContext.Setup(w =>
                    w.GetCustItemView(It.IsAny<IDbConnection>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<CustItem>());
            fakeCustItemDbContext.Setup(w =>
                w.GetVendorTrackedFlag(It.IsAny<IDbConnection>(), It.IsAny<string>(), It.IsAny<string>())).Returns("Y");
            fakeCustItemDbContext.Setup(w =>
                w.GetCustItemIsKitFlag(It.IsAny<IDbConnection>(), It.IsAny<string>(), It.IsAny<string>())).Returns(true);
            fakeCustItemDbContext.Setup(w =>
                    w.GetCustItemSubstituteByItem(It.IsAny<IDbConnection>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(new List<CustItemSubstitute>());
            CustItemDbContext = fakeCustItemDbContext.Object;
            return this;
        }

        internal ItemAggregateInitializerBuilder SetCustomerDbContext(IEnumerable<Customer> customers)
        {
            var fakeCustomerDbContext = new Mock<ICustomerDbContext>();
            fakeCustomerDbContext.Setup(w =>
                    w.GetCustomerPickByLine(It.IsAny<IDbConnection>(), It.IsAny<string>()))
                .Returns(customers);
            CustomerDbContext = fakeCustomerDbContext.Object;
            return this;
        }

      
        internal ItemAggregateInitializerBuilder SetItemDemandDbContext(double cube = 4)
        {
            var fakeItemDemandDbContext = new Mock<IItemDemandDbContext>();
            fakeItemDemandDbContext.Setup(w =>
                    w.Insert(It.IsAny<IDbConnection>(), It.IsAny<ItemDemand>()))
                .Returns(true);
            ItemDemandDbContext = fakeItemDemandDbContext.Object;
            return this;
        }

        internal ItemAggregateInitializerBuilder SetFindAPickAggregate(double staffHours = 12.3)
        {
            var fakeFindAPickAggregate = new Mock<IFindAPickAggregate>();
            var qty = 3;
            fakeFindAPickAggregate.Setup(w =>
                w.ExecuteFullPick(It.IsAny<IDbConnection>(), It.IsAny<PickTaskRequest>()));
            fakeFindAPickAggregate.Setup(w =>
                w.ExecuteBatchPick(It.IsAny<IDbConnection>(), It.IsAny<PickTaskRequest>()));
            FindAPickAggregate = fakeFindAPickAggregate.Object;
            return this;
        }

        internal ItemAggregateInitializer Build()
        {
            return new ItemAggregateInitializer(OrderDbContext, MappingEngine,
                TaskAggregate,
                CommitmentDbContext,
                CustItemDbContext, CustomerDbContext, ItemDemandDbContext,
                new Lazy<IFindAPickAggregate>(() => FindAPickAggregate));
        }
    }
}