﻿// <copyright file="OrderAggregateInitilizerBuilder.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core.Tests</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-23</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-23</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using Moq;
using Synapse.Backgrounds.Core.Aggregates;
using Synapse.Backgrounds.Core.Aggregates.Initializer;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Aggregates.Validation;
using Synapse.Backgrounds.Core.Aggregates.Validation.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Tests.MockObject.Builder
{
    using Data;
    using Infrastructure.Logging.Interface;
    using Newtonsoft.Json.Linq;

    internal class OrderAggregateInitializerBuilder
    {
        internal OrderAggregateInitializerBuilder()
        {
            InfrastructureInitializer = new InfrastructureInitializerBuilder().Build();
            LoggerService = new Mock<ILoggerService>().Object;
        }

        #region Private Member(S)

        private IOrderDbContext OrderDbContext { get; set; }
        private IItemAggregate ItemAggregate { get; set; }
        private IValidator<OrderValidator> OrderValidator { get; set; }
        private ICarrierStageLocationDbContext CarrierStageLocDbContext { get; set; }
        private IInfrastructureInitializer InfrastructureInitializer { get; set; }
        private IBatchTaskAggregate BatchTaskAggregate { get; set; }
        private IFelAggregate FelAggregate { get; set; }
        private IWaveAggregate WaveAggregate { get; set; }
        private ISubTaskAggregate SubTaskAggregate { get; set; }
        private IOrderStatusAggregate OrderStatusAggregate { get; set; }
        private ICustItemAggregate CustItemAggregate { get; set; }
        private IFindAPickAggregate FindAPickAggregate { get; set; }
        private ITaskService TaskService { get; set; }
        private ICartonService CartonService { get; set; }

        private IPlateAggregate PlateAggregate { get; set; }
        private ILoggerService LoggerService { get; }
        private IShippingPlateDbContext shippingPlateDbContext { get; }
        #endregion


        internal OrderAggregateInitializerBuilder SetOrderDbContext(List<OrderHeader> orderHeaders,
            List<OrderDetail> orderDetails)
        {
            var fakeOrderDbContext = new Mock<IOrderDbContext>();
            fakeOrderDbContext.Setup(w => w.GetOrderHeaderByWave(It.IsAny<IDbConnection>(), It.IsAny<long>()))
                .Returns(orderHeaders);
            fakeOrderDbContext.Setup(w =>
                    w.GetOrderDetailsById(It.IsAny<IDbConnection>(), It.IsAny<long>(), It.IsAny<int>()))
                .Returns(orderDetails);
            fakeOrderDbContext.Setup(w =>
                    w.GetOrderHeaderByOrderId(It.IsAny<IDbConnection>(), It.IsAny<long>(), It.IsAny<int>()))
                .Returns(orderHeaders);
            fakeOrderDbContext.Setup(w =>
                    w.GetOrderCountByWave(It.IsAny<IDbConnection>(), It.IsAny<long>()))
                .Returns(1);
            OrderDbContext = fakeOrderDbContext.Object;
            return this;
        }

        internal OrderAggregateInitializerBuilder SetItemAggregate()
        {
            var fakeItemAggregate = new Mock<IItemAggregate>();
            fakeItemAggregate.Setup(w =>
                w.ExecuteReleasLine(It.IsAny<IDbConnection>(), It.IsAny<OrderTo>(), It.IsAny<ICommand>()));

            ItemAggregate = fakeItemAggregate.Object;
            return this;
        }

        internal OrderAggregateInitializerBuilder SetBatchTaskAggregate()
        {
            var fakeBatchTaskAggregate = new Mock<IBatchTaskAggregate>();
            fakeBatchTaskAggregate.Setup(w =>
                w.GenerateBatchTasks(It.IsAny<IDbConnection>(), It.IsAny<Wave>(), It.IsAny<ICommand>(), null));

            BatchTaskAggregate = fakeBatchTaskAggregate.Object;
            return this;
        }
        internal OrderAggregateInitializerBuilder SetFelAggregate()
        {
            var fakeFelAggregate = new Mock<IFelAggregate>();
            fakeFelAggregate.Setup(w =>
                w.ExecuteUnReleaseFelOrder(It.IsAny<IDbConnection>(), It.IsAny<Wave>(), It.IsAny<ICommand>()));

            FelAggregate = fakeFelAggregate.Object;
            return this;
        }
        internal OrderAggregateInitializerBuilder SetWaveAggregate()
        {
            var fakeWaveAggregate = new Mock<IWaveAggregate>();
            fakeWaveAggregate.Setup(w =>
                w.ExecuteUnreleaseWave(It.IsAny<IDbConnection>(), It.IsAny<ICommand>()));

            WaveAggregate = fakeWaveAggregate.Object;
            return this;
        }

        internal OrderAggregateInitializerBuilder SetSubTaskAggregate()
        {
            var fakeSubTaskAggregate = new Mock<ISubTaskAggregate>();
            fakeSubTaskAggregate.Setup(w =>
                w.ResetTasksForUnrelease(It.IsAny<IDbConnection>(), It.IsAny<OrderUnreleaseRequest>()));

            SubTaskAggregate = fakeSubTaskAggregate.Object;
            return this;
        }

        internal OrderAggregateInitializerBuilder SetOrderStatusAggregate()
        {
            var fakeOrderStatusAggregate = new Mock<IOrderStatusAggregate>();
            fakeOrderStatusAggregate.Setup(w =>
                w.UpdateOrderHeaderStatusToReleased(It.IsAny<IDbConnection>(), It.IsAny<OrderHeader>(),
                    It.IsAny<ICommand>()));
            fakeOrderStatusAggregate.Setup(w =>
                w.AddOrderHistory(It.IsAny<IDbConnection>(), It.IsAny<OrderHeader>(), It.IsAny<string>(),
                    It.IsAny<string>()));
            fakeOrderStatusAggregate.Setup(w =>
                w.UpdateLoadStatus(It.IsAny<IDbConnection>(), It.IsAny<int>(), It.IsAny<string>(),
                    It.IsAny<int>()));

            OrderStatusAggregate = fakeOrderStatusAggregate.Object;
            return this;
        }


        internal OrderAggregateInitializerBuilder SetCarrierStageLocDbContext()
        {
            var fakeCarrierStageLocDbContext = new Mock<ICarrierStageLocationDbContext>();
            fakeCarrierStageLocDbContext.Setup(w => w.GetStageLocationByCarrier(It.IsAny<IDbConnection>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(string.Empty);
            CarrierStageLocDbContext = fakeCarrierStageLocDbContext.Object;
            return this;
        }

        internal OrderAggregateInitializerBuilder SetFindAPickAggregate()
        {
            var fakeFindAPickAggregate = new Mock<IFindAPickAggregate>();
            fakeFindAPickAggregate.Setup(w => w.ExecuteFindAPickForRemainders(It.IsAny<IDbConnection>(),
                It.IsAny<PickTaskRequest>()));
            FindAPickAggregate = fakeFindAPickAggregate.Object;
            return this;
        }
        internal OrderAggregateInitializerBuilder SetPlateAggregate()
        {
            var fakePlateAggregate = new Mock<IPlateAggregate>();
            fakePlateAggregate.Setup(w => w.UpdatePlateQtyAsked(It.IsAny<IDbConnection>(),
                It.IsAny<string>(), It.IsAny<int>(), It.IsAny<string>())).Returns(true);
            PlateAggregate = fakePlateAggregate.Object;
            return this;
        }

        internal OrderAggregateInitializerBuilder SetTaskService()
        {
            var fakeTaskService = new Mock<ITaskService>();
            fakeTaskService.Setup(w => w.DeleteByOrderAndType(It.IsAny<IDbConnection>(),
                It.IsAny<long>(), It.IsAny<int>(), It.IsAny<string>()));
            fakeTaskService.Setup(w => w.CreateTaskFromSubTask(It.IsAny<IDbConnection>(),
                It.IsAny<SubTask>(), It.IsAny<string>())).Returns(new Task()
                {
                    PickQty = 3,
                    Quantity = 3,
                    Weight = 1.1,
                    Cube = 2,
                    StaffHrs = 12,
                    TaskId = 19245700
                });
            fakeTaskService.Setup(w => w.GetTaskGroupingRulesByZone(It.IsAny<IDbConnection>(),
                It.IsAny<string>(), It.IsAny<string>())).Returns(new Zone
                {
                    BatchTasksLimit = 0,
                    OrderTasksLimit = 0,
                    LineTasksLimit = 0,
                    SeparateBatchTasksFlag = "N",
                    SeparateOrderTasksFlag = "N",
                    SeparateLineTasksFlag = "N"
                });

            TaskService = fakeTaskService.Object;
            return this;
        }

        internal OrderAggregateInitializerBuilder SetValidator()
        {
            if (OrderDbContext == null)
                throw new Exception("Dependent objects must be preploaded!");
            if (CarrierStageLocDbContext == null)
                SetCarrierStageLocDbContext();
            OrderValidator = new OrderValidator(InfrastructureInitializer,
                OrderDbContext, CarrierStageLocDbContext);
            return this;
        }

        internal OrderAggregateInitializer Build()
        {
            SetFindAPickAggregate();
            SetTaskService();
            return new OrderAggregateInitializer(OrderDbContext,
                            new Lazy<IItemAggregate>(() => ItemAggregate),
                            OrderValidator, BatchTaskAggregate, SubTaskAggregate,
                            new Lazy<IOrderStatusAggregate>(() => OrderStatusAggregate),
                            TaskService, CartonService, FindAPickAggregate,
                            PlateAggregate, CustItemAggregate,
                            new CustomerAuxDbContext(),
                            new Lazy<ILoggerService>(() => LoggerService),
                            FelAggregate, WaveAggregate, shippingPlateDbContext);
        }
    }
}