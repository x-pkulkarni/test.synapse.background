﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Core.Aggregates;
using Synapse.Backgrounds.Core.Aggregates.Initializer;
using Synapse.Backgrounds.Core.Data;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Profile;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;

namespace Synapse.Backgrounds.Core.Tests.MockObject.Builder
{
    internal class InfrastructureInitializerBuilder
    {
        #region Private Member(S)
        
        private IOracleConnectionProvider OracleConnection { get; }
        private IConfigService ConfigService { get; }
        private ILoggerService LoggerService { get; }

        #endregion

        public InfrastructureInitializerBuilder()
        {
            OracleConnection = new Mock<IOracleConnectionProvider>().Object;
            ConfigService = new Mock<IConfigService>().Object;
            LoggerService  = new Mock<ILoggerService>().Object;
        }
        internal InfrastructureInitializer Build()
        {
            return new InfrastructureInitializer(new Lazy<IOracleConnectionProvider>(() => OracleConnection),
                new Lazy<IConfigService>(() => ConfigService),
                new Lazy<ILoggerService>(() => LoggerService),new ParcelStagingDbContext());

        }
    }
}
