﻿// <copyright file="WaveAggregateInitilizerBuilder.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core.Tests</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-23</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-23</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Moq;
using Synapse.Backgrounds.Core.Aggregates;
using Synapse.Backgrounds.Core.Aggregates.Initializer;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Aggregates.Validation;
using Synapse.Backgrounds.Core.Aggregates.Validation.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Core.Tests.MockObject.Builder
{
    using Data;

    internal class WaveAggregateInitilizerBuilder
    {
        #region Private Member(S)

        private IWaveDbContext WaveDbContext { get; set; }
        private IOrderAggregate OrderAggregate { get; set; }
        private IValidator<WaveValidator> Validator { get; set; }
        private IInfrastructureInitializer InfrastructureInitializer { get; set; }

        #endregion

        public WaveAggregateInitilizerBuilder()
        {
            InfrastructureInitializer = new InfrastructureInitializerBuilder().Build();
        }

        internal WaveAggregateInitilizerBuilder SetWaveDbContext(IEnumerable<Wave> waves)
        {
            var fakeWaveDbContext = new Mock<IWaveDbContext>();
            fakeWaveDbContext.Setup(w => w.GetWaveById(It.IsAny<IDbConnection>(), It.IsAny<long>()))
                .Returns(waves);
            fakeWaveDbContext.Setup(w => w.UpdateProcessingIndicatorStart(It.IsAny<IDbConnection>(), It.IsAny<long>()))
                .Returns(true);
            fakeWaveDbContext.Setup(w => w.UpdateWaveStatus(It.IsAny<IDbConnection>(), It.IsAny<long>(),
                    It.IsAny<string>(), It.IsAny<string>()))
                .Returns(true);
            fakeWaveDbContext.Setup(w => w.UpdateProcessingIndicatorEnd(It.IsAny<IDbConnection>(), It.IsAny<long>()))
                .Returns(true);
            WaveDbContext = fakeWaveDbContext.Object;
            return this;
        }

        internal WaveAggregateInitilizerBuilder SetOrderAggregate(IEnumerable<Wave> waves, ICommand request)
        {
            var fakeOrderAggregate = new Mock<IOrderAggregate>();
            fakeOrderAggregate.Setup(w =>
                w.ExecuteReleaseOrder(It.IsAny<IDbConnection>(), waves.FirstOrDefault(), request));
            fakeOrderAggregate.Setup(w =>
                w.ExecuteUnreleaseOrder(It.IsAny<IDbConnection>(), waves.FirstOrDefault(), request));
            OrderAggregate = fakeOrderAggregate.Object;
            return this;
        }

        internal WaveAggregateInitilizerBuilder SetValidator(IEnumerable<Wave> waves)
        {
            if (WaveDbContext == null)
                throw new Exception("Wave DbContext is not set!");
            var fakeTaskDbContext = new Mock<ITaskDbContext>();
            fakeTaskDbContext.Setup(t => t.GetActiveTasksCount(It.IsAny<IDbConnection>(), It.IsAny<long>()))
                .Returns(0);
            fakeTaskDbContext.Setup(w =>
                w.GetNextTaskId(It.IsAny<IDbConnection>())).Returns(14216784);
            var validator = new WaveValidator(InfrastructureInitializer,
                WaveDbContext, fakeTaskDbContext.Object, new CustomerAuxDbContext(), new OrderDbContext());
            Validator = validator;
            return this;
        }

        internal WaveAggregateInitializer Build()
        {
            return new WaveAggregateInitializer(WaveDbContext, new Lazy<IOrderAggregate>(()=>  OrderAggregate),
                Validator);
        }
    }
}