﻿using System.Collections.Generic;
using Autofac;
using Autofac.Core;
using Synapse.Backgrounds.Configuration.DependencyResolver;
using Synapse.Backgrounds.Core.DependencyResolver;
using Synapse.Backgrounds.Infrastructure.DependencyResolver;

namespace Synapse.Backgrounds.Core.Tests
{
    public class TestBase
    {
        private IContainer _container;
        private ContainerBuilder _builder;
        public TestBase()
        {
            _builder = new ContainerBuilder();
            _builder.RegisterModule<ConfigurationDependencyModule>();
            _builder.RegisterModule<InfrastructureDependencyModule>();
            _builder.RegisterModule<CoreDependencyModule>();
            _container = _builder.Build();
        }

        public T GetInstance<T>(IEnumerable<Parameter> obj)
        {
            if (_container == null)
                _container = _builder.Build();
            if (obj != null)
                return _container.BeginLifetimeScope().Resolve<T>(obj);
            return _container.BeginLifetimeScope().Resolve<T>();


        }

        public T GetInstance<T>()
        {
            if (_container == null)
                _container = _builder.Build();
            return _container.BeginLifetimeScope().Resolve<T>();

        }

    }
}
