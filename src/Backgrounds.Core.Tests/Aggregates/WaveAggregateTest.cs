﻿// <copyright file="WaveAggregateTest.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core.Tests</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-23</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-23</lastchangeddate>

using System;
using System.Collections.Generic;
using System.Data;
using Autofac;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Core.Aggregates;
using Synapse.Backgrounds.Core.Aggregates.Initializer;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Core.Tests.MockObject.Builder;
using Synapse.Backgrounds.Core.Tests.MockObject.Factory;
using Synapse.Backgrounds.Core.Tests.TestDataFactory;
using Synapse.Backgrounds.Grains.Contract;
using Xunit;

namespace Synapse.Backgrounds.Core.Tests.Aggregates
{
    public class WaveAggregateTest : TestBase
    {
        #region Live Fixtures

        [Theory]//(Skip = "Require Live data")]
        [MemberData(nameof(WaveRequestDataSource.GetReleasedWaves), MemberType = typeof(WaveRequestDataSource))]
        public void ExecuteReleaseWave_Bat_ReleasedStatus(ICommand waveReleaseRequest, bool expectedResult)
        {
            #region Arrange

            var sut = GetInstance<IWaveAggregate>();
            bool actualResult;

            #endregion

            #region Act

            try
            {
                using (var connection = new OracleConnectionProvider(new ConfigService()).GetProfiledDbConnection("SYNTSTB"))
                {
                    if (connection != null && connection.State != ConnectionState.Open)
                        connection.Open();
                    sut.ExecuteReleaseWave(connection, waveReleaseRequest);
                }

                actualResult = true;
            }
            catch (Exception ex)
            {
                actualResult = false;
            }

            #endregion

            #region Assert

            Assert.Equal(actualResult, expectedResult);

            #endregion
        }


        [Theory(Skip = "Required Live data")]
        [MemberData(nameof(WaveRequestDataSource.GetCommittedWaveswthmultipleorders), MemberType =
            typeof(WaveRequestDataSource))]
        public void ExecuteReleaseWave_Bat_CommittedStatus(ICommand waveReleaseRequest, bool expectedResult)
        {
            #region Arrange

            var sut = GetInstance<IWaveAggregate>();
            bool actualResult;

            #endregion

            #region Act

            try
            {
                using (var connection =
                    new OracleConnectionProvider(new ConfigService()).GetProfiledDbConnection("SYNTSTB"))
                {
                    if (connection != null && connection.State != ConnectionState.Open)
                        connection.Open();
                    sut.ExecuteReleaseWave(connection, waveReleaseRequest);
                }

                actualResult = true;
            }
            catch (Exception ex)
            {
                actualResult = false;
            }

            #endregion

            #region Assert

            Assert.Equal(actualResult, expectedResult);

            #endregion
        }

        [Theory]//(Skip = "Required Live data")]
        [MemberData(nameof(WaveRequestDataSource.GetReleasedWaves), MemberType = typeof(WaveRequestDataSource))]
        public void ExecuteUneleaseWave_Bat_ReleasedStatus(ICommand waveReleaseRequest, bool expectedResult)
        {
            #region Arrange

            var sut = GetInstance<IWaveAggregate>();
            bool actualResult;

            #endregion

            #region Act

            try
            {
                using (var connection =
                    new OracleConnectionProvider(new ConfigService()).GetProfiledDbConnection("SYNTSTB"))
                {
                    if (connection != null && connection.State != ConnectionState.Open)
                        connection.Open();
                    sut.ExecuteUnreleaseWave(connection, waveReleaseRequest);
                }

                actualResult = true;
            }
            catch (Exception ex)
            {
                actualResult = false;
            }

            #endregion

            #region Assert

            Assert.Equal(actualResult, expectedResult);

            #endregion
        }

        [Theory(Skip = "Require Live data")]
        [MemberData(nameof(WaveRequestDataSource.GetCommittedWaveswthmultipleorders), MemberType =
            typeof(WaveRequestDataSource))]
        public void ExecuteUneleaseWave_Bat_CommittedStatus(ICommand waveReleaseRequest, bool expectedResult)
        {
            #region Arrange

            var sut = GetInstance<IWaveAggregate>();
            bool actualResult;

            #endregion

            #region Act

            try
            {
                using (var connection =
                    new OracleConnectionProvider(new ConfigService()).GetProfiledDbConnection("SYNTSTB"))
                {
                    if (connection != null && connection.State != ConnectionState.Open)
                        connection.Open();
                    sut.ExecuteUnreleaseWave(connection, waveReleaseRequest);
                }

                actualResult = true;
            }
            catch (Exception ex)
            {
                actualResult = false;
            }

            #endregion

            #region Assert

            Assert.Equal(actualResult, expectedResult);

            #endregion
        }

        #endregion

        #region Mock Fixtures

        [Theory]
        [MemberData(nameof(WaveRequestDataSource.GetRandomWaves), MemberType = typeof(WaveRequestDataSource))]
        public void ExecuteReleaseWave_Bat_ReleasedStatus_mock(ICommand waveReleaseRequest,
            bool expectedResult)
        {
            #region Arrange

            bool actualResult;

            var parameters = new List<TypedParameter>
            {
                new TypedParameter(typeof(IWaveAggregateInitializer),
                    GenerateWaveAggregateInitializer(WaveDataFactory.GetCompletedWaves(), waveReleaseRequest)),
                new TypedParameter(typeof(IInfrastructureInitializer),
                    new InfrastructureInitializerBuilder().Build())
            };
            var sut = GetInstance<IWaveAggregate>(parameters);

            #endregion

            #region Act

            //Act
            try
            {
                sut.ExecuteReleaseWave(null,waveReleaseRequest);
                actualResult = true;
            }
            catch (Exception ex)
            {
                actualResult = false;
            }

            #endregion

            #region Assert

            Assert.Equal(expectedResult, actualResult);

            #endregion
        }

        [Theory]
        [MemberData(nameof(WaveRequestDataSource.GetRandomWaves), MemberType = typeof(WaveRequestDataSource))]
        public void ExecuteReleaseWave_Bat_CompletedStatus_mock(ICommand waveReleaseRequest,
            bool expectedResult)
        {
            #region Arrange

            bool actualResult;
            var parameters = new List<TypedParameter>
            {
                new TypedParameter(typeof(IWaveAggregateInitializer),
                    GenerateWaveAggregateInitializer(WaveDataFactory.GetCompletedWaves(), waveReleaseRequest)),
                new TypedParameter(typeof(IInfrastructureInitializer),
                    new InfrastructureInitializerBuilder().Build())
            };
            var sut = GetInstance<IWaveAggregate>(parameters);

            #endregion

            #region Act

            try
            {
                sut.ExecuteReleaseWave(null, waveReleaseRequest);
                actualResult = true;
            }
            catch (Exception ex)
            {
                actualResult = false;
            }

            #endregion

            #region Assert

            Assert.Equal(expectedResult, actualResult);

            #endregion
        }


        [Theory]
        [MemberData(nameof(WaveRequestDataSource.GetRandomWaves), MemberType = typeof(WaveRequestDataSource))]
        public void ExecuteReleaseWave_Bat_CommittedStatus_mock(ICommand waveReleaseRequest,
            bool expectedResult)
        {
            #region Arrange

            bool actualResult;
            var parameters = new List<TypedParameter>
            {
                new TypedParameter(typeof(IWaveAggregateInitializer),
                    GenerateWaveAggregateInitializer(WaveDataFactory.GetCompletedWaves(), waveReleaseRequest)),
                new TypedParameter(typeof(IInfrastructureInitializer),
                    new InfrastructureInitializerBuilder().Build())
            };
            var sut = GetInstance<IWaveAggregate>(parameters);

            #endregion

            #region Act

            try
            {
                sut.ExecuteReleaseWave(null,waveReleaseRequest);
                actualResult = true;
            }
            catch (Exception ex)
            {
                actualResult = false;
            }

            #endregion

            #region Assert

            Assert.Equal(expectedResult, actualResult);

            #endregion
        }

        [Theory]
        [MemberData(nameof(WaveRequestDataSource.GetRandomWaves), MemberType = typeof(WaveRequestDataSource))]
        public void ExecuteUnreleaseWave_Bat_CommittedStatus_mock(ICommand waveReleaseRequest,
            bool expectedResult)
        {
            #region Arrange

            bool actualResult;
            var parameters = new List<TypedParameter>
            {
                new TypedParameter(typeof(IWaveAggregateInitializer),
                    GenerateWaveAggregateInitializer(WaveDataFactory.GetCompletedWaves(), waveReleaseRequest)),
                new TypedParameter(typeof(IInfrastructureInitializer),
                    new InfrastructureInitializerBuilder().Build())
            };
            var sut = GetInstance<IWaveAggregate>(parameters);

            #endregion

            #region Act

            try
            {
                sut.ExecuteUnreleaseWave(null,waveReleaseRequest);
                actualResult = true;
            }
            catch (Exception ex)
            {
                actualResult = false;
            }

            #endregion

            #region Assert

            Assert.Equal(expectedResult, actualResult);

            #endregion
        }

        [Theory]
        [MemberData(nameof(WaveRequestDataSource.GetRandomWaves), MemberType = typeof(WaveRequestDataSource))]
        public void ExecuteUnreleaseWave_Bat_ReleasedStatus_mock(ICommand waveReleaseRequest,
            bool expectedResult)
        {
            #region Arrange

            bool actualResult;
            var parameters = new List<TypedParameter>
            {
                new TypedParameter(typeof(IWaveAggregateInitializer),
                    GenerateWaveAggregateInitializer(WaveDataFactory.GetCompletedWaves(), waveReleaseRequest)),
                new TypedParameter(typeof(IInfrastructureInitializer),
                    new InfrastructureInitializerBuilder().Build())
            };
            var sut = GetInstance<IWaveAggregate>(parameters);

            #endregion

            #region Act

            try
            {
                sut.ExecuteUnreleaseWave(null, waveReleaseRequest);
                actualResult = true;
            }
            catch (Exception ex)
            {
                actualResult = false;
            }

            #endregion

            #region Assert

            Assert.Equal(expectedResult, actualResult);

            #endregion
        }


        [Theory]
        [MemberData(nameof(WaveRequestDataSource.GetRandomWaves), MemberType = typeof(WaveRequestDataSource))]
        public void ExecuteUnreleaseWave_Bat_CompletedStatus_mock(ICommand waveReleaseRequest,
            bool expectedResult)
        {
            #region Arrange

            bool actualResult;
            var parameters = new List<TypedParameter>
            {
                new TypedParameter(typeof(IWaveAggregateInitializer),
                    GenerateWaveAggregateInitializer(WaveDataFactory.GetCompletedWaves(), waveReleaseRequest)),
                new TypedParameter(typeof(IInfrastructureInitializer),
                    new InfrastructureInitializerBuilder().Build())
            };
            var sut = GetInstance<IWaveAggregate>(parameters);

            #endregion

            #region Act

            try
            {
                sut.ExecuteUnreleaseWave(null, waveReleaseRequest);
                actualResult = true;
            }
            catch (Exception ex)
            {
                actualResult = false;
            }

            #endregion

            #region Assert

            Assert.Equal(expectedResult, actualResult);

            #endregion
        }

        #endregion


        #region Private Method(S)

        private WaveAggregateInitializer GenerateWaveAggregateInitializer(IEnumerable<Wave> waves, ICommand request)
        {
            return new WaveAggregateInitilizerBuilder().SetOrderAggregate(waves, request)
                .SetWaveDbContext(waves).SetValidator(waves).Build();
        }

        #endregion
    }
}