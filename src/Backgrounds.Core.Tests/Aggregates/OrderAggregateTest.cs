﻿// <copyright file="OrderAggregateTest.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Core.Tests</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-23</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-05-23</lastchangeddate>

using System.Collections.Generic;
using System.Data;
using System.Linq;
using Autofac;
using Synapse.Backgrounds.Core.Aggregates.Initializer;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Core.Tests.MockObject.Builder;
using Synapse.Backgrounds.Core.Tests.MockObject.Factory;
using Synapse.Backgrounds.Core.Tests.TestDataFactory;
using Synapse.Backgrounds.Grains.Contract;
using Xunit;

namespace Synapse.Backgrounds.Core.Tests.Aggregates
{
    public class OrderAggregateTest : TestBase
    {
        #region Live Fixtures

        [Theory] //(Skip = "Require Live data")]
        [MemberData(nameof(WaveRequestDataSource.GetCommittedWaveswthmultipleorders), MemberType =
            typeof(WaveRequestDataSource))]
        public void ExecuteReleaseOrder_Bat_CommitedStatus(ICommand waveReleaseRequest, bool expectedResult)
        {
            #region Arrange

            var sut = GetInstance<IOrderAggregate>();
            var oracleConnection = GetInstance<IOracleConnectionProvider>();
            var waveDbContext = GetInstance<IWaveDbContext>();
            bool actualResult;

            #endregion

            #region Act

            try
            {
                using (var connection = oracleConnection.GetDbConnection(waveReleaseRequest.Environment))
                {
                    if (connection != null && connection.State != ConnectionState.Open) connection.Open();

                    var lstWaves = waveDbContext.GetWaveById(connection, waveReleaseRequest.Wave).ToList();

                    sut.ExecuteReleaseOrder(connection, lstWaves.FirstOrDefault(), waveReleaseRequest);
                }

                actualResult = true;
            }
            catch
            {
                actualResult = false;
            }

            #endregion

            #region Assert

            Assert.Equal(actualResult, expectedResult);

            #endregion Assert
        }
        
        #endregion


        #region Mock Fixtures

        [Theory]
        [MemberData(nameof(WaveRequestDataSource.GetRandomWaves), MemberType = typeof(WaveRequestDataSource))]
        public void ExecuteReleaseOrder_Bat_CommitedStatus_Mock(ICommand waveReleaseRequest,
            bool expectedResult)
        {
            //Arrange
            var parameters = new List<TypedParameter>
            {
                new TypedParameter(typeof(IOrderAggregateInitializer),
                    GenerateOrderAggregateInitializer(OrderDataFactory.GetCommitedOrders(), OrderDataFactory.GetCommitedOrderDetail())),
                new TypedParameter(typeof(IInfrastructureInitializer),
                    new InfrastructureInitializerBuilder().Build())
            };
            var sut = GetInstance<IOrderAggregate>(parameters);
            var oracleConnection = GetInstance<IOracleConnectionProvider>();
            bool actualResult;
            //Act
            try
            {
                using (var connection = oracleConnection.GetDbConnection(waveReleaseRequest.Environment))
                {
                    if (connection != null && connection.State != ConnectionState.Open) connection.Open();

                    var lstWaves = WaveDataFactory.GetCompletedWaves().ToList();
                    sut.ExecuteReleaseOrder(connection, lstWaves.FirstOrDefault(), waveReleaseRequest);
                }

                actualResult = true;
            }
            catch
            {
                actualResult = false;
            }

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Theory]
        [MemberData(nameof(WaveRequestDataSource.GetRandomWaves), MemberType = typeof(WaveRequestDataSource))]
        public void HandleUnrelease_Bat_CommitedStatus_Mock(ICommand waveReleaseRequest, bool expectedResult)
        {
            //Arrange
            var parameters = new List<TypedParameter>
            {
                new TypedParameter(typeof(IOrderAggregateInitializer),
                    GenerateOrderAggregateInitializer(OrderDataFactory.GetCommitedOrders(), OrderDataFactory.GetCommitedOrderDetail())),
                new TypedParameter(typeof(IInfrastructureInitializer),
                    new InfrastructureInitializerBuilder().Build())
            };
            var sut = GetInstance<IOrderAggregate>(parameters);
            var oracleConnection = GetInstance<IOracleConnectionProvider>();
            bool actualResult;
            //Act
            try
            {
                using (var connection = oracleConnection.GetDbConnection(waveReleaseRequest.Environment))
                {
                    if (connection != null && connection.State != ConnectionState.Open) connection.Open();

                    var lstWaves = WaveDataFactory.GetCompletedWaves().ToList();
                    sut.ExecuteUnreleaseOrder(connection, lstWaves.FirstOrDefault(), waveReleaseRequest);
                }

                actualResult = true;
            }
            catch 
            {
                actualResult = false;
            }

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }
        
        #endregion


        #region Private Method(S)

        private OrderAggregateInitializer GenerateOrderAggregateInitializer(IEnumerable<OrderHeader> orders,
            IEnumerable<OrderDetail> orderDetails)
        {
            return new OrderAggregateInitializerBuilder()
                .SetOrderDbContext(orders.ToList(), orderDetails.ToList())
                .SetItemAggregate()
                .SetBatchTaskAggregate()
                .SetFelAggregate()
                .SetWaveAggregate()
                .SetSubTaskAggregate()
                .SetOrderStatusAggregate()
                .SetCarrierStageLocDbContext()
                .SetValidator()
                .Build();
        }

        #endregion
    }
}