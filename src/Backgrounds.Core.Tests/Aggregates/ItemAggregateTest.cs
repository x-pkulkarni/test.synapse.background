﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Moq;
using Newtonsoft.Json.Linq;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Core.Tests.MockObject.Builder;
using Synapse.Backgrounds.Core.Tests.MockObject.Factory;
using Synapse.Backgrounds.Core.Tests.TestDataFactory;
using Synapse.Backgrounds.Grains.Contract;
using Xunit;

namespace Synapse.Backgrounds.Core.Tests.Aggregates
{
    public class ItemAggregateTest : TestBase
    {
        #region Live Fixtures

        #endregion


        #region Mock Fixtures

        [Theory]
        [MemberData(nameof(WaveRequestDataSource.GetCommittedWaveswthmultipleorders), MemberType =
            typeof(WaveRequestDataSource))]
        public void ExecuteReleasLine_Bat_CommitedStatus_Mock(ICommand waveReleaseRequest,
            bool expectedResult)
        {
            //Arrange
            var parameters = new List<TypedParameter>
            {
                new TypedParameter(typeof(IItemAggregateInitializer),
                    GenerateItemAggregateInitializer(OrderDataFactory.GetCommitedOrderDetail()))
            };
            var sut = GetInstance<IItemAggregate>(parameters);
            var oracleConnection = GetInstance<IOracleConnectionProvider>();
            bool actualResult;
            //Act
            try
            {
                using (var connection = oracleConnection.GetDbConnection(waveReleaseRequest.Environment))
                {
                    if (connection != null && connection.State != ConnectionState.Open) connection.Open();
                    sut.ExecuteReleasLine(connection, OrderDataFactory.GetCommitedOrderTo().ToList().First(), waveReleaseRequest);
                }

                actualResult = true;
            }
            catch (Exception ex)
            {
                actualResult = false;
            }

            //Assert
            Assert.Equal(actualResult, expectedResult);
        }

        #endregion


        #region Private Method(S)

        private IItemAggregateInitializer GenerateItemAggregateInitializer(IEnumerable<OrderDetail> orderDetails)
        {
            return new ItemAggregateInitializerBuilder()
                .SetOrderDbContext(orderDetails)
                .SetCommitmentDbContext(CommitmentDataFactory.GetRandomCommitment())
                .SetCustItemDbContext()
                .SetCustomerDbContext(CustomerDataFactory.GetRandomCustomer())
                .SetItemDemandDbContext(2)
                .SetFindAPickAggregate()
                .Build();
        }

        #endregion
    }
}
