﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Tests.MockObject.Builder;
using Synapse.Backgrounds.Core.Tests.MockObject.Factory;
using Synapse.Backgrounds.Core.Tests.TestDataFactory;
using Synapse.Backgrounds.Grains.Contract;
using Xunit;
using Moq;
using Newtonsoft.Json.Linq;

namespace Synapse.Backgrounds.Core.Tests.Aggregates
{
    public class FindAPickAggregateTest : TestBase
    {
        #region Mock Fixtures

        [Theory]
        [MemberData(nameof(WaveRequestDataSource.GetCommittedWaveswthmultipleorders), MemberType =
            typeof(WaveRequestDataSource))]
        public void ExecuteFullPickTest(ICommand waveReleaseRequest,
            bool expectedResult)
        {
            //Arrange
            var parameters = new List<TypedParameter>
            {
                new TypedParameter(typeof(IFindAPickAggregateInitializer),
                    GenerateFindAPickAggregateInitializer())
            };
            var sut = GetInstance<IFindAPickAggregate>(parameters);
            var oracleConnection = GetInstance<IOracleConnectionProvider>();
            bool actualResult;
            //Act
            try
            {
                using (var connection = oracleConnection.GetDbConnection(waveReleaseRequest.Environment))
                {
                    if (connection != null && connection.State != ConnectionState.Open) connection.Open();
                    sut.ExecuteFullPick(connection, PickTaskRequestDataFactory.GetRandomPickTaskRequest(waveReleaseRequest).FirstOrDefault());
                }

                actualResult = true;
            }
            catch (Exception ex)
            {
                actualResult = false;
            }

            //Assert
            Assert.Equal(actualResult, expectedResult);
        }

        [Theory]
        [MemberData(nameof(WaveRequestDataSource.GetCommittedWaveswthmultipleorders), MemberType =
            typeof(WaveRequestDataSource))]
        public void ExecuteBacthPickTest(ICommand waveReleaseRequest,
            bool expectedResult)
        {
            //Arrange
            var parameters = new List<TypedParameter>
            {
                new TypedParameter(typeof(IFindAPickAggregateInitializer),
                    GenerateFindAPickAggregateInitializer())
            };
            var sut = GetInstance<IFindAPickAggregate>(parameters);
            var oracleConnection = GetInstance<IOracleConnectionProvider>();
            bool actualResult;
            //Act
            try
            {
                using (var connection = oracleConnection.GetDbConnection(waveReleaseRequest.Environment))
                {
                    if (connection != null && connection.State != ConnectionState.Open) connection.Open();
                    sut.ExecuteBatchPick(connection, PickTaskRequestDataFactory.GetRandomPickTaskRequest(waveReleaseRequest).FirstOrDefault());
                }

                actualResult = true;
            }
            catch (Exception ex)
            {
                actualResult = false;
            }

            //Assert
            Assert.Equal(actualResult, expectedResult);
        }
        #endregion

        private IFindAPickAggregateInitializer GenerateFindAPickAggregateInitializer()
        {
            return new FindAPickAggregateInitializerBuilder()
                .SetBatchTaskAggregate()
                .SetCatchWeightAggregate()
                .SetCustItemAggregate()
                .SetLocationDbContext(LocationDataFactory.GetRandomLocation())
                .SetPickDbContext()
                .SetPlateAggregate(PlateDataFactory.GetRandomPlates())
                .SetShippingPlateDbContext()
                .SetSubTaskAggregate()
                .SetTaskDomain()
                .Build();
        }
    }
}
