﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Backgrounds.Grains.Contract.Command;

namespace Synapse.Backgrounds.Core.Tests.TestDataFactory
{
    public static class WaveRequestDataSource
    {
        public static IEnumerable<object[]> GetRandomWaves()
        {
            yield return new object[]
            {
                new ReleaseWaveCommand
                {
                    PickType = "BAT",
                    UserId = "SGOPINATH",
                    Wave = 100001,
                    LotNumber = "(none)",
                    Facility = "RHR",
                    Environment = "SYNTSTBConn"
                },
                true
            };
        }

        public static IEnumerable<object[]> GetReleasedWaves()
        {
            yield return new object[]
            {
                new ReleaseWaveCommand()
                {
                    PickType = "BAT",
                    UserId = "SGOPINATH",
                    Wave = 415416,
                    OrderId=19405462,
                    Item= "BL2100S",
                    TaskPriority="3",
                    ShipId=1,
                    LotNumber = null,
                    Facility = "RHR",
                    Environment = "SYNTSTBConn"
                },
                true
            };
        }


        public static IEnumerable<object[]> GetNoInventoryWaves()
        {
            yield return new object[]
            {
                new ReleaseWaveCommand
                {
                    PickType = "BAT",
                    UserId = "SGOPINATH",
                    Wave = 415004,
                    LotNumber = "(none)",
                    TaskPriority = "3",
                    Facility = "RHR",
                    Environment = "SYNTSTBConn"
                },
                true
            };
        }

        public static IEnumerable<object[]> GetCommittedWaveswthmultipleorders()
        {
            yield return new object[]
            {
                new ReleaseWaveCommand
                {
                    PickType = "BAT",
                    UserId = "SGOPINATH",
                    Wave = 415416,
                    LotNumber = "(none)",
                    TaskPriority = "3",
                    Facility = "RHR",
                    Environment = "SYNTSTBConn"
                },
                true
            };
        }

        public static IEnumerable<object[]> GetCommittedWaveWithCases()
        {
            yield return new object[]
            {
                new ReleaseWaveCommand
                {
                    PickType = "BAT",
                    UserId = "SGOPINATH",
                    Wave = 415352,
                    LotNumber = "(none)",
                    TaskPriority = "3",
                    Facility = "RHR",
                    Environment = "SYNTSTBConn"
                },
                true
            };
        }
    }
}
