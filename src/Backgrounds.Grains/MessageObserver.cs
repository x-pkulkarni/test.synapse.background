﻿// <copyright file="MessageObserver.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Grains</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System;
using System.CodeDom;
using System.Reflection;
using System.Threading.Tasks;
using Orleans.Streams;
using StackExchange.Profiling;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;
using Synapse.Backgrounds.Infrastructure.Profiler.Interface;

namespace Synapse.Backgrounds.Grains
{
    public class MessageObserver : IMessageObserver
    {
        private CommandDispatcher CommandDispatcher { get; }
        private readonly ILoggerService _loggerService;
        private readonly IProfileService _profileService;

        public MessageObserver(CommandDispatcher dispatcher, ILoggerService loggerService,
            IProfileService profileService)
        {
            CommandDispatcher = dispatcher;
            _loggerService = loggerService;
            _profileService = profileService;
        }

        public  Task OnNextAsync(ICommand item, StreamSequenceToken token = null)
        {
            MiniProfiler profiler = null;
            try
            {
                _loggerService.Information($"Command execution started: #{item.Id} of type {item.CommandText}, facility {item.Facility} and wave #{item.Wave}");
                profiler = _profileService.Start($"Command Execute: #{item.Id} of type {item.CommandText}");
                CommandDispatcher.ExecuteAsync(null,item);
            }
            catch (AggregateException ae)
            {
                foreach (var ex in ae.Flatten().InnerExceptions)
                {
                    _loggerService.Error($"{item.CommandText} Failed: {ex.Message}");
                }
            }
            catch (Exception ex)
            {
                _loggerService.Error(
                    $"{item.CommandText} Failed: {ex.Message}, Inner exception: [ StackTrace: {ex.InnerException?.StackTrace ?? string.Empty}, Message: {ex.InnerException?.Message ?? string.Empty}  ]");
            }
            finally
            {
                _loggerService.Information($"Command execution completed: #{item.Id} of type {item.CommandText} and wave #{item.Wave}");
                _profileService.Stop(profiler);
            }
            
            return Task.CompletedTask;
        }

        public Task OnCompletedAsync() => Task.CompletedTask;

        public Task OnErrorAsync(Exception ex)
        {
            _loggerService.Error(ex.StackTrace);
            return Task.CompletedTask;
        }
    }
}