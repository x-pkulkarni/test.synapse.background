﻿// <copyright file="CommandBus.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Grains</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-05-08</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-07-30</lastchangeddate>

using System;
using System.Data;
using System.Threading.Tasks;
using System.Transactions;
using Orleans;
using Orleans.Concurrency;
using Orleans.Placement;
using StackExchange.Profiling;
using Synapse.Backgrounds.Configuration;
using Synapse.Backgrounds.Core.Aggregates.Interface;
using Synapse.Backgrounds.Core.Data.Interface;
using Synapse.Backgrounds.Core.Model;
using Synapse.Backgrounds.Grains.Contract;
using Synapse.Backgrounds.Infrastructure.Logging.Interface;
using Synapse.Backgrounds.Infrastructure.Profiler.Interface;
using Synapse.Backgrounds.Integration;
using Task = System.Threading.Tasks.Task;

namespace Synapse.Backgrounds.Grains
{
    using Contract.Command;

    [Reentrant]
    [ActivationCountBasedPlacement]
    public class CommandBus : Grain, ICommandBus
    {
        private readonly CommandDispatcher _commandDispatcher;
        private readonly Lazy<ILoggerService> _loggerService;
        private readonly Lazy<IConfigService> _configService;
        private readonly IProfileService _profileService;
        private readonly IAppLockService _appLockService;
        private readonly Lazy<IOracleConnectionProvider> _oracleConnection;
        private readonly TransactionOptions _transactionScopeOption;

        public CommandBus(CommandDispatcher dispatcher, IInfrastructureInitializer infrastructureInitializer,
            IProfileService profileService, IAppLockService appLockService)
        {
            _commandDispatcher = dispatcher;
            _loggerService = infrastructureInitializer.LoggerService;
            _configService = infrastructureInitializer.ConfigService;
            _oracleConnection = infrastructureInitializer.OracleConnection;
            _profileService = profileService;
            _appLockService = appLockService;
            _transactionScopeOption = new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted };
            _loggerService.Value.Debug("CommandBus is initialized!");
        }

        public async Task Invoke(ICommand item)
        {
            var appLockCheckFlag = false;
            _loggerService.Value.Debug(this.GrainReference.GetPrimaryKeyString());
            MiniProfiler profiler = null;
            var isLockReleased = false;
            AppLockRequest appLockRequest = null;
            try
            {
                profiler = _profileService.Start($"Command Execute: #{item.Id} of type {item.CommandText}");
                appLockRequest = new AppLockRequest
                {
                    CustomerId = item.CustomerId,
                    Facility = item.Facility,
                    OrderId = item.OrderId,
                    UserId = item.UserId,
                    LockId = $"WAVEPLAN{item.Facility}~{item.CustomerId}"
                };
                var t1 = Task.Run(async () =>
                {
                    try
                    {
                        using (var scope = new TransactionScope(TransactionScopeOption.Required, _transactionScopeOption))
                        {
                            using (var connection = _oracleConnection.Value.GetProfiledDbConnection(item.Environment))
                            {
                                if (connection != null && connection.State != ConnectionState.Open)
                                    connection.Open();

                                _loggerService.Value.Debug(
                                    $"Command execution started: #{item.Id} of type {item.CommandText}, facility {item.Facility} and wave #{item.Wave}");

                                await _commandDispatcher.ExecuteAsync(connection, item);
                                appLockCheckFlag = _configService.Value.GetAppSetting("EnableAppLock")
                                    .Equals("Y", StringComparison.CurrentCultureIgnoreCase);
                                if (appLockCheckFlag && _appLockService.ReleaseAppLock(connection, appLockRequest))
                                    isLockReleased = true;
                                scope.Complete();
                            }
                        }
                    }
                    catch
                    {
                        using (var connection = _oracleConnection.Value.GetProfiledDbConnection(item.Environment))
                        {
                            if (item.Type.Equals(Enums.RequestType.RELWAV))
                            {
                                var resetitem = new ResetWaveCommand { Wave = item.Wave };
                                await _commandDispatcher.ExecuteAsync(connection, resetitem);
                            }

                            throw;
                        }
                    }
                });
                t1.Wait();

            }
            catch (AggregateException ae)
            {
                var context = new ServiceLogContext
                {
                    Facility = item.Facility,
                    CustomerId = item.CustomerId,
                    SessionId = item.SessionId,
                    Environment = item.Environment
                };
                foreach (var ex in ae.Flatten().InnerExceptions)
                    _loggerService.Value.Error(ex, context, item, "Background service: Command Bus grain exception!");
            }
            catch (Exception ex)
            {
                _loggerService.Value.Error(ex, new ServiceLogContext
                {
                    Facility = item.Facility,
                    CustomerId = item.CustomerId,
                    SessionId = item.SessionId,
                    Environment = item.Environment
                },
                    item, "Background service: Command Bus grain exception!");
            }
            finally
            {
                if (appLockCheckFlag && !isLockReleased)
                {
                    using (var connection = _oracleConnection.Value.GetProfiledDbConnection(item.Environment))
                    {
                        if (connection != null && connection.State != ConnectionState.Open)
                            connection.Open();
                        _appLockService.ReleaseAppLock(connection, appLockRequest);
                    }
                }

                _loggerService.Value.Debug(
                    $"Command execution completed: #{item.Id} of type {item.CommandText} and wave #{item.Wave}");
                await _profileService.Stop(profiler);
            }
        }
    }
}