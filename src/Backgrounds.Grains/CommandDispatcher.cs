﻿// <copyright file="CommandDispatcher.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Grains</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using System.Data;
using System.Threading.Tasks;
using Synapse.Backgrounds.Core.Factories.Interface;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Grains
{
    public class CommandDispatcher
    {
        private ICommandHandlerExecutor CommandHandlerExecutor { get; }

        public CommandDispatcher(ICommandHandlerExecutor commandHandlerExecutor)
        {
            CommandHandlerExecutor = commandHandlerExecutor;
        }

        public  Task ExecuteAsync(IDbConnection connection, ICommand command)
        {
            return CommandHandlerExecutor.ExecuteAsync(connection, command);
        }
    }
}