﻿// <copyright file="GrainsDependencyModule.cs" company="GEODIS">
// Copyright (c) 2018 GEODIS.
// All rights reserved. www.geodis.com
// Reproduction or transmission in whole or in part, in any form or by 
// any means, electronic, mechanical or otherwise, is prohibited without the 
// prior written consent of the copyright owner.
// </copyright>
//  
// <application>Synapse.Backgrounds</application>
// <module>Backgrounds.Grains</module>
// <author>Gopinath, Srinath</author> 
// <createddate>2018-04-17</createddate>
// <lastchangedby>Gopinath, Srinath</lastchangedby>
// <lastchangeddate>2018-04-25</lastchangeddate>

using Autofac;
using Synapse.Backgrounds.Core.DependencyResolver;
using Synapse.Backgrounds.Grains.Contract;

namespace Synapse.Backgrounds.Grains.DependencyResolver
{
    public class GrainsDependencyModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<CoreDependencyModule>();
            builder.RegisterType<MessageObserver>().As<IMessageObserver>();
            builder.RegisterType<CommandDispatcher>();
        }
    }
}