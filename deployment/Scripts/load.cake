#load "./definitions.cake"  // Class definitions
#load "./variables.cake"    // Variable declaration & initialization
#load "./setup.cake"        // Collect data from user & does initialization before first task run
#load "./util.cake"         // Misc C# functions
#load "./gitutil.cake"      // Git related C# functions
#load "./sqlutil.cake"      // SQL related C# functions
#load "./common.cake"       // Common reusable tasks
#load "./test.cake"         // Unit test tasks
#load "./publish.cake"      // Task sequence mapping
#load "./targets.cake"      // Task sequence mapping
#load "./qa.cake"           // QA envirnoment specific tasks
#load "./uat.cake"          // UAT envirnoment specific tasks
#load "./uat-hotfix.cake"   // Hotfix to UAT envirnoment specific tasks
#load "./prod-hotfix.cake"  // Hotfix envirnoment specific tasks
#load "./prod.cake"         // Production specific tasks 