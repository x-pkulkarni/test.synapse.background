//********************* QA PRE-DEPLOYMENT PROCESSS *************************
#region QA PRE-DEPLOYMENT
    
Task("Git-Pull-Qa")
	.Does(() => 
	{
  		using (var repo = new Repository(localRepo))
		{
			var head = repo.Branches.Single (branch => branch.FriendlyName == branchName);
			var checkoutOptions = new CheckoutOptions ();
			checkoutOptions.CheckoutModifiers = CheckoutModifiers.Force;
			repo.Checkout(head, checkoutOptions);
				
			LibGit2Sharp.PullOptions options = new LibGit2Sharp.PullOptions();
			options.FetchOptions = new FetchOptions();
			options.FetchOptions.CredentialsProvider = new CredentialsHandler(
					(url, usernameFromUrl, types) =>
						new UsernamePasswordCredentials()
						{
							Username = gitUser,
							Password = gitPassword
						});
									
			repo.Network.Pull(new LibGit2Sharp.Signature(gitUser, gitUser, new DateTimeOffset(DateTime.Now)), options);
		}			 
		FileAppendText(logFile, String.Format("############## Git Pull Completed ##############{0}",Environment.NewLine));
   		ConsoleWriteInfo(String.Format("############## Git Pull Completed ##############{0}",Environment.NewLine)); 
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}GIT-PULL ERROR :{0}{1}{0}",Environment.NewLine, exception.Dump()));
		ConsoleWriteError(String.Format("{0}GIT-PULL ERROR :{0}{1}{0}",Environment.NewLine, exception.Dump()));
		throw exception;
	});	

Task("Git-Qa-Process")
	.Does(() => 
	{
		List<string> listBranches = new List<string>();
							
		using (var repo = new Repository(localRepo))
		{
			Remote remote = repo.Network.Remotes["origin"];	
															
			LibGit2Sharp.PushOptions pushOptions = new LibGit2Sharp.PushOptions();
					pushOptions.CredentialsProvider = new CredentialsHandler(
					(url, usernameFromUrl, types) =>
						new UsernamePasswordCredentials()
						{
							Username = gitUser,
							Password = gitPassword
						});
			var branch = repo.Branches[branchName];
			Branch currentBranch = Commands.Checkout(repo , branch);					
	
			repo.Branches.Update(branch,
					 b => b.Remote = remote.Name,
					 b => b.UpstreamBranch = branch.CanonicalName);
			
			repo.Network.Push(branch, pushOptions);
				
			//fetch files from git
			FetchOptions fetchoptions = new FetchOptions();
			fetchoptions.CredentialsProvider = new CredentialsHandler((furl, usernameFromUrl, types) => 
				new UsernamePasswordCredentials() 
				{
					Username = gitUser,
					Password = gitPassword
				});
					
			foreach (Remote fremote in repo.Network.Remotes)
			{
				IEnumerable<string> refSpecs = fremote.FetchRefSpecs.Select(x => x.Specification);
				Commands.Fetch(repo, fremote.Name, refSpecs, fetchoptions, "");
			}
				
			//increment the sub version - version needs to be updated in config and assembly file. 
			subVersion+=1;
			
			if(IncludeSqlTasks)
			{
				//generate sql script file with git diff results 
				GenerateSQLMigrationScriptsFromGitDiffResults();
			}
				
			version = String.Format("v{0}-qa.{1}",assemblyVersion,subVersion ); 
								
			//commit assembly file and push it 
			CreateAssemblyInfoFile();
			
			var readMeFilePath = System.IO.Path.Combine(localRepo,String.Format("ReadMe_{0}.md",version));
				
			//create a readme markdown file with commit messages from Head to previous tag
			GitCommitMessagesFromHeadToTag(readMeFilePath); 
				
							
			//update version in config file
			string json = System.IO.File.ReadAllText(configFilePath);
			dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
				
			jsonObj["version"]= version;
					
			string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj,Newtonsoft.Json.Formatting.Indented);
			System.IO.File.WriteAllText(configFilePath,output);				
				
			//stage the changes
			Commands.Stage(repo, "*");
								
			// Create the committer's signature and commit
			Signature author = new Signature(gitUser, String.Format("@{0}",gitUser), DateTime.Now);
			Signature committer = author;
                
          	if (String.IsNullOrEmpty(commitMessage.Trim())) 
			{
				commitMessage = "QA Automated Build";
			}
				
			// Commit to the repository
			repo.Commit(commitMessage, author, committer);

			//Push Changes to remote
			repo.Network.Push(branch, pushOptions);
				
			Tag t = repo.ApplyTag(version);		
								
			//push tag
			repo.Network.Push(repo.Network.Remotes["origin"],String.Format("+refs/tags/{0}",version), pushOptions);
										
		}
				
		FileAppendText(logFile, String.Format("{0}############## Git Tag/Stage/Commit/Push Completed. ##############{0}",Environment.NewLine));	
		ConsoleWriteInfo(String.Format("{0}############## Git Tag/Stage/Commit/Push Completed. ##############{0}",Environment.NewLine)); 
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}Git-Qa-Process Error : {1}{0}",Environment.NewLine, exception.Dump()));
		ConsoleWriteError(String.Format("{0}Git-Qa-Process Error : {1}{0}",Environment.NewLine, exception.Dump()));
		throw exception;
	});	
		       
#endregion