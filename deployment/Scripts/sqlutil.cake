public void GenerateSQLMigrationScriptsFromGitDiffResults()
{
	try
		{
			Console.WriteLine("");
			Console.WriteLine("Generating SQL scripts file....");
			Console.WriteLine("");
			
   		    string scritpsFileName = String.Format("V{0}.{1}.{2}__{3}_for_{0}.{1}.{2}.sql",assemblyVersion,environmentVersion,subVersion,buildMode);
			 			
			List<string> longerNameNotAllowedSqlTypes = new List<string>() ; 
			string scritpsFileNamePath ="";
		
			var currentCommit = GitLogTip(localRepo);
			var olderCommit = GitLogLookup(localRepo,tagName);
																		
			var diff = GitDiff(localRepo, olderCommit.Sha, currentCommit.Sha);
			
			List<GitDiffFile> allResults = new List<GitDiffFile>();
			List<GitDiffFile> OrderedResults = new List<GitDiffFile>();
		
			foreach (GitDiffFile itm in diff) { allResults.Add(itm); }
			
			// Order scripts by the number in file name - ALETR_TABLE_001.sql
			OrderedResults = allResults.Where(t => System.IO.Path.GetExtension(t.Path).ToUpper() == ".SQL" && System.IO.Path.GetFileName(t.Path).Contains("_"))
			                           .OrderBy(l => System.IO.Path.GetFileNameWithoutExtension(l.Path).Substring(System.IO.Path.GetFileNameWithoutExtension(l.Path).LastIndexOf("_")))
									   .ToList();
									   
			foreach(GitDiffFile itm in allResults.Where(t => System.IO.Path.GetExtension(t.Path).ToUpper() == ".SQL" 
															 && !System.IO.Path.GetFileName(t.Path).Contains("_"))) 
			{ 
			  OrderedResults.Add(itm); 
			}					   
			
			foreach(var typ in sqlTypesWithLongerFileName ) 
			{ 
				foreach(var sch in sqlSchemas )
				{
					longerNameNotAllowedSqlTypes.Add(String.Format(@"SQL\{0}\{1}",sch.ToUpper(),typ.ToUpper())); 
			    }
			}
			
			foreach(var sqlSchema in sqlSchemas )
			{
				var scriptsTotalCount=0;
				
				scritpsFileNamePath = System.IO.Path.Combine(String.Format(@"{0}\sql\{1}\{2}\{3}\{4}",localRepo,sqlSchema,sqlReleaseManagementDir,buildMode,scritpsFileName));
				
				FileWriteText(scritpsFileNamePath,String.Format("{1}/* Sql Scripts - {0} */{1}",DateTime.Now.ToString(),Environment.NewLine));
				
				StringBuilder sbSqlTotalCounts = new StringBuilder();
				
				sbSqlTotalCounts.Append(String.Format("--******************************************{0}",Environment.NewLine));
				
				foreach(string scriptType in sqlScriptTypes)
				{
					int ScriptTypeCount = 0;
				
					FileAppendText(scritpsFileNamePath,String.Format(@"{1}{1}--###############################{1}--##  {0}{1}--###############################{1}{1}",scriptType,Environment.NewLine));	

					foreach(GitDiffFile file in OrderedResults.Where(t => t.Path.ToUpper().Contains(String.Format(@"\{0}\{1}",sqlSchema.ToUpper(),scriptType.ToUpper()))))
					{
						string fileNameWithOutExtn = System.IO.Path.GetFileNameWithoutExtension(file.Path);
						
						if( longerNameNotAllowedSqlTypes.Any(s => file.Path.ToUpper().Contains(s))
							&& fileNameWithOutExtn.Length > sqlFileNameLengthLimit 
							&& file.Path.ToUpper().Contains(String.Format(@"SQL\{0}\{1}",sqlSchema.ToUpper(),scriptType.ToUpper())))
						{		
							FileAppendText(logFile, String.Format("{0}######### SQL File With Name More Than 30 Chars: {1}{0}",Environment.NewLine,file.Path));				   
							Console.ForegroundColor = ConsoleColor.Magenta;
							Console.WriteLine(String.Format("{0}######### SQL File With Name More Than 30 Chars: {1}{0}",Environment.NewLine,file.Path));
							Console.ResetColor();
							
							continue;
						}
						
						if ((file.Status == GitChangeKind.Added 
						     || file.Status == GitChangeKind.Copied 
							 || file.Status == GitChangeKind.Modified 
							 || file.Status == GitChangeKind.Renamed) 
							&& file.Path.ToUpper().Contains(String.Format(@"SQL\{0}\{1}",sqlSchema.ToUpper(),scriptType.ToUpper())))  
						{
							ScriptTypeCount+=1 ;	
															   
							string text = System.IO.File.ReadAllText(System.IO.Path.Combine(localRepo,file.Path));	
													   
							if (scriptType.ToUpper()=="TABLES" || scriptType.ToUpper()=="DATA")
							{
								if( text.Trim()!= "" 
								    && scriptType.ToUpper() == "DATA" 
									&& file.Path.Contains(scriptType) 
									&& System.IO.Path.GetFileNameWithoutExtension(file.Path).ToUpper().StartsWith(SqlDataScrtiptExceptionType.ToUpper()))
								{
									scriptsTotalCount+=1;
																	
									FileAppendText(scritpsFileNamePath,
									String.Format(@"{5}--*****************************************************{5}-- {6}{5}-- @version {4}{5}-- @File {2}/{3}{5}--*****************************************************{5}{5}{0}{5}{1}{5}",
									text,
									"/",
									localRepo,
									file.Path,
									assemblyVersion,
									Environment.NewLine,
									scriptType));		
								}
								else if(text.Trim()!="")
								{	
									scriptsTotalCount+=1;
									
									FileAppendText(scritpsFileNamePath,
									String.Format(@"{5}--*****************************************************{5}-- {6}{5}-- @version {4}{5}-- @File {2}/{3}{5}--*****************************************************{5}{5}{0}{5}{1}{5}",
									text,
									" ",
									localRepo,
									file.Path,
									assemblyVersion,
									Environment.NewLine,
									scriptType));							
								}
							}
							else
							{
								if (text.Trim()!="")
								{                        
								scriptsTotalCount+=1;
									
								FileAppendText(scritpsFileNamePath,
								String.Format(@"{5}--*****************************************************{5}-- {6}{5}-- @version {4}{5}-- @File {2}/{3}{5}--*****************************************************{5}{5}{0}{5}{1}{5}",
								text,
								"/",
								localRepo,
								file.Path,
								assemblyVersion,
								Environment.NewLine,
								scriptType));							
								}
							}
						}
					}
					
					sbSqlTotalCounts.Append(String.Format("--****  Total {0} Count : {1}{2}", scriptType, ScriptTypeCount, Environment.NewLine));
				}
								
				sbSqlTotalCounts.Append(String.Format("--******************************************{0}",Environment.NewLine));
									
				FileAppendText(scritpsFileNamePath , sbSqlTotalCounts.ToString());	
				
				if (scriptsTotalCount == 0) { System.IO.File.Delete(scritpsFileNamePath); }
				
			}
			
			FileAppendText(logFile, String.Format("{0}############### Generated SQL Migration Files ###############{0}",Environment.NewLine));
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine(String.Format("{0}############### Generated SQL Migration Files  ###############{0}",Environment.NewLine));
            Console.ResetColor();
   }
  catch(Exception ex)
  {
		FileAppendText(logFile, String.Format("{0}Generate SQL Scripts From GitDiff Results Error : {1} {0}",Environment.NewLine, new CakeException()));
		Console.ForegroundColor = ConsoleColor.Red;
		Console.WriteLine(String.Format("{0}Generate SQL Scripts From GitDiff Results Error :{0}{1}{0}",Environment.NewLine, new CakeException()));
		Console.ResetColor();
		throw ex;
  }
}  