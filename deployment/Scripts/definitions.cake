  public class ConfigDetails
  {
		public string buildDefinitionName { get; set; }
		public string solution { get; set; }
		public string gitRepoURL { get; set; }
		public string gitUser { get; set; }
		public string gitPassword { get; set; }
		public string developmentBranchName { get; set; }
		public string localRepo { get; set; }
		public string logFile { get; set; }		
		public bool EmailEnabledForBuild  {get; set;}
		public string EmailRecipientsForBuildEvent  {get; set;}
		public bool EmailEnabledForPublish  {get; set;}
		public string EmailRecipientsForPublishEvent  {get; set;}
		public string stagingDir { get; set; }
		public string workingDirectory { get; set;}
		public string localDeployDir { get; set; }
		public string assemblyInfoFile { get; set; }
		public string version { get; set; }
		public string company { get; set; }
		public string product { get; set; }
		public int visualStudioToolVersion { get; set; }
		public string sourceTestDir { get; set; }
		public string sourceTestFile { get; set; }
		public string readMeFile { get; set; }
		public string[] sqlScriptTypes { get; set; }
		public string IncludeSqlTasks { get; set; }
		public string IncludeTestTasks { get; set; }
		public string SqlDataScrtiptExceptionType { get; set; }
		public string gitProtectBranchApiUrl { get; set; }
		public string gitUnProtectBranchApiUrl { get; set; } 
		public string gitProtectBranchApiToken { get; set; }
		public string remoteServerPrePublishDir { get; set; }
		public string flywayToolVersion {get; set;}
		public string zipUtility {get; set;}
		public string psUtility {get; set;}
		public string[] Schema {get; set;}
		public string[] SqlTypeWithLongerFileName {get; set;}
		public int SqlFileNameLengthLimit {get; set;}
		public string SqlReleaseManagementDir {get; set;}
		public List<appToBeDeployed> appsToBeDeployed {get; set;}
		public Environments Environments { get; set ; }
		public string gitApiUrl {get;set;}
		public string vaultApiUrl {get;set;}
		public string vaultRoleId {get;set;}
		public string vaultSecretId {get;set;}
		public string vaultGitCredentialsKVPath {get;set;}
		public string gitGetTokenUrl{get;set;}  
		public string remoteServerArchive { get; set; }
		public List<string> ArchiveExcludeDir { get; set; }
		public List<appToBeDeployed> servicesToBeDeployed {get; set;}
		public string domain { get; set; }
		public int retryCount { get; set; }
}

public class PublishConfigDetails
{
		public string publishDefinitionName { get; set; }
		public string appName { get; set; }
		public string ServiceName { get; set; }
		public string src { get; set; }
		public string AppType { get; set; }	 //windowservice & IISHosted
		public Environments Environments { get; set ; }
}



public class appToBeDeployed
{
	public string Name { get; set; }
	public string publishConfigFileName { get; set; }
	public string srcRoot {get; set;}
	public string[] prodFiles {get; set;}
	public List<publishDirectory> publishDirs {get; set;}
}

public class publishDirectory
{
	public string prodDir { get; set; }
	public string srcDir { get; set; }
}
	
  public class Environments
  {
		public EnvironmentDetails Dev { get ; set; }
		public EnvironmentDetails Qa { get ; set; }
		public EnvironmentDetails Uat { get ; set; }
		public EnvironmentDetails Prod { get ; set; }
  }

  public class EnvironmentDetails
  {
		public bool HealthCheckEnabled  {get; set;}
		public string HealthCheckApiUrl {get; set;}
		public string RemoteServerAppPath { get; set; }
		public List<Schemadetail> schemadetails { get; set; }
		public DataCenter PDC {get; set; }
		public DataCenter FDC {get; set; }
		public DataCenter DataCenterList {get; set; }
		public string Backup  {get; set;} //"ALL", "NONE" or "PRIMARY"
  }

public class Schemadetail
{
    public string name { get; set; }
    public DBServiceDetails details { get; set; }
}

public class DBServiceDetails
{
	public List<string> servicename { get; set; }
	public string username { get; set; }
}

 public class DeployStatus
 {
	 public string Server {get; set;}
	 public string Task {get; set;}
	 public string Status {get; set;}	 
	 public string Comments {get; set;}
 }

 public class DataCenter
 {
	public Dictionary<string, string> AppConfigPath  {get; set;}
	public List<RemoteServer> RemoteServerList {get; set; }
 }

 public class RemoteServer
 {
	public string Name {get; set;}
	public bool IsStopIIS {get; set;}
	public bool IsArchive {get; set;}
	public string Site {get; set;}
	public string AppPool {get; set;}
	public string AppPath {get; set;}
	public bool InUse {get; set;}
 }

  public enum EnvironmentType
  {
	  DEV = 1,
	  QA = 2,
	  UAT = 3,
	  PROD = 4
  }

  public class GitToken
  {
	public string Access_Token{get;set;}
	public string Token_Type{get;set;}
	public string Refresh_Token {get;set;}
	public string Scope{get;set;}
	public string Created_At{get;set;}
  }
