 
#region MISC METHODS

public async Task GetGitCredentials()
{
	try
	{
		var authMethod = new VaultSharp.V1.AuthMethods.AppRole.AppRoleAuthMethodInfo (vaultRoleId,vaultSecretId);
		var mountPoint = "secret";
		// Initialize settings. You can also set proxies, custom delegates etc. here.
		var vaultClientSettings = new VaultSharp.VaultClientSettings(vaultApiUrl, authMethod);
		//Using vault token create the vault client
		var vaultClient = new VaultClient(vaultClientSettings);
		//Query the get credentials config kv path 
		var activeConfig = await vaultClient.V1.Secrets.KeyValue.V1.
						    ReadSecretAsync(vaultGitCredentialsKVPath, mountPoint).ConfigureAwait(false);		
		if(activeConfig?.Data!=null && activeConfig.Data["username"] != null && activeConfig.Data["password"] != null)
		{
			gitUser = activeConfig.Data["username"].ToString();
			gitPassword = activeConfig.Data["password"].ToString();
			GetAuthToken();
		}
		else
		{
			throw new Exception("Unable to get git credentials");
		}
	}
	catch(Exception ex)
	{
		FileAppendText(logFile, String.Format("{0}Protect {2} Branch Error : {1} {0}",Environment.NewLine, ex.Message));
		ConsoleWriteError(String.Format("{0}Protect {2} Branch Error :{0}{1}{0}",Environment.NewLine, ex.Message));
		throw;
	}
}

public void GetAuthToken()
{
	try
	{			
		  
		var client = new RestClient(gitGetTokenUrl);
		var request = new RestRequest (Method.POST);		
		request.AddHeader ("Content-Type", "application/json");
		request.AddJsonBody(new { grant_type= "password",username = gitUser, password = gitPassword });
		IRestResponse response = client.Execute (request);
	
		HttpStatusCode statusCode = response.StatusCode;
		int numericStatusCode = (int)statusCode;		
		if(numericStatusCode == 200)
		{
			var deserial = new RestSharp.Deserializers.JsonDeserializer();
			gitProtectBranchApiToken = deserial.Deserialize<GitToken>(response).Access_Token;	
			gitProtectBranchTokenType = deserial.Deserialize<GitToken>(response).Token_Type;
		}
		else
		{
			throw new Exception("Unable to get token");
		}
			
	}
	catch(Exception ex)
	{
		FileAppendText(logFile, String.Format("{0}Protect {2} Branch Error : {1} {0}",Environment.NewLine, ex.Message));
		ConsoleWriteError(String.Format("{0}Protect {2} Branch Error :{0}{1}{0}",Environment.NewLine, ex.Message));
	}
}

public void ProtectBranch(string branch)
{
	try
	{
		Console.WriteLine("");
		Console.WriteLine("Sending an API request to protect the development branch....");
		Console.WriteLine("");  
		  
		var client = new RestClient(String.Format(gitProtectBranchApiUrl, branch));
		var request = new RestRequest (Method.POST);
		request.AddHeader ("Authorization", gitProtectBranchTokenType + ' ' + gitProtectBranchApiToken);
		request.AddHeader ("Content-Type", "application/json");
		IRestResponse response = client.Execute (request);
	
		HttpStatusCode statusCode = response.StatusCode;
		int numericStatusCode = (int)statusCode;
			      
		//201 - Created
		if(numericStatusCode == (int)HttpStatusCode.Created)  
		{
			FileAppendText(logFile, String.Format("{0}############### Protected {1} Branch ###############{0}",Environment.NewLine,branch.ToUpper()));
		    ConsoleWriteInfo(String.Format("{0}############### Protected {1} Branch ###############{0}",Environment.NewLine,branch.ToUpper()));
		}
		else
		{   
			ConsoleWriteError(String.Format("{2}Protect {3} Branch Request Not Successful - Status Code: {0} {2}Message : {1}{2}",statusCode,(response.ErrorMessage ?? response.Content), Environment.NewLine,branch.ToUpper()));
			FileAppendText(logFile,String.Format("{2}Protect {3} Branch Request Not Successful {2}Status Code:{0} {2}Message : {1}{2}",statusCode,(response.ErrorMessage ?? response.Content), Environment.NewLine,branch.ToUpper()));
		}			
	}
	catch(Exception ex)
	{
		FileAppendText(logFile, String.Format("{0}Protect {2} Branch Error : {1} {0}",Environment.NewLine, new CakeException(),branch.ToUpper()));
		ConsoleWriteError(String.Format("{0}Protect {2} Branch Error :{0}{1}{0}",Environment.NewLine, new CakeException(),branch.ToUpper()));
	}
}

public void UnProtectBranch(string branch)
{
	try
	{
		Console.WriteLine("");
		Console.WriteLine("Sending an API request to UnProtect the development branch....");
		Console.WriteLine("");
		
		var client = new RestClient(String.Format(gitUnProtectBranchApiUrl, branch));
		var request = new RestRequest(Method.DELETE);
		request.AddHeader ("Authorization", gitProtectBranchTokenType + ' ' + gitProtectBranchApiToken);
		request.AddHeader("Content-Type", "application/json");
		IRestResponse response = client.Execute(request);
		
		HttpStatusCode statusCode = response.StatusCode;
		int numericStatusCode = (int)statusCode;
		
		//204 - processed request successfully with no content		
		if(numericStatusCode == (int)HttpStatusCode.NoContent)  
		{
			FileAppendText(logFile, String.Format("{0}############### Unprotected {1} Branch ###############{0}",Environment.NewLine,branch));
			ConsoleWriteInfo(String.Format("{0}############### Unprotected {1} Branch ###############{0}",Environment.NewLine,branch));
		}
		else
		{
			ConsoleWriteError(String.Format("{2}Unprotect {3} Branch Request Not Successful - Status Code: {0} {2}Message : {1}{2}",statusCode,(response.ErrorMessage ?? response.Content),Environment.NewLine,branch.ToUpper()));
			FileAppendText(logFile,String.Format("{2}Unprotect {3} Branch Request Not Successful - Status Code:{0} {2}Message : {1}{2}",statusCode,(response.ErrorMessage ?? response.Content),Environment.NewLine,branch.ToUpper()));
		}
	}
	catch(Exception ex)
	{
		FileAppendText(logFile, String.Format("{0}Unprotect Branch API Error : {1} {0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Unprotect Branch API Error :{0}{1}{0}",Environment.NewLine, new CakeException()));
	}
}

public void GitCommitMessagesFromHeadToTag(string readMeFilePath)
{
	try
	{
		Console.WriteLine("");
		Console.WriteLine("Creating readme markdown file....");
		Console.WriteLine("");
		
		using (var repo = new Repository(localRepo))
		{
			var strSha = Convert.ToString(repo.Head.Tip.Id);  //current  Head commit id 
				
			Tag tagFrom  = repo.Tags[tagName];   //last tag - one before current tag 
					
			var filter = new CommitFilter();

			filter.IncludeReachableFrom = strSha;

			if(tagFrom != null && tagFrom.Target != null)
			{
				filter.ExcludeReachableFrom = tagFrom.Target.Sha;
			}

			var results = repo.Commits.QueryBy(filter).ToList();
			
			IList<string> listHeaders = new List<string>();

			foreach(var result in results)
			{
				if (result.Message.Contains(@":") && result.Message.Contains(@"(") && !result.Message.Contains(@"://"))
				{
					var  msgHeader =  result.Message.Split(':').ToArray();
					var msgSuperHeader = msgHeader[0].Split('(');
					
					if(listHeaders.Where(t => t.Trim().ToUpper() == msgSuperHeader[0].Trim().ToUpper()).Count() == 0) 
					{ 
						listHeaders.Add(msgSuperHeader[0].Trim()) ;
					}
				}
			}

			foreach (var supHeader in listHeaders.Distinct().OrderBy(t => t))
			{				
				StringBuilder sb = new StringBuilder();
				sb.Append( String.Format("{1}## {0}{1}",supHeader,Environment.NewLine));
				
				foreach(var result in results)
				{
					if (result.Message.Contains(@":") && result.Message.Contains(@"(") && !result.Message.Contains(@"://"))
					{
						var msgArray =  result.Message.Split(':').ToArray();
						var headMsgArray = msgArray[0].Split('(');
							  
						if ( supHeader.Trim().ToUpper() == headMsgArray[0].Trim().ToUpper()) 
						{ 
					
							sb.Append(String.Format("* **{0}** : {1}([{5}]({2}/commit/{3})){4}",
													headMsgArray[1].Replace(")","").Trim(),
													msgArray[1].Trim(),
													gitRepoURL.Replace(".git",""),
													result.Sha,Environment.NewLine,
													result.Sha.Substring(0,10)));
						}
					}
				}
			
			}
			
			StringBuilder sbMisc = new StringBuilder();
			foreach(var result in results)
			{
				if( !( result.Message.Contains(@":") && result.Message.Contains(@"(") ))
				{
					sbMisc.Append(String.Format("* {0}([{1}]({2}/commit/{3})){4}",
											result.Message,
											result.Sha.Substring(0,10),
											gitRepoURL.Replace(".git",""),
											result.Sha,
											Environment.NewLine
											));
				}
			}
		}
	}
	catch(Exception ex)
	{
		FileAppendText(logFile, String.Format("{0}Readme Markdown File Create Error : {1} {0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Readme Markdown File Create Error :{0}{1}{0}",Environment.NewLine, new CakeException()));
	}
} 
#endregion