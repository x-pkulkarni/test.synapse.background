//********************* MISC CLASSES & METH0DS *********************
   
#region MISC METHODS

public void ConsoleWriteInfo(string message)
{
		Console.ForegroundColor = ConsoleColor.Green;
		Console.WriteLine(message);
		Console.ResetColor();	
}

public void ConsoleWriteError(string message)
{
		Console.ForegroundColor = ConsoleColor.Red;
		Console.WriteLine(message);
		Console.ResetColor();	
}

public void ConsoleWriteCustom(string message, System.ConsoleColor color)
{
		Console.ForegroundColor = color;
		Console.WriteLine(message);
		Console.ResetColor();	
}

public void ProcStart(string gitArgs)
{
	System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
		
	startInfo.CreateNoWindow = false;
	startInfo.UseShellExecute = false;
	startInfo.FileName = "git.exe";
	  
	startInfo.Arguments = gitArgs;
		try			
		{
			using (System.Diagnostics.Process exeProcess = System.Diagnostics.Process.Start(startInfo))
			{
				Console.WriteLine("");
				exeProcess.WaitForExit();
			    Console.WriteLine("");
			}
		}
		catch { throw;}
}

public string MaskPassword(string parameterName)
{
	string strMaskInput="";
	
	try	
	{
		Console.ForegroundColor = ConsoleColor.DarkCyan;
		Console.WriteLine(String.Format("{1}{1}Please enter {0}:{1}",parameterName,Environment.NewLine));
		Console.ResetColor();
		ConsoleKeyInfo key;

		do
		{
			key = Console.ReadKey(true);

			if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
			{
				strMaskInput += key.KeyChar;
				Console.Write("*");
			}
			else
			{
				if (key.Key == ConsoleKey.Backspace && strMaskInput.Length > 0)
				{
					strMaskInput = strMaskInput.Substring(0, (strMaskInput.Length - 1));
					Console.Write("\b \b");
				}
		}
		}
		while (key.Key != ConsoleKey.Enter);   
	}
	catch { throw;}
	
	return strMaskInput;
  }

public void CreateAssemblyInfoFile()
{
	try
	{
		Console.WriteLine("");
		Console.WriteLine("Creating assembly version file....");
		Console.WriteLine("");
		
		var file = System.IO.Path.Combine(localRepo,ConfigSettings.assemblyInfoFile);
		var semVersion = string.Concat(assemblyVersion + "." + subVersion);
		CreateAssemblyInfo(file, new AssemblyInfoSettings {
					Configuration = "",
					Company= company,
					Product = product,
					Copyright = string.Format("Copyright (c) Geodis {0}", DateTime.Now.Year),
					ComVisible= false,
					Version = assemblyVersion,
					FileVersion = semVersion,
					InformationalVersion = semVersion
				});
		
		FileAppendText(logFile, String.Format("{0}############### Assembly Version File Created  ###############{0}",Environment.NewLine));
		ConsoleWriteInfo(String.Format("{0}############### Assembly Version File Created ###############{0}",Environment.NewLine));
	}
	catch(Exception ex)
	{
		FileAppendText(logFile, String.Format("{0}Assembly Version File Create Error : {1} {0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Assembly Version File Create Error :{0}{1}{0}",Environment.NewLine, new CakeException()));
	}
} 

public List<PublishConfigDetails> ReadPublishConfig(string selectedApps)
{	
	List<PublishConfigDetails> publishConfigSettings = new List<PublishConfigDetails>();;
	foreach(var app in appsToBeDeployed)
	{
		if(selectedApps.Equals("All") || app.Name.Equals(selectedApps))
		{
			var ConfigDetails = DeserializeJsonFromFile<PublishConfigDetails>(app.publishConfigFileName);
			publishConfigSettings.Add(ConfigDetails);
		}
	}
	return publishConfigSettings;
}

public bool OverrideAppConfig(string applicationName, string datacenter, EnvironmentDetails environmentDetails, string localDeploymentPath)
{
	if(datacenter == "PDC" && environmentDetails.PDC != null)
	{
		if(environmentDetails.PDC.AppConfigPath != null && environmentDetails.PDC.AppConfigPath.ContainsKey(applicationName))
		{
			var sourceFilePath = System.IO.Path.Combine(MakeAbsolute(Directory(".")).FullPath,environmentDetails.PDC.AppConfigPath[applicationName]);
			var sourceFileName =System.IO.Path.GetFileName(sourceFilePath);
			var destinationFilePath = System.IO.Path.Combine(localDeploymentPath + "/bin",sourceFileName);
			System.IO.File.Copy(sourceFilePath,destinationFilePath, true);
			return true;
		}
		else { return true; }
	}
	else if(datacenter == "FDC" && environmentDetails.FDC != null)
	{
		if(environmentDetails.FDC.AppConfigPath != null && environmentDetails.FDC.AppConfigPath.ContainsKey(applicationName))
		{
			var sourceFilePath = System.IO.Path.Combine(MakeAbsolute(Directory(".")).FullPath,environmentDetails.FDC.AppConfigPath[applicationName]);
			var sourceFileName =System.IO.Path.GetFileName(sourceFilePath);
			var destinationFilePath = System.IO.Path.Combine(localDeploymentPath + "/bin",sourceFileName);
			System.IO.File.Copy(sourceFilePath,destinationFilePath, true);
			return true;
		}
		else { return true; }
	}
	return false;
}

public bool ExecuteFlyWay(string schemaName, List<string> serviceNameList, string UserName)
{	
			Console.WriteLine("");
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine(String.Format("Starting {0} schema migration....",schemaName));
			Console.ResetColor();
			Console.WriteLine("");
		
			Console.ForegroundColor = ConsoleColor.DarkCyan;
			Console.WriteLine(String.Format("{0}Do you want to continue flyway process for {1} schema ?  please select an option to continue :{0}", Environment.NewLine, schemaName));
			Console.ResetColor();
			
			var option = ""; 
			var menuRF = new EasyConsole.Menu();
			
			menuRF.Add("Yes",() => { option="Yes"; })
				  .Add("No",() =>  { option="No"; });
			
			menuRF.Display();	
			
			if(option == "Yes")
			{
			
				Console.WriteLine("");
				var rootPath = MakeAbsolute(Directory("../")).FullPath;
				Console.WriteLine("Path {0}", System.IO.Path.Combine(rootPath,rootPath, flywayToolVersion));
				System.IO.Directory.SetCurrentDirectory(System.IO.Path.Combine(rootPath, flywayToolVersion));
				
				Environment.CurrentDirectory = System.IO.Directory.GetCurrentDirectory();
			
				var environment = buildMode;
				
				Console.WriteLine("");
				Console.ForegroundColor = ConsoleColor.DarkCyan;
				Console.WriteLine("Please enter the version number (don't include 'V'):");
				Console.ResetColor();
				
				var migrationVersion = Console.ReadLine();
				
				Console.WriteLine("");
				Console.ForegroundColor = ConsoleColor.DarkCyan;
				Console.WriteLine("Please enter the schema password: ");
				Console.ResetColor();
				
				var schemaPassword = Console.ReadLine();	
					
				var sqlScrtiptsDir = String.Format(@"{0}\sql\{1}\{2}\",localRepo,schemaName,sqlReleaseManagementDir);
					
				foreach(var serviceName in serviceNameList)
				{
					if(serviceName.Length > 0){
						
						var flywayCommand = "-user=" + UserName + " -password=" + schemaPassword + " -locations=" + "filesystem:" + rootPath + sqlScrtiptsDir + environment + " -target="+ migrationVersion + " migrate -url=jdbc:oracle:thin:@ldap://ldap.usup.ohlogistics.com/" + serviceName + ",cn=OracleContext,dc=ohlogistics,dc=com";
						
						Console.WriteLine("Flyway Command {0}", flywayCommand);
						Console.WriteLine("Service Name {0}", serviceName);
						Console.WriteLine("SQL path {0}", "filesystem:" + rootPath + sqlScrtiptsDir + environment );
					
						System.Diagnostics.ProcessStartInfo startInfoRF = new System.Diagnostics.ProcessStartInfo();
						
						startInfoRF.CreateNoWindow = false;
						startInfoRF.UseShellExecute = false;
						startInfoRF.FileName = "flyway.cmd";
						startInfoRF.Arguments = flywayCommand;
						
						try			
						{
							using (System.Diagnostics.Process exeProcess = System.Diagnostics.Process.Start(startInfoRF))
							{
								Console.WriteLine("");
								exeProcess.WaitForExit();
							}
						}
						catch
						{
							throw;
						}
						
					}
					
				}
				
				FileAppendText(logFile, String.Format("{0}############## {1} Schema Migration Completed  ##############{0}",Environment.NewLine,buildMode.ToUpper()));				   
				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine(String.Format("{0}############## {1} Schema Migration Completed  ##############{0}",Environment.NewLine,buildMode.ToUpper()));
				Console.ResetColor();
				Console.WriteLine();
				
				System.IO.Directory.SetCurrentDirectory(basePath);
				
				Environment.CurrentDirectory = System.IO.Directory.GetCurrentDirectory();
				
				Console.WriteLine("");
				Console.WriteLine("Changed Path {0}", Environment.CurrentDirectory);
				Console.WriteLine("");	
			}
			else
			{
				Console.ForegroundColor = ConsoleColor.Green;
				Console.WriteLine(String.Format("{0}############## {1} Schema Migration Skipped For {2} ##############{0}",Environment.NewLine,buildMode.ToUpper(),schemaName));
				Console.ResetColor();
				Console.WriteLine();
			}
			return true;
	}

public bool StageArtifacts(string dataCenter, RemoteServer remoteServer, Dictionary<string, string> webConfigs)
{
	Console.WriteLine("");
	Console.WriteLine( String.Format("Copying zip file to remote server[{0}] staging directory....",remoteServer.Name));
	Console.WriteLine("");
				
	remoteServerUserName="";
	remoteServerPass="";
								
	//Enter remote server login credentials
	while (String.IsNullOrEmpty(remoteServerUserName.Trim())) 
	{
		Console.ForegroundColor = ConsoleColor.DarkCyan;
		Console.WriteLine(String.Format("{0}Please enter remote server user-name:{0}",Environment.NewLine));
		Console.ResetColor();
					 
		remoteServerUserName = @"OHL\" + Console.ReadLine();
	}

	while (String.IsNullOrEmpty(remoteServerPass.Trim())) 
	{
		remoteServerPass = MaskPassword("remote server password");
	}	

	var zfiles = GetFiles(System.IO.Path.Combine(stagingDir, dataCenter, "*.zip"));   
				
	foreach (var zfile in zfiles)
	{
		try
		{
		Console.WriteLine("");
		Console.WriteLine( String.Format("Copying zip file to remote server[{0}] staging directory....",remoteServer.Name));
		Console.WriteLine("");	

		Policy
			.Handle<Exception>()
			.Retry(retryCount, (exception, rCount) => {
			
					ConsoleWriteCustom(String.Format("{0}StageArtifacts Error: {1}",Environment.NewLine,exception.Message),ConsoleColor.DarkRed);
		 			ConsoleWriteCustom(String.Format("Retry #{0}",rCount),ConsoleColor.DarkYellow);
		 	
					while (String.IsNullOrEmpty(remoteServerUserName.Trim())) 
					{
						ConsoleWriteCustom(String.Format(@"{0}Please enter remote server user-name:{0}",Environment.NewLine),ConsoleColor.DarkCyan);
					
						var user = Console.ReadLine();

						if (!String.IsNullOrEmpty(user) && !user.Contains(@"\"))
						{
							remoteServerUserName = String.Format(@"{0}\{1}",domain,user); 
						}
						else if(!String.IsNullOrEmpty(user) && user.Contains(@"\") )
						{
							remoteServerUserName = user;
						}			
					}		

					while (String.IsNullOrEmpty(remoteServerPass.Trim())) 
					{
						remoteServerPass = MaskPassword("valid remote server password");
					}
					
			})
			.Execute(() => {
				
				CopyFiles(dataCenter, remoteServer, zfile, remoteServerUserName, remoteServerPass, webConfigs);
					
				});
		}
		catch { throw;	}	
	}
	return true;
}

public void CopyFiles(string dataCenter, RemoteServer remoteServer, FilePath zfile, string remoteServerUserName, string remoteServerPass, Dictionary<string, string> webConfigs)
{
  	try
	{	
		var remoteFilePath = "";

		if(zfile.ToString().Contains("BACKGROUNDSHOST"))
		{
			remoteFilePath = String.Format(@"\\{0}\{1}\{2}\{3}",remoteServer.Name,remoteServer.AppPath.Replace(@"/",@"\"), @"Backgrounds.Host", @"bin_prepublish");
		}
		else if(zfile.ToString().Contains("BACKGROUNDSPUBLISHER"))
		{
			remoteFilePath = String.Format(@"\\{0}\{1}\{2}\{3}",remoteServer.Name,remoteServer.AppPath.Replace(@"/",@"\"), @"Backgrounds.Publisher", @"bin_prepublish");
		}

		var sourceFilePath = String.Format(@"\\{0}\{1}",
											Environment.MachineName,
											System.IO.Path.Combine(MakeAbsolute(Directory(".")).FullPath.Replace(":","$"),
																	stagingDir, 
																	dataCenter, 
																	System.IO.Path.GetFileName(zfile.ToString()))
																	.Replace(@"/",@"\"));	
   
		var addCredArgs = String.Format(@"cmdkey.exe /add:{0} /user:{0}\Administrator /pass:{1}",remoteServer.Name,remoteServerPass);

		var unzipArgs = String.Format("-u {0} -p {1} cmd /c {2} X {3} -o{4}", 
										remoteServerUserName,
										remoteServerPass,
										zipUtilityPath,
										sourceFilePath,
										remoteFilePath);
	
		var deleteCredArgs = String.Format("cmdkey.exe /delete:{0}",remoteServer.Name);
	
		RunProcess(addCredArgs);

		if (RunProcess(unzipArgs) == 0)
			{
					FileAppendText(logFile, String.Format("{0}############### Copying Files To Remote Server [{1}] Staging Directory Completed - [{2}] ###############{0}",Environment.NewLine,remoteServer.Name,System.IO.Path.GetFileName(remoteFilePath)));
					ConsoleWriteCustom(String.Format("{0}############### Copying Files To Remote Server [{1}] Staging Directory Completed - [{2}] ###############{0}",Environment.NewLine,remoteServer.Name,System.IO.Path.GetFileName(remoteFilePath)),ConsoleColor.Green);
			}
			else
			{
					FileAppendText(logFile, String.Format("{0}############### Copying Files To Remote Server [{1}] Staging Directory Not Successful - [{2}] ###############{0}",Environment.NewLine,remoteServer.Name,System.IO.Path.GetFileName(remoteFilePath)));
					ConsoleWriteCustom(String.Format("{0}############### Copying Files To Remote Server [{1}] Staging Directory Not Successful - [{2}] ###############{0}",Environment.NewLine,remoteServer.Name,System.IO.Path.GetFileName(remoteFilePath)),ConsoleColor.Magenta);
			}

		if(webConfigs.Count > 0)
		{
			var sourceConfigFilePath ="";
			if(remoteFilePath.Contains("Backgrounds.Host"))
			{
				sourceConfigFilePath = webConfigs["Backgrounds.Host"].Replace(@"/",@"\");
			}
			else if(remoteFilePath.Contains("Backgrounds.Publisher"))
			{
				sourceConfigFilePath = webConfigs["Backgrounds.Publisher"].Replace(@"/",@"\");
			}
		 
			var targetConfigFilePath = remoteFilePath;
		
			var webConfigPathArgs = String.Format("-u {0} -p {1} cmd /c copy {2} {3}", 
													remoteServerUserName,
													remoteServerPass,
													sourceConfigFilePath,
													targetConfigFilePath);					

			if (RunProcess(webConfigPathArgs) == 0)
				{
						FileAppendText(logFile, String.Format("{0}############### Copying Web Config File To Remote Server [{1}] Staging Directory Completed - [{2}] ###############{0}",Environment.NewLine,remoteServer.Name,System.IO.Path.GetFileName(remoteFilePath)));
						ConsoleWriteCustom(String.Format("{0}############### Copying Web Config File To Remote Server [{1}] Staging Directory Completed - [{2}] ###############{0}",Environment.NewLine,remoteServer.Name,System.IO.Path.GetFileName(remoteFilePath)),ConsoleColor.Green);
				}
				else
				{
						FileAppendText(logFile, String.Format("{0}############### Copying Web Config File To Remote Server [{1}] Staging Directory Not Successful - [{2}] ###############{0}",Environment.NewLine,remoteServer.Name,System.IO.Path.GetFileName(remoteFilePath)));
						ConsoleWriteCustom(String.Format("{0}############### Copying Web Config File To Remote Server [{1}] Staging Directory Not Successful - [{2}] ###############{0}",Environment.NewLine,remoteServer.Name,System.IO.Path.GetFileName(remoteFilePath)),ConsoleColor.Magenta);					
				}	
		}

		RunProcess(deleteCredArgs);
	
	}
	catch { throw;	}	
}

public int RunProcess(string strArgs)
{	
	int exitcode = 0;
	System.Diagnostics.ProcessStartInfo startProcess = new System.Diagnostics.ProcessStartInfo();
				
	startProcess.CreateNoWindow = false;
	startProcess.UseShellExecute = false;
	startProcess.FileName = psUtilityPath;
	startProcess.Arguments = String.Format("-accepteula -nobanner {0}",strArgs);
					
	using (System.Diagnostics.Process exeProcess = System.Diagnostics.Process.Start(startProcess))
		{
			Console.WriteLine("");
			exeProcess.WaitForExit();
			exitcode = exeProcess.ExitCode;
		}
	
	return exitcode ;
}

#endregion

#region Email Notification
//Import for MarkDown to HTML conversion
using Markdig;
public void NotifyReadMe(string filePath, string subject, string toEmail)
{
	try
	{
		var contentHTML = "";
		try
		{
			string contentMarkDown = System.IO.File.ReadAllText(filePath);
			contentHTML = Markdown.ToHtml(contentMarkDown);
		}
		catch(Exception e)
		{
			contentHTML = "<p style='color: red;font-style:italic;'>" + e.Message +"</p>";
		}
		StringBuilder content = new StringBuilder();
		content.Append("<p><b>Project:</b> ").Append(buildDefinitionName).Append("<br/>");
		content.Append("<b>Environment:</b> ").Append(buildMode).Append("<br/>");
		content.Append("<b>Date of ").Append(buildAction == "STAGE"? "build":"publish").Append(":</b> ");
		content.Append(String.Format("{0:F}",timeStarted)).Append("<br/>");

		if(buildAction == "STAGE")
		{
			var diff = DateTime.Now.Subtract(timeStarted);
			content.Append("<b>Build duration:</b> ").Append(diff.TotalSeconds.ToString("N2")).Append(" sec<br/>");
		}
		content.AppendLine("</p><br/> <div><span style='font-weight:bold;'>CHANGE SET: </span><br/>").Append(contentHTML).Append("</div>");
		subject =  "<<Automated Email>> ["+ buildDefinitionName + "] " + subject;
		Notify(content.ToString(),subject,toEmail);
	}
	catch(Exception e)
	{
		ConsoleWriteError("Email notification failed!");
	}
}

public void NotifyEventCompletion(string message, string subject, string toEmail)
{
	try
	{
		StringBuilder content = new StringBuilder();
		content.Append("<p><b>Project:</b> ").Append(buildDefinitionName).Append("<br/>");
		content.Append("<b>Environment:</b> ").Append(buildMode).Append("<br/>");
		content.Append("<b>Date of ").Append(buildAction == "STAGE"? "build":"publish").Append(":</b> ");
		content.Append(String.Format("{0:F}",timeStarted)).Append("<br/>");

		
		var diff = DateTime.Now.Subtract(timeStarted);
		content.Append("<b>Deployment duration:</b> ").Append(diff.TotalSeconds.ToString("N2")).Append(" sec<br/>");
		
		content.AppendLine("</p><br/> <div><br/>").Append(message).Append("</div>");
		subject =  "<<Automated Email>> ["+ buildDefinitionName + "] " + subject;
		Notify(content.ToString(),subject,toEmail);
	}
	catch(Exception e)
	{
		ConsoleWriteError("Email notification failed!");
	}
}

public void NotifyError(Exception exception, string toEmail)
{
	try
	{
		StringBuilder content = new StringBuilder();
		content.Append("<p><b>Project:</b> ").Append(buildDefinitionName).Append("<br/>");
		content.Append("<b>Environment:</b> ").Append(buildMode).Append("<br/>");
		content.Append("<b>Date of ").Append(buildAction == "STAGE"? "build":"publish").Append(":</b> ");
		content.Append(String.Format("{0:F}",timeStarted)).Append("<br/></p>");
		if(buildAction == "STAGE")
		{
			var diff = DateTime.Now.Subtract(timeStarted);
			content.Append("<b>Build duration:</b> ").Append(diff.TotalSeconds.ToString("N2")).Append("sec<br/>");
		}

		content.AppendLine("</p><br/> <div><span style='font-weight:bold;'>Build pipeline script error: </span><br/>");
		content.AppendLine("<p style='color: red;font-style:italic;'>");
		content.AppendLine(exception.Message).Append("</p>");
		content.AppendLine("<i>").Append(exception.StackTrace).Append("</i></div>");
		var subject = "<<Automated Email>> ["+ buildDefinitionName + "] " + buildMode + ": " + (buildAction == "STAGE"? "Staging":"Publish") + " failed!";	
		Notify(content.ToString(),subject,toEmail);
	}
	catch(Exception e)
	{
		ConsoleWriteError("Email notification failed!");
	}
}

public bool CheckEmailNotifcationEnabled()
{
	if(buildAction == "STAGE")
	{
 		if(ConfigSettings.EmailEnabledForBuild)
		{
			return true;
		}
	}
	else
	{
		if(ConfigSettings.EmailEnabledForPublish)
		{
			return true;
		}
	}
	return false;
}

public string GetEmailRecipients()
{
	if(buildAction == "STAGE")
	{
			return ConfigSettings.EmailRecipientsForBuildEvent;
	}
	else
	{
			return ConfigSettings.EmailRecipientsForPublishEvent;
	}
}

public void Notify(string content, string subject, string toEmail)
{
		SendEmail(toEmail, "do-not-reply@geodis.com", "SMTPRelay.ohlogistics.com", subject, content);
}

//Import for Send email method
using System.Net.Mail;

public void SendEmail(string toEmail, string fromEmail, string smtpHost, string subject, string mailBody)
{
	MailMessage mail = new MailMessage(fromEmail, toEmail);
	SmtpClient client = new SmtpClient();
	client.Port = 25;
	client.DeliveryMethod = SmtpDeliveryMethod.Network;
	client.UseDefaultCredentials = false;
	client.Host = smtpHost;
	mail.Subject = subject;
	mail.Body = mailBody;
	mail.IsBodyHtml = true;
	client.Send(mail);
}

#endregion

#region IIS operations
using System.Diagnostics;
public void IISStop( string remoteServerUserName, string remoteServerPass, string remoteServerName )
{
			Console.WriteLine("");
			ConsoleWriteCustom(String.Format("Stopping IIS on remote server {0}....",remoteServerName),ConsoleColor.Cyan);
			Console.WriteLine("");			
			
			try
            {
			 	using(Process iisreset = new Process())
				{                
					iisreset.StartInfo.FileName = @"iisreset.exe";
					iisreset.StartInfo.WorkingDirectory = Environment.SystemDirectory;
					iisreset.StartInfo.Arguments = "/stop " + remoteServerName;
					var username = remoteServerUserName.Split(@"\".ToCharArray());
					iisreset.StartInfo.Domain = username[0];
					iisreset.StartInfo.UserName = username.Length>1?username[1]:username[0];
					iisreset.StartInfo.PasswordInClearText = remoteServerPass;
					iisreset.StartInfo.UseShellExecute = false;
					iisreset.StartInfo.CreateNoWindow = true;
					var result = iisreset.Start();  
					iisreset.WaitForExit();
					if(result && iisreset.ExitCode == 0)
					{
						FileAppendText(logFile, String.Format("{0}############## IIS Stopped On Remote Server [{1}]. ##############{0}",Environment.NewLine ,remoteServerName));			   
						ConsoleWriteInfo(String.Format("{0}############## IIS Stopped On Remote Server [{1}]. ##############{0}",Environment.NewLine ,remoteServerName ));
					}
					else
					{
						FileAppendText(logFile, String.Format("{0}############## IIS Stop On Remote Server [{1}] Not Successful with exit code {2}. ##############{0}",Environment.NewLine, remoteServerName, iisreset.ExitCode ));			   
						ConsoleWriteError(String.Format("{0}############## IIS Stop On Remote Server [{1}] Not Successful with exit code {2}. ##############{0}",Environment.NewLine, remoteServerName, iisreset.ExitCode ));
						throw new Exception(String.Format("{0} IIS Stop On Remote Server [{1}] Failed With exit code {2}   ##############{0}",Environment.NewLine, remoteServerName,iisreset.ExitCode));
					}
				}
			}
            catch (Exception e)
            {
              FileAppendText(logFile, String.Format("{0}############## IIS Stop On Remote Server [{1}] Not Successful. ##############{0}",Environment.NewLine ,remoteServerName ));			   
				ConsoleWriteError(String.Format("{0}############## IIS Stop On Remote Server [{1}] Not Successful. ##############{0}",Environment.NewLine,remoteServerName ));
						throw new Exception(String.Format("{0} IIS Stop On Remote Server [{1}] Failed With error message {2}   ##############{0}",Environment.NewLine, remoteServerName,e.Message));
            }
}

public void IISStopSite(string psUtilityPath, string remoteServerUserName, string remoteServerPass, string remoteServerName, string siteName , string appPoolName )
{
			Console.WriteLine("");
			ConsoleWriteCustom(String.Format("Stopping IIS on remote server {0}....",remoteServerName),ConsoleColor.Cyan);
			Console.WriteLine("");			
			
			try
            {
				var iisSiteStopCommand = "\\\\" + remoteServerName + " -s -i " + Environment.SystemDirectory + "\\inetsrv\\appcmd.exe" ;
				var iisAppPoolCleanCommand = iisSiteStopCommand;
				iisSiteStopCommand += @" stop site """ + siteName + @"""";
				iisAppPoolCleanCommand += @" recycle apppool """ + appPoolName + @"""";
				 
			 	System.Diagnostics.ProcessStartInfo iisreset = new System.Diagnostics.ProcessStartInfo();

                iisreset.FileName = psUtilityPath;
                iisreset.WorkingDirectory = System.IO.Path.GetDirectoryName(psUtilityPath);
                iisreset.Arguments = iisSiteStopCommand;
				var username = remoteServerUserName.Split(@"\".ToCharArray());
                iisreset.Domain = username[0];
                iisreset.UserName = username.Length>1?username[1]:username[0];
                iisreset.PasswordInClearText = remoteServerPass;
                iisreset.UseShellExecute = false;
                iisreset.CreateNoWindow = true;
                using (System.Diagnostics.Process exeProcess = System.Diagnostics.Process.Start(iisreset))
				{ 
					Console.WriteLine("");
					exeProcess.WaitForExit();
					if(exeProcess.ExitCode == 0)
					{
						iisreset.Arguments = iisAppPoolCleanCommand;
						using (System.Diagnostics.Process exeProcessAppPool = System.Diagnostics.Process.Start(iisreset))
						{ 
							exeProcessAppPool.WaitForExit();
							if(exeProcessAppPool.ExitCode == 0)
							{
								FileAppendText(logFile, String.Format("{0}############## Site {2} Stopped On Remote Server [{1}]. ##############{0}",Environment.NewLine ,remoteServerName,siteName));			   
								ConsoleWriteInfo(String.Format("{0}############## Site {2} Stopped On Remote Server [{1}]. ##############{0}",Environment.NewLine ,remoteServerName,siteName));
							}
							else
							{
								FileAppendText(logFile, String.Format("{0}############## Site {2} Stopped On Remote Server [{1}], But recycle apppool is failed. ##############{0}",Environment.NewLine ,remoteServerName,siteName));			   
								ConsoleWriteInfo(String.Format("{0}############## Site {2} Stopped On Remote Server [{1}], But recycle apppool is failed. ##############{0}",Environment.NewLine ,remoteServerName,siteName));
							}
						}
					}
					else
					{
						FileAppendText(logFile, String.Format("{0}############## Site {2} Stop On Remote Server [{1}] Not Successful. ##############{0}",Environment.NewLine ,remoteServerName,siteName ));			   
			  			ConsoleWriteError(String.Format("{0}############## Site {2} Stop On Remote Server [{1}] Not Successful with exit code {3}. ##############{0}",Environment.NewLine,remoteServerName,siteName, exeProcess.ExitCode ));
					}	
				}
			}
            catch (Exception e)
            {
              	FileAppendText(logFile, String.Format("{0}############## Site {2} Stop On Remote Server [{1}] Not Successful. ##############{0}",Environment.NewLine ,remoteServerName, siteName ));			   
			  	ConsoleWriteError(String.Format("{0}############## Site {2} Stop On Remote Server [{1}] Not Successful. ##############{0}",Environment.NewLine,remoteServerName, siteName ));
				throw new Exception(String.Format("{0} Site {2} Stop On Remote Server [{1}] Failed With error message {3}   ##############{0}",Environment.NewLine, remoteServerName, siteName, e.Message));
            }
}



public void IIIStart( string remoteServerUserName, string remoteServerPass, string remoteServerName )
{
			Console.WriteLine("");
			ConsoleWriteCustom(String.Format("Re-starting IIS on remote server {0}....", remoteServerName), ConsoleColor.Cyan);
			Console.WriteLine("");
			
			try
            {
			 	using(Process iisreset = new Process())
				{
					iisreset.StartInfo.FileName = @"iisreset.exe";
					iisreset.StartInfo.WorkingDirectory = Environment.SystemDirectory;
					iisreset.StartInfo.Arguments = "/start " + remoteServerName;
					var username = remoteServerUserName.Split(@"\".ToCharArray());
					iisreset.StartInfo.Domain = username[0];
					iisreset.StartInfo.UserName = username.Length>1?username[1]:username[0];
					iisreset.StartInfo.PasswordInClearText = remoteServerPass;
					iisreset.StartInfo.UseShellExecute = false;
					iisreset.StartInfo.CreateNoWindow = true;
					var result = iisreset.Start();  
					iisreset.WaitForExit();
					if(result && iisreset.ExitCode == 0)
					{
						FileAppendText(logFile, String.Format("{0}############## IIS Started On Remote Server [{1}]. ##############{0}",Environment.NewLine, remoteServerName ));			   
						ConsoleWriteInfo(String.Format("{0}############## IIS Started On Remote Server [{1}]. ##############{0}",Environment.NewLine, remoteServerName ));
					}
					else
					{
						FileAppendText(logFile, String.Format("{0}############## IIS Start On Remote Server [{1}] Not Successful with exit code {2}. ##############{0}",Environment.NewLine, remoteServerName, iisreset.ExitCode ));			   
						ConsoleWriteError(String.Format("{0}############## IIS Start On Remote Server [{1}] Not Successful with exit code {2}. ##############{0}",Environment.NewLine, remoteServerName, iisreset.ExitCode ));
						throw new Exception(String.Format("{0} IIS Start On Remote Server [{1}] Failed With exit code {2}   ##############{0}",Environment.NewLine, remoteServerName,iisreset.ExitCode));
					}
				}
			}
            catch (Exception e)
            {
              	FileAppendText(logFile, String.Format("{0}############## IIS Start On Remote Server [{1}] Not Successful. ##############{0}",Environment.NewLine, remoteServerName ));			   
				ConsoleWriteError(String.Format("{0}############## IIS Start On Remote Server [{1}] Not Successful. ##############{0}",Environment.NewLine, remoteServerName ));
				throw new Exception(String.Format("{0} IIS Start On Remote Server [{1}] Failed With error message {2}   ##############{0}",Environment.NewLine, remoteServerName,e.Message));
            }				
}

public void IIIStartSite(string psUtilityPath, string remoteServerUserName, string remoteServerPass, string remoteServerName, string siteName)
{
			Console.WriteLine("");
			ConsoleWriteCustom(String.Format("Re-starting IIS on remote server {0}....", remoteServerName), ConsoleColor.Cyan);
			Console.WriteLine("");
			
			try
            {
			 	var iisSiteStartCommand = "\\\\" + remoteServerName + " -s -i " + Environment.SystemDirectory + "\\inetsrv\\appcmd.exe" ;
				iisSiteStartCommand += @" start site """ + siteName + @"""";
				 
			 	System.Diagnostics.ProcessStartInfo iisreset = new System.Diagnostics.ProcessStartInfo();

                iisreset.FileName = psUtilityPath;
                iisreset.WorkingDirectory = System.IO.Path.GetDirectoryName(psUtilityPath);
                iisreset.Arguments = iisSiteStartCommand;
				var username = remoteServerUserName.Split(@"\".ToCharArray());
                iisreset.Domain = username[0];
                iisreset.UserName = username.Length>1?username[1]:username[0];
                iisreset.PasswordInClearText = remoteServerPass;
                iisreset.UseShellExecute = false;
                iisreset.CreateNoWindow = true;
                using (System.Diagnostics.Process exeProcess = System.Diagnostics.Process.Start(iisreset))
				{ 
					Console.WriteLine("");
					exeProcess.WaitForExit();
					if(exeProcess.ExitCode == 0)
					{
						FileAppendText(logFile, String.Format("{0}############## Site {2} Started On Remote Server [{1}]. ##############{0}",Environment.NewLine, remoteServerName,siteName ));			   
						ConsoleWriteInfo(String.Format("{0}############## Site {2} Started On Remote Server [{1}]. ##############{0}",Environment.NewLine, remoteServerName ,siteName));
					}
					else
					{
						FileAppendText(logFile, String.Format("{0}############## Site {2} Start On Remote Server [{1}] Not Successful. ##############{0}",Environment.NewLine ,remoteServerName,siteName ));			   
			  			ConsoleWriteError(String.Format("{0}############## Site {2} Start On Remote Server [{1}] Not Successful with exit code {3}. ##############{0}",Environment.NewLine,remoteServerName,siteName, exeProcess.ExitCode ));
					}
				}
			}
            catch (Exception e)
            {
              	FileAppendText(logFile, String.Format("{0}############## Site {2} Start On Remote Server [{1}] Not Successful. ##############{0}",Environment.NewLine, remoteServerName ,siteName));			   
				ConsoleWriteError(String.Format("{0}############## Site {2} Start On Remote Server [{1}] Not Successful. ##############{0}",Environment.NewLine, remoteServerName,siteName ));
				throw new Exception(String.Format("{0} Site {2} Start On Remote Server [{1}] Failed With error message {3}   ##############{0}",Environment.NewLine, remoteServerName,siteName,e.Message));
            }				
}
#endregion

#region HELATH CHECK API - INITIALIZE APP

public void HealthCheckApi(string healthCheckApiUrl , string remoteServerName, string appName )
{
	try
	{
		Console.WriteLine();
		ConsoleWriteCustom(String.Format("Initializing {0} on remote server {1}.....",appName,remoteServerName), ConsoleColor.Cyan);
		Console.WriteLine();
								
		var appUrl = String.Format(healthCheckApiUrl, remoteServerName);
							
		var client = new RestClient(appUrl);
		var request = new RestRequest(Method.GET);
		IRestResponse response = client.Execute(request);
		HttpStatusCode statusCode = response.StatusCode;
		int numericStatusCode = (int)statusCode;
				
		if (numericStatusCode == (int) HttpStatusCode.OK )
		{
			ConsoleWriteInfo( String.Format("{0}############## HealthCheck Task: Initializing {2} completed on remote server {1} ##############{0}",Environment.NewLine,remoteServerName,appName));
			Console.WriteLine();
			FileAppendText(logFile, String.Format("HELATH CHECK API RESPONSE FROM REMOTE SERVER {2}: {0}{1}",response.Content,Environment.NewLine,remoteServerName));
		}
		else
		{
			ConsoleWriteError(String.Format("HealthCheck Task: Initializing {4} on server ({2}) Not Successful {3}Status Code:{0}{3}Message :{3}{1}",numericStatusCode,(response.ErrorMessage ?? response.Content),remoteServerName,Environment.NewLine,appName));
			Console.WriteLine("");
			FileAppendText(logFile,String.Format("HealthCheck Task: Initializing {4} on server ({2}) Not Successful {3}Status Code:{0}{3}Message :{3}{1}",numericStatusCode,(response.ErrorMessage ?? response.Content),remoteServerName,Environment.NewLine,appName));
		}
	}
	catch (Exception ex)
	{ 	
		ConsoleWriteError(String.Format("HealthCheck Task: Initializing {1} on server ({0}) Not Successful",remoteServerName,appName));
		Console.WriteLine("");
		FileAppendText(logFile,String.Format("HealthCheck Task: Initializing {1} on server ({0}) Not Successful",remoteServerName,appName));
	}
}

#endregion

#region Back files

public void BackUpFiles(string zipUtilityPath, string remoteServerUserName, string remoteServerPass, string remoteServerName, string appName, string remoteServerArchivePath, string remoteServerAppPath, List<string> archiveExcludeDir)
{
	Console.WriteLine("");
	Console.ForegroundColor = ConsoleColor.Cyan;
	ConsoleWriteCustom(String.Format("Moving existing {1} application files to remote server[{0}] archive directory....",remoteServerName,appName), ConsoleColor.Cyan);
	Console.ResetColor();
	Console.WriteLine("");

        
	string destinationArchiveFileName= String.Format("{0}_{1}.zip",appName,DateTime.Now.ToString("yyyyMMddHHmmss"));
	string destinationArchivePath = String.Format(@"\\{0}\{1}\{2}",remoteServerName,remoteServerArchivePath,destinationArchiveFileName);

	try
	{
		using(Process backupFiles = new Process())
		{
			backupFiles.StartInfo.FileName = zipUtilityPath;
			backupFiles.StartInfo.WorkingDirectory = System.IO.Path.GetDirectoryName(zipUtilityPath);
			backupFiles.StartInfo.Arguments = @" a " + destinationArchivePath.Replace(@"/",@"\") + "  " + String.Format(@"\\{0}\{1}\{2}",remoteServerName,remoteServerAppPath.Replace(@"/",@"\"),appName);
			if(archiveExcludeDir.Count>0)
			{
			 	archiveExcludeDir.ForEach(i=> {backupFiles.StartInfo.Arguments += " -xr!" + i;} );
			}
			var username = remoteServerUserName.Split(@"\".ToCharArray());
			backupFiles.StartInfo.Domain = username.Length>1?username[0]:"OHL";
			backupFiles.StartInfo.UserName = username.Length>1?username[1]:username[0];
			backupFiles.StartInfo.PasswordInClearText = remoteServerPass;
			backupFiles.StartInfo.UseShellExecute = false;
			backupFiles.StartInfo.CreateNoWindow = true;
			var result = backupFiles.Start();  
			backupFiles.WaitForExit();
			if(result && backupFiles.ExitCode == 0)
			{
				FileAppendText(logFile, String.Format("{0}############## Archiving {2} Files On Remote Server Completed [{1}] ##############{0}",Environment.NewLine, remoteServerName,appName));			   
				ConsoleWriteInfo(String.Format("{0}############## Archiving {2} Files On Remote Server Completed [{1}] ##############{0}",Environment.NewLine, remoteServerName,appName));
			}
			else
			{
				FileAppendText(logFile, String.Format("{0}############## Archiving {2} Files On Remote Server [{1}] Not Successful With exit code: {3}  ##############{0}",Environment.NewLine, remoteServerName,appName,backupFiles.ExitCode));			   
				ConsoleWriteError(String.Format("{0}############## Archiving {2} Files On Remote Server [{1}] Not Successful With exit code: {3}  ##############{0}",Environment.NewLine, remoteServerName,appName,backupFiles.ExitCode));
				throw new Exception(String.Format("Archiving {2} Files On Remote Server [{1}] Failed With exit code: {3}    ##############{0}",Environment.NewLine, remoteServerName,appName,backupFiles.ExitCode));
			}
		}
	}
    catch (Exception e)
    {
		FileAppendText(logFile, String.Format("{0}############## Archiving {2} Files On Remote Server [{1}] Not Successful  ##############{0}",Environment.NewLine, remoteServerName,appName));			   
		ConsoleWriteError(String.Format("{0}############## Archiving {2} Files On Remote Server [{1}] Not Successful [{1}] ##############{0}",Environment.NewLine, remoteServerName,appName));
		throw new Exception(String.Format("Archiving {2} Files On Remote Server [{1}] Failed With error message: {3}    ##############{0}",Environment.NewLine, remoteServerName,appName,e.Message));
	}
}

#endregion

#region PublishArtifacts

public void PublishArtifacts(string remoteServerUserName, string remoteServerPass, string remoteServerName, string appName, string remoteServerAppPath,string webConfigPath)
{
	Console.WriteLine("");
	ConsoleWriteCustom( String.Format("Copying {1} files to remote server[{0}] application directory....",remoteServerName,appName), ConsoleColor.Cyan);
	Console.WriteLine("");

	try
	{	

        var addCredArgs = String.Format(@"cmdkey.exe /add:{0} /user:{0}\Administrator /pass:{1}",remoteServerName,remoteServerPass);
        var deleteCredArgs = String.Format("cmdkey.exe /delete:{0}",remoteServerName);		
        
		var deleteArgs = String.Format("-u {0} -p {1} cmd /c rmdir /s /q {2}",
										remoteServerUserName,
										remoteServerPass,
										String.Format(@"\\{0}\{1}\{2}\{3}",remoteServerName,remoteServerAppPath.Replace(@"/",@"\"), appName,"bin_backup"));

		
		var renameArgs = String.Format("-u {0} -p {1} cmd /c rename {2} {3}",
										remoteServerUserName,
										remoteServerPass,
										String.Format(@"\\{0}\{1}\{2}\{3}",remoteServerName,remoteServerAppPath.Replace(@"/",@"\"), appName,"bin"),"bin_backup");

		var publishArgs = String.Format("-u {0} -p {1} cmd /c rename {2} {3}",
										remoteServerUserName,
										remoteServerPass,
										String.Format(@"\\{0}\{1}\{2}\{3}",remoteServerName,remoteServerAppPath.Replace(@"/",@"\"), appName,"bin_prepublish"),"bin");
		RunProcess(addCredArgs);
		RunProcess(deleteArgs);
		
		if(RunProcess(renameArgs) == 0 && RunProcess(publishArgs) == 0)
		{
			FileAppendText(logFile, String.Format("{0}############## Copied {2} Files To Remote Server[{1}] Application Directory Completed ##############{0}",Environment.NewLine, remoteServerName,appName));			   
			ConsoleWriteInfo(String.Format("{0}############## Copied {2} Files To Remote Server[{1}] Application Directory Completed ##############{0}",Environment.NewLine, remoteServerName,appName));
			contentDeployStatus.Add(new DeployStatus { Server = remoteServerName , Task = "Publish Artifacts", Status = "Success", Comments = String.Format("App:{0}",appName)});										
		}
		else
		{
			FileAppendText(logFile, String.Format("{0}############## Copying {2} Files To Remote Server[{1}] Application Directory Not Successful  ##############{0}",Environment.NewLine, remoteServerName,appName));			   
			ConsoleWriteError(String.Format("{0}############## Copying {2} Files To Remote Server[{1}] Application Directory Not Successful  ##############{0}",Environment.NewLine, remoteServerName,appName));
			contentDeployStatus.Add(new DeployStatus { Server = remoteServerName , Task = "Publish Artifacts", Status = "Failed", Comments = String.Format("App:{0}",appName)});										
		}
		
		RunProcess(deleteCredArgs);	
	}
    catch (Exception e)
    {
		contentDeployStatus.Add(new DeployStatus { Server = remoteServerName , Task = "Publish WebConfig", Status = "Failed", Comments = e.Message});										
		FileAppendText(logFile, String.Format("{0}############## Copying {2} Files To Remote Server[{1}] Application Directory Not Successful ##############{0}",Environment.NewLine, remoteServerName,appName));			   
		ConsoleWriteError(String.Format("{0}############## Copying {2} Files To Remote Server[{1}] Application Directory Not Successful ##############{0}",Environment.NewLine, remoteServerName,appName));
		throw new Exception(String.Format("{0}Copying {2} Files To Remote Server[{1}] Application Directory Failed With error message: {3}   ##############{0}",Environment.NewLine, remoteServerName,appName,e.Message));
	}
		
}

#endregion

#region Window service operations
public void WinService(String serviceName ,String serviceAction,String remoteServer, String remoteUser ,String remotePass)
{
	try
	{
	    var connectoptions = new ConnectionOptions();
        connectoptions.Username = remoteUser;
        connectoptions.Password = remotePass;

        uint processId = 0;
        string processStatus = "" ;
        string processState = "";
       
        ManagementScope scope = new ManagementScope(@"\\" + remoteServer + @"\root\cimv2", connectoptions);

        // WMI query
        var query = new SelectQuery("SELECT * FROM WIN32_SERVICE WHERE NAME = '" + serviceName + "'");

        using (var searcher = new ManagementObjectSearcher(scope, query))
        {

			foreach (System.Management.ManagementObject mngntObj in searcher.Get())
            {
                processId = (uint)mngntObj["PROCESSID"];
                processStatus = (string)mngntObj["STATUS"];
                processState = (string)mngntObj["STATE"];

            }
		}
		if(processState.ToUpper() == "RUNNING" && serviceAction == "START")
		{
			return;
		}
		if(processState.ToUpper() == "STOPPED" && serviceAction == "STOP")
		{
			return;
		}

		if(processStatus.ToUpper() == "OK" )
		{
			Console.WriteLine("");
			Console.WriteLine("");
			Console.ForegroundColor = ConsoleColor.Cyan;
			Console.WriteLine(String.Format("{2}ing service {1} on remote server {0}....",remoteServer,serviceName,serviceAction));
			Console.ResetColor();
			Console.WriteLine("");
			
			using(Process serviceOps = new Process())
			{
				serviceOps.StartInfo.FileName = "SC.exe";
				serviceOps.StartInfo.WorkingDirectory = Environment.SystemDirectory;
				serviceOps.StartInfo.Arguments = String.Format(@"\\{0} {1} {2}",remoteServer,serviceAction,serviceName);
				var username = remoteUser.Split(@"\".ToCharArray());
				serviceOps.StartInfo.Domain = username.Length>1?username[0]:"OHL";
				serviceOps.StartInfo.UserName = username.Length>1?username[1]:username[0];
				serviceOps.StartInfo.PasswordInClearText = remotePass;
				serviceOps.StartInfo.UseShellExecute = false;
				serviceOps.StartInfo.CreateNoWindow = true;
				var result = serviceOps.Start();  
				serviceOps.WaitForExit();
				if (result && serviceOps.ExitCode == 0)
				{
					FileAppendText(logFile, String.Format("{0}############## {3} Service {2} completed On Remote Server [{1}]. ##############{0}",Environment.NewLine ,remoteServer,serviceAction,serviceName));			   
					ConsoleWriteInfo(String.Format("{0}############## {3} Service {2} completed On Remote Server [{1}]. ##############{0}",Environment.NewLine ,remoteServer,serviceAction,serviceName));
				}
				else
				{
					FileAppendText(logFile, String.Format("{0}############## {3} Service {2} On Remote Server [{1}] Not Successful with exit code {4}. ##############{0}",Environment.NewLine ,remoteServer,serviceAction,serviceName, serviceOps.ExitCode ));			   
					Console.WriteLine(String.Format("{0}############## {3} Service {2} On Remote Server [{1}] Not Successful with exit code {4}. ##############{0}",Environment.NewLine,remoteServer,serviceAction,serviceName, serviceOps.ExitCode));
					throw new Exception(String.Format("{0}############## {3} Service {2} On Remote Server [{1}] Not Successful with exit code {4}. ##############{0}",Environment.NewLine,remoteServer,serviceAction,serviceName, serviceOps.ExitCode ));
				}
			}		
		}
		else
		{
			Console.WriteLine(String.Format("The service {0} not running on remote server {1} [service status : {2}  state:{3} id: {4}]", serviceName,remoteServer,processStatus,processState,processId));
		}
	}
	catch { throw; }
  }
  
  #endregion
  

