//********************* HOTFIX PRE-DEPLOYMENT PROCESSS *********************
#region HOTFIX PRE-DEPLOYMENT
	
// Git Hotfix process
Task("Git-Uat-Hotfix-Process")
	.Does(() => 
	{
		Console.WriteLine("");
		Console.WriteLine("started hotfix deployment git process....");		
        Console.WriteLine("");

		List<string> listBranches = new List<string>();
		
		var newVersionNumber="";
		var newBuildNumber=0;
		var newVersionMinor=0; 
		var rcTag = "";	   
		bool copySqlFiles = false;
		var releaseBranchVersion="";
		
		Console.WriteLine("");
		Console.WriteLine("Checking for existing hotfix branch....");		
        Console.WriteLine("");
		
		using (var repo = new Repository(localRepo))
		{
			Remote remote = repo.Network.Remotes["origin"];	
			
			foreach(Branch b in repo.Branches.Where(b => !b.IsRemote))
			{
				listBranches.Add(b.FriendlyName);
			}
				
			var releaseBranches =listBranches.Where(s => s.Contains("hotfix/"));
				
			if (releaseBranches.Count() > 1) 
			{				
				ConsoleWriteError("############### FOUND MORE THAN ONE HOTFIX BRANCH ###############");
				throw new Exception("FOUND MORE THAN ONE HOTFIX BRANCH");
			}				
					
			var releaseBranch =releaseBranches.FirstOrDefault();
										
			if (releaseBranch != null )
			{			  
				if(releaseBranch.Trim() != "" && releaseBranch.Contains("hotfix/"))
				{							
				  	newVersionNumber = assemblyVersion;				  
				  	subVersion += 1;				  
				  	rcTag = String.Format("v{0}-rc.{1}",newVersionNumber,subVersion);
				}  
			}
			else
			{  
				var newHotFixVersions =  assemblyVersion.Split('.').ToArray();			   
			    copySqlFiles = true;
				  
				newBuildNumber = Convert.ToInt32(newHotFixVersions[2])+1;
				newVersionNumber = String.Format("{0}.{1}.{2}",newHotFixVersions[0],newHotFixVersions[1],newBuildNumber);
						
				subVersion=0;
				assemblyVersion=newVersionNumber;
								
				rcTag = String.Format("v{0}-rc.{1}",newVersionNumber,subVersion);
				repo.CreateBranch(String.Format("hotfix/v{0}",newVersionNumber));	
				
				Console.WriteLine("");
				Console.WriteLine(String.Format("Creating a new {0} branch....",String.Format("hotfix/v{0}",newVersionNumber)));		
				Console.WriteLine("");	
			}
												
			LibGit2Sharp.PushOptions pushOptions = new LibGit2Sharp.PushOptions();
					pushOptions.CredentialsProvider = new CredentialsHandler(
							(url, usernameFromUrl, types) =>
								new UsernamePasswordCredentials()
								{
									Username = gitUser,
									Password = gitPassword
								});

			Console.WriteLine("");
			Console.WriteLine(String.Format("Checking out {0} branch....",String.Format("hotfix/v{0}",newVersionNumber)));		
			Console.WriteLine("");

			var branch = repo.Branches[String.Format("hotfix/v{0}",newVersionNumber)];
			Branch currentBranch = Commands.Checkout(repo , branch);
			
			repo.Branches.Update(branch,
				b => b.Remote = remote.Name,
				b => b.UpstreamBranch = branch.CanonicalName);
		
			repo.Network.Push(branch, pushOptions);
			
			//fetch files from git			
			FetchOptions fetchoptions = new FetchOptions();
			fetchoptions.CredentialsProvider = new CredentialsHandler((furl, usernameFromUrl, types) => 
			new UsernamePasswordCredentials() 
			{
				Username = gitUser,
				Password = gitPassword
			});
				
			foreach (Remote fremote in repo.Network.Remotes)
			{
				IEnumerable<string> refSpecs = fremote.FetchRefSpecs.Select(x => x.Specification);
				Commands.Fetch(repo, fremote.Name, refSpecs, fetchoptions, "");
			}
			
			//create assembly file and push it 
			CreateAssemblyInfoFile();					
					
			//Copy Release Management file from QA to UAT 
			if(copySqlFiles && IncludeSqlTasks)
			{
				Console.WriteLine("");
				Console.WriteLine("Copying release management SQL files from QA to UAT....");		
				Console.WriteLine("");

				foreach(var sqlSchema in sqlSchemas)
				{
					var SqlScrtiptsDir =String.Format(@"{0}\sql\{1}\{2}",localRepo,sqlSchema,sqlReleaseManagementDir);
					var SQL_files = GetFiles(System.IO.Path.Combine(SqlScrtiptsDir,"Qa", "*.*")); 
					foreach(var file in SQL_files)
					{
						var filenameSQL = System.IO.Path.GetFileName(file.FullPath);
						var destSQLFilePath = System.IO.Path.Combine(SqlScrtiptsDir,buildMode,filenameSQL);
						
						if(filenameSQL.ToUpper().StartsWith("V") && !FileExists(destSQLFilePath))
						{
							System.IO.File.Copy(file.FullPath,destSQLFilePath);	
						}				
					}
				}						
			}	
			
			//add assembly file to repo 
			repo.Index.Add(assemblyInfoFile);
			
			// generate sql script merge files
			if(IncludeSqlTasks)
			{
				GenerateSQLMigrationScriptsFromGitDiffResults(); 
			}
			
			var readMeFilePath = System.IO.Path.Combine(localRepo,String.Format("ReadMe_{0}.md",version));

			//create a readme markdown file with commit messages from Head to previous tag
			GitCommitMessagesFromHeadToTag(readMeFilePath);  
			
			Console.WriteLine("");
			Console.WriteLine("Updating version number in configuration file....");		
        	Console.WriteLine("");

			//update version in config file
			string json = System.IO.File.ReadAllText(configFilePath);
			dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
				
			jsonObj["version"]= rcTag;
				
			string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj,Newtonsoft.Json.Formatting.Indented);

			System.IO.File.WriteAllText(configFilePath,output);		
			
			Console.WriteLine("");
			Console.WriteLine("Staging all the changes....");		
        	Console.WriteLine("");
			
		    //stage all the files	
			Commands.Stage(repo, "*");			
					   
			//Create the committer's signature and commit
			Signature author = new Signature(gitUser, String.Format("@{0}",gitUser), DateTime.Now);
			Signature committer = author;
			
			if (String.IsNullOrEmpty(commitMessage.Trim())) 
			{
				commitMessage = "UAT hotfix Automated Build";
			}
			
			//Commit to the repository
			repo.Commit(commitMessage, author, committer);
			
			Console.WriteLine("");
			Console.WriteLine(String.Format("Applying tag {0}....",rcTag));		
        	Console.WriteLine("");

			//Tag the branch 	
			Tag t = repo.ApplyTag(rcTag);
			
			//push tag
			repo.Network.Push(repo.Network.Remotes["origin"],String.Format("+refs/tags/{0}",rcTag), pushOptions);
			
			Console.WriteLine("");
			Console.WriteLine("pushing all the changes to remote....");		
        	Console.WriteLine("");

			//Push Changes to remote
			repo.Network.Push(branch, pushOptions);									
		}
						
		FileAppendText(logFile, String.Format("{0}############## Git Tag/Stage/Commit/Push Completed. ##############{0}",Environment.NewLine));		
		ConsoleWriteInfo(String.Format("{0}############## Git Tag/Stage/Commit/Push Completed. ##############{0}",Environment.NewLine)); 
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}Git-Uat-Hotfix-Process Error : {1} {0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Git-Uat-Hotfix-Process ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		throw exception;
	});

	 
Task("Git-Pull-Uat-Hotfix")
	.Does(() => 
	{		
		Console.WriteLine("");
		Console.WriteLine("");
		Console.WriteLine("pulling code to local machine....");
		Console.WriteLine("");
		
		branchName = "master";
			
		List<string> listhotfixBranches = new List<string>();
		
		using (var repo = new Repository(localRepo))
		{		
			Remote remote = repo.Network.Remotes["origin"];	
						
			foreach(Branch b in repo.Branches.Where(b => !b.IsRemote))
			{
				listhotfixBranches.Add(b.FriendlyName);
			}
					
			var hotfixBranches =listhotfixBranches.Where(s => s.Contains("hotfix/"));
					
			if(hotfixBranches.Any())
			{
				if (hotfixBranches.Count() > 1)
				{
					Console.ForegroundColor = ConsoleColor.Red;
					Console.WriteLine("############### FOUND MORE THAN ONE RELEASE BRANCH ###############");
					Console.ResetColor();
					throw new Exception("FOUND MORE THAN ONE RELEASE BRANCH");
				}
						
				if(hotfixBranches.First().ToString().Contains("hotfix/"))
				{
					branchName = hotfixBranches.First();
				}
						
				LibGit2Sharp.PushOptions pushOptions = new LibGit2Sharp.PushOptions();
						pushOptions.CredentialsProvider = new CredentialsHandler(
								(url, usernameFromUrl, types) =>
									new UsernamePasswordCredentials()
									{
										Username = gitUser,
										Password = gitPassword
									});

				var branch = repo.Branches[branchName];
				Branch currentBranch = Commands.Checkout(repo , branch);
					
				repo.Branches.Update(branch,
						b => b.Remote = remote.Name,
						b => b.UpstreamBranch = branch.CanonicalName);
				
			}
		}
		using (var repo = new Repository(localRepo))
		{
			var head = repo.Branches.Single (branch => branch.FriendlyName == branchName);
			var checkoutOptions = new CheckoutOptions ();
						checkoutOptions.CheckoutModifiers = CheckoutModifiers.Force;
						repo.Checkout(head, checkoutOptions);
						
			LibGit2Sharp.PullOptions options = new LibGit2Sharp.PullOptions();
						options.FetchOptions = new FetchOptions();
						options.FetchOptions.CredentialsProvider = new CredentialsHandler(
							(url, usernameFromUrl, types) =>
								new UsernamePasswordCredentials()
								{
									Username = gitUser,
									Password = gitPassword
								});
											
			repo.Network.Pull(new LibGit2Sharp.Signature(gitUser, gitUser, new DateTimeOffset(DateTime.Now)), options);
		}
				 
		FileAppendText(logFile, String.Format("{0}############## Git Pull Completed ##############{0}",Environment.NewLine));				   
		ConsoleWriteInfo( String.Format("{0}############## Git Pull Completed ##############{0}",Environment.NewLine));
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}Git-Pull-Uat-Hotfix ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Git-Pull-Uat-Hotfix ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		throw exception;
	});			

	#endregion
	//**************************************************************************
