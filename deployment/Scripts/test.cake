Task("RunTests")
    .WithCriteria(() => (SkipTask != "Skip-Test" && IncludeTestTasks))
    .Does(() =>
    {
        Information("Start Running Tests");
        XUnit2( sourceTestFile,
        new XUnit2Settings {
            Parallelism = ParallelismOption.All,
            HtmlReport = true,
            NoAppDomain = false,
            XmlReport = true,
                OutputDirectory = TestResultsDir
        });
    }).OnError(exception =>
    {
        FileAppendText(logFile, String.Format("{0} Build failed @ RunTests task: {1}{0}",Environment.NewLine, new CakeException()));
        ConsoleWriteError(String.Format("{0} Build failed @ RunTests task: {0}{1}{0}",Environment.NewLine, new CakeException()));
        throw new Exception("Build failed!");
    }); 