//********************* UAT PRE-DEPLOYMENT PROCESSS ************************
#region UAT PRE-DEPLOYMENT
			
// Git UAT process
Task("Git-Uat-Process")
	.Does(() => 
	{	
	    Console.WriteLine("");
		Console.WriteLine("started uat deployment git process....");		
        Console.WriteLine("");

		List<string> listBranches = new List<string>();
		var newVersionNumber="";
		var newVersionMajor=0;
		var newVersionMinor=0; 
		var rcTag = "";	   
		bool copySqlFiles = false;
		var releaseBranchVersion="";
		
		Console.WriteLine("");
		Console.WriteLine("Checking for existing release branch....");		
        Console.WriteLine("");	

		using (var repo = new Repository(localRepo))
		{
			Remote remote = repo.Network.Remotes["origin"];	
				
			foreach(Branch b in repo.Branches.Where(b => !b.IsRemote))
			{
				listBranches.Add(b.FriendlyName);
			}
					
			var releaseBranches =listBranches.Where(s => s.Contains("release/"));
					
			if (releaseBranches.Count() > 1) 
			{
				ConsoleWriteError("############### FOUND MORE THAN ONE RELEASE BRANCH ###############");					
				throw new Exception("FOUND MORE THAN ONE RELEASE BRANCH");
			}				
						
			var releaseBranch =releaseBranches.FirstOrDefault();
											
			if (releaseBranch != null )
			{			  
				if(releaseBranch.Trim() != "" && releaseBranch.Contains("release/"))
				{							
					newVersionNumber = assemblyVersion; 
					subVersion += 1;
					rcTag = String.Format("v{0}-rc.{1}",newVersionNumber,subVersion);
					
				}  
			}
			else
			{  
				// Select release version type
				Console.ForegroundColor = ConsoleColor.DarkCyan;
				Console.WriteLine(String.Format("{0}{0}Please select the release version type ?{0}",Environment.NewLine));
				Console.ResetColor();
					
				var menuRelease = new EasyConsole.Menu();
				menuRelease.Add("major",() =>{
											Console.WriteLine(String.Format("{0} Selected.","major"));
											releaseType = "major";
										})
							.Add("minor",() =>{
											Console.WriteLine(String.Format("{0} Selected.","minor"));
											releaseType = "minor";
										});
						
				menuRelease.Display();

				var newReleaseVersions =  assemblyVersion.Split('.').ToArray();			   
					
				copySqlFiles = true;
					
				switch(releaseType)
				{
					case "minor":
						newVersionNumber = assemblyVersion;  						
						repo.CreateBranch(String.Format("release/v{0}",newVersionNumber));						
						subVersion=0;						
						rcTag = String.Format("v{0}-rc.{1}",newVersionNumber,subVersion);						
						break;
						
					case "major":										  
						newVersionMajor = Convert.ToInt32(newReleaseVersions[0])+1;
						newVersionNumber = String.Format("{0}.0.0",newVersionMajor);						
						subVersion=0;
						assemblyVersion=newVersionNumber;
								
						rcTag = String.Format("v{0}-rc.{1}",newVersionNumber,subVersion);						
						repo.CreateBranch(String.Format("release/v{0}",newVersionNumber));						
						break;
						
					default:
						newVersionNumber = releaseBranch.Replace("release/","").Substring(1);
						break;
				}
						
			}
			//Protect development branch
			ProtectBranch(ConfigSettings.developmentBranchName);			
										
			LibGit2Sharp.PushOptions pushOptions = new LibGit2Sharp.PushOptions();
						pushOptions.CredentialsProvider = new CredentialsHandler(
								(url, usernameFromUrl, types) =>
									new UsernamePasswordCredentials()
									{
										Username = gitUser,
										Password = gitPassword
									});

			Console.WriteLine("");
			Console.WriteLine(String.Format("Checking out {0} branch....",String.Format("release/v{0}",newVersionNumber)));		
			Console.WriteLine("");

			var branch = repo.Branches[String.Format("release/v{0}",newVersionNumber)];
			Branch currentBranch = Commands.Checkout(repo , branch);
				
			repo.Branches.Update(branch,
					b => b.Remote = remote.Name,
					b => b.UpstreamBranch = branch.CanonicalName);
			
			repo.Network.Push(branch, pushOptions);
				
			//fetch files from git			
			FetchOptions fetchoptions = new FetchOptions();
			fetchoptions.CredentialsProvider = new CredentialsHandler((furl, usernameFromUrl, types) => 
					new UsernamePasswordCredentials() 
					{
						Username = gitUser,
						Password = gitPassword
					});
					
			foreach (Remote fremote in repo.Network.Remotes)
			{
				IEnumerable<string> refSpecs = fremote.FetchRefSpecs.Select(x => x.Specification);
				Commands.Fetch(repo, fremote.Name, refSpecs, fetchoptions, "");
			}
				
			//create assembly file and push it 
			CreateAssemblyInfoFile();
							
			//Copy Release Management file from QA to UAT
			if(copySqlFiles && IncludeSqlTasks)
			{		
				Console.WriteLine("");
				Console.WriteLine("Copying release management SQL files from QA to UAT....");		
				Console.WriteLine("");

				foreach(var sqlSchema in sqlSchemas )
				{
					var SqlScrtiptsDir =String.Format(@"{0}\sql\{1}\{2}",localRepo,sqlSchema,sqlReleaseManagementDir);
					var SQL_files = GetFiles(System.IO.Path.Combine(SqlScrtiptsDir,"Qa", "*.*")); 
					foreach(var file in SQL_files)
					{
						var filenameSQL = System.IO.Path.GetFileName(file.FullPath);
						var destSQLFilePath = System.IO.Path.Combine(SqlScrtiptsDir,buildMode,filenameSQL);
						if(filenameSQL.ToUpper().StartsWith("V") && !FileExists(destSQLFilePath) )
						{
							System.IO.File.Copy(file.FullPath,destSQLFilePath);	
						}				
					}
				}								
				
			}
			// generate sql script merge files			
			if(IncludeSqlTasks)
			{
				GenerateSQLMigrationScriptsFromGitDiffResults(); 
			}
				
			var readMeFilePath = System.IO.Path.Combine(localRepo,String.Format("ReadMe_{0}.md",version));

			//create a readme markdown file with commit messages from Head to previous tag
			GitCommitMessagesFromHeadToTag(readMeFilePath);    
			
			Console.WriteLine("");
			Console.WriteLine("Updating version number in configuration file....");		
        	Console.WriteLine("");
				
			//update version in config file
			string json = System.IO.File.ReadAllText(configFilePath);
			dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
					
			jsonObj["version"]= rcTag;
					
			string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj,Newtonsoft.Json.Formatting.Indented);

			System.IO.File.WriteAllText(configFilePath,output);					
				
				//stage all the files	
			Commands.Stage(repo, "*");			
						
			//Create the committer's signature and commit
			Signature author = new Signature(gitUser, String.Format("@{0}",gitUser), DateTime.Now);
			Signature committer = author;
				
			if (String.IsNullOrEmpty(commitMessage.Trim())) 
			{
				commitMessage = "UAT Automated Build";
			}
				
			//Commit to the repository
			repo.Commit(commitMessage, author, committer);
			
			Console.WriteLine("");
			Console.WriteLine(String.Format("Applying tag {0}....",rcTag));		
        	Console.WriteLine("");	

			//Tag the branch 	
			Tag t = repo.ApplyTag(rcTag);
				
			//push tag
			repo.Network.Push(repo.Network.Remotes["origin"],String.Format("+refs/tags/{0}",rcTag), pushOptions);

			Console.WriteLine("");
			Console.WriteLine("pushing all the changes to remote....");		
        	Console.WriteLine("");
				
			//Push Changes to remote
			repo.Network.Push(branch, pushOptions);
										
		}
			
		FileAppendText(logFile, String.Format("{0}############## Git Tag/Stage/Commit/Push Completed. ##############{0}",Environment.NewLine));		
		ConsoleWriteInfo(String.Format("{0}############## Git Tag/Stage/Commit/Push Completed. ##############{0}",Environment.NewLine)); 
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}Git-Uat-Process Error : {1} {0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Git-Uat-Process ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		throw exception;
	});	

// Pull code from git repo			 
Task("Git-Pull-Uat")
	.Does(() => 
	{	
		Console.WriteLine("");
		Console.WriteLine("pulling code to local machine....");
		Console.WriteLine("");
				     
		List<string> listReleaseBranches = new List<string>();
			
		using (var repo = new Repository(localRepo))
		{			
			Remote remote = repo.Network.Remotes["origin"];	
						
			foreach(Branch b in repo.Branches.Where(b => !b.IsRemote))
			{
				listReleaseBranches.Add(b.FriendlyName);
			}
				
			var releaseBranches =listReleaseBranches.Where(s => s.Contains("release/"));
				
			if(releaseBranches.Any())
			{
				if (releaseBranches.Count() > 1)
				{
					ConsoleWriteError("############### FOUND MORE THAN ONE RELEASE BRANCH ###############");
					throw new Exception("FOUND MORE THAN ONE RELEASE BRANCH");
				}					
				if(releaseBranches.First().ToString().Contains("release/"))
				{
					branchName = releaseBranches.First();
				}
					
				LibGit2Sharp.PushOptions pushOptions = new LibGit2Sharp.PushOptions();
						pushOptions.CredentialsProvider = new CredentialsHandler(
								(url, usernameFromUrl, types) =>
									new UsernamePasswordCredentials()
									{
										Username = gitUser,
										Password = gitPassword
									});

				var branch = repo.Branches[branchName];
				Branch currentBranch = Commands.Checkout(repo , branch);
				
				repo.Branches.Update(branch,
					b => b.Remote = remote.Name,
					b => b.UpstreamBranch = branch.CanonicalName);
			
			}
		}
			
		using (var repo = new Repository(localRepo))
		{
			var head = repo.Branches.Single (branch => branch.FriendlyName == branchName);
			var checkoutOptions = new CheckoutOptions ();
			checkoutOptions.CheckoutModifiers = CheckoutModifiers.Force;
			repo.Checkout(head, checkoutOptions);
					
			LibGit2Sharp.PullOptions options = new LibGit2Sharp.PullOptions();
			options.FetchOptions = new FetchOptions();
			options.FetchOptions.CredentialsProvider = new CredentialsHandler(
						(url, usernameFromUrl, types) =>
							  new UsernamePasswordCredentials()
							  {
								  Username = gitUser,
								  Password = gitPassword
							  });
										
					 repo.Network.Pull(new LibGit2Sharp.Signature(gitUser, gitUser, new DateTimeOffset(DateTime.Now)), options);
		}
				 
		FileAppendText(logFile, String.Format("{0}############## Git Pull Completed ##############{0}",Environment.NewLine));				   
		ConsoleWriteInfo( String.Format("{0}############## Git Pull Completed ##############{0}",Environment.NewLine));
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}Git-Pull-Uat ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Git-Pull-Uat ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		throw exception;
	});	

		
	#endregion 
//**************************************************************************