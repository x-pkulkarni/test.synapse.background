Task("ExecutePublish")
    .Does(() =>
    { 
        Console.WriteLine("");
		Console.WriteLine("running publish process ....");		
        Console.WriteLine("");

        var options = new ParallelOptions {
                                    MaxDegreeOfParallelism = maxDegreeOfParallelism,
									CancellationToken = cancellationToken
                                };

        switch(buildMode.ToUpper())
        {
                case "QA":
                selecetdAppEnvironment = ConfigSettings.Environments.Qa;
                break;
                
                case "UAT":
                selecetdAppEnvironment = ConfigSettings.Environments.Uat;
                break;
                
                case "PROD":
                selecetdAppEnvironment = ConfigSettings.Environments.Prod;
                break;
        }

        remoteServerArchive  = ConfigSettings.remoteServerArchive;
        ArchiveExcludeDir  = ConfigSettings.ArchiveExcludeDir;
        
        var serverList = new List<RemoteServer>();
        var selectedServerList = new List<string>();
        var allServerList = new List<RemoteServer>();
		var allPDCServerList = new List<RemoteServer>();
		var allFDCServerList = new List<RemoteServer>();
		
		var appConfigPathPDC = new Dictionary<string, string>();
		var appConfigPathFDC = new Dictionary<string, string>();
		
        if(selecetdAppEnvironment.FDC != null && selecetdAppEnvironment.FDC.RemoteServerList != null){
            allFDCServerList = allServerList.Union(selecetdAppEnvironment.FDC.RemoteServerList).ToList();
			appConfigPathFDC = selecetdAppEnvironment.FDC.AppConfigPath; 
        }

        if(selecetdAppEnvironment.PDC != null && selecetdAppEnvironment.PDC.RemoteServerList != null){
            allPDCServerList = allServerList.Union(selecetdAppEnvironment.PDC.RemoteServerList).ToList();
			appConfigPathPDC = selecetdAppEnvironment.PDC.AppConfigPath; 
        }
        
        int servCount = 0;
        var strServers ="";
        var dictServ = new Dictionary<int,string>();
        var menuServers = new EasyConsole.Menu();

        while(String.IsNullOrEmpty(strServers))
            {   
                Console.WriteLine("");     
                ConsoleWriteCustom("Please enter number separated by comma(,) to select multiple servers. [eg: 1,3,5]",ConsoleColor.DarkCyan);
				Console.WriteLine("1. All PDC Servers");
				Console.WriteLine("2. All FDC Servers");              

                Console.Write("Choose Servers: ");
                strServers= Console.ReadLine();
            }			
                
        foreach(var num in strServers.Split(',').ToList())
        {   
			switch (num)
			{
				case "1" :  {
					serverList = serverList.Union(selecetdAppEnvironment.PDC.RemoteServerList).ToList();
					break;
				}
				case "2" :  {
				 serverList=serverList.Union(selecetdAppEnvironment.FDC.RemoteServerList).ToList();
					break;
				}
			}           
        }         
         
                     
         var actionsStop = new List<Action>();
         var actionsBackUp = new List<Action>();
         var actionsPublish = new List<Action>();
     
        foreach(var remoteServer in serverList)
		{	            
			ConsoleWriteCustom(String.Format("############## Publish Process Started For Remote Server {0} ##############",remoteServer.Name),ConsoleColor.DarkMagenta);
			
			 //Enter remote server login credentials
            while (String.IsNullOrEmpty(remoteServerUserName.Trim())) 
            {
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                Console.WriteLine(String.Format(@"{0}Please enter remote server user-name:{0}",Environment.NewLine));
                Console.ResetColor();

                var user = Console.ReadLine();

                if (!String.IsNullOrEmpty(user) && !user.Contains(@"\"))
                    {
                        remoteServerUserName = String.Format(@"{0}\{1}","OHL", user); 
                    }
                    else if(!String.IsNullOrEmpty(user) && user.Contains(@"\") )
                    {
                        remoteServerUserName = user;
                    }		
            }
                
            while (String.IsNullOrEmpty(remoteServerPass.Trim())) 
            {
                   remoteServerPass = MaskPassword("remote server password");
            }  

			bool deployAll = false;
			var app = "";
            if(selectedPublishApps == "All")
			{
				deployAll = true;
			}  
			else
			{
				app = appsToBeDeployed
                            .Where(s => s.Name == selectedPublishApps)
                            .Select(s => s.Name)
                            .FirstOrDefault(); 
			}
			
			if(deployAll)
			{
				foreach(var application in appsToBeDeployed)
				{
					actionsStop.Add(() =>                       
					WinService(application.Name ,"STOP", remoteServer.Name, remoteServerUserName ,remoteServerPass)
					); 
				}
			}
			else
			{
				 actionsStop.Add(() =>                       
					WinService(app ,"STOP", remoteServer.Name, remoteServerUserName ,remoteServerPass)
					); 
			}
            
            if(remoteServer.IsArchive)
            {
                actionsBackUp.Add(() => 
                    BackUpFiles(zipUtilityPath, remoteServerUserName, remoteServerPass, remoteServer.Name,remoteServer.AppPool,remoteServerArchive,remoteServer.AppPath, ArchiveExcludeDir)
                );
            }

			if(deployAll)
			{
				foreach(var application in appsToBeDeployed)
				{
					if(strServers == "1")
					{
						actionsPublish.Add(() => 
						PublishArtifacts(remoteServerUserName, remoteServerPass, remoteServer.Name, application.Name, remoteServer.AppPath, appConfigPathPDC[application.Name])
						);
					}
					else if(strServers == "2")
					{
						actionsPublish.Add(() => 
						PublishArtifacts(remoteServerUserName, remoteServerPass, remoteServer.Name, application.Name, remoteServer.AppPath, appConfigPathFDC[application.Name])
						);
					}
					      
				}
			}
			else
			{
				if(strServers == "1")
				{
					actionsPublish.Add(() => 
					PublishArtifacts(remoteServerUserName, remoteServerPass, remoteServer.Name, app, remoteServer.AppPath, appConfigPathPDC[app])
					);
				}
				else if(strServers == "2")
				{
					actionsPublish.Add(() => 
					PublishArtifacts(remoteServerUserName, remoteServerPass, remoteServer.Name, app, remoteServer.AppPath, appConfigPathFDC[app])
					);
				}
			}
        } 
        Parallel.Invoke(options, actionsPublish.ToArray());                 

    }).OnError(exception =>
    {
	    FileAppendText(logFile, String.Format("{0} Build failed @ RunTests task: {1}{0}",Environment.NewLine, exception.Message));
	    ConsoleWriteError(String.Format("{0} Build failed @ RunTests task: {0}{1}{0}",Environment.NewLine, exception.Message));
        throw exception;
    }); 

    

Task("FlyWay")
	.Does(() =>
		{
            #break
			if(IncludeSqlTasks){
                foreach(var sqlSchema in sqlSchemas )
                {
                    if(SelectedEnvironmentDetails.schemadetails != null && SelectedEnvironmentDetails.schemadetails.Any(s=> s.name == sqlSchema))
                    {
                        var item = SelectedEnvironmentDetails.schemadetails.Where(s=>s.name == sqlSchema).First();
                        ExecuteFlyWay(sqlSchema, item.details.servicename, item.details.username);
                    }
                }
            }
		})
	.OnError(exception =>
	{
        ConsoleWriteError("Error in flyway");
		throw exception;
	});

    
Task("Restart")
	.DoesForEach(ReadPublishConfig(selectedPublishApps),(selectedApp) =>
    {
        var isIISHosted = string.Equals(selectedApp.AppType,"IISHosted", StringComparison.OrdinalIgnoreCase);
        var serviceName = selectedApp.ServiceName;
        var serverList = new List<RemoteServer>();
        if(selecetdAppEnvironment.FDC != null && selecetdAppEnvironment.FDC.RemoteServerList != null){
            serverList = serverList.Union(selecetdAppEnvironment.FDC.RemoteServerList).ToList();
        }
        if(selecetdAppEnvironment.PDC != null && selecetdAppEnvironment.PDC.RemoteServerList != null){
            serverList = serverList.Union(selecetdAppEnvironment.PDC.RemoteServerList).ToList();
        }
        foreach(var remoteServer in serverList)
		{
            if(!serverData.ContainsKey(selectedApp.appName + remoteServer.Name)) {continue;}
            var serverDataInfo = serverData[selectedApp.appName + remoteServer.Name];
            remoteServerUserName = serverDataInfo.Item1;
            remoteServerPass = serverDataInfo.Item2;
            var siteName = serverDataInfo.Item3;
            var appPoolName = serverDataInfo.Item4;
            var iisServerLevel = serverDataInfo.Item5;
            if(isIISHosted)
            {
                if(remoteServer.InUse)
                {
                    if(iisServerLevel){
                        IIIStart( remoteServerUserName, remoteServerPass, remoteServer.Name);
                    }
                    else{
                        IIIStartSite(psUtilityPath, remoteServerUserName, remoteServerPass, remoteServer.Name,siteName);
                    }
                }
                else
                {
                    FileAppendText(logFile, String.Format("{0}############## IIS Start Skipped For Remote Server [{1}] ##############{0}",Environment.NewLine ,remoteServer.Name ));			   
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(String.Format("{0}############## IIS Start Skipped For Remote Server [{1}] ##############{0}",Environment.NewLine,remoteServer.Name ));
                    Console.ResetColor();
                }
                if(selecetdAppEnvironment.HealthCheckEnabled)
                {
                    HealthCheckApi(selecetdAppEnvironment.HealthCheckApiUrl, remoteServer.Name, selectedApp.appName);
                }
            }
            else
            {
                if(remoteServer.InUse)
                {
                    WinService(serviceName,"START",remoteServer.Name,remoteServerUserName,remoteServerPass);	
                }
                else
                {
                    FileAppendText(logFile, String.Format("{0}############## {2} Service Start Process Skipped On Remote Server [{1}] ##############{0}",Environment.NewLine, remoteServer.Name, serviceName));			   
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(String.Format("{0}############## {2} Service Start Process Skipped On Remote Server [{1}] ##############{0}",Environment.NewLine, remoteServer.Name, serviceName));
                    Console.ResetColor();
                }
		
            }
        }
            	
    })
	.OnError(exception =>
	{
        ConsoleWriteError("Error in Restart");
		throw exception;
	});
