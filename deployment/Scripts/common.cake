
// Create a log file 
Task("Create-Log-File")
	.Does(() =>
				{
				if (FileExists(logFile) == false)
					{
						FileWriteText(logFile,
									String.Format("Cake Build Automation - Log File - {0}{1}{1}",
									DateTime.Now.ToString(), Environment.NewLine ));
					}
				})
	.OnError(exception =>
				{
			          FileAppendText(logFile, String.Format("{0}Log File Error : {1} {0}",Environment.NewLine,new CakeException()));
					  ConsoleWriteError(String.Format("{0}Log File ERROR :{0}{1}{0}",Environment.NewLine,new CakeException()));
					  throw exception;
				});	
Task("Log-Build-Variables")
	.Does(() =>
		{ 
			FileAppendText(logFile, String.Format("{0}**** Build Mode: {1} **** ",Environment.NewLine, buildModeDescription));
			FileAppendText(logFile, String.Format("{0}**** Build Environment: {1} **** ",Environment.NewLine, buildMode));
			FileAppendText(logFile, String.Format("{0}**** Build Action: {1} **** ",Environment.NewLine, buildAction));	
			FileAppendText(logFile, String.Format("{0}**** Git User: {1} **** {0}{0}",Environment.NewLine, gitUser));				
		})
	.OnError(exception =>
				{
			          FileAppendText(logFile, String.Format("{0}Task 'Log-Build-Variables' Failed! : {1} {0}",Environment.NewLine, new CakeException()));
					  ConsoleWriteError(String.Format("{0}Task 'Log-Build-Variables' Failed! :{0}{1}{0}",Environment.NewLine, new CakeException()));
					  throw exception;
				});	
	//get version from config file
Task("Get-Version-From-Config-File")
	.Does(() =>
	{
		string json = System.IO.File.ReadAllText(configFilePath);
		dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
				
		version = jsonObj["version"];
		tagName = version;
		
        Console.WriteLine("");
		Console.WriteLine("");
		Console.WriteLine("Reading version number from config file....");		
		
		if(version.ToUpper().Contains("QA"))
		{
		  var arrVersion= version.Split('-').ToArray();
		  assemblyVersion= Convert.ToString(arrVersion[0]).ToUpper().Replace("V","");
		  subVersion = Convert.ToInt32(arrVersion[1].ToUpper().Replace("QA.",""));
		}
		else if(version.ToUpper().Contains("RC"))
		{
		  var arrVersion= version.Split('-').ToArray();
		  assemblyVersion= Convert.ToString(arrVersion[0]).ToUpper().Replace("V","");
		  subVersion = Convert.ToInt32(arrVersion[1].ToUpper().Replace("RC.",""));
		}
		else
		{
		  assemblyVersion=version.ToUpper().Replace("V","");
	    }
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}Get-Version-From-Config-File Error : {1} {0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Get-Version-From-Config-File ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		throw exception;
	});

// Clean build directories
Task("Clean-Build-Directories")
	.Does(() =>
	{
		Console.WriteLine("");
		 Console.WriteLine("Cleaning build directories ....");
			
		if (System.IO.Directory.Exists(stagingDir))
		{
			System.IO.DirectoryInfo di = new DirectoryInfo(stagingDir);
                
			foreach (FileInfo file in di.GetFiles())
			{
				file.Delete(); 
			}
					
			foreach (DirectoryInfo dir in di.GetDirectories())
			{
				dir.Delete(true); 
			}
					
			di.Delete(true);
		}
					
		foreach(var app in appsToBeDeployed)
		{
			CleanDirectory(System.IO.Path.Combine(localRepo,app.srcRoot,"bin"));
		}
 
		FileAppendText(logFile, String.Format("{0}############## Build Directories Cleaned ##############{0}",Environment.NewLine));
		ConsoleWriteInfo(String.Format("{0}############## Build Directories Cleaned ##############{0}",Environment.NewLine));
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}Clean-Build-Directories Error : {1}{0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Clean-Build-Directories ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		throw exception;
	});	

// Restore nuget packages
Task("Restore-NuGet-Packages") 
    .Does(() =>
	{ 
		Console.WriteLine("");
		Console.WriteLine("Restoring nuget packages to build the code....");
		Console.WriteLine("");
					 
   		NuGetRestore(solution, new NuGetRestoreSettings { Verbosity = NuGetVerbosity.Quiet }); 
					
 		FileAppendText(logFile, String.Format("{0}############### Restoring Nuget Packages Completed ###############{0}",Environment.NewLine));	
		ConsoleWriteInfo(String.Format("{0}############### Restoring Nuget Packages Completed ###############{0}",Environment.NewLine));
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}Restore-NuGet-Packages Error : {1}{0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Restore-NuGet-Packages ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		throw exception;
	});

// Build 
Task("Build")
    .Does( () =>
	{   
		Console.WriteLine("");
        Console.WriteLine("Building code....");
		Console.WriteLine("");					 
					 
		Cake.Common.Tools.MSBuild.MSBuildToolVersion msBuildToolVersionVS ;
					 
		/*CAKE MS BUILD TOOL VERSION ENUM REFERENCE : https://cakebuild.net/api/Cake.Common.Tools.MSBuild/MSBuildToolVersion/
			Set visualStudioToolVersion in json config file with below number
				VS2005	1	
				VS2008	2	
				VS2010	3	
				VS2011	3	
				VS2012	3	
				VS2013	4	
				VS2015	5	
				VS2017	6	
            */
					
		switch(visualStudioToolVersion)
		{						
            case 6:
				msBuildToolVersionVS =Cake.Common.Tools.MSBuild.MSBuildToolVersion.VS2017;
				break;

			case 7:
				msBuildToolVersionVS =Cake.Common.Tools.MSBuild.MSBuildToolVersion.VS2019;
				break;
						
            default:
				msBuildToolVersionVS =Cake.Common.Tools.MSBuild.MSBuildToolVersion.VS2017;
				break;
		}
					              	
     	MSBuild(solution, new MSBuildSettings {    
     				Verbosity = Verbosity.Quiet,    
     				ToolVersion =  msBuildToolVersionVS,
     				Configuration = "Release",
					NoConsoleLogger = true,
     				PlatformTarget = PlatformTarget.MSIL });
     			    
		FileAppendText(logFile, String.Format("{0}############### Build Completed ###############{0}",Environment.NewLine));
		ConsoleWriteInfo(String.Format("{0}############### Build Completed ###############{0}",Environment.NewLine));
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}MS Build Error : {1}{0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}MS Build ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		throw exception;
	});	

Task("Prepare-Artifacts")
	.Does(() =>
	{
		Console.WriteLine("");
		Console.WriteLine("Copying files to a local deployment folder....");		
        Console.WriteLine("");
				
		var srcAppName="";
				
		// create/clean directories 
		if (!System.IO.Directory.Exists(stagingDir))
			{
				System.IO.Directory.CreateDirectory(stagingDir);
			}
 		else { CleanDirectory(stagingDir); }

		foreach(var app in appsToBeDeployed)
		{
			srcAppName = new DirectoryInfo(app.srcRoot).Name.Replace(".","");
			localDeployDir = System.IO.Path.Combine(stagingDir,srcAppName);
									
			if (!System.IO.Directory.Exists(localDeployDir))
			{
				System.IO.Directory.CreateDirectory(localDeployDir);
			}
			else { CleanDirectory(localDeployDir);}		
		}					
					
		foreach(var app in appsToBeDeployed)
		{
			srcAppName = new DirectoryInfo(app.srcRoot).Name.Replace(".","");
			localDeployDir = System.IO.Path.Combine(stagingDir,srcAppName);
					
			// move files to local deployment folder 					
			foreach (var prodfile in app.prodFiles)
			{
				var prodfileName = System.IO.Path.GetFileName(prodfile);						
				System.IO.File.Copy(System.IO.Path.Combine(localRepo,app.srcRoot,prodfile), 
						System.IO.Path.Combine(localDeployDir,prodfileName));	
			}
                        							
			foreach (var Dir in app.publishDirs)
			{
				if (!System.IO.Directory.Exists(System.IO.Path.Combine(localDeployDir, System.IO.Path.GetDirectoryName(Dir.prodDir))))
				{
					System.IO.Directory.CreateDirectory(System.IO.Path.Combine(localDeployDir, 
							System.IO.Path.GetDirectoryName(Dir.prodDir)));
				}	

				var ignoredExts = new string[] { ".config", ".xml", ".pdb" };
				var files = GetFiles(System.IO.Path.Combine(localRepo,app.srcRoot,Dir.srcDir)).Where(f => !ignoredExts.Contains(f.GetExtension().ToLower()));

				CopyFiles(files,System.IO.Path.Combine(localDeployDir,System.IO.Path.GetDirectoryName(Dir.prodDir)));
			}				
			
			zipFileName = String.Format("{0}_{1}.{2}.{3}_{4}.zip",srcAppName.ToUpper(),environmentVersion,assemblyVersion,subVersion,DateTime.Now.ToString("yyyyMMddHHmmss"));
			
			if (!System.IO.Directory.Exists(System.IO.Path.Combine(stagingDir, "PDC")))
			{
				System.IO.Directory.CreateDirectory(System.IO.Path.Combine(stagingDir, "PDC"));
			}
			//Zip files in a folder to copy over to a PDC remote servers
			Zip(localDeployDir, System.IO.Path.Combine(stagingDir, "PDC", zipFileName));
			
					
			if (!System.IO.Directory.Exists(System.IO.Path.Combine(stagingDir, "FDC")))
			{
				System.IO.Directory.CreateDirectory(System.IO.Path.Combine(stagingDir, "FDC"));
			}
			//Zip files in a folder to copy over to a FDC remote servers
			Zip(localDeployDir, System.IO.Path.Combine(stagingDir, "FDC", zipFileName));
			
					                								
			FileAppendText(logFile, String.Format("{0}############### Created Zip File With Build Artifacts : {1} ###############{0}",Environment.NewLine,  zipFileName));
			ConsoleWriteInfo(String.Format("{0}############### Created Zip File With Build Artifacts: {1} ###############{0}",Environment.NewLine, zipFileName));
		}
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}Prepare-Artifacts Error :{0}{1}{0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Prepare-Artifacts ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		throw exception;
	});

// Copy Zip file to remote server staging directory
Task("Stage-Artifacts")
	.Does(() =>
	{
		if(SelectedEnvironmentDetails.PDC != null && SelectedEnvironmentDetails.PDC.RemoteServerList != null )
		{
			foreach(var remoteServer in SelectedEnvironmentDetails.PDC.RemoteServerList)
			{
				StageArtifacts("PDC",remoteServer,SelectedEnvironmentDetails.PDC.AppConfigPath);
			}
		}
		
		if(SelectedEnvironmentDetails.FDC != null && SelectedEnvironmentDetails.FDC.RemoteServerList != null )
		{
			foreach(var remoteServer in SelectedEnvironmentDetails.FDC.RemoteServerList)
			{
				StageArtifacts("FDC",remoteServer, SelectedEnvironmentDetails.FDC.AppConfigPath);
			}
		}
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}Stage-Artifacts Error :{0}{1}{0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Stage-Artifacts Error :{0}{1}{0}",Environment.NewLine, new CakeException()));
		throw exception;
	});

	Task("Get-Git-Crendentials")
	.Does(() =>
	{
		GetGitCredentials();		
	});

	
