
	var configFilePath = EnvironmentVariable("ConfigFilePath");

	//Read settings from JSON file  
	var ConfigSettings = DeserializeJsonFromFile<ConfigDetails>(configFilePath);
	
	//Get build mode from target parameter passed to script
	string buildMode = "";
	string buildModeDescription = "";
	string buildAction = "";
	var environmentVersion = 1;
	var targetMode  = "";
	var isQA = false;
	var isUAT = false;
	var isUATHotFix = false;
	var isHotFix = false;
	var isProd = false;
	var timeStarted = DateTime.Now;
	var timeStamp = DateTime.Now.ToString("yyyyMMddHHmm");
    var selectedPublishApps = "All";
    EnvironmentDetails selecetdAppEnvironment = null;
    var serverData = new Dictionary<string, Tuple<string,string,string,string,bool>>();
		

	// set variables 
	var localRepo = MakeAbsolute(Directory(ConfigSettings.workingDirectory)).FullPath;
	var buildDefinitionName = ConfigSettings.buildDefinitionName;
	var solution = System.IO.Path.Combine(localRepo,ConfigSettings.solution);
	var gitRepoURL = ConfigSettings.gitRepoURL;
	var appsToBeDeployed = ConfigSettings.appsToBeDeployed;
	var stagingDir= ConfigSettings.stagingDir;
	var localDeployDir = "" ;
	var assemblyInfoFile = ConfigSettings.assemblyInfoFile;
	var version =  ConfigSettings.version;
	var logFile= ConfigSettings.logFile;
	var readMeFile = System.IO.Path.Combine(localRepo,ConfigSettings.readMeFile);
	var company= ConfigSettings.company;
	var product = ConfigSettings.product;
	var visualStudioToolVersion = ConfigSettings.visualStudioToolVersion;
	var gitProtectBranchApiUrl=ConfigSettings.gitProtectBranchApiUrl;
	var gitUnProtectBranchApiUrl = ConfigSettings.gitUnProtectBranchApiUrl;
	var gitProtectBranchApiToken =ConfigSettings.gitProtectBranchApiToken;
	var gitProtectBranchTokenType = "";
	var assemblyVersion = "";
	var zipFileName = "";
	var releaseType = "";
	var tagName = version;
	var serviceName ="";
    //Unit tests
    var sourceTestDir = System.IO.Path.Combine(localRepo,ConfigSettings.sourceTestDir);
    var sourceTestFile = System.IO.Path.Combine(sourceTestDir,ConfigSettings.sourceTestFile);
    var TestResultsDir = System.IO.Path.Combine(stagingDir,  "tests");
    var IncludeSqlTasks = StringComparer.OrdinalIgnoreCase.Equals("Y",ConfigSettings.IncludeSqlTasks);
    var IncludeTestTasks = StringComparer.OrdinalIgnoreCase.Equals("Y",ConfigSettings.IncludeTestTasks);

	var commitMessage = "";
	var currentReleaseBranch="";
	var currentHotfixBranch="";
	var branchName = ConfigSettings.developmentBranchName;
	var subVersion = 0;
	var zipFolderName = "";
	var basePath = System.IO.Directory.GetCurrentDirectory();
	var remoteServerAppPath = "";
	var remoteServerArchive = "";
	List<string> ArchiveExcludeDir = null;
	var remoteServerAppDir  = "";
	var remoteServerPrePublishDir = ConfigSettings.remoteServerPrePublishDir;
	var HealthCheckApiUrl = "";
	var flywayToolVersion = ConfigSettings.flywayToolVersion;
	var zipUtilityPath = System.IO.Path.Combine(MakeAbsolute(Directory(".")).FullPath,ConfigSettings.zipUtility);
	var psUtilityPath = System.IO.Path.Combine(MakeAbsolute(Directory(".")).FullPath,ConfigSettings.psUtility);
	
	var sqlScriptTypes = ConfigSettings.sqlScriptTypes;
	var SqlDataScrtiptExceptionType = ConfigSettings.SqlDataScrtiptExceptionType;
	var sqlSchemas = ConfigSettings.Schema;
    var sqlFileNameLengthLimit = ConfigSettings.SqlFileNameLengthLimit;
    var sqlTypesWithLongerFileName = ConfigSettings.SqlTypeWithLongerFileName;	
	var sqlReleaseManagementDir = ConfigSettings.SqlReleaseManagementDir;	
		
	List<RemoteServer> listRemoteServers = new List<RemoteServer>();
	EnvironmentDetails SelectedEnvironmentDetails = null;
	List<RemoteServer> remoteServers= new List<RemoteServer>();
	List<string> tagList = new List<string>();

	int maxDegreeOfParallelism = -1;
    //##############  Build Automation Process ##############  
    
	Console.WriteLine("");
	Console.ForegroundColor = ConsoleColor.Yellow;
	Console.WriteLine(String.Format("{1}@@@@@@@@@@@@@@@@@ {2} BUILD AUTOMATION PROCESS STARTED : {0} @@@@@@@@@@@@@@@@@{1}",DateTime.Now.ToString(),Environment.NewLine,targetMode.ToUpper()));
	Console.ResetColor();
	FileAppendText(logFile, String.Format("{1}@@@@@@@@@@@@@@@@@ {2} BUILD AUTOMATION PROCESS STARTED : {0} @@@@@@@@@@@@@@@@@{1}",DateTime.Now.ToString(),Environment.NewLine,targetMode.ToUpper()));
  	

    var gitUser = ConfigSettings.gitUser;
	var gitPassword = ConfigSettings.gitPassword;	
	var gitGetTokenUrl = ConfigSettings.gitGetTokenUrl; 
	var gitApiUrl = ConfigSettings.gitApiUrl;
	var vaultApiUrl = ConfigSettings.vaultApiUrl;
	var vaultRoleId = ConfigSettings.vaultRoleId;
	var vaultSecretId = ConfigSettings.vaultSecretId;
	var vaultGitCredentialsKVPath = ConfigSettings.vaultGitCredentialsKVPath;
	String remoteServerUserName = "";
	String remoteServerPass = "";
	var domain = ConfigSettings.domain;
	CancellationToken cancellationToken = default(CancellationToken);
	List<DeployStatus> contentDeployStatus = new List<DeployStatus>();
	var retryCount = ConfigSettings.retryCount;
