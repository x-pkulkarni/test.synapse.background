Task("Main") 
	    .IsDependentOn("Create-Log-File")
	    .IsDependentOn("Log-Build-Variables")
		.IsDependentOn("Get-Git-Crendentials")
	    .Does(() => {
			Console.WriteLine(String.Format("{1}**** Build automation process started : {0} ****{1}",DateTime.Now.ToString(),Environment.NewLine));
			FileAppendText(logFile, String.Format("**** Build automation process started : {0} ****{1}",DateTime.Now.ToString(),Environment.NewLine));
			if(isQA)
			{
				if(buildAction == "STAGE")
					RunTarget("QABuild");
				else if(buildAction == "PUBLISH")
					RunTarget("QAPublish");
			}
			else if(isUAT)
			{
				if(buildAction == "STAGE")
					RunTarget("UATBuild");
				else if(buildAction == "PUBLISH")
					RunTarget("UATPublish");
			}
			else if(isUATHotFix)
			{
				if(buildAction == "STAGE")
					RunTarget("UATHotFixBuild");
				else if(buildAction == "PUBLISH")
					RunTarget("UATHotFixPublish");
			}
			else if(isHotFix)
			{
				if(buildAction == "STAGE")
					RunTarget("PRODHotFixBuild");
				else if(buildAction == "PUBLISH")
					RunTarget("PRODHotFixPublish");
			}
			else if(isProd)
			{
				if(buildAction == "STAGE")
					RunTarget("PRODUCTIONBuild");
				else if(buildAction == "PUBLISH")
					RunTarget("PRODUCTIONPublish");
			}
		})
	.OnError(exception =>
	{
		if(CheckEmailNotifcationEnabled())
		{
			NotifyError( exception, GetEmailRecipients());
		}
	}) 
	.Finally(() =>
	{  
		Console.WriteLine(String.Format("{1}{1}**** {2} Build Automation Process Completed : {0} ****{1}",DateTime.Now.ToString(),Environment.NewLine,buildMode));
		FileAppendText(logFile, String.Format("{1}**** {2} Build Automation Process Completed : {0} ****{1}",DateTime.Now.ToString(),Environment.NewLine,buildMode));
	});


	Task("QABuild")
		.IsDependentOn("Get-Version-From-Config-File")
		.IsDependentOn("Clean-Build-Directories")
		.IsDependentOn("Git-Pull-Qa")
		.IsDependentOn("Restore-NuGet-Packages") 
		.IsDependentOn("Build")
		.IsDependentOn("RunTests")
		.IsDependentOn("Git-Qa-Process")
		.IsDependentOn("Prepare-Artifacts")
		.IsDependentOn("Stage-Artifacts")
    	.WithCriteria(() => CheckEmailNotifcationEnabled())	 
	    .Does(() => 
		{
			NotifyReadMe(readMeFile,"QABuild is completed",GetEmailRecipients());
		});

	Task("QAPublish")
	    .IsDependentOn("ExecutePublish")
	    .IsDependentOn("FlyWay")
	    .IsDependentOn("Restart")
    	.WithCriteria(() => CheckEmailNotifcationEnabled())	 
	    .Does(() => 
		{
			NotifyEventCompletion("","QAPublish is completed",GetEmailRecipients());
		});
	
	
	Task("UATBuild")
		.IsDependentOn("Get-Version-From-Config-File")
		.IsDependentOn("Clean-Build-Directories")
		.IsDependentOn("Git-Pull-Uat")
		.IsDependentOn("Restore-NuGet-Packages") 
		.IsDependentOn("Build")
		.IsDependentOn("RunTests")
		.IsDependentOn("Git-Uat-Process")  
		.IsDependentOn("Prepare-Artifacts")
		.IsDependentOn("Stage-Artifacts") 
	    .Does(() => 
		{
			NotifyReadMe(readMeFile,"UATBuild is completed",GetEmailRecipients());
		});

	Task("UATPublish")
	    .IsDependentOn("ExecutePublish")
	    .IsDependentOn("FlyWay")
	    .IsDependentOn("Restart")
    	.WithCriteria(() => CheckEmailNotifcationEnabled())	 
	    .Does(() => 
		{
			NotifyEventCompletion("","UATPublish is completed",GetEmailRecipients());
		});
	
	Task("UATHotFixBuild")
		.IsDependentOn("Get-Version-From-Config-File")
		 .IsDependentOn("Clean-Build-Directories")
		 .IsDependentOn("Git-Pull-Uat-Hotfix")
		 .IsDependentOn("Restore-NuGet-Packages") 
		 .IsDependentOn("Build")
		 .IsDependentOn("RunTests")
		.IsDependentOn("Git-Uat-Hotfix-Process")  
		.IsDependentOn("Prepare-Artifacts")
		.IsDependentOn("Stage-Artifacts")
	    .Does(() => 
		{
			NotifyReadMe(readMeFile,"UAT HotFix Build is completed",GetEmailRecipients());
		});

	Task("UATHotFixPublish")
	    .IsDependentOn("ExecutePublish")
	    .IsDependentOn("FlyWay")
	    .IsDependentOn("Restart")
    	.WithCriteria(() => CheckEmailNotifcationEnabled())	 
	    .Does(() => 
		{
			NotifyEventCompletion("","HotFix-UATPublish is completed",GetEmailRecipients());
		});

	Task("PRODHotFixBuild")
		.IsDependentOn("Get-Version-From-Config-File")
		.IsDependentOn("Clean-Build-Directories")
		.IsDependentOn("Get-Current-Hotfix-Branch-Prod")
		.IsDependentOn("Git-Pull-Prod-Hotfix")
		.IsDependentOn("Restore-NuGet-Packages") 
		.IsDependentOn("Build")
		.IsDependentOn("RunTests")
		.IsDependentOn("Git-Prod-Hotfix-Process") 
		.IsDependentOn("Prepare-Artifacts")
		.IsDependentOn("Stage-Artifacts")
	    .Does(() => 
		{
			NotifyReadMe(readMeFile,"PROD HotFix Build is completed",GetEmailRecipients());
		});

	Task("PRODHotFixPublish")
	    .IsDependentOn("ExecutePublish")
	    .IsDependentOn("FlyWay")
	    .IsDependentOn("Restart")
    	.WithCriteria(() => CheckEmailNotifcationEnabled())	 
	    .Does(() => 
		{
			NotifyEventCompletion("","HotFix-Production Publish is completed",GetEmailRecipients());
		});
	
	Task("PRODUCTIONBuild")
		.IsDependentOn("Get-Version-From-Config-File")
		.IsDependentOn("Clean-Build-Directories")
		.IsDependentOn("Get-Current-Release-Branch-Prod")
		.IsDependentOn("Git-Pull-Release-Branch-Prod")
		.IsDependentOn("Restore-NuGet-Packages") 
		.IsDependentOn("Build")
		.IsDependentOn("RunTests")
		.IsDependentOn("Git-Prod-Release-Process") 
		.IsDependentOn("Prepare-Artifacts")
		.IsDependentOn("Stage-Artifacts")
	    .Does(() => 
		{
			NotifyReadMe(readMeFile,"PROD Build is completed",GetEmailRecipients());
		});

	Task("PRODUCTIONPublish")
	    .IsDependentOn("ExecutePublish")
	    .IsDependentOn("FlyWay")
	    .IsDependentOn("Restart")
    	.WithCriteria(() => CheckEmailNotifcationEnabled())	 
	    .Does(() => 
		{
			NotifyEventCompletion("","Production Publish is completed",GetEmailRecipients());
		});
	
	
	
