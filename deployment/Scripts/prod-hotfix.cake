//***************** PRODUCTION PRE-DEPLOYMENT PROCESSS *********************
#region PRODUCTION HOTFIX PRE-DEPLOYMENT
	
Task("Git-Prod-Hotfix-Process")     
	.Does(() =>
	{
		Console.WriteLine("");
		Console.WriteLine("started hotfix deployment git process....");		
        Console.WriteLine("");
		
		var cakeRoot=MakeAbsolute(Directory(".")).FullPath;
		var workingRoot=MakeAbsolute(Directory(ConfigSettings.workingDirectory)).FullPath;
		var ConfigFileDir = cakeRoot.Replace(String.Format("{0}{1}",workingRoot,@"/"),"");
						
		var hotfixVersion = currentHotfixBranch.ToUpper().Replace("HOTFIX/V","");		
		var arrVersions = hotfixVersion.Split('.').ToArray();		
		var minorSubVersion = Convert.ToInt32(arrVersions[1]);	
		var newQaTag = String.Format("v{0}.{1}.{2}-qa.0",arrVersions[0],(minorSubVersion),arrVersions[2]);
			
		assemblyVersion =  String.Format("{0}.{1}.{2}",arrVersions[0],(minorSubVersion),arrVersions[2]);
		subVersion = 0;
			
		var masterVersion = String.Format("v{0}",assemblyVersion);
		
		if (String.IsNullOrEmpty(commitMessage.Trim())) 
		{
			commitMessage = "PROD hotfix Automated Build";
		}
							
		// run git commands for release
		Console.WriteLine("");
		ConsoleWriteCustom("fetching all from git...........", ConsoleColor.Blue);
		Console.WriteLine("");
		ProcStart(@"fetch --all");
		
		Console.WriteLine("");
		ConsoleWriteCustom(String.Format("Checking out {0} branch to add files...........",currentHotfixBranch), ConsoleColor.Blue);
		Console.WriteLine("");
		ProcStart(String.Format("checkout {0}",currentHotfixBranch));	 
		
		//sql files
		if(IncludeSqlTasks)
		{	
			Console.WriteLine("");
			ConsoleWriteCustom("Copying release management SQL files from UAT to PROD....", ConsoleColor.Blue);
			Console.WriteLine("");
			
			foreach(var sqlSchema in sqlSchemas)
			{
				var SqlScrtiptsDir =String.Format(@"{0}\sql\{1}\{2}",localRepo,sqlSchema,sqlReleaseManagementDir);
				var SQL_files = GetFiles(System.IO.Path.Combine(SqlScrtiptsDir,"Uat", "*.*")); 
				foreach(var file in SQL_files)
				{
					var filenameSQL = System.IO.Path.GetFileName(file.FullPath);
					var destSQLFilePath = System.IO.Path.Combine(SqlScrtiptsDir,buildMode,filenameSQL);
					if(filenameSQL.ToUpper().StartsWith("V") && !FileExists(destSQLFilePath))
					{
						System.IO.File.Copy(file.FullPath,destSQLFilePath);	
						ProcStart(String.Format("add {0}",filenameSQL));
					}				
				}
			}
		}
				
			
		Console.WriteLine("");
		ConsoleWriteCustom("Committing files...........", ConsoleColor.Blue);
		Console.WriteLine("");
		ProcStart(String.Format(@"commit -m {0}",commitMessage)); 
			
		Console.WriteLine("");
		ConsoleWriteCustom(String.Format("push {0} branch changes to remote...........",currentHotfixBranch), ConsoleColor.Blue);
		Console.WriteLine("");
		ProcStart(String.Format(@"push -u origin {0}",currentHotfixBranch));
			
		Console.WriteLine("");
		ConsoleWriteCustom("Checkout master branch...........", ConsoleColor.Blue);
		Console.WriteLine("");
		ProcStart("checkout master");
			
		Console.WriteLine("");
		ConsoleWriteCustom(String.Format("Merge {0} branch in to master...........",currentHotfixBranch), ConsoleColor.Blue);
		Console.WriteLine("");
		ProcStart(String.Format(@"merge {0} --no-ff",currentHotfixBranch));
				
		using (var repo = new Repository(localRepo))
		{
			branchName="master";
			
			Remote remote = repo.Network.Remotes["origin"];	
															
			LibGit2Sharp.PushOptions pushOptions = new LibGit2Sharp.PushOptions();
					pushOptions.CredentialsProvider = new CredentialsHandler(
								(url, usernameFromUrl, types) =>
										new UsernamePasswordCredentials()
										{
											Username = gitUser,
											Password = gitPassword
										});
				
			var masterBranch = repo.Branches[branchName];
									
			repo.Branches.Update(masterBranch,
					b => b.Remote = remote.Name,
					b => b.UpstreamBranch = masterBranch.CanonicalName);
			
			MergeOptions opts = new MergeOptions() { FileConflictStrategy = CheckoutFileConflictStrategy.Merge };
			repo.Merge(masterBranch, new LibGit2Sharp.Signature(gitUser, gitUser, new DateTimeOffset(DateTime.Now)), opts);

			if (repo.Index.Conflicts.Count() > 0)
			{
				foreach(var itm in repo.Index.Conflicts)
				{
					if(itm.Theirs != null)
					{					
						repo.Index.Add(itm.Theirs.Path);
					}
					else if(itm.Ours != null)
					{
						repo.Index.Add(itm.Ours.Path);	
					}					
				}
					
				Commands.Stage(repo, "*");
								
				// Create the committer's signature and commit
				Signature author = new Signature(gitUser, String.Format("@{0}",gitUser), DateTime.Now);
				Signature committer = author;
											
				// Commit to the repository
				repo.Commit("", author, committer);
				
				//Push Changes to remote
				repo.Network.Push(masterBranch, pushOptions);		
			} 
		}
		
		Console.WriteLine("");	
		ConsoleWriteCustom(String.Format("Delete local {0} branch...........",currentHotfixBranch), ConsoleColor.Blue);
		Console.WriteLine("");
		ProcStart(String.Format("branch -D {0}",currentHotfixBranch));
		
		Console.WriteLine("");			
		ConsoleWriteCustom(String.Format("Delete remote {0} branch...........",currentHotfixBranch), ConsoleColor.Blue);
		Console.WriteLine("");
		ProcStart(String.Format("push origin --delete {0}",currentHotfixBranch));
		
		//Unprotect master branch
		Console.WriteLine("");
		ConsoleWriteCustom("Unprotecting master branch...........", ConsoleColor.Blue);
		Console.WriteLine("");
		UnProtectBranch("master");			
			
		Console.WriteLine("");	
		ConsoleWriteCustom("Pushing master branch to remote...........", ConsoleColor.Blue);
		Console.WriteLine("");
		ProcStart("push origin master");
			
		using (var repo = new Repository(localRepo))
		{   
			branchName="master";
		
			Remote remote = repo.Network.Remotes["origin"];	
																
			LibGit2Sharp.PushOptions pushOptions = new LibGit2Sharp.PushOptions();
						pushOptions.CredentialsProvider = new CredentialsHandler(
								(url, usernameFromUrl, types) =>
										new UsernamePasswordCredentials()
										{
											Username = gitUser,
											Password = gitPassword
										});
				
			var masterBranch = repo.Branches[branchName];
			Branch currentBranch = Commands.Checkout(repo , masterBranch);					
						
			repo.Branches.Update(masterBranch,
					b => b.Remote = remote.Name,
					b => b.UpstreamBranch = masterBranch.CanonicalName);
				
			var head = repo.Branches.Single (branch => branch.FriendlyName == branchName);
			var checkoutOptions = new CheckoutOptions ();
			checkoutOptions.CheckoutModifiers = CheckoutModifiers.Force;
			repo.Checkout(head, checkoutOptions);
						
			LibGit2Sharp.PullOptions options = new LibGit2Sharp.PullOptions();
			options.FetchOptions = new FetchOptions();
			options.FetchOptions.CredentialsProvider = new CredentialsHandler(
						(url, usernameFromUrl, types) =>
								new UsernamePasswordCredentials()
								{
									Username = gitUser,
									Password = gitPassword
								});
											
			repo.Network.Pull(new LibGit2Sharp.Signature(gitUser, gitUser, new DateTimeOffset(DateTime.Now)), options);
											
			MergeOptions opts = new MergeOptions() { FileConflictStrategy = CheckoutFileConflictStrategy.Merge };
			repo.Merge(masterBranch, new LibGit2Sharp.Signature(gitUser, gitUser, new DateTimeOffset(DateTime.Now)), opts);

			if (repo.Index.Conflicts.Count() > 0)
			{
				foreach(var itm in repo.Index.Conflicts)
				{
					if(itm.Theirs != null)
					{					
						repo.Index.Add(itm.Theirs.Path);
					}
					else if(itm.Ours != null)
					{
						repo.Index.Add(itm.Ours.Path);	
					}
				}
				
				Commands.Stage(repo, "*");
									
				// Create the committer's signature and commit
				Signature author = new Signature(gitUser, String.Format("@{0}",gitUser), DateTime.Now);
				Signature committer = author;
											
				// Commit to the repository
				repo.Commit("", author, committer);
				
				//Push Changes to remote
				repo.Network.Push(masterBranch, pushOptions);		
			} 
		}

		//Protect master branch
		Console.WriteLine("");
		ConsoleWriteCustom("protecting master branch.....", ConsoleColor.Blue);
		Console.WriteLine("");
		ProtectBranch("master");		  		

			
		using (var repo = new Repository(localRepo))
		{
			branchName="master";
				
			Remote remote = repo.Network.Remotes["origin"];	
															
			LibGit2Sharp.PushOptions pushOptions = new LibGit2Sharp.PushOptions();
						pushOptions.CredentialsProvider = new CredentialsHandler(
								(url, usernameFromUrl, types) =>
									new UsernamePasswordCredentials()
									{
										Username = gitUser,
										Password = gitPassword
									});
			var branch = repo.Branches[branchName];
			Branch currentBranch = Commands.Checkout(repo , branch);					
							
			repo.Branches.Update(branch,
					b => b.Remote = remote.Name,
					b => b.UpstreamBranch = branch.CanonicalName);
			
			//fetch files from git
			FetchOptions fetchoptions = new FetchOptions();
			fetchoptions.CredentialsProvider = new CredentialsHandler((furl, usernameFromUrl, types) => 
						new UsernamePasswordCredentials() 
						{			
							Username = gitUser,
							Password = gitPassword
						});
						
			foreach (Remote fremote in repo.Network.Remotes)
			{
				IEnumerable<string> refSpecs = fremote.FetchRefSpecs.Select(x => x.Specification);
				Commands.Fetch(repo, fremote.Name, refSpecs, fetchoptions, "");
			}
					
			//commit assembly file and push it 
			CreateAssemblyInfoFile();
									
			//update version in config file
			string json = System.IO.File.ReadAllText(configFilePath);
			dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
				
			jsonObj["version"]= masterVersion;
				
			string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj,Newtonsoft.Json.Formatting.Indented);
			System.IO.File.WriteAllText(configFilePath,output);				
									
				
			//stage the changes
			Commands.Stage(repo, "*");
								
			// Create the committer's signature and commit
			Signature author = new Signature(gitUser, String.Format("@{0}",gitUser), DateTime.Now);
			Signature committer = author;
				
			if (String.IsNullOrEmpty(commitMessage.Trim())) 
			{
				commitMessage = "PROD hotfix Automated Build";
			}
				
			// Commit to the repository
			repo.Commit(commitMessage, author, committer);
			
			//Push Changes to remote
			repo.Network.Push(branch, pushOptions);
				
			Tag t = repo.ApplyTag(masterVersion);		
								
			//push tag
			repo.Network.Push(repo.Network.Remotes["origin"],String.Format("+refs/tags/{0}",masterVersion), pushOptions);
		}
			
		Console.WriteLine("");
		ConsoleWriteCustom("fetching from git...........", ConsoleColor.Blue);
		Console.WriteLine("");
		ProcStart(@"fetch");
			
		Console.WriteLine("");
		ConsoleWriteCustom("Checkout master branch to copy files...........", ConsoleColor.Blue);
		Console.WriteLine("");
		ProcStart("checkout master");
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}Git-Prod-Hotfix-Process ERROR : {1}{0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Git-Prod-Hotfix-Process ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		throw exception;
	});
		
// Get current hotfix branch
Task("Get-Current-Hotfix-Branch-Prod")
	.Does(() =>
	{
		Console.WriteLine("");
		Console.WriteLine("checking for any existing hotfix branch....");		
        Console.WriteLine("");

		List<string> listBranches = new List<string>();
		
		using (var repo = new Repository(localRepo))
		{
			Remote remote = repo.Network.Remotes["origin"];	
						
			foreach(Branch b in repo.Branches.Where(b => !b.IsRemote))
			{
				listBranches.Add(b.FriendlyName);
			}
				
			var hotfixBranches =listBranches.Where(s => s.Contains("hotfix/"));
				
			if (hotfixBranches.Count() > 1)
			{
				ConsoleWriteError("************* FOUND MORE THAN ONE HOTFIX BRANCH *************");
				throw new Exception("************* FOUND MORE THAN ONE HOTFIX BRANCH *************");
			}				
					
			currentHotfixBranch = hotfixBranches.FirstOrDefault();
		}
			
		if (currentHotfixBranch == null) 
		{
			ConsoleWriteError("************* HOTFIX BRANCH DO NOT EXIST  *************");
			throw new Exception("************* HOTFIX BRANCH DO NOT EXIST *************");
		}
		 
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}Get-Current-Hotfix-Branch-Prod ERROR : {1}{0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Get-Current-Hotfix-Branch-Prod ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		throw exception;
	});  

// Pull code from git repo			 
Task("Git-Pull-Prod-Hotfix")
	.Does(() => 
	{		
		Console.WriteLine("");
		Console.WriteLine("pulling code to local machine....");
		Console.WriteLine("");
		
		List<string> listHotfixBranches = new List<string>();
		
		branchName=currentHotfixBranch;
		
		using (var repo = new Repository(localRepo))
		{
			Remote remote = repo.Network.Remotes["origin"];	
						
		    var head = repo.Branches.Single (branch => branch.FriendlyName == branchName);
			var checkoutOptions = new CheckoutOptions ();
			checkoutOptions.CheckoutModifiers = CheckoutModifiers.Force;
			repo.Checkout(head, checkoutOptions);
					
			LibGit2Sharp.PullOptions options = new LibGit2Sharp.PullOptions();
					 options.FetchOptions = new FetchOptions();
					 options.FetchOptions.CredentialsProvider = new CredentialsHandler(
						  (url, usernameFromUrl, types) =>
							  new UsernamePasswordCredentials()
							  {
								  Username = gitUser,
								  Password = gitPassword
							  });
										
			repo.Network.Pull(new LibGit2Sharp.Signature(gitUser, gitUser, new DateTimeOffset(DateTime.Now)), options);
		}
			
		FileAppendText(logFile, String.Format("{0}############## Git Pull Hotfix Branch Completed ##############{0}",Environment.NewLine));				   
		ConsoleWriteInfo( String.Format("{0}############## Git Pull Hotfix Branch Completed ##############{0}",Environment.NewLine));
	})
	.OnError(exception =>
	{
		FileAppendText(logFile, String.Format("{0}Git-Pull-Hotfix-Branch ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		ConsoleWriteError(String.Format("{0}Git-Pull-Hotfix-Branch ERROR :{0}{1}{0}",Environment.NewLine, new CakeException()));
		throw exception;
	});	
	 
   
	#endregion 
	//**************************************************************************
	