
#region User inputs

// Enter build type 
	Console.WriteLine(String.Format("{0}{0}Please select the build mode ?{0}",Environment.NewLine));
  
    var menuBuild = new EasyConsole.Menu();
  	menuBuild
	.Add("QA",() =>{
               			Console.WriteLine( String.Format("{0} Selected.","QA"));
						isQA=true;
						buildMode = "Qa";
						buildModeDescription= "QA";
						environmentVersion = (int)EnvironmentType.QA; 
						SelectedEnvironmentDetails = ConfigSettings.Environments.Qa;
					})
	.Add("UAT",() =>{
               			Console.WriteLine( String.Format("{0} Selected.","UAT"));
						buildMode = "Uat";
						buildModeDescription = "UAT";
						isUAT = true;
						environmentVersion = (int)EnvironmentType.UAT;
						SelectedEnvironmentDetails = ConfigSettings.Environments.Uat;
					})
    .Add("HOTFIX",() =>{
               			  	Console.WriteLine( String.Format("{0} Selected.","Production"));
						  	buildMode = "Uat";
							buildModeDescription= "UAT-Hotfix";
						  	isUATHotFix = true;
						  	environmentVersion = (int)EnvironmentType.UAT;
							SelectedEnvironmentDetails = ConfigSettings.Environments.Uat;
						})
	.Add("PRODUCTION (HOTFIX BRANCH)",() =>{
               			  		Console.WriteLine( String.Format("{0} Selected.","PRODUCTION (HOTFIX BRANCH)"));
						  		buildMode ="Prod";
								buildModeDescription= "Production-Hotfix";
						  		isHotFix = true;
						  		environmentVersion = (int)EnvironmentType.PROD;
								SelectedEnvironmentDetails = ConfigSettings.Environments.Prod;
						  	})
	.Add("PRODUCTION (RELEASE BRANCH)",() =>{
               			  		Console.WriteLine( String.Format("{0} Selected.","PRODUCTION (HOTFIX BRANCH)"));
						  		buildMode = "Prod"; 
								buildModeDescription= "Production";
						  		isProd = true;
						  		environmentVersion = (int)EnvironmentType.PROD;
								SelectedEnvironmentDetails = ConfigSettings.Environments.Prod;
						  	});							  
	menuBuild.Display();

	Console.WriteLine(String.Format("{0}{0}Please select the build Action ?{0}",Environment.NewLine));
	 var menuBuildAction = new EasyConsole.Menu();
  	menuBuildAction
	.Add("STAGE",() =>{
							Console.WriteLine( String.Format("{0} Selected.","STAGE"));
							buildAction="STAGE";
						  })
	.Add("PUBLISH",() =>{
							Console.WriteLine( String.Format("{0} Selected.","PUBLISH"));
							buildAction="PUBLISH";
						  })
    .Add("Cancel",() =>{
							Console.WriteLine( String.Format("Process has been cancelled!"));
							buildAction="Cancel";
							throw new Exception("Process has been cancelled!"); 
						  });						  
	menuBuildAction.Display();

	if(buildAction == "PUBLISH")
	{
		var menuAppsToBEDeployed = new EasyConsole.Menu();
		Console.WriteLine(String.Format("{0}{0}Please select the applications to be deployed?{0}",Environment.NewLine));
        foreach(var app in appsToBeDeployed)
		{
			menuAppsToBEDeployed.Add(app.Name,() =>{
											Console.WriteLine(String.Format("{0}{1} Selected.", Environment.NewLine, app.Name));
											selectedPublishApps = app.Name;
										});
        }
		menuAppsToBEDeployed.Add("All of the above",() =>{
											Console.WriteLine(String.Format("{0}All applications are queued for publish.",Environment.NewLine));
											selectedPublishApps = "All";
										});				
		menuAppsToBEDeployed.Display(); 
	}

#endregion


