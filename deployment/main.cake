#addin nuget:?package=Cake.Json&version=3.0.1
#addin nuget:?package=Newtonsoft.Json&version=11.0.2
#addin nuget:?package=Cake.Git&version=0.18.0
#addin nuget:?package=EasyConsole&version=1.1.0
#addin nuget:?package=LibGit2Sharp&version=0.24.0
#addin nuget:?package=RestSharp&version=106.2.1&loaddependencies=true
#addin nuget:?package=VaultSharp&version=0.10.4003&loaddependencies=true
#addin "nuget:?package=xunit.runner.console"
#addin "Cake.IIS"
#addin "Cake.Incubator"
#addin "Cake.FileHelpers"
#addin nuget:?package=Markdig
#addin nuget:?package=Polly&version=6.0.1 
#addin nuget:?package=morelinq&version=3.0.0
#addin nuget:?package=MarkdownLog&version=0.9.3

using System;
using System.Text;
using System.Net;
using System.IO;
using System.Collections;
using System.Reflection ;
using System.Security.AccessControl;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Mail;
using LibGit2Sharp;
using LibGit2Sharp.Handlers;
using RestSharp; 
using System.Management;
using Polly;
using MoreLinq;
using MarkdownLog;
using Markdig;
using VaultSharp;
using VaultSharp.V1.AuthMethods.AppRole;


var target = Argument("target", "Main");
var SkipTask = Argument("skiptask", "");
var configuration = Argument("configuration", "Release");


#load "./scripts/load.cake"

RunTarget(target);